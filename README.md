# webidl

## References

- https://webidl.spec.whatwg.org/
- https://developer.mozilla.org/en-US/docs/MDN/Contribute/Howto/Write\_an\_API\_reference/Information\_contained\_in\_a\_WebIDL\_file
- https://firefox-source-docs.mozilla.org/dom/webIdlBindings/index.html
- https://github.com/mozilla/gecko-dev/tree/master/dom/webidl
- https://rustwasm.github.io/wasm-bindgen/examples/index.html
