// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-button-element

// http://www.whatwg.org/specs/web-apps/current-work/#the-button-element
#[exposed = Window]
pub trait HTMLButtonElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub autofocus: bool,
  #[ce_reactions, setter_throws, pure]
  pub disabled: bool,
  #[pure]
  form: Option<HTMLFormElement>,
  #[ce_reactions, setter_throws, pure]
  #[alias = "formAction"]
  pub form_action: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "formEnctype"]
  pub form_enctype: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "formMethod"]
  pub form_method: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "formNoValidate"]
  pub form_no_validate: bool,
  #[ce_reactions, setter_throws, pure]
  #[alias = "formTarget"]
  pub form_target: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub name: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub type: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub value: DOMString,

  #[alias = "willValidate"]
  will_validate: bool,
  validity: ValidityState,
  #[throws]
  #[alias = "validationMessage"]
  validation_message: DOMString,
  #[alias = "checkValidity"]
  pub fn check_validity() -> bool;
  #[alias = "reportValidity"]
  pub fn report_validity() -> bool;
  #[alias = "setCustomValidity"]
  pub fn set_custom_validity(error: DOMString);

  labels: NodeList,
}
