// The origin of this IDL file is
// https://webaudio.github.io/web-midi-api/

#[SecureContext, pref = "dom.webmidi.enabled",
 exposed = Window]
pub trait MIDIOutputMap {
  readonly maplike<DOMString, MIDIOutput>;
}
