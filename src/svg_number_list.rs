// The origin of this IDL file is
// http://www.w3.org/TR/SVG11/

#[exposed = Window]
pub trait SVGNumberList {
  #[alias = "numberOfItems"]
  number_of_items: u32,
  #[throws]
  pub fn clear();
  #[throws]
  pub fn initialize(newItem: SVGNumber) -> SVGNumber;
  #[throws]
  getter SVGNumber getItem(index: u32);
  #[throws]
  #[alias = "insertItemBefore"]
  pub fn insert_item_before(newItem: SVGNumber, newItem: SVGNumber) -> SVGNumber;
  #[throws]
  #[alias = "replaceItem"]
  pub fn replace_item(newItem: SVGNumber, newItem: SVGNumber) -> SVGNumber;
  #[throws]
  #[alias = "removeItem"]
  pub fn remove_item(index: u32) -> SVGNumber;
  #[throws]
  #[alias = "appendItem"]
  pub fn append_item(newItem: SVGNumber) -> SVGNumber;

  // Mozilla-specific stuff
  length: u32, // synonym for numberOfItems
}
