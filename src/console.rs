// For more information on this trait, please see
// https://console.spec.whatwg.org/#console-namespace

use super::*;

#[exposed = (Window, Worker, WorkerDebugger, Worklet),
  ClassString = "Console",
  ProtoObjectHack]
namespace console {
  // NOTE: if you touch this namespace, remember to update the ConsoleInstance
  // trait as well!

  // Logging
  #[use_counter]
  pub fn assert(&self, #[optional = false] condition: bool, any... data);
  #[use_counter]
  pub fn clear(&self, );
  #[use_counter]
  pub fn count(&self, #[optional = "default"] label: DOMString);
  #[use_counter]
  #[alias = "countReset"]
  pub fn count_reset(&self, #[optional = "default"] label: DOMString);
  #[use_counter]
  pub fn debug(&self, any... data);
  #[use_counter]
  pub fn error(&self, any... data);
  #[use_counter]
  pub fn info(&self, any... data);
  #[use_counter]
  pub fn log(&self, any... data);
  #[use_counter]
  pub fn table(&self, any... data); // FIXME: The spec is still unclear about this.
  #[use_counter]
  pub fn trace(&self, any... data);
  #[use_counter]
  pub fn warn(&self, any... data);
  #[use_counter]
  pub fn dir(&self, any... data); // FIXME: This doesn't follow the spec yet.
  #[use_counter]
  pub fn dirxml(&self, any... data);

  // Grouping
  #[use_counter]
  pub fn group(&self, any... data);
  #[use_counter]
  #[alias = "groupCollapsed"]
  pub fn group_collapsed(&self, any... data);
  #[use_counter]
  #[alias = "groupEnd"]
  pub fn group_end(&self);

  // Timing
  #[use_counter]
  pub fn time(&self, #[optional = "default"] label: DOMString);
  #[use_counter]
  #[alias = "timeLog"]
  pub fn time_log(&self, #[optional = "default"] label: DOMString, any... data);
  #[use_counter]
  #[alias = "timeEnd"]
  pub fn time_end(&self, #[optional = "default"] label: DOMString);

  // Mozilla only or Webcompat methods

  #[use_counter]
  pub fn _exception(&self, any... data);
  #[use_counter]
  #[alias = "timeStamp"]
  pub fn time_stamp(&self, optional data: any);

  #[use_counter]
  pub fn profile(&self, any... data);
  #[use_counter]
  #[alias = "profileEnd"]
  pub fn profile_end(&self, any... data);

  #[chrome_only]
  const bool IS_NATIVE_CONSOLE = true;

  #[chrome_only, new_object]
  #[alias = "createInstance"]
  pub fn create_instance(&self, #[optional = {}] options: ConsoleInstanceOptions) -> ConsoleInstance;
}

// This is used to propagate console events to the observers.
#[GenerateConversionToJS]
pub struct ConsoleEvent {
  (u64 or DOMString) ID;
  (u64 or DOMString) innerID;
  pub #[optional = ""] consoleID: DOMString,
  pub #[optional = ""] addonId: DOMString,
  pub #[optional = ""] level: DOMString,
  pub #[optional = ""] filename: DOMString,
  // Unique identifier within the process for the script source this event is
  // associated with, or zero.
  pub #[optional = 0] sourceId: u32,
  #[alias = "lineNumber"]
  pub #[optional = 0] line_number: u32,
  #[alias = "columnNumber"]
  pub #[optional = 0] column_number: u32,
  #[alias = "functionName"]
  pub #[optional = ""] function_name: DOMString,
  #[alias = "timeStamp"]
  pub #[optional = 0] time_stamp: f64,
  pub arguments: Vec<any>,
  Vec<Option<DOMString>> styles;
  pub #[optional = false] private: bool,
  // stacktrace is handled via a getter in some cases so we can construct it
  // lazily. Note that we're not making this whole thing an trait because
  // consumers expect to see own properties on it, which would mean making the
  // props unforgeable, which means lots of JSfunction allocations. Maybe we
  // should fix those consumers, of course....
  // Vec<ConsoleStackEntry> stacktrace;
  #[alias = "groupName"]
  pub #[optional = ""] group_name: DOMString,
  pub #[optional = None] timer: any,
  pub #[optional = None] counter: any,
  pub #[optional = ""] prefix: DOMString,
  #[alias = "chromeContext"]
  pub #[optional = false] chrome_context: bool,
}

// Event for profile operations
#[GenerateConversionToJS]
pub struct ConsoleProfileEvent {
  pub #[optional = ""] action: DOMString,
  pub arguments: Vec<any>,
  #[alias = "chromeContext"]
  pub #[optional = false] chrome_context: bool,
}

// This pub struct is used to manage stack trace data.
#[GenerateConversionToJS]
pub struct ConsoleStackEntry {
  pub #[optional = ""] filename: DOMString,
  // Unique identifier within the process for the script source this entry is
  // associated with, or zero.
  pub #[optional = 0] sourceId: u32,
  #[alias = "lineNumber"]
  pub #[optional = 0] line_number: u32,
  #[alias = "columnNumber"]
  pub #[optional = 0] column_number: u32,
  #[alias = "functionName"]
  pub #[optional = ""] function_name: DOMString,
  #[alias = "asyncCause"]
  pub async_cause: Option<DOMString>,
}

#[GenerateConversionToJS]
pub struct ConsoleTimerStart {
  pub #[optional = ""] name: DOMString,
}

#[GenerateConversionToJS]
pub struct ConsoleTimerLogOrEnd {
  pub #[optional = ""] name: DOMString,
  pub #[optional = 0] duration: f64,
}

#[GenerateConversionToJS]
pub struct ConsoleTimerError {
  pub #[optional = ""] error: DOMString,
  pub #[optional = ""] name: DOMString,
}

#[GenerateConversionToJS]
pub struct ConsoleCounter {
  pub #[optional = ""] label: DOMString,
  pub #[optional = 0] count: u32,
}

#[GenerateConversionToJS]
pub struct ConsoleCounterError {
  pub #[optional = ""] label: DOMString,
  pub #[optional = ""] error: DOMString,
}

#[chrome_only,
 exposed = (Window, Worker, WorkerDebugger, Worklet)]
// This is basically a copy of the console namespace.
pub trait ConsoleInstance {
  // Logging
  pub fn assert(&self, #[optional = false] condition: bool, any... data);
  pub fn clear(&self);
  pub fn count(&self, #[optional = "default"] label: DOMString);
  #[alias = "countReset"]
  pub fn count_reset(&self, #[optional = "default"] label: DOMString);
  pub fn debug(&self, any... data);
  pub fn error(&self, any... data);
  pub fn info(&self, any... data);
  pub fn log(&self, any... data);
  pub fn table(&self, any... data); // FIXME: The spec is still unclear about this.
  pub fn trace(&self, any... data);
  pub fn warn(&self, any... data);
  pub fn dir(&self, any... data); // FIXME: This doesn't follow the spec yet.
  pub fn dirxml(&self, any... data);

  // Grouping
  pub fn group(&self, any... data);
  #[alias = "groupCollapsed"]
  pub fn group_collapsed(&self, any... data);
  #[alias = "groupEnd"]
  pub fn group_end(&self);

  // Timing
  pub fn time(&self, #[optional = "default"] label: DOMString);
  #[alias = "timeLog"]
  pub fn time_log(&self, #[optional = "default"] label: DOMString, any... data);
  #[alias = "timeEnd"]
  pub fn time_end(&self, #[optional = "default"] label: DOMString);

  // Mozilla only or Webcompat methods

  pub fn _exception(&self, any... data);
  #[alias = "timeStamp"]
  pub fn time_stamp(&self, optional data: any);

  pub fn profile(&self, any... data);
  #[alias = "profileEnd"]
  pub fn profile_end(&self, any... data);
}

callback ConsoleInstanceDumpCallback = void (message: DOMString);

pub enum ConsoleLogLevel {
  "All",
  "Debug",
  "Log",
  "Info",
  "Clear",
  "Trace",
  "TimeLog",
  "TimeEnd",
  "Time",
  "Group",
  "GroupEnd",
  "Profile",
  "ProfileEnd",
  "Dir",
  "Dirxml",
  "Warn",
  "Error",
  "Off"
}

pub struct ConsoleInstanceOptions {
  // An optional to: function intercept all strings written to stdout.
  pub dump: ConsoleInstanceDumpCallback,

  // An optional string: prefix to be printed before the actual logged message.
  pub #[optional = ""] prefix: DOMString,

  // An ID representing the source of the message. Normally the inner ID of a
  // DOM window.
  pub #[optional = ""] innerID: DOMString,

  // String identified for the console, this will be passed through the console
  // notifications.
  pub #[optional = ""] consoleID: DOMString,

  // Identifier that allows to filter which messages are logged based on their
  // log level.
  #[alias = "maxLogLevel"]
  pub max_log_level: ConsoleLogLevel,

  // String pref name which contains the level to use for maxLogLevel. If the
  // pref doesn't exist, gets removed or it is used in workers, the maxLogLevel
  // will default to the value passed to this constructor (or "all" if it wasn't
  // specified).
  #[alias = "maxLogLevelPref"]
  pub #[optional = ""] max_log_level_pref: DOMString,
}

pub enum ConsoleLevel {
  #[alias = "log"]
  Log,
  #[alias = "warning"]
  Warning,
  #[alias = "error"]
  Error
}

// this trait is just for testing
partial trait ConsoleInstance {
  #[chrome_only]
  #[alias = reportForServiceWorkerScope]
  pub fn report_for_service_worker_scope(
    &self,
    scope: DOMString, message: DOMString,
    filename: DOMString, line_number: u32,
    column_number: u32,
    level: ConsoleLevel);
}
