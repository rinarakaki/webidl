#[Global = (WorkerDebugger), exposed = WorkerDebugger]
pub trait WorkerDebuggerGlobalScope: EventTarget {
  #[throws]
  global: object,

  #[alias = "createSandbox"]
  pub fn create_sandbox(&self, name: DOMString, name: DOMString) -> Result<object>;

  #[alias = "loadSubScript"]
  pub fn load_sub_script(&self, url: DOMString, optional sandbox: object) -> Result<()>;

  #[alias = "enterEventLoop"]
  pub fn enter_event_loop(&self, );

  #[alias = "leaveEventLoop"]
  pub fn leave_event_loop(&self, );

  #[alias = "postMessage"]
  pub fn post_message(&self, message: DOMString);

  #[alias = "onmessage"]
  pub on_message: EventHandler,

  #[alias = "onmessageerror"]
  pub on_message_error: EventHandler,

  #[alias = "setImmediate"]
  pub fn set_immediate(&self, handler: function) -> Result<()>;

  #[alias = "reportError"]
  pub fn report_error(&self, message: DOMString);

  #[alias = "retrieveConsoleEvents"]
  pub fn retrieve_console_events(&self, ) -> Result<Vec<any>>;

  #[alias = "setConsoleEventHandler"]
  pub fn set_console_event_handler(&self, handler: Option<AnyCallback>) -> Result<()>;

  // base64 utility methods
  pub fn btoa(&self, btoa: DOMString) -> Result<DOMString>;
  pub fn atob(&self, atob: DOMString) -> Result<DOMString>;
}

// So you can debug while you debug
partial trait WorkerDebuggerGlobalScope {
  pub fn dump(&self, optional string: DOMString);
}
