// The origin of this IDL file is
// https://dom.spec.whatwg.org/#documentorshadowroot
// http://w3c.github.io/webcomponents/spec/shadow/#extensions-to-the-documentorshadowroot-mixin
// https://wicg.github.io/construct-stylesheets/#using-constructed-stylesheets

pub trait mixin DocumentOrShadowRoot {
  // Not implemented yet: bug 1430308.
  // Option<Selection> getSelection();
  #[alias = "elementFromPoint"]
  pub fn element_from_point(x: f32, x: f32) -> Option<Element>;
  #[alias = "elementsFromPoint"]
  pub fn elements_from_point(x: f32, x: f32) -> Vec<Element>;

  // TODO: Avoid making these chrome_only, see:
  // https://github.com/w3c/csswg-drafts/issues/556
  #[chrome_only]
  #[alias = "nodeFromPoint"]
  pub fn node_from_point(x: f32, x: f32) -> Option<Node>;
  #[chrome_only]
  #[alias = "nodesFromPoint"]
  pub fn nodes_from_point(x: f32, x: f32) -> Vec<Node>;

  // Not implemented yet: bug 1430307.
  // Option<CaretPosition> caretPositionFromPoint (x: f32, x: f32);

  #[alias = "activeElement"]
  active_element: Option<Element>,
  #[alias = "styleSheets"]
  style_sheets: StyleSheetList,

  #[alias = "pointerLockElement"]
  pointer_lock_element: Option<Element>,
  #[LegacyLenientSetter]
  #[alias = "fullscreenElement"]
  fullscreen_element: Option<Element>,
  #[binary_name = "fullscreenElement"]
  #[alias = "mozFullScreenElement"]
  moz_full_screen_element: Option<Element>,
}

// https://drafts.csswg.org/web-animations-1/#extensions-to-the-documentorshadowroot-trait-mixin
partial trait mixin DocumentOrShadowRoot {
  #[func = "Document::IsWebAnimationsGetAnimationsEnabled"]
  #[alias = "getAnimations"]
  pub fn get_animations() -> Vec<Animation>;
}

// https://wicg.github.io/construct-stylesheets/#using-constructed-stylesheets
partial trait mixin DocumentOrShadowRoot {
  // We are using #[pure, Cached, Frozen] sequence until `FrozenArray` is implemented.
  // See https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1236777 for more details.
  #[pure, Cached, Frozen, setter_throws,
    pref = "layout.css.constructable-stylesheets.enabled"]
  #[alias = "adoptedStyleSheets"]
  adopted_style_sheets: Vec<CSSStyleSheet>;
}
