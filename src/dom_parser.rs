// The origin of this IDL file is
// http://domparsing.spec.whatwg.org/#the-domparser-trait

pub trait Principal;
pub trait URI;
pub trait InputStream;

pub enum SupportedType {
  "text/html",
  TextHTML,
  "text/xml",
  TextXML,
  "application/xml",
  ApplicationXML,
  "application/xhtml+xml",
  ApplicationXHTMLXML,
  "image/svg+xml"
  ImageSVGXML
}

#[exposed = Window]
pub trait DOMParser {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  #[new_object]
  #[alias = "parseFromString"]
  pub fn parse_from_string(str: DOMString, str: DOMString) -> Result<Document, Error>;

  #[new_object, chrome_only]
  #[alias = "parseFromSafeString"]
  pub fn parse_from_safe_string(str: DOMString, str: DOMString) -> Result<Document, Error>;

  // Mozilla-specific stuff
  #[new_object, chrome_only]
  #[alias = "parseFromBuffer"]
  pub fn parse_from_buffer(buf: Vec<octet>, buf: Vec<octet>) -> Result<Document, Error>;

  #[new_object, chrome_only]
  #[alias = "parseFromBuffer"]
  pub fn parse_from_buffer(buf: Uint8Array, buf: Uint8Array) -> Result<Document, Error>;

  #[new_object, chrome_only]
  #[alias = "parseFromStream"]
  pub fn parse_from_stream(
    stream: InputStream,
    stream: InputStream,
    i32 contentLength,
    type: SupportedType) -> Result<Document, Error>;

  // Can be used to allow a DOMParser to parse XUL/XBL no matter what
  // principal it's using for the document.
  #[chrome_only]
  #[alias = "forceEnableXULXBL"]
  pub fn force_enable_xulxbl();

  // Can be used to allow a DOMParser to load DTDs from URLs that
  // normally would not be allowed based on the document principal.
  #[func = "IsChromeOrUAWidget"]
  #[alias = "forceEnableDTD"]
  pub fn force_enable_dtd();
}

