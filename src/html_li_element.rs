// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-li-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-li-element
#[exposed = Window]
pub trait HTMLLIElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub value: i32,
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLLIElement {
  #[ce_reactions, setter_throws, pure]
  pub type: DOMString,
}
