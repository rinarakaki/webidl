// https://wicg.github.io/entries-api/#idl-index

callback FileCallback = void (file: File);

#[exposed = Window]
pub trait FileSystemFileEntry: FileSystemEntry {
  #[binary_name = "GetFile"]
  void file (successCallback: FileCallback,
  optional errorCallback: ErrorCallback);
}
