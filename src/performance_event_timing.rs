// The origin of this IDL file is
// https://w3c.github.io/navigation-timing/#the-performancenavigation-trait

#[pref = "dom.enable_event_timing",
 exposed = Window]
pub trait EventCounts {
  readonly maplike<DOMString, u64>;
}

#[pref = "dom.enable_event_timing",
 exposed = Window]
pub trait PerformanceEventTiming: PerformanceEntry {
  #[alias = "processingStart"]
  processing_start: DOMHighResTimeStamp,
  #[alias = "processingEnd"]
  processing_end: DOMHighResTimeStamp,
  cancelable: bool,
  target: Option<Node>,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
