// The origin of this IDL file is
// https://dvcs.w3.org/hg/dap/raw-file/default/media-stream-capture/ImageCapture.html

#[pref = "dom.imagecapture.enabled",
 exposed = Window]
pub trait ImageCaptureErrorEvent: Event {
  constructor(
    type: DOMString,
    #[optional = {}] imageCaptureErrorInitDict: ImageCaptureErrorEventInit) Self;

  #[alias = "imageCaptureError"]
  image_capture_error: Option<ImageCaptureError>,
}

pub struct ImageCaptureErrorEventInit: EventInit {
  #[alias = "imageCaptureError"]
  pub image_capture_error: Option<ImageCaptureError> = None,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait ImageCaptureError {
  const u16 FRAME_GRAB_ERROR = 1;
  const u16 SETTINGS_ERROR = 2;
  const u16 PHOTO_ERROR = 3;
  const u16 ERROR_UNKNOWN = 4;
  code: u16,
  message: DOMString,
}

