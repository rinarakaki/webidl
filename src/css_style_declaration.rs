// The origin of this IDL file is
// http://dev.w3.org/csswg/cssom/

 // Because of getComputedStyle, many CSSStyleDeclaration objects can be
 // short-living.
#[ProbablyShortLivingWrapper,
 exposed = Window]
pub trait CSSStyleDeclaration {
  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws]
  #[alias = "cssText"]
  pub css_text: UTF8String,

  length: u32,
  getter UTF8String item(index: u32);

  #[chrome_only]
  #[alias = "getCSSImageURLs"]
  pub fn get_css_image_urls(&self, property: UTF8String) -> Result<Vec<UTF8String>>;

  #[alias = "getPropertyValue"]
  pub fn get_property_value(&self, property: UTF8String) -> Result<UTF8String>;

  #[alias = "getPropertyPriority"]
  pub fn get_property_priority(&self, property: UTF8String) -> UTF8String;

  // TODO Fix
  #[ce_reactions, needs_subject_principal = NonSystem, throws]
  #[alias = "setProperty"]
  pub fn set_property(
    &self, 
    property: UTF8String,
    value: [LegacyNullToEmptyString] UTF8String,
    #[optional = ""] priority: [LegacyNullToEmptyString] UTF8String),

  #[ce_reactions]
  #[alias = "removeProperty"]
  pub fn remove_property(&self, property: UTF8String) -> Result<UTF8String>;

  #[alias = "parentRule"]
  parent_rule: Option<CSSRule>,
}
