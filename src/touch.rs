// The origin of this IDL file is
// http://dvcs.w3.org/hg/webevents/raw-file/default/touchevents.html

pub struct TouchInit {
  required i32 identifier;
  required EventTarget target;
  pub clientX: i32 = 0,
  pub clientY: i32 = 0,
  pub screenX: i32 = 0,
  pub screenY: i32 = 0,
  pub pageX: i32 = 0,
  pub pageY: i32 = 0,
  pub radiusX: f32 = 0,
  pub radiusY: f32 = 0,
  #[alias = "rotationAngle"]
  pub rotation_angle: f32 = 0,
  pub force: f32 = 0,
}

#[func = "mozilla::dom::Touch::PrefEnabled",
 exposed = Window]
pub trait Touch {
  #[alias = "constructor"]
  pub fn new(touchInitDict: TouchInit) -> Self;
 
  identifier: i32,
  target: Option<EventTarget>,
  #[needs_caller_type]
  screenX: i32,
  #[needs_caller_type]
  screenY: i32,
  clientX: i32,
  clientY: i32,
  pageX: i32,
  pageY: i32,
  #[needs_caller_type]
  radiusX: i32,
  #[needs_caller_type]
  radiusY: i32,
  #[needs_caller_type]
  #[alias = "rotationAngle"]
  rotation_angle: f32,
  #[needs_caller_type]
  force: f32,
}
