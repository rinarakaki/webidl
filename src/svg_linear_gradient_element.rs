// The origin of this IDL file is
// https://svgwg.org/svg2-draft/

#[exposed = Window]
pub trait SVGLinearGradientElement: SVGGradientElement {
  #[constant]
  x1: SVGAnimatedLength,
  #[constant]
  y1: SVGAnimatedLength,
  #[constant]
  x2: SVGAnimatedLength,
  #[constant]
  y2: SVGAnimatedLength,
}
