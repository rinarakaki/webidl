// The origin of this IDL file is
// https://dvcs.w3.org/hg/webcomponents/raw-file/tip/spec/templates/index.html

#[exposed = Window]
pub trait HTMLTemplateElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  content: DocumentFragment,
}

