// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/getusermedia.html

// These two enums are in the spec even though they're not used directly in the
// API due to https://www.w3.org/Bugs/Public/show_bug.Option<cgi>id = 19936
// Their binding code is used in the implementation.

pub enum VideoFacingModeEnum {
  #[alias = "user"]
  User,
  #[alias = "environment"]
  Environment,
  #[alias = "left"]
  Left,
  #[alias = "right"]
  Right
}

pub enum MediaSourceEnum {
  #[alias = "camera"]
  Camera,
  #[alias = "screen"]
  Screen,
  #[alias = "application"]
  Application,
  #[alias = "window"]
  Window,
  #[alias = "browser"]
  Browser,
  #[alias = "microphone"]
  Microphone,
  #[alias = "audioCapture"]
  AudioCapture,
  #[alias = "other"]
  Other
  // If values are added, adjust n_values in Histograms.json (places: 2)
}

pub struct ConstrainLongRange {
  pub min: i32,
  pub max: i32,
  pub exact: i32,
  pub ideal: i32,
}

pub struct ConstrainDoubleRange {
  pub min: f64,
  pub max: f64,
  pub exact: f64,
  pub ideal: f64,
}

pub struct ConstrainBooleanParameters {
  pub exact: bool,
  pub ideal: bool,
}

pub struct ConstrainDOMStringParameters {
  (DOMString or Vec<DOMString>) exact;
  (DOMString or Vec<DOMString>) ideal;
}

pub type ConstrainLong = (i32 or ConstrainLongRange);
pub type ConstrainDouble = (f64 or ConstrainDoubleRange);
pub type ConstrainBoolean = (bool or ConstrainBooleanParameters);
pub type ConstrainDOMString = (DOMString or Vec<DOMString> or ConstrainDOMStringParameters);

// Note: When adding new constraints, remember to update the SelectSettings()
// function in MediaManager.cpp to make OverconstrainedError's constraint work!

pub struct MediaTrackConstraintSet {
  pub width: ConstrainLong,
  pub height: ConstrainLong,
  #[alias = "frameRate"]
  pub frame_rate: ConstrainDouble,
  #[alias = "facingMode"]
  pub facing_mode: ConstrainDOMString,
  #[alias = "mediaSource"]
  pub media_source: DOMString,
  #[alias = "browserWindow"]
  pub browser_window: i64,
  #[alias = "scrollWithPage"]
  pub scroll_with_page: bool,
  pub deviceId: ConstrainDOMString,
  pub groupId: ConstrainDOMString,
  #[alias = "viewportOffsetX"]
  pub viewport_offsetX: ConstrainLong,
  #[alias = "viewportOffsetY"]
  pub viewport_offsetY: ConstrainLong,
  #[alias = "viewportWidth"]
  pub viewport_width: ConstrainLong,
  #[alias = "viewportHeight"]
  pub viewport_height: ConstrainLong,
  #[alias = "echoCancellation"]
  pub echo_cancellation: ConstrainBoolean,
  #[alias = "noiseSuppression"]
  pub noise_suppression: ConstrainBoolean,
  #[alias = "autoGainControl"]
  pub auto_gain_control: ConstrainBoolean,
  #[alias = "channelCount"]
  pub channel_count: ConstrainLong,
}

#[GenerateToJSON]
pub struct MediaTrackConstraints: MediaTrackConstraintSet {
  pub advanced: Vec<MediaTrackConstraintSet>,
}

pub enum MediaStreamTrackState {
  #[alias = "live"]
  Live,
  #[alias = "ended"]
  Ended
}

#[exposed = Window]
pub trait MediaStreamTrack: EventTarget {
  kind: DOMString,
  id: DOMString,
  #[needs_caller_type]
  label: DOMString,
  pub enabled: bool,
  muted: bool,
  #[alias = "onmute"]
  pub on_mute: EventHandler,
  #[alias = "onunmute"]
  pub on_unmute: EventHandler,
  #[alias = "readyState"]
  ready_state: MediaStreamTrackState,
  #[alias = "onended"]
  pub on_ended: EventHandler,
  pub fn clone() -> MediaStreamTrack;
  pub fn stop();
  //  MediaTrackCapabilities getCapabilities();
  #[alias = "getConstraints"]
  pub fn get_constraints() -> MediaTrackConstraints;

  #[needs_caller_type]
  #[alias = "getSettings"]
  pub fn get_settings() -> MediaTrackSettings;

  #[throws, needs_caller_type]
  #[alias = "applyConstraints"]
  pub fn apply_constraints(#[optional = {}] constraints: MediaTrackConstraints)
    -> Promise<void> ;
  // #[alias = "onoverconstrained"]
  // pub on_overconstrained: EventHandler;
}
