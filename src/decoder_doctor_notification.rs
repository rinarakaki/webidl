pub enum DecoderDoctorNotificationType {
  #[alias = "cannot-play"]
  CannotPlay,
  "platform-decoder-not-found",
  "can-play-but-some-missing-decoders",
  #[alias = "cannot-initialize-pulseaudio"]
  CannotInitialisePulseaudio,
  #[alias = "unsupported-libavcodec"]
  UnsupportedLibavcodec,
  #[alias = "decode-error"]
  DecodeError,
  #[alias = "decode-warning"]
  DecodeWarning,
}

pub enum DecoderDoctorReportType {
  #[alias = "mediawidevinenowmf"]
  MediaWidevinenowmf,  // TODO
  #[alias = "mediawmfneeded"]
  MediaWmfNeeded,  // TODO
  #[alias = "mediaplatformdecodernotfound"]
  MediaPlatformDecoderNotFound,
  #[alias = "mediacannotplaynodecoders"]
  MediaCannotPlaynodecoders,  // TODO
  #[alias = "medianodecoders"]
  MediaNodecoders,  // TODO
  #[alias = "mediacannotinitializepulseaudio"]
  MediaCannotInitialisePulseaudio,  // TODO
  #[alias = "mediaunsupportedlibavcodec"]
  MediaUnsupportedLibavcodec,  // TODO
  #[alias = "mediadecodeerror"]
  MediaDecodeError,
  #[alias = "mediadecodewarning"]
  MediaDecodeWarning,
}

#[GenerateToJSON]
pub struct DecoderDoctorNotification {
  required DecoderDoctorNotificationType type;
  // True when the issue has been solved.
  required bool isSolved;
  // Key from dom.properties, used for telemetry and prefs.
  required DOMString decoderDoctorReportId;
  // If provided, formats (or key systems) at issue.
  pub formats: DOMString,
  // If provided, technical details about the decode-error/warning.
  #[alias = "decodeIssue"]
  pub decode_issue: DOMString,
  // If provided, URL of the document where the issue happened.
  #[alias = "docURL"]
  pub doc_url: DOMString,
  // If provided, URL of the media resource that caused a decode-error/warning.
  #[alias = "resourceURL"]
  pub resource_url: DOMString,
}
