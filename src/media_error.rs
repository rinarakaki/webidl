// The origin of this IDL file is
// http://www.whatwg.org/html/#mediaerror

#[exposed = Window]
pub trait MediaError {
  // Keep these constants in sync with the ones defined in HTMLMediaElement.h
  const u16 MEDIA_ERR_ABORTED = 1;
  const u16 MEDIA_ERR_NETWORK = 2;
  const u16 MEDIA_ERR_DECODE = 3;
  const u16 MEDIA_ERR_SRC_NOT_SUPPORTED = 4;

  #[constant]
  code: u16,
  message: DOMString,
}
