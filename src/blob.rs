// The origin of this IDL file is
// https://w3c.github.io/FileAPI/#blob

use super::*;

pub type BlobPart = (BufferSource or Blob or USVString);

#[exposed = (Window, Worker)]
pub trait Blob {
  #[alias = "constructor"]
  pub fn new(
    #[optional] blob_parts: Vec<BlobPart>,
    #[optional = {}] options: BlobPropertyBag) -> Result<Self>;

  #[getter_throws]
  pub size: u64,

  pub type: DOMString,

  // Slice Blob into byte-ranged chunks

  pub fn slice(
    &self,
    #[optional] start: #[Clamp] i64,  // TODO
    #[optional] end: #[Clamp] i64,
    #[optional] content_type: DOMString) -> Result<Self>;

  // Read from the Blob.
  #[new_object]
  pub fn stream(&self) -> Result<ReadableStream>;

  #[new_object]
  pub fn text(&self) -> Promise<USVString>;
  
  #[new_object]
  #[alias = "arrayBuffer"]
  pub fn array_buffer(&self) -> Promise<ArrayBuffer>;
}

pub enum EndingType {
  #[alias = "transparent"]
  Transparent,
  #[alias = "native"]
  Native
}

pub struct BlobPropertyBag {
  pub #[optional = ""] type: DOMString,
  pub #[optional = Transparent] endings: EndingType,
}

partial trait Blob {
  // This returns the type of BlobImpl used for this Blob.
  #[chrome_only]
  #[alias = "blobImplType"]
  pub blob_impl_type: DOMString,
}

