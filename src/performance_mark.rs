// The origin of this IDL file is
// http://www.w3.org/TR/user-timing/#performancemark

#[exposed = (Window, Worker)]
pub trait PerformanceMark: PerformanceEntry {
}
