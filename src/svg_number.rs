// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGNumber {
  #[setter_throws]
  pub value: f32,
}
