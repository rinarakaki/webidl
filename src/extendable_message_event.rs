// For more information on this trait, please see
// https://w3c.github.io/ServiceWorker/#extendablemessage-event-section

use super::ExtendableEvent;

#[exposed = (ServiceWorker)]
pub trait ExtendableMessageEvent: ExtendableEvent {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] eventInitDict: ExtendableMessageEventInit);

  // Custom data associated with this event.
  #[getter_throws]
  data: any,

  // The origin of the site from which this event originated.
  origin: DOMString,

  // The last event ID string of the event source.
  #[alias = "lastEventId"]
  last_event_id: DOMString,

  // The client, service worker or port which originated this event.
  readonly attribute (Client or ServiceWorker or MessagePort)? source;

  #[constant, Cached, Frozen]
  ports: Vec<MessagePort>,
}

pub struct ExtendableMessageEventInit: ExtendableEventInit {
  pub data: any = None,
  pub origin: DOMString = "",
  #[alias = "lastEventId"]
  pub last_event_id: DOMString = "",
  (Client or ServiceWorker or MessagePort)? source = None;
  pub ports: Vec<MessagePort> = [],
}
