// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/#audioworkletglobalscope

use super::*;

// TODO `callback constructor`
#[constructor]
type AudioWorkletProcessorConstructor =
  Fn(options: object) -> AudioWorkletProcessor;

#[Global = (Worklet, AudioWorklet), exposed = AudioWorklet]
pub trait AudioWorkletGlobalScope: WorkletGlobalScope {
  #[alias = "registerProcessor"]
  pub fn register_pocessor(
    &self, name: DOMString, processor_ctor: AudioWorkletProcessorConstructor)
    -> Result<()>;

  #[alias = "currentFrame"]
  pub current_frame: u64,
  #[alias = "currentTime"]
  pub current_time: f64,
  #[alias = "sampleRate"]
  pub sample_rate: f32,
}
