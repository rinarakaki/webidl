// The origin of this IDL file is
// https://w3c.github.io/mediacapture-record/

pub struct MediaRecorderErrorEventInit: EventInit {
  required DOMException error;
}

#[exposed = Window]
pub trait MediaRecorderErrorEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;

  #[same_object]
  error: DOMException,
}
