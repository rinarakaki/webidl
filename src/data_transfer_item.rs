// The origin of this IDL file is:
// https://html.spec.whatwg.org/multipage/interaction.html#the-datatransferitem-trait
// https://wicg.github.io/entries-api/#idl-index

use super::File;

#[exposed = Window]
pub trait DataTransferItem {
  kind: DOMString,
  type: DOMString,

  #[needs_subject_principal]
  #[alias = "getAsString"]
  pub fn get_as_string(callback: Option<FunctionStringCallback>)
    -> Result<()>;

  #[needs_subject_principal]
  #[alias = "getAsFile"]
  pub fn get_as_file() -> Result<Option<File>>;
}

callback FunctionStringCallback = void (data: DOMString);

// https://wicg.github.io/entries-api/#idl-index
partial trait DataTransferItem {
  #[pref = "dom.webkitBlink.filesystem.enabled", binary_name = "getAsEntry",
    needs_subject_principal]
  #[alias = "webkitGetAsEntry"]
  pub fn webkit_get_as_entry() -> Result<Option<FileSystemEntry>>;
}
