// For more information on this trait, please see
// http://www.whatwg.org/specs/web-apps/current-work/#beforeunloadevent

use super::*;

#[exposed = Window]
pub trait BeforeUnloadEvent: Event {
  #[alias = "returnValue"]
  pub mut return_value: DOMString,
}
