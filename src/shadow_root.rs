// The origin of this IDL file is
// https://dvcs.w3.org/hg/webcomponents/raw-file/tip/spec/shadow/index.html

// https://dom.spec.whatwg.org/#enumdef-shadowrootmode
pub enum ShadowRootMode {
  #[alias = "open"]
  Open,
  #[alias = "closed"]
  Closed
}

pub enum SlotAssignmentMode {
  #[alias = "manual"]
  Manual,
  #[alias = "named"]
  Named
}

// https://dom.spec.whatwg.org/#shadowroot
#[exposed = Window]
pub trait ShadowRoot: DocumentFragment {
  // Shadow DOM v1
  mode: ShadowRootMode,
  #[pref = "dom.shadowdom.delegatesFocus.enabled"]
  #[alias = "delegatesFocus"]
  delegates_focus: bool,
  #[pref = "dom.shadowdom.slot.assign.enabled"]
  #[alias = "slotAssignment"]
  slot_assignment: SlotAssignmentMode,
  host: Element,
  #[alias = "onslotchange"]
  pub onslotchange: EventHandler,

  #[alias = "getElementById"]
  pub fn get_element_by_id(elementId: DOMString) -> Option<Element>;

  // https://w3c.github.io/DOM-Parsing/#the-innerhtml-mixin
  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString innerHTML;

  // When JS invokes importNode or createElement, the binding code needs to
  // create a reflector, and so invoking those methods directly on the content
  // document would cause the reflector to be created in the content scope,
  // at which point it would be difficult to move into the UA Widget scope.
  // As such, these methods allow UA widget code to simultaneously create nodes
  // and associate them with the UA widget tree, so that the reflectors get
  // created in the right scope.
  #[ce_reactions, throws, func = "IsChromeOrUAWidget"]
  #[alias = "importNodeAndAppendChildAt"]
  pub fn import_node_and_append_child_at(parentNode: Node, parentNode: Node, #[optional = false] deep: bool) -> Node;

  #[ce_reactions, throws, func = "IsChromeOrUAWidget"]
  #[alias = "createElementAndAppendChildAt"]
  pub fn create_element_and_append_child_at(parentNode: Node, parentNode: Node) -> Node;

  // For triggering UA Widget scope in tests.
  #[chrome_only]
  #[alias = "setIsUAWidget"]
  pub fn set_is_ua_widget();
  #[chrome_only]
  #[alias = "isUAWidget"]
  pub fn is_ua_widget() -> bool;
}

ShadowRoot includes DocumentOrShadowRoot;
