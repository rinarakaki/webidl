// The origin of this IDL file is
// https://w3c.github.io/gamepad/extensions.html#gamepadpose-trait

#[pref = "dom.gamepad.extensions.enabled",
 exposed = Window,
 SecureContext]
pub trait GamepadPose
{
  #[alias = "hasOrientation"]
  has_orientation: bool,
  #[alias = "hasPosition"]
  has_position: bool,

  // position, linearVelocity, and linearAcceleration are 3-component vectors.
  // position is relative to a sitting space. Transforming this point with
  // VRStageParameters.sittingToStandingTransform converts this to standing space.
  #[constant, throws]
  position: Option<F32Array>,
  #[constant, throws]
  #[alias = "linearVelocity"]
  linear_velocity: Option<F32Array>,
  #[constant, throws]
  #[alias = "linearAcceleration"]
  linear_acceleration: Option<F32Array>,

  // orientation is a 4-entry array representing the components of a quaternion. #[constant, throws]
  orientation: Option<F32Array>,
  // angularVelocity and angularAcceleration are the components of 3-dimensional vectors. #[constant, throws]
  #[alias = "angularVelocity"]
  angular_velocity: Option<F32Array>,
  #[constant, throws]
  #[alias = "angularAcceleration"]
  angular_acceleration: Option<F32Array>,
}
