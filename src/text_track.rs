// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#texttrack

pub enum TextTrackKind {
  #[alias = "subtitles"]
  Subtitles,
  #[alias = "captions"]
  Captions,
  #[alias = "descriptions"]
  Descriptions,
  #[alias = "chapters"]
  Chapters,
  #[alias = "metadata"]
  Metadata
}

pub enum TextTrackMode {
  #[alias = "disabled"]
  Disabled,
  #[alias = "hidden"]
  Hidden,
  #[alias = "showing"]
  Showing
}

#[exposed = Window]
pub trait TextTrack: EventTarget {
  kind: TextTrackKind,
  label: DOMString,
  language: DOMString,

  id: DOMString,
  #[alias = "inBandMetadataTrackDispatchType"]
  in_band_metadata_track_dispatch_type: DOMString,

  pub mode: TextTrackMode,

  cues: Option<TextTrackCueList>,
  #[alias = "activeCues"]
  active_cues: Option<TextTrackCueList>,

  #[alias = "addCue"]
  pub fn add_cue(cue: VTTCue);
  #[throws]
  #[alias = "removeCue"]
  pub fn remove_cue(cue: VTTCue);

  #[alias = "oncuechange"]
  pub oncuechange: EventHandler,
}

// Mozilla Extensions
partial trait TextTrack {
  #[chrome_only]
  #[alias = "textTrackList"]
  text_track_list: Option<TextTrackList>,
}
