// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/#dictdef-audioparamdescriptor

use super::*;

#[GenerateInit]
pub struct AudioParamDescriptor {
  required name: DOMString;
  #[alias = "defaultValue"]
  pub default_value: f32 = 0,
  #[alias = "minValue"]
  pub min_value: f32 = -3.4028235e38,
  #[alias = "maxValue"]
  pub max_value: f32 = 3.4028235e38,
  // AutomationRate for AudioWorklet is not needed until bug 1504984 is
  // implemented
  // AutomationRate automationRate = "a-rate";
}

