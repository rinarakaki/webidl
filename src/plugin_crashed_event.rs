#[chrome_only, exposed = Window]
pub trait PluginCrashedEvent: Event {
  constructor(type: DOMString,
    #[optional = {}] eventInitDict: PluginCrashedEventInit);

  pluginID: u32,
  #[alias = "pluginDumpID"]
  plugin_dump_id: DOMString,
  #[alias = "pluginName"]
  plugin_name: DOMString,
  #[alias = "pluginFilename"]
  plugin_filename: Option<DOMString>,
  #[alias = "submittedCrashReport"]
  submitted_crash_report: bool,
  #[alias = "gmpPlugin"]
  gmp_plugin: bool,
}

pub struct PluginCrashedEventInit: EventInit {
  pub pluginID: u32 = 0,
  #[alias = "pluginDumpID"]
  pub plugin_dumpID: DOMString = "",
  #[alias = "pluginName"]
  pub plugin_name: DOMString = "",
  #[alias = "pluginFilename"]
  pub plugin_filename: Option<DOMString> = None,
  #[alias = "submittedCrashReport"]
  pub submitted_crash_report: bool = false,
  #[alias = "gmpPlugin"]
  pub gmp_plugin: bool = false,
}
