// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub enum PanningModelType {
  #[alias = "equalpower"]
  EqualPower,
  "HRTF"
}

pub enum DistanceModelType {
  #[alias = "linear"]
  Linear,
  #[alias = "inverse"]
  Inverse,
  #[alias = "exponential"]
  Exponential
}

pub struct PannerOptions: AudioNodeOptions {
  #[alias = "panningModel"]
  pub panning_model: PanningModelType = "equalpower",
  #[alias = "distanceModel"]
  pub distance_model: DistanceModelType = "inverse",
  pub positionX: f32 = 0,
  pub positionY: f32 = 0,
  pub positionZ: f32 = 0,
  pub orientationX: f32 = 1,
  pub orientationY: f32 = 0,
  pub orientationZ: f32 = 0,
  #[alias = "refDistance"]
  pub ref_distance: f64 = 1,
  #[alias = "maxDistance"]
  pub max_distance: f64 = 10000,
  #[alias = "rolloffFactor"]
  pub rolloff_factor: f64 = 1,
  #[alias = "coneInnerAngle"]
  pub cone_inner_angle: f64 = 360,
  #[alias = "coneOuterAngle"]
  pub cone_outer_angle: f64 = 360,
  #[alias = "coneOuterGain"]
  pub cone_outer_gain: f64 = 0,
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait PannerNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(context: BaseAudioContext, #[optional = {}] options: PannerOptions) -> Result<Self, Error>;

  // Default for stereo is equalpower
  #[alias = "panningModel"]
  pub panning_model: PanningModelType,

  // Uses a 3D cartesian coordinate system
  #[throws]
  #[alias = "setPosition"]
  pub fn set_position(x: f64, x: f64, x: f64);
  #[throws]
  #[alias = "setOrientation"]
  pub fn set_orientation(x: f64, x: f64, x: f64);

  // Cartesian coordinate for position
  positionX: AudioParam,
  positionY: AudioParam,
  positionZ: AudioParam,

  // Cartesian coordinate for orientation
  orientationX: AudioParam,
  orientationY: AudioParam,
  orientationZ: AudioParam,

  // Distance model and attributes
  #[alias = "distanceModel"]
  pub distance_model: DistanceModelType,
  #[setter_throws]
  #[alias = "refDistance"]
  pub ref_distance: f64,
  #[setter_throws]
  #[alias = "maxDistance"]
  pub max_distance: f64,
  #[setter_throws]
  #[alias = "rolloffFactor"]
  pub rolloff_factor: f64,

  // Directional sound cone
  #[alias = "coneInnerAngle"]
  pub cone_inner_angle: f64,
  #[alias = "coneOuterAngle"]
  pub cone_outer_angle: f64,
  #[setter_throws]
  #[alias = "coneOuterGain"]
  pub cone_outer_gain: f64,

}

// Mozilla extension
PannerNode includes AudioNodePassThrough;

