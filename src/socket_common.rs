// The origin of this IDL file is
// http://www.w3.org/2012/sysapps/tcp-udp-sockets/#readystate

pub enum SocketReadyState {
  #[alias = "opening"]
  Opening,
  #[alias = "open"]
  Open,
  #[alias = "closing"]
  Closing,
  #[alias = "closed"]
  Closed,
  #[alias = "halfclosed"]
  HalfClosed
}
