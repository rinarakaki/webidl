// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGPreserveAspectRatio {
  // Alignment Types
  const u16 SVG_PRESERVEASPECTRATIO_UNKNOWN = 0;
  const u16 SVG_PRESERVEASPECTRATIO_NONE = 1;
  const u16 SVG_PRESERVEASPECTRATIO_XMINYMIN = 2;
  const u16 SVG_PRESERVEASPECTRATIO_XMIDYMIN = 3;
  const u16 SVG_PRESERVEASPECTRATIO_XMAXYMIN = 4;
  const u16 SVG_PRESERVEASPECTRATIO_XMINYMID = 5;
  const u16 SVG_PRESERVEASPECTRATIO_XMIDYMID = 6;
  const u16 SVG_PRESERVEASPECTRATIO_XMAXYMID = 7;
  const u16 SVG_PRESERVEASPECTRATIO_XMINYMAX = 8;
  const u16 SVG_PRESERVEASPECTRATIO_XMIDYMAX = 9;
  const u16 SVG_PRESERVEASPECTRATIO_XMAXYMAX = 10;

  // Meet-or-slice Types
  const u16 SVG_MEETORSLICE_UNKNOWN = 0;
  const u16 SVG_MEETORSLICE_MEET = 1;
  const u16 SVG_MEETORSLICE_SLICE = 2;

  #[setter_throws]
  pub align: u16,
  #[setter_throws]
  #[alias = "meetOrSlice"]
  pub meet_or_slice: u16,
}

