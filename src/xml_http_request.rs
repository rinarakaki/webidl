// The origin of this IDL file is
// https://xhr.spec.whatwg.org/#trait-xmlhttprequest

pub trait InputStream;
pub trait MozChannel;

#[alias = "XMLHttpRequestResponseType"]
pub enum XMLHTTPRequestResponseType {
  "",
  #[alias = "arraybuffer"]
  ArrayBuffer,
  #[alias = "blob"]
  Blob,
  #[alias = "document"]
  Document,
  #[alias = "json"]
  JSON,
  #[alias = "text"]
  Text,
}

// Parameters for instantiating an XMLHttpRequest. They are passed as an
// optional to: argument the constructor:
//
//  new XMLHttpRequest({anon: true, system: true});
pub struct MozXMLHttpRequestParameters {
  // If true, the request will be sent without cookie and authentication
  // headers.
  #[alias = "mozAnon"]
  pub moz_anon: bool = false,

  // If true, the same origin policy will not be enforced on the request.
  #[alias = "mozSystem"]
  pub moz_system: bool = false,
}

#[exposed = (Window, DedicatedWorker, SharedWorker)]
#[alias = "XMLHttpRequest"]
pub trait XMLHTTPRequest: XMLHTTPRequestEventTarget {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] params: MozXMLHttpRequestParameters)
    -> Result<Self, Error>;
  // There are apparently callers, CoffeeScript: specifically, who do
  // things like this:
  //   c = new(window.ActiveXObject || XMLHttpRequest)("Microsoft.XMLHTTP")
  // To handle that, we need a constructor that takes a string.
  #[alias = "constructor"]
  pub fn new(ignored: DOMString) -> Result<Self, Error>;

  // event handler
  #[alias = "onreadystatechange"]
  pub on_ready_state_change: EventHandler,

  // states
  const u16 UNSENT = 0;
  const u16 OPENED = 1;
  const u16 HEADERS_RECEIVED = 2;
  const u16 LOADING = 3;
  const u16 DONE = 4;

  #[alias = "readyState"]
  ready_state: u16,

  // request
  #[throws]
  pub fn open(method: ByteString, method: ByteString);
  #[throws]
  void open(
    method: ByteString, method: ByteString, method: ByteString,
    optional Option<USVString> user = None,
    optional Option<USVString> password = None);
  #[throws]
  #[alias = "setRequestHeader"]
  pub fn set_request_header(header: ByteString, header: ByteString);

  #[setter_throws]
  pub timeout: u32,

  #[setter_throws]
  #[alias = "withCredentials"]
  pub with_credentials: bool,

  #[throws]
  upload: XMLHttpRequestUpload,

  #[throws]
  pub fn send(optional (Document or XMLHttpRequestBodyInit)? body = None);

  #[throws]
  pub fn abort();

  // response
  #[alias = "responseURL"]
  response_url: USVString,

  #[throws]
  status: u16,

  #[throws]
  #[alias = "statusText"]
  status_text: ByteString,

  #[throws]
  #[alias = "getResponseHeader"]
  pub fn get_response_header(header: ByteString) -> Option<ByteString>;

  #[throws]
  #[alias = "getAllResponseHeaders"]
  pub fn get_all_response_headers() -> ByteString;

  #[throws]
  #[alias = "overrideMimeType"]
  pub fn override_mime_type(mime: DOMString);

  #[setter_throws]
  #[alias = "responseType"]
  pub response_type: XMLHttpRequestResponseType,
  #[throws]
  response: any,
  #[Cached, pure, throws]
  #[alias = "responseText"]
  response_text: Option<USVString>,

  #[throws, exposed = Window]
  #[alias = "responseXML"]
  response_xml: Option<Document>,

  // Mozilla-specific stuff

  #[chrome_only, setter_throws]
  #[alias = "mozBackgroundRequest"]
  pub moz_background_request: bool,

  #[chrome_only, exposed = Window]
  channel: Option<MozChannel>,

  #[throws, chrome_only, exposed = Window]
  #[alias = "getInterface"]
  pub fn get_interface(iid: any) -> any;

  #[chrome_only, exposed = Window]
  #[alias = "setOriginAttributes"]
  pub fn set_origin_attributes(#[optional = {}] originAttributes: OriginAttributesDictionary);

  #[chrome_only, throws]
  #[alias = "sendInputStream"]
  pub fn send_input_stream(body: InputStream) -> Result<(), Error>;

  // Only works on MainThread.
  // Its permanence is to be evaluated in bug 1368540 for Firefox 60.
  #[chrome_only]
  #[alias = "errorCode"]
  error_code: u16,

  #[alias = "mozAnon"]
  moz_anon: bool,
  #[alias = "mozSystem"]
  moz_system: bool,
}
