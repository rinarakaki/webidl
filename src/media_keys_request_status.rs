pub enum MediaKeySystemStatus {
  #[alias = "available"]
  Available,
  #[alias = "api-disabled"]
  APIDisabled,
  #[alias = "cdm-disabled"]
  CDMDisabled,
  #[alias = "cdm-not-supported"]
  CDMNotSupported,
  #[alias = "cdm-not-installed"]
  CDMNotInstalled,
  #[alias = "cdm-created"]
  CDMCreated,
}

// Note: This pub struct and enum is only used by Gecko to convey messages
// to chrome JS code. It is not exposed to the web.
#[GenerateToJSON]
pub struct RequestMediaKeySystemAccessNotification {
  required DOMString keySystem;
  required MediaKeySystemStatus status;
}
