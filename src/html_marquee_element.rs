// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/obsolete.html#the-marquee-element

// https://html.spec.whatwg.org/multipage/obsolete.html#the-marquee-element

#[exposed = Window]
pub trait HTMLMarqueeElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub behavior: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "bgColor"]
  pub bg_color: DOMString,
  #[ce_reactions, setter_throws]
  pub direction: DOMString,
  #[ce_reactions, setter_throws]
  pub height: DOMString,
  #[ce_reactions, setter_throws]
  pub hspace: u32,
  #[ce_reactions, setter_throws]
  pub loop: i32,
  #[ce_reactions, setter_throws]
  #[alias = "scrollAmount"]
  pub scroll_amount: u32,
  #[ce_reactions, setter_throws]
  #[alias = "scrollDelay"]
  pub scroll_delay: u32,
  #[ce_reactions, setter_throws]
  #[alias = "trueSpeed"]
  pub true_speed: bool,
  #[ce_reactions, setter_throws]
  pub vspace: u32,
  #[ce_reactions, setter_throws]
  pub width: DOMString,

  #[alias = "onbounce"]
  pub onbounce: EventHandler,
  #[alias = "onfinish"]
  pub on_finish: EventHandler,
  #[alias = "onstart"]
  pub onstart: EventHandler,

  pub fn start();
  pub fn stop();
}

