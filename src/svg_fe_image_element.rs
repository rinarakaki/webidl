// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEImageElement: SVGElement {
  #[constant]
  #[alias = "preserveAspectRatio"]
  preserve_aspect_ratio: SVGAnimatedPreserveAspectRatio,
}

SVGFEImageElement includes SVGFilterPrimitiveStandardAttributes;
SVGFEImageElement includes SVGURIReference;
