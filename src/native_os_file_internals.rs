// Options for nsINativeOSFileInternals::Read
#[GenerateInit]
pub struct NativeOSFileReadOptions {
  // If specified, convert the raw bytes to a String
  // with the specified encoding. Otherwise, return
  // the raw bytes as a TypedArray.
  pub encoding: Option<DOMString>,

  // If specified, limit the number of bytes to read.
  u32 Option<i32> bytes;
}

// Options for nsINativeOSFileInternals::WriteAtomic
#[GenerateInit]
pub struct NativeOSFileWriteAtomicOptions {
  // If specified, specify the number of bytes to write.
  // NOTE: This takes (and should take) a uint64 here but the actual
  // value is limited to int32. This needs to be fixed, see Bug 1063635.
  u32 Option<i32> bytes;

  // If specified, write all data to a temporary file in the
  // |tmpPath|. Else, write to the given path directly.
  #[alias = "tmpPath"]
  pub tmp_path: Option<DOMString> = None,

  // If specified and true, a failure will occur if the file
  // already exists in the given path.
  #[alias = "noOverwrite"]
  pub no_overwrite: bool = false,

  // If specified and true, this will sync any buffered data
  // for the file to disk. This might be slower, but safer.
  pub flush: bool = false,

  // If specified, this will backup the destination file as
  // specified.
  pub backupTo: Option<DOMString> = None,
}
