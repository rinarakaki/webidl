// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

pub trait Principal;
pub trait URI;

#[exposed = Window]
pub trait Node: EventTarget {
  const u16 ELEMENT_NODE = 1;
  const u16 ATTRIBUTE_NODE = 2; // historical
  const u16 TEXT_NODE = 3;
  const u16 CDATA_SECTION_NODE = 4; // historical
  const u16 ENTITY_REFERENCE_NODE = 5; // historical
  const u16 ENTITY_NODE = 6; // historical
  const u16 PROCESSING_INSTRUCTION_NODE = 7;
  const u16 COMMENT_NODE = 8;
  const u16 DOCUMENT_NODE = 9;
  const u16 DOCUMENT_TYPE_NODE = 10;
  const u16 DOCUMENT_FRAGMENT_NODE = 11;
  const u16 NOTATION_NODE = 12; // historical
  #[constant]
  #[alias = "nodeType"]
  node_type: u16,
  #[pure]
  #[alias = "nodeName"]
  node_name: DOMString,

  #[pure, throws, needs_caller_type, binary_name = "baseURIFromJS"]
  #[alias = "baseURI"]
  base_uri: Option<DOMString>,

  #[pure, binary_name = isInComposedDoc]
  #[alias = "isConnected"]
  is_connected: bool,
  #[pure]
  #[alias = "ownerDocument"]
  owner_document: Option<Document>,
  #[pure]
  #[alias = "getRootNode"]
  pub fn get_root_node(#[optional = {}] options: GetRootNodeOptions) -> Node;
  #[pure]
  #[alias = "parentNode"]
  parent_node: Option<Node>,
  #[pure]
  #[alias = "parentElement"]
  parent_element: Option<Element>,
  #[pure]
  #[alias = "hasChildNodes"]
  pub fn has_child_nodes() -> bool;
  #[same_object]
  #[alias = "childNodes"]
  child_nodes: NodeList,
  #[pure]
  #[alias = "firstChild"]
  first_child: Option<Node>,
  #[pure]
  #[alias = "lastChild"]
  last_child: Option<Node>,
  #[pure]
  #[alias = "previousSibling"]
  previous_sibling: Option<Node>,
  #[pure]
  #[alias = "nextSibling"]
  next_sibling: Option<Node>,

  #[ce_reactions, setter_throws, pure]
  pub nodeValue: Option<DOMString>,
  #[ce_reactions, setter_throws, GetterCanOOM,
  Setterneeds_subject_principal = NonSystem, pure]
  pub textContent: Option<DOMString>,
  // These DOM methods cannot be accessed by UA Widget scripts
  // because the DOM element reflectors will be in the content scope,
  // instead of the desired UA Widget scope.
  #[ce_reactions, func = "IsNotUAWidget"]
  #[alias = "insertBefore"]
  pub fn insert_before(node: Node, node: Node) -> Result<Node, Error>;
  
  #[ce_reactions, throws, func = "IsNotUAWidget"]
  #[alias = "appendChild"]
  pub fn append_child(node: Node) -> Node;

  #[ce_reactions, throws, func = "IsNotUAWidget"]
  #[alias = "replaceChild"]
  pub fn replace_child(node: Node, node: Node) -> Node;

  #[ce_reactions, throws]
  #[alias = "removeChild"]
  pub fn remove_child(child: Node) -> Node;

  #[ce_reactions]
  pub fn normalize();

  #[ce_reactions, throws, func = "IsNotUAWidget"]
  #[alias = "cloneNode"]
  pub fn clone_node(#[optional = false] deep: bool) -> Node;
  #[pure]
  #[alias = "isSameNode"]
  pub fn is_same_node(node: Option<Node>) -> bool;
  #[pure]
  #[alias = "isEqualNode"]
  pub fn is_equal_node(node: Option<Node>) -> bool;

  const u16 DOCUMENT_POSITION_DISCONNECTED = 0x01;
  const u16 DOCUMENT_POSITION_PRECEDING = 0x02;
  const u16 DOCUMENT_POSITION_FOLLOWING = 0x04;
  const u16 DOCUMENT_POSITION_CONTAINS = 0x08;
  const u16 DOCUMENT_POSITION_CONTAINED_BY = 0x10;
  const u16 DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 0x20; // historical

  #[pure]
  u16 compareDocumentPosition(other: Node);

  #[pure]
  pub fn contains(other: Option<Node>) -> bool;

  #[pure]
  #[alias = "lookupPrefix"]
  pub fn lookup_prefix(namespace: Option<DOMString>) -> Option<DOMString>;
  #[pure]
  #[alias = "lookupNamespaceURI"]
  pub fn lookup_namespace_uri(prefix: Option<DOMString>) -> Option<DOMString>;
  #[pure]
  #[alias = "isDefaultNamespace"]
  pub fn is_default_namespace(namespace: Option<DOMString>) -> bool;

  // Mozilla-specific stuff
  #[chrome_only]
  #[alias = "nodePrincipal"]
  node_principal: Principal,
  #[chrome_only]
  #[alias = "baseURIObject"]
  base_uri_object: Option<URI>,
  #[chrome_only]
  #[alias = "generateXPath"]
  pub fn generate_xpath() -> DOMString;
  #[chrome_only, pure, binary_name = "flattenedTreeParentNodeNonInline"]
  #[alias = "flattenedTreeParentNode"]
  flattened_tree_parent_node: Option<Node>,
  #[chrome_only, pure, binary_name = "isInNativeAnonymousSubtree"]
  #[alias = "isNativeAnonymous"]
  is_native_anonymous: bool,

  // Maybe this would be useful to Option<authors> https://github.com/whatwg/dom/issues/826
  #[func = "IsChromeOrUAWidget", pure, binary_name = "containingShadow"]
  #[alias = "containingShadowRoot"]
  containing_shadow_root: Option<ShadowRoot>,

  // Mozilla devtools-specific stuff
  // If this element is a flex item (or has one or more anonymous box ancestors
  // that chain up to an anonymous flex item), then this method returns the
  // flex container that the flex item participates in. Otherwise, this method
  // returns None.
  #[chrome_only]
  #[alias = "parentFlexElement"]
  parent_flex_element: Option<Element>,

  #[cfg(ACCESSIBILITY)]
  #[func = "mozilla::dom::AccessibleNode::IsAOMEnabled", same_object]
  #[alias = "accessibleNode"]
  accessible_node: Option<AccessibleNode>,
}

pub struct GetRootNodeOptions {
  pub composed: bool = false,
}
