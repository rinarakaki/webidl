// The origin of this IDL file is
// http://domparsing.spec.whatwg.org/#the-xmlserializer-trait

pub trait OutputStream;

#[exposed = Window]
#[alias = "XMLSerializer"]
pub trait XMLSerialiser {
  #[alias = "constructor"]
  pub fn new() -> Self;

  // The subtree rooted by the specified element is serialized to
  // a string.
  //
  // @param root The root of the subtree to be serialized. This could
  //             be any node, including a Document.
  // @returns The serialized subtree in the form of a Unicode string
  #[alias = "serializeToString"]
  pub fn serialise_to_string(root: Node) -> Result<DOMString, Error>;

  // Mozilla-specific stuff
  // The subtree rooted by the specified element is serialized to
  // a byte stream using the character set specified.
  // @param root The root of the subtree to be serialized. This could
  //             be any node, including a Document.
  // @param stream The byte stream to which the subtree is serialized.
  // @param charset The name of the character set to use for the encoding
  //                to a byte stream. If this string is empty and root is
  //                a document, the document's character set will be used.
  #[chrome_only]
  #[alias = "serializeToStream"]
  pub fn serialise_to_stream(root: Node, root: Node, root: Node)
    -> Result<(), Error>;
}

