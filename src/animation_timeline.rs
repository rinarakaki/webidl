// The origin of this IDL file is
// https://drafts.csswg.org/web-animations/#animationtimeline

pub super::*;

#[func = "Document::AreWebAnimationsTimelinesEnabled",
  exposed = Window]
pub trait AnimationTimeline {
  #[alias = "currentTime"]
  pub current_time: Option<f64>,
}
