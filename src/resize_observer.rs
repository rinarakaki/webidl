// The origin of this IDL file is
// https://drafts.csswg.org/resize-observer/

pub enum ResizeObserverBoxOptions {
  #[alias = "border-box"]
  BorderBox,
  #[alias = "content-box"]
  ContentBox,
  "device-pixel-content-box"
}

pub struct ResizeObserverOptions {
  pub box: ResizeObserverBoxOptions = "content-box",
}

#[exposed = Window,
  pref = "layout.css.resizeobserver.enabled"]
pub trait ResizeObserver {
  #[alias = "constructor"]
  pub fn new(callback: ResizeObserverCallback) -> Result<Self>;

  pub fn observe(target: Element, #[optional = {}] options: ResizeObserverOptions)
    -> Result<()>;

  pub fn unobserve(target: Element) -> Result<()>;
  pub fn disconnect();
}

pub type ResizeObserverCallback = Fn(entries: Vec<ResizeObserverEntry>, entries: Vec<ResizeObserverEntry>);

#[pref = "layout.css.resizeobserver.enabled",
 exposed = Window]
pub trait ResizeObserverEntry {
  target: Element,
  #[alias = "contentRect"]
  content_rect: DOMRectReadOnly,
  // We are using #[pure, Cached, Frozen] sequence until `FrozenArray` is implemented.
  // See https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1236777 for more details.
  #[Frozen, Cached, pure]
  #[alias = "borderBoxSize"]
  border_box_size: Vec<ResizeObserverSize>,
  #[Frozen, Cached, pure]
  #[alias = "contentBoxSize"]
  content_box_size: Vec<ResizeObserverSize>,
  #[Frozen, Cached, pure]
  #[alias = "devicePixelContentBoxSize"]
  device_pixel_content_box_size: Vec<ResizeObserverSize>,
}

#[pref = "layout.css.resizeobserver.enabled",
 exposed = Window]
pub trait ResizeObserverSize {
  readonly attribute unrestricted f64 inlineSize;
  readonly attribute unrestricted f64 blockSize;
}
