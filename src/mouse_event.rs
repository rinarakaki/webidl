// For more information on this trait please see
// http://dev.w3.org/2006/webapi/DOM-Level-3-Events/html/DOM3-Events.html
// https://drafts.csswg.org/cssom-view/#extensions-to-the-mouseevent-trait

#[exposed = Window]
pub trait MouseEvent: UIEvent {
  constructor(typeArg: DOMString,
  #[optional = {}] mouseEventInitDict: MouseEventInit);

  #[needs_caller_type]
  screenX: i32,
  #[needs_caller_type]
  screenY: i32,
  pageX: i32,
  pageY: i32,
  clientX: i32,
  clientY: i32,
  #[binary_name = "clientX"]
  x: i32,
  #[binary_name = "clientY"]
  y: i32,
  offsetX: i32,
  offsetY: i32,
  #[alias = "ctrlKey"]
  ctrl_key: bool,
  #[alias = "shiftKey"]
  shift_key: bool,
  #[alias = "altKey"]
  alt_key: bool,
  #[alias = "metaKey"]
  meta_key: bool,
  button: short,
  buttons: u16,
  #[alias = "relatedTarget"]
  related_target: Option<EventTarget>,
  region: Option<DOMString>,

  // Pointer Lock
  movementX: i32,
  movementY: i32,

  // deprecated in DOM Level 3:
void initMouseEvent(typeArg: DOMString,
  #[optional = false] canBubbleArg: bool,
  #[optional = false] cancelableArg: bool,
  optional Option<Window> viewArg = None,
  #[optional = 0] detailArg: i32,
  #[optional = 0] screenXArg: i32,
  #[optional = 0] screenYArg: i32,
  #[optional = 0] clientXArg: i32,
  #[optional = 0] clientYArg: i32,
  #[optional = false] ctrlKeyArg: bool,
  #[optional = false] altKeyArg: bool,
  #[optional = false] shiftKeyArg: bool,
  #[optional = false] metaKeyArg: bool,
  #[optional = 0] buttonArg: short,
  optional Option<EventTarget> relatedTargetArg = None);
  // Introduced in DOM Level 3:
  #[alias = "getModifierState"]
  pub fn get_modifier_state(keyArg: DOMString) -> bool;
}

// Suggested initMouseEvent replacement initializer:
pub struct MouseEventInit: EventModifierInit {
  // Attributes for MouseEvent:
  pub screenX: i32 = 0,
  pub screenY: i32 = 0,
  pub clientX: i32 = 0,
  pub clientY: i32 = 0,
  pub button: short = 0,
  // Note: "buttons" was not previously initializable through initMouseEvent!
  pub buttons: u16 = 0,
  #[alias = "relatedTarget"]
  pub related_target: Option<EventTarget> = None,

  // Pointer Lock
  pub movementX: i32 = 0,
  pub movementY: i32 = 0,
}

// Mozilla extensions
partial trait MouseEvent
{
  // Finger or touch pressure event value
  // ranges between 0.0 and 1.0
  #[deprecated = "MouseEvent_MozPressure"]
  #[alias = "mozPressure"]
  moz_pressure: f32,

  const u16 MOZ_SOURCE_UNKNOWN = 0;
  const u16 MOZ_SOURCE_MOUSE = 1;
  const u16 MOZ_SOURCE_PEN = 2;
  const u16 MOZ_SOURCE_ERASER = 3;
  const u16 MOZ_SOURCE_CURSOR = 4;
  const u16 MOZ_SOURCE_TOUCH = 5;
  const u16 MOZ_SOURCE_KEYBOARD = 6;

  #[alias = "mozInputSource"]
  moz_input_source: u16,

  void initNSMouseEvent(typeArg: DOMString,
  #[optional = false] canBubbleArg: bool,
  #[optional = false] cancelableArg: bool,
  optional Option<Window> viewArg = None,
  #[optional = 0] detailArg: i32,
  #[optional = 0] screenXArg: i32,
  #[optional = 0] screenYArg: i32,
  #[optional = 0] clientXArg: i32,
  #[optional = 0] clientYArg: i32,
  #[optional = false] ctrlKeyArg: bool,
  #[optional = false] altKeyArg: bool,
  #[optional = false] shiftKeyArg: bool,
  #[optional = false] metaKeyArg: bool,
  #[optional = 0] buttonArg: short,
  optional Option<EventTarget> relatedTargetArg = None,
  #[optional = 0] pressure: f32,
  #[optional = 0] inputSourceArg: u16);

  // preventClickEvent() prevents the following "click", "auxclick" and
  // "dblclick" events of "mousedown" and "mouseup" events.
  #[chrome_only]
  #[alias = "preventClickEvent"]
  pub fn prevent_click_event();

  // Returns true if the following "click", "auxclick" and "dblclick"
  // events of "mousedown" and "mouseup" events are prevented.
  #[chrome_only]
  #[alias = "clickEventPrevented"]
  pub fn click_event_prevented() -> bool;
}

