// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/comms.html#the-eventsource-trait

#[exposed = (Window,DedicatedWorker, SharedWorker)]
pub trait EventSource: EventTarget {
  #[alias = "constructor"]
  pub fn new(url: USVString, #[optional = {}] eventSourceInitDict: EventSourceInit) -> Result<Self, Error>;

  #[constant]
  url: DOMString,
  #[constant]
  #[alias = "withCredentials"]
  with_credentials: bool,

  // ready state
  const u16 CONNECTING = 0;
  const u16 OPEN = 1;
  const u16 CLOSED = 2;
  #[alias = "readyState"]
  ready_state: u16,

  // networking
  #[alias = "onopen"]
  pub onopen: EventHandler,
  #[alias = "onmessage"]
  pub on_message: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  pub fn close();
}

pub struct EventSourceInit {
  #[alias = "withCredentials"]
  pub with_credentials: bool = false,
}
