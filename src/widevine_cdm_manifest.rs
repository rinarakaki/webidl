#[GenerateInitFromJSON]
pub struct WidevineCDMManifest {
  required DOMString name;
  required DOMString description;
  required DOMString version;
  required DOMString x-cdm-module-versions;
  required DOMString x-cdm-trait-versions;
  required DOMString x-cdm-host-versions;
  required DOMString x-cdm-codecs;
}
