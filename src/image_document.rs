#[chrome_only, LegacyOverrideBuiltIns,
 exposed = Window]
pub trait ImageDocument: HTMLDocument {
  // Whether the image is overflowing visible area. readonly attribute bool imageIsOverflowing;

  // Whether the image has been resized to fit visible area. readonly attribute bool imageIsResized;

  // Resize the image to fit visible area. void shrinkToFit();

  // Restore image original size. void restoreImage();
}
