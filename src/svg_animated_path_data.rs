// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

pub trait mixin SVGAnimatedPathData {
  #[pref = "dom.svg.pathSeg.enabled"]
  #[alias = "pathSegList"]
  path_seg_list: SVGPathSegList,
  #[pref = "dom.svg.pathSeg.enabled"]
  #[alias = "animatedPathSegList"]
  animated_path_seg_list: SVGPathSegList,
}

