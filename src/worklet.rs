// The origin of this IDL file is
// https://drafts.css-houdini.org/worklets/#idl-index

#[SecureContext, pref = "dom.worklet.enabled", exposed = Window]
pub trait Worklet {
  #[new_object, needs_caller_type]
  #[alias = "addModule"]
  pub fn add_module(
    module_url: USVString,
    #[optional = {}] options: WorkletOptions)
    -> Result<Promise<void>>;
}

pub struct WorkletOptions {
  pub credentials: RequestCredentials = "same-origin",
}
