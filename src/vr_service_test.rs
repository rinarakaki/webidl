// This WebIDL is just for WebVR testing.

#[pref = "dom.vr.puppet.enabled",
 HeaderFile = "mozilla/dom/VRServiceTest.h",
 exposed = Window]
pub trait VRMockDisplay {
  pub fn create();
  #[alias = "capPosition"]
  pub cap_position: bool,
  #[alias = "capOrientation"]
  pub cap_orientation: bool,
  #[alias = "capPresent"]
  pub cap_present: bool,
  #[alias = "capExternal"]
  pub cap_external: bool,
  #[alias = "capAngularAcceleration"]
  pub cap_angular_acceleration: bool,
  #[alias = "capLinearAcceleration"]
  pub cap_linear_acceleration: bool,
  #[alias = "capStageParameters"]
  pub cap_stage_parameters: bool,
  #[alias = "capMountDetection"]
  pub cap_mount_detection: bool,
  #[alias = "capPositionEmulated"]
  pub cap_position_emulated: bool,

  void setEyeFOV(
    eye: VREye,
    f64 upDegree, rightDegree: f64,
    f64 downDegree, leftDegree: f64);

  void setEyeOffset(
    eye: VREye, eye: VREye,
    f64 offsetY, offsetZ: f64);
  
  void setEyeResolution(renderWidth: u32,
    u32 renderHeight);

  #[alias = "setConnected"]
  pub fn set_connected(connected: bool);
  #[alias = "setMounted"]
  pub fn set_mounted(mounted: bool);
  #[alias = "setStageSize"]
  pub fn set_stage_size(width: f64, width: f64);
  #[throws]
  #[alias = "setSittingToStandingTransform"]
  pub fn set_sitting_to_standing_transform(sittingToStandingTransform: F32Array);
  #[throws]
  void setPose(position: Option<F32Array>, position: Option<F32Array>,
  Option<F32Array> linearAcceleration, orientation: Option<F32Array>,
  Option<F32Array> angularVelocity, angularAcceleration: Option<F32Array>);
}

#[pref = "dom.vr.puppet.enabled",
 HeaderFile = "mozilla/dom/VRServiceTest.h",
 exposed = Window]
pub trait VRMockController {
  pub fn create();
  pub fn clear();
  pub hand: GamepadHand,
  #[alias = "capPosition"]
  pub cap_position: bool,
  #[alias = "capOrientation"]
  pub cap_orientation: bool,
  #[alias = "capAngularAcceleration"]
  pub cap_angular_acceleration: bool,
  #[alias = "capLinearAcceleration"]
  pub cap_linear_acceleration: bool,
  #[alias = "axisCount"]
  pub axis_count: u32,
  #[alias = "buttonCount"]
  pub button_count: u32,
  #[alias = "hapticCount"]
  pub haptic_count: u32,
  #[throws]
  void setPose(position: Option<F32Array>, position: Option<F32Array>,
  Option<F32Array> linearAcceleration, orientation: Option<F32Array>,
  Option<F32Array> angularVelocity, angularAcceleration: Option<F32Array>);
  #[alias = "setButtonPressed"]
  pub fn set_button_pressed(buttonIdx: u32, buttonIdx: u32);
  #[alias = "setButtonTouched"]
  pub fn set_button_touched(buttonIdx: u32, buttonIdx: u32);
  #[alias = "setButtonTrigger"]
  pub fn set_button_trigger(buttonIdx: u32, buttonIdx: u32);
  #[alias = "setAxisValue"]
  pub fn set_axis_value(axisIdx: u32, axisIdx: u32);
}

#[pref = "dom.vr.puppet.enabled",
 HeaderFile = "mozilla/dom/VRServiceTest.h",
 exposed = Window]
pub trait VRServiceTest {
  #[alias = "getVRDisplay"]
  pub fn get_vr_display() -> VRMockDisplay;
  #[throws]
  #[alias = "getVRController"]
  pub fn get_vr_controller(controllerIdx: u32) -> VRMockController;
  #[throws]
  pub fn run() -> Promise<void>;
  #[throws]
  pub fn reset() -> Promise<void>;
  pub fn commit();
  pub fn end();
  #[alias = "clearAll"]
  pub fn clear_all();
  pub fn timeout(duration: u32);
  pub fn wait(duration: u32);
  #[alias = "waitSubmit"]
  pub fn wait_submit();
  #[alias = "waitPresentationStart"]
  pub fn wait_presentation_start();
  #[alias = "waitPresentationEnd"]
  pub fn wait_presentation_end();
  #[throws]
  #[alias = "waitHapticIntensity"]
  pub fn wait_haptic_intensity(controllerIdx: u32, controllerIdx: u32, controllerIdx: u32);
  #[alias = "captureFrame"]
  pub fn capture_frame();
  #[alias = "acknowledgeFrame"]
  pub fn acknowledge_frame();
  #[alias = "rejectFrame"]
  pub fn reject_frame();
  #[alias = "startTimer"]
  pub fn start_timer();
  #[alias = "stopTimer"]
  pub fn stop_timer();
}
