pub super::*;

#[func = "mozilla::dom::AccessibleNode::IsAOMEnabled",
 exposed = Window]
pub trait AccessibleNode {
    #[alias = "computedRole"]
    pub computed_role: DOMString,
    #[Frozen, Cached, pure]
    pub states: Vec<DOMString>,
    #[Frozen, Cached, pure]
    pub attributes: Vec<DOMString>,
    #[alias = "DOMNode"]
    pub dom_node: Option<Node>,

    pub fn is(&self, DOMString... states) -> bool;
    pub fn has(&self, DOMString... attributes) -> bool;
    pub fn get(&self, attribute: DOMString) -> Result<any>;

    pub mut role: Option<DOMString>,
    #[alias = "roleDescription"]
    pub mut role_description: Option<DOMString>,

    // Accessible label and descriptor
    pub mut label: Option<DOMString>,

    // Global states and properties
    pub mut current: Option<DOMString>,

    // Accessible properties
    pub mut autocomplete: Option<DOMString>,
    #[alias = "keyShortcuts"]
    pub mut key_shortcuts: Option<DOMString>,
    pub mut modal: Option<bool>,
    pub mut multiline: Option<bool>,
    pub mut multiselectable: Option<bool>,
    pub mut orientation: Option<DOMString>,
    #[alias = "readOnly"]
    pub mut read_only: Option<bool>,
    pub mut required: Option<bool>,
    pub mut sort: Option<DOMString>,

    // Range values
    pub mut placeholder: Option<DOMString>,
    #[alias = "valueMax"]
    pub mut value_max: Option<f64>,
    #[alias = "valueMin"]
    pub mut value_min: Option<f64>,
    #[alias = "valueNow"]
    pub mut value_now: Option<f64>,
    #[alias = "valueText"]
    pub mut value_text: Option<DOMString>,

    // Accessible states
    pub mut checked: Option<DOMString>,
    pub mut disabled: Option<bool>,
    pub mut expanded: Option<bool>,
    #[alias = "hasPopUp"]
    pub mut has_pop_up: Option<DOMString>,
    pub mut hidden: Option<bool>,
    pub mut invalid: Option<DOMString>,
    pub mut pressed: Option<DOMString>,
    pub mut selected: Option<bool>,

    // Live regions
    pub mut atomic: Option<bool>,
    pub mut busy: Option<bool>,
    pub mut live: Option<DOMString>,
    pub mut relevant: Option<DOMString>,

    // Other relationships
    #[alias = "activeDescendant"]
    pub mut active_descendant: Option<AccessibleNode>,
    pub mut details: Option<AccessibleNode>,
    #[alias = "errorMessage"]
    pub mut error_message: Option<AccessibleNode>,

    // Collections.
    #[alias = "colCount"]
    pub mut col_count: Option<i32>,
    #[alias = "colIndex"]
    pub mut col_index: Option<u32>,
    #[alias = "colSpan"]
    pub mut col_span: Option<u32>,
    pub mut level: Option<u32>,
    #[alias = "posInSet"]
    pub mut pos_in_set: Option<u32>,
    #[alias = "rowCount"]
    pub mut row_count: Option<i32>,
    #[alias = "rowIndex"]
    pub mut row_index: Option<u32>,
    #[alias = "rowSpan"]
    pub mut row_span: Option<u32>,
    #[alias = "setSize"]
    pub mut set_size: Option<i32>,
}
