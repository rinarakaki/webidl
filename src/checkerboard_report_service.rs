// This file declares data structures used to communicate checkerboard reports
// from C++ code to about:checkerboard (see bug 1238042). These dictionaries
// are NOT exposed to standard web content.

use super::*;

pub enum CheckerboardReason {
  #[alias = "severe"]
  Severe,
  #[alias = "recent"]
  Recent
}

// Individual checkerboard report. Contains fields for the severity of the
// checkerboard event, the timestamp at which it was reported, the detailed
// log of the event, and the reason this report was saved (currently either
// "severe" or "recent").
pub struct CheckerboardReport {
  pub #[optional] severity: u32,
  pub #[optional] timestamp: DOMTimeStamp,  // Milliseconds since epoch
  pub #[optional] log: DOMString,
  pub #[optional] reason: CheckerboardReason,
}

// The guard function only allows creation of this trait on the
// about:checkerboard page, and only if it's in the parent process.
#[func = "mozilla::dom::CheckerboardReportService::IsEnabled",
  exposed = Window]
pub trait CheckerboardReportService {
  #[alias = "constructor"]
  pub fn new() -> Self;

  // Gets the available checkerboard reports.
  #[alias = "getReports"]
  pub fn get_reports(&self) -> Vec<CheckerboardReport>;

  // Gets the state of the apz.record_checkerboarding pref.
  #[alias = "isRecordingEnabled"]
  pub fn is_recording_enabled(&self) -> bool;

  // Sets the state of the apz.record_checkerboarding pref.
  #[alias = "setRecordingEnabled"]
  pub fn set_recording_enabled(&self, a_enabled: bool);

  // Flush any in-progress checkerboard reports. Since this happens
  // asynchronously, the caller may register an observer with the observer
  // service to be notified when this operation is complete. The observer should
  // listen for the topic "APZ:FlushActiveCheckerboard:Done". Upon receiving
  // this notification, the caller may call getReports() to obtain the flushed
  // reports, along with any other reports that are available.
  #[alias = "flushActiveReports"]
  pub fn flush_active_reports(&self);
}
