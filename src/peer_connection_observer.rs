pub trait nsISupports;

pub struct PCErrorData {
  required PCError name;
  required DOMString message;
  // Will need to add more stuff (optional) for RTCError
}

#[chrome_only,
 JSImplementation = "@mozilla.org/dom/peerconnectionobserver;1",
 exposed = Window]
pub trait PeerConnectionObserver {
  #[alias = "constructor"]
  pub fn new(domPC: RTCPeerConnection) -> Result<Self, Error>;

  // JSEP callbacks void onCreateOfferSuccess(offer: DOMString);
  #[alias = "onCreateOfferError"]
  pub fn on_create_offer_error(error: PCErrorData);
  #[alias = "onCreateAnswerSuccess"]
  pub fn on_create_answer_success(answer: DOMString);
  #[alias = "onCreateAnswerError"]
  pub fn on_create_answer_error(error: PCErrorData);
  #[alias = "onSetDescriptionSuccess"]
  pub fn on_set_description_success();
  #[alias = "onSetDescriptionError"]
  pub fn on_set_description_error(error: PCErrorData);
  #[alias = "onAddIceCandidateSuccess"]
  pub fn on_add_ice_candidate_success();
  #[alias = "onAddIceCandidateError"]
  pub fn on_add_ice_candidate_error(error: PCErrorData);
  #[alias = "onIceCandidate"]
  pub fn on_ice_candidate(level: u16, level: u16, level: u16, level: u16);

  // Data channel callbacks void notifyDataChannel(channel: RTCDataChannel);

  // Notification of one of several types of state changed void onStateChange(state: PCObserverStateType);

  // Transceiver management; called when setRemoteDescription causes a
  transceiver to be created on the C++ side void onTransceiverNeeded(kind: DOMString, kind: DOMString);

  //
  Lets PeerConnectionImpl fire track events on the RTCPeerConnection
  #[alias = "fireTrackEvent"]
  pub fn fire_track_event(receiver: RTCRtpReceiver, receiver: RTCRtpReceiver);

  //
  Lets PeerConnectionImpl fire addstream events on the RTCPeerConnection
  #[alias = "fireStreamEvent"]
  pub fn fire_stream_event(stream: MediaStream);

  // Packet dump callback void onPacket(level: u32, level: u32, level: u32,
  packet: ArrayBuffer);

  // Transceiver sync void syncTransceivers();
}
