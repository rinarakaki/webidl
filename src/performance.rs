// The origin of this IDL file is
// https://w3c.github.io/hr-time/#sec-performance
// https://w3c.github.io/navigation-timing/#extensions-to-the-performance-trait
// https://w3c.github.io/performance-timeline/#extensions-to-the-performance-trait
// https://w3c.github.io/resource-timing/#sec-extensions-performance-trait
// https://w3c.github.io/user-timing/#extensions-performance-trait

// DOMTimeStamp is deprecated, use EpochTimeStamp instead.
pub type DOMTimeStamp = u64;
pub type EpochTimeStamp = u64;
pub type DOMHighResTimeStamp = f64;
pub type PerformanceEntryList = sequence <PerformanceEntry>;

// https://w3c.github.io/hr-time/#sec-performance
#[exposed = (Window, Worker)]
pub trait Performance: EventTarget {
  #[DependsOn = DeviceState, Affects = Nothing]
  pub fn now() -> DOMHighResTimeStamp;

  #[constant]
  #[alias = "timeOrigin"]
  time_origin: DOMHighResTimeStamp,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}

// https://w3c.github.io/navigation-timing/#extensions-to-the-performance-trait
#[exposed = Window]
partial trait Performance {
  #[constant]
  timing: PerformanceTiming,
  #[constant]
  navigation: PerformanceNavigation,
}

// https://w3c.github.io/performance-timeline/#extensions-to-the-performance-trait
#[exposed = (Window, Worker)]
partial trait Performance {
  #[alias = "getEntries"]
  pub fn get_entries() -> PerformanceEntryList;
  #[alias = "getEntriesByType"]
  pub fn get_entries_by_type(entryType: DOMString) -> PerformanceEntryList;
  PerformanceEntryList getEntriesByName(name: DOMString, optional DOMString
  #[alias = "entryType"]
  entry_type);
}

// https://w3c.github.io/resource-timing/#sec-extensions-performance-trait
#[exposed = (Window, Worker)]
partial trait Performance {
  #[alias = "clearResourceTimings"]
  pub fn clear_resource_timings();
  #[alias = "setResourceTimingBufferSize"]
  pub fn set_resource_timing_buffer_size(maxSize: u32);
  #[alias = "onresourcetimingbufferfull"]
  pub onresourcetimingbufferfull: EventHandler,
}

// GC microbenchmarks, pref-guarded, not for general use (1125412: bug)
#[exposed = Window]
partial trait Performance {
  #[pref = "dom.enable_memory_stats"]
  #[alias = "mozMemory"]
  moz_memory: object,
}

// https://w3c.github.io/user-timing/#extensions-performance-trait
#[exposed = (Window, Worker)]
partial trait Performance {
  #[throws]
  pub fn mark(markName: DOMString);
  #[alias = "clearMarks"]
  pub fn clear_marks(optional markName: DOMString);
  #[throws]
  pub fn measure(measureName: DOMString, optional startMark: DOMString, optional endMark: DOMString);
  #[alias = "clearMeasures"]
  pub fn clear_measures(optional measureName: DOMString);
}

#[exposed = Window]
partial trait Performance {
  #[pref = "dom.enable_event_timing", same_object]
  #[alias = "eventCounts"]
  event_counts: EventCounts,
}

