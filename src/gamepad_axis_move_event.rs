#[pref = "dom.gamepad.non_standard_events.enabled",
 exposed = Window, SecureContext]
pub trait GamepadAxisMoveEvent: GamepadEvent {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: GamepadAxisMoveEventInit);

  axis: u32,
  value: f64,
}

pub struct GamepadAxisMoveEventInit: GamepadEventInit {
  pub axis: u32 = 0,
  pub value: f64 = 0,
}
