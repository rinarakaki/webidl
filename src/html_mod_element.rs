// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#attributes-common-to-ins-and-del-elements

// http://www.whatwg.org/specs/web-apps/current-work/#attributes-common-to-ins-and-del-elements
#[exposed = Window]
pub trait HTMLModElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub cite: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "dateTime"]
  pub date_time: DOMString,
}
