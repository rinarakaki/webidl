// The origin of this IDL file is:
// https://w3c.github.io/requestidlecallback/

#[exposed = Window,
 func = "nsGlobalWindowInner::IsRequestIdleCallbackEnabled"]
pub trait IdleDeadline {
  #[alias = "timeRemaining"]
  pub fn time_remaining() -> DOMHighResTimeStamp;
  #[alias = "didTimeout"]
  did_timeout: bool,
}
