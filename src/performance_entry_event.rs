pub struct PerformanceEntryEventInit: EventInit {
  pub name: DOMString = "",
  #[alias = "entryType"]
  pub entry_type: DOMString = "",
  #[alias = "startTime"]
  pub start_time: DOMHighResTimeStamp = 0,
  pub duration: DOMHighResTimeStamp = 0,
  pub epoch: f64 = 0,
  pub origin: DOMString = "",
}

#[chrome_only, exposed = Window]
pub trait PerformanceEntryEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: PerformanceEntryEventInit);

  name: DOMString,
  #[alias = "entryType"]
  entry_type: DOMString,
  #[alias = "startTime"]
  start_time: DOMHighResTimeStamp,
  duration: DOMHighResTimeStamp,
  epoch: f64,
  origin: DOMString,
}
