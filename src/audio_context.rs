// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

pub struct AudioContextOptions {
  #[alias = "sampleRate"]
  pub sample_rate: f32,
}

pub struct AudioTimestamp {
  #[alias = "contextTime"]
  pub context_time: f64,
  #[alias = "performanceTime"]
  pub performance_time: DOMHighResTimeStamp,
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait AudioContext: BaseAudioContext {
  #[alias = "constructor"]
  pub fn new(#[optional] contextOptions: AudioContextOptions = {})
    -> Result<Self>;

  #[alias = "baseLatency"]
  pub base_latency: f64,
  #[alias = "outputLatency"]
  pub output_latency: f64,

  #[alias = "getOutputTimestamp"]
  pub fn get_output_timestamp() -> AudioTimestamp;

  pub fn suspend(&self) -> Result<Promise<void>>;
  pub fn close(&self) -> Result<Promise<void>>;

  #[new_object]
  #[alias = "createMediaElementSource"]
  pub fn create_media_element_source(&self, media_element: HTMLMediaElement)
    -> Result<MediaElementAudioSourceNode>;

  #[new_object]
  #[alias = "createMediaStreamSource"]
  pub fn create_media_stream_source(&self, media_stream: MediaStream)
    -> Result<MediaStreamAudioSourceNode>;

  #[new_object]
  #[alias = "createMediaStreamTrackSource"]
  pub fn create_media_stream_track_source(
    &self, media_stream_track: MediaStreamTrack)
    -> Result<MediaStreamTrackAudioSourceNode>;

  #[new_object]
  #[alias = "createMediaStreamDestination"]
  pub fn create_media_stream_destination(&self)
    -> Result<MediaStreamAudioDestinationNode>;
}
