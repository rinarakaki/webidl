// The origin of this IDL file is
// http://dvcs.w3.org/hg/html-media/raw-file/default/media-source/media-source.html

#[pref = "media.mediasource.enabled",
 exposed = Window]
pub trait SourceBufferList: EventTarget {
  length: u32,
  #[alias = "onaddsourcebuffer"]
  pub onaddsourcebuffer: EventHandler,
  #[alias = "onremovesourcebuffer"]
  pub on_removesourcebuffer: EventHandler,
  getter SourceBuffer (index: u32);
}
