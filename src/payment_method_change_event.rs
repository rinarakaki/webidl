// The origin of this WebIDL file is
// https://w3c.github.io/payment-request/#paymentmethodchangeevent-trait

#[SecureContext,
 exposed = Window,
 func = "mozilla::dom::PaymentRequest::PrefEnabled"]
pub trait PaymentMethodChangeEvent: PaymentRequestUpdateEvent {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: PaymentMethodChangeEventInit);

  #[alias = "methodName"]
  method_name: DOMString,
  #[alias = "methodDetails"]
  method_details: Option<object>,
}

pub struct PaymentMethodChangeEventInit: PaymentRequestUpdateEventInit {
  #[alias = "methodName"]
  pub method_name: DOMString = "",
  #[alias = "methodDetails"]
  pub method_details: Option<object> = None,
}
