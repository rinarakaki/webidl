// The origin of this IDL file is a combination of the FIDO U2F Raw Message Formats:
// https://www.fidoalliance.org/specs/fido-u2f-v1.1-id-20160915/fido-u2f-raw-message-formats-v1.1-id-20160915.html
// and the U2F JavaScript API v1.1:
// https://www.fidoalliance.org/specs/fido-u2f-v1.1-id-20160915/fido-u2f-javascript-api-v1.1-id-20160915.html

pub trait mixin GlobalU2F {
  #[SecureContext, throws, pref = "security.webauth.u2f", replaceable]
  u2f: U2F,
}

pub type ErrorCode = u16;
pub type Transports = Vec<Transport>;

pub enum Transport {
  #[alias = "bt"]
  Bt,  // TODO
  #[alias = "ble"]
  Ble,  // TODO
  #[alias = "nfc"]
  Nfc,  // TODO
  #[alias = "usb"]
  Usb  // TODO
}

#[GenerateToJSON]
pub struct U2FClientData {
  pub typ: DOMString, // Spelling is from the specification
  pub challenge: DOMString,
  pub origin: DOMString,
  // cid_pubkey for Token Binding is not implemented
}

pub struct RegisterRequest {
  pub version: DOMString,
  pub challenge: DOMString,
}

pub struct RegisterResponse {
  pub version: DOMString,
  #[alias = "registrationData"]
  pub registration_data: DOMString,
  #[alias = "clientData"]
  pub client_data: DOMString,

  // From Error
  #[alias = "errorCode"]
  pub error_code: Option<ErrorCode>,
  #[alias = "errorMessage"]
  pub error_message: Option<DOMString>,
}

pub struct RegisteredKey {
  pub version: DOMString,
  #[alias = "keyHandle"]
  pub key_handle: DOMString,
  pub transports: Option<Transports>,
  pub appId: Option<DOMString>,
}

pub struct SignResponse {
  #[alias = "keyHandle"]
  pub key_handle: DOMString,
  #[alias = "signatureData"]
  pub signature_data: DOMString,
  #[alias = "clientData"]
  pub client_data: DOMString,

  // From Error
  #[alias = "errorCode"]
  pub error_code: Option<ErrorCode>,
  #[alias = "errorMessage"]
  pub error_message: Option<DOMString>,
}

callback U2FRegisterCallback = void(response: RegisterResponse);
callback U2FSignCallback = void(response: SignResponse);

#[SecureContext, pref = "security.webauth.u2f",
 exposed = Window]
pub trait U2F {
  // These enumerations are defined in the FIDO U2F Javascript API under the
  // trait "ErrorCode" as constant integers, and also in the U2F.cpp file.
  // Any changes to these must occur in both locations.
  const u16 OK = 0;
  const u16 OTHER_ERROR = 1;
  const u16 BAD_REQUEST = 2;
  const u16 CONFIGURATION_UNSUPPORTED = 3;
  const u16 DEVICE_INELIGIBLE = 4;
  const u16 TIMEOUT = 5;

  // Returns a function. It's readonly + #[LenientSetter] to keep the Google
  // U2F polyfill from stomping on the value.
  #[LegacyLenientSetter, pure, Cached, throws]
  register: object,

  // A way to generate the actual implementation of register()
  #[Unexposed, throws, binary_name = "Register"]
  void register_impl(appId: DOMString,
  Vec<RegisterRequest> registerRequests,
  Vec<RegisteredKey> registeredKeys,
  callback: U2FRegisterCallback,
  optional Option<i32> opt_timeout_seconds);

  // Returns a function. It's readonly + #[LenientSetter] to keep the Google
  // U2F polyfill from stomping on the value.
  #[LegacyLenientSetter, pure, Cached, throws]
  sign: object,

  // A way to generate the actual implementation of sign()
  #[Unexposed, throws, binary_name = "Sign"]
  void sign_impl (appId: DOMString,
  challenge: DOMString,
  Vec<RegisteredKey> registeredKeys,
  callback: U2FSignCallback,
  optional Option<i32> opt_timeout_seconds);
}
