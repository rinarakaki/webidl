// The origin of this IDL file is
// https://svgwg.org/svg2-draft/types.html#InterfaceSVGAnimatedNumber

#[exposed = Window]
pub trait SVGAnimatedNumber {
  #[alias = "baseVal"]
  pub base_val: f32,
  #[alias = "animVal"]
  anim_val: f32,
}
