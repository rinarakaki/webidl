// The origin of this IDL file is
// http://dev.w3.org/fxtf/web-animations/#the-animatable-trait

// This typedef is off in its own file, because of bug 995352.
pub type UnrestrictedDoubleOrKeyframeAnimationOptions = (unrestricted f64 or KeyframeAnimationOptions);
