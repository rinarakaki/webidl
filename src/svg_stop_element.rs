// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGStopElement: SVGElement {
  #[constant]
  offset: SVGAnimatedNumber,
}

