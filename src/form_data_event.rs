// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#the-formdataevent-trait

#[exposed = Window]
pub trait FormDataEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: FormDataEventInit) -> Self;
  
  // C++ can't deal with a method called FormData() in the generated code
  #[binary_name = "GetFormData"]
  #[alias = "formData"]
  form_data: FormData,
}

pub struct FormDataEventInit: EventInit {
  required FormData formData;
}
