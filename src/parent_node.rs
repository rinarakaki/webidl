// The origin of this IDL file is
// http://dom.spec.whatwg.org/#trait-parentnode

pub trait mixin ParentNode {
  #[constant]
  children: HTMLCollection,
  #[pure]
  #[alias = "firstElementChild"]
  first_element_child: Option<Element>,
  #[pure]
  #[alias = "lastElementChild"]
  last_element_child: Option<Element>,
  #[pure]
  #[alias = "childElementCount"]
  child_element_count: u32,

  #[chrome_only]
  HTMLCollection getElementsByAttribute(name: DOMString,
  #[LegacyNullToEmptyString]
  value: DOMString);
  #[chrome_only, throws]
  HTMLCollection getElementsByAttributeNS(namespaceURI: Option<DOMString>, namespaceURI: Option<DOMString>,
  #[LegacyNullToEmptyString]
  value: DOMString);

  #[ce_reactions, throws, Unscopable]
  pub fn prepend((Node or DOMString)... nodes);
  #[ce_reactions, throws, Unscopable]
  pub fn append((Node or DOMString)... nodes);
  #[ce_reactions, throws, Unscopable]
  #[alias = "replaceChildren"]
  pub fn replace_children((Node or DOMString)... nodes);
}
