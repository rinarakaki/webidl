// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBRequest
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBRequestReadyState

use super::{
  DOMException,
  EventHandler,
  EventTarget,
  IDBCursor,
  IDBIndex,
  IDBObjectStore,
  IDBTransaction
};

pub enum IDBRequestReadyState {
  #[alias = "pending"]
  Pending,
  #[alias = "done"]
  Done
}

#[exposed = (Window, Worker)]
pub trait IDBRequest: EventTarget {
  #[throws]
  result: any,

  #[throws]
  error: Option<DOMException>,

  readonly attribute (IDBObjectStore or IDBIndex or IDBCursor)? source;
  transaction: Option<IDBTransaction>,
  #[alias = "readyState"]
  ready_state: IDBRequestReadyState,

  #[alias = "onsuccess"]
  pub on_success: EventHandler,

  #[alias = "onerror"]
  pub on_error: EventHandler,
}
