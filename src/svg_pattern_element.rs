// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/pservers.html#InterfaceSVGPatternElement

#[exposed = Window]
pub trait SVGPatternElement: SVGElement {
  #[constant]
  #[alias = "patternUnits"]
  pattern_units: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "patternContentUnits"]
  pattern_content_units: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "patternTransform"]
  pattern_transform: SVGAnimatedTransformList,
  #[constant]
  x: SVGAnimatedLength,
  #[constant]
  y: SVGAnimatedLength,
  #[constant]
  width: SVGAnimatedLength,
  #[constant]
  height: SVGAnimatedLength,
}

SVGPatternElement includes SVGFitToViewBox;
SVGPatternElement includes SVGURIReference;
