// The origin of this IDL file is
// https://drafts.csswg.org/web-animations/#animationplaybackevent

pub super::*;

#[func = "Document::IsWebAnimationsEnabled",
 exposed = Window]
pub trait AnimationPlaybackEvent: Event {
  #[constructor]
  pub fn new(
    type: DOMString,
    #[optional] event_init: AnimationPlaybackEventInit = {}) -> Self;

  #[alias = "currentTime"]
  pub current_time: Option<f64>,
  #[alias = "timelineTime"]
  pub timeline_time: Option<f64>,
}

pub struct AnimationPlaybackEventInit: EventInit {
  #[alias = "currentTime"]
  pub current_time: Option<f64> = None,
  #[alias = "timelineTime"]
  pub timeline_time: Option<f64> = None,
}
