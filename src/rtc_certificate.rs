// Specification: http://w3c.github.io/webrtc-pc/#certificate-management

#[GenerateInit]
pub struct RTCCertificateExpiration {
  #[EnforceRange]
  pub expires: DOMTimeStamp,
}

#[pref = "media.peerconnection.enabled", Serializable,
 exposed = Window]
pub trait RTCCertificate {
  expires: DOMTimeStamp,
}
