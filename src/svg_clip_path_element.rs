// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGClipPathElement: SVGElement {
  #[constant]
  #[alias = "clipPathUnits"]
  clip_path_units: SVGAnimatedEnumeration,
  #[constant]
  transform: SVGAnimatedTransformList,
}

