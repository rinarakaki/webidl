// The origin of this IDL file is
// https://w3c.github.io/reporting/#trait-reporting-observer

#[pref = "dom.reporting.enabled",
 exposed = (Window, Worker)]
pub trait ReportBody {
  #[Default]
  object toJSON
();
}

#[pref = "dom.reporting.enabled",
 exposed = (Window, Worker)]
pub trait Report {
  #[Default]
  object toJSON
();
  type: DOMString,
  url: DOMString,
  body: Option<ReportBody>,
}

#[pref = "dom.reporting.enabled",
 exposed = (Window, Worker)]
pub trait ReportingObserver {
  #[alias = "constructor"]
  pub fn new(callback: ReportingObserverCallback, #[optional = {}] options: ReportingObserverOptions) -> Result<Self, Error>;
  pub fn observe();
  pub fn disconnect();
  #[alias = "takeRecords"]
  pub fn take_records() -> ReportList;
}

callback ReportingObserverCallback = void (reports: Vec<Report>, reports: Vec<Report>);

pub struct ReportingObserverOptions {
  pub types: Vec<DOMString>,
  pub buffered: bool = false,
}

pub type ReportList = Vec<Report>;

#[pref = "dom.reporting.enabled",
 exposed = Window]
pub trait DeprecationReportBody: ReportBody {
  id: DOMString,
  // The spec currently has Date, but that's not a type that exists in Web IDL.
  // In any case, we always return None, so we just need _some_ Noneable type
  // here.
  #[alias = "anticipatedRemoval"]
  anticipated_removal: Option<DOMTimeStamp>,
  message: DOMString,
  #[alias = "sourceFile"]
  source_file: Option<DOMString>,
  readonly attribute Option<u32> lineNumber;
  readonly attribute Option<u32> columnNumber;
}

#[deprecated = "DeprecatedTestingInterface",
 pref = "dom.reporting.testing.enabled",
 exposed = (Window, DedicatedWorker)]
pub trait TestingDeprecatedInterface {
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[deprecated = "DeprecatedTestingMethod"]
  #[alias = "deprecatedMethod"]
  pub fn deprecated_method();

  #[deprecated = "DeprecatedTestingAttribute"]
  #[alias = "deprecatedAttribute"]
  deprecated_attribute: bool,
}

// Used internally to process the JSON
#[GenerateInit]
pub struct ReportingHeaderValue {
  pub items: Vec<ReportingItem>,
}

// Used internally to process the JSON
pub struct ReportingItem {
  // This is a long.
  pub max_age: any,
  // This is a sequence of ReportingEndpoint.
  pub endpoints: any,
  // This is a string. If missing, the value is 'default'.
  pub group: any,
  pub include_subdomains: bool = false,
}

// Used internally to process the JSON
#[GenerateInit]
pub struct ReportingEndpoint {
  // This is a string.
  pub url: any,
  // This is an u32.
  pub priority: any,
  // This is an u32.
  pub weight: any,
}
