// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEBlendElement: SVGElement {

  // Blend Mode Types
  const u16 SVG_FEBLEND_MODE_UNKNOWN = 0;
  const u16 SVG_FEBLEND_MODE_NORMAL = 1;
  const u16 SVG_FEBLEND_MODE_MULTIPLY = 2;
  const u16 SVG_FEBLEND_MODE_SCREEN = 3;
  const u16 SVG_FEBLEND_MODE_DARKEN = 4;
  const u16 SVG_FEBLEND_MODE_LIGHTEN = 5;
  const u16 SVG_FEBLEND_MODE_OVERLAY = 6;
  const u16 SVG_FEBLEND_MODE_COLOR_DODGE = 7;
  const u16 SVG_FEBLEND_MODE_COLOR_BURN = 8;
  const u16 SVG_FEBLEND_MODE_HARD_LIGHT = 9;
  const u16 SVG_FEBLEND_MODE_SOFT_LIGHT = 10;
  const u16 SVG_FEBLEND_MODE_DIFFERENCE = 11;
  const u16 SVG_FEBLEND_MODE_EXCLUSION = 12;
  const u16 SVG_FEBLEND_MODE_HUE = 13;
  const u16 SVG_FEBLEND_MODE_SATURATION = 14;
  const u16 SVG_FEBLEND_MODE_COLOR = 15;
  const u16 SVG_FEBLEND_MODE_LUMINOSITY = 16;
  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  in2: SVGAnimatedString,
  #[constant]
  mode: SVGAnimatedEnumeration,
}

SVGFEBlendElement includes SVGFilterPrimitiveStandardAttributes;
