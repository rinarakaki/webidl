// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimatedPreserveAspectRatio {
  #[constant]
  #[alias = "baseVal"]
  base_val: SVGPreserveAspectRatio,
  #[constant]
  #[alias = "animVal"]
  anim_val: SVGPreserveAspectRatio,
}

