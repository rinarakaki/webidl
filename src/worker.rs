// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/workers.html

use super::{EventHandler, StructuredSerialiseOptions};

#[exposed = (Window,DedicatedWorker, SharedWorker)]
pub trait Worker: EventTarget {
  #[alias = "constructor"]
  pub fn new(script_url: USVString, #[optional = {}] options: WorkerOptions)
    -> Result<Self>;

  pub fn terminate(&self);

  #[alias = "postMessage"]
  pub fn post_message(&self, message: any, transfer: Vec<object>)
    -> Result<()>;
  
  #[alias = "postMessage"]
  pub fn post_message(
    &self, 
    message: any,
    #[optional = {}] aOptions: StructuredSerialiseOptions)
    -> Result<()>;

  #[alias = "onmessage"]
  pub on_message: EventHandler,
  #[alias = "onmessageerror"]
  pub on_message_error: EventHandler,
}

Worker includes AbstractWorker;

pub struct WorkerOptions {
  // WorkerType type = "classic"; TODO: Bug 1247687
  // RequestCredentials credentials = "omit"; // credentials is only used if type is "module" TODO: Bug 1247687
  pub name: DOMString = "",
}

#[func = "mozilla::dom::ChromeWorker::WorkerAvailable",
 exposed = (Window, DedicatedWorker, SharedWorker)]
pub trait ChromeWorker: Worker {
  #[alias = "constructor"]
  pub fn new(&self, script_url: USVString) -> Result<Self>;
}
