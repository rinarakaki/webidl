// The origin of this IDL file is
// http://www.w3.org/2012/sysapps/tcp-udp-sockets/#trait-udpsocket
// http://www.w3.org/2012/sysapps/tcp-udp-sockets/#pub struct-udpoptions

pub struct UDPOptions {
  #[alias = "localAddress"]
  pub local_address: DOMString,
  #[alias = "localPort"]
  pub local_port: u16,
  #[alias = "remoteAddress"]
  pub remote_address: DOMString,
  #[alias = "remotePort"]
  pub remote_port: u16,
  #[alias = "addressReuse"]
  pub address_reuse: bool = true,
  pub loopback: bool = false,
}

#[pref = "dom.udpsocket.enabled",
 chrome_only,
 exposed = Window]
pub trait UDPSocket: EventTarget {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] options: UDPOptions) -> Result<Self, Error>;

  #[alias = "localAddress"]
  local_address: Option<DOMString>,
  readonly attribute Option<u16> localPort;
  #[alias = "remoteAddress"]
  remote_address: Option<DOMString>,
  readonly attribute Option<u16> remotePort;
  #[alias = "addressReuse"]
  address_reuse: bool,
  loopback: bool,
  #[alias = "readyState"]
  ready_state: SocketReadyState,
  opened: Promise<void>,
  closed: Promise<void>,
//    readonly attribute ReadableStream input; //Bug 1056444: Stream API is not ready
//    readonly attribute WriteableStream output; //Bug 1056444: Stream API is not ready
  #[alias = "onmessage"]
  pub on_message: EventHandler, //Bug 1056444: use event trait before Stream API is ready
  Promise<void> close ();
  #[throws]
  void joinMulticastGroup(multicastGroupAddress: DOMString);
  #[throws]
  void leaveMulticastGroup(multicastGroupAddress: DOMString);
  #[throws]
  bool send((DOMString or Blob or ArrayBuffer or ArrayBufferView) data, optional Option<DOMString> remoteAddress, optional Option<u16> remotePort); //Bug 1056444: use send method before Stream API is ready
}
