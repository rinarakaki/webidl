// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

use super::*;

#[exposed = Window]
pub trait SVGElement: Element {
  pub id: DOMString,

  #[constant]
  #[alias = "className"]
  pub fn class_name(&self) -> SVGAnimatedString;

  #[alias = "ownerSVGElement"]
  pub fn owner_svg_element(&self) -> Option<SVGSVGElement>;
  
  #[alias = "viewportElement"]
  pub fn viewport_element(&self) -> Option<SVGElement>;

  pub nonce: DOMString,
}

SVGElement includes GlobalEventHandlers;
SVGElement includes HTMLOrForeignElement;
SVGElement includes DocumentAndElementEventHandlers;
SVGElement includes ElementCSSInlineStyle;
SVGElement includes TouchEventHandlers;
SVGElement includes OnErrorEventHandlerForNodes;
