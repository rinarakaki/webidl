// The origin of this IDL file is
// http://www.w3.org/TR/WebCryptoAPI/

pub struct KeyAlgorithm {
  required DOMString name;
}

#[GenerateConversionToJS]
pub struct AesKeyAlgorithm: KeyAlgorithm {
  required u16 length;
}

#[GenerateConversionToJS]
pub struct EcKeyAlgorithm: KeyAlgorithm {
  required DOMString namedCurve;
}

#[GenerateConversionToJS]
pub struct HmacKeyAlgorithm: KeyAlgorithm {
  required KeyAlgorithm hash;
  required u32 length;
}

#[GenerateConversionToJS]
pub struct RsaHashedKeyAlgorithm: KeyAlgorithm {
  required u16 modulusLength;
  required Uint8Array publicExponent;
  required KeyAlgorithm hash;
}

#[GenerateConversionToJS]
pub struct DhKeyAlgorithm: KeyAlgorithm {
  required Uint8Array prime;
  required Uint8Array generator;
}

