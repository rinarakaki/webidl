#[chrome_only, exposed = Window]
pub trait TreeContentView {
  // Retrieve the content item associated with the specified row.
  #[throws]
  #[alias = "getItemAtIndex"]
  pub fn get_item_at_index(row: i32) -> Option<Element>;

  // Retrieve the index associated with the specified content item.
  #[alias = "getIndexOfItem"]
  pub fn get_index_of_item(item: Option<Element>) -> i32;
}

TreeContentView includes TreeView;
