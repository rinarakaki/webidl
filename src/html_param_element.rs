// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-param-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-param-element
#[exposed = Window]
pub trait HTMLParamElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub name: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub value: DOMString,
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLParamElement {
  #[ce_reactions, setter_throws, pure]
  pub type: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "valueType"]
  pub value_type: DOMString,
}
