// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120405/#trait-documentfragment
// http://www.w3.org/TR/2012/WD-selectors-api-20120628/#trait-definitions

#[exposed = Window]
pub trait DocumentFragment: Node {
  #[alias = "constructor"]
  pub fn new() -> Result<Self>;

  #[alias = "getElementById"]
  pub fn get_element_by_id(elementId: DOMString) -> Option<Element>;
}

// http://www.w3.org/TR/2012/WD-selectors-api-20120628/#trait-definitions
partial trait DocumentFragment {
  #[alias = "querySelector"]
  pub fn query_selector(selectors: UTF8String) -> Result<Option<Element>>;

  #[alias = "querySelectorAll"]
  pub fn query_selector_all(selectors: UTF8String) -> Result<NodeList>;
}

DocumentFragment includes ParentNode;
