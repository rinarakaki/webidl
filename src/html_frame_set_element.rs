// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLFrameSetElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub cols: DOMString,
  #[ce_reactions, setter_throws]
  pub rows: DOMString,
}

HTMLFrameSetElement includes WindowEventHandlers;
