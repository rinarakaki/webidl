// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimatedNumberList {
  #[constant]
  #[alias = "baseVal"]
  base_val: SVGNumberList,
  #[constant]
  #[alias = "animVal"]
  anim_val: SVGNumberList,
}

