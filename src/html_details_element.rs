// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/forms.html#the-details-element

#[exposed = Window]
pub trait HTMLDetailsElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub open: bool,
}
