// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#functiocn

pub type function = any(any... arguments);

pub type Voidfunction = Fn();
