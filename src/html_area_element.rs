// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-area-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-area-element
#[exposed = Window]
pub trait HTMLAreaElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub alt: DOMString,
  #[ce_reactions, setter_throws]
  pub coords: DOMString,
  #[ce_reactions, setter_throws]
  pub shape: DOMString,
  #[ce_reactions, setter_throws]
  pub target: DOMString,
  #[ce_reactions, setter_throws]
  pub download: DOMString,
  #[ce_reactions, setter_throws]
  pub ping: DOMString,
  #[ce_reactions, setter_throws]
  pub rel: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "referrerPolicy"]
  pub referrer_policy: DOMString,
  #[put_forwards = value]
  #[alias = "relList"]
  rel_list: DOMTokenList,
}

HTMLAreaElement includes HTMLHyperlinkElementUtils;

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLAreaElement {
  #[ce_reactions, setter_throws]
  #[alias = "noHref"]
  pub no_href: bool,
}
