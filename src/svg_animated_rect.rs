// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimatedRect {
  #[alias = "baseVal"]
  base_val: Option<SVGRect>,
  #[alias = "animVal"]
  anim_val: Option<SVGRect>,
}
