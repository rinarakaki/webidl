// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-input-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
// https://wicg.github.io/entries-api/#idl-index

pub enum SelectionMode {
  #[alias = "select"]
  Select,
  #[alias = "start"]
  Start,
  #[alias = "end"]
  End,
  #[alias = "preserve"]
  Preserve,
}

pub trait XULControllers;

#[exposed = Window]
pub trait HTMLInputElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, pure, setter_throws]
  pub accept: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub alt: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub autocomplete: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub autofocus: bool,
  #[ce_reactions, pure, setter_throws, pref = "dom.capture.enabled"]
  pub capture: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "defaultChecked"]
  pub default_checked: bool,
  #[pure]
  pub checked: bool,
  // Bug 850337 - attribute DOMString dirName;
  #[ce_reactions, pure, setter_throws]
  pub disabled: bool,
  form: Option<HTMLFormElement>,
  #[pure]
  pub files: Option<FileList>;
  #[ce_reactions, pure, setter_throws]
  #[alias = "formAction"]
  pub form_action: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "formEnctype"]
  pub form_enctype: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "formMethod"]
  pub form_method: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "formNoValidate"]
  pub form_no_validate: bool,
  #[ce_reactions, pure, setter_throws]
  #[alias = "formTarget"]
  pub form_target: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub height: u32,
  #[pure]
  pub indeterminate: bool,
  #[pure]
  list: Option<HTMLElement>,
  #[ce_reactions, pure, setter_throws]
  pub max: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "maxLength"]
  pub max_length: i32,
  #[ce_reactions, pure, setter_throws]
  pub min: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "minLength"]
  pub min_length: i32,
  #[ce_reactions, pure, setter_throws]
  pub multiple: bool,
  #[ce_reactions, pure, setter_throws]
  pub name: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub pattern: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub placeholder: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "readOnly"]
  pub read_only: bool,
  #[ce_reactions, pure, setter_throws]
  pub required: bool,
  #[ce_reactions, pure, setter_throws]
  pub size: u32,
  #[ce_reactions, pure, Setterneeds_subject_principal = NonSystem, setter_throws]
  pub src: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub step: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub type: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "defaultValue"]
  pub default_value: DOMString,
  #[ce_reactions, pure, setter_throws, needs_caller_type]
  pub value: [LegacyNullToEmptyString] DOMString;
  #[throws]
  #[alias = "valueAsDate"]
  pub value_as_date: Option<object>;
  #[pure, setter_throws]
  #[alias = "valueAsNumber"]
  pub value_as_number: unrestricted f64;
  #[ce_reactions, setter_throws]
  pub width: u32,

  #[alias = "stepUp"]
  pub fn step_up(#[optional = 1] n: i32) -> Result<()>;

  #[alias = "stepDown"]
  pub fn step_down(#[optional = 1] n: i32) -> Result<()>;

  #[pure]
  #[alias = "willValidate"]
  will_validate: bool,
  #[pure]
  validity: ValidityState,
  #[throws]
  #[alias = "validationMessage"]
  validation_message: DOMString,
  #[alias = "checkValidity"]
  pub fn check_validity() -> bool;
  #[alias = "reportValidity"]
  pub fn report_validity() -> bool;
  #[alias = "setCustomValidity"]
  pub fn set_custom_validity(error: DOMString);

  labels: Option<NodeList>,

  pub fn select();

  #[throws]
  selectionStart: Option<u32>;
  #[throws]
  selectionEnd: Option<u32>;
  #[throws]
  selectionDirection: Option<DOMString>;

  #[alias = "setRangeText"]
  pub fn set_range_text(replacement: DOMString) -> Result<()>;

  #[alias = "setRangeText"]
  pub fn set_range_text(
    replacement: DOMString, replacement: DOMString,
    end: u32,
    #[optional = "preserve"] selectionMode: SelectionMode) -> Result<()>;

  #[alias = "setSelectionRange"]
  pub fn set_selection_range(start: u32, start: u32, optional direction: DOMString)
    -> Result<()>;

  // also has obsolete members
}

partial trait HTMLInputElement {
  #[ce_reactions, pure, setter_throws]
  pub align: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "useMap"]
  pub use_map: DOMString,
}

// Mozilla extensions

partial trait HTMLInputElement {
  #[getter_throws, chrome_only]
  controllers: Option<XULControllers>,
  // Binaryname because we have a FragmentOrElement function named "TextLength()".
  #[needs_caller_type, binary_name = "inputTextLength"]
  #[alias = "textLength"]
  text_length: i32,

  #[throws, chrome_only]
  #[alias = "mozGetFileNameArray"]
  pub fn moz_get_file_name_array() -> Vec<DOMString>;

  #[chrome_only, throws]
  #[alias = "mozSetFileNameArray"]
  pub fn moz_set_file_name_array(fileNames: Vec<DOMString>);

  #[chrome_only]
  #[alias = "mozSetFileArray"]
  pub fn moz_set_file_array(files: Vec<File>);

  // This method is meant to use for testing only.
  #[chrome_only, throws]
  #[alias = "mozSetDirectory"]
  pub fn moz_set_directory(directoryPath: DOMString);

  // This method is meant to use for testing only.
  #[chrome_only]
  #[alias = "mozSetDndFilesAndDirectories"]
  pub fn moz_set_dnd_files_and_directories(Vec<(File or Directory)> list);

  #[alias = "mozIsTextField"]
  pub fn moz_is_text_field(aExcludePassword: bool) -> bool;

  #[chrome_only]
  #[alias = "hasBeenTypePassword"]
  has_been_type_password: bool,

  #[chrome_only]
  #[alias = "previewValue"]
  pub preview_value: DOMString,

  #[chrome_only]
  // This function will return None if @autocomplete is not defined for the
  // current @type
  #[alias = "getAutocompleteInfo"]
  pub fn get_autocomplete_info() -> Option<AutocompleteInfo>;

  #[chrome_only]
  // The reveal password state for a type = password control.
  #[alias = "revealPassword"]
  pub reveal_password: bool,
}

pub trait mixin MozEditableElement {
  // Returns an nsIEditor instance which is associated with the element.
  // If the element can be associated with an editor but not yet created,
  // this creates new one automatically.
  #[pure, chrome_only, binary_name = "editorForBindings"]
  editor: Option<nsIEditor>,

  // Returns true if an nsIEditor instance has already been associated with
  // the element.
  #[pure, chrome_only]
  #[alias = "hasEditor"]
  has_editor: bool,

  // This is set to true if "input" event should be fired with InputEvent on
  // the element. Otherwise, i.e., if "input" event should be fired with
  // Event, set to false.
  #[chrome_only]
  #[alias = "isInputEventTarget"]
  is_input_event_target: bool,

  // This is similar to set .value on nsIDOMInput/TextAreaElements, but handling
  // of the value change is closer to the normal user input, so 'change' event
  // for example will be dispatched when focusing out the element.
  #[func = "IsChromeOrUAWidget", needs_subject_principal]
  #[alias = "setUserInput"]
  pub fn set_user_input(input: DOMString);
}

HTMLInputElement includes MozEditableElement;

partial trait HTMLInputElement {
  #[pref = "dom.input.dirpicker", setter_throws]
  pub allowdirs: bool,

  #[pref = "dom.input.dirpicker"]
  #[alias = "isFilesAndDirectoriesSupported"]
  is_files_and_directories_supported: bool,

  #[throws, pref = "dom.input.dirpicker"]
  Promise<Vec<(File or Directory)>> getFilesAndDirectories();

  #[throws, pref = "dom.input.dirpicker"]
  Promise<Vec<File>> getFiles(#[optional = false] recursiveFlag: bool);

  #[throws, pref = "dom.input.dirpicker"]
  #[alias = "chooseDirectory"]
  pub fn choose_directory();
}

HTMLInputElement includes MozImageLoadingContent;

// https://wicg.github.io/entries-api/#idl-index
partial trait HTMLInputElement {
  #[pref = "dom.webkitBlink.filesystem.enabled", Frozen, Cached, pure]
  #[alias = "webkitEntries"]
  webkit_entries: Vec<FileSystemEntry>,

  #[pref = "dom.webkitBlink.dirPicker.enabled", binary_name = "WebkitDirectoryAttr", setter_throws]
  pub webkitdirectory: bool,
}

pub struct DateTimeValue {
  pub hour: i32,
  pub minute: i32,
  pub year: i32,
  pub month: i32,
  pub day: i32,
}

partial trait HTMLInputElement {
  #[chrome_only]
  #[alias = "getDateTimeInputBoxValue"]
  pub fn get_date_time_input_box_value() -> DateTimeValue;

  #[chrome_only]
  #[alias = "dateTimeBoxElement"]
  date_time_box_element: Option<Element>,

  #[chrome_only, binary_name = "getMinimumAsDouble"]
  #[alias = "getMinimum"]
  pub fn get_minimum() -> f64;

  #[chrome_only, binary_name = "getMaximumAsDouble"]
  #[alias = "getMaximum"]
  pub fn get_maximum() -> f64;

  #[func = "IsChromeOrUAWidget"]
  #[alias = "openDateTimePicker"]
  pub fn open_date_time_picker(#[optional = {}] initialValue: DateTimeValue);

  #[func = "IsChromeOrUAWidget"]
  #[alias = "updateDateTimePicker"]
  pub fn update_date_time_picker(#[optional = {}] value: DateTimeValue);

  #[func = "IsChromeOrUAWidget"]
  #[alias = "closeDateTimePicker"]
  pub fn close_date_time_picker();

  #[func = "IsChromeOrUAWidget"]
  #[alias = "setFocusState"]
  pub fn set_focus_state(aIsFocused: bool);

  #[func = "IsChromeOrUAWidget"]
  #[alias = "updateValidityState"]
  pub fn update_validity_state();

  #[func = "IsChromeOrUAWidget", binary_name = "getStepAsDouble"]
  #[alias = "getStep"]
  pub fn get_step() -> f64;

  #[func = "IsChromeOrUAWidget", binary_name = "getStepBaseAsDouble"]
  #[alias = "getStepBase"]
  pub fn get_step_base() -> f64;
}
