// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-navigator-object
// http://www.w3.org/TR/tracking-dnt/
// http://www.w3.org/TR/geolocation-API/#geolocation_interface
// http://www.w3.org/TR/battery-status/#navigatorbattery-trait
// http://www.w3.org/TR/vibration/#vibration-trait
// http://www.w3.org/2012/sysapps/runtime/#extension-to-the-navigator-trait-1
// https://dvcs.w3.org/hg/gamepad/raw-file/default/gamepad.html#navigator-trait-extension
// http://www.w3.org/TR/beacon/#sec-beacon-method
// https://html.spec.whatwg.org/#navigatorconcurrenthardware
// http://wicg.github.io/netinfo/#extensions-to-the-navigator-trait
// https://w3c.github.io/webappsec-credential-management/#framework-credential-management
// https://w3c.github.io/webdriver/webdriver-spec.html#trait
// https://wicg.github.io/media-capabilities/#idl-index
// https://w3c.github.io/mediasession/#idl-index

pub trait URI;

// http://www.whatwg.org/specs/web-apps/current-work/#the-navigator-object
#[HeaderFile = "Navigator.h",
 exposed = Window]
pub trait Navigator {
  // objects implementing this trait also implement the interfaces given below
}
Navigator includes NavigatorID;
Navigator includes NavigatorLanguage;
Navigator includes NavigatorOnLine;
Navigator includes NavigatorContentUtils;
Navigator includes NavigatorStorageUtils;
Navigator includes NavigatorConcurrentHardware;
Navigator includes NavigatorStorage;
Navigator includes NavigatorAutomationInformation;
Navigator includes GPUProvider;

pub trait mixin NavigatorID {
  // WebKit/Blink/Trident/Presto support this (hardcoded "Mozilla").
  #[constant, Cached, throws]
  #[alias = "appCodeName"]
  app_code_name: DOMString, // constant "Mozilla"
  #[constant, Cached, needs_caller_type]
  #[alias = "appName"]
  app_name: DOMString,
  #[constant, Cached, throws, needs_caller_type]
  #[alias = "appVersion"]
  app_version: DOMString,
  #[pure, Cached, throws, needs_caller_type]
  platform: DOMString,
  #[pure, Cached, throws, needs_caller_type]
  #[alias = "userAgent"]
  user_agent: DOMString,
  #[constant, Cached]
  product: DOMString, // constant "Gecko"

  // Everyone but WebKit/Blink supports this. See bug 679971.
  #[exposed = Window]
  #[alias = "taintEnabled"]
  pub fn taint_enabled() -> bool; // constant false
}

pub trait mixin NavigatorLanguage {

  // These two attributes are cached because this trait is also implemented
  // by Workernavigator and this way we don't have to go back to the
  // main-thread from the worker thread anytime we need to retrieve them. They
  // are updated when pref intl.accept_languages is changed.

  #[pure, Cached]
  language: Option<DOMString>,
  #[pure, Cached, Frozen]
  languages: Vec<DOMString>,
}

pub trait mixin NavigatorOnLine {
  #[alias = "onLine"]
  on_line: bool,
}

pub trait mixin NavigatorContentUtils {
  // content handler registration
  #[throws, chrome_only]
  #[alias = "checkProtocolHandlerAllowed"]
  pub fn check_protocol_handler_allowed(scheme: DOMString, scheme: DOMString, scheme: DOMString);
  #[throws, SecureContext]
  #[alias = "registerProtocolHandler"]
  pub fn register_protocol_handler(scheme: DOMString, scheme: DOMString);
  // NOT IMPLEMENTED
  //void unregisterProtocolHandler(scheme: DOMString, scheme: DOMString);
}

#[SecureContext]
pub trait mixin NavigatorStorage {
  #[pref = "dom.storageManager.enabled"]
  storage: StorageManager,
}

pub trait mixin NavigatorStorageUtils {
  // NOT IMPLEMENTED
  //void yieldForStorageUpdates();
}

partial trait Navigator {
  #[throws]
  permissions: Permissions,
}

// Things that definitely need to be in the spec and and are not for some
// reason. See https://www.w3.org/Bugs/Public/show_bug.Option<cgi>id = 22406
partial trait Navigator {
  #[throws]
  #[alias = "mimeTypes"]
  mime_types: MimeTypeArray,
  #[throws]
  plugins: PluginArray,
}

// http://www.w3.org/TR/tracking-dnt/ sort of
partial trait Navigator {
  #[alias = "doNotTrack"]
  do_not_track: DOMString,
}

// https://globalprivacycontrol.github.io/gpc-spec/
partial trait Navigator {
  #[pref = "privacy.globalprivacycontrol.functionality.enabled"]
  #[alias = "globalPrivacyControl"]
  global_privacy_control: bool,
}

// http://www.w3.org/TR/geolocation-API/#geolocation_interface
pub trait mixin NavigatorGeolocation {
  #[throws, pref = "geo.enabled"]
  geolocation: Geolocation,
}
Navigator includes NavigatorGeolocation;

// http://www.w3.org/TR/battery-status/#navigatorbattery-trait
partial trait Navigator {
  // chrome_only to prevent web content from fingerprinting users' batteries.
  #[throws, chrome_only, pref = "dom.battery.enabled"]
  #[alias = "getBattery"]
  pub fn get_battery() -> Promise<BatteryManager>;
}

// http://www.w3.org/TR/vibration/#vibration-trait
partial trait Navigator {
  // We don't support sequences in unions yet
  //bool vibrate ((u32 or Vec<u32>) pattern);
  pub fn vibrate(duration: u32) -> bool;
  pub fn vibrate(pattern: Vec<u32>) -> bool;
}

// http://www.w3.org/TR/pointerevents/#extensions-to-the-navigator-trait
partial trait Navigator {
  #[needs_caller_type]
  #[alias = "maxTouchPoints"]
  max_touch_points: i32,
}

// https://wicg.github.io/media-capabilities/#idl-index
#[exposed = Window]
partial trait Navigator {
  #[same_object, func = "mozilla::dom::MediaCapabilities::Enabled"]
  #[alias = "mediaCapabilities"]
  media_capabilities: MediaCapabilities,
}

// Mozilla-specific extensions

// Chrome-only trait for Vibration API permission handling.
partial trait Navigator {
  // Set permission state to device vibration.
  // @param permitted permission state (true for allowing vibration)
  // @param persistent make the permission session-persistent
  #[chrome_only]
  void setVibrationPermission(permitted: bool,
  #[optional = true] persistent: bool);
}

partial trait Navigator {
  #[throws, constant, Cached, needs_caller_type]
  oscpu: DOMString,
  // WebKit/Blink support this; Trident/Presto do not.
  vendor: DOMString,
  // WebKit/Blink supports this (hardcoded ""); Trident/Presto do not.
  #[alias = "vendorSub"]
  vendor_sub: DOMString,
  // WebKit/Blink supports this (hardcoded "20030107"); Trident/Presto don't
  #[alias = "productSub"]
  product_sub: DOMString,
  // WebKit/Blink/Trident/Presto support this.
  #[alias = "cookieEnabled"]
  cookie_enabled: bool,
  #[throws, constant, Cached, needs_caller_type]
  #[alias = "buildID"]
  build_id: DOMString,

  // WebKit/Blink/Trident/Presto support this.
  #[Affects = Nothing, DependsOn = Nothing]
  #[alias = "javaEnabled"]
  pub fn java_enabled() -> bool;
}

// Addon manager bits
partial trait Navigator {
  #[throws, func = "mozilla::AddonManagerWebAPI::IsAPIEnabled"]
  #[alias = "mozAddonManager"]
  moz_addon_manager: AddonManager,
}

// NetworkInformation
partial trait Navigator {
  #[throws, pref = "dom.netinfo.enabled"]
  connection: NetworkInformation,
}

// https://dvcs.w3.org/hg/gamepad/raw-file/default/gamepad.html#navigator-trait-extension
partial trait Navigator {
  #[throws, pref = "dom.gamepad.enabled", SecureContext]
  Vec<Option<Gamepad>> getGamepads();
}
partial trait Navigator {
  #[pref = "dom.gamepad.test.enabled"]
  #[alias = "requestGamepadServiceTest"]
  pub fn request_gamepad_service_test() -> GamepadServiceTest;
}

// https://immersive-web.github.io/webvr/spec/1.1/#trait-navigator
partial trait Navigator {
  #[throws, SecureContext, pref = "dom.vr.enabled"]
  Promise<Vec<VRDisplay>> getVRDisplays();
  // TODO: Use FrozenArray once available. (1236777: Bug)
  #[SecureContext, Frozen, Cached, pure, pref = "dom.vr.enabled"]
  #[alias = "activeVRDisplays"]
  active_vr_displays: Vec<VRDisplay>,
  #[chrome_only, pref = "dom.vr.enabled"]
  #[alias = "isWebVRContentDetected"]
  is_web_vr_content_detected: bool,
  #[chrome_only, pref = "dom.vr.enabled"]
  #[alias = "isWebVRContentPresenting"]
  is_web_vr_content_presenting: bool,
  #[chrome_only, pref = "dom.vr.enabled"]
  #[alias = "requestVRPresentation"]
  pub fn request_vr_presentation(display: VRDisplay);
}
partial trait Navigator {
  #[pref = "dom.vr.puppet.enabled"]
  #[alias = "requestVRServiceTest"]
  pub fn request_vr_service_test() -> VRServiceTest;
}

// https://immersive-web.github.io/webxr/#dom-navigator-xr
partial trait Navigator {
  #[SecureContext, same_object, throws, pref = "dom.vr.webxr.enabled"]
  xr: XRSystem,
}

// http://webaudio.github.io/web-midi-api/#requestmidiaccess
partial trait Navigator {
  #[throws, pref = "dom.webmidi.enabled"]
  #[alias = "requestMIDIAccess"]
  pub fn request_midi_access(#[optional = {}] options: MIDIOptions) -> Promise<MIDIAccess>;
}

callback NavigatorUserMediaSuccessCallback = void (stream: MediaStream);
callback NavigatorUserMediaErrorCallback = void (error: MediaStreamError);

partial trait Navigator {
  #[throws, func = "Navigator::HasUserMediaSupport"]
  #[alias = "mediaDevices"]
  media_devices: MediaDevices,

  // deprecated. Use mediaDevices.getUserMedia instead.
  #[deprecated = "NavigatorGetUserMedia", throws,
  func = "Navigator::HasUserMediaSupport",
  needs_caller_type,
  use_counter]
  void mozGetUserMedia(constraints: MediaStreamConstraints,
  NavigatorUserMediaSuccessCallback successCallback,
  NavigatorUserMediaErrorCallback errorCallback);
}

// Service Workers/Navigation Controllers
partial trait Navigator {
  #[func = "ServiceWorkerContainer::IsEnabled", same_object]
  #[alias = "serviceWorker"]
  service_worker: ServiceWorkerContainer,
}

partial trait Navigator {
  #[throws, pref = "beacon.enabled"]
  bool sendBeacon(url: DOMString,
  optional Option<BodyInit> data = None);
}

partial trait Navigator {
  #[new_object, func = "mozilla::dom::TCPSocket::ShouldTCPSocketExist"]
  #[alias = "mozTCPSocket"]
  moz_tcp_socket: LegacyMozTCPSocket,
}

partial trait Navigator {
  #[new_object]
  Promise<MediaKeySystemAccess>
  #[alias = "requestMediaKeySystemAccess"]
  request_media_key_system_access(keySystem: DOMString,
  Vec<MediaKeySystemConfiguration> supportedConfigurations);
}

pub trait mixin NavigatorConcurrentHardware {
  #[alias = "hardwareConcurrency"]
  hardware_concurrency: u64,
}

// https://w3c.github.io/webappsec-credential-management/#framework-credential-management
partial trait Navigator {
  #[pref = "security.webauth.webauthn", SecureContext, same_object]
  credentials: CredentialsContainer,
}

// https://w3c.github.io/webdriver/webdriver-spec.html#trait
pub trait mixin NavigatorAutomationInformation {
  #[constant, Cached]
  webdriver: bool,
}

// https://www.w3.org/TR/clipboard-apis/#navigator-trait
partial trait Navigator {
  #[pref = "dom.events.asyncClipboard", SecureContext, same_object]
  clipboard: Clipboard,
}

// https://wicg.github.io/web-share/#navigator-trait
partial trait Navigator {
  #[SecureContext, throws, func = "Navigator::HasShareSupport"]
  pub fn share(#[optional = {}] data: ShareData) -> Promise<void>;
  #[SecureContext, func = "Navigator::HasShareSupport"]
  #[alias = "canShare"]
  pub fn can_share(#[optional = {}] data: ShareData) -> bool;
}
// https://wicg.github.io/web-share/#sharedata-pub struct
pub struct ShareData {
  pub title: USVString,
  pub text: USVString,
  pub url: USVString,
  // Note: we don't actually support files yet
  // we have it here for the .canShare() checks.
  pub files: Vec<File>,
}

// https://w3c.github.io/mediasession/#idl-index
#[exposed = Window]
partial trait Navigator {
  #[pref = "dom.media.mediasession.enabled", same_object]
  #[alias = "mediaSession"]
  media_session: MediaSession,
}

// https://wicg.github.io/web-locks/#navigator-mixins
#[SecureContext]
pub trait mixin NavigatorLocks {
  #[pref = "dom.weblocks.enabled"]
  locks: LockManager,
}
Navigator includes NavigatorLocks;
