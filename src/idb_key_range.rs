// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html

#[exposed = (Window, Worker)]
pub trait IDBKeyRange {
  #[throws]
  lower: any,
  #[throws]
  upper: any,
  #[constant]
  #[alias = "lowerOpen"]
  lower_open: bool,
  #[constant]
  #[alias = "upperOpen"]
  upper_open: bool,
  #[throws]
  pub fn _includes(key: any) -> bool;

  #[new_object, throws]
  static IDBKeyRange only (value: any);
  #[new_object, throws]
  static IDBKeyRange lowerBound (lower: any, #[optional = false] open: bool);
  #[new_object, throws]
  static IDBKeyRange upperBound (upper: any, #[optional = false] open: bool);
  #[new_object, throws]
  static IDBKeyRange bound (lower: any, lower: any, #[optional = false] lowerOpen: bool, #[optional = false] upperOpen: bool);
}

#[exposed = (Window, Worker),
 func = "mozilla::dom::IndexedDatabaseManager::ExperimentalFeaturesEnabled"]
pub trait IDBLocaleAwareKeyRange: IDBKeyRange {
  #[new_object, throws]
  static IDBLocaleAwareKeyRange bound (lower: any, lower: any, #[optional = false] lowerOpen: bool, #[optional = false] upperOpen: bool);
}
