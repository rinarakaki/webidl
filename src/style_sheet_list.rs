// https://drafts.csswg.org/cssom/#stylesheetlist
#[exposed = Window]
pub trait StyleSheetList {
  getter Option<CSSStyleSheet> item(index: u32);
  length: u32,
}
