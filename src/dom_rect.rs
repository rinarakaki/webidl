// The origin of this IDL file is
// https://drafts.fxtf.org/geometry/

#[exposed = (Window, Worker),
 Serializable]
pub trait DOMRect: DOMRectReadOnly {
  constructor(#[optional = 0] f64: unrestricted x,
  #[optional = 0] f64: unrestricted y,
  #[optional = 0] f64: unrestricted width,
  #[optional = 0] f64: unrestricted height);

  #[new_object]
  static DOMRect fromRect(#[optional = {}] other: DOMRectInit);

  inherit attribute unrestricted f64 x;
  inherit attribute unrestricted f64 y;
  inherit attribute unrestricted f64 width;
  inherit attribute unrestricted f64 height;
}

#[ProbablyShortLivingWrapper,
 exposed = (Window, Worker),
 Serializable]
pub trait DOMRectReadOnly {
  constructor(#[optional = 0] f64: unrestricted x,
  #[optional = 0] f64: unrestricted y,
  #[optional = 0] f64: unrestricted width,
  #[optional = 0] f64: unrestricted height);

  #[new_object]
  static DOMRectReadOnly fromRect(#[optional = {}] other: DOMRectInit);

  readonly attribute unrestricted f64 x;
  readonly attribute unrestricted f64 y;
  readonly attribute unrestricted f64 width;
  readonly attribute unrestricted f64 height;
  readonly attribute unrestricted f64 top;
  readonly attribute unrestricted f64 right;
  readonly attribute unrestricted f64 bottom;
  readonly attribute unrestricted f64 left;

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}

pub struct DOMRectInit {
  unrestricted f64 x = 0;
  unrestricted f64 y = 0;
  unrestricted f64 width = 0;
  unrestricted f64 height = 0;
}
