// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-head-element

// http://www.whatwg.org/specs/web-apps/current-work/#the-head-element
#[exposed = Window]
pub trait HTMLHeadElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;
}

