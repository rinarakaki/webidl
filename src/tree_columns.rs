#[chrome_only, exposed = Window]
pub trait TreeColumns {
  // The tree widget for these columns.
  tree: Option<XULTreeElement>,

  // The number of columns.
  count: u32,

  // An alias for count (for the benefit of scripts which treat this as an
  // array).
  length: u32,

  // Get the first/last column.
  #[alias = "getFirstColumn"]
  pub fn get_first_column() -> Option<TreeColumn>;
  #[alias = "getLastColumn"]
  pub fn get_last_column() -> Option<TreeColumn>;

  // Attribute based column getters.
  #[alias = "getPrimaryColumn"]
  pub fn get_primary_column() -> Option<TreeColumn>;
  #[alias = "getSortedColumn"]
  pub fn get_sorted_column() -> Option<TreeColumn>;
  #[alias = "getKeyColumn"]
  pub fn get_key_column() -> Option<TreeColumn>;

  // Get the column for the given element.
  #[alias = "getColumnFor"]
  pub fn get_column_for(element: Option<Element>) -> Option<TreeColumn>;

  // Parametric column getters.
  getter Option<TreeColumn> getNamedColumn(name: DOMString);
  getter Option<TreeColumn> getColumnAt(index: u32);

  // This method is called whenever a treecol is added or removed and
  // the column cache needs to be rebuilt.
  #[alias = "invalidateColumns"]
  pub fn invalidate_columns();
}
