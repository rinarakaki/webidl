// The origin of this IDL file is
// http://dev.w3.org/2006/webapi/DOM-Level-3-Events/html/DOM3-Events.html

#[exposed = Window]
pub trait MutationEvent: Event
{
  const u16 MODIFICATION = 1;
  const u16 ADDITION = 2;
  const u16 REMOVAL = 3;
  #[chrome_only]
  const u16 SMIL = 4;

  #[alias = "relatedNode"]
  related_node: Option<Node>,
  #[alias = "prevValue"]
  prev_value: DOMString,
  #[alias = "newValue"]
  new_value: DOMString,
  #[alias = "attrName"]
  attr_name: DOMString,
  #[alias = "attrChange"]
  attr_change: u16,

  #[throws]
  void initMutationEvent(type: DOMString,
  #[optional = false] canBubble: bool,
  #[optional = false] cancelable: bool,
  optional Option<Node> relatedNode = None,
  #[optional = ""] prevValue: DOMString,
  #[optional = ""] newValue: DOMString,
  #[optional = ""] attrName: DOMString,
  #[optional = 0] attrChange: u16);
}
