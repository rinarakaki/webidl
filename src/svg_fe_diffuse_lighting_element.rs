// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEDiffuseLightingElement: SVGElement {
  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  #[alias = "surfaceScale"]
  surface_scale: SVGAnimatedNumber,
  #[constant]
  #[alias = "diffuseConstant"]
  diffuse_constant: SVGAnimatedNumber,
  #[constant]
  #[alias = "kernelUnitLengthX"]
  kernel_unit_length_x: SVGAnimatedNumber,
  #[constant]
  #[alias = "kernelUnitLengthY"]
  kernel_unit_length_y: SVGAnimatedNumber,
}

SVGFEDiffuseLightingElement includes SVGFilterPrimitiveStandardAttributes;
