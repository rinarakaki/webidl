// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-history-trait

pub enum ScrollRestoration {
  #[alias = "auto"]
  Auto,
  #[alias = "manual"]
  Manual
}

#[exposed = Window]
pub trait History {
  #[throws]
  length: u32,
  #[throws]
  #[alias = "scrollRestoration"]
  pub scroll_restoration: ScrollRestoration,
  #[throws]
  state: any,

  #[needs_subject_principal]
  pub fn go(#[optional = 0] delta: i32) -> Result<()>;

  #[needs_caller_type]
  pub fn back() -> Result<()>;

  #[needs_caller_type]
  pub fn forward() -> Result<()>;

  #[needs_caller_type]
  #[alias = "pushState"]
  pub fn push_state(data: any, data: any, optional Option<DOMString> url = None)
    -> Result<()>;

  #[needs_caller_type]
  #[alias = "replaceState"]
  pub fn replace_state(data: any, data: any, optional Option<DOMString> url = None)
    -> Result<()> ;
}
