// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBCursorDirection

pub super::{IDBIndex, IDBObjectStore};

pub enum IDBCursorDirection {
  #[alias = "next"]
  Next,
  #[alias = "nextunique"]
  NextUnique,
  #[alias = "prev"]
  Prev,
  #[alias = "prevunique"]
  PrevUnique
}

#[exposed = (Window, Worker)]
pub trait IDBCursor {
  readonly source: (IDBObjectStore or IDBIndex);

  #[binary_name = "getDirection"]
  direction: IDBCursorDirection,

  #[throws]
  key: any,

  #[throws]
  #[alias = "primaryKey"]
  primary_key: any,

  request: IDBRequest,

  pub fn update(value: any) -> Result<IDBRequest>;

  pub fn advance([EnforceRange] u32 count) -> Result<()>;

  pub fn continue(optional key: any) -> Result<()>;

  #[alias = "continuePrimaryKey"]
  pub fn continue_primary_key(key: any, key: any) -> Result<()>;

  pub fn delete() -> Result<IDBRequest>;
}

#[exposed = (Window, Worker)]
pub trait IDBCursorWithValue: IDBCursor {
  #[throws]
  value: any,
}
