// This IDL file is related to the WebExtensions browser.runtime's Port.
//
// You are granted a license to use, reproduce and create derivative works of
// this document.

#[exposed = (ServiceWorker), LegacyNoInterfaceObject]
pub trait ExtensionPort {
  #[replaceable]
  name: DOMString,
  #[replaceable]
  sender: any,
  #[replaceable]
  error: any,

  #[throws, WebExtensionStub = "NoReturn"]
  pub fn disconnect();
  #[throws, WebExtensionStub = "NoReturn"]
  #[alias = "postMessage"]
  pub fn post_message(message: any);

  #[replaceable, same_object]
  #[alias = "onDisconnect"]
  on_disconnect: ExtensionEventManager,
  #[replaceable, same_object]
  #[alias = "onMessage"]
  on_message: ExtensionEventManager,
}

// Dictionary used by ExtensionAPIRequestForwarder and ExtensionCallabck to receive from the
// mozIExtensionAPIRequestHandler an internal description of a runtime.Port (and then used in
// the webidl implementation to create an ExtensionPort instance).
#[GenerateInit]
pub struct ExtensionPortDescriptor {
  required DOMString portId;
  pub name: DOMString = "",
}
