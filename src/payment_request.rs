// The origin of this WebIDL file is
//   https://w3c.github.io/payment-request/#paymentrequest-trait
//   https://w3c.github.io/payment-request/#idl-index

pub struct PaymentMethodData {
  required DOMString supportedMethods;
  pub data: object,
}

pub struct PaymentCurrencyAmount {
  required DOMString currency;
  required DOMString value;
}

pub struct PaymentItem {
  required DOMString label;
  required PaymentCurrencyAmount amount;
  pub pending: bool = false,
}

pub struct PaymentShippingOption {
  required DOMString id;
  required DOMString label;
  required PaymentCurrencyAmount amount;
  pub selected: bool = false,
}

pub struct PaymentDetailsModifier {
  required DOMString supportedMethods;
  pub total: PaymentItem,
  #[alias = "additionalDisplayItems"]
  pub additional_display_items: Vec<PaymentItem>,
  pub data: object,
}

pub struct PaymentDetailsBase {
  #[alias = "displayItems"]
  pub display_items: Vec<PaymentItem>,
  #[alias = "shippingOptions"]
  pub shipping_options: Vec<PaymentShippingOption>,
  pub modifiers: Vec<PaymentDetailsModifier>,
}

pub struct PaymentDetailsInit: PaymentDetailsBase {
  pub id: DOMString,
  required PaymentItem total;
}

#[GenerateInitFromJSON, GenerateToJSON]
pub struct AddressErrors {
  #[alias = "addressLine"]
  pub address_line: DOMString,
  pub city: DOMString,
  pub country: DOMString,
  #[alias = "dependentLocality"]
  pub dependent_locality: DOMString,
  pub organization: DOMString,
  pub phone: DOMString,
  #[alias = "postalCode"]
  pub postal_code: DOMString,
  pub recipient: DOMString,
  pub region: DOMString,
  #[alias = "regionCode"]
  pub region_code: DOMString,
  #[alias = "sortingCode"]
  pub sorting_code: DOMString,
}

pub struct PaymentValidationErrors {
  pub payer: PayerErrors,
  #[alias = "shippingAddress"]
  pub shipping_address: AddressErrors,
  pub error: DOMString,
  #[alias = "paymentMethod"]
  pub payment_method: object,
}

#[GenerateInitFromJSON, GenerateToJSON]
pub struct PayerErrors {
  pub email: DOMString,
  pub name: DOMString,
  pub phone: DOMString,
}

pub struct PaymentDetailsUpdate: PaymentDetailsBase {
  pub error: DOMString,
  #[alias = "shippingAddressErrors"]
  pub shipping_address_errors: AddressErrors,
  #[alias = "payerErrors"]
  pub payer_errors: PayerErrors,
  #[alias = "paymentMethodErrors"]
  pub payment_method_errors: object,
  pub total: PaymentItem,
}

pub enum PaymentShippingType {
  #[alias = "shipping"]
  Shipping,
  #[alias = "delivery"]
  Delivery,
  #[alias = "pickup"]
  Pickup
}

pub struct PaymentOptions {
  #[alias = "requestPayerName"]
  pub request_payer_name: bool = false,
  #[alias = "requestPayerEmail"]
  pub request_payer_email: bool = false,
  #[alias = "requestPayerPhone"]
  pub request_payer_phone: bool = false,
  #[alias = "requestShipping"]
  pub request_shipping: bool = false,
  #[alias = "requestBillingAddress"]
  pub request_billing_address: bool = false,
  #[alias = "shippingType"]
  pub shipping_type: PaymentShippingType = "shipping",
}

#[SecureContext,
 func = "mozilla::dom::PaymentRequest::PrefEnabled",
 exposed = Window]
pub trait PaymentRequest: EventTarget {
  #[throws]
  constructor(methodData: Vec<PaymentMethodData>,
  details: PaymentDetailsInit,
  #[optional = {}] options: PaymentOptions);

  #[new_object]
  pub fn show(optional Promise<PaymentDetailsUpdate> detailsPromise) -> Promise<PaymentResponse>;
  #[new_object]
  pub fn abort() -> Promise<void>;
  #[new_object]
  #[alias = "canMakePayment"]
  pub fn can_make_payment() -> Promise<bool>;

  id: DOMString,
  #[alias = "shippingAddress"]
  shipping_address: Option<PaymentAddress>,
  #[alias = "shippingOption"]
  shipping_option: Option<DOMString>,
  #[alias = "shippingType"]
  shipping_type: Option<PaymentShippingType>,

  #[alias = "onmerchantvalidation"]
  pub onmerchantvalidation: EventHandler,
  #[alias = "onshippingaddresschange"]
  pub onshippingaddresschange: EventHandler,
  #[alias = "onshippingoptionchange"]
  pub onshippingoptionchange: EventHandler,
  #[alias = "onpaymentmethodchange"]
  pub onpaymentmethodchange: EventHandler,
}
