// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEGaussianBlurElement: SVGElement {
  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  #[alias = "stdDeviationX"]
  std_deviation_x: SVGAnimatedNumber,
  #[constant]
  #[alias = "stdDeviationY"]
  std_deviation_y: SVGAnimatedNumber,

  #[alias = "setStdDeviation"]
  pub fn set_std_deviation(stdDeviationX: f32, stdDeviationX: f32);
}

SVGFEGaussianBlurElement includes SVGFilterPrimitiveStandardAttributes;
