#[exposed = Window]
pub trait ScrollAreaEvent: UIEvent {
  x: f32,
  y: f32,
  width: f32,
  height: f32,

  void initScrollAreaEvent(type: DOMString,
  #[optional = false] canBubble: bool,
  #[optional = false] cancelable: bool,
  optional Option<Window> view = None,
  #[optional = 0] detail: i32,
  #[optional = 0] x: f32,
  #[optional = 0] y: f32,
  #[optional = 0] width: f32,
  #[optional = 0] height: f32);
}
