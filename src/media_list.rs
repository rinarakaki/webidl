// https://drafts.csswg.org/cssom/#the-medialist-trait

#[exposed = Window]
pub trait MediaList {
  stringifier attribute #[LegacyNullToEmptyString] UTF8String mediaText;

  length: u32,
  getter Option<UTF8String> item(index: u32);
  #[throws]
  #[alias = "deleteMedium"]
  pub fn delete_medium(oldMedium: UTF8String);
  #[throws]
  #[alias = "appendMedium"]
  pub fn append_medium(newMedium: UTF8String);
}
