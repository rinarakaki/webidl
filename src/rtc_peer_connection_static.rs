//  now: Right, it is not possible to add static functions to a JS implemented
//  trait (see bug 863952), so we need to create a simple trait with a
//  trivial constructor and no data to hold these functions that really ought to
//  be static in RTCPeerConnection.
//  TODO(bcampen@mozilla.com) Merge this code into RTCPeerConnection once this
//  limitation is gone. (1017082: Bug)

pub enum RTCLifecycleEvent {
  #[alias = "initialized"]
  Initialised,
  #[alias = "icegatheringstatechange"]
  IceGatheringStateChange,
  #[alias = "iceconnectionstatechange"]
  IceConnectionStateChange
}

pub type PeerConnectionLifecycleCallback =
  Fn(pc: RTCPeerConnection,
    u64 windowId,
    RTCLifecycleEvent eventType);

#[chrome_only,
  pref = "media.peerconnection.enabled",
  JSImplementation = "@mozilla.org/dom/peerconnectionstatic;1",
  exposed = Window]
pub trait RTCPeerConnectionStatic {
  #[alias = "constructor"]
  pub fn new() -> Result<Self>;

  // One slot per window (the window in which the register call is made),
  // automatically unregistered when window goes away.
  // Fires when a PC is created, and whenever the ICE connection state or
  // gathering state changes.
  #[alias = "registerPeerConnectionLifecycleCallback"]
  pub fn register_peer_connection_lifecycle_callback(
    cb: PeerConnectionLifecycleCallback);
}

