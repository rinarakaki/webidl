// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEOffsetElement: SVGElement {
  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  dx: SVGAnimatedNumber,
  #[constant]
  dy: SVGAnimatedNumber,
}

SVGFEOffsetElement includes SVGFilterPrimitiveStandardAttributes;
