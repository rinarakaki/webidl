// The origin of this IDL file is
// https://dom.spec.whatwg.org/#staticrange

#[exposed = Window]
pub trait StaticRange: AbstractRange {
  #[alias = "constructor"]
  pub fn new(init: StaticRangeInit) -> Result<Self, Error>;
  // And no additional functions/properties.
}

pub struct StaticRangeInit {
  required Node startContainer;
  required u32 startOffset;
  required Node endContainer;
  required u32 endOffset;
}
