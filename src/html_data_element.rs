// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/multipage/text-level-semantics.html#the-data-element

#[exposed = Window]
pub trait HTMLDataElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub value: DOMString,
}
