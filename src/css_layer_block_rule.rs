// The origin of this IDL file is
// https://drafts.csswg.org/css-cascade-5/#the-csslayerblockrule-trait

use super::CSSGroupingRule;

#[exposed = Window, pref = "layout.css.cascade-layers.enabled"]
pub trait CSSLayerBlockRule: CSSGroupingRule {
  name: UTF8String,
}
