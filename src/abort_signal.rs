// The origin of this IDL file is
// https://dom.spec.whatwg.org/#abortsignal

use super::*;

#[exposed = (Window, Worker)]
pub trait AbortSignal: EventTarget {
    #[new_object]
    pub fn abort(#[optional] reason: any) -> Result<AbortSignal>;

    pub aborted: bool,
    pub reason: any,

    #[alias = "throwIfAborted"]
    pub fn throw_if_aborted(&self) -> Result<()>;

    #[alias = "onabort"]
    pub mut on_abort: EventHandler,
}
