// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/getusermedia.html

pub enum MediaDeviceKind {
  #[alias = "audioinput"]
  AudioInput,
  #[alias = "audiooutput"]
  AudioOutput,
  #[alias = "videoinput"]
  VideoInput
}

#[func = "Navigator::HasUserMediaSupport",
  exposed = Window]
pub trait MediaDeviceInfo {
  #[alias = "deviceId"]
  device_id: DOMString,
  kind: MediaDeviceKind,
  label: DOMString,
  #[alias = "groupId"]
  group_id: DOMString,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
