// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-blockquote-element

// http://www.whatwg.org/specs/web-apps/current-work/#the-blockquote-element
#[exposed = Window]
pub trait HTMLQuoteElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub cite: DOMString,
}

