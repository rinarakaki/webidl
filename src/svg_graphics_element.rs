// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

pub struct SVGBoundingBoxOptions {
  pub fill: bool = true,
  pub stroke: bool = false,
  pub markers: bool = false,
  pub clipped: bool = false,
}

#[exposed = Window]
pub trait SVGGraphicsElement: SVGElement {
  #[pure]
  pub autofocus: bool,

  transform: SVGAnimatedTransformList,

  #[alias = "nearestViewportElement"]
  nearest_viewport_element: Option<SVGElement>,
  #[alias = "farthestViewportElement"]
  farthest_viewport_element: Option<SVGElement>,

  #[new_object]
  #[alias = "getBBox"]
  pub fn get_b_box(#[optional = {}] aOptions: SVGBoundingBoxOptions) -> SVGRect;
  // Not implemented
  // SVGRect getStrokeBBox();
  #[alias = "getCTM"]
  pub fn get_ctm() -> Option<SVGMatrix>;
  #[alias = "getScreenCTM"]
  pub fn get_screen_ctm() -> Option<SVGMatrix>;
  #[throws]
  #[alias = "getTransformToElement"]
  pub fn get_transform_to_element(element: SVGGraphicsElement) -> SVGMatrix;
}

SVGGraphicsElement includes SVGTests;
