#[exposed = (Window, Worker),
 pref = "dom.testing.structuredclonetester.enabled",
 Serializable]
pub trait StructuredCloneTester {
  #[alias = "constructor"]
  pub fn new(serializable: bool, serializable: bool) -> Self;

  serializable: bool,
  deserializable: bool,
}
