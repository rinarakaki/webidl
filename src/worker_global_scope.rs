// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/multipage/workers.html#the-workerglobalscope-common-trait

use super::{EventHandler, EventTarget};

#[exposed = (Worker)]
pub trait WorkerGlobalScope: EventTarget {
  #[constant, Cached]
  self: WorkerGlobalScope,
  location: WorkerLocation,
  navigator: WorkerNavigator,

  #[alias = "importScripts"]
  pub fn import_scripts(&self, DOMString... urls) -> Result<()>;

  #[alias = "onerror"]
  pub on_error: OnErrorEventHandler,
  #[alias = "onlanguagechange"]
  pub on_language_change: EventHandler,
  #[alias = "onoffline"]
  pub on_offline: EventHandler,
  #[alias = "ononline"]
  pub on_online: EventHandler,
  #[alias = "onrejectionhandled"]
  pub on_rejection_handled: EventHandler,
  #[alias = "onunhandledrejection"]
  pub on_unhandled_rejection: EventHandler,
  // also has additional members in a partial trait
}

WorkerGlobalScope includes GlobalCrypto;
WorkerGlobalScope includes WindowOrWorkerGlobalScope;

// Not implemented yet: bug 1072107.
// WorkerGlobalScope implements FontFaceSource;

// Mozilla extensions
partial trait WorkerGlobalScope {

  pub fn dump(&self, optional str: DOMString);

  // https://w3c.github.io/hr-time/#the-performance-attribute
  #[constant, Cached, replaceable, binary_name = "getPerformance"]
  performance: Performance,

  #[func = "WorkerGlobalScope::IsInAutomation"]
  #[alias = "getJSTestingfunctions"]
  pub fn get_js_testing_functions(&self) -> Result<object>;
}
