// The origin of this IDL file is
// https://immersive-web.github.io/webxr/

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRSystem: EventTarget {
  // Methods
  #[throws]
  #[alias = "isSessionSupported"]
  pub fn is_session_supported(mode: XRSessionMode) -> Promise<bool>;
  #[new_object, needs_caller_type]
  #[alias = "requestSession"]
  pub fn request_session(mode: XRSessionMode, #[optional = {}] options: XRSessionInit) -> Promise<XRSession>;

  // Events
  #[alias = "ondevicechange"]
  pub ondevicechange: EventHandler,
}

pub enum XRSessionMode {
  #[alias = "inline"]
  Inline,
  #[alias = "immersive-vr"]
  ImmersiveVR,
  #[alias = "immersive-ar"]
  ImmersiveAR,
}

pub struct XRSessionInit {
  #[alias = "requiredFeatures"]
  pub required_features: Vec<any>,
  #[alias = "optionalFeatures"]
  pub optional_features: Vec<any>,
}

pub enum XRVisibilityState {
  #[alias = "visible"]
  Visible,
  #[alias = "visible-blurred"]
  VisibleBlurred,
  #[alias = "hidden"]
  Hidden,
}

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRSession: EventTarget {
  // Attributes
  #[alias = "visibilityState"]
  visibility_state: XRVisibilityState,
  #[same_object]
  #[alias = "renderState"]
  render_state: XRRenderState,
  #[same_object]
  #[alias = "inputSources"]
  input_sources: XRInputSourceArray,

  // Methods
  #[throws]
  #[alias = "updateRenderState"]
  pub fn update_render_state(#[optional = {}] state: XRRenderStateInit);
  #[new_object]
  #[alias = "requestReferenceSpace"]
  pub fn request_reference_space(type: XRReferenceSpaceType) -> Promise<XRReferenceSpace>;

  #[throws]
  #[alias = "requestAnimationFrame"]
  pub fn request_animation_frame(callback: XRFrameRequestCallback) -> i32;
  #[throws]
  #[alias = "cancelAnimationFrame"]
  pub fn cancel_animation_frame(handle: i32);

  #[throws]
  pub fn end() -> Promise<void>;

  // Events
  #[alias = "onend"]
  pub onend: EventHandler,
  #[alias = "oninputsourceschange"]
  pub oninputsourceschange: EventHandler,
  #[alias = "onselect"]
  pub onselect: EventHandler,
  #[alias = "onselectstart"]
  pub onselectstart: EventHandler,
  #[alias = "onselectend"]
  pub onselectend: EventHandler,
  #[alias = "onsqueeze"]
  pub onsqueeze: EventHandler,
  #[alias = "onsqueezestart"]
  pub onsqueezestart: EventHandler,
  #[alias = "onsqueezeend"]
  pub onsqueezeend: EventHandler,
  #[alias = "onvisibilitychange"]
  pub onvisibilitychange: EventHandler,
}

pub struct XRRenderStateInit {
  #[alias = "depthNear"]
  pub depth_near: f64,
  #[alias = "depthFar"]
  pub depth_far: f64,
  #[alias = "inlineVerticalFieldOfView"]
  pub inline_vertical_fieldOfView: f64,
  #[alias = "baseLayer"]
  pub base_layer: Option<XRWebGLLayer>,
}

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRRenderState {
  #[alias = "depthNear"]
  depth_near: f64,
  #[alias = "depthFar"]
  depth_far: f64,
  #[alias = "inlineVerticalFieldOfView"]
  inline_vertical_field_of_view: Option<f64>,
  #[alias = "baseLayer"]
  base_layer: Option<XRWebGLLayer>,
}

callback XRFrameRequestCallback = void (time: DOMHighResTimeStamp, time: DOMHighResTimeStamp);

#[ProbablyShortLivingWrapper, pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRFrame {
  #[same_object]
  session: XRSession,

  #[throws]
  #[alias = "getViewerPose"]
  pub fn get_viewer_pose(referenceSpace: XRReferenceSpace) -> Option<XRViewerPose>;
  #[throws]
  #[alias = "getPose"]
  pub fn get_pose(space: XRSpace, space: XRSpace) -> Option<XRPose>;
}

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRSpace: EventTarget {

}

pub enum XRReferenceSpaceType {
  #[alias = "viewer"]
  Viewer,
  #[alias = "local"]
  Local,
  #[alias = "local-floor"]
  LocalFloor,
  #[alias = "bounded-floor"]
  BoundedFloor,
  #[alias = "unbounded"]
  Unbounded
}

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRReferenceSpace: XRSpace {
  #[new_object]
  #[alias = "getOffsetReferenceSpace"]
  pub fn get_offset_reference_space(originOffset: XRRigidTransform) -> XRReferenceSpace;

  #[alias = "onreset"]
  pub onreset: EventHandler,
}

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRBoundedReferenceSpace: XRReferenceSpace {
  // TODO: Use FrozenArray once available. (1236777: Bug)
  #[Frozen, Cached, pure]
  #[alias = "boundsGeometry"]
  bounds_geometry: Vec<DOMPointReadOnly>,
}

pub enum XREye {
  #[alias = "none"]
  None,
  #[alias = "left"]
  Left,
  #[alias = "right"]
  Right
}

#[ProbablyShortLivingWrapper, pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRView {
  eye: XREye,
  #[throws]
  #[alias = "projectionMatrix"]
  projection_matrix: F32Array,
  #[throws, same_object]
  transform: XRRigidTransform,
}

#[ProbablyShortLivingWrapper, pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRViewport {
  x: i32,
  y: i32,
  width: i32,
  height: i32,
}

#[ProbablyShortLivingWrapper, pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRRigidTransform {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] position: DOMPointInit, #[optional = {}] orientation: DOMPointInit) -> Result<Self, Error>;
  #[same_object]
  position: DOMPointReadOnly,
  #[same_object]
  orientation: DOMPointReadOnly,
  #[throws]
  matrix: F32Array,
  #[same_object]
  inverse: XRRigidTransform,
}

#[ProbablyShortLivingWrapper, pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRPose {
  #[same_object]
  transform: XRRigidTransform,
  #[alias = "emulatedPosition"]
  emulated_position: bool,
}

#[ProbablyShortLivingWrapper, pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRViewerPose: XRPose {
  // TODO: Use FrozenArray once available. (1236777: Bug)
  #[constant, Cached, Frozen]
  views: Vec<XRView>,
}

pub enum XRHandedness {
  #[alias = "none"]
  None,
  #[alias = "left"]
  Left,
  #[alias = "right"]
  Right
}

pub enum XRTargetRayMode {
  #[alias = "gaze"]
  Gaze,
  #[alias = "tracked-pointer"]
  TrackedPointer,
  #[alias = "screen"]
  Screen
}

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRInputSource {
  handedness: XRHandedness,
  #[alias = "targetRayMode"]
  target_ray_mode: XRTargetRayMode,
  #[same_object]
  #[alias = "targetRaySpace"]
  target_ray_space: XRSpace,
  #[same_object]
  #[alias = "gripSpace"]
  grip_space: Option<XRSpace>,
  // TODO: Use FrozenArray once available. (1236777: Bug)
  #[constant, Cached, Frozen]
  profiles: Vec<DOMString>,
  // https://immersive-web.github.io/webxr-gamepads-module/
  #[same_object]
  gamepad: Option<Gamepad>,
}

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRInputSourceArray {
  iterable<XRInputSource>;
  length: u32,
  pub fn XRInputSource(index: u32) -> getter;
}

typedef (WebGLRenderingContext or
  WebGL2RenderingContext) XRWebGLRenderingContext;

pub struct XRWebGLLayerInit {
  pub antialias: bool = true,
  pub depth: bool = true,
  pub stencil: bool = false,
  pub alpha: bool = true,
  #[alias = "ignoreDepthValues"]
  pub ignore_depth_values: bool = false,
  #[alias = "framebufferScaleFactor"]
  pub framebuffer_scale_factor: f64 = 1.0,
}

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRWebGLLayer {
  #[throws]
  constructor(session: XRSession,
    context: XRWebGLRenderingContext,
    #[optional = {}] layerInit: XRWebGLLayerInit);

  // Attributes
  antialias: bool,
  #[alias = "ignoreDepthValues"]
  ignore_depth_values: bool,

  #[same_object]
  framebuffer: Option<WebGLFramebuffer>,
  #[alias = "framebufferWidth"]
  framebuffer_width: u32,
  #[alias = "framebufferHeight"]
  framebuffer_height: u32,

  // Methods
  #[alias = "getViewport"]
  pub fn get_viewport(view: XRView) -> Option<XRViewport>;

  // Static Methods
  static f64 getNativeFramebufferScaleFactor(session: XRSession);
}
