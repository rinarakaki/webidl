// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/webrtc.html#rtcstatsreport-object
// http://www.w3.org/2011/04/webrtc/wiki/Stats

pub enum RTCStatsType {
  #[alias = "inbound-rtp"]
  InboundRTP,
  #[alias = "outbound-rtp"]
  OutboundRTP,
  #[alias = "remote-inbound-rtp"]
  RemoteInboundRTP,
  #[alias = "remote-outbound-rtp"]
  RemoteOutboundRTP,
  #[alias = "csrc"]
  Csrc,  // TODO
  #[alias = "data-channel"]
  DataChannel,
  #[alias = "session"]
  Session,
  #[alias = "track"]
  Track,
  #[alias = "transport"]
  Transport,
  #[alias = "candidate-pair"]
  CandidatePair,
  #[alias = "local-candidate"]
  LocalCandidate,
  #[alias = "remote-candidate"]
  RemoteCandidate
}

pub struct RTCStats {
  pub timestamp: DOMHighResTimeStamp,
  pub type: RTCStatsType,
  pub id: DOMString,
}

pub struct RTCRtpStreamStats: RTCStats {
  pub ssrc: u32,
  #[alias = "mediaType"]
  pub media_type: DOMString,
  pub kind: DOMString,
  pub transportId: DOMString,
}

pub struct RTCReceivedRtpStreamStats: RTCRtpStreamStats {
  #[alias = "packetsReceived"]
  pub packets_received: u32,
  #[alias = "packetsLost"]
  pub packets_lost: u32,
  pub jitter: f64,
  #[alias = "discardedPackets"]
  pub discarded_packets: u32, // non-standard alias for packetsDiscarded
  #[alias = "packetsDiscarded"]
  pub packets_discarded: u32,
}

pub struct RTCInboundRtpStreamStats: RTCReceivedRtpStreamStats {
  pub remoteId: DOMString,
  #[alias = "framesDecoded"]
  pub frames_decoded: u32,
  #[alias = "bytesReceived"]
  pub bytes_received: u64,
  #[alias = "nackCount"]
  pub nack_count: u32,
  #[alias = "firCount"]
  pub fir_count: u32,
  #[alias = "pliCount"]
  pub pli_count: u32,
}

pub struct RTCRemoteInboundRtpStreamStats: RTCReceivedRtpStreamStats {
  pub localId: DOMString,
  #[alias = "roundTripTime"]
  pub round_trip_time: f64,
}

pub struct RTCSentRtpStreamStats: RTCRtpStreamStats {
  #[alias = "packetsSent"]
  pub packets_sent: u32,
  #[alias = "bytesSent"]
  pub bytes_sent: u64,
}

pub struct RTCOutboundRtpStreamStats: RTCSentRtpStreamStats {
  pub remoteId: DOMString,
  #[alias = "framesEncoded"]
  pub frames_encoded: u32,
  #[alias = "qpSum"]
  pub qp_sum: u64,
  #[alias = "nackCount"]
  pub nack_count: u32,
  #[alias = "firCount"]
  pub fir_count: u32,
  #[alias = "pliCount"]
  pub pli_count: u32,
}

pub struct RTCRemoteOutboundRtpStreamStats: RTCSentRtpStreamStats {
  pub localId: DOMString,
  #[alias = "remoteTimestamp"]
  pub remote_timestamp: DOMHighResTimeStamp,
}

pub struct RTCRTPContributingSourceStats: RTCStats {
  #[alias = "contributorSsrc"]
  pub contributor_ssrc: u32,
  #[alias = "inboundRtpStreamId"]
  pub inbound_rtp_streamId: DOMString,
}

pub struct RTCDataChannelStats: RTCStats {
  pub label: DOMString,
  pub protocol: DOMString,
  #[alias = "dataChannelIdentifier"]
  pub data_channel_identifier: i32,
  // RTCTransportId is not yet implemented - Bug 1225723
  // DOMString transportId;
  pub state: RTCDataChannelState,
  #[alias = "messagesSent"]
  pub messages_sent: u32,
  #[alias = "bytesSent"]
  pub bytes_sent: u64,
  #[alias = "messagesReceived"]
  pub messages_received: u32,
  #[alias = "bytesReceived"]
  pub bytes_received: u64,
}

pub enum RTCStatsIceCandidatePairState {
  #[alias = "frozen"]
  Frozen,
  #[alias = "waiting"]
  Waiting,
  #[alias = "inprogress"]
  InProgress,
  #[alias = "failed"]
  Failed,
  #[alias = "succeeded"]
  Succeeded,
  #[alias = "cancelled"]
  Cancelled
}

pub struct RTCIceCandidatePairStats: RTCStats {
  pub transportId: DOMString,
  #[alias = "localCandidateId"]
  pub local_candidate_id: DOMString,
  #[alias = "remoteCandidateId"]
  pub remote_candidate_id: DOMString,
  pub state: RTCStatsIceCandidatePairState,
  pub priority: u64,
  pub nominated: bool,
  pub writable: bool,
  pub readable: bool,
  #[alias = "bytesSent"]
  pub bytes_sent: u64,
  #[alias = "bytesReceived"]
  pub bytes_received: u64,
  #[alias = "lastPacketSentTimestamp"]
  pub last_packet_sentTimestamp: DOMHighResTimeStamp,
  #[alias = "lastPacketReceivedTimestamp"]
  pub last_packet_receivedTimestamp: DOMHighResTimeStamp,
  pub selected: bool,
  #[chrome_only]
  pub componentId: u32, // moz
}

pub enum RTCIceCandidateType {
  #[alias = "host"]
  Host,
  #[alias = "srflx"]
  Srflx,  // TODO
  #[alias = "prflx"]
  Prflx,  // TODO
  #[alias = "relay"]
  Relay
}

pub struct RTCIceCandidateStats: RTCStats {
  pub address: DOMString,
  pub port: i32,
  pub protocol: DOMString,
  #[alias = "candidateType"]
  pub candidate_type: RTCIceCandidateType,
  pub priority: i32,
  #[alias = "relayProtocol"]
  pub relay_protocol: DOMString,
  // Because we use this internally but don't support RTCIceCandidateStats,
  // we need to keep the field as chrome_only. Bug 1225723
  #[chrome_only]
  pub transportId: DOMString,
  #[chrome_only]
  pub proxied: DOMString,
}

// This is for tracking the frame rate in about:webrtc
pub struct RTCVideoFrameHistoryEntryInternal {
  required u32 width;
  required u32 height;
  required u32 rotationAngle;
  required DOMHighResTimeStamp firstFrameTimestamp;
  required DOMHighResTimeStamp lastFrameTimestamp;
  required u64 consecutiveFrames;
  required u32 localSsrc;
  required u32 remoteSsrc;
}

// Collection over the entries for a single track for about:webrtc
pub struct RTCVideoFrameHistoryInternal {
  required DOMString trackIdentifier;
  pub entries: Vec<RTCVideoFrameHistoryEntryInternal> = [],
}

// Collection over the libwebrtc bandwidth estimation stats
pub struct RTCBandwidthEstimationInternal {
  required DOMString trackIdentifier;
  #[alias = "sendBandwidthBps"]
  pub send_bandwidth_bps: i32, // Estimated available send bandwidth
  #[alias = "maxPaddingBps"]
  pub max_padding_bps: i32, // Cumulative configured max padding
  #[alias = "receiveBandwidthBps"]
  pub receive_bandwidth_bps: i32, // Estimated available receive bandwidth
  #[alias = "pacerDelayMs"]
  pub pacer_delay_ms: i32,
  pub rttMs: i32,
}

// This is used by about:webrtc to report SDP parsing errors
pub struct RTCSdpParsingErrorInternal {
  required u32 lineNumber;
  required DOMString error;
}

// This is for tracking the flow of SDP for about:webrtc
pub struct RTCSdpHistoryEntryInternal {
  required DOMHighResTimeStamp timestamp;
  required bool isLocal;
  required DOMString sdp;
  pub errors: Vec<RTCSdpParsingErrorInternal> = [],
}

// This is intended to be a list of dictionaries that inherit from RTCStats
// (with some raw ICE candidates thrown in). Unfortunately, we cannot simply
// store a Vec<RTCStats> because of slicing. So, we have to have a
// separate list for each type. Used in c++ gecko code.
pub struct RTCStatsCollection {
  #[alias = "inboundRtpStreamStats"]
  pub inbound_rtp_streamStats: Vec<RTCInboundRtpStreamStats> = [],
  #[alias = "outboundRtpStreamStats"]
  pub outbound_rtp_streamStats: Vec<RTCOutboundRtpStreamStats> = [],
  #[alias = "remoteInboundRtpStreamStats"]
  pub remote_inbound_rtpStreamStats: Vec<RTCRemoteInboundRtpStreamStats> = [],
  #[alias = "remoteOutboundRtpStreamStats"]
  pub remote_outbound_rtpStreamStats: Vec<RTCRemoteOutboundRtpStreamStats> = [],
  #[alias = "rtpContributingSourceStats"]
  pub rtp_contributing_sourceStats: Vec<RTCRTPContributingSourceStats> = [],
  #[alias = "iceCandidatePairStats"]
  pub ice_candidate_pairStats: Vec<RTCIceCandidatePairStats> = [],
  #[alias = "iceCandidateStats"]
  pub ice_candidate_stats: Vec<RTCIceCandidateStats> = [],
  #[alias = "trickledIceCandidateStats"]
  pub trickled_ice_candidateStats: Vec<RTCIceCandidateStats> = [],
  #[alias = "rawLocalCandidates"]
  pub raw_local_candidates: Vec<DOMString> = [],
  #[alias = "rawRemoteCandidates"]
  pub raw_remote_candidates: Vec<DOMString> = [],
  #[alias = "dataChannelStats"]
  pub data_channel_stats: Vec<RTCDataChannelStats> = [],
  #[alias = "videoFrameHistories"]
  pub video_frame_histories: Vec<RTCVideoFrameHistoryInternal> = [],
  #[alias = "bandwidthEstimations"]
  pub bandwidth_estimations: Vec<RTCBandwidthEstimationInternal> = [],
}

// Details that about:webrtc can display about configured ICE servers
pub struct RTCIceServerInternal {
  pub urls: Vec<DOMString> = [],
  required bool credentialProvided;
  required bool userNameProvided;
}

// Details that about:webrtc can display about the RTCConfiguration
// Chrome only
pub struct RTCConfigurationInternal {
  #[alias = "bundlePolicy"]
  pub bundle_policy: RTCBundlePolicy,
  required bool certificatesProvided;
  #[alias = "iceServers"]
  pub ice_servers: Vec<RTCIceServerInternal> = [],
  #[alias = "iceTransportPolicy"]
  pub ice_transport_policy: RTCIceTransportPolicy,
  required bool peerIdentityProvided;
  #[alias = "sdpSemantics"]
  pub sdp_semantics: DOMString,
}

// A collection of RTCStats dictionaries, plus some other info. Used by
// WebrtcGlobalInformation for about:webrtc, and telemetry.
pub struct RTCStatsReportInternal: RTCStatsCollection {
  required DOMString pcid;
  required u32 browserId;
  pub configuration: RTCConfigurationInternal,
  #[alias = "jsepSessionErrors"]
  pub jsep_session_errors: DOMString,
  #[alias = "localSdp"]
  pub local_sdp: DOMString,
  #[alias = "remoteSdp"]
  pub remote_sdp: DOMString,
  #[alias = "sdpHistory"]
  pub sdp_history: Vec<RTCSdpHistoryEntryInternal> = [],
  required DOMHighResTimeStamp timestamp;
  #[alias = "callDurationMs"]
  pub call_duration_ms: f64,
  required u32 iceRestarts;
  required u32 iceRollbacks;
  pub offerer: bool, // Is the PC the offerer
  required bool closed; // Is the PC now closed
}

#[pref = "media.peerconnection.enabled",
 exposed = Window]
pub trait RTCStatsReport {

  // TODO(1586109: bug): Remove this once we no longer need to be able to
  // construct empty RTCStatsReports from JS.
  #[chrome_only]
  #[alias = "constructor"]
  pub fn new() -> Self;

  readonly maplike<DOMString, object>;
}
