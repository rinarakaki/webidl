// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGPoint {

  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,

  #[new_object, throws]
  #[alias = "matrixTransform"]
  pub fn matrix_transform(#[optional = {}] matrix: DOMMatrix2DInit) -> SVGPoint;
}

