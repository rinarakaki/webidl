// The origin of this IDL file is
// https://notifications.spec.whatwg.org/

#[exposed = (Window, Worker),
 func = "mozilla::dom::Notification::PrefEnabled"]
pub trait Notification: EventTarget {
  #[alias = "constructor"]
  pub fn new(title: DOMString, #[optional = {}] options: NotificationOptions)
    -> Result<Self>;

  #[getter_throws]
  static readonly permission: NotificationPermission;

  #[throws,
    func = "mozilla::dom::Notification::RequestPermissionEnabledForScope"]
  #[alias = "requestPermission"]
  static fn request_permission(
    optional permissionCallback: NotificationPermissionCallback)
    -> Promise<NotificationPermission>;

  #[throws, func = "mozilla::dom::Notification::IsGetEnabled"]
  static fn get(#[optional = {}] filter: GetNotificationOptions)
    -> Promise<Vec<Notification>>;

  #[alias = "onclick"]
  pub on_click: EventHandler,

  #[alias = "onshow"]
  pub on_show: EventHandler,

  #[alias = "onerror"]
  pub on_error: EventHandler,

  #[alias = "onclose"]
  pub on_close: EventHandler,

  #[pure]
  title: DOMString,

  #[pure]
  dir: NotificationDirection,

  #[pure]
  lang: Option<DOMString>,

  #[pure]
  body: Option<DOMString>,

  #[constant]
  tag: Option<DOMString>,

  #[pure]
  icon: Option<DOMString>,

  #[constant, pref = "dom.webnotifications.requireinteraction.enabled"]
  #[alias = "requireInteraction"]
  require_interaction: bool,

  #[constant, pref = "dom.webnotifications.silent.enabled"]
  silent: bool,

  #[Cached, Frozen, pure, pref = "dom.webnotifications.vibrate.enabled"]
  readonly vibrate: Vec<u32>;

  #[constant]
  data: any,

  pub fn close();
}

pub type VibratePattern = (u32 or Vec<u32>);

pub struct NotificationOptions {
  pub dir: NotificationDirection = "auto",
  pub lang: DOMString = "",
  pub body: DOMString = "",
  pub tag: DOMString = "",
  pub icon: DOMString = "",
  #[alias = "requireInteraction"]
  pub require_interaction: bool = false,
  pub silent: bool = false,
  pub vibrate: VibratePattern,
  pub data: any = None,
  pub mozbehavior: NotificationBehaviour = {},
}

pub struct GetNotificationOptions {
  pub tag: DOMString = "",
}

#[GenerateToJSON]
#[alias = "NotificationBehavior"]
pub struct NotificationBehaviour {
  #[alias = "noscreen"]
  pub no_screen: bool = false,
  #[alias = "noclear"]
  pub no_clear: bool = false,
  #[alias = "showOnlyOnce"]
  pub show_only_once: bool = false,
  #[alias = "soundFile"]
  pub sound_file: DOMString = "",
  #[alias = "vibrationPattern"]
  pub vibration_pattern: Vec<u32>,
}

pub enum NotificationPermission {
  #[alias = "default"]
  Default,
  #[alias = "denied"]
  Denied,
  #[alias = "granted"]
  Granted
}

callback NotificationPermissionCallback = void (permission: NotificationPermission);

pub enum NotificationDirection {
  #[alias = "auto"]
  Auto,
  #[alias = "ltr"]
  LtoR,
  #[alias = "rtl"]
  RtoL
}
