 TCPServerSocket
//
// An trait to a server socket that can accept incoming connections for gaia apps.

pub struct ServerSocketOptions {
  #[alias = "binaryType"]
  pub binary_type: TCPSocketBinaryType = "string",
}

#[func = "mozilla::dom::TCPSocket::ShouldTCPSocketExist",
 exposed = Window]
pub trait TCPServerSocket: EventTarget {
  #[throws]
  constructor(port: u16, #[optional = {}] options: ServerSocketOptions,
  #[optional = 0] backlog: u16);

  // The port of this server socket object.
  #[alias = "localPort"]
  local_port: u16,

  // The "connect" event is dispatched when a client connection is accepted.
  // The event object will be a TCPServerSocketEvent containing a TCPSocket
  // instance, which is used for communication between client and server.
  #[alias = "onconnect"]
  pub onconnect: EventHandler,

  // The "error" event will be dispatched when a listening server socket is
  // unexpectedly disconnected.
  #[alias = "onerror"]
  pub on_error: EventHandler,

  // Close the server socket.
  pub fn close();
}
