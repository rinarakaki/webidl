pub enum VRDisplayEventReason {
  #[alias = "mounted"]
  Mounted,
  #[alias = "navigation"]
  Navigation,
  #[alias = "requested"]
  Requested,
  #[alias = "unmounted"]
  Unmounted,
}

pub struct VRDisplayEventInit: EventInit {
  required VRDisplay display;
  pub reason: VRDisplayEventReason,
}

#[pref = "dom.vr.enabled",
  SecureContext,
  exposed = Window]
pub trait VRDisplayEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;

  display: VRDisplay,
  reason: Option<VRDisplayEventReason>,
}
