// The origin of this IDL file is
// https://drafts.csswg.org/css-animations/#trait-csskeyframesrule

use super::{CSSRule, CSSRuleList};

#[exposed = Window]
pub trait CSSKeyframesRule: CSSRule {
  pub name: DOMString,
  #[alias = "cssRules"]
  css_rules: CSSRuleList,

  #[alias = "appendRule"]
  pub fn append_rule(&self, rule: DOMString);
  #[alias = "deleteRule"]
  pub fn delete_rule(&self, select: DOMString);
  #[alias = "findRule"]
  pub fn find_rule(&self, select: DOMString) -> Option<CSSKeyframeRule>;
}
