// This file includes all the deprecated mozRTC prefixed interfaces.
//
// The declaration of each should match the declaration of the real, unprefixed
// trait. These aliases will be removed at some point (1155923: Bug).

#[deprecated = "WebrtcDeprecatedPrefix",
 pref = "media.peerconnection.enabled",
 JSImplementation = "@mozilla.org/dom/rtcicecandidate;1",
 exposed = Window]
pub trait mozRTCIceCandidate: RTCIceCandidate {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] candidateInitDict: RTCIceCandidateInit) -> Result<Self, Error>;
}

#[deprecated = "WebrtcDeprecatedPrefix",
 pref = "media.peerconnection.enabled",
 JSImplementation = "@mozilla.org/dom/peerconnection;1",
 exposed = Window]
pub trait mozRTCPeerConnection: RTCPeerConnection {
  #[throws]
  constructor(#[optional = {}] configuration: RTCConfiguration,
  optional Option<object> constraints);
}

#[deprecated = "WebrtcDeprecatedPrefix",
 pref = "media.peerconnection.enabled",
 JSImplementation = "@mozilla.org/dom/rtcsessiondescription;1",
 exposed = Window]
pub trait mozRTCSessionDescription: RTCSessionDescription {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] descriptionInitDict: RTCSessionDescriptionInit) -> Result<Self, Error>;
}
