// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGRectElement: SVGGeometryElement {
  #[constant]
  x: SVGAnimatedLength,
  #[constant]
  y: SVGAnimatedLength,
  #[constant]
  width: SVGAnimatedLength,
  #[constant]
  height: SVGAnimatedLength,
  #[constant]
  rx: SVGAnimatedLength,
  #[constant]
  ry: SVGAnimatedLength,
}

