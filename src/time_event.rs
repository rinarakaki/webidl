// For more information on this trait please see
// http://dev.w3.org/2006/webapi/DOM-Level-3-Events/html/DOM3-Events.html

#[exposed = Window]
pub trait TimeEvent: Event {
  detail: i32,
  view: Option<WindowProxy>,
  void initTimeEvent(aType: DOMString,
  optional Option<Window> aView = None,
  #[optional = 0] aDetail: i32);
}
