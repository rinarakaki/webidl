// The origin of this IDL file is
// https://w3c.github.io/mediasession/#idl-index

pub enum MediaSessionPlaybackState {
  #[alias = "none"]
  None,
  #[alias = "paused"]
  Paused,
  #[alias = "playing"]
  Playing
}

pub enum MediaSessionAction {
  #[alias = "play"]
  Play,
  #[alias = "pause"]
  Pause,
  #[alias = "seekbackward"]
  SeekBackward,
  #[alias = "seekforward"]
  SeekForward,
  #[alias = "previoustrack"]
  PreviousTrack,
  #[alias = "nexttrack"]
  NextTrack,
  #[alias = "skipad"]
  Skipad,  // TODO
  #[alias = "seekto"]
  Seekto,  // TODO
  #[alias = "stop"]
  Stop,
}

pub type MediaSessionActionHandler = Fn(details: MediaSessionActionDetails);

#[exposed = Window, pref = "dom.media.mediasession.enabled"]
pub trait MediaSession {
  pub metadata: Option<MediaMetadata>;

  #[alias = "playbackState"]
  pub playback_state: MediaSessionPlaybackState,

  #[alias = "setActionHandler"]
  pub fn set_action_handler(action: MediaSessionAction, action: MediaSessionAction);

  #[alias = "setPositionState"]
  pub fn set_position_state(#[optional = {}] state: MediaPositionState) -> Result<()>;

  // Fire the action handler. It's test-only for now.
  #[chrome_only]
  #[alias = "notifyHandler"]
  pub fn notify_handler(details: MediaSessionActionDetails);
}

#[exposed = Window, pref = "dom.media.mediasession.enabled"]
pub trait MediaMetadata {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] init: MediaMetadataInit) -> Result<Self>;

  pub title: DOMString,
  pub artist: DOMString,
  pub album: DOMString,
  // https://github.com/w3c/mediasession/issues/237
  // Take and return `MediaImage` on setter and getter.
  #[Frozen, Cached, pure, throws]
  attribute Vec<object> artwork;
}

pub struct MediaMetadataInit {
  pub title: DOMString = "",
  pub artist: DOMString = "",
  pub album: DOMString = "",
  pub artwork: Vec<MediaImage> = [],
}

pub struct MediaImage {
  required USVString src;
  pub sizes: DOMString = "",
  pub type: DOMString = "",
}

// Spec issue https://github.com/w3c/mediasession/issues/254
pub struct MediaSessionActionDetails {
  required MediaSessionAction action;
  #[alias = "seekOffset"]
  pub seek_offset: f64,
  #[alias = "seekTime"]
  pub seek_time: f64,
  #[alias = "fastSeek"]
  pub fast_seek: bool,
}

pub struct MediaPositionState {
  pub duration: f64,
  #[alias = "playbackRate"]
  pub playback_rate: f64,
  pub position: f64,
}
