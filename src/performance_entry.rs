// The origin of this IDL file is
// https://w3c.github.io/performance-timeline/#dom-performanceentry

#[exposed = (Window, Worker)]
pub trait PerformanceEntry {
  name: DOMString,
  #[alias = "entryType"]
  entry_type: DOMString,
  #[alias = "startTime"]
  start_time: DOMHighResTimeStamp,
  duration: DOMHighResTimeStamp,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
