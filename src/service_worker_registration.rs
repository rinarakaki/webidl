// The origin of this IDL file is
// http://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html
// https://w3c.github.io/push-api/
// https://notifications.spec.whatwg.org/

#[pref = "dom.serviceWorkers.enabled",
 exposed = (Window, Worker)]
pub trait ServiceWorkerRegistration: EventTarget {
  installing: Option<ServiceWorker>,
  waiting: Option<ServiceWorker>,
  active: Option<ServiceWorker>,

  #[pref = "dom.serviceWorkers.navigationPreload.enabled", same_object]
  #[alias = "navigationPreload"]
  navigation_preload: NavigationPreloadManager,

  scope: USVString,
  #[throws]
  #[alias = "updateViaCache"]
  update_via_cache: ServiceWorkerUpdateViaCache,

  #[throws, new_object]
  pub fn update() -> Promise<void>;

  #[throws, new_object]
  pub fn unregister() -> Promise<bool>;

  // event
  #[alias = "onupdatefound"]
  pub onupdatefound: EventHandler,
}

pub enum ServiceWorkerUpdateViaCache {
  #[alias = "imports"]
  Imports,
  #[alias = "all"]
  All,
  #[alias = "none"]
  None
}

// https://w3c.github.io/push-api/
partial trait ServiceWorkerRegistration {
  #[throws, exposed = (Window, Worker), pref = "dom.push.enabled"]
  #[alias = "pushManager"]
  push_manager: PushManager,
}

// https://notifications.spec.whatwg.org/
partial trait ServiceWorkerRegistration {
  #[throws, pref = "dom.webnotifications.serviceworker.enabled"]
  #[alias = "showNotification"]
  pub fn show_notification(title: DOMString, #[optional = {}] options: NotificationOptions) -> Promise<void>;
  #[throws, pref = "dom.webnotifications.serviceworker.enabled"]
  Promise<Vec<Notification>> getNotifications(#[optional = {}] filter: GetNotificationOptions);
}
