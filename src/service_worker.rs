// The origin of this IDL file is
// http://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html#service-worker-obj

// Still unclear what should be subclassed.
// https://github.com/slightlyoff/ServiceWorker/issues/189
#[func = "ServiceWorkerVisible",
 // FIXME(nsm): Bug 1113522. This is exposed to satisfy webidl constraints, but it won't actually work.
 exposed = (Window, Worker)]
pub trait ServiceWorker: EventTarget {
  #[alias = "scriptURL"]
  script_url: USVString,
  state: ServiceWorkerState,

  #[alias = "onstatechange"]
  pub onstatechange: EventHandler,

  #[alias = "postMessage"]
  pub fn post_message(message: any, message: any) -> Result<()>;

  #[alias = "postMessage"]
  pub fn post_message(
    message: any,
    #[optional = {}] options: StructuredSerializeOptions) -> Result<()>;
}

ServiceWorker includes AbstractWorker;

pub enum ServiceWorkerState {
  // https://github.com/w3c/ServiceWorker/issues/1162
  #[alias = "parsed"]
  Parsed,

  #[alias = "installing"]
  Installing,
  #[alias = "installed"]
  Installed,
  #[alias = "activating"]
  Activating,
  #[alias = "activated"]
  Activated,
  #[alias = "redundant"]
  Redundant
}
