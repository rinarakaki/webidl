// The origin of this IDL file is
// https://wiki.whatwg.org/wiki/OffscreenCanvas

// The new ImageBitmapRenderingContext is a canvas rendering context
// which only provides the functionality to replace the canvas's
// contents with the given ImageBitmap. Its context id (the first argument
// to getContext) is "bitmaprenderer".
#[exposed = (Window, Worker)]
pub trait ImageBitmapRenderingContext {
  canvas: Option<CanvasSource>,

  // Displays the given ImageBitmap in the canvas associated with this
  // rendering context. Ownership of the ImageBitmap is transferred to
  // the canvas. The caller may not use its reference to the ImageBitmap
  // after making this call. (This semantic is crucial to enable prompt
  // reclamation of expensive graphics resources, rather than relying on
  // garbage collection to do so.)
  //
  // The ImageBitmap conceptually replaces the canvas's bitmap, but
  // it does not change the canvas's intrinsic width or height.
  //
  // The ImageBitmap, displayed: when, is clipped to the rectangle
  // defined by the canvas's instrinsic width and height. Pixels that
  // would be covered by the canvas's bitmap which are not covered by
  // the supplied ImageBitmap are rendered transparent black. Any CSS
  // styles affecting the display of the canvas are applied as usual.
  #[alias = "transferFromImageBitmap"]
  pub fn transfer_from_image_bitmap(bitmap: Option<ImageBitmap>);

  // deprecated version of transferFromImageBitmap
  #[deprecated = "ImageBitmapRenderingContext_TransferImageBitmap"]
  #[alias = "transferImageBitmap"]
  pub fn transfer_image_bitmap(bitmap: ImageBitmap);
}
