// For more information on this trait please see
// http://dev.w3.org/2006/webapi/DOM-Level-3-Events/html/DOM3-Events.html

#[exposed = Window]
pub trait FocusEvent: UIEvent {
  constructor(typeArg: DOMString,
  #[optional = {}] focusEventInitDict: FocusEventInit);

  // Introduced in DOM Level 3:
  #[alias = "relatedTarget"]
  related_target: Option<EventTarget>,
}

pub struct FocusEventInit: UIEventInit {
  #[alias = "relatedTarget"]
  pub related_target: Option<EventTarget> = None,
}
