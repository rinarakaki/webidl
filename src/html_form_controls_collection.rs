// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#htmlformcontrolscollection

#[exposed = Window]
pub trait HTMLFormControlsCollection: HTMLCollection {
  // inherits length and item()
  // legacycaller */ getter (RadioNodeList or Element)? namedItem(name: DOMString); // shadows inherited namedItem()
}
