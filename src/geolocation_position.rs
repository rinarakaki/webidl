// The origin of this IDL file is
// http://www.w3.org/TR/geolocation-API

#[exposed = Window, SecureContext]
pub trait GeolocationPosition {
  coords: GeolocationCoordinates,
  timestamp: EpochTimeStamp,
}
