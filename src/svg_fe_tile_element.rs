// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFETileElement: SVGElement {
  #[constant]
  in1: SVGAnimatedString,
}

SVGFETileElement includes SVGFilterPrimitiveStandardAttributes;
