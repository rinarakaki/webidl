// The origin of this IDL file is
// https://dvcs.w3.org/hg/html-media/raw-file/default/encrypted-media/encrypted-media.html

#[exposed = Window]
pub trait MediaKeySession: EventTarget {
  // error state
  error: Option<MediaKeyError>,

  // session properties
  #[alias = "sessionId"]
  session_id: DOMString,

  readonly attribute unrestricted f64 expiration;

  closed: Promise<void>,

  #[alias = "keyStatuses"]
  key_statuses: MediaKeyStatusMap,

  #[alias = "onkeystatuseschange"]
  pub onkeystatuseschange: EventHandler,

  #[alias = "onmessage"]
  pub on_message: EventHandler,

  #[new_object]
  #[alias = "generateRequest"]
  pub fn generate_request(initDataType: DOMString, initDataType: DOMString) -> Promise<void>;

  #[new_object]
  pub fn load(sessionId: DOMString) -> Promise<bool>;

  // session operations
  #[new_object]
  pub fn update(response: BufferSource) -> Promise<void>;

  #[new_object]
  pub fn close() -> Promise<void>;

  #[new_object]
  pub fn remove() -> Promise<void>;
}
