#[exposed = (Window,Worker, Worklet),
//Transferable See Bug 1734240
]
pub trait ReadableStream {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] underlyingSource: object, optional strategy: QueuingStrategy) -> Result<Self, Error>;

  locked: bool,

  #[throws]
  pub fn cancel(optional reason: any) -> Promise<void>;

  #[throws]
  #[alias = "getReader"]
  pub fn get_reader(#[optional = {}] options: ReadableStreamGetReaderOptions) -> ReadableStreamReader;

  // Bug 1734243
  // ReadableStream pipeThrough(transform: ReadableWritablePair, #[optional = {}] options: StreamPipeOptions);

  // Bug 1734241
  // Promise<undefined> pipeTo(destination: WritableStream, #[optional = {}] options: StreamPipeOptions);

  #[throws]
  pub fn tee() -> Vec<ReadableStream>;

  // Bug 1734244
  // async iterable<any>(#[optional = {}] options: ReadableStreamIteratorOptions);
}

pub enum ReadableStreamReaderMode { "byob" };

pub struct ReadableStreamGetReaderOptions {
  pub mode: ReadableStreamReaderMode,
}
