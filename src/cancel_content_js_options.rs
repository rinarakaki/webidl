use super::*;

#[GenerateInit]
pub struct CancelContentJSOptions {
  pub #[optional = 0] index: i32,
  pub #[optional = None] uri: Option<URI>,
  pub #[optional = 0] epoch: i32,
}
