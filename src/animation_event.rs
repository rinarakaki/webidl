// The origin of this IDL file is
// http://www.w3.org/TR/css3-animations/#animation-events-
// http://dev.w3.org/csswg/css3-animations/#animation-events-

pub super::*;

#[exposed = Window]
pub trait AnimationEvent: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional] event_init: AnimationEventInit = {}) -> Self;

  #[alias = "animationName"]
  pub animation_name: DOMString,
  #[alias = "elapsedTime"]
  pub elapsed_time: f32,
  #[alias = "pseudoElement"]
  pub pseudo_element: DOMString,
}

pub struct AnimationEventInit: EventInit {
  #[alias = "animationName"]
  pub animation_name: DOMString = "",
  #[alias = "elapsedTime"]
  pub elapsed_time: f32 = 0,
  #[alias = "pseudoElement"]
  pub pseudo_element: DOMString = "",
}
