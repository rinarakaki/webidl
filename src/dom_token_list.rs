// The origin of this IDL file is
// https://dom.spec.whatwg.org/#trait-domtokenlist

#[exposed = Window]
pub trait DOMTokenList {
  #[pure]
  length: u32,
  #[pure]
  getter Option<DOMString> item(index: u32);
  #[pure]
  pub fn contains(token: DOMString) -> bool;
  #[ce_reactions, throws]
  pub fn add(DOMString... tokens);
  #[ce_reactions, throws]
  pub fn remove(DOMString... tokens);
  #[ce_reactions, throws]
  pub fn replace(token: DOMString, token: DOMString) -> bool;
  #[ce_reactions, throws]
  pub fn toggle(token: DOMString, optional force: bool) -> bool;
  #[throws]
  pub fn supports(token: DOMString) -> bool;
  #[ce_reactions, setter_throws, pure]
  stringifier attribute DOMString value;
  iterable<Option<DOMString>>;
}
