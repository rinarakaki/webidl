// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLTableCaptionElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;
}

partial trait HTMLTableCaptionElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
}
