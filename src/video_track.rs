// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#videotrack

#[pref = "media.track.enabled",
 exposed = Window]
pub trait VideoTrack {
  id: DOMString,
  kind: DOMString,
  label: DOMString,
  language: DOMString,
  pub selected: bool,
}
