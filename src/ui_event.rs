// For more information on this trait please see
// http://dev.w3.org/2006/webapi/DOM-Level-3-Events/html/DOM3-Events.html

#[exposed = Window]
pub trait UIEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: UIEventInit) -> Self;

  view: Option<WindowProxy>,
  detail: i32,
  void initUIEvent(aType: DOMString,
  #[optional = false] aCanBubble: bool,
  #[optional = false] aCancelable: bool,
  optional Option<Window> aView = None,
  #[optional = 0] aDetail: i32);
}

// Additional DOM0 properties.
partial trait UIEvent {
  const i32 SCROLL_PAGE_UP = -32768;
  const i32 SCROLL_PAGE_DOWN = 32768;

  layerX: i32,
  layerY: i32,
  #[needs_caller_type]
  which: u32,
  #[alias = "rangeParent"]
  range_parent: Option<Node>,
  #[alias = "rangeOffset"]
  range_offset: i32,
}

pub struct UIEventInit: EventInit {
  pub view: Option<Window> = None,
  pub detail: i32 = 0,
}

// NOTE: Gecko doesn't support commented out modifiers yet.
pub struct EventModifierInit: UIEventInit {
  #[alias = "ctrlKey"]
  pub ctrl_key: bool = false,
  #[alias = "shiftKey"]
  pub shift_key: bool = false,
  #[alias = "altKey"]
  pub alt_key: bool = false,
  #[alias = "metaKey"]
  pub meta_key: bool = false,
  #[alias = "modifierAltGraph"]
  pub modifier_alt_graph: bool = false,
  #[alias = "modifierCapsLock"]
  pub modifier_caps_lock: bool = false,
  pub modifierFn: bool = false,
  #[alias = "modifierFnLock"]
  pub modifier_fn_lock: bool = false,
  // bool modifierHyper = false;
  #[alias = "modifierNumLock"]
  pub modifier_num_lock: bool = false,
  pub modifierOS: bool = false,
  #[alias = "modifierScrollLock"]
  pub modifier_scroll_lock: bool = false,
  // bool modifierSuper = false;
  #[alias = "modifierSymbol"]
  pub modifier_symbol: bool = false,
  #[alias = "modifierSymbolLock"]
  pub modifier_symbol_lock: bool = false,
}
