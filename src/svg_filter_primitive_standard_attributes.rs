// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

pub trait mixin SVGFilterPrimitiveStandardAttributes {
  #[constant]
  x: SVGAnimatedLength,
  #[constant]
  y: SVGAnimatedLength,
  #[constant]
  width: SVGAnimatedLength,
  #[constant]
  height: SVGAnimatedLength,
  #[constant]
  result: SVGAnimatedString,
}
