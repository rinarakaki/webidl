// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSeg {

  // Path Segment Types
  const u16 PATHSEG_UNKNOWN = 0;
  const u16 PATHSEG_CLOSEPATH = 1;
  const u16 PATHSEG_MOVETO_ABS = 2;
  const u16 PATHSEG_MOVETO_REL = 3;
  const u16 PATHSEG_LINETO_ABS = 4;
  const u16 PATHSEG_LINETO_REL = 5;
  const u16 PATHSEG_CURVETO_CUBIC_ABS = 6;
  const u16 PATHSEG_CURVETO_CUBIC_REL = 7;
  const u16 PATHSEG_CURVETO_QUADRATIC_ABS = 8;
  const u16 PATHSEG_CURVETO_QUADRATIC_REL = 9;
  const u16 PATHSEG_ARC_ABS = 10;
  const u16 PATHSEG_ARC_REL = 11;
  const u16 PATHSEG_LINETO_HORIZONTAL_ABS = 12;
  const u16 PATHSEG_LINETO_HORIZONTAL_REL = 13;
  const u16 PATHSEG_LINETO_VERTICAL_ABS = 14;
  const u16 PATHSEG_LINETO_VERTICAL_REL = 15;
  const u16 PATHSEG_CURVETO_CUBIC_SMOOTH_ABS = 16;
  const u16 PATHSEG_CURVETO_CUBIC_SMOOTH_REL = 17;
  const u16 PATHSEG_CURVETO_QUADRATIC_SMOOTH_ABS = 18;
  const u16 PATHSEG_CURVETO_QUADRATIC_SMOOTH_REL = 19;

  #[pure]
  #[alias = "pathSegType"]
  path_seg_type: u16,
  #[pure]
  #[alias = "pathSegTypeAsLetter"]
  path_seg_type_as_letter: DOMString,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegClosePath: SVGPathSeg {
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegMovetoAbs: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegMovetoRel: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegLinetoAbs: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegLinetoRel: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegCurvetoCubicAbs: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
  #[setter_throws]
  pub x1: f32,
  #[setter_throws]
  pub y1: f32,
  #[setter_throws]
  pub x2: f32,
  #[setter_throws]
  pub y2: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegCurvetoCubicRel: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
  #[setter_throws]
  pub x1: f32,
  #[setter_throws]
  pub y1: f32,
  #[setter_throws]
  pub x2: f32,
  #[setter_throws]
  pub y2: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegCurvetoQuadraticAbs: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
  #[setter_throws]
  pub x1: f32,
  #[setter_throws]
  pub y1: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegCurvetoQuadraticRel: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
  #[setter_throws]
  pub x1: f32,
  #[setter_throws]
  pub y1: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegArcAbs: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
  #[setter_throws]
  pub r1: f32,
  #[setter_throws]
  pub r2: f32,
  #[setter_throws]
  pub angle: f32,
  #[setter_throws]
  #[alias = "largeArcFlag"]
  pub large_arc_flag: bool,
  #[setter_throws]
  #[alias = "sweepFlag"]
  pub sweep_flag: bool,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegArcRel: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
  #[setter_throws]
  pub r1: f32,
  #[setter_throws]
  pub r2: f32,
  #[setter_throws]
  pub angle: f32,
  #[setter_throws]
  #[alias = "largeArcFlag"]
  pub large_arc_flag: bool,
  #[setter_throws]
  #[alias = "sweepFlag"]
  pub sweep_flag: bool,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegLinetoHorizontalAbs: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegLinetoHorizontalRel: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegLinetoVerticalAbs: SVGPathSeg {
  #[setter_throws]
  pub y: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegLinetoVerticalRel: SVGPathSeg {
  #[setter_throws]
  pub y: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegCurvetoCubicSmoothAbs: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
  #[setter_throws]
  pub x2: f32,
  #[setter_throws]
  pub y2: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegCurvetoCubicSmoothRel: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
  #[setter_throws]
  pub x2: f32,
  #[setter_throws]
  pub y2: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegCurvetoQuadraticSmoothAbs: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait SVGPathSegCurvetoQuadraticSmoothRel: SVGPathSeg {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
}

