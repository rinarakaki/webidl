// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/multipage/workers.html#the-workerglobalscope-common-trait

use super::WorkerGlobalScope;

#[Global = (Worker, DedicatedWorker),
 exposed = DedicatedWorker]
pub trait DedicatedWorkerGlobalScope: WorkerGlobalScope {
  #[replaceable]
  name: DOMString,

  #[alias = "postMessage"]
  pub fn post_message(message: any, message: any) -> Result<()>;

  #[alias = "postMessage"]
  pub fn post_message(
    message: any,
    #[optional = {}] options: StructuredSerializeOptions) -> Result<()>;

  pub fn close();

  #[alias = "onmessage"]
  pub on_message: EventHandler,
  #[alias = "onmessageerror"]
  pub on_message_error: EventHandler,

  // https://html.spec.whatwg.org/multipage/imagebitmap-and-animations.html#animation-frames
  // Ideally we would just include AnimationFrameProvider to add the trait,
  // but we cannot make an include conditional.
  #[pref = "dom.workers.requestAnimationFrame"]
  #[alias = "requestAnimationFrame"]
  pub fn request_animation_frame(callback: FrameRequestCallback) -> Result<i32>;

  #[pref = "dom.workers.requestAnimationFrame"]
  #[alias = "cancelAnimationFrame"]
  pub fn cancel_animation_frame(handle: i32) -> Result<()>;
}
