// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-span-element

// http://www.whatwg.org/specs/web-apps/current-work/#the-span-element
#[exposed = Window]
pub trait HTMLSpanElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;
}
