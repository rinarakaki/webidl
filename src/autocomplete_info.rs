// This pub struct is used for the input, textarea and select element's
// getAutocompleteInfo method.

use super::*;

pub struct AutocompleteInfo {
  pub section: DOMString = "",
  #[alias = "addressType"]
  pub address_type: DOMString = "",
  #[alias = "contactType"]
  pub contact_type: DOMString = "",
  #[alias = "fieldName"]
  pub field_name: DOMString = "",
  #[alias = "canAutomaticallyPersist"]
  pub can_automatically_persist: bool = true,
}
