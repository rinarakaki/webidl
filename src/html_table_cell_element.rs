// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLTableCellElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  #[alias = "colSpan"]
  pub col_span: u32,
  #[ce_reactions, setter_throws]
  #[alias = "rowSpan"]
  pub row_span: u32,
  //#[put_forwards = value] readonly attribute DOMTokenList headers;
  #[ce_reactions, setter_throws]
  pub headers: DOMString,
  #[alias = "cellIndex"]
  cell_index: i32,

  // Mozilla-specific extensions
  #[ce_reactions, setter_throws]
  pub abbr: DOMString,
  #[ce_reactions, setter_throws]
  pub scope: DOMString,
}

partial trait HTMLTableCellElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
  #[ce_reactions, setter_throws]
  pub axis: DOMString,
  #[ce_reactions, setter_throws]
  pub height: DOMString,
  #[ce_reactions, setter_throws]
  pub width: DOMString,

  #[ce_reactions, setter_throws]
  pub ch: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "chOff"]
  pub ch_off: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "noWrap"]
  pub no_wrap: bool,
  #[ce_reactions, setter_throws]
  #[alias = "vAlign"]
  pub v_align: DOMString,

  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString bgColor;
}
