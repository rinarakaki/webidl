// The origin of this IDL file is
// http://w3c.github.io/mediacapture-main/getusermedia.html#idl-def-MediaStreamError

// The future of MediaStreamError is uncertain.
// https://www.w3.org/Bugs/Public/show_bug.Option<cgi>id = 26776

// TODO: This is an 'exception', not an trait, by virtue of needing to be
// passed as a promise rejection-reason. Revisit if DOMException grows a customArg

#[ExceptionClass, LegacyNoInterfaceObject,
 exposed = Window]
pub trait MediaStreamError {
  name: DOMString,
  message: Option<DOMString>,
  constraint: Option<DOMString>,
}
