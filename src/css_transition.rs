// The origin of this IDL file is
// http://dev.w3.org/csswg/css-transitions-2/#the-CSSTransition-trait

use super::CSSRule;

#[func = "Document::IsWebAnimationsGetAnimationsEnabled",
 HeaderFile = "nsTransitionManager.h",
 exposed = Window]
pub trait CSSTransition: Animation {
  #[constant]
  #[alias = "transitionProperty"]
  transition_property: DOMString,
}
