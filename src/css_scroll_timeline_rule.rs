// The origin of this IDL file is
// https://drafts.csswg.org/scroll-animations-1/#scrolltimeline-trait

use super::CSSRule;

#[exposed = Window, pref = "layout.css.scroll-linked-animations.enabled"]
pub trait CSSScrollTimelineRule: CSSRule {
  name: DOMString,
  source: DOMString,
  orientation: DOMString,
  #[alias = "scrollOffsets"]
  scroll_offsets: DOMString,
}
