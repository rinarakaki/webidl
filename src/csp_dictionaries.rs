// Dictionary used to display CSP info.

// TODO
pub struct CSP {
  bool report-only = false;

  Vec<DOMString> default-src;
  Vec<DOMString> script-src;
  Vec<DOMString> object-src;
  Vec<DOMString> style-src;
  Vec<DOMString> img-src;
  Vec<DOMString> media-src;
  Vec<DOMString> frame-src;
  Vec<DOMString> font-src;
  Vec<DOMString> connect-src;
  Vec<DOMString> report-uri;
  Vec<DOMString> frame-ancestors;
  // Vec<DOMString> reflected-xss; // not supported in Firefox
  Vec<DOMString> base-uri;
  Vec<DOMString> form-action;
  pub referrer: Vec<DOMString>,
  Vec<DOMString> manifest-src;
  Vec<DOMString> upgrade-insecure-requests;
  Vec<DOMString> child-src;
  Vec<DOMString> block-all-mixed-content;
  pub sandbox: Vec<DOMString>,
  Vec<DOMString> worker-src;
}

#[GenerateToJSON]
pub struct CSPPolicies {
  Vec<CSP> csp-policies;
}
