#[exposed = Window]
pub trait MimeType {
  description: DOMString,
  #[alias = "enabledPlugin"]
  enabled_plugin: Option<Plugin>,
  suffixes: DOMString,
  type: DOMString,
}
