// The origin of this IDL file is
// http://www.w3.org/TR/raw-sockets/#trait-udpmessageevent

//Bug 1056444: This trait should be removed after UDPSocket.input/UDPSocket.output are ready.
#[pref = "dom.udpsocket.enabled",
 chrome_only,
 exposed = Window]
pub trait UDPMessageEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: UDPMessageEventInit);

  #[alias = "remoteAddress"]
  remote_address: DOMString,
  #[alias = "remotePort"]
  remote_port: u16,
  data: any,
}

pub struct UDPMessageEventInit: EventInit {
  #[alias = "remoteAddress"]
  pub remote_address: DOMString = "",
  #[alias = "remotePort"]
  pub remote_port: u16 = 0,
  pub data: any = None,
}
