// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

pub trait SVGViewSpec;

#[exposed = Window]
pub trait SVGSVGElement: SVGGraphicsElement {

  #[constant]
  x: SVGAnimatedLength,
  #[constant]
  y: SVGAnimatedLength,
  #[constant]
  width: SVGAnimatedLength,
  #[constant]
  height: SVGAnimatedLength,
  // readonly attribute SVGRect viewport;
  #[alias = "useCurrentView"]
  use_current_view: bool,
  // readonly attribute SVGViewSpec currentView;
  #[use_counter]
  #[alias = "currentScale"]
  pub current_scale: f32,
  #[alias = "currentTranslate"]
  current_translate: SVGPoint,

  #[DependsOn = Nothing, Affects = Nothing]
  u32 suspendRedraw(maxWaitMilliseconds: u32);
  #[DependsOn = Nothing, Affects = Nothing]
  #[alias = "unsuspendRedraw"]
  pub fn unsuspend_redraw(suspendHandleID: u32);
  #[DependsOn = Nothing, Affects = Nothing]
  #[alias = "unsuspendRedrawAll"]
  pub fn unsuspend_redraw_all();
  #[DependsOn = Nothing, Affects = Nothing]
  #[alias = "forceRedraw"]
  pub fn force_redraw();
  #[alias = "pauseAnimations"]
  pub fn pause_animations();
  #[alias = "unpauseAnimations"]
  pub fn unpause_animations();
  #[alias = "animationsPaused"]
  pub fn animations_paused() -> bool;
  #[binary_name = "getCurrentTimeAsFloat"]
  #[alias = "getCurrentTime"]
  pub fn get_current_time() -> f32;
  #[alias = "setCurrentTime"]
  pub fn set_current_time(seconds: f32);
  // NodeList getIntersectionList(rect: SVGRect, rect: SVGRect);
  // NodeList getEnclosureList(rect: SVGRect, rect: SVGRect);
  // bool checkIntersection(element: SVGElement, element: SVGElement);
  // bool checkEnclosure(element: SVGElement, element: SVGElement);
  #[alias = "deselectAll"]
  pub fn deselect_all();
  #[new_object]
  #[alias = "createSVGNumber"]
  pub fn create_svg_number() -> SVGNumber;
  #[new_object]
  #[alias = "createSVGLength"]
  pub fn create_svg_length() -> SVGLength;
  #[new_object]
  #[alias = "createSVGAngle"]
  pub fn create_svg_angle() -> SVGAngle;
  #[new_object]
  #[alias = "createSVGPoint"]
  pub fn create_svg_point() -> SVGPoint;
  #[new_object]
  #[alias = "createSVGMatrix"]
  pub fn create_svg_matrix() -> SVGMatrix;
  #[new_object]
  #[alias = "createSVGRect"]
  pub fn create_svg_rect() -> SVGRect;
  #[new_object]
  #[alias = "createSVGTransform"]
  pub fn create_svg_transform() -> SVGTransform;
  #[new_object, throws]
  #[alias = "createSVGTransformFromMatrix"]
  pub fn create_svg_transform_from_matrix(#[optional = {}] matrix: DOMMatrix2DInit) -> SVGTransform;
  #[use_counter]
  #[alias = "getElementById"]
  pub fn get_element_by_id(elementId: DOMString) -> Option<Element>;
}

SVGSVGElement includes SVGFitToViewBox;
SVGSVGElement includes SVGZoomAndPan;

