// This pub struct is used for exposing failed channel info
// to about:neterror to built UI.

pub struct NetErrorInfo {
  #[alias = "errorCodeString"]
  pub error_code_string: DOMString = "",
}
