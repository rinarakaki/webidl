// https://wicg.github.io/entries-api/#idl-index

pub struct FileSystemFlags {
  pub create: bool = false,
  pub exclusive: bool = false,
}

pub type FileSystemEntryCallback = Fn(FileSystemEntry);

pub type ErrorCallback = Fn(DOMException);

#[exposed = Window]
pub trait FileSystem {
  name: USVString,
  root: FileSystemDirectoryEntry,
}
