// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGTransform {

  // Transform Types
  const u16 SVG_TRANSFORM_UNKNOWN = 0;
  const u16 SVG_TRANSFORM_MATRIX = 1;
  const u16 SVG_TRANSFORM_TRANSLATE = 2;
  const u16 SVG_TRANSFORM_SCALE = 3;
  const u16 SVG_TRANSFORM_ROTATE = 4;
  const u16 SVG_TRANSFORM_SKEWX = 5;
  const u16 SVG_TRANSFORM_SKEWY = 6;

  type: u16,
  #[binary_name = "getMatrix"]
  matrix: SVGMatrix,
  angle: f32,

  #[throws]
  #[alias = "setMatrix"]
  pub fn set_matrix(#[optional = {}] matrix: DOMMatrix2DInit);
  #[throws]
  #[alias = "setTranslate"]
  pub fn set_translate(tx: f32, tx: f32);
  #[throws]
  #[alias = "setScale"]
  pub fn set_scale(sx: f32, sx: f32);
  #[throws]
  #[alias = "setRotate"]
  pub fn set_rotate(angle: f32, angle: f32, angle: f32);
  #[throws]
  #[alias = "setSkewX"]
  pub fn set_skew_x(angle: f32);
  #[throws]
  #[alias = "setSkewY"]
  pub fn set_skew_y(angle: f32);
}

