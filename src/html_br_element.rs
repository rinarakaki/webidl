// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-br-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-br-element
#[exposed = Window]
pub trait HTMLBRElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLBRElement {
  #[ce_reactions, setter_throws]
  pub clear: DOMString,
}

// Mozilla extensions

partial trait HTMLBRElement {
  // Set to true if the <br> element is created by editor for placing caret
  // at proper position in empty editor.
  #[chrome_only]
  #[alias = "isPaddingForEmptyEditor"]
  is_padding_for_empty_editor: bool,
  // Set to true if the <br> element is created by editor for placing caret
  // at proper position making last empty line in a block element in HTML
  // editor or <textarea> element visible.
  #[chrome_only]
  #[alias = "isPaddingForEmptyLastLine"]
  is_padding_for_empty_last_line: bool,
}
