// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

pub struct AudioBufferSourceOptions {
  pub buffer: Option<AudioBuffer>,
  pub detune: f32 = 0,
  pub loop: bool = false,
  #[alias = "loopEnd"]
  pub loop_end: f64 = 0,
  #[alias = "loopStart"]
  pub loop_start: f64 = 0,
  #[alias = "playbackRate"]
  pub playback_rate: f32 = 1,
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait AudioBufferSourceNode: AudioScheduledSourceNode {
  #[alias = "constructor"]
  pub fn new(
    context: BaseAudioContext,
    #[optional = {}] options: AudioBufferSourceOptions) -> Self;

  #[setter_throws]
  pub mut buffer: Option<AudioBuffer>,

  #[alias = "playbackRate"]
  pub playback_rate: AudioParam,
  pub detune: AudioParam,

  pub mut loop: bool,
  #[alias = "loopStart"]
  pub mut loop_start: f64,
  #[alias = "loopEnd"]
  pub mut loop_end: f64,

  pub fn start(
    &self,
    #[optional = 0] when: f64,
    #[optional = 0] grain_offset: f64,
    #[optional] grain_duration: f64) -> Result<()>;
}

// Mozilla extensions
AudioBufferSourceNode includes AudioNodePassThrough;
