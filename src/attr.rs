// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

pub super::*;

#[exposed = Window]
pub trait Attr: Node {
  #[alias = "localName"]
  pub local_name: DOMString,
  #[ce_reactions, setter_needs_subject_principal = NonSystem, setter_throws]
  pub mut value: DOMString,

  #[constant]
  pub name: DOMString,
  #[constant]
  #[alias = "namespaceURI"]
  pub namespace_uri: Option<DOMString>,
  #[constant]
  pub prefix: Option<DOMString>,

  pub specified: bool,
}

// Mozilla extensions

partial trait Attr {
  #[getter_throws]
  #[alias = "ownerElement"]
  pub owner_element: Option<Element>,
}
