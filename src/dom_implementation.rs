// The origin of this IDL file is
// http://dom.spec.whatwg.org/#trait-domimplementation

#[exposed = Window]
pub trait DOMImplementation {
  #[alias = "hasFeature"]
  pub fn has_feature() -> bool;

  #[alias = "createDocumentType"]
  pub fn create_document_type(
    qualified_name: DOMString,
    qualified_name: DOMString,
    system_id: DOMString) -> Result<DocumentType>;

  #[alias = "createDocument"]
  pub fn create_document(
    namespace: Option<DOMString>,
    qualified_name: [LegacyNullToEmptyString] DOMString,
    optional Option<DocumentType> doctype = None) -> Result<Document>;

  #[alias = "createHTMLDocument"]
  pub fn create_html_document(optional title: DOMString) -> Result<Document>;
}
