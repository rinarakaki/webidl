// The origin of this IDL file is
// https://fetch.spec.whatwg.org/#request-class

pub type RequestInfo = (Request or USVString);
pub type nsContentPolicyType = u32;

#[exposed = (Window, Worker)]
pub trait Request {
  #[alias = "constructor"]
  pub fn new(input: RequestInfo, #[optional = {}] init: RequestInit) -> Result<Self, Error>;

  method: ByteString,
  url: USVString,
  #[same_object, binary_name = "headers_"]
  headers: Headers,

  destination: RequestDestination,
  referrer: USVString,
  #[binary_name = "referrerPolicy_"]
  #[alias = "referrerPolicy"]
  referrer_policy: ReferrerPolicy,
  mode: RequestMode,
  credentials: RequestCredentials,
  cache: RequestCache,
  redirect: RequestRedirect,
  integrity: DOMString,

  // If a main-thread fetch() promise rejects, the error passed will be a
  // nsresult code.
  #[chrome_only]
  #[alias = "mozErrors"]
  moz_errors: bool,

  #[binary_name = "getOrCreateSignal"]
  signal: AbortSignal,

  #[throws,
  new_object] Request clone();

  // Bug 1124638 - Allow chrome callers to set the context.
  #[chrome_only]
  #[alias = "overrideContentPolicyType"]
  pub fn override_content_policy_type(context: nsContentPolicyType);
}
Request includes Body;

pub struct RequestInit {
  pub method: ByteString,
  pub headers: HeadersInit,
  pub body: Option<BodyInit>,
  pub referrer: USVString,
  #[alias = "referrerPolicy"]
  pub referrer_policy: ReferrerPolicy,
  pub mode: RequestMode,
  pub credentials: RequestCredentials,
  pub cache: RequestCache,
  pub redirect: RequestRedirect,
  pub integrity: DOMString,

  #[chrome_only]
  #[alias = "mozErrors"]
  pub moz_errors: bool,

  pub signal: Option<AbortSignal>,

  #[pref = "dom.fetchObserver.enabled"]
  pub observe: ObserverCallback,
}

pub enum RequestDestination {
  "",
  "audio", "audioworklet", "document", "embed", "font", "frame", "iframe",
  "image", "manifest", "object", "paintworklet", "report", "script",
  "sharedworker", "style", "track", "video", "worker", "xslt"
}

pub enum RequestMode { "same-origin", "no-cors", "cors", "navigate" };
pub enum RequestCredentials { "omit", "same-origin", "include" };
pub enum RequestCache { "default", "no-store", "reload", "no-cache", "force-cache", "only-if-cached" };
pub enum RequestRedirect { "follow", "error", "manual" };
