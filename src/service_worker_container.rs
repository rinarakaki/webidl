// The origin of this IDL file is
// https://w3c.github.io/ServiceWorker/#serviceworkercontainer

#[func = "ServiceWorkerContainer::IsEnabled",
 exposed = Window]
pub trait ServiceWorkerContainer: EventTarget {
  // FIXME(nsm):
  // https://github.com/slightlyoff/ServiceWorker/issues/198
  // and discussion at https://etherpad.mozilla.org/serviceworker07apr
  controller: Option<ServiceWorker>,

  #[throws]
  ready: Promise<ServiceWorkerRegistration>,

  #[new_object, needs_caller_type]
  Promise<ServiceWorkerRegistration> register(scriptURL: USVString,
  #[optional = {}] options: RegistrationOptions);

  #[new_object]
  #[alias = "getRegistration"]
  pub fn get_registration(#[optional = ""] documentURL: USVString) -> Promise<any>;

  #[new_object]
  Promise<Vec<ServiceWorkerRegistration>> getRegistrations();

  #[alias = "startMessages"]
  pub fn start_messages();

  #[alias = "oncontrollerchange"]
  pub oncontrollerchange: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  #[alias = "onmessage"]
  pub on_message: EventHandler,
  #[alias = "onmessageerror"]
  pub on_messageerror: EventHandler,
}

// Testing only.
partial trait ServiceWorkerContainer {
  #[throws,pref = "dom.serviceWorkers.testing.enabled"]
  #[alias = "getScopeForUrl"]
  pub fn get_scope_for_url(url: DOMString) -> DOMString;
}

pub struct RegistrationOptions {
  pub scope: USVString,
  #[alias = "updateViaCache"]
  pub update_via_cache: ServiceWorkerUpdateViaCache = "imports",
}
