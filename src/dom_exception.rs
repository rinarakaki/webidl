// The origin of this IDL file is
// http://dom.spec.whatwg.org/#exception-domexception

// This is the WebIDL version of nsIException. This is mostly legacy stuff.

pub trait StackFrame;

pub trait mixin ExceptionMembers {
  // The nsresult associated with this exception.
  result: u32,

  // Filename location. This is the location that caused the
  // error, which may or may not be a source file location.
  // For example, standard language errors would generally have
  // the same location as their top stack entry. File
  // parsers may put the location of the file they were parsing,
  // etc.

  // None indicates "no data"
  filename: DOMString,
  // Valid line numbers begin at '1'. '0' indicates unknown.
  #[alias = "lineNumber"]
  line_number: u32,
  // Valid column numbers begin at 0. 
  // We don't have an unambiguous indicator for unknown.
  #[alias = "columnNumber"]
  column_number: u32,

  // A stack trace, if available. nsIStackFrame does not have classinfo so
  // this was only ever usefully available to chrome JS.
  #[chrome_only, exposed = Window]
  location: Option<StackFrame>,

  // Arbitary data for the implementation.
  #[exposed = Window]
  data: Option<nsISupports>,

  // Formatted exception stack
  #[replaceable]
  stack: DOMString,
}

#[LegacyNoInterfaceObject, exposed = (Window, Worker)]
pub trait Exception {
  // The name of the error code (ie, a string repr of |result|).
  name: DOMString,
  // A custom message set by the thrower.
  #[binary_name = "messageMoz"]
  message: DOMString,
  // A generic formatter - make it suitable to print, etc.
  stringifier;
}

Exception includes ExceptionMembers;

// XXXkhuey this is an 'exception', not an trait, but we don't have any
// parser or codegen mechanisms for dealing with exceptions.
#[ExceptionClass, exposed = (Window, Worker)]
pub trait DOMException {
  #[alias = "constructor"]
  pub fn new(#[optional = ""] message: DOMString, optional name: DOMString) -> Self;

  // The name of the error code (ie, a string repr of |result|).
  name: DOMString,
  // A custom message set by the thrower.
  #[binary_name = "messageMoz"]
  message: DOMString,
  code: u16,

  const u16 INDEX_SIZE_ERR = 1;
  const u16 DOMSTRING_SIZE_ERR = 2; // historical
  const u16 HIERARCHY_REQUEST_ERR = 3;
  const u16 WRONG_DOCUMENT_ERR = 4;
  const u16 INVALID_CHARACTER_ERR = 5;
  const u16 NO_DATA_ALLOWED_ERR = 6; // historical
  const u16 NO_MODIFICATION_ALLOWED_ERR = 7;
  const u16 NOT_FOUND_ERR = 8;
  const u16 NOT_SUPPORTED_ERR = 9;
  const u16 INUSE_ATTRIBUTE_ERR = 10; // historical
  const u16 INVALID_STATE_ERR = 11;
  const u16 SYNTAX_ERR = 12;
  const u16 INVALID_MODIFICATION_ERR = 13;
  const u16 NAMESPACE_ERR = 14;
  const u16 INVALID_ACCESS_ERR = 15;
  const u16 VALIDATION_ERR = 16; // historical
  const u16 TYPE_MISMATCH_ERR = 17; // historical; use JavaScript's TypeError instead
  const u16 SECURITY_ERR = 18;
  const u16 NETWORK_ERR = 19;
  const u16 ABORT_ERR = 20;
  const u16 URL_MISMATCH_ERR = 21;
  const u16 QUOTA_EXCEEDED_ERR = 22;
  const u16 TIMEOUT_ERR = 23;
  const u16 INVALID_NODE_TYPE_ERR = 24;
  const u16 DATA_CLONE_ERR = 25;
}

// XXXkhuey copy all of Gecko's non-standard stuff onto DOMException, but leave
// the prototype chain sane.
DOMException includes ExceptionMembers;
