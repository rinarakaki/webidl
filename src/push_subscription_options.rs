// The origin of this IDL file is
// https://w3c.github.io/push-api/

#[exposed = (Window, Worker), pref = "dom.push.enabled"]
pub trait PushSubscriptionOptions {
  #[same_object, throws]
  #[alias = "applicationServerKey"]
  application_server_key: Option<ArrayBuffer>,
}
