// The origin of this IDL file is
// http://fetch.spec.whatwg.org/

pub type JSON = object;
pub type XMLHttpRequestBodyInit = (Blob or BufferSource or FormData or URLSearchParams or USVString);
// no support for request body streams yettypedef XMLHttpRequestBodyInit BodyInit;

pub trait mixin Body {
  #[throws]
  #[alias = "bodyUsed"]
  body_used: bool,
  #[throws]
  #[alias = "arrayBuffer"]
  pub fn array_buffer() -> Promise<ArrayBuffer>;
  #[throws]
  pub fn blob() -> Promise<Blob>;
  #[throws]
  #[alias = "formData"]
  pub fn form_data() -> Promise<FormData>;
  #[throws]
  pub fn json() -> Promise<JSON>;
  #[throws]
  pub fn text() -> Promise<USVString>;
}

// These are helper dictionaries for the parsing of a
// getReader().read().then(data) parsing.
// See more about how these 2 helpers are used in
// dom/fetch/FetchStreamReader.cpp
#[GenerateInit]
pub struct FetchReadableStreamReadDataDone {
  pub done: bool = false,
}

#[GenerateInit]
pub struct FetchReadableStreamReadDataArray {
  pub value: Uint8Array,
}
