// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

#[exposed = Window]
pub trait TreeWalker {
  #[constant]
  root: Node,
  #[constant]
  #[alias = "whatToShow"]
  what_to_show: u32,
  #[constant]
  filter: Option<NodeFilter>,
  #[pure, setter_throws]
  #[alias = "currentNode"]
  pub current_node: Node,

  #[throws]
  #[alias = "parentNode"]
  pub fn parent_node() -> Option<Node>;
  #[throws]
  #[alias = "firstChild"]
  pub fn first_child() -> Option<Node>;
  #[throws]
  #[alias = "lastChild"]
  pub fn last_child() -> Option<Node>;
  #[throws]
  #[alias = "previousSibling"]
  pub fn previous_sibling() -> Option<Node>;
  #[throws]
  #[alias = "nextSibling"]
  pub fn next_sibling() -> Option<Node>;
  #[throws]
  #[alias = "previousNode"]
  pub fn previous_node() -> Option<Node>;
  #[throws]
  #[alias = "nextNode"]
  pub fn next_node() -> Option<Node>;
}
