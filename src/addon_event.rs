pub super::*;

#[func = "mozilla::AddonManagerWebAPI::IsAPIEnabled"]
#[exposed = Window]
pub trait AddonEvent: Event {
    #[alias = "constructor"]
    pub fn new(r#type: DOMString, event_init: AddonEventInit) -> Self;

    pub id: DOMString,
}

pub struct AddonEventInit: EventInit {
    required DOMString id;
}
