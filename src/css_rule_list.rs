#[exposed = Window]
pub trait CSSRuleList {
  length: u32,
  getter item(index: u32) -> Option<CSSRule>;
}
