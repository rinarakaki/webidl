// The origin of this IDL file is
// http://dom.spec.whatwg.org/#range
// http://domparsing.spec.whatwg.org/#dom-range-createcontextualfragment
// http://dvcs.w3.org/hg/csswg/raw-file/tip/cssom-view/Overview.html#extensions-to-the-range-trait

#[exposed = Window]
pub trait Range: AbstractRange {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  #[throws]
  #[alias = "commonAncestorContainer"]
  common_ancestor_container: Node,

  #[throws, binary_name = "setStartJS"]
  #[alias = "setStart"]
  pub fn set_start(refNode: Node, refNode: Node);
  #[throws, binary_name = "setEndJS"]
  #[alias = "setEnd"]
  pub fn set_end(refNode: Node, refNode: Node);
  #[throws, binary_name = "setStartBeforeJS"]
  #[alias = "setStartBefore"]
  pub fn set_start_before(refNode: Node);
  #[throws, binary_name = "setStartAfterJS"]
  #[alias = "setStartAfter"]
  pub fn set_start_after(refNode: Node);
  #[throws, binary_name = "setEndBeforeJS"]
  #[alias = "setEndBefore"]
  pub fn set_end_before(refNode: Node);
  #[throws, binary_name = "setEndAfterJS"]
  #[alias = "setEndAfter"]
  pub fn set_end_after(refNode: Node);
  #[binary_name = "collapseJS"]
  pub fn collapse(#[optional = false] toStart: bool);
  #[throws, binary_name = "selectNodeJS"]
  #[alias = "selectNode"]
  pub fn select_node(refNode: Node);
  #[throws, binary_name = "selectNodeContentsJS"]
  #[alias = "selectNodeContents"]
  pub fn select_node_contents(refNode: Node);

  const u16 START_TO_START = 0;
  const u16 START_TO_END = 1;
  const u16 END_TO_END = 2;
  const u16 END_TO_START = 3;
  #[throws]
  #[alias = "compareBoundaryPoints"]
  pub fn compare_boundary_points(how: u16, how: u16) -> short;
  #[ce_reactions, throws]
  #[alias = "deleteContents"]
  pub fn delete_contents();
  #[ce_reactions, throws]
  #[alias = "extractContents"]
  pub fn extract_contents() -> DocumentFragment;
  #[ce_reactions, throws]
  #[alias = "cloneContents"]
  pub fn clone_contents() -> DocumentFragment;
  #[ce_reactions, throws]
  #[alias = "insertNode"]
  pub fn insert_node(node: Node);
  #[ce_reactions, throws]
  #[alias = "surroundContents"]
  pub fn surround_contents(newParent: Node);

  #[alias = "cloneRange"]
  pub fn clone_range() -> Range;
  pub fn detach();

  #[throws]
  #[alias = "isPointInRange"]
  pub fn is_point_in_range(node: Node, node: Node) -> bool;
  #[throws]
  #[alias = "comparePoint"]
  pub fn compare_point(node: Node, node: Node) -> short;

  #[throws]
  #[alias = "intersectsNode"]
  pub fn intersects_node(node: Node) -> bool;

  #[throws, binary_name = "ToString"]
  stringifier;
}

// http://domparsing.spec.whatwg.org/#dom-range-createcontextualfragment
partial trait Range {
  #[ce_reactions, throws]
  #[alias = "createContextualFragment"]
  pub fn create_contextual_fragment(fragment: DOMString) -> DocumentFragment;
}

// http://dvcs.w3.org/hg/csswg/raw-file/tip/cssom-view/Overview.html#extensions-to-the-range-trait
partial trait Range {
  #[alias = "getClientRects"]
  pub fn get_client_rects() -> Option<DOMRectList>;
  #[alias = "getBoundingClientRect"]
  pub fn get_bounding_client_rect() -> DOMRect;
}

pub struct ClientRectsAndTexts {
  required DOMRectList rectList;
  required Vec<DOMString> textList;
}

partial trait Range {
  #[chrome_only, throws]
  #[alias = "getClientRectsAndTexts"]
  pub fn get_client_rects_and_texts() -> ClientRectsAndTexts;
}
