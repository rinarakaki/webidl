// The origin of this IDL file is
// http://xhr.spec.whatwg.org

pub type FormDataEntryValue = (Blob or Directory or USVString);

#[exposed = (Window, Worker)]
pub trait FormData {
  #[alias = "constructor"]
  pub fn new(optional form: HTMLFormElement) -> Result<Self, Error>;

  #[throws]
  pub fn append(name: USVString, name: USVString, optional filename: USVString);
  #[throws]
  pub fn append(name: USVString, name: USVString);
  pub fn delete(name: USVString);
  pub fn get(name: USVString) -> Option<FormDataEntryValue>;
  #[alias = "getAll"]
  pub fn get_all(name: USVString) -> Vec<FormDataEntryValue>;
  pub fn has(name: USVString) -> bool;
  #[throws]
  pub fn set(name: USVString, name: USVString, optional filename: USVString);
  #[throws]
  pub fn set(name: USVString, name: USVString);
  iterable<USVString, FormDataEntryValue>;
}
