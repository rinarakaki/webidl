// The origin of this IDL file is
// http://dev.w3.org/csswg/css3-conditional/
// http://dev.w3.org/csswg/cssom/#the-css.escape%28%29-method

#[exposed = Window]
namespace CSS {
  pub fn supports(property: UTF8String, property: UTF8String) -> bool;
  pub fn supports(conditionText: UTF8String) -> bool;
}

// http://dev.w3.org/csswg/cssom/#the-css.escape%28%29-method
partial namespace CSS {
  pub fn escape(ident: DOMString) -> DOMString;
}
