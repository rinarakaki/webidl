// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFETurbulenceElement: SVGElement {

  // Turbulence Types
  const u16 SVG_TURBULENCE_TYPE_UNKNOWN = 0;
  const u16 SVG_TURBULENCE_TYPE_FRACTALNOISE = 1;
  const u16 SVG_TURBULENCE_TYPE_TURBULENCE = 2;

  // Stitch Options
  const u16 SVG_STITCHTYPE_UNKNOWN = 0;
  const u16 SVG_STITCHTYPE_STITCH = 1;
  const u16 SVG_STITCHTYPE_NOSTITCH = 2;

  #[constant]
  #[alias = "baseFrequencyX"]
  base_frequency_x: SVGAnimatedNumber,
  #[constant]
  #[alias = "baseFrequencyY"]
  base_frequency_y: SVGAnimatedNumber,
  #[constant]
  #[alias = "numOctaves"]
  num_octaves: SVGAnimatedInteger,
  #[constant]
  seed: SVGAnimatedNumber,
  #[constant]
  #[alias = "stitchTiles"]
  stitch_tiles: SVGAnimatedEnumeration,
  #[constant]
  type: SVGAnimatedEnumeration,
}

SVGFETurbulenceElement includes SVGFilterPrimitiveStandardAttributes;
