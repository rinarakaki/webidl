// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct PeriodicWaveConstraints {
  #[alias = "disableNormalization"]
  pub disable_normalisation: bool = false,
}

pub struct PeriodicWaveOptions: PeriodicWaveConstraints {
  pub real: Vec<f32>,
  pub imag: Vec<f32>,
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait PeriodicWave {
  #[alias = "constructor"]
  pub fn new(
    context: BaseAudioContext,
    #[optional = {}] options: PeriodicWaveOptions) -> Result<Self>;
}
