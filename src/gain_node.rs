// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct GainOptions: AudioNodeOptions {
  pub gain: f32 = 1.0,
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait GainNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(context: BaseAudioContext, #[optional = {}] options: GainOptions) -> Result<Self, Error>;

  gain: AudioParam,

}

// Mozilla extension
GainNode includes AudioNodePassThrough;

