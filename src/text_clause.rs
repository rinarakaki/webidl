#[chrome_only, exposed = Window]
pub trait TextClause {
  // The start offset of TextClause
  #[alias = "startOffset"]
  start_offset: i32,

  // The end offset of TextClause
  #[alias = "endOffset"]
  end_offset: i32,

  // If the TextClause is Caret or not
  #[alias = "isCaret"]
  is_caret: bool,

  // If the TextClause is TargetClause or not
  #[alias = "isTargetClause"]
  is_target_clause: bool,
}
