// The origin of this IDL file is
// https://gpuweb.github.io/gpuweb/

pub type GPUBufferDynamicOffset = #[EnforceRange] u32;
pub type GPUStencilValue = #[EnforceRange] u32;
pub type GPUSampleMask = #[EnforceRange] u32;
pub type GPUDepthBias = #[EnforceRange] i32;

pub type GPUSize64 = #[EnforceRange] u64;
pub type GPUIntegerCoordinate = #[EnforceRange] u32;
pub type GPUIndex32 = #[EnforceRange] u32;
pub type GPUSize32 = #[EnforceRange] u32;
pub type GPUSignedOffset32 = #[EnforceRange] i32;

pub struct GPUColorDict {
  required f64 r;
  required f64 g;
  required f64 b;
  required f64 a;
}

pub struct GPUOrigin2DDict {
  pub x: GPUIntegerCoordinate = 0,
  pub y: GPUIntegerCoordinate = 0,
}

pub struct GPUOrigin3DDict {
  pub x: GPUIntegerCoordinate = 0,
  pub y: GPUIntegerCoordinate = 0,
  pub z: GPUIntegerCoordinate = 0,
}

pub struct GPUExtent3DDict {
  required GPUIntegerCoordinate width;
  pub height: GPUIntegerCoordinate = 1,
  #[alias = "depthOrArrayLayers"]
  pub depth_or_arrayLayers: GPUIntegerCoordinate = 1,
}

pub type GPUColor = (Vec<f64> or GPUColorDict);
pub type GPUOrigin2D = (Vec<GPUIntegerCoordinate> or GPUOrigin2DDict);
pub type GPUOrigin3D = (Vec<GPUIntegerCoordinate> or GPUOrigin3DDict);
pub type GPUExtent3D = (Vec<GPUIntegerCoordinate> or GPUExtent3DDict);

pub trait mixin GPUObjectBase {
  pub label: Option<USVString>,
}

pub struct GPUObjectDescriptorBase {
  pub label: USVString,
}

// ****************************************************************************
// INITIALIZATION
// ****************************************************************************

#[
  pref = "dom.webgpu.enabled",
  exposed = Window
]
pub trait GPU {
  // May reject with DOMException
  #[new_object]
  Promise<Option<GPUAdapter>> requestAdapter(#[optional = {}] options: GPURequestAdapterOptions);
}

// Add a "webgpu" member to Navigator/Worker that contains the global instance of a "WebGPU"
pub trait mixin GPUProvider {
  #[same_object, replaceable, pref = "dom.webgpu.enabled", exposed = Window]
  gpu: GPU,
}

pub enum GPUPowerPreference {
  #[alias = "low-power"]
  LowPower,
  #[alias = "high-performance"]
  HighPerformance
}

pub struct GPURequestAdapterOptions {
  #[alias = "powerPreference"]
  pub power_preference: GPUPowerPreference,
  #[alias = "forceFallbackAdapter"]
  pub force_fallback_adapter: bool = false,
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUSupportedFeatures {
  readonly setlike<DOMString>;
}

pub struct GPUDeviceDescriptor {
  #[alias = "requiredFeatures"]
  pub required_features: Vec<GPUFeatureName> = [],
  record<DOMString, GPUSize64> requiredLimits;
}

pub enum GPUFeatureName {
  #[alias = "depth-clip-control"]
  DepthClipControl,
  #[alias = "depth24unorm-stencil8"]
  Depth24unormStencil8,
  #[alias = "depth32float-stencil8"]
  Depth32floatStencil8,
  #[alias = "pipeline-statistics-query"]
  PipelineStatisticsQuery,
  #[alias = "texture-compression-bc"]
  TextureCompressionBc,
  #[alias = "texture-compression-etc2"]
  TextureCompressionEtc2,
  #[alias = "texture-compression-astc"]
  TextureCompressionAstc,
  #[alias = "timestamp-query"]
  TimestampQuery,
  #[alias = "indirect-first-instance"]
  IndirectFirstInstance,
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUSupportedLimits {
  #[alias = "maxTextureDimension1D"]
  max_texture_dimension1D: u32,
  #[alias = "maxTextureDimension2D"]
  max_texture_dimension2D: u32,
  #[alias = "maxTextureDimension3D"]
  max_texture_dimension3D: u32,
  #[alias = "maxTextureArrayLayers"]
  max_texture_arrayLayers: u32,
  #[alias = "maxBindGroups"]
  max_bind_groups: u32,
  #[alias = "maxDynamicUniformBuffersPerPipelineLayout"]
  max_dynamic_uniformBuffersPerPipelineLayout: u32,
  #[alias = "maxDynamicStorageBuffersPerPipelineLayout"]
  max_dynamic_storageBuffersPerPipelineLayout: u32,
  #[alias = "maxSampledTexturesPerShaderStage"]
  max_sampled_texturesPerShaderStage: u32,
  #[alias = "maxSamplersPerShaderStage"]
  max_samplers_perShaderStage: u32,
  #[alias = "maxStorageBuffersPerShaderStage"]
  max_storage_buffersPerShaderStage: u32,
  #[alias = "maxStorageTexturesPerShaderStage"]
  max_storage_texturesPerShaderStage: u32,
  #[alias = "maxUniformBuffersPerShaderStage"]
  max_uniform_buffersPerShaderStage: u32,
  #[alias = "maxUniformBufferBindingSize"]
  max_uniform_bufferBindingSize: u32,
  #[alias = "maxStorageBufferBindingSize"]
  max_storage_bufferBindingSize: u32,
  #[alias = "maxVertexBuffers"]
  max_vertex_buffers: u32,
  #[alias = "maxVertexAttributes"]
  max_vertex_attributes: u32,
  #[alias = "maxVertexBufferArrayStride"]
  max_vertex_bufferArrayStride: u32,
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUAdapter {
  name: DOMString,
  #[same_object]
  features: GPUSupportedFeatures,
  #[same_object]
  limits: GPUSupportedLimits,
  #[alias = "isFallbackAdapter"]
  is_fallback_adapter: bool,

  #[new_object]
  #[alias = "requestDevice"]
  pub fn request_device(#[optional = {}] descriptor: GPUDeviceDescriptor) -> Promise<GPUDevice>;
}

// Device
#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUDevice: EventTarget {
  #[same_object]
  features: GPUSupportedFeatures,
  #[same_object]
  limits: GPUSupportedLimits,

  // Overriding the name to avoid collision with `class Queue` in gcc
  #[same_object, binary_name = "getQueue"]
  queue: GPUQueue,

  pub fn destroy();

  #[new_object, throws]
  #[alias = "createBuffer"]
  pub fn create_buffer(descriptor: GPUBufferDescriptor) -> GPUBuffer;
  #[new_object]
  #[alias = "createTexture"]
  pub fn create_texture(descriptor: GPUTextureDescriptor) -> GPUTexture;
  #[new_object]
  #[alias = "createSampler"]
  pub fn create_sampler(#[optional = {}] descriptor: GPUSamplerDescriptor) -> GPUSampler;

  #[alias = "createBindGroupLayout"]
  pub fn create_bind_group_layout(descriptor: GPUBindGroupLayoutDescriptor) -> GPUBindGroupLayout;
  #[alias = "createPipelineLayout"]
  pub fn create_pipeline_layout(descriptor: GPUPipelineLayoutDescriptor) -> GPUPipelineLayout;
  #[alias = "createBindGroup"]
  pub fn create_bind_group(descriptor: GPUBindGroupDescriptor) -> GPUBindGroup;

  #[alias = "createShaderModule"]
  pub fn create_shader_module(descriptor: GPUShaderModuleDescriptor) -> GPUShaderModule;
  #[alias = "createComputePipeline"]
  pub fn create_compute_pipeline(descriptor: GPUComputePipelineDescriptor) -> GPUComputePipeline;
  #[alias = "createRenderPipeline"]
  pub fn create_render_pipeline(descriptor: GPURenderPipelineDescriptor) -> GPURenderPipeline;

  #[throws]
  #[alias = "createComputePipelineAsync"]
  pub fn create_compute_pipeline_async(descriptor: GPUComputePipelineDescriptor) -> Promise<GPUComputePipeline>;
  #[throws]
  #[alias = "createRenderPipelineAsync"]
  pub fn create_render_pipeline_async(descriptor: GPURenderPipelineDescriptor) -> Promise<GPURenderPipeline>;

  #[new_object]
  #[alias = "createCommandEncoder"]
  pub fn create_command_encoder(#[optional = {}] descriptor: GPUCommandEncoderDescriptor) -> GPUCommandEncoder;
  #[new_object]
  #[alias = "createRenderBundleEncoder"]
  pub fn create_render_bundle_encoder(descriptor: GPURenderBundleEncoderDescriptor) -> GPURenderBundleEncoder;
  //#[new_object]
  //GPUQuerySet createQuerySet(descriptor: GPUQuerySetDescriptor);
}
GPUDevice includes GPUObjectBase;

// ****************************************************************************
// ERROR HANDLING
// ****************************************************************************

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUDeviceLostInfo {
  message: DOMString,
}

pub enum GPUErrorFilter {
  #[alias = "out-of-memory"]
  OutOfMemory,
  #[alias = "validation"]
  Validation
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUOutOfMemoryError {
  //constructor();
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUValidationError {
  #[alias = "constructor"]
  pub fn new(message: DOMString) -> Self;
  message: DOMString,
}

pub type GPUError = (GPUOutOfMemoryError or GPUValidationError);

partial trait GPUDevice {
  //readonly attribute Promise<GPUDeviceLostInfo> lost;
  #[alias = "pushErrorScope"]
  pub fn push_error_scope(filter: GPUErrorFilter);
  #[new_object]
  Promise<Option<GPUError>> popErrorScope();
  #[exposed = Window]
  #[alias = "onuncapturederror"]
  pub onuncapturederror: EventHandler,
}

// ****************************************************************************
// SHADER RESOURCES (buffer, textures, views: texture, samples)
// ****************************************************************************

// Buffer
pub type GPUBufferUsageFlags = #[EnforceRange] u32;
#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUBufferUsage {
  const GPUBufferUsageFlags MAP_READ = 0x0001;
  const GPUBufferUsageFlags MAP_WRITE = 0x0002;
  const GPUBufferUsageFlags COPY_SRC = 0x0004;
  const GPUBufferUsageFlags COPY_DST = 0x0008;
  const GPUBufferUsageFlags INDEX = 0x0010;
  const GPUBufferUsageFlags VERTEX = 0x0020;
  const GPUBufferUsageFlags UNIFORM = 0x0040;
  const GPUBufferUsageFlags STORAGE = 0x0080;
  const GPUBufferUsageFlags INDIRECT = 0x0100;
  const GPUBufferUsageFlags QUERY_RESOLVE = 0x0200;
}

pub struct GPUBufferDescriptor: GPUObjectDescriptorBase {
  required GPUSize64 size;
  required GPUBufferUsageFlags usage;
  #[alias = "mappedAtCreation"]
  pub mapped_at_creation: bool = false,
}

pub type GPUMapModeFlags = #[EnforceRange] u32;

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUMapMode
 {
  const GPUMapModeFlags READ = 0x0001;
  const GPUMapModeFlags WRITE = 0x0002;
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUBuffer {
  #[new_object]
  #[alias = "mapAsync"]
  pub fn map_async(mode: GPUMapModeFlags, #[optional = 0] offset: GPUSize64, optional size: GPUSize64) -> Promise<void>;
  #[new_object, throws]
  #[alias = "getMappedRange"]
  pub fn get_mapped_range(#[optional = 0] offset: GPUSize64, optional size: GPUSize64) -> ArrayBuffer;
  #[throws]
  pub fn unmap();

  pub fn destroy();
}
GPUBuffer includes GPUObjectBase;

pub type GPUMappedBuffer = Vec<any>;

// Texture
pub enum GPUTextureDimension {
  "1d",
  "2d",
  "3d",
}

pub enum GPUTextureFormat {
  // 8-bit formats
  #[alias = "r8unorm"]
  R8unorm,
  #[alias = "r8snorm"]
  R8snorm,
  #[alias = "r8uint"]
  R8uint,
  #[alias = "r8sint"]
  R8sint,

  // 16-bit formats
  #[alias = "r16uint"]
  R16uint,
  #[alias = "r16sint"]
  R16sint,
  #[alias = "r16float"]
  R16float,
  #[alias = "rg8unorm"]
  Rg8unorm,
  #[alias = "rg8snorm"]
  Rg8snorm,
  #[alias = "rg8uint"]
  Rg8uint,
  #[alias = "rg8sint"]
  Rg8sint,

  // 32-bit formats
  #[alias = "r32uint"]
  R32uint,
  #[alias = "r32sint"]
  R32sint,
  #[alias = "r32float"]
  R32float,
  #[alias = "rg16uint"]
  Rg16uint,
  #[alias = "rg16sint"]
  Rg16sint,
  #[alias = "rg16float"]
  Rg16float,
  #[alias = "rgba8unorm"]
  Rgba8unorm,
  #[alias = "rgba8unorm-srgb"]
  Rgba8unormSrgb,
  #[alias = "rgba8snorm"]
  Rgba8snorm,
  #[alias = "rgba8uint"]
  Rgba8uint,
  #[alias = "rgba8sint"]
  Rgba8sint,
  #[alias = "bgra8unorm"]
  Bgra8unorm,
  #[alias = "bgra8unorm-srgb"]
  Bgra8unormSrgb,
  // Packed 32-bit formats
  #[alias = "rgb10a2unorm"]
  Rgb10a2unorm,
  #[alias = "rg11b10float"]
  Rg11b10float,

  // 64-bit formats
  #[alias = "rg32uint"]
  Rg32uint,
  #[alias = "rg32sint"]
  Rg32sint,
  #[alias = "rg32float"]
  Rg32float,
  #[alias = "rgba16uint"]
  Rgba16uint,
  #[alias = "rgba16sint"]
  Rgba16sint,
  #[alias = "rgba16float"]
  Rgba16float,

  // 128-bit formats
  #[alias = "rgba32uint"]
  Rgba32uint,
  #[alias = "rgba32sint"]
  Rgba32sint,
  #[alias = "rgba32float"]
  Rgba32float,

  // Depth and stencil formats
  //"stencil8", //TODO
  //"depth16unorm",
  #[alias = "depth24plus"]
  Depth24plus,
  #[alias = "depth24plus-stencil8"]
  Depth24plusStencil8,
  #[alias = "depth32float"]
  Depth32float,

  // BC compressed formats usable if "texture-compression-bc" is both
  // supported by the device/user agent and enabled in requestDevice.
  #[alias = "bc1-rgba-unorm"]
  Bc1RgbaUnorm,
  "bc1-rgba-unorm-srgb",
  #[alias = "bc2-rgba-unorm"]
  Bc2RgbaUnorm,
  "bc2-rgba-unorm-srgb",
  #[alias = "bc3-rgba-unorm"]
  Bc3RgbaUnorm,
  "bc3-rgba-unorm-srgb",
  "bc4-r-unorm",
  "bc4-r-snorm",
  #[alias = "bc5-rg-unorm"]
  Bc5RgUnorm,
  #[alias = "bc5-rg-snorm"]
  Bc5RgSnorm,
  #[alias = "bc6h-rgb-ufloat"]
  Bc6hRgbUfloat,
  #[alias = "bc6h-rgb-f32"]
  Bc6hRgbF32,
  #[alias = "bc7-rgba-unorm"]
  Bc7RgbaUnorm,
  "bc7-rgba-unorm-srgb",

  // "depth24unorm-stencil8" feature
  //"depth24unorm-stencil8",

  // "depth32float-stencil8" feature
  //"depth32float-stencil8",
}

pub type GPUTextureUsageFlags = #[EnforceRange] u32;
#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUTextureUsage {
  const GPUTextureUsageFlags COPY_SRC = 0x01;
  const GPUTextureUsageFlags COPY_DST = 0x02;
  const GPUTextureUsageFlags TEXTURE_BINDING = 0x04;
  const GPUTextureUsageFlags STORAGE_BINDING = 0x08;
  const GPUTextureUsageFlags RENDER_ATTACHMENT = 0x10;
}

pub struct GPUTextureDescriptor: GPUObjectDescriptorBase {
  required GPUExtent3D size;
  #[alias = "mipLevelCount"]
  pub mip_level_count: GPUIntegerCoordinate = 1,
  #[alias = "sampleCount"]
  pub sample_count: GPUSize32 = 1,
  pub dimension: GPUTextureDimension = "2d",
  required GPUTextureFormat format;
  required GPUTextureUsageFlags usage;
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUTexture {
  #[new_object]
  #[alias = "createView"]
  pub fn create_view(#[optional = {}] descriptor: GPUTextureViewDescriptor) -> GPUTextureView;

  pub fn destroy();
}
GPUTexture includes GPUObjectBase;

// Texture view
pub enum GPUTextureViewDimension {
  "1d",
  "2d",
  "2d-array",
  #[alias = "cube"]
  Cube,
  #[alias = "cube-array"]
  CubeArray,
  "3d"
}

pub enum GPUTextureAspect {
  #[alias = "all"]
  All,
  #[alias = "stencil-only"]
  StencilOnly,
  #[alias = "depth-only"]
  DepthOnly
}

pub struct GPUTextureViewDescriptor: GPUObjectDescriptorBase {
  pub format: GPUTextureFormat,
  pub dimension: GPUTextureViewDimension,
  pub aspect: GPUTextureAspect = "all",
  #[alias = "baseMipLevel"]
  pub base_mip_level: GPUIntegerCoordinate = 0,
  #[alias = "mipLevelCount"]
  pub mip_level_count: GPUIntegerCoordinate,
  #[alias = "baseArrayLayer"]
  pub base_array_layer: GPUIntegerCoordinate = 0,
  #[alias = "arrayLayerCount"]
  pub array_layer_count: GPUIntegerCoordinate,
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUTextureView {
}
GPUTextureView includes GPUObjectBase;

// Sampler
pub enum GPUAddressMode {
  #[alias = "clamp-to-edge"]
  ClampToEdge,
  #[alias = "repeat"]
  Repeat,
  #[alias = "mirror-repeat"]
  MirrorRepeat
}

pub enum GPUFilterMode {
  #[alias = "nearest"]
  Nearest,
  #[alias = "linear"]
  Linear,
}

pub enum GPUComparefunction {
  #[alias = "never"]
  Never,
  #[alias = "less"]
  Less,
  #[alias = "equal"]
  Equal,
  #[alias = "less-equal"]
  LessEqual,
  #[alias = "greater"]
  Greater,
  #[alias = "not-equal"]
  NotEqual,
  #[alias = "greater-equal"]
  GreaterEqual,
  #[alias = "always"]
  Always
}

pub struct GPUSamplerDescriptor: GPUObjectDescriptorBase {
  #[alias = "addressModeU"]
  pub address_modeU: GPUAddressMode = "clamp-to-edge",
  #[alias = "addressModeV"]
  pub address_modeV: GPUAddressMode = "clamp-to-edge",
  #[alias = "addressModeW"]
  pub address_modeW: GPUAddressMode = "clamp-to-edge",
  #[alias = "magFilter"]
  pub mag_filter: GPUFilterMode = "nearest",
  #[alias = "minFilter"]
  pub min_filter: GPUFilterMode = "nearest",
  #[alias = "mipmapFilter"]
  pub mipmap_filter: GPUFilterMode = "nearest",
  #[alias = "lodMinClamp"]
  pub lod_min_clamp: f32 = 0,
  #[alias = "lodMaxClamp"]
  pub lod_max_clamp: f32 = 1000.0, // TODO: What should this Option<be>
  pub compare: GPUComparefunction,
  #[Clamp]
  #[alias = "maxAnisotropy"]
  pub max_anisotropy: u16 = 1,
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUSampler {
}
GPUSampler includes GPUObjectBase;

pub enum GPUTextureComponentType {
  #[alias = "f32"]
  F32,
  #[alias = "sint"]
  Sint,
  #[alias = "uint"]
  Uint,
  #[alias = "depth-comparison"]
  DepthComparison
}

// ****************************************************************************
// BINDING MODEL (layout: bindgroup, bindgroup)
// ****************************************************************************

// PipelineLayout
pub struct GPUPipelineLayoutDescriptor: GPUObjectDescriptorBase {
  required Vec<GPUBindGroupLayout> bindGroupLayouts;
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUPipelineLayout {
}
GPUPipelineLayout includes GPUObjectBase;

// BindGroupLayout
pub type GPUShaderStageFlags = #[EnforceRange] u32;
#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUShaderStage {
  const GPUShaderStageFlags VERTEX = 1;
  const GPUShaderStageFlags FRAGMENT = 2;
  const GPUShaderStageFlags COMPUTE = 4;
}

pub enum GPUBufferBindingType {
  #[alias = "uniform"]
  Uniform,
  #[alias = "storage"]
  Storage,
  #[alias = "read-only-storage"]
  ReadOnlyStorage,
}

pub struct GPUBufferBindingLayout {
  pub type: GPUBufferBindingType = "uniform",
  #[alias = "hasDynamicOffset"]
  pub has_dynamic_offset: bool = false,
  #[alias = "minBindingSize"]
  pub min_binding_size: GPUSize64 = 0,
}

pub enum GPUSamplerBindingType {
  #[alias = "filtering"]
  Filtering,
  #[alias = "non-filtering"]
  NonFiltering,
  #[alias = "comparison"]
  Comparison,
}

pub struct GPUSamplerBindingLayout {
  pub type: GPUSamplerBindingType = "filtering",
}

pub enum GPUTextureSampleType {
  #[alias = "f32"]
  F32,
  #[alias = "unfilterable-f32"]
  UnfilterableF32,
  #[alias = "depth"]
  Depth,
  #[alias = "sint"]
  Sint,
  #[alias = "uint"]
  Uint,
}

pub struct GPUTextureBindingLayout {
  #[alias = "sampleType"]
  pub sample_type: GPUTextureSampleType = "f32",
  #[alias = "viewDimension"]
  pub view_dimension: GPUTextureViewDimension = "2d",
  pub multisampled: bool = false,
}

pub enum GPUStorageTextureAccess {
  #[alias = "write-only"]
  WriteOnly,
}

pub struct GPUStorageTextureBindingLayout {
  pub access: GPUStorageTextureAccess = "write-only",
  required GPUTextureFormat format;
  #[alias = "viewDimension"]
  pub view_dimension: GPUTextureViewDimension = "2d",
}

pub struct GPUBindGroupLayoutEntry {
  required GPUIndex32 binding;
  required GPUShaderStageFlags visibility;
  pub buffer: GPUBufferBindingLayout,
  pub sampler: GPUSamplerBindingLayout,
  pub texture: GPUTextureBindingLayout,
  #[alias = "storageTexture"]
  pub storage_texture: GPUStorageTextureBindingLayout,
}

pub struct GPUBindGroupLayoutDescriptor: GPUObjectDescriptorBase {
  required Vec<GPUBindGroupLayoutEntry> entries;
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUBindGroupLayout {
}
GPUBindGroupLayout includes GPUObjectBase;

// BindGroup
pub struct GPUBufferBinding {
  required GPUBuffer buffer;
  pub offset: GPUSize64 = 0,
  pub size: GPUSize64,
}

pub type GPUBindingResource = (GPUSampler or GPUTextureView or GPUBufferBinding);

pub struct GPUBindGroupEntry {
  required GPUIndex32 binding;
  required GPUBindingResource resource;
}

pub struct GPUBindGroupDescriptor: GPUObjectDescriptorBase {
  required GPUBindGroupLayout layout;
  required Vec<GPUBindGroupEntry> entries;
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUBindGroup {
}
GPUBindGroup includes GPUObjectBase;

//
// PIPELINE CREATION (state: blend, state: blend, ..., pipelines)
//

pub enum GPUCompilationMessageType {
  #[alias = "error"]
  Error,
  #[alias = "warning"]
  Warning,
  #[alias = "info"]
  Info
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUCompilationMessage {
  message: DOMString,
  type: GPUCompilationMessageType,
  #[alias = "lineNum"]
  line_num: u64,
  #[alias = "linePos"]
  line_pos: u64,
  offset: u64,
  length: u64,
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUCompilationInfo {
  //TODO:
  //#[Cached, Frozen, pure]
  //readonly attribute Vec<GPUCompilationMessage> messages;
}

// ShaderModule

pub struct GPUShaderModuleDescriptor: GPUObjectDescriptorBase {
  // UTF8String is not observably different from USVString
  required UTF8String code;
  #[alias = "sourceMap"]
  pub source_map: object,
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUShaderModule {
  //TODO:
  //Promise<GPUCompilationInfo> compilationInfo();
}
GPUShaderModule includes GPUObjectBase;

// Common stuff for ComputePipeline and RenderPipeline
pub struct GPUPipelineDescriptorBase: GPUObjectDescriptorBase {
  pub layout: GPUPipelineLayout,
}

pub trait mixin GPUPipelineBase {
  #[alias = "getBindGroupLayout"]
  pub fn get_bind_group_layout(index: u32) -> GPUBindGroupLayout;
}

pub struct GPUProgrammableStage {
  required GPUShaderModule module;
  required USVString entryPoint;
}

// ComputePipeline
pub struct GPUComputePipelineDescriptor: GPUPipelineDescriptorBase {
  required GPUProgrammableStage compute;
}

//TODO: Serializable
// https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1696219
#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUComputePipeline {
}
GPUComputePipeline includes GPUObjectBase;
GPUComputePipeline includes GPUPipelineBase;

// InputState
pub enum GPUIndexFormat {
  #[alias = "uint16"]
  Uint16,
  #[alias = "uint32"]
  Uint32,
}

pub enum GPUVertexFormat {
  #[alias = "uint8x2"]
  Uint8x2,
  #[alias = "uint8x4"]
  Uint8x4,
  #[alias = "sint8x2"]
  Sint8x2,
  #[alias = "sint8x4"]
  Sint8x4,
  #[alias = "unorm8x2"]
  Unorm8x2,
  #[alias = "unorm8x4"]
  Unorm8x4,
  #[alias = "snorm8x2"]
  Snorm8x2,
  #[alias = "snorm8x4"]
  Snorm8x4,
  #[alias = "uint16x2"]
  Uint16x2,
  #[alias = "uint16x4"]
  Uint16x4,
  #[alias = "sint16x2"]
  Sint16x2,
  #[alias = "sint16x4"]
  Sint16x4,
  #[alias = "unorm16x2"]
  Unorm16x2,
  #[alias = "unorm16x4"]
  Unorm16x4,
  #[alias = "snorm16x2"]
  Snorm16x2,
  #[alias = "snorm16x4"]
  Snorm16x4,
  #[alias = "float16x2"]
  Float16x2,
  #[alias = "float16x4"]
  Float16x4,
  #[alias = "float32"]
  Float32,
  #[alias = "float32x2"]
  Float32x2,
  #[alias = "float32x3"]
  Float32x3,
  #[alias = "float32x4"]
  Float32x4,
  #[alias = "uint32"]
  Uint32,
  #[alias = "uint32x2"]
  Uint32x2,
  #[alias = "uint32x3"]
  Uint32x3,
  #[alias = "uint32x4"]
  Uint32x4,
  #[alias = "sint32"]
  Sint32,
  #[alias = "sint32x2"]
  Sint32x2,
  #[alias = "sint32x3"]
  Sint32x3,
  #[alias = "sint32x4"]
  Sint32x4,
}

pub enum GPUVertexStepMode {
  #[alias = "vertex"]
  Vertex,
  #[alias = "instance"]
  Instance,
}

pub struct GPUVertexAttribute {
  required GPUVertexFormat format;
  required GPUSize64 offset;
  required GPUIndex32 shaderLocation;
}

pub struct GPUVertexBufferLayout {
  required GPUSize64 arrayStride;
  #[alias = "stepMode"]
  pub step_mode: GPUVertexStepMode = "vertex",
  required Vec<GPUVertexAttribute> attributes;
}

pub struct GPUVertexState: GPUProgrammableStage {
  Vec<Option<GPUVertexBufferLayout>> buffers = [];
}

// GPURenderPipeline
pub enum GPUPrimitiveTopology {
  #[alias = "point-list"]
  PointList,
  #[alias = "line-list"]
  LineList,
  #[alias = "line-strip"]
  LineStrip,
  #[alias = "triangle-list"]
  TriangleList,
  #[alias = "triangle-strip"]
  TriangleStrip
}

pub enum GPUFrontFace {
  #[alias = "ccw"]
  Ccw,
  #[alias = "cw"]
  Cw
}

pub enum GPUCullMode {
  #[alias = "none"]
  None,
  #[alias = "front"]
  Front,
  #[alias = "back"]
  Back
}

pub struct GPUPrimitiveState {
  pub topology: GPUPrimitiveTopology = "triangle-list",
  #[alias = "stripIndexFormat"]
  pub strip_index_format: GPUIndexFormat,
  #[alias = "frontFace"]
  pub front_face: GPUFrontFace = "ccw",
  #[alias = "cullMode"]
  pub cull_mode: GPUCullMode = "none",
  // Enable depth clamping (requires "depth-clamping" feature)
  #[alias = "clampDepth"]
  pub clamp_depth: bool = false,
}

pub struct GPUMultisampleState {
  pub count: GPUSize32 = 1,
  pub mask: GPUSampleMask = 0xFFFFFFFF,
  #[alias = "alphaToCoverageEnabled"]
  pub alpha_to_coverageEnabled: bool = false,
}

// BlendState
pub enum GPUBlendFactor {
  #[alias = "zero"]
  Zero,
  #[alias = "one"]
  One,
  #[alias = "src"]
  Src,
  #[alias = "one-minus-src"]
  OneMinusSrc,
  #[alias = "src-alpha"]
  SrcAlpha,
  "one-minus-src-alpha",
  #[alias = "dst"]
  Dst,
  #[alias = "one-minus-dst"]
  OneMinusDst,
  #[alias = "dst-alpha"]
  DstAlpha,
  "one-minus-dst-alpha",
  #[alias = "src-alpha-saturated"]
  SrcAlphaSaturated,
  #[alias = "constant"]
  Constant,
  #[alias = "one-minus-constant"]
  OneMinusConstant,
}

pub enum GPUBlendOperation {
  #[alias = "add"]
  Add,
  #[alias = "subtract"]
  Subtract,
  #[alias = "reverse-subtract"]
  ReverseSubtract,
  #[alias = "min"]
  Min,
  #[alias = "max"]
  Max
}

pub struct GPUBlendComponent {
  #[alias = "srcFactor"]
  pub src_factor: GPUBlendFactor = "one",
  #[alias = "dstFactor"]
  pub dst_factor: GPUBlendFactor = "zero",
  pub operation: GPUBlendOperation = "add",
}

pub struct GPUBlendState {
  required GPUBlendComponent color;
  required GPUBlendComponent alpha;
}

pub type GPUColorWriteFlags = #[EnforceRange] u32;
#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUColorWrite {
  const GPUColorWriteFlags RED = 0x1;
  const GPUColorWriteFlags GREEN = 0x2;
  const GPUColorWriteFlags BLUE = 0x4;
  const GPUColorWriteFlags ALPHA = 0x8;
  const GPUColorWriteFlags ALL = 0xF;
}

pub struct GPUColorTargetState {
  required GPUTextureFormat format;
  pub blend: GPUBlendState,
  #[alias = "writeMask"]
  pub write_mask: GPUColorWriteFlags = 0xF, // GPUColorWrite.ALL
}

pub struct GPUFragmentState: GPUProgrammableStage {
  required Vec<GPUColorTargetState> targets;
}

// DepthStencilState
pub enum GPUStencilOperation {
  #[alias = "keep"]
  Keep,
  #[alias = "zero"]
  Zero,
  #[alias = "replace"]
  Replace,
  #[alias = "invert"]
  Invert,
  #[alias = "increment-clamp"]
  IncrementClamp,
  #[alias = "decrement-clamp"]
  DecrementClamp,
  #[alias = "increment-wrap"]
  IncrementWrap,
  #[alias = "decrement-wrap"]
  DecrementWrap
}

pub struct GPUStencilFaceState {
  pub compare: GPUComparefunction = "always",
  pub failOp: GPUStencilOperation = "keep",
  #[alias = "depthFailOp"]
  pub depth_fail_op: GPUStencilOperation = "keep",
  pub passOp: GPUStencilOperation = "keep",
}

pub struct GPUDepthStencilState {
  required GPUTextureFormat format;

  #[alias = "depthWriteEnabled"]
  pub depth_write_enabled: bool = false,
  #[alias = "depthCompare"]
  pub depth_compare: GPUComparefunction = "always",

  #[alias = "stencilFront"]
  pub stencil_front: GPUStencilFaceState = {},
  #[alias = "stencilBack"]
  pub stencil_back: GPUStencilFaceState = {},

  #[alias = "stencilReadMask"]
  pub stencil_read_mask: GPUStencilValue = 0xFFFFFFFF,
  #[alias = "stencilWriteMask"]
  pub stencil_write_mask: GPUStencilValue = 0xFFFFFFFF,

  #[alias = "depthBias"]
  pub depth_bias: GPUDepthBias = 0,
  #[alias = "depthBiasSlopeScale"]
  pub depth_bias_slopeScale: f32 = 0,
  #[alias = "depthBiasClamp"]
  pub depth_bias_clamp: f32 = 0,
}

pub struct GPURenderPipelineDescriptor: GPUPipelineDescriptorBase {
  required GPUVertexState vertex;
  pub primitive: GPUPrimitiveState = {},
  #[alias = "depthStencil"]
  pub depth_stencil: GPUDepthStencilState,
  pub multisample: GPUMultisampleState = {},
  pub fragment: GPUFragmentState,
}

//TODO: Serializable
// https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1696219
#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPURenderPipeline {
}
GPURenderPipeline includes GPUObjectBase;
GPURenderPipeline includes GPUPipelineBase;

//
// COMMAND RECORDING (Command buffer and all relevant structures)
//

pub enum GPULoadOp {
  #[alias = "load"]
  Load
}

pub enum GPUStoreOp {
  #[alias = "store"]
  Store,
  #[alias = "discard"]
  Discard
}

pub struct GPURenderPassColorAttachment {
  required GPUTextureView view;
  #[alias = "resolveTarget"]
  pub resolve_target: GPUTextureView,

  required (GPULoadOp or GPUColor) loadValue;
  required GPUStoreOp storeOp;
}

pub struct GPURenderPassDepthStencilAttachment {
  required GPUTextureView view;

  required (GPULoadOp or f32) depthLoadValue;
  required GPUStoreOp depthStoreOp;

  required (GPULoadOp or GPUStencilValue) stencilLoadValue;
  required GPUStoreOp stencilStoreOp;
}

pub struct GPURenderPassDescriptor: GPUObjectDescriptorBase {
  required Vec<GPURenderPassColorAttachment> colorAttachments;
  #[alias = "depthStencilAttachment"]
  pub depth_stencil_attachment: GPURenderPassDepthStencilAttachment,
  #[alias = "occlusionQuerySet"]
  pub occlusion_query_set: GPUQuerySet,
}

pub struct GPUImageDataLayout {
  pub offset: GPUSize64 = 0,
  required GPUSize32 bytesPerRow;
  #[alias = "rowsPerImage"]
  pub rows_per_image: GPUSize32 = 0,
}

pub struct GPUImageCopyBuffer: GPUImageDataLayout {
  required GPUBuffer buffer;
}

pub struct GPUImageCopyTexture {
  required GPUTexture texture;
  #[alias = "mipLevel"]
  pub mip_level: GPUIntegerCoordinate = 0,
  pub origin: GPUOrigin3D,
  pub aspect: GPUTextureAspect = "all",
}

pub struct GPUImageBitmapCopyView {
  //required ImageBitmap imageBitmap; //TODO
  pub origin: GPUOrigin2D,
}

pub struct GPUCommandEncoderDescriptor: GPUObjectDescriptorBase {
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUCommandEncoder {
  #[new_object]
  #[alias = "beginComputePass"]
  pub fn begin_compute_pass(#[optional = {}] descriptor: GPUComputePassDescriptor) -> GPUComputePassEncoder;
  #[new_object]
  #[alias = "beginRenderPass"]
  pub fn begin_render_pass(descriptor: GPURenderPassDescriptor) -> GPURenderPassEncoder;

  void copyBufferToBuffer(
  source: GPUBuffer,
  GPUSize64 sourceOffset,
  destination: GPUBuffer,
  GPUSize64 destinationOffset,
  size: GPUSize64);

  void copyBufferToTexture(
  source: GPUImageCopyBuffer,
  destination: GPUImageCopyTexture,
  GPUExtent3D copySize);

  void copyTextureToBuffer(
  source: GPUImageCopyTexture,
  destination: GPUImageCopyBuffer,
  GPUExtent3D copySize);

  void copyTextureToTexture(
  source: GPUImageCopyTexture,
  destination: GPUImageCopyTexture,
  GPUExtent3D copySize);

  //
  void copyImageBitmapToTexture(
  source: GPUImageBitmapCopyView,
  destination: GPUImageCopyTexture,
  GPUExtent3D copySize);

  #[alias = "pushDebugGroup"]
  pub fn push_debug_group(groupLabel: USVString);
  #[alias = "popDebugGroup"]
  pub fn pop_debug_group();
  #[alias = "insertDebugMarker"]
  pub fn insert_debug_marker(markerLabel: USVString);

  #[new_object]
  pub fn finish(#[optional = {}] descriptor: GPUCommandBufferDescriptor) -> GPUCommandBuffer;
}
GPUCommandEncoder includes GPUObjectBase;

pub trait mixin GPUProgrammablePassEncoder {
  void setBindGroup(index: GPUIndex32, index: GPUIndex32,
  optional Vec<GPUBufferDynamicOffset> dynamicOffsets = []);

  #[alias = "pushDebugGroup"]
  pub fn push_debug_group(groupLabel: USVString);
  #[alias = "popDebugGroup"]
  pub fn pop_debug_group();
  #[alias = "insertDebugMarker"]
  pub fn insert_debug_marker(markerLabel: USVString);
}

// Render Pass
pub trait mixin GPURenderEncoderBase {
  #[alias = "setPipeline"]
  pub fn set_pipeline(pipeline: GPURenderPipeline);

  #[alias = "setIndexBuffer"]
  pub fn set_index_buffer(buffer: GPUBuffer, buffer: GPUBuffer, #[optional = 0] offset: GPUSize64, #[optional = 0] size: GPUSize64);
  #[alias = "setVertexBuffer"]
  pub fn set_vertex_buffer(slot: GPUIndex32, slot: GPUIndex32, #[optional = 0] offset: GPUSize64, #[optional = 0] size: GPUSize64);

  void draw(vertexCount: GPUSize32,
  #[optional = 1] instanceCount: GPUSize32,
  #[optional = 0] firstVertex: GPUSize32,
  #[optional = 0] firstInstance: GPUSize32);
  void drawIndexed(indexCount: GPUSize32,
  #[optional = 1] instanceCount: GPUSize32,
  #[optional = 0] firstIndex: GPUSize32,
  #[optional = 0] baseVertex: GPUSignedOffset32,
  #[optional = 0] firstInstance: GPUSize32);

  #[alias = "drawIndirect"]
  pub fn draw_indirect(indirectBuffer: GPUBuffer, indirectBuffer: GPUBuffer);
  #[alias = "drawIndexedIndirect"]
  pub fn draw_indexed_indirect(indirectBuffer: GPUBuffer, indirectBuffer: GPUBuffer);
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPURenderPassEncoder {
  void setViewport(x: f32, x: f32,
  width: f32, height: f32,
  f32 minDepth, maxDepth: f32);

  void setScissorRect(x: GPUIntegerCoordinate, x: GPUIntegerCoordinate,
  width: GPUIntegerCoordinate, height: GPUIntegerCoordinate);

  #[alias = "setBlendConstant"]
  pub fn set_blend_constant(color: GPUColor);
  #[alias = "setStencilReference"]
  pub fn set_stencil_reference(reference: GPUStencilValue);

  //void beginOcclusionQuery(queryIndex: GPUSize32);
  //void endOcclusionQuery();

  //void beginPipelineStatisticsQuery(querySet: GPUQuerySet, querySet: GPUQuerySet);
  //void endPipelineStatisticsQuery();

  //void writeTimestamp(querySet: GPUQuerySet, querySet: GPUQuerySet);

  #[alias = "executeBundles"]
  pub fn execute_bundles(bundles: Vec<GPURenderBundle>);

  #[throws]
  #[alias = "endPass"]
  pub fn end_pass();
}
GPURenderPassEncoder includes GPUObjectBase;
GPURenderPassEncoder includes GPUProgrammablePassEncoder;
GPURenderPassEncoder includes GPURenderEncoderBase;

// Compute Pass
pub struct GPUComputePassDescriptor: GPUObjectDescriptorBase {
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUComputePassEncoder {
  #[alias = "setPipeline"]
  pub fn set_pipeline(pipeline: GPUComputePipeline);
  pub fn dispatch(x: GPUSize32, #[optional = 1] y: GPUSize32, #[optional = 1] z: GPUSize32);
  #[alias = "dispatchIndirect"]
  pub fn dispatch_indirect(indirectBuffer: GPUBuffer, indirectBuffer: GPUBuffer);

  #[throws]
  #[alias = "endPass"]
  pub fn end_pass();
}
GPUComputePassEncoder includes GPUObjectBase;
GPUComputePassEncoder includes GPUProgrammablePassEncoder;

// Command Buffer
pub struct GPUCommandBufferDescriptor: GPUObjectDescriptorBase {
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUCommandBuffer {
}
GPUCommandBuffer includes GPUObjectBase;

// Render Bundle
#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPURenderBundle {
}
GPURenderBundle includes GPUObjectBase;

pub struct GPURenderBundleDescriptor: GPUObjectDescriptorBase {
}

pub struct GPURenderBundleEncoderDescriptor: GPURenderPassLayout {
  #[alias = "depthReadOnly"]
  pub depth_read_only: bool = false,
  #[alias = "stencilReadOnly"]
  pub stencil_read_only: bool = false,
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPURenderBundleEncoder {
  pub fn finish(#[optional = {}] descriptor: GPURenderBundleDescriptor) -> GPURenderBundle;
}
GPURenderBundleEncoder includes GPUObjectBase;
GPURenderBundleEncoder includes GPUProgrammablePassEncoder;
GPURenderBundleEncoder includes GPURenderEncoderBase;

pub struct GPURenderPassLayout: GPUObjectDescriptorBase {
  required Vec<GPUTextureFormat> colorFormats;
  #[alias = "depthStencilFormat"]
  pub depth_stencil_format: GPUTextureFormat,
  #[alias = "sampleCount"]
  pub sample_count: GPUSize32 = 1,
}

//
// OTHER (Canvas, Query, Queue, Device)
//

// Query set
pub enum GPUQueryType {
  #[alias = "occlusion"]
  Occlusion,
  #[alias = "pipeline-statistics"]
  PipelineStatistics,
  #[alias = "timestamp"]
  Timestamp
}

pub enum GPUPipelineStatisticName {
  #[alias = "vertex-shader-invocations"]
  VertexShaderInvocations,
  #[alias = "clipper-invocations"]
  ClipperInvocations,
  #[alias = "clipper-primitives-out"]
  ClipperPrimitivesOut,
  #[alias = "fragment-shader-invocations"]
  FragmentShaderInvocations,
  #[alias = "compute-shader-invocations"]
  ComputeShaderInvocations
}

pub struct GPUQuerySetDescriptor: GPUObjectDescriptorBase {
  required GPUQueryType type;
  required GPUSize32 count;
  #[alias = "pipelineStatistics"]
  pub pipeline_statistics: Vec<GPUPipelineStatisticName> = [],
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUQuerySet {
  pub fn destroy();
}
GPUQuerySet includes GPUObjectBase;

//TODO: use #[AllowShared] on BufferSource
// https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1696216
// https://github.com/heycam/webidl/issues/961

// Queue
#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUQueue {
  pub fn submit(buffers: Vec<GPUCommandBuffer>);

  //TODO:
  //Promise<void> onSubmittedWorkDone();

  #[throws]
  void writeBuffer(
  buffer: GPUBuffer,
  GPUSize64 bufferOffset,
  data: BufferSource,
  #[optional = 0] dataOffset: GPUSize64,
  optional size: GPUSize64);

  #[throws]
  void writeTexture(
  destination: GPUImageCopyTexture,
  data: BufferSource,
  GPUImageDataLayout dataLayout,
  size: GPUExtent3D);
}
GPUQueue includes GPUObjectBase;

pub struct GPUCanvasConfiguration {
  required GPUDevice device;
  required GPUTextureFormat format;
  pub usage: GPUTextureUsageFlags = 0x10, //GPUTextureUsage.OUTPUT_ATTACHMENT
  //GPUPredefinedColorSpace colorSpace = "srgb"; //TODO
  #[alias = "compositingAlphaMode"]
  pub compositing_alpha_mode: GPUCanvasCompositingAlphaMode = "opaque",
  pub size: GPUExtent3D,
}

pub enum GPUCanvasCompositingAlphaMode {
  #[alias = "opaque"]
  Opaque,
  #[alias = "premultiplied"]
  Premultiplied,
}

#[pref = "dom.webgpu.enabled",
  exposed = Window]
pub trait GPUCanvasContext {
  // Calling configure() a second time invalidates the previous one,
  // and all of the textures it's produced.
  pub fn configure(descriptor: GPUCanvasConfiguration);
  pub fn unconfigure();

  #[alias = "getPreferredFormat"]
  pub fn get_preferred_format(adapter: GPUAdapter) -> GPUTextureFormat;

  #[alias = "getCurrentTexture"]
  pub fn get_current_texture() -> Result<GPUTexture>;
}
