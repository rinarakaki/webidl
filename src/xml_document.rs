// The origin of this IDL file is:
// http://dom.spec.whatwg.org/#xmldocument

// http://dom.spec.whatwg.org/#xmldocument
#[exposed = Window]
pub trait XMLDocument: Document {};
