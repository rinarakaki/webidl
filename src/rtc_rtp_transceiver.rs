// The origin of this IDL file is
// http://w3c.github.io/webrtc-pc/#rtcrtptransceiver-trait

pub enum RTCRtpTransceiverDirection {
  #[alias = "sendrecv"]
  SendReceive,
  #[alias = "sendonly"]
  SendOnly,
  #[alias = "recvonly"]
  ReceiveOnly,
  #[alias = "inactive"]
  Inactive
}

#[alias = "RTCRtpTransceiverInit"]
pub struct RTCRTPTransceiverInit {
  pub direction: RTCRtpTransceiverDirection = "sendrecv",
  pub streams: Vec<MediaStream> = [],
  // TODO: bug 1396918
  // Vec<RTCRtpEncodingParameters> sendEncodings;
}

#[pref = "media.peerconnection.enabled",
  JSImplementation = "@mozilla.org/dom/rtptransceiver;1",
  exposed = Window]
#[alias = "RTCRtpTransceiver"]
pub trait RTCRTPTransceiver {
  mid: Option<DOMString>,
  #[same_object]
  sender: RTCRtpSender,
  #[same_object]
  receiver: RTCRtpReceiver,
  stopped: bool,
  pub direction: RTCRtpTransceiverDirection,
  #[alias = "currentDirection"]
  current_direction: Option<RTCRtpTransceiverDirection>,

  pub fn stop();
  // TODO: bug 1396922
  // #[alias = "setCodecPreferences"]
  // fn setCodecPreferences(codecs: Vec<RTCRtpCodecCapability>);

  #[chrome_only]
  #[alias = "setAddTrackMagic"]
  pub fn set_add_track_magic();
  #[chrome_only]
  #[alias = "addTrackMagic"]
  add_track_magic: bool,
  #[chrome_only]
  #[alias = "shouldRemove"]
  pub should_remove: bool,
  #[chrome_only]
  #[alias = "setCurrentDirection"]
  pub fn set_current_direction(direction: RTCRtpTransceiverDirection);
  #[chrome_only]
  #[alias = "setDirectionInternal"]
  pub fn set_direction_internal(direction: RTCRtpTransceiverDirection);
  #[chrome_only]
  #[alias = "setMid"]
  pub fn set_mid(mid: DOMString);
  #[chrome_only]
  #[alias = "unsetMid"]
  pub fn unset_mid();
  #[chrome_only]
  #[alias = "setStopped"]
  pub fn set_stopped();

  #[chrome_only]
  #[alias = "getKind"]
  pub fn get_kind() -> DOMString;
  #[chrome_only]
  #[alias = "hasBeenUsedToSend"]
  pub fn has_been_used_to_send() -> bool;
  #[chrome_only]
  pub fn sync();
}

