// The origin of this IDL file is
// https://w3c.github.io/gamepad/
// https://w3c.github.io/gamepad/extensions.html
// https://w3c.github.io/webvr/spec/1.1/#trait-gamepad

#[pref = "dom.gamepad.enabled",
 exposed = Window,
 SecureContext]
pub trait GamepadButton {
  pressed: bool,
  touched: bool,
  value: f64,
}

pub enum GamepadHand {
  "",
  #[alias = "left"]
  Left,
  #[alias = "right"]
  Right
}

// https://www.w3.org/TR/gamepad/#gamepadmappingtype-enum
// https://immersive-web.github.io/webxr-gamepads-module/#enumdef-gamepadmappingtype
pub enum GamepadMappingType {
  "",
  #[alias = "standard"]
  Standard,
  #[alias = "xr-standard"]
  XrStandard
}

#[pref = "dom.gamepad.enabled",
 exposed = Window,
 SecureContext]
pub trait Gamepad {
  // An identifier, unique per type of device.
  id: DOMString,

  // The game port index for the device. Unique per device
  // attached to this system.
  index: i32,

  // The mapping in use for this device. The empty string
  // indicates that no mapping is in use.
  mapping: GamepadMappingType,

  // The hand in use for this device. The empty string
  // indicates that unknown, hands: both, or not applicable
  #[pref = "dom.gamepad.extensions.enabled"]
  hand: GamepadHand,

  // The displayId in use for as an association point in the VRDisplay API
  // to identify which VRDisplay that the gamepad is associated with.
  #[pref = "dom.vr.enabled"]
  displayId: u32,

  // true if this gamepad is currently connected to the system.
  connected: bool,

  // The current state of all buttons on the device, an
  // array of GamepadButton.
  #[pure, Cached, Frozen]
  buttons: Vec<GamepadButton>,

  // The current position of all axes on the device, an
  // array of doubles.
  #[pure, Cached, Frozen]
  axes: Vec<f64>,

  // Timestamp from when the data of this device was last updated.
  timestamp: DOMHighResTimeStamp,

  // The current pose of the device, a GamepadPose.
  #[pref = "dom.gamepad.extensions.enabled"]
  pose: Option<GamepadPose>,

  // The current haptic actuator of the device, an array of
  // GamepadHapticActuator.
  #[constant, Cached, Frozen, pref = "dom.gamepad.extensions.enabled"]
  #[alias = "hapticActuators"]
  haptic_actuators: Vec<GamepadHapticActuator>,

  #[constant, Cached, Frozen, pref = "dom.gamepad.extensions.enabled", pref = "dom.gamepad.extensions.lightindicator"]
  #[alias = "lightIndicators"]
  light_indicators: Vec<GamepadLightIndicator>,

  #[constant, Cached, Frozen, pref = "dom.gamepad.extensions.enabled", pref = "dom.gamepad.extensions.multitouch"]
  #[alias = "touchEvents"]
  touch_events: Vec<GamepadTouch>,
}
