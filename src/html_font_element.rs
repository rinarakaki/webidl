// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLFontElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub color: DOMString,
  #[ce_reactions, setter_throws]
  pub face: DOMString,
  #[ce_reactions, setter_throws]
  pub size: DOMString,
}
