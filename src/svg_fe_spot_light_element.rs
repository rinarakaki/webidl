// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFESpotLightElement: SVGElement {
  #[constant]
  x: SVGAnimatedNumber,
  #[constant]
  y: SVGAnimatedNumber,
  #[constant]
  z: SVGAnimatedNumber,
  #[constant]
  #[alias = "pointsAtX"]
  points_at_x: SVGAnimatedNumber,
  #[constant]
  #[alias = "pointsAtY"]
  points_at_y: SVGAnimatedNumber,
  #[constant]
  #[alias = "pointsAtZ"]
  points_at_z: SVGAnimatedNumber,
  #[constant]
  #[alias = "specularExponent"]
  specular_exponent: SVGAnimatedNumber,
  #[constant]
  #[alias = "limitingConeAngle"]
  limiting_cone_angle: SVGAnimatedNumber,
}
