// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

pub struct ConvolverOptions: AudioNodeOptions {
  pub buffer: Option<AudioBuffer>,
  #[alias = "disableNormalization"]
  pub #[optional = false] disable_normalisation: bool,
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait ConvolverNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(
    context: BaseAudioContext,
    #[optional = {}] options: ConvolverOptions) -> Result<Self>;

  #[setter_throws]
  pub mut buffer: Option<AudioBuffer>,
  #[alias = "normalize"]
  pub mut normalise: bool,

}

// Mozilla extension
ConvolverNode includes AudioNodePassThrough;

