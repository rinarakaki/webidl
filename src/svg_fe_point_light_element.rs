// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEPointLightElement: SVGElement {
  #[constant]
  x: SVGAnimatedNumber,
  #[constant]
  y: SVGAnimatedNumber,
  #[constant]
  z: SVGAnimatedNumber,
}
