// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGRect {
  #[setter_throws]
  pub x: f32,
  #[setter_throws]
  pub y: f32,
  #[setter_throws]
  pub width: f32,
  #[setter_throws]
  pub height: f32,
}
