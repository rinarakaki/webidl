// The origin of this IDL file is
// http://dom.spec.whatwg.org/#comment

use super::*;

#[exposed = Window]
pub trait Comment: CharacterData {
  #[alias = "constructor"]
  pub fn new(#[optional = ""] data: DOMString) -> Result<Self>;
}
