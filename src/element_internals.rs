// The origin of this IDL file is
// https://html.spec.whatwg.org/#elementinternals

#[pref = "dom.webcomponents.elementInternals.enabled", exposed = Window]
pub trait ElementInternals {
  // Shadow root access
  #[alias = "shadowRoot"]
  shadow_root: Option<ShadowRoot>,

  // Form-associated custom elements
  #[pref = "dom.webcomponents.formAssociatedCustomElement.enabled", throws]
  void setFormValue((File or USVString or FormData)? value,
  optional (File or USVString or FormData)? state);

  #[pref = "dom.webcomponents.formAssociatedCustomElement.enabled", throws]
  form: Option<HTMLFormElement>,

  #[pref = "dom.webcomponents.formAssociatedCustomElement.enabled", throws]
  void setValidity(#[optional = {}] flags: ValidityStateFlags,
  optional message: DOMString,
  optional anchor: HTMLElement);
  #[pref = "dom.webcomponents.formAssociatedCustomElement.enabled", throws]
  #[alias = "willValidate"]
  will_validate: bool,
  #[pref = "dom.webcomponents.formAssociatedCustomElement.enabled", throws]
  validity: ValidityState,
  #[pref = "dom.webcomponents.formAssociatedCustomElement.enabled", throws]
  #[alias = "validationMessage"]
  validation_message: DOMString,
  #[pref = "dom.webcomponents.formAssociatedCustomElement.enabled", throws]
  #[alias = "checkValidity"]
  pub fn check_validity() -> bool;
  #[pref = "dom.webcomponents.formAssociatedCustomElement.enabled", throws]
  #[alias = "reportValidity"]
  pub fn report_validity() -> bool;

  #[pref = "dom.webcomponents.formAssociatedCustomElement.enabled", throws]
  labels: NodeList,
}

partial trait ElementInternals {
  #[chrome_only, throws]
  #[alias = "validationAnchor"]
  validation_anchor: Option<HTMLElement>,
}

pub struct ValidityStateFlags {
  #[alias = "valueMissing"]
  pub value_missing: bool = false,
  #[alias = "typeMismatch"]
  pub type_mismatch: bool = false,
  #[alias = "patternMismatch"]
  pub pattern_mismatch: bool = false,
  #[alias = "tooLong"]
  pub too_long: bool = false,
  #[alias = "tooShort"]
  pub too_short: bool = false,
  #[alias = "rangeUnderflow"]
  pub range_underflow: bool = false,
  #[alias = "rangeOverflow"]
  pub range_overflow: bool = false,
  #[alias = "stepMismatch"]
  pub step_mismatch: bool = false,
  #[alias = "badInput"]
  pub bad_input: bool = false,
  #[alias = "customError"]
  pub custom_error: bool = false,
}
