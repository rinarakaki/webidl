// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-iframe-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
// https://wicg.github.io/feature-policy/#policy

#[exposed = Window]
pub trait HTMLIFrameElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws, pure]
  pub src: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub srcdoc: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub name: DOMString,
  #[put_forwards = value]
  sandbox: DOMTokenList,
  // attribute bool seamless;
  #[ce_reactions, setter_throws, pure]
  #[alias = "allowFullscreen"]
  pub allow_fullscreen: bool,
  #[ce_reactions, setter_throws, pure]
  pub width: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub height: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "referrerPolicy"]
  pub referrer_policy: DOMString,
  #[needs_subject_principal]
  #[alias = "contentDocument"]
  content_document: Option<Document>,
  #[alias = "contentWindow"]
  content_window: Option<WindowProxy>,
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLIFrameElement {
  #[ce_reactions, setter_throws, pure]
  pub align: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub scrolling: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "frameBorder"]
  pub frame_border: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "longDesc"]
  pub long_desc: DOMString,

  #[ce_reactions, setter_throws, pure]
  attribute #[LegacyNullToEmptyString] DOMString marginHeight;
  #[ce_reactions, setter_throws, pure]
  attribute #[LegacyNullToEmptyString] DOMString marginWidth;
}

partial trait HTMLIFrameElement {
  // GetSVGDocument
  #[needs_subject_principal]
  #[alias = "getSVGDocument"]
  pub fn get_svg_document() -> Option<Document>;
}

HTMLIFrameElement includes MozFrameLoaderOwner;

// https://w3c.github.io/webappsec-feature-policy/#idl-index
partial trait HTMLIFrameElement {
  #[same_object, pref = "dom.security.featurePolicy.webidl.enabled"]
  #[alias = "featurePolicy"]
  feature_policy: FeaturePolicy,

  #[ce_reactions, setter_throws, pure]
  pub allow: DOMString,
}
