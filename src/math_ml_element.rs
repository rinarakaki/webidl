// The origin of this IDL file is
// https://mathml-refresh.github.io/mathml-core/

#[exposed = Window]
pub trait MathMLElement: Element { };
MathMLElement includes GlobalEventHandlers;
MathMLElement includes HTMLOrForeignElement;
MathMLElement includes DocumentAndElementEventHandlers;
MathMLElement includes ElementCSSInlineStyle;
MathMLElement includes TouchEventHandlers;
MathMLElement includes OnErrorEventHandlerForNodes;
