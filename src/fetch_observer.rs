use super::EventHandler;

#[exposed = Window]
callback trait ObserverCallback {
  #[alias = "handleEvent"]
  pub fn handle_event(observer: FetchObserver);
}

pub enum FetchState {
  // Pending states
  #[alias = "requesting"]
  Requesting,
  #[alias = "responding"]
  Responding,

  // Final states
  #[alias = "aborted"]
  Aborted,
  #[alias = "errored"]
  Errored,
  #[alias = "complete"]
  Complete
}

#[exposed = (Window, Worker),
  pref = "dom.fetchObserver.enabled"]
pub trait FetchObserver: EventTarget {
  state: FetchState,

  // Events
  #[alias = "onstatechange"]
  pub on_state_change: EventHandler,
  #[alias = "onrequestprogress"]
  pub on_request_progress: EventHandler,
  #[alias = "onresponseprogress"]
  pub on_response_progress: EventHandler,
}
