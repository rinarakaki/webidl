// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-h1,-h2,-h3,-h4,-h5,-and-h6-elements
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-h1,-h2,-h3,-h4,-h5,-and-h6-elements
#[exposed = Window]
pub trait HTMLHeadingElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLHeadingElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
}
