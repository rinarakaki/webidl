// The origin of this IDL file is
// http://dev.w3.org/csswg/css-animations-2/#the-CSSAnimation-trait

#[func = "Document::IsWebAnimationsGetAnimationsEnabled",
 HeaderFile = "nsAnimationManager.h",
 exposed = Window]
pub trait CSSAnimation: Animation {
  #[constant]
  #[alias = "animationName"]
  animation_name: DOMString,
}
