// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/webrtc.html#idl-def-RTCDataChannelEvent

pub struct RTCDataChannelEventInit: EventInit {
  required RTCDataChannel channel;
}

#[pref = "media.peerconnection.enabled",
 exposed = Window]
pub trait RTCDataChannelEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;

  channel: RTCDataChannel,
}
