// The origin of this IDL file is
// http://www.whatwg.org/html/#the-select-element

#[exposed = Window]
pub trait HTMLSelectElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub autofocus: bool,
  #[ce_reactions, setter_throws, pure]
  pub autocomplete: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub disabled: bool,
  #[pure]
  form: Option<HTMLFormElement>,
  #[ce_reactions, setter_throws, pure]
  pub multiple: bool,
  #[ce_reactions, setter_throws, pure]
  pub name: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub required: bool,
  #[ce_reactions, setter_throws, pure]
  pub size: u32,

  #[chrome_only, pure]
  #[alias = "isCombobox"]
  is_combobox: bool,

  #[pure]
  type: DOMString,

  #[constant]
  options: HTMLOptionsCollection,
  #[ce_reactions, setter_throws, pure]
  pub length: u32,
  getter Option<Element> item(index: u32);
  #[alias = "namedItem"]
  pub fn named_item(name: DOMString) -> Option<HTMLOptionElement>;
  #[ce_reactions, throws]
  pub fn add((HTMLOptionElement or HTMLOptGroupElement) element, optional (HTMLElement or i32)? before = None);
  #[ce_reactions]
  pub fn remove(index: i32);
  #[ce_reactions, throws]
  setter void (index: u32, index: u32);

  #[alias = "selectedOptions"]
  selected_options: HTMLCollection,
  #[pure]
  #[alias = "selectedIndex"]
  pub selected_index: i32,
  #[pure]
  pub value: DOMString,

  #[alias = "willValidate"]
  will_validate: bool,
  validity: ValidityState,
  #[throws]
  #[alias = "validationMessage"]
  validation_message: DOMString,
  #[alias = "checkValidity"]
  pub fn check_validity() -> bool;
  #[alias = "reportValidity"]
  pub fn report_validity() -> bool;
  #[alias = "setCustomValidity"]
  pub fn set_custom_validity(error: DOMString);

  labels: NodeList,

  // https://www.w3.org/Bugs/Public/show_bug.Option<cgi>id = 20720
  #[ce_reactions]
  pub fn remove();
}

// Chrome only trait

partial trait HTMLSelectElement {
  #[chrome_only]
  #[alias = "openInParentProcess"]
  pub open_in_parent_process: bool,
  #[chrome_only]
  #[alias = "getAutocompleteInfo"]
  pub fn get_autocomplete_info() -> AutocompleteInfo;
  #[chrome_only]
  #[alias = "previewValue"]
  pub preview_value: DOMString,
}
