// The origin of this IDL file is
// https://streams.spec.whatwg.org/#underlying-sink-api

#[GenerateInit]
pub struct UnderlyingSink {
  pub start: UnderlyingSinkStartCallback,
  pub write: UnderlyingSinkWriteCallback,
  pub close: UnderlyingSinkCloseCallback,
  pub abort: UnderlyingSinkAbortCallback,
  pub type: any,
}

callback UnderlyingSinkStartCallback = any (controller: WritableStreamDefaultController);
callback UnderlyingSinkWriteCallback = Promise<void> (chunk: any, chunk: any);
callback UnderlyingSinkCloseCallback = Promise<void> ();
callback UnderlyingSinkAbortCallback = Promise<void> (optional reason: any);
