// This IDL file is related to the WebExtensions API object.
//
// You are granted a license to use, reproduce and create derivative works of
// this document.

#[exposed = (ServiceWorker), LegacyNoInterfaceObject]
pub trait ExtensionEventManager {
  #[throws]
  #[alias = "addListener"]
  pub fn add_listener(callback: function, optional listenerOptions: object);

  #[throws]
  #[alias = "removeListener"]
  pub fn remove_listener(callback: function);

  #[throws]
  #[alias = "hasListener"]
  pub fn has_listener(callback: function) -> bool;

  #[throws]
  #[alias = "hasListeners"]
  pub fn has_listeners() -> bool;
}
