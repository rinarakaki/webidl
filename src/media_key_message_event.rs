// The origin of this IDL file is
// https://dvcs.w3.org/hg/html-media/raw-file/default/encrypted-media/encrypted-media.html


pub enum MediaKeyMessageType {
  #[alias = "license-request"]
  LicenseRequest,
  #[alias = "license-renewal"]
  LicenseRenewal,
  #[alias = "license-release"]
  LicenseRelease,
  #[alias = "individualization-request"]
  IndividualisationRequest
}

#[exposed = Window]
pub trait MediaKeyMessageEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Result<Self>;

  #[alias = "messageType"]
  message_type: MediaKeyMessageType,
  #[throws]
  message: ArrayBuffer,
}

pub struct MediaKeyMessageEventInit: EventInit {
  required MediaKeyMessageType messageType;
  required ArrayBuffer message;
}
