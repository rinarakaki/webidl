#[exposed = Window]
pub trait PaintRequestList {
  length: u32,
  getter Option<PaintRequest> item(index: u32);
}
