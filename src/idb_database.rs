// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBObjectStoreParameters

#[exposed = (Window, Worker)]
pub trait IDBDatabase: EventTarget {
  name: DOMString,
  version: u64,

  #[alias = "objectStoreNames"]
  object_store_names: DOMStringList,

  #[throws]
  IDBObjectStore createObjectStore (name: DOMString, #[optional = {}] optionalParameters: IDBObjectStoreParameters);

  #[throws]
  void deleteObjectStore (name: DOMString);

  #[throws]
  IDBTransaction transaction ((DOMString or Vec<DOMString>) storeNames,
  #[optional = "readonly"] mode: IDBTransactionMode);

  void close ();

  #[alias = "onabort"]
  pub on_abort: EventHandler,
  #[alias = "onclose"]
  pub onclose: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  #[alias = "onversionchange"]
  pub onversionchange: EventHandler,
}

partial trait IDBDatabase {
  #[func = "mozilla::dom::IndexedDatabaseManager::ExperimentalFeaturesEnabled"]
  storage: StorageType,

  #[exposed = Window, throws, deprecated = "IDBDatabaseCreateMutableFile"]
  IDBRequest createMutableFile (name: DOMString, optional type: DOMString);
}
