// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-map-element

// http://www.whatwg.org/specs/web-apps/current-work/#the-map-element
#[exposed = Window]
pub trait HTMLMapElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub name: DOMString,
  #[constant]
  areas: HTMLCollection,
  // Not supported yet.
  //readonly attribute HTMLCollection images;
}
