// The origin of this IDL file is:
// https://html.spec.whatwg.org/multipage/interaction.html#the-datatransferitemlist-trait

use super::*;

#[exposed = Window]
pub trait DataTransferItemList {
  length: u32,
  #[alias = "DataTransferItem"]
  getter data_transfer_item(index: u32);
  
  #[needs_subject_principal]
  pub fn add(data: DOMString, data: DOMString) -> Result<Option<DataTransferItem>>;

  #[needs_subject_principal]
  pub fn add(data: File) -> Result<Option<DataTransferItem>>;

  #[needs_subject_principal]
  pub fn remove(index: u32) -> Result<()>;

  #[needs_subject_principal]
  pub fn clear() -> Result<()>;
}
