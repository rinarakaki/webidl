// The origin of this IDL file is
// https://drafts.csswg.org/cssom/#cssnamespacerule

use super::CSSRule;

#[exposed = Window]
pub trait CSSNamespaceRule: CSSRule {
  #[alias = "namespaceURI"]
  namespace_uri: DOMString,
  prefix: DOMString,
}
