// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGPathElement: SVGGeometryElement {
  #[pref = "dom.svg.pathSeg.enabled"]
  u32 getPathSegAtLength(distance: f32);
}

SVGPathElement includes SVGAnimatedPathData;

