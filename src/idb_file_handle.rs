pub struct IDBFileMetadataParameters {
  pub size: bool = true,
  #[alias = "lastModified"]
  pub last_modified: bool = true,
}

#[exposed = Window]
pub trait IDBFileHandle: EventTarget {
  #[alias = "mutableFile"]
  mutable_file: Option<IDBMutableFile>,
  // this is deprecated due to renaming in the spec
  #[alias = "fileHandle"]
  file_handle: Option<IDBMutableFile>, // now mutableFile
  mode: FileMode,
  active: bool,
  attribute u32 Option<i32> location;

  #[throws]
  #[alias = "getMetadata"]
  pub fn get_metadata(#[optional = {}] parameters: IDBFileMetadataParameters) -> Option<IDBFileRequest>;
  #[throws]
  #[alias = "readAsArrayBuffer"]
  pub fn read_as_array_buffer(size: u64) -> Option<IDBFileRequest>;
  #[throws]
  Option<IDBFileRequest> readAsText(size: u64,
  optional Option<DOMString> encoding = None);

  #[throws]
  pub fn write((DOMString or ArrayBuffer or ArrayBufferView or Blob) value) -> Option<IDBFileRequest>;
  #[throws]
  pub fn append((DOMString or ArrayBuffer or ArrayBufferView or Blob) value) -> Option<IDBFileRequest>;
  #[throws]
  pub fn truncate(optional size: u64) -> Option<IDBFileRequest>;
  #[throws]
  pub fn flush() -> Option<IDBFileRequest>;
  #[throws]
  pub fn abort();

  #[alias = "oncomplete"]
  pub oncomplete: EventHandler,
  #[alias = "onabort"]
  pub on_abort: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
}
