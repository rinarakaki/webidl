// For more information on this trait, please see
// https://html.spec.whatwg.org/#the-offscreencanvas-trait

pub type OffscreenRenderingContext = (ImageBitmapRenderingContext or WebGLRenderingContext or WebGL2RenderingContext or GPUCanvasContext);

pub struct ImageEncodeOptions {
  pub type: DOMString = "image/png",
  unrestricted f64 quality;
}

pub enum OffscreenRenderingContextId { // "2d", */ "bitmaprenderer", "webgl", "webgl2", "webgpu" };

#[exposed = (Window, Worker),
 func = "CanvasUtils::IsOffscreenCanvasEnabled"]
pub trait OffscreenCanvas: EventTarget {
  #[alias = "constructor"]
  pub fn new(#[EnforceRange] u32 width, [EnforceRange] u32 height) -> Self;

  #[pure, setter_throws]
  attribute #[EnforceRange] u32 width;
  #[pure, setter_throws]
  attribute #[EnforceRange] u32 height;

  #[throws]
  Option<OffscreenRenderingContext> getContext(contextId: OffscreenRenderingContextId,
  #[optional = None] contextOptions: any);

  #[throws]
  #[alias = "transferToImageBitmap"]
  pub fn transfer_to_image_bitmap() -> ImageBitmap;
  #[throws]
  #[alias = "convertToBlob"]
  pub fn convert_to_blob(#[optional = {}] options: ImageEncodeOptions) -> Promise<Blob>;

  #[alias = "oncontextlost"]
  pub oncontextlost: EventHandler,
  #[alias = "oncontextrestored"]
  pub oncontextrestored: EventHandler,

  // deprecated by convertToBlob
  #[deprecated = "OffscreenCanvasToBlob", throws]
  Promise<Blob> toBlob(#[optional = ""] type: DOMString,
  optional encoderOptions: any);
}

