// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

pub struct ChannelSplitterOptions: AudioNodeOptions {
  #[alias = "numberOfOutputs"]
  pub #[optional = 6] number_of_outputs: u32
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait ChannelSplitterNode: AudioNode {
  #[alias = constructor]
  pub fn new(
    context: BaseAudioContext,
    #[optional = {}] options: ChannelSplitterOptions) -> Result<Self>;
}

