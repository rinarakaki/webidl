// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

use super::event_listner::EventListener;
use super::window::Window;
use super::worker::Worker;

pub struct EventListenerOptions {
  pub capture: bool = false,
  // Setting to true make the listener be added to the system group. #[func = "ThreadSafeIsChromeOrUAWidget"]
  #[alias = "mozSystemGroup"]
  pub moz_system_group: bool = false,
}

pub struct AddEventListenerOptions: EventListenerOptions {
  pub passive: bool,
  pub once: bool = false,
  pub signal: AbortSignal,
  #[chrome_only]
  #[alias = "wantUntrusted"]
  pub want_untrusted: bool,
}

#[exposed = (Window, Worker, WorkerDebugger, AudioWorklet)]
pub trait EventTarget {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  // Passing None for wantsUntrusted means "default behavior", which
  // differs in content and chrome. In content that default bool
  // value is true, while in chrome the default bool value is
  // false.
  #[alias = "addEventListener"]
  pub fn add_event_listener(
    event: DOMString,
    listener: Option<EventListener>,
    optional (AddEventListenerOptions or bool) options = {},
    optional Option<bool> wantsUntrusted = None) -> Result<(), Error>;

  #[alias = "removeEventListener"]
  pub fn remove_event_listener(
    event: DOMString,
    listener: Option<EventListener>,
    optional (EventListenerOptions or bool) options = {}) -> Result<(), Error>;

  #[needs_caller_type]
  #[alias = "dispatchEvent"]
  pub fn dispatch_event(event: Event) -> Result<bool, Error>;
}

// Mozilla extensions for use by JS-implemented event targets to
// implement on* properties.
partial trait EventTarget {
  // The use of #[TreatNonCallableAsNull] here is a bit of a hack: it just makes
  // the codegen check whether the type involved is either
  // #[TreatNonCallableAsNull] or [TreatNonObjectAsNull] and if it is handle it
  // accordingly. In particular, it will NOT actually treat a non-None
  // non-callable object as None here.
  #[chrome_only]
  #[alias = "setEventHandler"]
  pub fn set_event_handler(type: DOMString,
    #[TreatNonCallableAsNull]
    handler: EventHandler) -> Result<(), Error>;

  #[chrome_only]
  #[alias = "getEventHandler"]
  pub fn get_event_handler(type: DOMString) -> EventHandler;
}

// Mozilla extension to make firing events on event targets from
// chrome easier. This returns the window which can be used to create
// events to fire at this EventTarget, or None if there isn't one.
partial trait EventTarget {
  #[chrome_only, exposed = Window, binary_name = "ownerGlobalForBindings"]
  #[alias = "ownerGlobal"]
  owner_global: Option<WindowProxy>,
}
