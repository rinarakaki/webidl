// For more information on this trait, please see
// http://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html

#[func = "ServiceWorkerVisible",
 exposed = (ServiceWorker)]
pub trait FetchEvent: ExtendableEvent {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;

  #[same_object, binary_name = "request_"]
  request: Request,
  #[pref = "dom.serviceWorkers.navigationPreload.enabled"]
  #[alias = "preloadResponse"]
  preload_response: Promise<any>,
  #[alias = "clientId"]
  client_id: DOMString,
  #[alias = "resultingClientId"]
  resulting_client_id: DOMString,
  handled: Promise<void>,

  #[alias = "respondWith"]
  pub fn respond_with(r: Promise<Response>) -> Result<()>;
}

pub struct FetchEventInit: EventInit {
  required Request request;
  pub clientId: DOMString = "",
  #[alias = "resultingClientId"]
  pub resulting_client_id: DOMString = "",
}
