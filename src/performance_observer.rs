// The origin of this IDL file is
// https://w3c.github.io/performance-timeline/#the-performanceobserver-trait

pub struct PerformanceObserverInit {
  #[alias = "entryTypes"]
  pub entry_types: Vec<DOMString>,
	DOMString type;
  pub buffered: bool,
  #[pref = "dom.enable_event_timing"]
  #[alias = "durationThreshold"]
  pub duration_threshold: DOMHighResTimeStamp,
}

callback PerformanceObserverCallback = void (entries: PerformanceObserverEntryList,
  observer: PerformanceObserver);

#[pref = "dom.enable_performance_observer",
 exposed = (Window, Worker)]
pub trait PerformanceObserver {
  #[alias = "constructor"]
  pub fn new(callback: PerformanceObserverCallback) -> Result<Self, Error>;

  #[throws]
  pub fn observe(#[optional = {}] options: PerformanceObserverInit);
  pub fn disconnect();
  #[alias = "takeRecords"]
  pub fn take_records() -> PerformanceEntryList;
  static readonly attribute object supportedEntryTypes;
}
