// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct MediaElementAudioSourceOptions {
  required HTMLMediaElement mediaElement;
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait MediaElementAudioSourceNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(context: AudioContext, context: AudioContext) -> Result<Self, Error>;

  #[alias = "mediaElement"]
  media_element: HTMLMediaElement,
}

// Mozilla extensions
MediaElementAudioSourceNode includes AudioNodePassThrough;

