// The origin of this IDL file is
// http://dvcs.w3.org/hg/html-media/raw-file/default/media-source/media-source.html

pub enum MediaSourceReadyState {
  #[alias = "closed"]
  Closed,
  #[alias = "open"]
  Open,
  #[alias = "ended"]
  Ended
}

pub enum MediaSourceEndOfStreamError {
  #[alias = "network"]
  Network,
  #[alias = "decode"]
  Decode
}

#[pref = "media.mediasource.enabled",
 exposed = Window]
pub trait MediaSource: EventTarget {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  #[alias = "sourceBuffers"]
  source_buffers: SourceBufferList,
  #[alias = "activeSourceBuffers"]
  active_source_buffers: SourceBufferList,
  #[alias = "readyState"]
  ready_state: MediaSourceReadyState,
  #[setter_throws]
  attribute unrestricted f64 duration;
  #[alias = "onsourceopen"]
  pub onsourceopen: EventHandler,
  #[alias = "onsourceended"]
  pub onsourceended: EventHandler,
  #[alias = "onsourceclose"]
  pub onsourceclose: EventHandler,
  #[new_object, throws]
  #[alias = "addSourceBuffer"]
  pub fn add_source_buffer(type: DOMString) -> SourceBuffer;
  #[throws]
  #[alias = "removeSourceBuffer"]
  pub fn remove_source_buffer(sourceBuffer: SourceBuffer);
  #[throws]
  #[alias = "endOfStream"]
  pub fn end_of_stream(optional error: MediaSourceEndOfStreamError);
  #[throws]
  #[alias = "setLiveSeekableRange"]
  pub fn set_live_seekable_range(start: f64, start: f64);
  #[throws]
  #[alias = "clearLiveSeekableRange"]
  pub fn clear_live_seekable_range();
  static bool isTypeSupported(type: DOMString);
  #[throws, chrome_only]
  #[alias = "mozDebugReaderData"]
  pub fn moz_debug_reader_data() -> Promise<MediaSourceDecoderDebugInfo>;
}
