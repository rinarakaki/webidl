// For more information on this trait please see
// http://dev.w3.org/2006/webapi/DOM-Level-3-Events/html/DOM3-Events.html

#[exposed = Window]
pub trait WheelEvent: MouseEvent {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: WheelEventInit) -> Self;

  const u32 DOM_DELTA_PIXEL = 0x00;
  const u32 DOM_DELTA_LINE = 0x01;
  const u32 DOM_DELTA_PAGE = 0x02;

  // Legacy MouseWheelEvent API replaced by standard WheelEvent API.
  #[needs_caller_type]
  #[alias = "wheelDeltaX"]
  wheel_delta_x: i32,
  #[needs_caller_type]
  #[alias = "wheelDeltaY"]
  wheel_delta_y: i32,
  #[needs_caller_type]
  #[alias = "wheelDelta"]
  wheel_delta: i32,

  #[needs_caller_type]
  deltaX: f64,
  #[needs_caller_type]
  deltaY: f64,
  #[needs_caller_type]
  deltaZ: f64,
  #[needs_caller_type]
  #[alias = "deltaMode"]
  delta_mode: u32,
}

pub struct WheelEventInit: MouseEventInit {
  pub deltaX: f64 = 0,
  pub deltaY: f64 = 0,
  pub deltaZ: f64 = 0,
  #[alias = "deltaMode"]
  pub delta_mode: u32 = 0,
}
