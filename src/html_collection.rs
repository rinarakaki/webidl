// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

#[LegacyUnenumerableNamedProperties,
 exposed = Window]
pub trait HTMLCollection {
  length: u32,
  getter Option<Element> item(index: u32);
  getter Option<Element> namedItem(name: DOMString);
}
