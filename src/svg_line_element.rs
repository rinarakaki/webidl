// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGLineElement: SVGGeometryElement {
  #[constant]
  x1: SVGAnimatedLength,
  #[constant]
  y1: SVGAnimatedLength,
  #[constant]
  x2: SVGAnimatedLength,
  #[constant]
  y2: SVGAnimatedLength,
}

