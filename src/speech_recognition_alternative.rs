// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

#[pref = "media.webspeech.recognition.enable",
 func = "SpeechRecognition::IsAuthorized",
 exposed = Window]
pub trait SpeechRecognitionAlternative {
  transcript: DOMString,
  confidence: f32,
}
