// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

#[exposed = Window]
pub trait Text: CharacterData {
  #[alias = "constructor"]
  pub fn new(#[optional = ""] data: DOMString) -> Result<Self, Error>;

  #[throws]
  #[alias = "splitText"]
  pub fn split_text(offset: u32) -> Text;
  #[throws]
  #[alias = "wholeText"]
  whole_text: DOMString,
}

partial trait Text {
  #[binary_name = "assignedSlotByMode"]
  #[alias = "assignedSlot"]
  assigned_slot: Option<HTMLSlotElement>,

  #[chrome_only, binary_name = "assignedSlot"]
  #[alias = "openOrClosedAssignedSlot"]
  open_or_closed_assigned_slot: Option<HTMLSlotElement>,
}

Text includes GeometryUtils;
