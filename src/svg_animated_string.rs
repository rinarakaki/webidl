// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimatedString {
  #[alias = "baseVal"]
  pub base_val: DOMString,
  #[alias = "animVal"]
  anim_val: DOMString,
}
