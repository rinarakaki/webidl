// https://wicg.github.io/entries-api/#idl-index

use super::{
  ErrorCallback,
  FSDirectoryReader,
  FSEntry,
  FSEntryCallback,
  FSFlags
};

#[exposed = Window]
#[alias = "FileSystemDirectoryEntry"]
pub trait FSDirectoryEntry: FSEntry {
  #[alias = "createReader"]
  pub fn create_reader() -> FSDirectoryReader;

  #[alias = "getFile"]
  pub fn get_file(
    optional path: Option<USVString>,
    #[optional = {}] options: FSFlags,
    optional success_callback: FSEntryCallback,
    optional error_callback: ErrorCallback);

  #[alias = "getDirectory"]
  pub fn get_directory(
    optional path: Option<USVString>,
    #[optional = {}] options: FSFlags,
    optional success_callback: FSEntryCallback,
    optional error_callback: ErrorCallback);
}
