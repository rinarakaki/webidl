// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/forms.html#the-dialog-element

#[exposed = Window]
pub trait HTMLSlotElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub name: DOMString,
  #[alias = "assignedNodes"]
  pub fn assigned_nodes(#[optional = {}] options: AssignedNodesOptions) -> Vec<Node>;
  #[alias = "assignedElements"]
  pub fn assigned_elements(#[optional = {}] options: AssignedNodesOptions) -> Vec<Element>;
  #[pref = "dom.shadowdom.slot.assign.enabled"]
  pub fn assign((Element or Text)... nodes);
}

pub struct AssignedNodesOptions {
  pub flatten: bool = false,
}
