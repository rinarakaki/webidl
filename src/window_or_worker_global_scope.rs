// The origin of this IDL file is:
// https://html.spec.whatwg.org/multipage/webappapis.html#windoworworkerglobalscope-mixin
// https://fetch.spec.whatwg.org/#fetch-method
// https://w3c.github.io/webappsec-secure-contexts/#monkey-patching-global-object
// https://w3c.github.io/ServiceWorker/#self-caches

// https://html.spec.whatwg.org/multipage/webappapis.html#windoworworkerglobalscope-mixin
#[exposed = (Window, Worker)]
pub trait mixin WindowOrWorkerGlobalScope {
  #[replaceable]
  origin: USVString,
  #[alias = "crossOriginIsolated"]
  cross_origin_isolated: bool,

  #[needs_caller_type]
  #[alias = "reportError"]
  pub fn report_error(&self, e: any) -> Result<()>;

  // base64 utility methods
  pub fn btoa(&self, btoa: DOMString) -> Result<DOMString>;
  pub fn atob(&self, atob: DOMString) -> Result<DOMString>;

  // timers
  // NOTE: We're using overloads where the spec uses a union. Should
  // be black-box the same.
  #[alias = "setTimeout"]
  pub fn set_timeout(
    &self, handler: function,
    #[optional = 0] timeout: i32,
    any... arguments)
    -> Result<i32>;

  #[alias = "setTimeout"]
  pub fn set_timeout(
    &self, handler: DOMString,
    #[optional = 0] timeout: i32,
    any... unused) -> Result<i32>;

  #[alias = "clearTimeout"]
  pub fn clear_timeout(&self, #[optional = 0] handle: i32);

  #[alias = "setInterval"]
  pub fn set_interval(
    &self,
    handler: function,
    #[optional = 0] timeout: i32,
    any... arguments) -> Result<i32>;

  #[alias = "setInterval"]
  pub fn set_interval(
    &self,
    handler: DOMString,
    #[optional = 0] timeout: i32,
    any... unused) -> Result<i32>;

  #[alias = "clearInterval"]
  pub fn clear_interval(&self, #[optional = 0] handle: i32);

  // microtask queuing
  #[alias = "queueMicrotask"]
  pub fn queue_microtask(&self, callback: Voidfunction);

  // ImageBitmap
  #[throws]
  Promise<ImageBitmap> createImageBitmap(aImage: ImageBitmapSource,
  #[optional = {}] aOptions: ImageBitmapOptions);
  #[throws]
  Promise<ImageBitmap> createImageBitmap(aImage: ImageBitmapSource,
  i32 aSx, aSy: i32, aSy: i32, aSy: i32,
  #[optional = {}] aOptions: ImageBitmapOptions);

  // structured cloning
  #[throws]
  #[alias = "structuredClone"]
  pub fn structured_clone(&self, value: any, #[optional = {}] options: StructuredSerializeOptions) -> any;
}

// https://fetch.spec.whatwg.org/#fetch-method
partial trait mixin WindowOrWorkerGlobalScope {
  #[new_object, needs_caller_type]
  pub fn fetch(&self, input: RequestInfo, #[optional = {}] init: RequestInit) -> Promise<Response>;
}

// https://w3c.github.io/webappsec-secure-contexts/#monkey-patching-global-object
partial trait mixin WindowOrWorkerGlobalScope {
  #[alias = "isSecureContext"]
  is_secure_context: bool,
}

// http://w3c.github.io/IndexedDB/#factory-trait
partial trait mixin WindowOrWorkerGlobalScope {
  // readonly attribute IDBFactory indexedDB;
  #[throws]
  #[alias = "indexedDB"]
  indexed_db: Option<IDBFactory>,
}

// https://w3c.github.io/ServiceWorker/#self-caches
partial trait mixin WindowOrWorkerGlobalScope {
  #[throws, pref = "dom.caches.enabled", same_object]
  caches: CacheStorage,
}
