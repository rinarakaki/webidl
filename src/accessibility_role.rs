// The origin of this IDL file is
// https://rawgit.com/w3c/aria/master/#AccessibilityRole

pub super::*;

pub trait mixin AccessibilityRole {
    #[pref = "accessibility.ARIAReflection.enabled", ce_reactions]
    pub fn set_role(&mut self, DOMString) -> Result<()>;

    pub fn get_role(&self) -> DOMString;
}
