#[exposed = (Window, Worker)]
pub trait ProgressEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: ProgressEventInit) -> Self;

  #[alias = "lengthComputable"]
  length_computable: bool,
  loaded: u64,
  total: u64,
}

pub struct ProgressEventInit: EventInit {
  #[alias = "lengthComputable"]
  pub length_computable: bool = false,
  pub loaded: u64 = 0,
  pub total: u64 = 0,
}
