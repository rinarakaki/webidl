// The origin of this IDL file is
// https://drafts.csswg.org/css-animations/#trait-csskeyframerule

use super::{CSSRule, CSSStyleDeclaration};

#[exposed = Window]
pub trait CSSKeyframeRule: CSSRule {
  #[alias = "keyText"]
  pub key_text: UTF8String,
  #[same_object, put_forwards = cssText]
  style: CSSStyleDeclaration,
}
