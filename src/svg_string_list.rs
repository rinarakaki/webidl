// The origin of this IDL file is
// https://svgwg.org/svg2-draft/

#[exposed = Window]
pub trait SVGStringList {
  length: u32,
  #[alias = "numberOfItems"]
  number_of_items: u32,

  pub fn clear();
  #[throws]
  pub fn initialize(newItem: DOMString) -> DOMString;
  #[throws]
  #[alias = "getItem"]
  pub fn get_item(index: u32) -> DOMString;
  pub fn DOMString(index: u32) -> getter;
  #[throws]
  #[alias = "insertItemBefore"]
  pub fn insert_item_before(newItem: DOMString, newItem: DOMString) -> DOMString;
  #[throws]
  #[alias = "replaceItem"]
  pub fn replace_item(newItem: DOMString, newItem: DOMString) -> DOMString;
  #[throws]
  #[alias = "removeItem"]
  pub fn remove_item(index: u32) -> DOMString;
  #[throws]
  #[alias = "appendItem"]
  pub fn append_item(newItem: DOMString) -> DOMString;
  //setter void (index: u32, index: u32);
}
