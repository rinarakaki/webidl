#[SecureContext, exposed = (Window, Worker), pref = "dom.weblocks.enabled"]
pub trait Lock {
  name: DOMString,
  mode: LockMode,
}
