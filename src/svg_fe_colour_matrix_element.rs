// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

use super::SVGElement;

#[exposed = Window]
#[alias = "SVGFEColorMatrixElement"]
pub trait SVGFEColourMatrixElement: SVGElement {

  // Colour Matrix Types
  const u16 SVG_FECOLORMATRIX_TYPE_UNKNOWN = 0;
  const u16 SVG_FECOLORMATRIX_TYPE_MATRIX = 1;
  const u16 SVG_FECOLORMATRIX_TYPE_SATURATE = 2;
  const u16 SVG_FECOLORMATRIX_TYPE_HUEROTATE = 3;
  const u16 SVG_FECOLORMATRIX_TYPE_LUMINANCETOALPHA = 4;

  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  type: SVGAnimatedEnumeration,
  #[constant]
  values: SVGAnimatedNumberList,
}

SVGFEColourMatrixElement includes SVGFilterPrimitiveStandardAttributes;
