// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-dl-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-dl-element
#[exposed = Window]
pub trait HTMLDListElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLDListElement {
  #[ce_reactions, setter_throws]
  pub compact: bool,
}
