// The origin of this IDL file is
// https://w3c.github.io/FileAPI/#file
// https://wicg.github.io/entries-api

pub trait nsIFile;

#[exposed = (Window, Worker)]
pub trait File: Blob {
  #[throws]
  constructor(fileBits: Vec<BlobPart>,
  USVString fileName, #[optional = {}] options: FilePropertyBag);

  name: DOMString,

  #[getter_throws]
  #[alias = "lastModified"]
  last_modified: i64,
}

pub struct FilePropertyBag: BlobPropertyBag {
  #[alias = "lastModified"]
  pub last_modified: i64,
}

pub struct ChromeFilePropertyBag: FilePropertyBag {
  pub name: DOMString = "",
  #[alias = "existenceCheck"]
  pub existence_check: bool = true,
}

// https://wicg.github.io/entries-api
partial trait File {
  #[binary_name = "relativePath", pref = "dom.webkitBlink.dirPicker.enabled"]
  #[alias = "webkitRelativePath"]
  webkit_relative_path: USVString,
}

// Mozilla extensions
partial trait File {
  #[getter_throws, chrome_only, needs_caller_type]
  #[alias = "mozFullPath"]
  moz_full_path: DOMString,
}

// Mozilla extensions
// These 2 methods can be used only in these conditions:
// - the main-thread
// - parent process OR file process OR, only for testing, with pref
//   `dom.file.createInChild' set to true.
#[exposed = (Window)]
partial trait File {
  #[chrome_only, throws, needs_caller_type]
  static Promise<File> createFromNsIFile(file: nsIFile,
  #[optional = {}] options: ChromeFilePropertyBag);

  #[chrome_only, throws, needs_caller_type]
  static Promise<File> createFromFileName(fileName: USVString,
  #[optional = {}] options: ChromeFilePropertyBag);
}
