// The origin of this IDL file is
// https://immersive-web.github.io/webxr/#xrsessionevent-trait

use super::{Event, XRSession};

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRSessionEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;
  session: XRSession,
}

pub struct XRSessionEventInit: EventInit {
  required XRSession session;
}
