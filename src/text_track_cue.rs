// The origin of this IDL file is
// https://html.spec.whatwg.org/#texttrackcue

#[exposed = Window]
pub trait TextTrackCue: EventTarget {
  track: Option<TextTrack>,

  pub id: DOMString,
  #[alias = "startTime"]
  pub start_time: f64,
  #[alias = "endTime"]
  pub end_time: f64,
  #[alias = "pauseOnExit"]
  pub pause_on_exit: bool,

  #[alias = "onenter"]
  pub onenter: EventHandler,
  #[alias = "onexit"]
  pub onexit: EventHandler,
}
