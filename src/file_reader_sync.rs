// The origin of this IDL file is
// http://dev.w3.org/2006/webapi/FileAPI/

use super::Blob;

#[exposed = (DedicatedWorker, SharedWorker)]
pub trait FileReaderSync {
  #[alias = "constructor"]
  pub fn new() -> Self;
 
  // Synchronously return strings

  #[alias = "readAsArrayBuffer"]
  pub fn read_as_array_buffer(&self, blob: Blob) -> Result<ArrayBuffer>;
  #[alias = "readAsBinaryString"]
  pub fn read_as_binary_string(&self, blob: Blob) -> Result<DOMString>;
  #[alias = "readAsText"]
  pub fn read_as_text(&self, blob: Blob, optional encoding: DOMString)
    -> Result<DOMString>;
  #[alias = "readAsDataURL"]
  pub fn read_as_data_url(&self, blob: Blob) -> Result<DOMString>;
}
