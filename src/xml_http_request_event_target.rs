// The origin of this IDL file is
// www.w3.org/TR/2012/WD-XMLHttpRequest-20120117/

use super::EventTarget;

#[exposed = (Window,DedicatedWorker, SharedWorker)]
#[alias = "XMLHttpRequestEventTarget"]
pub trait XMLHTTPRequestEventTarget: EventTarget {
  // event handlers
  #[alias = "onloadstart"]
  pub on_load_start: EventHandler,

  #[alias = "onprogress"]
  pub on_progress: EventHandler,

  #[alias = "onabort"]
  pub on_abort: EventHandler,

  #[alias = "onerror"]
  pub on_error: EventHandler,

  #[alias = "onload"]
  pub on_load: EventHandler,

  #[alias = "ontimeout"]
  pub on_timeout: EventHandler,

  #[alias = "onloadend"]
  pub on_load_end: EventHandler,
}
