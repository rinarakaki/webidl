// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGUseElement: SVGGraphicsElement {
  #[constant]
  x: SVGAnimatedLength,
  #[constant]
  y: SVGAnimatedLength,
  #[constant]
  width: SVGAnimatedLength,
  #[constant]
  height: SVGAnimatedLength,
  //readonly attribute SVGElementInstance instanceRoot;
  //readonly attribute SVGElementInstance animatedInstanceRoot;
}

SVGUseElement includes SVGURIReference;
