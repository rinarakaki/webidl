// The origin of this IDL file is
// https://www.w3.org/TR/webrtc/#rtcdtmfsender

#[exposed = Window]
pub trait RTCDTMFSender: EventTarget {
  #[throws]
  void insertDTMF(tones: DOMString,
  #[optional = 100] duration: u32,
  #[optional = 70] interToneGap: u32);
  #[alias = "ontonechange"]
  pub ontonechange: EventHandler,
  #[alias = "toneBuffer"]
  tone_buffer: DOMString,
}
