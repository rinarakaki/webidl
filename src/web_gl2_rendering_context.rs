// The source for this IDL is found at https://www.khronos.org/registry/webgl/specs/latest/2.0
// This IDL depends on WebGLRenderingContext.webidl

pub type GLint64 = i64;
pub type GLuint64 = u64;

#[pref = "webgl.enable-webgl2",
 func = "mozilla::dom::OffscreenCanvas::PrefEnabledOnWorkerThread",
 exposed = (Window, Worker)]
pub trait WebGLSampler {
}

#[pref = "webgl.enable-webgl2",
 func = "mozilla::dom::OffscreenCanvas::PrefEnabledOnWorkerThread",
 exposed = (Window, Worker)]
pub trait WebGLSync {
}

#[pref = "webgl.enable-webgl2",
 func = "mozilla::dom::OffscreenCanvas::PrefEnabledOnWorkerThread",
 exposed = (Window, Worker)]
pub trait WebGLTransformFeedback {
}

pub type Uint32List = (#[AllowShared] Uint32Array or Vec<GLuint>);

// WebGL2 spec has this as an empty trait that pulls in everything
// via WebGL2RenderingContextBase.
#[pref = "webgl.enable-webgl2",
 func = "mozilla::dom::OffscreenCanvas::PrefEnabledOnWorkerThread",
 exposed = (Window, Worker)]
pub trait WebGL2RenderingContext
{
}

pub trait mixin WebGL2RenderingContextBase
{
  const GLenum READ_BUFFER = 0x0C02;
  const GLenum UNPACK_ROW_LENGTH = 0x0CF2;
  const GLenum UNPACK_SKIP_ROWS = 0x0CF3;
  const GLenum UNPACK_SKIP_PIXELS = 0x0CF4;
  const GLenum PACK_ROW_LENGTH = 0x0D02;
  const GLenum PACK_SKIP_ROWS = 0x0D03;
  const GLenum PACK_SKIP_PIXELS = 0x0D04;
  const GLenum COLOR = 0x1800;
  const GLenum DEPTH = 0x1801;
  const GLenum STENCIL = 0x1802;
  const GLenum RED = 0x1903;
  const GLenum RGB8 = 0x8051;
  const GLenum RGBA8 = 0x8058;
  const GLenum RGB10_A2 = 0x8059;
  const GLenum TEXTURE_BINDING_3D = 0x806A;
  const GLenum UNPACK_SKIP_IMAGES = 0x806D;
  const GLenum UNPACK_IMAGE_HEIGHT = 0x806E;
  const GLenum TEXTURE_3D = 0x806F;
  const GLenum TEXTURE_WRAP_R = 0x8072;
  const GLenum MAX_3D_TEXTURE_SIZE = 0x8073;
  const GLenum UNSIGNED_INT_2_10_10_10_REV = 0x8368;
  const GLenum MAX_ELEMENTS_VERTICES = 0x80E8;
  const GLenum MAX_ELEMENTS_INDICES = 0x80E9;
  const GLenum TEXTURE_MIN_LOD = 0x813A;
  const GLenum TEXTURE_MAX_LOD = 0x813B;
  const GLenum TEXTURE_BASE_LEVEL = 0x813C;
  const GLenum TEXTURE_MAX_LEVEL = 0x813D;
  const GLenum MIN = 0x8007;
  const GLenum MAX = 0x8008;
  const GLenum DEPTH_COMPONENT24 = 0x81A6;
  const GLenum MAX_TEXTURE_LOD_BIAS = 0x84FD;
  const GLenum TEXTURE_COMPARE_MODE = 0x884C;
  const GLenum TEXTURE_COMPARE_FUNC = 0x884D;
  const GLenum CURRENT_QUERY = 0x8865;
  const GLenum QUERY_RESULT = 0x8866;
  const GLenum QUERY_RESULT_AVAILABLE = 0x8867;
  const GLenum STREAM_READ = 0x88E1;
  const GLenum STREAM_COPY = 0x88E2;
  const GLenum STATIC_READ = 0x88E5;
  const GLenum STATIC_COPY = 0x88E6;
  const GLenum DYNAMIC_READ = 0x88E9;
  const GLenum DYNAMIC_COPY = 0x88EA;
  const GLenum MAX_DRAW_BUFFERS = 0x8824;
  const GLenum DRAW_BUFFER0 = 0x8825;
  const GLenum DRAW_BUFFER1 = 0x8826;
  const GLenum DRAW_BUFFER2 = 0x8827;
  const GLenum DRAW_BUFFER3 = 0x8828;
  const GLenum DRAW_BUFFER4 = 0x8829;
  const GLenum DRAW_BUFFER5 = 0x882A;
  const GLenum DRAW_BUFFER6 = 0x882B;
  const GLenum DRAW_BUFFER7 = 0x882C;
  const GLenum DRAW_BUFFER8 = 0x882D;
  const GLenum DRAW_BUFFER9 = 0x882E;
  const GLenum DRAW_BUFFER10 = 0x882F;
  const GLenum DRAW_BUFFER11 = 0x8830;
  const GLenum DRAW_BUFFER12 = 0x8831;
  const GLenum DRAW_BUFFER13 = 0x8832;
  const GLenum DRAW_BUFFER14 = 0x8833;
  const GLenum DRAW_BUFFER15 = 0x8834;
  const GLenum MAX_FRAGMENT_UNIFORM_COMPONENTS = 0x8B49;
  const GLenum MAX_VERTEX_UNIFORM_COMPONENTS = 0x8B4A;
  const GLenum SAMPLER_3D = 0x8B5F;
  const GLenum SAMPLER_2D_SHADOW = 0x8B62;
  const GLenum FRAGMENT_SHADER_DERIVATIVE_HINT = 0x8B8B;
  const GLenum PIXEL_PACK_BUFFER = 0x88EB;
  const GLenum PIXEL_UNPACK_BUFFER = 0x88EC;
  const GLenum PIXEL_PACK_BUFFER_BINDING = 0x88ED;
  const GLenum PIXEL_UNPACK_BUFFER_BINDING = 0x88EF;
  const GLenum FLOAT_MAT2x3 = 0x8B65;
  const GLenum FLOAT_MAT2x4 = 0x8B66;
  const GLenum FLOAT_MAT3x2 = 0x8B67;
  const GLenum FLOAT_MAT3x4 = 0x8B68;
  const GLenum FLOAT_MAT4x2 = 0x8B69;
  const GLenum FLOAT_MAT4x3 = 0x8B6A;
  const GLenum SRGB = 0x8C40;
  const GLenum SRGB8 = 0x8C41;
  const GLenum SRGB8_ALPHA8 = 0x8C43;
  const GLenum COMPARE_REF_TO_TEXTURE = 0x884E;
  const GLenum RGBA32F = 0x8814;
  const GLenum RGB32F = 0x8815;
  const GLenum RGBA16F = 0x881A;
  const GLenum RGB16F = 0x881B;
  const GLenum VERTEX_ATTRIB_ARRAY_INTEGER = 0x88FD;
  const GLenum MAX_ARRAY_TEXTURE_LAYERS = 0x88FF;
  const GLenum MIN_PROGRAM_TEXEL_OFFSET = 0x8904;
  const GLenum MAX_PROGRAM_TEXEL_OFFSET = 0x8905;
  const GLenum MAX_VARYING_COMPONENTS = 0x8B4B;
  const GLenum TEXTURE_2D_ARRAY = 0x8C1A;
  const GLenum TEXTURE_BINDING_2D_ARRAY = 0x8C1D;
  const GLenum R11F_G11F_B10F = 0x8C3A;
  const GLenum UNSIGNED_INT_10F_11F_11F_REV = 0x8C3B;
  const GLenum RGB9_E5 = 0x8C3D;
  const GLenum UNSIGNED_INT_5_9_9_9_REV = 0x8C3E;
  const GLenum TRANSFORM_FEEDBACK_BUFFER_MODE = 0x8C7F;
  const GLenum MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS = 0x8C80;
  const GLenum TRANSFORM_FEEDBACK_VARYINGS = 0x8C83;
  const GLenum TRANSFORM_FEEDBACK_BUFFER_START = 0x8C84;
  const GLenum TRANSFORM_FEEDBACK_BUFFER_SIZE = 0x8C85;
  const GLenum TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN = 0x8C88;
  const GLenum RASTERIZER_DISCARD = 0x8C89;
  const GLenum MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS = 0x8C8A;
  const GLenum MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS = 0x8C8B;
  const GLenum INTERLEAVED_ATTRIBS = 0x8C8C;
  const GLenum SEPARATE_ATTRIBS = 0x8C8D;
  const GLenum TRANSFORM_FEEDBACK_BUFFER = 0x8C8E;
  const GLenum TRANSFORM_FEEDBACK_BUFFER_BINDING = 0x8C8F;
  const GLenum RGBA32UI = 0x8D70;
  const GLenum RGB32UI = 0x8D71;
  const GLenum RGBA16UI = 0x8D76;
  const GLenum RGB16UI = 0x8D77;
  const GLenum RGBA8UI = 0x8D7C;
  const GLenum RGB8UI = 0x8D7D;
  const GLenum RGBA32I = 0x8D82;
  const GLenum RGB32I = 0x8D83;
  const GLenum RGBA16I = 0x8D88;
  const GLenum RGB16I = 0x8D89;
  const GLenum RGBA8I = 0x8D8E;
  const GLenum RGB8I = 0x8D8F;
  const GLenum RED_INTEGER = 0x8D94;
  const GLenum RGB_INTEGER = 0x8D98;
  const GLenum RGBA_INTEGER = 0x8D99;
  const GLenum SAMPLER_2D_ARRAY = 0x8DC1;
  const GLenum SAMPLER_2D_ARRAY_SHADOW = 0x8DC4;
  const GLenum SAMPLER_CUBE_SHADOW = 0x8DC5;
  const GLenum UNSIGNED_INT_VEC2 = 0x8DC6;
  const GLenum UNSIGNED_INT_VEC3 = 0x8DC7;
  const GLenum UNSIGNED_INT_VEC4 = 0x8DC8;
  const GLenum INT_SAMPLER_2D = 0x8DCA;
  const GLenum INT_SAMPLER_3D = 0x8DCB;
  const GLenum INT_SAMPLER_CUBE = 0x8DCC;
  const GLenum INT_SAMPLER_2D_ARRAY = 0x8DCF;
  const GLenum UNSIGNED_INT_SAMPLER_2D = 0x8DD2;
  const GLenum UNSIGNED_INT_SAMPLER_3D = 0x8DD3;
  const GLenum UNSIGNED_INT_SAMPLER_CUBE = 0x8DD4;
  const GLenum UNSIGNED_INT_SAMPLER_2D_ARRAY = 0x8DD7;
  const GLenum DEPTH_COMPONENT32F = 0x8CAC;
  const GLenum DEPTH32F_STENCIL8 = 0x8CAD;
  const GLenum FLOAT_32_UNSIGNED_INT_24_8_REV = 0x8DAD;
  const GLenum FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING = 0x8210;
  const GLenum FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE = 0x8211;
  const GLenum FRAMEBUFFER_ATTACHMENT_RED_SIZE = 0x8212;
  const GLenum FRAMEBUFFER_ATTACHMENT_GREEN_SIZE = 0x8213;
  const GLenum FRAMEBUFFER_ATTACHMENT_BLUE_SIZE = 0x8214;
  const GLenum FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE = 0x8215;
  const GLenum FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE = 0x8216;
  const GLenum FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE = 0x8217;
  const GLenum FRAMEBUFFER_DEFAULT = 0x8218;
  const GLenum UNSIGNED_INT_24_8 = 0x84FA;
  const GLenum DEPTH24_STENCIL8 = 0x88F0;
  const GLenum UNSIGNED_NORMALIZED = 0x8C17;
  const GLenum DRAW_FRAMEBUFFER_BINDING = 0x8CA6; // Same as FRAMEBUFFER_BINDING const GLenum READ_FRAMEBUFFER = 0x8CA8;
  const GLenum DRAW_FRAMEBUFFER = 0x8CA9;
  const GLenum READ_FRAMEBUFFER_BINDING = 0x8CAA;
  const GLenum RENDERBUFFER_SAMPLES = 0x8CAB;
  const GLenum FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = 0x8CD4;
  const GLenum MAX_COLOR_ATTACHMENTS = 0x8CDF;
  const GLenum COLOR_ATTACHMENT1 = 0x8CE1;
  const GLenum COLOR_ATTACHMENT2 = 0x8CE2;
  const GLenum COLOR_ATTACHMENT3 = 0x8CE3;
  const GLenum COLOR_ATTACHMENT4 = 0x8CE4;
  const GLenum COLOR_ATTACHMENT5 = 0x8CE5;
  const GLenum COLOR_ATTACHMENT6 = 0x8CE6;
  const GLenum COLOR_ATTACHMENT7 = 0x8CE7;
  const GLenum COLOR_ATTACHMENT8 = 0x8CE8;
  const GLenum COLOR_ATTACHMENT9 = 0x8CE9;
  const GLenum COLOR_ATTACHMENT10 = 0x8CEA;
  const GLenum COLOR_ATTACHMENT11 = 0x8CEB;
  const GLenum COLOR_ATTACHMENT12 = 0x8CEC;
  const GLenum COLOR_ATTACHMENT13 = 0x8CED;
  const GLenum COLOR_ATTACHMENT14 = 0x8CEE;
  const GLenum COLOR_ATTACHMENT15 = 0x8CEF;
  const GLenum FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = 0x8D56;
  const GLenum MAX_SAMPLES = 0x8D57;
  const GLenum HALF_FLOAT = 0x140B;
  const GLenum RG = 0x8227;
  const GLenum RG_INTEGER = 0x8228;
  const GLenum R8 = 0x8229;
  const GLenum RG8 = 0x822B;
  const GLenum R16F = 0x822D;
  const GLenum R32F = 0x822E;
  const GLenum RG16F = 0x822F;
  const GLenum RG32F = 0x8230;
  const GLenum R8I = 0x8231;
  const GLenum R8UI = 0x8232;
  const GLenum R16I = 0x8233;
  const GLenum R16UI = 0x8234;
  const GLenum R32I = 0x8235;
  const GLenum R32UI = 0x8236;
  const GLenum RG8I = 0x8237;
  const GLenum RG8UI = 0x8238;
  const GLenum RG16I = 0x8239;
  const GLenum RG16UI = 0x823A;
  const GLenum RG32I = 0x823B;
  const GLenum RG32UI = 0x823C;
  const GLenum VERTEX_ARRAY_BINDING = 0x85B5;
  const GLenum R8_SNORM = 0x8F94;
  const GLenum RG8_SNORM = 0x8F95;
  const GLenum RGB8_SNORM = 0x8F96;
  const GLenum RGBA8_SNORM = 0x8F97;
  const GLenum SIGNED_NORMALIZED = 0x8F9C;
  const GLenum COPY_READ_BUFFER = 0x8F36;
  const GLenum COPY_WRITE_BUFFER = 0x8F37;
  const GLenum COPY_READ_BUFFER_BINDING = 0x8F36; // Same as COPY_READ_BUFFER const GLenum COPY_WRITE_BUFFER_BINDING = 0x8F37; // Same as COPY_WRITE_BUFFER const GLenum UNIFORM_BUFFER = 0x8A11;
  const GLenum UNIFORM_BUFFER_BINDING = 0x8A28;
  const GLenum UNIFORM_BUFFER_START = 0x8A29;
  const GLenum UNIFORM_BUFFER_SIZE = 0x8A2A;
  const GLenum MAX_VERTEX_UNIFORM_BLOCKS = 0x8A2B;
  const GLenum MAX_FRAGMENT_UNIFORM_BLOCKS = 0x8A2D;
  const GLenum MAX_COMBINED_UNIFORM_BLOCKS = 0x8A2E;
  const GLenum MAX_UNIFORM_BUFFER_BINDINGS = 0x8A2F;
  const GLenum MAX_UNIFORM_BLOCK_SIZE = 0x8A30;
  const GLenum MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = 0x8A31;
  const GLenum MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = 0x8A33;
  const GLenum UNIFORM_BUFFER_OFFSET_ALIGNMENT = 0x8A34;
  const GLenum ACTIVE_UNIFORM_BLOCKS = 0x8A36;
  const GLenum UNIFORM_TYPE = 0x8A37;
  const GLenum UNIFORM_SIZE = 0x8A38;
  const GLenum UNIFORM_BLOCK_INDEX = 0x8A3A;
  const GLenum UNIFORM_OFFSET = 0x8A3B;
  const GLenum UNIFORM_ARRAY_STRIDE = 0x8A3C;
  const GLenum UNIFORM_MATRIX_STRIDE = 0x8A3D;
  const GLenum UNIFORM_IS_ROW_MAJOR = 0x8A3E;
  const GLenum UNIFORM_BLOCK_BINDING = 0x8A3F;
  const GLenum UNIFORM_BLOCK_DATA_SIZE = 0x8A40;
  const GLenum UNIFORM_BLOCK_ACTIVE_UNIFORMS = 0x8A42;
  const GLenum UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES = 0x8A43;
  const GLenum UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER = 0x8A44;
  const GLenum UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER = 0x8A46;
  const GLenum INVALID_INDEX = 0xFFFFFFFF;
  const GLenum MAX_VERTEX_OUTPUT_COMPONENTS = 0x9122;
  const GLenum MAX_FRAGMENT_INPUT_COMPONENTS = 0x9125;
  const GLenum MAX_SERVER_WAIT_TIMEOUT = 0x9111;
  const GLenum OBJECT_TYPE = 0x9112;
  const GLenum SYNC_CONDITION = 0x9113;
  const GLenum SYNC_STATUS = 0x9114;
  const GLenum SYNC_FLAGS = 0x9115;
  const GLenum SYNC_FENCE = 0x9116;
  const GLenum SYNC_GPU_COMMANDS_COMPLETE = 0x9117;
  const GLenum UNSIGNALED = 0x9118;
  const GLenum SIGNALED = 0x9119;
  const GLenum ALREADY_SIGNALED = 0x911A;
  const GLenum TIMEOUT_EXPIRED = 0x911B;
  const GLenum CONDITION_SATISFIED = 0x911C;
  const GLenum WAIT_FAILED = 0x911D;
  const GLenum SYNC_FLUSH_COMMANDS_BIT = 0x00000001;
  const GLenum VERTEX_ATTRIB_ARRAY_DIVISOR = 0x88FE;
  const GLenum ANY_SAMPLES_PASSED = 0x8C2F;
  const GLenum ANY_SAMPLES_PASSED_CONSERVATIVE = 0x8D6A;
  const GLenum SAMPLER_BINDING = 0x8919;
  const GLenum RGB10_A2UI = 0x906F;
  const GLenum INT_2_10_10_10_REV = 0x8D9F;
  const GLenum TRANSFORM_FEEDBACK = 0x8E22;
  const GLenum TRANSFORM_FEEDBACK_PAUSED = 0x8E23;
  const GLenum TRANSFORM_FEEDBACK_ACTIVE = 0x8E24;
  const GLenum TRANSFORM_FEEDBACK_BINDING = 0x8E25;
  const GLenum TEXTURE_IMMUTABLE_FORMAT = 0x912F;
  const GLenum MAX_ELEMENT_INDEX = 0x8D6B;
  const GLenum TEXTURE_IMMUTABLE_LEVELS = 0x82DF;

  const GLint64 TIMEOUT_IGNORED = -1;

  // WebGL-specific enums const GLenum MAX_CLIENT_WAIT_TIMEOUT_WEBGL = 0x9247;

  // Buffer objects // WebGL1:
  #[alias = "bufferData"]
  pub fn buffer_data(target: GLenum, target: GLenum, target: GLenum);
  #[alias = "bufferData"] fn buffer_data(target: GLenum, #[AllowShared]
  Option<ArrayBuffer> srcData, target: GLenum);
  #[alias = "bufferData"] fn buffer_data(target: GLenum, #[AllowShared]
  ArrayBufferView srcData, target: GLenum);
  #[alias = "bufferSubData"] fn buffer_sub_data(target: GLenum, target: GLenum, #[AllowShared]
  ArrayBuffer srcData);
  #[alias = "bufferSubData"] fn buffer_sub_data(target: GLenum, target: GLenum, #[AllowShared]
  ArrayBufferView srcData);
  // WebGL2:
  void bufferData(target: GLenum, #[AllowShared] ArrayBufferView srcData, target: GLenum,
  GLuint srcOffset, #[optional = 0] length: GLuint);
  void bufferSubData(target: GLenum, target: GLenum, #[AllowShared] ArrayBufferView srcData,
  GLuint srcOffset, #[optional = 0] length: GLuint);

  void copyBufferSubData(readTarget: GLenum, readTarget: GLenum, readTarget: GLenum,
  GLintptr writeOffset, size: GLsizeiptr);
  // MapBufferRange, in particular its read-only and write-only modes,
  // can not be exposed safely to JavaScript. GetBufferSubData
  // replaces it for the purpose of fetching data back from the GPU.
  void getBufferSubData(target: GLenum, target: GLenum, #[AllowShared] ArrayBufferView dstData,
  #[optional = 0] dstOffset: GLuint, #[optional = 0] length: GLuint);

  // Framebuffer objects void blitFramebuffer(srcX0: GLint, srcX0: GLint, srcX0: GLint, srcX0: GLint, srcX0: GLint, srcX0: GLint,
  GLint dstX1, dstY1: GLint, dstY1: GLint, dstY1: GLint);
  void framebufferTextureLayer(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  layer: GLint);

  #[throws]
  #[alias = "invalidateFramebuffer"]
  pub fn invalidate_framebuffer(target: GLenum, target: GLenum);

  #[throws]
  void invalidateSubFramebuffer(target: GLenum, target: GLenum,
  x: GLint, y: GLint, y: GLint, y: GLint);

  #[alias = "readBuffer"]
  pub fn read_buffer(src: GLenum);

  // Renderbuffer objects #[throws]
  #[alias = "getInternalformatParameter"]
  pub fn get_internalformat_parameter(target: GLenum, target: GLenum, target: GLenum) -> any;
  void renderbufferStorageMultisample(target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei);

  // Texture objects void texStorage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei);
  void texStorage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, depth: GLsizei);

  // WebGL1 legacy entrypoints:
  #[throws]
  // Another overhead throws.
  void texImage2D(target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei,
  type: GLenum, #[AllowShared] Option<ArrayBufferView> pixels);
  #[throws]
  void texImage2D(target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum); // May throw DOMException
  #[throws]
  void texImage2D(target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum); // May throw DOMException
  #[throws]
  void texImage2D(target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum); // May throw DOMException
  #[throws]
  // Another overhead throws.
  void texImage2D(target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum);
  #[throws]
  // Another overhead throws.
  void texImage2D(target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum);
  #[throws]
  // Another overhead throws.
  void texImage2D(target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum);

  #[throws]
  // Another overhead throws.
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei,
  format: GLenum, type: GLenum, #[AllowShared] Option<ArrayBufferView> pixels);
  #[throws]
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum); // May throw DOMException
  #[throws]
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum); // May throw DOMException
  #[throws]
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum); // May throw DOMException
  #[throws]
  // Another overhead throws.
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum);
  #[throws]
  // Another overhead throws.
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum);
  #[throws]
  // Another overhead throws.
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  format: GLenum, type: GLenum, type: GLenum);

  // WebGL2 entrypoints:
  #[throws]
  // Another overhead throws.
  void texImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  border: GLint, format: GLenum, format: GLenum, format: GLenum);
  #[throws]
  void texImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  border: GLint, format: GLenum, format: GLenum,
  source: HTMLCanvasElement); // May throw DOMException
  #[throws]
  void texImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  border: GLint, format: GLenum, format: GLenum,
  source: HTMLImageElement); // May throw DOMException
  #[throws]
  void texImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  border: GLint, format: GLenum, format: GLenum,
  source: HTMLVideoElement); // May throw DOMException
  #[throws]
  // Another overhead throws.
  void texImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  border: GLint, format: GLenum, format: GLenum,
  source: ImageBitmap);
  #[throws]
  // Another overhead throws.
  void texImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  border: GLint, format: GLenum, format: GLenum,
  source: ImageData);
  #[throws]
  // Another overhead throws.
  void texImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  border: GLint, format: GLenum, format: GLenum,
  source: OffscreenCanvas);
  #[throws]
  // Another overhead throws.
  void texImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  border: GLint, format: GLenum, format: GLenum, #[AllowShared] ArrayBufferView srcData,
  GLuint srcOffset);

  #[throws]
  // Another overhead throws.
  void texImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  depth: GLsizei, border: GLint, border: GLint, border: GLint, border: GLint);
  #[throws]
  void texImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  depth: GLsizei, border: GLint, border: GLint, border: GLint,
  source: HTMLCanvasElement); // May throw DOMException
  #[throws]
  void texImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  depth: GLsizei, border: GLint, border: GLint, border: GLint,
  source: HTMLImageElement); // May throw DOMException
  #[throws]
  void texImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  depth: GLsizei, border: GLint, border: GLint, border: GLint,
  source: HTMLVideoElement); // May throw DOMException
  #[throws]
  // Another overhead throws.
  void texImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  depth: GLsizei, border: GLint, border: GLint, border: GLint,
  source: ImageBitmap);
  #[throws]
  // Another overhead throws.
  void texImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  depth: GLsizei, border: GLint, border: GLint, border: GLint,
  source: ImageData);
  #[throws]
  // Another overhead throws.
  void texImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  depth: GLsizei, border: GLint, border: GLint, border: GLint,
  source: OffscreenCanvas);
  #[throws]
  // Another overhead throws.
  void texImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  depth: GLsizei, border: GLint, border: GLint, border: GLint, #[AllowShared] Option<ArrayBufferView> srcData);
  #[throws]
  // Another overhead throws.
  void texImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  depth: GLsizei, border: GLint, border: GLint, border: GLint, #[AllowShared] ArrayBufferView srcData,
  GLuint srcOffset);

  #[throws]
  // Another overhead throws.
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, format: GLenum, format: GLenum, format: GLenum);
  #[throws]
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, format: GLenum, format: GLenum,
  source: HTMLCanvasElement); // May throw DOMException
  #[throws]
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, format: GLenum, format: GLenum,
  source: HTMLImageElement); // May throw DOMException
  #[throws]
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, format: GLenum, format: GLenum,
  source: HTMLVideoElement); // May throw DOMException
  #[throws]
  // Another overhead throws.
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, format: GLenum, format: GLenum,
  source: ImageBitmap);
  #[throws]
  // Another overhead throws.
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, format: GLenum, format: GLenum,
  source: ImageData);
  #[throws]
  // Another overhead throws.
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, format: GLenum, format: GLenum,
  source: OffscreenCanvas);
  #[throws]
  // Another overhead throws.
  void texSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, format: GLenum, format: GLenum, #[AllowShared] ArrayBufferView srcData,
  GLuint srcOffset);

  #[throws]
  // Another overhead throws.
  void texSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei,
  GLintptr pboOffset);
  #[throws]
  void texSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei,
  source: HTMLCanvasElement); // May throw DOMException
  #[throws]
  void texSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei,
  source: HTMLImageElement); // May throw DOMException
  #[throws]
  void texSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei,
  source: HTMLVideoElement); // May throw DOMException
  #[throws]
  // Another overhead throws.
  void texSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei,
  source: ImageBitmap);
  #[throws]
  // Another overhead throws.
  void texSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei,
  source: ImageData);
  #[throws]
  // Another overhead throws.
  void texSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei,
  source: OffscreenCanvas);
  #[throws]
  // Another overhead throws.
  void texSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei,
  #[AllowShared]
  Option<ArrayBufferView> srcData, #[optional = 0] srcOffset: GLuint);

  void copyTexSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  x: GLint, y: GLint, y: GLint, y: GLint);

  void compressedTexImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, border: GLint, border: GLint, border: GLint);
  void compressedTexImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, border: GLint, #[AllowShared] ArrayBufferView srcData,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLengthOverride: GLuint);

  void compressedTexImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, depth: GLsizei, depth: GLsizei, depth: GLsizei, depth: GLsizei);
  void compressedTexImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  height: GLsizei, depth: GLsizei, depth: GLsizei, #[AllowShared] ArrayBufferView srcData,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLengthOverride: GLuint);

  void compressedTexSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei, height: GLsizei);
  void compressedTexSubImage2D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  width: GLsizei, height: GLsizei, height: GLsizei,
  #[AllowShared]
  ArrayBufferView srcData,
  #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLengthOverride: GLuint);

  void compressedTexSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  zoffset: GLint, width: GLsizei, width: GLsizei, width: GLsizei,
  format: GLenum, imageSize: GLsizei, imageSize: GLsizei);
  void compressedTexSubImage3D(target: GLenum, target: GLenum, target: GLenum, target: GLenum,
  zoffset: GLint, width: GLsizei, width: GLsizei, width: GLsizei,
  format: GLenum, #[AllowShared] ArrayBufferView srcData,
  #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLengthOverride: GLuint);

  // Programs and shaders #[WebGLHandlesContextLoss]
  #[alias = "getFragDataLocation"]
  pub fn get_frag_data_location(program: WebGLProgram, program: WebGLProgram) -> GLint;

  // Uniforms void uniform1ui(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>);
  pub fn uniform2ui(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>);
  pub fn uniform3ui(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>);
  pub fn uniform4ui(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>);

  void uniform1fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);
  void uniform2fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);
  void uniform3fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);
  void uniform4fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);

  void uniform1iv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);
  void uniform2iv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);
  void uniform3iv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);
  void uniform4iv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);

  void uniform1uiv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);
  void uniform2uiv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);
  void uniform3uiv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);
  void uniform4uiv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, #[optional = 0] srcOffset: GLuint,
  #[optional = 0] srcLength: GLuint);

  void uniformMatrix2fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLength: GLuint);
  void uniformMatrix3x2fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLength: GLuint);
  void uniformMatrix4x2fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLength: GLuint);

  void uniformMatrix2x3fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLength: GLuint);
  void uniformMatrix3fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLength: GLuint);
  void uniformMatrix4x3fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLength: GLuint);

  void uniformMatrix2x4fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLength: GLuint);
  void uniformMatrix3x4fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLength: GLuint);
  void uniformMatrix4fv(location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>, location: Option<WebGLUniformLocation>,
  #[optional = 0] srcOffset: GLuint, #[optional = 0] srcLength: GLuint);

  // Vertex attribs void vertexAttribI4i(index: GLuint, index: GLuint, index: GLuint, index: GLuint, index: GLuint);
  #[alias = "vertexAttribI4iv"]
  pub fn vertex_attrib_i4iv(index: GLuint, index: GLuint);
  #[alias = "vertexAttribI4ui"]
  pub fn vertex_attrib_i4ui(index: GLuint, index: GLuint, index: GLuint, index: GLuint, index: GLuint);
  #[alias = "vertexAttribI4uiv"]
  pub fn vertex_attrib_i4uiv(index: GLuint, index: GLuint);
  #[alias = "vertexAttribIPointer"]
  pub fn vertex_attrib_i_pointer(index: GLuint, index: GLuint, index: GLuint, index: GLuint, index: GLuint);

  // Writing to the drawing buffer void vertexAttribDivisor(index: GLuint, index: GLuint);
  #[alias = "drawArraysInstanced"]
  pub fn draw_arrays_instanced(mode: GLenum, mode: GLenum, mode: GLenum, mode: GLenum);
  #[alias = "drawElementsInstanced"]
  pub fn draw_elements_instanced(mode: GLenum, mode: GLenum, mode: GLenum, mode: GLenum, mode: GLenum);
  #[alias = "drawRangeElements"]
  pub fn draw_range_elements(mode: GLenum, mode: GLenum, mode: GLenum, mode: GLenum, mode: GLenum, mode: GLenum);

  // Reading back pixels // WebGL1:
  #[throws, needs_caller_type]
  // throws on readback in a write-only context.
  void readPixels(x: GLint, x: GLint, x: GLint, x: GLint, x: GLint, x: GLint,
  #[AllowShared]
  Option<ArrayBufferView> dstData);
  // WebGL2:
  #[throws, needs_caller_type]
  // throws on readback in a write-only context.
  void readPixels(x: GLint, x: GLint, x: GLint, x: GLint, x: GLint, x: GLint,
  offset: GLintptr);
  #[throws, needs_caller_type]
  // throws on readback in a write-only context.
  void readPixels(x: GLint, x: GLint, x: GLint, x: GLint, x: GLint, x: GLint,
  #[AllowShared]
  ArrayBufferView dstData, dstOffset: GLuint);

  // Multiple Render Targets void drawBuffers(buffers: Vec<GLenum>);

  void clearBufferfv(buffer: GLenum, buffer: GLenum, buffer: GLenum,
  #[optional = 0] srcOffset: GLuint);
  void clearBufferiv(buffer: GLenum, buffer: GLenum, buffer: GLenum,
  #[optional = 0] srcOffset: GLuint);
  void clearBufferuiv(buffer: GLenum, buffer: GLenum, buffer: GLenum,
  #[optional = 0] srcOffset: GLuint);

  #[alias = "clearBufferfi"]
  pub fn clear_bufferfi(buffer: GLenum, buffer: GLenum, buffer: GLenum, buffer: GLenum);

  // Query Objects Option<WebGLQuery> createQuery();
  #[alias = "deleteQuery"]
  pub fn delete_query(query: Option<WebGLQuery>);
  #[WebGLHandlesContextLoss]
  #[alias = "isQuery"]
  pub fn is_query(query: Option<WebGLQuery>) -> GLboolean;
  #[alias = "beginQuery"]
  pub fn begin_query(target: GLenum, target: GLenum);
  #[alias = "endQuery"]
  pub fn end_query(target: GLenum);
  #[alias = "getQuery"]
  pub fn get_query(target: GLenum, target: GLenum) -> any;
  #[alias = "getQueryParameter"]
  pub fn get_query_parameter(query: WebGLQuery, query: WebGLQuery) -> any;

  // Sampler Objects Option<WebGLSampler> createSampler();
  #[alias = "deleteSampler"]
  pub fn delete_sampler(sampler: Option<WebGLSampler>);
  #[WebGLHandlesContextLoss]
  #[alias = "isSampler"]
  pub fn is_sampler(sampler: Option<WebGLSampler>) -> GLboolean;
  #[alias = "bindSampler"]
  pub fn bind_sampler(unit: GLuint, unit: GLuint);
  #[alias = "samplerParameteri"]
  pub fn sampler_parameteri(sampler: WebGLSampler, sampler: WebGLSampler, sampler: WebGLSampler);
  #[alias = "samplerParameterf"]
  pub fn sampler_parameterf(sampler: WebGLSampler, sampler: WebGLSampler, sampler: WebGLSampler);
  #[alias = "getSamplerParameter"]
  pub fn get_sampler_parameter(sampler: WebGLSampler, sampler: WebGLSampler) -> any;

  // Sync objects Option<WebGLSync> fenceSync(condition: GLenum, condition: GLenum);
  #[WebGLHandlesContextLoss]
  #[alias = "isSync"]
  pub fn is_sync(sync: Option<WebGLSync>) -> GLboolean;
  #[alias = "deleteSync"]
  pub fn delete_sync(sync: Option<WebGLSync>);
  #[alias = "clientWaitSync"]
  pub fn client_wait_sync(sync: WebGLSync, sync: WebGLSync, sync: WebGLSync) -> GLenum;
  #[alias = "waitSync"]
  pub fn wait_sync(sync: WebGLSync, sync: WebGLSync, sync: WebGLSync);
  #[alias = "getSyncParameter"]
  pub fn get_sync_parameter(sync: WebGLSync, sync: WebGLSync) -> any;

  // Transform Feedback Option<WebGLTransformFeedback> createTransformFeedback();
  #[alias = "deleteTransformFeedback"]
  pub fn delete_transform_feedback(tf: Option<WebGLTransformFeedback>);
  #[WebGLHandlesContextLoss]
  #[alias = "isTransformFeedback"]
  pub fn is_transform_feedback(tf: Option<WebGLTransformFeedback>) -> GLboolean;
  #[alias = "bindTransformFeedback"]
  pub fn bind_transform_feedback(target: GLenum, target: GLenum);
  #[alias = "beginTransformFeedback"]
  pub fn begin_transform_feedback(primitiveMode: GLenum);
  #[alias = "endTransformFeedback"]
  pub fn end_transform_feedback();
  #[alias = "transformFeedbackVaryings"]
  pub fn transform_feedback_varyings(program: WebGLProgram, program: WebGLProgram, program: WebGLProgram);
  #[new_object]
  #[alias = "getTransformFeedbackVarying"]
  pub fn get_transform_feedback_varying(program: WebGLProgram, program: WebGLProgram) -> Option<WebGLActiveInfo>;
  #[alias = "pauseTransformFeedback"]
  pub fn pause_transform_feedback();
  #[alias = "resumeTransformFeedback"]
  pub fn resume_transform_feedback();

  // Uniform Buffer Objects and Transform Feedback Buffers void bindBufferBase(target: GLenum, target: GLenum, target: GLenum);
  #[alias = "bindBufferRange"]
  pub fn bind_buffer_range(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum);
  #[throws]
  // GetOrCreateDOMReflector can fail.
  #[alias = "getIndexedParameter"]
  pub fn get_indexed_parameter(target: GLenum, target: GLenum) -> any;
  Vec<GLuint>? getUniformIndices(program: WebGLProgram, program: WebGLProgram);
  #[alias = "getActiveUniforms"]
  pub fn get_active_uniforms(program: WebGLProgram, program: WebGLProgram, program: WebGLProgram) -> any;
  #[alias = "getUniformBlockIndex"]
  pub fn get_uniform_block_index(program: WebGLProgram, program: WebGLProgram) -> GLuint;
  #[throws]
  // Creating a Uint32Array can fail.
  #[alias = "getActiveUniformBlockParameter"]
  pub fn get_active_uniform_block_parameter(program: WebGLProgram, program: WebGLProgram, program: WebGLProgram) -> any;
  #[alias = "getActiveUniformBlockName"]
  pub fn get_active_uniform_block_name(program: WebGLProgram, program: WebGLProgram) -> Option<DOMString>;
  #[alias = "uniformBlockBinding"]
  pub fn uniform_block_binding(program: WebGLProgram, program: WebGLProgram, program: WebGLProgram);

  // Vertex Array Objects Option<WebGLVertexArrayObject> createVertexArray();
  #[alias = "deleteVertexArray"]
  pub fn delete_vertex_array(vertexArray: Option<WebGLVertexArrayObject>);
  #[WebGLHandlesContextLoss]
  #[alias = "isVertexArray"]
  pub fn is_vertex_array(vertexArray: Option<WebGLVertexArrayObject>) -> GLboolean;
  #[alias = "bindVertexArray"]
  pub fn bind_vertex_array(array: Option<WebGLVertexArrayObject>);
}

WebGL2RenderingContext includes WebGLRenderingContextBase;
WebGL2RenderingContext includes WebGL2RenderingContextBase;

#[LegacyNoInterfaceObject,
 exposed = (Window, Worker)]
pub trait EXT_color_buffer_float {
}

#[LegacyNoInterfaceObject,
 exposed = (Window, Worker)]
pub trait OVR_multiview2 {
  const GLenum FRAMEBUFFER_ATTACHMENT_TEXTURE_NUM_VIEWS_OVR = 0x9630;
  const GLenum FRAMEBUFFER_ATTACHMENT_TEXTURE_BASE_VIEW_INDEX_OVR = 0x9632;
  const GLenum MAX_VIEWS_OVR = 0x9631;
  const GLenum FRAMEBUFFER_INCOMPLETE_VIEW_TARGETS_OVR = 0x9633;

  #[alias = "framebufferTextureMultiviewOVR"]
  pub fn framebuffer_texture_multiview_ovr(target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum, target: GLenum);
}
