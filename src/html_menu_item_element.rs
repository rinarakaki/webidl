// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-menuitem-element

// http://www.whatwg.org/specs/web-apps/current-work/#the-menuitem-element
#[exposed = Window, pref = "dom.menuitem.enabled"]
pub trait HTMLMenuItemElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub type: DOMString,
  #[ce_reactions, setter_throws]
  pub label: DOMString,
  #[ce_reactions, setter_throws]
  pub icon: DOMString,
  #[ce_reactions, setter_throws]
  pub disabled: bool,
  #[ce_reactions]
  pub checked: bool,
  #[ce_reactions, setter_throws]
  pub radiogroup: DOMString,

  // This should be 'default' but in the IDL implementation
  // this has been renamed 'defaultChecked'.
  #[ce_reactions, setter_throws]
  #[alias = "defaultChecked"]
  pub default_checked: bool,

  // Currently not implemented.
//  readonly attribute Option<HTMLElement> command;
}
