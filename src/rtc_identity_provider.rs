// http://w3c.github.io/webrtc-pc/ (with https://github.com/w3c/webrtc-pc/pull/178)

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait RTCIdentityProviderRegistrar {
  pub fn register(idp: RTCIdentityProvider);

  // Whether an IdP was passed to register() to chrome code. #[chrome_only]
  #[alias = "hasIdp"]
  has_idp: bool,
  // The following two chrome-only functions forward to the corresponding
  // function on the registered IdP. This is necessary because the
  // JS-implemented WebIDL can't see these functions on `idp` above, chrome JS
  // gets an Xray onto the content code that suppresses functions, see
  // https://developer.mozilla.org/en-US/docs/Xray_vision#Xrays_for_JavaScript_objects // Forward to idp.generateAssertion() */
  #[chrome_only, throws]
  Promise<RTCIdentityAssertionResult>  // TODO
  #[alias = "generateAssertion"]
  generate_assertion(contents: DOMString, contents: DOMString,
  #[optional = {}] options: RTCIdentityProviderOptions);
  // Forward to idp.validateAssertion() #[chrome_only, throws]
  Promise<RTCIdentityValidationResult>
  #[alias = "validateAssertion"]
  validate_assertion(assertion: DOMString, assertion: DOMString);
}

pub struct RTCIdentityProvider {
  required GenerateAssertionCallback generateAssertion;
  required ValidateAssertionCallback validateAssertion;
}

callback GenerateAssertionCallback =
  Promise<RTCIdentityAssertionResult>
  (contents: DOMString, contents: DOMString,
  options: RTCIdentityProviderOptions);
callback ValidateAssertionCallback =
  Promise<RTCIdentityValidationResult> (assertion: DOMString, assertion: DOMString);

pub struct RTCIdentityAssertionResult {
  required RTCIdentityProviderDetails idp;
  required DOMString assertion;
}

pub struct RTCIdentityProviderDetails {
  required DOMString domain;
  pub protocol: DOMString = "default",
}

pub struct RTCIdentityValidationResult {
  required DOMString identity;
  required DOMString contents;
}

pub struct RTCIdentityProviderOptions {
  pub protocol: DOMString = "default",
  #[alias = "usernameHint"]
  pub username_hint: DOMString,
  #[alias = "peerIdentity"]
  pub peer_identity: DOMString,
}

