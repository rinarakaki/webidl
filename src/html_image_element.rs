// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#htmlimageelement
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

pub trait imgINotificationObserver;
pub trait imgIRequest;
pub trait URI;
pub trait nsIStreamListener;

#[LegacyFactoryfunction = Image(optional width: u32, optional height: u32),
 exposed = Window]
pub trait HTMLImageElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub alt: DOMString,
  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws]
  pub src: DOMString,
  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws]
  pub srcset: DOMString,
  #[ce_reactions, setter_throws]
  pub crossOrigin: Option<DOMString>,
  #[ce_reactions, setter_throws]
  #[alias = "useMap"]
  pub use_map: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "referrerPolicy"]
  pub referrer_policy: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "isMap"]
  pub is_map: bool,
  #[ce_reactions, setter_throws]
  pub width: u32,
  #[ce_reactions, setter_throws]
  pub height: u32,
  #[ce_reactions, setter_throws]
  pub decoding: DOMString,
  #[ce_reactions, setter_throws, pref = "dom.image-lazy-loading.enabled"]
  pub loading: DOMString,
  #[alias = "naturalWidth"]
  natural_width: u32,
  #[alias = "naturalHeight"]
  natural_height: u32,
  complete: bool,
  #[new_object]
  pub fn decode() -> Promise<void>;
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLImageElement {
  #[ce_reactions, setter_throws]
  pub name: DOMString,
  #[ce_reactions, setter_throws]
  pub align: DOMString,
  #[ce_reactions, setter_throws]
  pub hspace: u32,
  #[ce_reactions, setter_throws]
  pub vspace: u32,
  #[ce_reactions, setter_throws]
  #[alias = "longDesc"]
  pub long_desc: DOMString,

  #[ce_reactions, setter_throws]
  pub border: DOMString,
}

// #[Update me: not in whatwg spec yet]
// http://picture.responsiveimages.org/#the-img-element
partial trait HTMLImageElement {
  #[ce_reactions, setter_throws]
  pub sizes: DOMString,
  #[alias = "currentSrc"]
  current_src: DOMString,
}

// Mozilla extensions.
partial trait HTMLImageElement {
  #[ce_reactions, setter_throws]
  pub lowsrc: DOMString,

  // These attributes are offsets from the closest view (to mimic
  // NS4's "offset-from-layer" behavior).
  x: i32,
  y: i32,
}

pub trait mixin MozImageLoadingContent {
  // Mirrored chrome-only nsIImageLoadingContent methods. Please make sure
  // to update this list if nsIImageLoadingContent changes.
  #[chrome_only]
  const i32 UNKNOWN_REQUEST = -1;
  #[chrome_only]
  const i32 CURRENT_REQUEST = 0;
  #[chrome_only]
  const i32 PENDING_REQUEST = 1;

  #[chrome_only]
  #[alias = "loadingEnabled"]
  pub loading_enabled: bool,
  // Same as addNativeObserver but intended for scripted observers or observers
  // from another or without a document.
  #[chrome_only]
  #[alias = "addObserver"]
  pub fn add_observer(aObserver: imgINotificationObserver);
  // Same as removeNativeObserver but intended for scripted observers or
  // observers from another or without a document.
  #[chrome_only]
  #[alias = "removeObserver"]
  pub fn remove_observer(aObserver: imgINotificationObserver);
  #[chrome_only,throws]
  #[alias = "getRequest"]
  pub fn get_request(aRequestType: i32) -> Option<imgIRequest>;
  #[chrome_only,throws]
  #[alias = "getRequestType"]
  pub fn get_request_type(aRequest: imgIRequest) -> i32;
  #[chrome_only]
  #[alias = "currentURI"]
  current_uri: Option<URI>,
  // Gets the final URI of the current request, if available.
  // Otherwise, returns None.
  #[chrome_only]
  #[alias = "currentRequestFinalURI"]
  current_request_final_uri: Option<URI>,
  // forceReload forces reloading of the image pointed to by currentURI
  //
  // @param aNotify request should notify
  // @throws NS_ERROR_NOT_AVAILABLE if there is no current URI to reload
  #[chrome_only,throws]
  #[alias = "forceReload"]
  pub fn force_reload(#[optional = true] aNotify: bool);
}

HTMLImageElement includes MozImageLoadingContent;
