// The origin of this IDL file is
// https://streams.spec.whatwg.org/#generic-reader-mixin-definition
// https://streams.spec.whatwg.org/#default-reader-class-definition

pub type ReadableStreamReader = (ReadableStreamDefaultReader or ReadableStreamBYOBReader);

pub enum ReadableStreamType { "bytes" };

pub trait mixin ReadableStreamGenericReader {
  closed: Promise<void>,

  #[throws]
  pub fn cancel(optional reason: any) -> Promise<void>;
}

#[exposed = (Window,Worker, Worklet),
pref = "dom.streams.expose.ReadableStreamDefaultReader"]
pub trait ReadableStreamDefaultReader {
  #[alias = "constructor"]
  pub fn new(stream: ReadableStream) -> Result<Self, Error>;

  #[throws]
  pub fn read() -> Promise<ReadableStreamDefaultReadResult>;

  #[throws]
  #[alias = "releaseLock"]
  pub fn release_lock();
}
ReadableStreamDefaultReader includes ReadableStreamGenericReader;

pub struct ReadableStreamDefaultReadResult {
 any value;
 bool done;
}
