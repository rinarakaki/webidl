// The origin of this IDL file is
// http://dev.w3.org/csswg/css-font-loading/#FontFaceSet-trait

pub struct FontFaceSetLoadEventInit: EventInit {
  pub fontfaces: Vec<FontFace> = [],
}

#[pref = "layout.css.font-loading-api.enabled",
 exposed = Window]
pub trait FontFaceSetLoadEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: FontFaceSetLoadEventInit);

  #[Cached, constant, Frozen]
  fontfaces: Vec<FontFace>,
}
