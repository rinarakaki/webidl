// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

#[ProbablyShortLivingWrapper,
 exposed = Window]
pub trait NodeList {
  getter Option<Node> item(index: u32);
  length: u32,
  iterable<Option<Node>>;
}
