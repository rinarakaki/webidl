// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-link-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-link-element
#[exposed = Window]
pub trait HTMLLinkElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub disabled: bool,
  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws, pure]
  pub href: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub crossOrigin: Option<DOMString>,
  #[ce_reactions, setter_throws, pure]
  pub rel: DOMString,
  #[put_forwards = value]
  #[alias = "relList"]
  rel_list: DOMTokenList,
  #[ce_reactions, setter_throws, pure]
  pub media: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub hreflang: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub type: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "referrerPolicy"]
  pub referrer_policy: DOMString,
  #[put_forwards = value]
  sizes: DOMTokenList,
  #[ce_reactions, setter_throws, pure]
  #[alias = "imageSrcset"]
  pub image_srcset: USVString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "imageSizes"]
  pub image_sizes: USVString,
}
HTMLLinkElement includes LinkStyle;

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLLinkElement {
  #[ce_reactions, setter_throws, pure]
  pub charset: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub rev: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub target: DOMString,
}

// https://w3c.github.io/webappsec/specs/subresourceintegrity/#htmllinkelement-1
partial trait HTMLLinkElement {
  #[ce_reactions, setter_throws]
  pub integrity: DOMString,
}

//https://w3c.github.io/preload/
partial trait HTMLLinkElement {
  #[setter_throws, pure]
  pub as: DOMString,
}
