// For Javascript markers, the |stack| of a ProfileTimelineMarker
// holds an object of this type. It intentionally looks like a
// SavedStack object and is a representation of the frame that is
// about to be constructed at the entry point.
#[GenerateConversionToJS]
pub struct ProfileTimelineStackFrame {
  pub line: i32,
  pub column: i32 = 0,
  pub source: DOMString,
  #[alias = "functionDisplayName"]
  pub function_display_name: DOMString,
  pub parent: Option<object> = None,
  #[alias = "asyncParent"]
  pub async_parent: Option<object> = None,
  #[alias = "asyncCause"]
  pub async_cause: DOMString,
}

pub struct ProfileTimelineLayerRect {
  pub x: i32 = 0,
  pub y: i32 = 0,
  pub width: i32 = 0,
  pub height: i32 = 0,
}

pub enum ProfileTimelineMessagePortOperationType {
  #[alias = "serializeData"]
  SerialiseData,
  #[alias = "deserializeData"]
  DeserialiseData,
}

pub enum ProfileTimelineWorkerOperationType {
  #[alias = "serializeDataOffMainThread"]
  SerialiseDataOffMainThread,
  #[alias = "serializeDataOnMainThread"]
  SerialiseDataOnMainThread,
  #[alias = "deserializeDataOffMainThread"]
  DeserialiseDataOffMainThread,
  #[alias = "deserializeDataOnMainThread"]
  DeserialiseDataOnMainThread,
}

#[GenerateConversionToJS]
pub struct ProfileTimelineMarker {
  pub name: DOMString = "",
  pub start: DOMHighResTimeStamp = 0,
  pub end: DOMHighResTimeStamp = 0,
  pub stack: Option<object> = None,

  #[alias = "processType"]
  pub process_type: u16,
  #[alias = "isOffMainThread"]
  pub is_off_mainThread: bool,

  // For ConsoleTime, Timestamp and Javascript markers. DOMString causeName;

  // For ConsoleTime markers. Option<object> endStack = None;

  // For DOMEvent markers. DOMString type;
  #[alias = "eventPhase"]
  pub event_phase: u16,

  // For document::DOMContentLoaded and document::Load markers. Using this
  // instead of the `start` and `end` timestamps is strongly discouraged. u64 unixTime; // in microseconds

  // For Paint markers. Vec<ProfileTimelineLayerRect> rectangles;

  // For Style markers. bool isAnimationOnly;

  // For MessagePort markers. ProfileTimelineMessagePortOperationType messagePortOperation;

  // For Worker markers. ProfileTimelineWorkerOperationType workerOperation;
}
