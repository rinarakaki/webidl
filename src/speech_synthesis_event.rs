// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

#[pref = "media.webspeech.synth.enabled",
 exposed = Window]
pub trait SpeechSynthesisEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;

  utterance: SpeechSynthesisUtterance,
  #[alias = "charIndex"]
  char_index: u32,
  readonly attribute Option<u32> charLength;
  #[alias = "elapsedTime"]
  elapsed_time: f32,
  name: Option<DOMString>,
}

pub struct SpeechSynthesisEventInit: EventInit {
  required SpeechSynthesisUtterance utterance;
  #[alias = "charIndex"]
  pub char_index: u32 = 0,
  Option<u32> charLength = None;
  #[alias = "elapsedTime"]
  pub elapsed_time: f32 = 0,
  pub name: DOMString = "",
}
