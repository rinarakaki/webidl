// For more information on this trait, please see
// https://html.spec.whatwg.org/#messageevent

#[exposed = (Window, Worker)]
pub trait MessageEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: MessageEventInit) -> Self;
  
  // Custom data associated with this event.
  #[getter_throws]
  data: any,

  // The origin of the site from which this event originated, which is the
  // scheme, ":", and if the URI has a host, "//" followed by the
  // host, and if the port is not the default for the given scheme,
  // ":" followed by that port. This value does not have a trailing slash.
  origin: USVString,

  // The last event ID string of the event source, for server-sent DOM events; this
  // value is the empty string for cross-origin messaging.
  #[alias = "lastEventId"]
  last_event_id: DOMString,

  // The window or port which originated this event.
  source: Option<MessageEventSource>,

  #[pure, Cached, Frozen]
  ports: Vec<MessagePort>,

  // Initializes this event with the given data, in a manner analogous to
  // the similarly-named method on the Event trait, also setting the
  // data, origin, source, and lastEventId attributes of this appropriately.
  void initMessageEvent(type: DOMString,
  #[optional = false] bubbles: bool,
  #[optional = false] cancelable: bool,
  #[optional = None] data: any,
  #[optional = ""] origin: DOMString,
  #[optional = ""] lastEventId: DOMString,
  optional Option<MessageEventSource> source = None,
  optional Vec<MessagePort> ports = []);
}

pub struct MessageEventInit: EventInit {
  pub data: any = None,
  pub origin: DOMString = "",
  #[alias = "lastEventId"]
  pub last_event_id: DOMString = "",
  pub source: Option<MessageEventSource> = None,
  pub ports: Vec<MessagePort> = [],
}

pub type MessageEventSource = (WindowProxy or MessagePort or ServiceWorker);
