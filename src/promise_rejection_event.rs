// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/webappapis.html#the-promiserejectionevent-trait

#[exposed = (Window, Worker)]
pub trait PromiseRejectionEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString);

  #[binary_name = "rejectedPromise"]
  promise: Promise<any>,
  reason: any,
}

pub struct PromiseRejectionEventInit: EventInit {
  required Promise<any> promise;
  pub reason: any,
}
