// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#validitystate

#[exposed = Window]
pub trait ValidityState {
  #[alias = "valueMissing"]
  value_missing: bool,
  #[alias = "typeMismatch"]
  type_mismatch: bool,
  #[alias = "patternMismatch"]
  pattern_mismatch: bool,
  #[alias = "tooLong"]
  too_long: bool,
  #[alias = "tooShort"]
  too_short: bool,
  #[alias = "rangeUnderflow"]
  range_underflow: bool,
  #[alias = "rangeOverflow"]
  range_overflow: bool,
  #[alias = "stepMismatch"]
  step_mismatch: bool,
  #[alias = "badInput"]
  bad_input: bool,
  #[alias = "customError"]
  custom_error: bool,
  valid: bool,
}

