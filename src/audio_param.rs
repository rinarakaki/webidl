// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/#enumdef-automationrate
// https://webaudio.github.io/web-audio-api/#audioparam

use super::*;

pub enum AutomationRate {
  #[alias = "a-rate"]
  ARate,
  #[alias = "k-rate"]
  KRate
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait AudioParam {
  #[setter_throws]
  pub mut value: f32,
  #[alias = "defaultValue"]
  pub default_value: f32,
  #[alias = "minValue"]
  pub min_value: f32,
  #[alias = "maxValue"]
  pub max_value: f32,

  // Parameter automation. 
  #[alias = "setValueAtTime"]
  pub fn set_value_at_time(&self, value: f32, start_time: f64)
    -> Result<AudioParam>;

  #[alias = "linearRampToValueAtTime"]
  pub fn linear_ramp_to_value_at_time(&self, value: f32, end_time: f64)
    -> Result<AudioParam>;

  #[alias = "exponentialRampToValueAtTime"]
  pub fn exponential_ramp_to_value_at_time(&self, value: f32, end_time: f64)
    -> Result<AudioParam>;

  // Exponentially approach the target value with a rate having the given time constant. 
  #[alias = "setTargetAtTime"]
  pub fn set_target_at_time(
    &self, target: f32, start_time: f64, time_constant: f64)
    -> Result<AudioParam>;

  // Sets an array of arbitrary parameter values starting at time for the given duration. 
  // The number of values will be scaled to fit into the desired duration. 
  #[alias = "setValueCurveAtTime"]
  pub fn set_value_curve_at_time(
    &self, values: Vec<f32>, start_time: f64, duration: f64)
    -> Result<AudioParam>;

  // Cancels all scheduled parameter changes with times greater than or equal to startTime. 
  #[alias = "cancelScheduledValues"]
  pub fn cancel_scheduled_values(&self, start_time: f64) -> Result<AudioParam>;
}

// Mozilla extension
partial trait AudioParam {
  // The ID of the AudioNode this AudioParam belongs to.
  #[chrome_only]
  #[alias = "parentNodeId"]
  pub parent_node_id: u32,
  // The name of the AudioParam
  #[chrome_only]
  pub name: DOMString,
}

partial trait AudioParam {
  // This attribute is used for mochitest only.
  #[chrome_only]
  #[alias = "isTrackSuspended"]
  pub is_track_suspended: bool,
}
