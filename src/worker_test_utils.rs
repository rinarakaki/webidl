#[exposed = Worker, pref = "dom.workers.testing.enabled"]
namespace WorkerTestUtils {
  #[throws]
  u32 currentTimerNestingLevel();
}

