// The origin of this IDL file is
// https://dvcs.w3.org/hg/html-media/raw-file/default/encrypted-media/encrypted-media.html

use super::Event;

#[exposed = Window]
pub trait MediaEncryptedEvent: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] eventInitDict: MediaKeyNeededEventInit) -> Result<Self>;

  #[alias = "initDataType"]
  init_data_type: DOMString,
  #[throws]
  #[alias = "initData"]
  init_data: Option<ArrayBuffer>,
}

pub struct MediaKeyNeededEventInit: EventInit {
  #[alias = "initDataType"]
  pub init_data_type: DOMString = "",
  #[alias = "initData"]
  pub init_data: Option<ArrayBuffer> = None,
}
