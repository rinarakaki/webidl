// The origin of this IDL file is
// www.w3.org/TR/2012/WD-XMLHttpRequest-20120117/

use super::XMLHTTPRequestEventTarget;

#[exposed = (Window, DedicatedWorker, SharedWorker)]
#[alias = "XMLHttpRequestUpload"]
pub trait XMLHTTPRequestUpload: XMLHTTPRequestEventTarget {}
