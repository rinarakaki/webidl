// The origin of this IDL file is
// https://webaudio.github.io/web-midi-api/

#[SecureContext,
 pref = "dom.webmidi.enabled",
 exposed = Window]
pub trait MIDIConnectionEvent: Event
{
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: MIDIConnectionEventInit);

  port: Option<MIDIPort>,
}

pub struct MIDIConnectionEventInit: EventInit
{
  pub port: Option<MIDIPort> = None,
}
