#[exposed = Window]
pub trait Screen: EventTarget {
  // CSSOM-View
  // http://dev.w3.org/csswg/cssom-view/#the-screen-trait
  #[throws]
  #[alias = "availWidth"]
  avail_width: i32,
  #[throws]
  #[alias = "availHeight"]
  avail_height: i32,
  #[throws]
  width: i32,
  #[throws]
  height: i32,
  #[throws]
  #[alias = "colorDepth"]
  color_depth: i32,
  #[throws]
  #[alias = "pixelDepth"]
  pixel_depth: i32,

  #[throws]
  top: i32,
  #[throws]
  left: i32,
  #[throws]
  #[alias = "availTop"]
  avail_top: i32,
  #[throws]
  #[alias = "availLeft"]
  avail_left: i32,

  // DEPRECATED, use ScreenOrientation API instead.
  // Returns the current screen orientation.
  // Can be: landscape-primary, landscape-secondary,
  //         portrait-primary or portrait-secondary.
  #[needs_caller_type]
  #[alias = "mozOrientation"]
  moz_orientation: DOMString,

  #[alias = "onmozorientationchange"]
  pub onmozorientationchange: EventHandler,

  // DEPRECATED, use ScreenOrientation API instead.
  // Lock screen orientation to the specified type.
  #[throws]
  #[alias = "mozLockOrientation"]
  pub fn moz_lock_orientation(orientation: DOMString) -> bool;
  #[throws]
  #[alias = "mozLockOrientation"]
  pub fn moz_lock_orientation(orientation: Vec<DOMString>) -> bool;

  // DEPRECATED, use ScreenOrientation API instead.
  // Unlock the screen orientation.
  #[alias = "mozUnlockOrientation"]
  pub fn moz_unlock_orientation();
}

// https://w3c.github.io/screen-orientation
partial trait Screen {
  orientation: ScreenOrientation,
}

// https://wicg.github.io/media-capabilities/#idl-index
#[alias = "ScreenColorGamut"]
pub enum ScreenColourGamut {
  #[alias = "srgb"]
  Srgb,
  #[alias = "p3"]
  P3,
  #[alias = "rec2020"]
  Rec2020,
}

#[func = "nsScreen::MediaCapabilitiesEnabled",
 exposed = Window]
pub trait ScreenLuminance {
  min: f64,
  max: f64,
  #[alias = "maxAverage"]
  max_average: f64,
}

partial trait Screen {
  #[func = "nsScreen::MediaCapabilitiesEnabled"]
  #[alias = "colorGamut"]
  colour_gamut: ScreenColourGamut,
  #[func = "nsScreen::MediaCapabilitiesEnabled"]
  luminance: Option<ScreenLuminance>,

  #[func = "nsScreen::MediaCapabilitiesEnabled"]
  #[alias = "onchange"]
  pub on_change: EventHandler,
}
