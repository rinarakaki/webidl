// The origin of this IDL file is
// https://w3c.github.io/netinfo/

pub enum ConnectionType {
  #[alias = "cellular"]
  Cellular,
  #[alias = "bluetooth"]
  Bluetooth,
  #[alias = "ethernet"]
  Ethernet,
  #[alias = "wifi"]
  Wifi,
  #[alias = "other"]
  Other,
  #[alias = "none"]
  None,
  #[alias = "unknown"]
  Unknown
}

#[pref = "dom.netinfo.enabled",
  exposed = (Window, Worker)]
pub trait NetworkInformation: EventTarget {
  type: ConnectionType,
  #[alias = "ontypechange"]
  pub on_type_change: EventHandler,
}
