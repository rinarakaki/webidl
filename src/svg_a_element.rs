// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAElement: SVGGraphicsElement {
  target: SVGAnimatedString,

  #[setter_throws]
  pub download: DOMString,
  #[setter_throws]
  pub ping: DOMString,
  #[setter_throws]
  pub rel: DOMString,
  #[setter_throws]
  #[alias = "referrerPolicy"]
  pub referrer_policy: DOMString,
  #[put_forwards = value]
  #[alias = "relList"]
  rel_list: DOMTokenList,
  #[setter_throws]
  pub hreflang: DOMString,
  #[setter_throws]
  pub type: DOMString,

  #[throws]
  pub text: DOMString,
}

SVGAElement includes SVGURIReference;

