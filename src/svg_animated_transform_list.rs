// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimatedTransformList {
  #[constant]
  #[alias = "baseVal"]
  base_val: SVGTransformList,
  #[constant]
  #[alias = "animVal"]
  anim_val: SVGTransformList,
}

