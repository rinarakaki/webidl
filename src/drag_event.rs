// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/#dragevent

#[exposed = Window]
pub trait DragEvent: MouseEvent {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: DragEventInit) -> Self;

  #[alias = "dataTransfer"]
  data_transfer: Option<DataTransfer>,

  void initDragEvent(type: DOMString,
  #[optional = false] canBubble: bool,
  #[optional = false] cancelable: bool,
  optional Option<Window> aView = None,
  #[optional = 0] aDetail: i32,
  #[optional = 0] aScreenX: i32,
  #[optional = 0] aScreenY: i32,
  #[optional = 0] aClientX: i32,
  #[optional = 0] aClientY: i32,
  #[optional = false] aCtrlKey: bool,
  #[optional = false] aAltKey: bool,
  #[optional = false] aShiftKey: bool,
  #[optional = false] aMetaKey: bool,
  #[optional = 0] aButton: u16,
  optional Option<EventTarget> aRelatedTarget = None,
  optional Option<DataTransfer> aDataTransfer = None);
}

pub struct DragEventInit: MouseEventInit {
  #[alias = "dataTransfer"]
  pub data_transfer: Option<DataTransfer> = None,
}
