// This pub struct is used for exposing failed channel certificate information
// to about:certerror to display information.

pub struct FailedCertSecurityInfo {
  #[alias = "errorCodeString"]
  pub error_code_string: DOMString = "",
  #[alias = "isUntrusted"]
  pub is_untrusted: bool = false,
  #[alias = "isDomainMismatch"]
  pub is_domain_mismatch: bool = false,
  #[alias = "isNotValidAtThisTime"]
  pub is_not_valid_at_this_time: bool = false,
  #[alias = "validNotBefore"]
  pub valid_not_before: DOMTimeStamp = 0,
  #[alias = "validNotAfter"]
  pub valid_not_after: DOMTimeStamp = 0,
  #[alias = "issuerCommonName"]
  pub issuer_common_name: DOMString = "",
  #[alias = "certValidityRangeNotAfter"]
  pub cert_validity_range_not_after: DOMTimeStamp = 0,
  #[alias = "certValidityRangeNotBefore"]
  pub cert_validity_range_not_before: DOMTimeStamp = 0,
  #[alias = "errorMessage"]
  pub error_message: DOMString = "",
  #[alias = "hasHSTS"]
  pub has_hsts: bool = true,
  #[alias = "hasHPKP"]
  pub has_hpkp: bool = true,
  #[alias = "certChainStrings"]
  pub cert_chain_strings: Vec<DOMString>,
}
