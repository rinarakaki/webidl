// TCPSocket exposes a TCP client socket (no server sockets yet)
// to highly privileged apps. It provides a buffered, non-blocking
// trait for sending. For receiving, it uses an asynchronous,
// event handler based trait.

pub trait nsISocketTransport;

pub enum TCPSocketBinaryType {
  #[alias = "arraybuffer"]
  Arraybuffer,
  #[alias = "string"]
  String
}

pub struct SocketOptions {
  #[alias = "useSecureTransport"]
  pub use_secure_transport: bool = false,
  #[alias = "binaryType"]
  pub binary_type: TCPSocketBinaryType = "string",
}

pub enum TCPReadyState {
  #[alias = "connecting"]
  Connecting,
  #[alias = "open"]
  Open,
  #[alias = "closing"]
  Closing,
  #[alias = "closed"]
  Closed,
}

#[LegacyNoInterfaceObject,
  exposed = Window]
pub trait LegacyMozTCPSocket {
  // Legacy constructor for API compatibility.
  #[throws]
  pub fn open(host: DOMString, host: DOMString,
    #[optional = {}] options: SocketOptions) -> TCPSocket;

  #[throws]
  pub fn listen(port: u16, #[optional = {}] options: ServerSocketOptions,
    #[optional = 0] backlog: u16) -> TCPServerSocket;
}

#[func = "mozilla::dom::TCPSocket::ShouldTCPSocketExist",
  exposed = Window]
pub trait TCPSocket: EventTarget {
  #[throws]
  constructor(
    host: DOMString, host: DOMString,
    #[optional = {}] options: SocketOptions);

  // Upgrade an insecure connection to use TLS. throws if the ready state is not OPEN.
  #[throws]
  #[alias = "upgradeToSecure"]
  pub fn upgrade_to_secure();

  // The raw internal socket transport.
  transport: Option<nsISocketTransport>,

  // The UTF16 host of this socket object.
  host: USVString,

  // The port of this socket object.
  port: u16,

  // True if this socket object is an SSL socket.
  ssl: bool,

  // The number of bytes which have previously been buffered by calls to
  // send on this socket.
  #[alias = "bufferedAmount"]
  buffered_amount: u64,

  // Pause reading incoming data and invocations of the ondata handler until
  // resume is called. Can be called multiple times without resuming.
  pub fn suspend();

  // Resume reading incoming data and invoking ondata as usual. There must be
  // an equal number of resume as suspends that took place. throws if the
  // socket is not suspended.
  #[throws]
  pub fn resume();

  // Close the socket.
  pub fn close();

  // Close the socket immediately without waiting for unsent data.
  #[chrome_only]
  #[alias = "closeImmediately"]
  pub fn close_immediately();

  // Write data to the socket.
  //
  // @param data The data to write to the socket.
  //
  // @return Send returns true or false as a hint to the caller that
  //         they may either continue sending more data immediately, or
  //         may want to wait until the other side has read some of the
  //         data which has already been written to the socket before
  //         buffering more. If send returns true, then less than 64k
  //         has been buffered and it's safe to immediately write more.
  //         If send returns false, then more than 64k has been buffered,
  //         and the caller may wish to wait until the ondrain event
  //         handler has been called before buffering more data by more
  //         calls to send.
  // 
  // @throws throws if the ready state is not OPEN.
  #[throws]
  pub fn send(data: ByteString) -> bool;

  // Write data to the socket.
  //
  // @param data The data to write to the socket.
  // @param byteOffset The offset within the data from which to begin writing.
  // @param byteLength The number of bytes to write.
  //                   Defaults to the byte length of the ArrayBuffer if not present,
  //                   and clamped to (length - byteOffset).
  //
  // @return Send returns true or false as a hint to the caller that
  //         they may either continue sending more data immediately, or
  //         may want to wait until the other side has read some of the
  //         data which has already been written to the socket before
  //         buffering more. If send returns true, then less than 64k
  //         has been buffered and it's safe to immediately write more.
  //         If send returns false, then more than 64k has been buffered,
  //         and the caller may wish to wait until the ondrain event
  //         handler has been called before buffering more data by more
  //         calls to send.
  // 
  // @throws throws if the ready state is not OPEN.
  #[throws]
  pub fn send(data: ArrayBuffer, #[optional = 0] byteOffset: u32, optional byteLength: u32) -> bool;

  // The readyState attribute indicates which state the socket is currently
  // in.
  #[alias = "readyState"]
  ready_state: TCPReadyState,

  // The binaryType attribute indicates which mode this socket uses for
  // sending and receiving data. If the binaryType: "arraybuffer" option
  // was passed to the open method that created this socket, binaryType
  // will be "arraybuffer". Otherwise, it will be "string".
  #[alias = "binaryType"]
  binary_type: TCPSocketBinaryType,

  // The "open" event is dispatched when the connection to the server
  // has been established. If the connection is refused, the "error" event
  // will be dispatched, instead.
  #[alias = "onopen"]
  pub onopen: EventHandler,

  // After send has buffered more than 64k of data, it returns false to
  // indicate that the client should pause before sending more data, to
  // avoid accumulating large buffers. This is only advisory, and the client
  // is free to ignore it and buffer as much data as desired, but if reducing
  // the size of buffers is important (especially for a streaming application)
  // the "drain" event will be dispatched once the previously-buffered data has
  // been written to the network, at which point the client can resume calling
  // send again.
  #[alias = "ondrain"]
  pub ondrain: EventHandler,

  // The "data" event will be dispatched repeatedly and asynchronously after
  // "open" is dispatched, every time some data was available from the server
  // and was read. The event object will be a TCPSocketEvent; if the "arraybuffer"
  // binaryType was passed to the constructor, the data attribute of the event
  // object will be an ArrayBuffer. If not, it will be a normal JavaScript string,
  // truncated at the first None byte found in the payload and the remainder
  // interpreted as ASCII bytes.
  //
  // At any time, the client may choose to pause reading and receiving "data"
  // events by calling the socket's suspend() method. Further "data" events
  // will be paused until resume() is called.
  #[alias = "ondata"]
  pub ondata: EventHandler,

  // The "error" event will be dispatched when there is an error. The event
  // object will be a TCPSocketErrorEvent.
  //
  // If an "error" event is dispatched before an "open" one, the connection
  // was refused, and the "close" event will not be dispatched. If an "error"
  // event is dispatched after an "open" event, the connection was lost,
  // and a "close" event will be dispatched subsequently.
  #[alias = "onerror"]
  pub on_error: EventHandler,

  // The "close" event is dispatched once the underlying network socket
  // has been closed, either by the server, or by the client calling
  // close.
  //
  // If the "error" event was not dispatched before "close", then one of
  // the sides cleanly closed the connection.
  #[alias = "onclose"]
  pub onclose: EventHandler, 
}
