// The origin of this IDL file is
// http://www.w3.org/TR/user-timing/#performancemeasure

#[exposed = (Window, Worker)]
pub trait PerformanceMeasure: PerformanceEntry {
}
