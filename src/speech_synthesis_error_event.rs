// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

pub enum SpeechSynthesisErrorCode {
  #[alias = "canceled"]
  Cancelled,
  #[alias = "interrupted"]
  Interrupted,
  #[alias = "audio-busy"]
  AudioBusy,
  #[alias = "audio-hardware"]
  AudioHardware,
  #[alias = "network"]
  Network,
  #[alias = "synthesis-unavailable"]
  SynthesisUnavailable,
  #[alias = "synthesis-failed"]
  SynthesisFailed,
  #[alias = "language-unavailable"]
  LanguageUnavailable,
  #[alias = "voice-unavailable"]
  VoiceUnavailable,
  #[alias = "text-too-long"]
  TextTooLong,
  #[alias = "invalid-argument"]
  InvalidArgument,
}

#[pref = "media.webspeech.synth.enabled",
  exposed = Window]
pub trait SpeechSynthesisErrorEvent: SpeechSynthesisEvent {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;

  error: SpeechSynthesisErrorCode,
}

pub struct SpeechSynthesisErrorEventInit: SpeechSynthesisEventInit {
  required SpeechSynthesisErrorCode error;
}
