// The origin of this IDL file is
// https://w3c.github.io/webrtc-pc/ Editor's Draft 18 January 2018

use super::RTCRTPSourceEntryType;

#[alias = "RTCRtpContributingSource"]
pub struct RTCRTPContributingSource {
  required DOMHighResTimeStamp timestamp;
  required u32 source;
  #[alias = "audioLevel"]
  pub audio_level: f64,
  required u32 rtpTimestamp;
}

#[alias = "RTCRtpSynchronizationSource"]
pub struct RTCRTPSynchronisationSource: RTCRTPContributingSource {
  #[alias = "voiceActivityFlag"]
  pub voice_activity_flag: Option<bool>,
}

// Internal enum of types used by RTCRtpSourceEntryenum RTCRtpSourceEntryType {
  #[alias = "contributing"]
  Contributing,
  #[alias = "synchronization"]
  Synchronisation,
}
// Internal shared representation of Contributing and Synchronization sourcespub struct RTCRtpSourceEntry: RTCRtpSynchronizationSource {
  required RTCRTPSourceEntryType sourceType;
}
