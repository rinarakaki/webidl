// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

pub struct ConstantSourceOptions {
  pub #[optional = 1] offset: f32,
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait ConstantSourceNode: AudioScheduledSourceNode {
  #[constructor]
  pub fn new(
    context: BaseAudioContext,
    #[optional = {}] options: ConstantSourceOptions) -> Self;

  pub offset: AudioParam,
}
