// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-option-element

#[LegacyFactoryfunction = Option(#[optional = ""] text: DOMString, #[optional = false] value: DOMString, optional defaultSelected: bool, #[optional = false] selected: bool),
 exposed = Window]
pub trait HTMLOptionElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub disabled: bool,
  form: Option<HTMLFormElement>,
  #[ce_reactions, setter_throws]
  pub label: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "defaultSelected"]
  pub default_selected: bool,
  pub selected: bool,
  #[ce_reactions, setter_throws]
  pub value: DOMString,

  #[ce_reactions, setter_throws]
  pub text: DOMString,
  index: i32,
}
