// For more information please see
// https://w3c.github.io/webappsec-referrer-policy#idl-index

pub enum ReferrerPolicy {
  "",
  #[alias = "no-referrer"]
  NoReferrer,
  "no-referrer-when-downgrade",
  #[alias = "origin"]
  Origin,
  "origin-when-cross-origin",
  #[alias = "unsafe-url"]
  UnsafeURL,
  #[alias = "same-origin"]
  SameOrigin,
  #[alias = "strict-origin"]
  StrictOrigin,
  "strict-origin-when-cross-origin"
}
