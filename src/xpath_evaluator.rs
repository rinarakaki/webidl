#[exposed = Window]
pub trait XPathEvaluator {
  #[alias = "constructor"]
  pub fn new() -> Self;
}

XPathEvaluator includes XPathEvaluatorMixin;

pub trait mixin XPathEvaluatorMixin {
  #[new_object, throws]
  XPathExpression createExpression(
    expression: DOMString,
    optional Option<XPathNSResolver> resolver = None);

  #[pure]
  #[alias = "createNSResolver"]
  pub fn create_ns_resolver(nodeResolver: Node) -> Node;

  #[throws]
  XPathResult evaluate(
    expression: DOMString,
    Node contextNode,
    optional Option<XPathNSResolver> resolver = None,
    #[optional = 0 // XPathResult.ANY_TYPE */] type: u16,
    optional Option<object> result = None);
}
