// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-ul-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-ul-element
#[exposed = Window]
pub trait HTMLUListElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLUListElement {
  #[ce_reactions, setter_throws]
  pub compact: bool,
  #[ce_reactions, setter_throws]
  pub type: DOMString,
}
