// The origin of this IDL file is
// https://dvcs.w3.org/hg/webcrypto-api/raw-file/tip/spec/Overview.html#crypto-trait

use super::*;

#[exposed = (Window, Worker)]
pub trait mixin GlobalCrypto {
  #[throws]
  pub crypto: Crypto,
}

#[exposed = (Window, Worker)]
pub trait Crypto {
  #[SecureContext]
  pub subtle: SubtleCrypto,

  #[alias = "getRandomValues"]
  pub fn get_random_values(&self, array: ArrayBufferView)
    -> Result<ArrayBufferView>;

  #[SecureContext, pref = "dom.crypto.randomUUID.enabled"]
  #[alias = "randomUUID"]
  pub fn random_uuid(&self) -> DOMString;
}
