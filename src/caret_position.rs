use super::*;

#[exposed = Window]
pub trait CaretPosition {
  // The offsetNode could potentially be None due to anonymous content.
  #[alias = "offsetNode"]
  pub offset_node: Option<Node>,
  pub offset: u32,

}

// Gecko specific methods and properties for CaretPosition.
partial trait CaretPosition {
  #[alias = "getClientRect"]
  pub fn get_client_rect(&self) -> Option<DOMRect>;
}
