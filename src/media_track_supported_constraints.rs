// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/getusermedia.html

pub struct MediaTrackSupportedConstraints {
  pub width: bool = true,
  pub height: bool = true,
  #[alias = "aspectRatio"]
  pub aspect_ratio: bool, // to be supported
  #[alias = "frameRate"]
  pub frame_rate: bool = true,
  #[alias = "facingMode"]
  pub facing_mode: bool = true,
  pub volume: bool, // to be supported
  #[alias = "sampleRate"]
  pub sample_rate: bool, // to be supported
  #[alias = "sampleSize"]
  pub sample_size: bool, // to be supported
  #[alias = "echoCancellation"]
  pub echo_cancellation: bool = true,
  #[alias = "noiseSuppression"]
  pub noise_suppression: bool = true,
  #[alias = "autoGainControl"]
  pub auto_gain_control: bool = true,
  pub latency: bool, // to be supported
  #[alias = "channelCount"]
  pub channel_count: bool = true,
  pub deviceId: bool = true,
  pub groupId: bool = true,

  // Mozilla-specific extensions:

  // http://fluffy.github.io/w3c-screen-share/#screen-based-video-constraints
  // OBE by http://w3c.github.io/mediacapture-screen-share

  #[alias = "mediaSource"]
  pub media_source: bool = true,

  // Experimental https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1131568#c3
  //              https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1193075

  #[alias = "browserWindow"]
  pub browser_window: bool = true,
  #[alias = "scrollWithPage"]
  pub scroll_with_page: bool = true,
  #[alias = "viewportOffsetX"]
  pub viewport_offsetX: bool = true,
  #[alias = "viewportOffsetY"]
  pub viewport_offsetY: bool = true,
  #[alias = "viewportWidth"]
  pub viewport_width: bool = true,
  #[alias = "viewportHeight"]
  pub viewport_height: bool = true,
}
