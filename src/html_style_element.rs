// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-style-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

#[exposed = Window]
pub trait HTMLStyleElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[pure]
  pub disabled: bool,
  #[ce_reactions, setter_throws, pure]
  pub media: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub type: DOMString,
}
HTMLStyleElement includes LinkStyle;

// Mozilla-specific additions to support devtools
partial trait HTMLStyleElement {
  // Mark this style element with a devtools-specific principal that
  // skips Content Security Policy unsafe-inline checks. This triggering
  // principal will be overwritten by any callers that set textContent
  // or innerHTML on this element.
  #[chrome_only]
  #[alias = "setDevtoolsAsTriggeringPrincipal"]
  pub fn set_devtools_as_triggering_principal();
}
