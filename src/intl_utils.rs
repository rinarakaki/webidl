#[GenerateConversionToJS]
pub struct DisplayNameOptions {
  pub type: DOMString,
  pub style: DOMString,
  pub calendar: DOMString,
  pub keys: Vec<DOMString>,
}

#[GenerateInit]
pub struct DisplayNameResult {
  pub locale: DOMString,
  pub type: DOMString,
  pub style: DOMString,
  pub calendar: DOMString,
  pub values: Vec<DOMString>,
}

#[GenerateInit]
pub struct LocaleInfo {
  pub locale: DOMString,
  pub direction: DOMString,
}

// The IntlUtils trait provides helper functions for localization.
#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait IntlUtils {
  // Helper function to retrieve the localized values for a list of requested
  // keys.
  //
  // The function takes two arguments - locales which is a list of locale
  // strings and options which is an object with four optional properties:
  //
  //   keys:
  //     an Array of string values to localize
  //
  //   type:
  //     a String with a value "language", "region", "script", "currency",
  //     "weekday", "month", "quarter", "dayPeriod", or "dateTimeField"
  //
  //   style:
  //     a String with a value "long", "abbreviated", "short", or "narrow"
  //
  //   calendar:
  //     a String to select a specific calendar type, e.g. "gregory"
  //
  // It returns an object with properties:
  //
  //   locale:
  //     a negotiated locale string
  //
  //   type:
  //     negotiated type
  //
  //   style:
  //     negotiated style
  //
  //   calendar:
  //     negotiated calendar
  //
  //   values:
  //     a list of translated values for the requested keys
  //
  #[throws]
  DisplayNameResult getDisplayNames(locales: Vec<DOMString>,
  #[optional = {}] options: DisplayNameOptions);

  // Helper function to determine if the current application locale is RTL.
  //
  // The result of this function can be overriden by this pref:
  //  - `intl.l10n.pseudo`
  #[alias = "isAppLocaleRTL"]
  pub fn is_app_locale_rtl() -> bool;
}
