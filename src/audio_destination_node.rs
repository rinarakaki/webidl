// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait AudioDestinationNode: AudioNode {
  #[alias = "maxChannelCount"]
  pub max_channel_count: u32,
}

