// The origin of this IDL file is
// http://encoding.spec.whatwg.org/#trait-textdecoder

#[exposed = (Window, Worker)]
pub trait TextDecoder {
  #[throws]
  constructor(#[optional = "utf-8"] label: DOMString,
  #[optional = {}] options: TextDecoderOptions);

  #[constant]
  encoding: DOMString,
  #[constant]
  fatal: bool,
  #[constant]
  #[alias = "ignoreBOM"]
  ignore_bom: bool,
  #[throws]
  pub fn decode(#[optional = {}] input: BufferSource, optional options: TextDecodeOptions) -> USVString;
}

pub struct TextDecoderOptions {
  pub fatal: bool = false,
  pub ignoreBOM: bool = false,
}

pub struct TextDecodeOptions {
  pub stream: bool = false,
}

