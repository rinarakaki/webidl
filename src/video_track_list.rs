// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#videotracklist

#[pref = "media.track.enabled",
 exposed = Window]
pub trait VideoTrackList: EventTarget {
  length: u32,
  getter VideoTrack (index: u32);
  #[alias = "getTrackById"]
  pub fn get_track_by_id(id: DOMString) -> Option<VideoTrack>;
  #[alias = "selectedIndex"]
  selected_index: i32,

  #[alias = "onchange"]
  pub onchange: EventHandler,
  #[alias = "onaddtrack"]
  pub onaddtrack: EventHandler,
  #[alias = "onremovetrack"]
  pub on_removetrack: EventHandler,
}

