// This module defines dictonaries that are filled with debug information
// through GetDebugInfo() calls in the media component. To get the information
// filled and returned, we have two methods that return promises, one in
// HTMLMediaElement and one in MediaSource.
//
// If you need to add some extra info, there's one pub struct per class,
// following the pattern <ClassName>DebugInfo, where you can add some fields
// and fill them in the corresponding GetDebugInfo() call.
//
// Below is the structures returned.
//
// Used by HTMLMediaElement.GetMozRequestDebugInfo(), see HTMLMediaElement.webidl:
//
// HTMLMediaElementDebugInfo
//   EMEDebugInfo
//   MediaDecoderDebugInfo
//     MediaFormatReaderDebugInfo
//       MediaStateDebugInfo
//       MediaStateDebugInfo
//       MediaFrameStats
//     MediaDecoderStateMachineDebugInfo
//       MediaDecoderStateMachineDecodingStateDebugInfo
//       MediaSinkDebugInfo
//         VideoSinkDebugInfo
//         AudioSinkDebugInfo
//         DecodedStreamDebugInfo
//           DecodedStreamDataDebugInfo
//     MediaResourceDebugInfo
//       MediaCacheStreamDebugInfo
//
// Used by MediaSource.GetMozDebugReaderData(), see MediaSource.webidl:
//
// MediaSourceDecoderDebugInfo
//  MediaFormatReaderDebugInfo
//    MediaStateDebugInfo
//    MediaStateDebugInfo
//    MediaFrameStats
//  MediaSourceDemuxerDebugInfo
//    TrackBuffersManagerDebugInfo
//    TrackBuffersManagerDebugInfo
pub struct MediaCacheStreamDebugInfo {
  #[alias = "streamLength"]
  pub stream_length: i64 = 0,
  #[alias = "channelOffset"]
  pub channel_offset: i64 = 0,
  #[alias = "cacheSuspended"]
  pub cache_suspended: bool = false,
  #[alias = "channelEnded"]
  pub channel_ended: bool = false,
  pub loadID: i32 = 0,
}

pub struct MediaResourceDebugInfo {
  #[alias = "cacheStream"]
  pub cache_stream: MediaCacheStreamDebugInfo = {},
}

pub struct MediaDecoderDebugInfo {
  pub instance: DOMString = "",
  pub channels: u32 = 0,
  pub rate: u32 = 0,
  #[alias = "hasAudio"]
  pub has_audio: bool = false,
  #[alias = "hasVideo"]
  pub has_video: bool = false,
  pub PlayState: DOMString = "",
  #[alias = "containerType"]
  pub container_type: DOMString = "",
  pub reader: MediaFormatReaderDebugInfo = {},
  #[alias = "stateMachine"]
  pub state_machine: MediaDecoderStateMachineDebugInfo = {},
  pub resource: MediaResourceDebugInfo = {},
}

pub struct AudioSinkDebugInfo {
  #[alias = "startTime"]
  pub start_time: i64 = 0,
  #[alias = "lastGoodPosition"]
  pub last_good_position: i64 = 0,
  #[alias = "isPlaying"]
  pub is_playing: bool = false,
  #[alias = "isStarted"]
  pub is_started: bool = false,
  #[alias = "audioEnded"]
  pub audio_ended: bool = false,
  #[alias = "outputRate"]
  pub output_rate: i32 = 0,
  pub written: i64 = 0,
  #[alias = "hasErrored"]
  pub has_errored: bool = false,
  #[alias = "playbackComplete"]
  pub playback_complete: bool = false,
}

pub struct AudioSinkWrapperDebugInfo {
  #[alias = "isPlaying"]
  pub is_playing: bool = false,
  #[alias = "isStarted"]
  pub is_started: bool = false,
  #[alias = "audioEnded"]
  pub audio_ended: bool = false,
  #[alias = "audioSink"]
  pub audio_sink: AudioSinkDebugInfo = {},
}

pub struct VideoSinkDebugInfo {
  #[alias = "isStarted"]
  pub is_started: bool = false,
  #[alias = "isPlaying"]
  pub is_playing: bool = false,
  pub finished: bool = false,
  pub size: i32 = 0,
  #[alias = "videoFrameEndTime"]
  pub video_frame_end_time: i64 = 0,
  #[alias = "hasVideo"]
  pub has_video: bool = false,
  #[alias = "videoSinkEndRequestExists"]
  pub video_sink_end_request_exists: bool = false,
  #[alias = "endPromiseHolderIsEmpty"]
  pub end_promise_holder_is_empty: bool = false,
}

pub struct DecodedStreamDataDebugInfo {
  pub instance: DOMString = "",
  #[alias = "audioFramesWritten"]
  pub audio_frames_written: i64 = 0,
  #[alias = "streamAudioWritten"]
  pub stream_audio_written: i64 = 0,
  #[alias = "streamVideoWritten"]
  pub stream_video_written: i64 = 0,
  #[alias = "nextAudioTime"]
  pub next_audio_time: i64 = 0,
  #[alias = "lastVideoStartTime"]
  pub last_video_start_time: i64 = 0,
  #[alias = "lastVideoEndTime"]
  pub last_video_end_time: i64 = 0,
  #[alias = "haveSentFinishAudio"]
  pub have_sent_finish_audio: bool = false,
  #[alias = "haveSentFinishVideo"]
  pub have_sent_finish_video: bool = false,
}

pub struct DecodedStreamDebugInfo {
  pub instance: DOMString = "",
  #[alias = "startTime"]
  pub start_time: i64 = 0,
  #[alias = "lastOutputTime"]
  pub last_output_time: i64 = 0,
  pub playing: i32 = 0,
  #[alias = "lastAudio"]
  pub last_audio: i64 = 0,
  #[alias = "audioQueueFinished"]
  pub audio_queue_finished: bool = false,
  #[alias = "audioQueueSize"]
  pub audio_queue_size: i32 = 0,
  pub data: DecodedStreamDataDebugInfo = {},
}

pub struct MediaSinkDebugInfo {
  #[alias = "audioSinkWrapper"]
  pub audio_sink_wrapper: AudioSinkWrapperDebugInfo = {},
  #[alias = "videoSink"]
  pub video_sink: VideoSinkDebugInfo = {},
  #[alias = "decodedStream"]
  pub decoded_stream: DecodedStreamDebugInfo = {},
}

pub struct MediaDecoderStateMachineDecodingStateDebugInfo {
  #[alias = "isPrerolling"]
  pub is_prerolling: bool = false,
}

pub struct MediaDecoderStateMachineDebugInfo {
  pub duration: i64 = 0,
  #[alias = "mediaTime"]
  pub media_time: i64 = 0,
  pub clock: i64 = 0,
  pub state: DOMString = "",
  #[alias = "playState"]
  pub play_state: i32 = 0,
  #[alias = "sentFirstFrameLoadedEvent"]
  pub sent_first_frame_loaded_event: bool = false,
  #[alias = "isPlaying"]
  pub is_playing: bool = false,
  #[alias = "audioRequestStatus"]
  pub audio_request_status: DOMString = "",
  #[alias = "videoRequestStatus"]
  pub video_request_status: DOMString = "",
  #[alias = "decodedAudioEndTime"]
  pub decoded_audio_end_time: i64 = 0,
  #[alias = "decodedVideoEndTime"]
  pub decoded_video_end_time: i64 = 0,
  #[alias = "audioCompleted"]
  pub audio_completed: bool = false,
  #[alias = "videoCompleted"]
  pub video_completed: bool = false,
  #[alias = "stateObj"]
  pub state_obj: MediaDecoderStateMachineDecodingStateDebugInfo = {},
  #[alias = "mediaSink"]
  pub media_sink: MediaSinkDebugInfo = {},
}

pub struct MediaStateDebugInfo {
  #[alias = "needInput"]
  pub need_input: bool = false,
  #[alias = "hasPromise"]
  pub has_promise: bool = false,
  #[alias = "waitingPromise"]
  pub waiting_promise: bool = false,
  #[alias = "hasDemuxRequest"]
  pub has_demux_request: bool = false,
  #[alias = "demuxQueueSize"]
  pub demux_queue_size: i32 = 0,
  #[alias = "hasDecoder"]
  pub has_decoder: bool = false,
  #[alias = "timeTreshold"]
  pub time_treshold: f64 = 0.0,
  #[alias = "timeTresholdHasSeeked"]
  pub time_treshold_has_seeked: bool = false,
  #[alias = "numSamplesInput"]
  pub num_samples_input: i64 = 0,
  #[alias = "numSamplesOutput"]
  pub num_samples_output: i64 = 0,
  #[alias = "queueSize"]
  pub queue_size: i32 = 0,
  pub pending: i32 = 0,
  #[alias = "waitingForData"]
  pub waiting_for_data: bool = false,
  pub demuxEOS: i32 = 0,
  #[alias = "drainState"]
  pub drain_state: i32 = 0,
  #[alias = "waitingForKey"]
  pub waiting_for_key: bool = false,
  #[alias = "lastStreamSourceID"]
  pub last_stream_sourceID: i32 = 0,
}

pub struct MediaFrameStats {
  #[alias = "droppedDecodedFrames"]
  pub dropped_decoded_frames: i64 = 0,
  #[alias = "droppedSinkFrames"]
  pub dropped_sink_frames: i64 = 0,
  #[alias = "droppedCompositorFrames"]
  pub dropped_compositor_frames: i64 = 0,
}

pub struct MediaFormatReaderDebugInfo {
  #[alias = "videoType"]
  pub video_type: DOMString = "",
  #[alias = "videoDecoderName"]
  pub video_decoder_name: DOMString = "",
  #[alias = "videoWidth"]
  pub video_width: i32 = 0,
  #[alias = "videoHeight"]
  pub video_height: i32 = 0,
  #[alias = "videoRate"]
  pub video_rate: f64 = 0.0,
  #[alias = "audioType"]
  pub audio_type: DOMString = "",
  #[alias = "audioDecoderName"]
  pub audio_decoder_name: DOMString = "",
  #[alias = "videoHardwareAccelerated"]
  pub video_hardware_accelerated: bool = false,
  #[alias = "videoNumSamplesOutputTotal"]
  pub video_num_samples_output_total: i64 = 0,
  #[alias = "videoNumSamplesSkippedTotal"]
  pub video_num_samples_skipped_total: i64 = 0,
  #[alias = "audioChannels"]
  pub audio_channels: i32 = 0,
  #[alias = "audioRate"]
  pub audio_rate: f64 = 0.0,
  #[alias = "audioFramesDecoded"]
  pub audio_frames_decoded: i64 = 0,
  #[alias = "audioState"]
  pub audio_state: MediaStateDebugInfo = {},
  #[alias = "videoState"]
  pub video_state: MediaStateDebugInfo = {},
  #[alias = "frameStats"]
  pub frame_stats: MediaFrameStats = {},
}

pub struct BufferRange {
  pub start: f64 = 0,
  pub end: f64 = 0,
}

pub struct TrackBuffersManagerDebugInfo {
  pub type: DOMString = "",
  #[alias = "nextSampleTime"]
  pub next_sample_time: f64 = 0.0,
  #[alias = "numSamples"]
  pub num_samples: i32 = 0,
  #[alias = "bufferSize"]
  pub buffer_size: i32 = 0,
  pub evictable: i32 = 0,
  #[alias = "nextGetSampleIndex"]
  pub next_get_sample_index: i32 = 0,
  #[alias = "nextInsertionIndex"]
  pub next_insertion_index: i32 = 0,
  pub ranges: Vec<BufferRange> = [],
}

pub struct MediaSourceDemuxerDebugInfo {
  #[alias = "audioTrack"]
  pub audio_track: TrackBuffersManagerDebugInfo = {},
  #[alias = "videoTrack"]
  pub video_track: TrackBuffersManagerDebugInfo = {},
}

pub struct MediaSourceDecoderDebugInfo {
  pub reader: MediaFormatReaderDebugInfo = {},
  pub demuxer: MediaSourceDemuxerDebugInfo = {},
}

pub struct EMEDebugInfo {
  #[alias = "keySystem"]
  pub key_system: DOMString = "",
  #[alias = "sessionsInfo"]
  pub sessions_info: DOMString = "",
}

pub struct HTMLMediaElementDebugInfo {
  #[alias = "compositorDroppedFrames"]
  pub compositor_dropped_frames: u32 = 0,
  pub EMEInfo: EMEDebugInfo = {},
  pub decoder: MediaDecoderDebugInfo = {},
}
