// The origin of this IDL file is
// http://www.w3.org/TR/battery-status/

use super::*;

#[chrome_only, exposed = Window]
pub trait BatteryManager: EventTarget {
  pub charging: bool,
  pub unrestricted f64 chargingTime;
  pub unrestricted f64 dischargingTime;
  pub level: f64,

  #[alias = "onchargingchange"]
  pub mut on_charging_change: EventHandler,
  #[alias = "onchargingtimechange"]
  pub mut on_charging_time_change: EventHandler,
  #[alias = "ondischargingtimechange"]
  pub mut on_discharging_time_change: EventHandler,
  #[alias = "onlevelchange"]
  pub mut on_level_change: EventHandler,
}
