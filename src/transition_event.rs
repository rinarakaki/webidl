// Transition events are defined in:
// http://www.w3.org/TR/css3-transitions/#transition-events-
// http://dev.w3.org/csswg/css3-transitions/#transition-events-

#[exposed = Window]
pub trait TransitionEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: TransitionEventInit)
    -> Self;

  #[alias = "propertyName"]
  property_name: DOMString,
  #[alias = "elapsedTime"]
  elapsed_time: f32,
  #[alias = "pseudoElement"]
  pseudo_element: DOMString,
}

pub struct TransitionEventInit: EventInit {
  #[alias = "propertyName"]
  pub property_name: DOMString = "",
  #[alias = "elapsedTime"]
  pub elapsed_time: f32 = 0,
  #[alias = "pseudoElement"]
  pub pseudo_element: DOMString = "",
}
