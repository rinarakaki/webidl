// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-source-element

#[exposed = Window]
pub trait HTMLSourceElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws]
  pub src: DOMString,
  #[ce_reactions, setter_throws]
  pub type: DOMString,
}

partial trait HTMLSourceElement {
  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws]
  pub srcset: DOMString,
  #[ce_reactions, setter_throws]
  pub sizes: DOMString,
  #[ce_reactions, setter_throws]
  pub media: DOMString,
}
