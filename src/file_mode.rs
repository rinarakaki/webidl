pub enum FileMode {
  #[alias = "readonly"]
  ReadOnly,
  #[alias = "readwrite"]
  ReadWrite
}
