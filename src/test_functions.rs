// A dumping ground for random testing functions

callback PromiseReturner = Promise<any>();
callback PromiseReturner2 = Promise<any>(arg: any, arg: any);

#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait WrapperCachedNonISupportsTestInterface {
  #[pref = "dom.webidl.test1"]
  #[alias = "constructor"]
  pub fn new() -> Self;
}

// The type of string C++ sees.
pub enum StringType {
  "literal", // A string with the LITERAL flag.
  "stringbuffer", // A string with the REFCOUNTED flag.
  "inline", // A string with the INLINE flag.
  "other", // Anything else.
}

#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait Testfunctions {
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[throws]
  static void throwUncatchableException();

  // Simply returns its argument. Can be used to test Promise
  // argument processing behavior.
  static Promise<any> passThroughPromise(arg: Promise<any>);

  // Returns whatever Promise the given PromiseReturner returned.
  #[throws]
  static Promise<any> passThroughCallbackPromise(callback: PromiseReturner);

  // Some basic tests for string binding round-tripping behavior.
  #[alias = "setStringData"]
  pub fn set_string_data(arg: DOMString);

  // Get the string data, using an nsAString argument on the C++ side.
  // This will just use Assign/operator = , whatever that does.
  #[alias = "getStringDataAsAString"]
  pub fn get_string_data_as_a_string() -> DOMString;

  // Get the string data, but only "length" chars of it, using an
  // nsAString argument on the C++ side. This will always copy on the
  // C++ side.
  #[alias = "getStringDataAsAString"]
  pub fn get_string_data_as_a_string(length: u32) -> DOMString;

  // Get the string data, but only "length" chars of it, using a
  // DOMString argument on the C++ side and trying to hand it
  // stringbuffers. If length not passed, use our full length.
  #[alias = "getStringDataAsDOMString"]
  pub fn get_string_data_as_dom_string(optional length: u32) -> DOMString;

  // Get a short (short enough to fit in a JS inline string) literal string.
  #[alias = "getShortLiteralString"]
  pub fn get_short_literal_string() -> DOMString;

  // Get a medium (long enough to not be a JS inline, but short enough
  // to fit in a FakeString inline buffer) literal string.
  #[alias = "getMediumLiteralString"]
  pub fn get_medium_literal_string() -> DOMString;

  // Get a long (long enough to not fit in any inline buffers) literal string.
  #[alias = "getLongLiteralString"]
  pub fn get_long_literal_string() -> DOMString;

  // Get a stringbuffer string for whatever string is passed in.
  #[alias = "getStringbufferString"]
  pub fn get_stringbuffer_string(input: DOMString) -> DOMString;

  // Get the type of string that the C++ sees after going through bindings.
  #[alias = "getStringType"]
  pub fn get_string_type(str: DOMString) -> StringType;

  // Returns true if both the incoming string and the stored (via setStringData())
  // string have stringbuffers and they're the same stringbuffer.
  #[alias = "stringbufferMatchesStored"]
  pub fn stringbuffer_matches_stored(str: DOMString) -> bool;

  // functions that just punch through to mozITestInterfaceJS.idl
  #[throws]
  #[alias = "testThrowNsresult"]
  pub fn test_throw_nsresult();
  #[throws]
  #[alias = "testThrowNsresultFromNative"]
  pub fn test_throw_nsresult_from_native();

  // throws an InvalidStateError to auto-create a rejected promise.
  #[throws]
  static Promise<any> throwToRejectPromise();

  // Some attributes for the toJSON to work with.
  #[alias = "one"]
  one: i32,
  #[func = "mozilla::dom::Testfunctions::ObjectFromAboutBlank"]
  two: i32,

  // Testing for how default toJSON behaves.
  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;

  // This returns a wrappercached non-ISupports object. While this will always
  // return the same object, no optimization attributes like #[pure] should be
  // used here because the object should not be held alive from JS by the
  // bindings. This is needed to test wrapper preservation for weak map keys.
  // See bug 1351501.
  #[alias = "wrapperCachedNonISupportsObject"]
  wrapper_cached_non_i_supports_object: WrapperCachedNonISupportsTestInterface,

  attribute #[Clamp] Option<octet> clampedNullableOctet;
  attribute #[EnforceRange] Option<octet> enforcedNullableOctet;

  // Testing for #[AllowShared]
  #[getter_throws]
  #[alias = "arrayBufferView"]
  pub array_buffer_view: ArrayBufferView,
  #[getter_throws]
  attribute #[AllowShared] ArrayBufferView allowSharedArrayBufferView;
  #[Cached, pure, getter_throws]
  attribute Vec<ArrayBufferView> sequenceOfArrayBufferView;
  #[Cached, pure, getter_throws]
  attribute Vec<#[AllowShared] ArrayBufferView> sequenceOfAllowSharedArrayBufferView;
  #[getter_throws]
  #[alias = "arrayBuffer"]
  pub array_buffer: ArrayBuffer,
  #[getter_throws]
  attribute #[AllowShared] ArrayBuffer allowSharedArrayBuffer;
  #[Cached, pure, getter_throws]
  attribute Vec<ArrayBuffer> sequenceOfArrayBuffer;
  #[Cached, pure, getter_throws]
  attribute Vec<#[AllowShared] ArrayBuffer> sequenceOfAllowSharedArrayBuffer;
  #[alias = "testNotAllowShared"]
  pub fn test_not_allow_shared(buffer: ArrayBufferView);
  #[alias = "testNotAllowShared"]
  pub fn test_not_allow_shared(buffer: ArrayBuffer);
  #[alias = "testNotAllowShared"]
  pub fn test_not_allow_shared(buffer: DOMString);
  #[alias = "testAllowShared"] fn test_allow_shared(#[AllowShared]
  buffer: ArrayBufferView);
  #[alias = "testAllowShared"] fn test_allow_shared(#[AllowShared]
  buffer: ArrayBuffer);
  #[alias = "testDictWithAllowShared"]
  pub fn test_dict_with_allow_shared(#[optional = {}] buffer: DictWithAllowSharedBufferSource);
  #[alias = "testUnionOfBuffferSource"]
  pub fn test_union_of_bufffer_source((ArrayBuffer or ArrayBufferView or DOMString) foo);
  #[alias = "testUnionOfAllowSharedBuffferSource"] fn test_union_of_allow_shared_bufffer_source((#[AllowShared] ArrayBuffer or [AllowShared]
  ArrayBufferView) foo);
}

pub struct DictWithAllowSharedBufferSource {
  #[alias = "arrayBuffer"]
  pub array_buffer: ArrayBuffer,
  #[alias = "arrayBufferView"]
  pub array_buffer_view: ArrayBufferView,
  #[AllowShared]
  #[alias = "allowSharedArrayBuffer"]
  pub allow_shared_arrayBuffer: ArrayBuffer,
  #[AllowShared]
  #[alias = "allowSharedArrayBufferView"]
  pub allow_shared_arrayBufferView: ArrayBufferView,
}
