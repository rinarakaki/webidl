#[LegacyUnenumerableNamedProperties,
 exposed = Window]
pub trait Plugin {
  description: DOMString,
  filename: DOMString,
  version: DOMString,
  name: DOMString,

  length: u32,
  getter Option<MimeType> item(index: u32);
  getter Option<MimeType> namedItem(name: DOMString);
}
