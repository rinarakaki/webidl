#ifdef MOZ_DOM_STREAMS
pub struct FileSystemCreateWritableOptions {
  #[alias = "keepExistingData"]
  pub keep_existing_data: bool = false,
}
#endif

// TODO: Add Serializable
#[exposed = (Window, Worker), SecureContext, pref = "dom.fs.enabled"]
pub trait FileSystemFileHandle: FileSystemHandle {
  #[alias = "getFile"]
  pub fn get_file() -> Promise<File>;
#ifdef MOZ_DOM_STREAMS
  #[alias = "createWritable"]
  pub fn create_writable(#[optional = {}] options: FileSystemCreateWritableOptions)
    -> Promise<FileSystemWritableFileStream>;
#endif

  #[exposed = DedicatedWorker]
  #[alias = "createSyncAccessHandle"]
  pub fn create_sync_access_handle() -> Promise<FileSystemSyncAccessHandle>;
}
