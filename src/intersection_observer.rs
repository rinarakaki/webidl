// The origin of this IDL file is
// https://w3c.github.io/IntersectionObserver/

#[ProbablyShortLivingWrapper, pref = "dom.IntersectionObserver.enabled",
 exposed = Window]
pub trait IntersectionObserverEntry {
  #[constant]
  time: DOMHighResTimeStamp,
  #[constant]
  #[alias = "rootBounds"]
  root_bounds: Option<DOMRectReadOnly>,
  #[constant]
  #[alias = "boundingClientRect"]
  bounding_client_rect: DOMRectReadOnly,
  #[constant]
  #[alias = "intersectionRect"]
  intersection_rect: DOMRectReadOnly,
  #[constant]
  #[alias = "isIntersecting"]
  is_intersecting: bool,
  #[constant]
  #[alias = "intersectionRatio"]
  intersection_ratio: f64,
  #[constant]
  target: Element,
}

#[pref = "dom.IntersectionObserver.enabled",
 exposed = Window]
pub trait IntersectionObserver {
  #[throws]
  constructor(intersectionCallback: IntersectionCallback,
  #[optional = {}] options: IntersectionObserverInit);

  #[constant]
  root: Option<Node>,
  #[constant]
  #[alias = "rootMargin"]
  root_margin: UTF8String,
  #[constant,Cached]
  thresholds: Vec<f64>,
  pub fn observe(target: Element);
  pub fn unobserve(target: Element);
  pub fn disconnect();
  #[alias = "takeRecords"]
  pub fn take_records() -> Vec<IntersectionObserverEntry>;
}

callback IntersectionCallback =
  void (entries: Vec<IntersectionObserverEntry>, entries: Vec<IntersectionObserverEntry>);

pub struct IntersectionObserverEntryInit {
  required DOMHighResTimeStamp time;
  required DOMRectInit rootBounds;
  required DOMRectInit boundingClientRect;
  required DOMRectInit intersectionRect;
  required Element target;
}

pub struct IntersectionObserverInit {
  (Element or Document)? root = None;
  #[alias = "rootMargin"]
  pub root_margin: UTF8String = "0px",
  (f64 or Vec<f64>) threshold = 0;
}
