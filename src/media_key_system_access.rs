// The origin of this IDL file is
// https://dvcs.w3.org/hg/html-media/raw-file/default/encrypted-media/encrypted-media.html

pub enum MediaKeysRequirement {
  #[alias = "required"]
  Required,
  #[alias = "optional"]
  Optional,
  #[alias = "not-allowed"]
  NotAllowed
}

pub struct MediaKeySystemMediaCapability {
  #[alias = "contentType"]
  pub content_type: DOMString = "",
  pub robustness: DOMString = "",
  #[pref = "media.eme.encrypted-media-encryption-scheme.enabled"]
  #[alias = "encryptionScheme"]
  pub encryption_scheme: Option<DOMString> = None,
}

pub struct MediaKeySystemConfiguration {
  pub label: DOMString = "",
  #[alias = "initDataTypes"]
  pub init_data_types: Vec<DOMString> = [],
  #[alias = "audioCapabilities"]
  pub audio_capabilities: Vec<MediaKeySystemMediaCapability> = [],
  #[alias = "videoCapabilities"]
  pub video_capabilities: Vec<MediaKeySystemMediaCapability> = [],
  #[alias = "distinctiveIdentifier"]
  pub distinctive_identifier: MediaKeysRequirement = "optional",
  #[alias = "persistentState"]
  pub persistent_state: MediaKeysRequirement = "optional",
  #[alias = "sessionTypes"]
  pub session_types: Vec<DOMString>,
}

#[exposed = Window]
pub trait MediaKeySystemAccess {
  #[alias = "keySystem"]
  key_system: DOMString,

  #[new_object]
  #[alias = "getConfiguration"]
  pub fn get_configuration() -> MediaKeySystemConfiguration;

  #[new_object]
  #[alias = "createMediaKeys"]
  pub fn create_media_keys() -> Promise<MediaKeys>;
}
