// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-meter-element	

// http://www.whatwg.org/specs/web-apps/current-work/#the-meter-element
#[exposed = Window]
pub trait HTMLMeterElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub value: f64,
  #[ce_reactions, setter_throws]
  pub min: f64,
  #[ce_reactions, setter_throws]
  pub max: f64,
  #[ce_reactions, setter_throws]
  pub low: f64,
  #[ce_reactions, setter_throws]
  pub high: f64,
  #[ce_reactions, setter_throws]
  pub optimum: f64,
  labels: NodeList,
}
