#[LegacyUnenumerableNamedProperties,
 exposed = Window]
pub trait PluginArray {
  #[needs_caller_type]
  length: u32,

  #[needs_caller_type]
  getter Option<Plugin> item(index: u32);
  #[needs_caller_type]
  getter Option<Plugin> namedItem(name: DOMString);

  pub fn refresh(#[optional = false] reloadDocuments: bool);
}
