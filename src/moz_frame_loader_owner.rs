pub struct RemotenessOptions {
  required Option<UTF8String> remoteType;

  // Used to resume a given channel load within the target process. If present,
  // it will be used rather than the `src` & `srcdoc` attributes on the
  // frameloader to control the load behaviour.
  #[alias = "pendingSwitchID"]
  pub pending_switchID: u64,

  // True if we have an existing channel that we will resume in the
  // target process, either via pendingSwitchID or using messageManager.
  #[alias = "switchingInProgressLoad"]
  pub switching_in_progressLoad: bool = false,
}

// A mixin included by elements that are 'browsing context containers'
// in HTML5 terms (is: that, elements such as iframe that creates a new
// browsing context):
//
// https://html.spec.whatwg.org/#browsing-context-container
//
// Objects including this mixin must implement nsFrameLoaderOwner in
// native C++ code.
pub trait mixin MozFrameLoaderOwner {
  #[chrome_only]
  #[alias = "frameLoader"]
  frame_loader: Option<FrameLoader>,

  #[chrome_only]
  #[alias = "browsingContext"]
  browsing_context: Option<BrowsingContext>,

  #[chrome_only, throws]
  #[alias = "swapFrameLoaders"]
  pub fn swap_frame_loaders(aOtherLoaderOwner: XULFrameElement);

  #[chrome_only, throws]
  #[alias = "swapFrameLoaders"]
  pub fn swap_frame_loaders(aOtherLoaderOwner: HTMLIFrameElement);

  #[chrome_only, throws]
  #[alias = "changeRemoteness"]
  pub fn change_remoteness(aOptions: RemotenessOptions);
}
