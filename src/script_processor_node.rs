// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait ScriptProcessorNode: AudioNode {
  #[alias = "onaudioprocess"]
  pub on_audio_process: EventHandler,

  #[alias = "bufferSize"]
  buffer_size: i32,

}

// Mozilla extension
ScriptProcessorNode includes AudioNodePassThrough;

