// The origin of this IDL file is
// https://w3c.github.io/navigation-timing/#the-performancenavigation-trait

#[exposed = Window]
pub trait PerformanceNavigation {
  const u16 TYPE_NAVIGATE = 0;
  const u16 TYPE_RELOAD = 1;
  const u16 TYPE_BACK_FORWARD = 2;
  const u16 TYPE_RESERVED = 255;

  type: u16,
  #[alias = "redirectCount"]
  redirect_count: u16,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
