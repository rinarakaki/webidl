// The origin of this IDL file is
// https://w3c.github.io/clipboard-apis/

use super::*;

pub type ClipboardItems = Vec<ClipboardItem>;

#[SecureContext, exposed = Window, pref = "dom.events.asyncClipboard"]
pub trait Clipboard: EventTarget {
  #[pref = "dom.events.asyncClipboard.read", needs_subject_principal]
  pub fn read(&self) -> Result<Promise<ClipboardItems>>;
  
  #[func = "Clipboard::ReadTextEnabled", needs_subject_principal]
  #[alias = "readText"]
  pub fn read_text(&self) -> Result<Promise<DOMString>>;

  #[pref = "dom.events.asyncClipboard.clipboardItem",
    needs_subject_principal]
  pub fn write(&self, data: ClipboardItems) -> Result<Promise<void>>;

  #[throws, needs_subject_principal]
  #[alias = "writeText"]
  pub fn write_text(&self, data: DOMString) -> Promise<void>;
}

pub type ClipboardItemDataType = (DOMString or Blob);
// pub type ClipboardItemData = Promise<ClipboardItemDataType>;
// callback ClipboardItemDelayedCallback = ClipboardItemData ();

#[SecureContext, exposed = Window,
  pref = "dom.events.asyncClipboard.clipboardItem"]
pub trait ClipboardItem {
  // Note: The spec uses Promise<ClipboardItemDataType>.
  #[alias = "constructor"]
  pub fn new(
    items: record<DOMString, ClipboardItemDataType>,  // TODO
    #[optional = {}] options: ClipboardItemOptions) -> Result<Self>;

  // #[alias = "createDelayed"]
  // pub fn create_delayed(
  //   items: record<DOMString, ClipboardItemDelayedCallback>,
  //   #[optional = {}] options: ClipboardItemOptions) -> Self;

  #[alias = "presentationStyle"]
  pub presentation_style: PresentationStyle,
  // pub lastModified: i64,
  // pub delayed bool,

  // TODO: Use FrozenArray once available. (1236777: Bug)
  // pub types: FrozenArray<DOMString>,
  #[Frozen, Cached, pure]
  pub types: Vec<DOMString>,

  #[alias = "getType"]
  pub fn get_type(&self, type: DOMString) -> Result<Promise<Blob>>;
}

pub enum PresentationStyle {
  #[alias = "unspecified"]
  Unspecified,
  #[alias = "inline"]
  Inline,
  #[alias = "attachment"]
  Attachment
}

pub struct ClipboardItemOptions {
  #[alias = "presentationStyle"]
  pub #[optional = Unspecified] presentation_style: PresentationStyle
}
