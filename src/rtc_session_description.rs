// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/webrtc.html#idl-def-RTCSessionDescription

#[alias = "RTCSdpType"]
pub enum RTCSDPType {
  #[alias = "offer"]
  Offer,
  #[alias = "pranswer"]
  PrAnswer,
  #[alias = "answer"]
  Answer,
  #[alias = "rollback"]
  Rollback
}

pub struct RTCSessionDescriptionInit {
  pub type: RTCSDPType,
  pub sdp: DOMString = "",
}

#[pref = "media.peerconnection.enabled",
 JSImplementation = "@mozilla.org/dom/rtcsessiondescription;1",
 exposed = Window]
pub trait RTCSessionDescription {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] descriptionInitDict: RTCSessionDescriptionInit)
    -> Result<Self, Error>;

  // These should be readonly, but writing causes deprecation warnings for a bit
  pub type: RTCSDPType,
  pub sdp: DOMString,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
