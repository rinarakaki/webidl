// The origin of this IDL file is
// https://webaudio.github.io/web-midi-api/

#[SecureContext, pref = "dom.webmidi.enabled",
 exposed = Window]
pub trait MIDIInput: MIDIPort {
  #[alias = "onmidimessage"]
  pub on_midi_message: EventHandler,
}

