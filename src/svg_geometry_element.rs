// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGGeometryElement: SVGGraphicsElement {
  #[same_object]
  #[alias = "pathLength"]
  path_length: SVGAnimatedNumber,

  #[alias = "isPointInFill"]
  pub fn is_point_in_fill(#[optional = {}] point: DOMPointInit) -> bool;
  #[alias = "isPointInStroke"]
  pub fn is_point_in_stroke(#[optional = {}] point: DOMPointInit) -> bool;

  #[binary_name = "getTotalLengthForBinding"]
  #[alias = "getTotalLength"]
  pub fn get_total_length() -> f32;
  #[new_object, throws]
  #[alias = "getPointAtLength"]
  pub fn get_point_at_length(distance: f32) -> SVGPoint;
}
