// For more information on this trait, please see
// https://w3c.github.io/webappsec-feature-policy/#idl-index

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait FeaturePolicy {
  #[alias = "allowsFeature"]
  pub fn allows_feature(feature: DOMString, optional origin: DOMString) -> bool;
  pub fn features() -> Vec<DOMString>;
  #[alias = "allowedFeatures"]
  pub fn allowed_features() -> Vec<DOMString>;
  #[alias = "getAllowlistForFeature"]
  pub fn get_allowlist_for_feature(feature: DOMString) -> Vec<DOMString>;
}

#[pref = "dom.reporting.featurePolicy.enabled",
 exposed = Window]
pub trait FeaturePolicyViolationReportBody: ReportBody {
  #[alias = "featureId"]
  feature_id: DOMString,
  #[alias = "sourceFile"]
  source_file: Option<DOMString>,
  #[alias = "lineNumber"]
  line_number: Option<i32>,
  #[alias = "columnNumber"]
  column_number: Option<i32>,
  disposition: DOMString,
}
