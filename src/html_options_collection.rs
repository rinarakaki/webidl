// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-html5-20120329/

#[exposed = Window]
pub trait HTMLOptionsCollection: HTMLCollection {
  #[ce_reactions, setter_throws]
  pub length: u32,
  #[ce_reactions, throws]
  setter void (index: u32, index: u32);
  #[ce_reactions, throws]
  pub fn add((HTMLOptionElement or HTMLOptGroupElement) element, optional (HTMLElement or i32)? before = None);
  #[ce_reactions]
  pub fn remove(index: i32);
  #[alias = "selectedIndex"]
  pub selected_index: i32,
}
