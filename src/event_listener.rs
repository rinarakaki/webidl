// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

use super::Event;

#[exposed = Window]
callback trait EventListener {
  #[alias = "handleEvent"]
  pub fn handle_event(event: Event);
}
