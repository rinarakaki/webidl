// The origin of this IDL file is
// https://immersive-web.github.io/webxr/#xrinputsourceevent-trait

use super::{Event, XRFrame, XRInputSource};

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRInputSourceEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;
  #[same_object]
  frame: XRFrame,
  #[same_object]
  #[alias = "inputSource"]
  input_source: XRInputSource,
}

pub struct XRInputSourceEventInit: EventInit {
  required XRFrame frame;
  required XRInputSource inputSource;
}
