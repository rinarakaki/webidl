#[exposed = Window]
pub trait XPathExpression {
  // The result specifies a specific result object which may be reused and
  // returned by this method. If this is specified as None or it's not an
  // XPathResult object, a new result object will be constructed and returned.
  #[throws]
  XPathResult evaluate(
    contextNode: Node,
    #[optional = 0 // XPathResult.ANY_TYPE */] type: u16,
    optional Option<object> result = None);

  // The result specifies a specific result object which may be reused and
  // returned by this method. If this is specified as None or it's not an
  // XPathResult object, a new result object will be constructed and returned.
  #[throws, chrome_only]
  XPathResult evaluateWithContext(
    contextNode: Node,
    u32 contextPosition,
    u32 contextSize,
    #[optional = 0 // XPathResult.ANY_TYPE */] type: u16,
    optional Option<object> result = None);
}
