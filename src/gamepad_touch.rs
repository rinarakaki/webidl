// The origin of this IDL file is
// https://github.com/knyg/gamepad/blob/multitouch/extensions.html

#[SecureContext, pref = "dom.gamepad.extensions.multitouch",
 exposed = Window]
pub trait GamepadTouch {
  touchId: u32,
  #[alias = "surfaceId"]
  surface_id: octet,
  #[constant, throws]
  position: F32Array,
  #[constant, throws]
  #[alias = "surfaceDimensions"]
  surface_dimensions: Option<Uint32Array>,
}
