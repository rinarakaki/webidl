// The origin of this IDL file is
// https://w3c.github.io/permissions/#permissions-trait

pub enum PermissionName {
  #[alias = "geolocation"]
  Geolocation,
  #[alias = "notifications"]
  Notifications,
  #[alias = "push"]
  Push,
  #[alias = "persistent-storage"]
  PersistentStorage
  // Unsupported: "midi"
}

#[GenerateInit]
pub struct PermissionDescriptor {
  required PermissionName name;
}

// We don't implement `PushPermissionDescriptor` because we use a background
// message quota instead of `userVisibleOnly`.

#[exposed = Window]
pub trait Permissions {
  #[throws]
  pub fn query(permission: object) -> Promise<PermissionStatus>;

  #[throws, pref = "dom.permissions.revoke.enable"]
  pub fn revoke(permission: object) -> Promise<PermissionStatus>;
}
