// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#texttracklist

#[exposed = Window]
pub trait TextTrackList: EventTarget {
  length: u32,
  getter TextTrack (index: u32);
  #[alias = "getTrackById"]
  pub fn get_track_by_id(id: DOMString) -> Option<TextTrack>;

  #[alias = "onchange"]
  pub onchange: EventHandler,
  #[alias = "onaddtrack"]
  pub onaddtrack: EventHandler,
  #[alias = "onremovetrack"]
  pub on_removetrack: EventHandler,
}

// Mozilla extensions
partial trait TextTrackList {
  #[chrome_only]
  #[alias = "mediaElement"]
  media_element: Option<HTMLMediaElement>,
}
