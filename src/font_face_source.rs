// The origin of this IDL file is
// http://dev.w3.org/csswg/css-font-loading/#font-face-source

pub trait mixin FontFaceSource {

  #[pref = "layout.css.font-loading-api.enabled"]
  fonts: FontFaceSet,
}
