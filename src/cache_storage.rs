// The origin of this IDL file is
// http://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html

use super::*;

// https://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html#cache-storage

pub trait Principal;

#[exposed = (Window, Worker), pref = "dom.caches.enabled"]
pub trait CacheStorage {
  #[chrome_only]
  #[alias = "constructor"]
  pub fn new(
    namespace: CacheStorageNamespace,
    principal: Principal)
    -> Result<Self>;

  #[new_object]
  pub fn match(
    &self,
    request: RequestInfo,
    #[optional = {}] options: MultiCacheQueryOptions)
    -> Promise<Response>;

  #[new_object]
  pub fn has(&self, cache_name: DOMString) -> Promise<bool>;

  #[new_object]
  pub fn open(&self, cache_name: DOMString) -> Promise<Cache>;

  #[new_object]
  pub fn delete(&self, cache_name: DOMString) -> Promise<bool>;

  #[new_object]
  pub fn keys(&self) -> Promise<Vec<DOMString>>;
}

pub struct MultiCacheQueryOptions: CacheQueryOptions {
  #[alias = "cacheName"]
  pub #[optional] cache_name: DOMString,
}

// chrome-only, gecko specific extension
pub enum CacheStorageNamespace {
  #[alias = "content"]
  Content,
  #[alias = "chrome"]
  Chrome
}
