// The origin of this IDL file is
// https://www.w3.org/TR/webrtc/#rtcdtmftonechangeevent

#[exposed = Window]
pub trait RTCDTMFToneChangeEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: RTCDTMFToneChangeEventInit);

  tone: DOMString,
}

pub struct RTCDTMFToneChangeEventInit: EventInit {
  pub tone: DOMString = "",
}
