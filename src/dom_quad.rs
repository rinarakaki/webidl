// The origin of this IDL file is
// https://drafts.fxtf.org/geometry/

#[exposed = (Window, Worker), Serializable]
pub trait DOMQuad {
  constructor(
    #[optional = {}] p1: DOMPointInit, #[optional = {}] p2: DOMPointInit,
    #[optional = {}] p3: DOMPointInit, #[optional = {}] p4: DOMPointInit);

  #[alias = "constructor"]
  pub fn new(rect: DOMRectReadOnly) -> Self;

  #[new_object]
  static DOMQuad fromRect(#[optional = {}] other: DOMRectInit);
  #[new_object]
  static DOMQuad fromQuad(#[optional = {}] other: DOMQuadInit);

  #[same_object]
  p1: DOMPoint,
  #[same_object]
  p2: DOMPoint,
  #[same_object]
  p3: DOMPoint,
  #[same_object]
  p4: DOMPoint,
  #[new_object]
  #[alias = "getBounds"]
  pub fn get_bounds() -> DOMRectReadOnly;

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}

pub struct DOMQuadInit {
  pub p1: DOMPointInit = {},
  pub p2: DOMPointInit = {},
  pub p3: DOMPointInit = {},
  pub p4: DOMPointInit = {},
}
