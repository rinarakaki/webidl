// This file declares the AnonymousContent trait which is used to
// manipulate content that has been inserted into the document's canvasFrame
// anonymous container.
// See Document.insertAnonymousContent.
//
// This API never returns a reference to the actual inserted DOM node on
// purpose. This is to make sure the content cannot be randomly changed and the
// DOM cannot be traversed from the node, so that Gecko can remain in control of
// the inserted content.

pub super::*;

#[chrome_only, exposed = Window]
pub trait AnonymousContent {
  // Get the text content of an element inside this custom anonymous content.
  #[alias = "getTextContentForElement"]
  pub fn get_text_content_for_element(&self, element_id: DOMString)
    -> Result<DOMString>;

  // Set the text content of an element inside this custom anonymous content.
  #[alias = "setTextContentForElement"]
  pub fn set_text_content_for_element(
    &self, element_id: DOMString, text: DOMString)
    -> Result<()>;

  // Get the value of an attribute of an element inside this custom anonymous
  // content.
  #[alias = "getAttributeForElement"]
  pub fn get_attribute_for_element(
    &self, element_id: DOMString, attribute_name: DOMString)
    -> Result<Option<DOMString>>;

  // Set the value of an attribute of an element inside this custom anonymous
  // content.
  #[needs_subject_principal = NonSystem]
  #[alias = "setAttributeForElement"]
  pub fn set_attribute_for_element(
    &self, element_id: DOMString, attribute_name: DOMString, value: DOMString)
    -> Result<()>;

  // Remove an attribute from an element inside this custom anonymous content.
  #[alias = "removeAttributeForElement"]
  pub fn remove_attribute_for_element(
    &self, element_id: DOMString, attribute_name: DOMString)
    -> Result<()>;

  // Get the canvas' context for the element specified if it's a <canvas>
  // node, `None` otherwise.
  #[alias = "getCanvasContext"]
  pub fn get_canvas_context(&self, element_id: DOMString, context_id: DOMString)
    -> Result<Option<nsISupports>>;

  #[alias = "setAnimationForElement"]
  pub fn set_animation_for_element(
    &self, element_id: DOMString,
    keyframes: Option<object>,
    #[optional = {}] options: UnrestrictedDoubleOrKeyframeAnimationOptions)
    -> Result<Animation>;

  // Accepts a list of (overlapping: possibly) DOMRects which describe a shape
  // in CSS pixels relative to the element's border box. This shape will be
  // excluded from the element's background color rendering. The element will
  // not render any background images once this method has been called.
  #[alias = "setCutoutRectsForElement"]
  pub fn set_cutout_rects_for_element(
    &self, element_id: DOMString, rects: Vec<DOMRect>) -> Result<()>;

  // Get the computed value of a property on an element inside this custom
  // anonymous content.
  #[alias = "getComputedStylePropertyValue"]
  pub fn get_computed_style_property_value(
    &self, element_id: DOMString, property_name: UTF8String)
    -> Result<Option<UTF8String>>;

  // If event's original target is in the anonymous content, this returns the id
  // attribute value of the target.
  #[alias = "getTargetIdForEvent"]
  pub fn get_target_id_for_event(&self, event: Event) -> Option<DOMString>;

  // Set given style to this AnonymousContent.
  #[alias = "setStyle"]
  pub fn set_style(&self, property: UTF8String, property: UTF8String) -> Result<()>;
}
