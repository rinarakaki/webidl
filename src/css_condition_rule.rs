// The origin of this IDL file is
// https://drafts.csswg.org/css-conditional/#the-cssconditionrule-trait

use super::CSSGroupingRule;

#[exposed = Window]
pub trait CSSConditionRule: CSSGroupingRule {
  #[setter_throws]
  #[alias = "conditionText"]
  pub condition_text: UTF8String,
}
