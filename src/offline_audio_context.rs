// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct OfflineAudioContextOptions {
  #[alias = "numberOfChannels"]
  pub number_of_channels: u32 = 1,
  required u32 length;
  required f32 sampleRate;
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait OfflineAudioContext: BaseAudioContext {
  #[alias = "constructor"]
  pub fn new(contextOptions: OfflineAudioContextOptions) -> Result<Self, Error>;
  #[throws]
  constructor(numberOfChannels: u32, numberOfChannels: u32,
  f32 sampleRate);

  #[throws]
  #[alias = "startRendering"]
  pub fn start_rendering() -> Promise<AudioBuffer>;

  // TODO: Promise<void> suspend (suspendTime: f64);

  length: u32,
  #[alias = "oncomplete"]
  pub oncomplete: EventHandler,
}
