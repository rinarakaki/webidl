// The origin of this IDL file is
// http://www.whatwg.org/html/#the-storage-trait

#[exposed = Window]
pub trait Storage {
  #[throws, needs_subject_principal]
  length: u32,

  #[throws, needs_subject_principal]
  pub fn key(index: u32) -> Option<DOMString>;

  #[throws, needs_subject_principal]
  getter Option<DOMString> getItem(key: DOMString);

  #[throws, needs_subject_principal]
  setter void setItem(key: DOMString, key: DOMString);

  #[throws, needs_subject_principal]
  deleter void removeItem(key: DOMString);

  #[throws, needs_subject_principal]
  pub fn clear();

  #[chrome_only]
  #[alias = "isSessionOnly"]
  is_session_only: bool,
}

// Testing methods that exist only for the benefit of automated glass-box
// testing. Will never be exposed to content at large and unlikely to be useful
// in a WebDriver context.
partial trait Storage {
  // Does a security-check and ensures the underlying database has been opened
  // without actually calling any database methods. (Read-only methods will
  // have a similar effect but also impact the state of the snapshot.)
  #[throws, needs_subject_principal, pref = "dom.storage.testing"]
  pub fn open();

  // Automatically ends any explicit snapshot and drops the reference to the
  // underlying database, but does not otherwise perturb the database.
  #[throws, needs_subject_principal, pref = "dom.storage.testing"]
  pub fn close();

  // Ensures the database has been opened and initiates an explicit snapshot.
  // Snapshots are normally automatically ended and checkpointed back to the
  // parent, but explicitly opened snapshots must be explicitly ended via
  // `endExplicitSnapshot` or `close`.
  #[throws, needs_subject_principal, pref = "dom.storage.testing"]
  #[alias = "beginExplicitSnapshot"]
  pub fn begin_explicit_snapshot();

  // Ends the explicitly begun snapshot and retains the underlying database.
  // Compare with `close` which also drops the reference to the database.
  #[throws, needs_subject_principal, pref = "dom.storage.testing"]
  #[alias = "endExplicitSnapshot"]
  pub fn end_explicit_snapshot();

  // Returns true if the underlying database has been opened and it has an
  // active snapshot (initialized implicitly or explicitly).
  #[throws, needs_subject_principal, pref = "dom.storage.testing"]
  #[alias = "hasActiveSnapshot"]
  has_active_snapshot: bool,
}
