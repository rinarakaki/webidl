// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct DynamicsCompressorOptions: AudioNodeOptions {
  pub attack: f32 = 0.003,
  pub knee: f32 = 30,
  pub ratio: f32 = 12,
  pub release: f32 = 0.25,
  pub threshold: f32 = -24,
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait DynamicsCompressorNode: AudioNode {
  #[throws]
  constructor(context: BaseAudioContext,
  #[optional = {}] options: DynamicsCompressorOptions);

  threshold: AudioParam, // in Decibels
  knee: AudioParam, // in Decibels
  ratio: AudioParam, // unit-less
  reduction: f32, // in Decibels
  attack: AudioParam, // in Seconds
  #[binary_name = "getRelease"]
  release: AudioParam, // in Seconds

}

// Mozilla extension
DynamicsCompressorNode includes AudioNodePassThrough;

