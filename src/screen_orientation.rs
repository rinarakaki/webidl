// The origin of this IDL file is
// https://w3c.github.io/screen-orientation

pub enum OrientationType {
  #[alias = "portrait-primary"]
  PortraitPrimary,
  #[alias = "portrait-secondary"]
  PortraitSecondary,
  #[alias = "landscape-primary"]
  LandscapePrimary,
  #[alias = "landscape-secondary"]
  LandscapeSecondary
}

pub enum OrientationLockType {
  #[alias = "any"]
  Any,
  #[alias = "natural"]
  Natural,
  #[alias = "landscape"]
  Landscape,
  #[alias = "portrait"]
  Portrait,
  #[alias = "portrait-primary"]
  PortraitPrimary,
  #[alias = "portrait-secondary"]
  PortraitSecondary,
  #[alias = "landscape-primary"]
  LandscapePrimary,
  #[alias = "landscape-secondary"]
  LandscapeSecondary
}

#[exposed = Window]
pub trait ScreenOrientation: EventTarget {
  #[throws]
  pub fn lock(orientation: OrientationLockType) -> Promise<void>;
  #[throws]
  pub fn unlock();
  #[throws, needs_caller_type]
  type: OrientationType,
  #[throws, needs_caller_type]
  angle: u16,
  #[alias = "onchange"]
  pub on_change: EventHandler,
}
