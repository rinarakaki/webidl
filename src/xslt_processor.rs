pub trait nsIVariant;

#[exposed = Window]
pub trait XSLTProcessor {
  #[alias = "constructor"]
  pub fn new() -> Self;

  // Import the stylesheet into this XSLTProcessor for transformations.
  //
  // @param style The root-node of a XSLT stylesheet. This can be either
  //              a document node or an element node. If a document node
  //              then the document can contain either a XSLT stylesheet
  //              or a LRE stylesheet.
  //              If the argument is an element node it must be the
  //              xsl:stylesheet (or xsl:transform) element of an XSLT
  //              stylesheet.
  #[throws]
  #[alias = "importStylesheet"]
  pub fn import_stylesheet(style: Node);

  // Transforms the node source applying the stylesheet given by
  // the importStylesheet() function. The owner document of the output node
  // owns the returned document fragment.
  //
  // @param source The node to be transformed
  // @param output This document is used to generate the output
  // @return DocumentFragment The result of the transformation
  #[ce_reactions, throws]
  DocumentFragment transformToFragment(source: Node,
  output: Document);

  // Transforms the node source applying the stylesheet given by the
  // importStylesheet() function.
  //
  // @param source The node to be transformed
  // @return Document The result of the transformation
  #[ce_reactions, throws]
  #[alias = "transformToDocument"]
  pub fn transform_to_document(source: Node) -> Document;

  // Sets a parameter to be used in subsequent transformations with this
  // XSLTProcessor. If the parameter doesn't exist in the stylesheet the
  // parameter will be ignored.
  //
  // @param namespaceURI The namespaceURI of the XSLT parameter
  // @param localName The local name of the XSLT parameter
  // @param value The new value of the XSLT parameter
  #[throws]
  void setParameter(#[LegacyNullToEmptyString] DOMString namespaceURI,
  DOMString localName,
  value: any);

  // Gets a parameter if previously set by setParameter. Returns None
  // otherwise.
  //
  // @param namespaceURI The namespaceURI of the XSLT parameter
  // @param localName The local name of the XSLT parameter
  // @return nsIVariant The value of the XSLT parameter
  #[throws]
  Option<nsIVariant> getParameter(#[LegacyNullToEmptyString] DOMString namespaceURI,
  DOMString localName);
  // Removes a parameter, if set. This will make the processor use the
  // default-value for the parameter as specified in the stylesheet.
  //
  // @param namespaceURI The namespaceURI of the XSLT parameter
  // @param localName The local name of the XSLT parameter
  #[throws]
  void removeParameter(#[LegacyNullToEmptyString] DOMString namespaceURI,
  DOMString localName);

  // Removes all set parameters from this XSLTProcessor. This will make
  // the processor use the default-value for all parameters as specified in
  // the stylesheet.
  #[alias = "clearParameters"]
  pub fn clear_parameters();

  // Remove all parameters and stylesheets from this XSLTProcessor.
  pub fn reset();

  // Disables all loading of external documents, such as from
  // <xsl:import> and document()
  // Defaults to off and is *not* reset by calls to reset()
  #[chrome_only]
  const u32 DISABLE_ALL_LOADS = 1;

  // Flags for this processor. Defaults to 0. See individual flags above
  // for documentation for effect of reset()
  #[chrome_only, needs_caller_type]
  pub flags: u32,
}
