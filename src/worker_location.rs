// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/workers.html#worker-locations

#[exposed = Worker]
pub trait WorkerLocation {
  stringifier readonly href: USVString,
  origin: USVString,
  protocol: USVString,
  host: USVString,
  hostname: USVString,
  port: USVString,
  pathname: USVString,
  search: USVString,
  hash: USVString,
}
