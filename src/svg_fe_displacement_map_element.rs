// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEDisplacementMapElement: SVGElement {

  // Channel Selectors
  const u16 SVG_CHANNEL_UNKNOWN = 0;
  const u16 SVG_CHANNEL_R = 1;
  const u16 SVG_CHANNEL_G = 2;
  const u16 SVG_CHANNEL_B = 3;
  const u16 SVG_CHANNEL_A = 4;

  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  in2: SVGAnimatedString,
  #[constant]
  scale: SVGAnimatedNumber,
  #[constant]
  #[alias = "xChannelSelector"]
  x_channel_selector: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "yChannelSelector"]
  y_channel_selector: SVGAnimatedEnumeration,
}

SVGFEDisplacementMapElement includes SVGFilterPrimitiveStandardAttributes;
