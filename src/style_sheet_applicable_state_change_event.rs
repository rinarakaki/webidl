#[chrome_only, exposed = Window]
pub trait StyleSheetApplicableStateChangeEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: StyleSheetApplicableStateChangeEventInit);

  stylesheet: Option<CSSStyleSheet>,
  applicable: bool,
}

pub struct StyleSheetApplicableStateChangeEventInit: EventInit {
  pub stylesheet: Option<CSSStyleSheet> = None,
  pub applicable: bool = false,
}
