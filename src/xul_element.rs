use super::Element;

pub trait XULControllers;

#[chrome_only, exposed = Window]
pub trait XULElement: Element {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  // Layout properties
  #[setter_throws]
  pub flex: DOMString,

  // Properties for hiding elements.
  pub hidden: bool,
  pub collapsed: bool,

  // Property for hooking up to broadcasters
  #[setter_throws]
  pub observes: DOMString,

  // Properties for hooking up to popups
  #[setter_throws]
  pub menu: DOMString,
  #[setter_throws]
  #[alias = "contextMenu"]
  pub context_menu: DOMString,
  #[setter_throws]
  pub tooltip: DOMString,

  // Width/height properties
  #[setter_throws]
  pub width: DOMString,
  #[setter_throws]
  pub height: DOMString,
  #[setter_throws]
  #[alias = "minWidth"]
  pub min_width: DOMString,
  #[setter_throws]
  #[alias = "minHeight"]
  pub min_height: DOMString,
  #[setter_throws]
  #[alias = "maxWidth"]
  pub max_width: DOMString,
  #[setter_throws]
  #[alias = "maxHeight"]
  pub max_height: DOMString,

  // Return the screen coordinates of the element.
  screenX: i32,
  screenY: i32,

  // Tooltip
  #[setter_throws]
  #[alias = "tooltipText"]
  pub tooltip_text: DOMString,

  // Properties for images
  #[setter_throws]
  pub src: DOMString,

  #[throws, chrome_only]
  controllers: XULControllers,

  #[needs_caller_type]
  pub fn click();
  #[alias = "doCommand"]
  pub fn do_command();

  // Returns true if this is a menu-type element that has a menu
  // frame associated with it.
  #[alias = "hasMenu"]
  pub fn has_menu() -> bool;

  // If this is a menu-type element, opens or closes the menu
  // depending on the argument passed.
  #[alias = "openMenu"]
  pub fn open_menu(open: bool);
}

XULElement includes GlobalEventHandlers;
XULElement includes HTMLOrForeignElement;
XULElement includes ElementCSSInlineStyle;
XULElement includes TouchEventHandlers;
XULElement includes OnErrorEventHandlerForNodes;
