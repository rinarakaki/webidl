// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#trackevent

#[exposed = Window]
pub trait TrackEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: TrackEventInit) -> Self;

  readonly attribute (VideoTrack or AudioTrack or TextTrack)? track;
}

pub struct TrackEventInit: EventInit {
  (VideoTrack or AudioTrack or TextTrack)? track = None;
}
