pub enum DOMRequestReadyState {
  #[alias = "pending"]
  Pending,
  #[alias = "done"]
  Done
}

#[exposed = (Window, Worker)]
pub trait mixin DOMRequestShared {
  #[alias = "readyState"]
  ready_state: DOMRequestReadyState,

  result: any,
  error: Option<DOMException>,

  #[alias = "onsuccess"]
  pub on_success: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
}

#[exposed = (Window, Worker)]
pub trait DOMRequest: EventTarget {
  // The #[TreatNonCallableAsNull] annotation is required since then() should do
  // nothing instead of throwing errors when non-callable arguments are passed.
  // See documentation for Promise.then to see why we return "any".
  #[new_object]
  pub fn then(
    [TreatNonCallableAsNull] optional Option<AnyCallback> fulfillCallback = None,
    [TreatNonCallableAsNull] optional Option<AnyCallback> rejectCallback = None)
    -> Result<any>;

  #[chrome_only]
  #[alias = "fireDetailedError"]
  pub fn fire_detailed_error(a_error: DOMException);
}

DOMRequest includes DOMRequestShared;
