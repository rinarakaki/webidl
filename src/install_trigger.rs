// A callback function that webpages can implement to be notified when triggered
// installs complete.
callback InstallTriggerCallback = void(url: DOMString, url: DOMString);

pub struct InstallTriggerData {
  pub URL: DOMString,
  pub IconURL: Option<DOMString>,
  pub Hash: Option<DOMString>,
}

// The trait for the InstallTrigger object available to all websites.
#[chrome_only,
 JSImplementation = "@mozilla.org/addons/installtrigger;1",
 exposed = Window]
pub trait InstallTriggerImpl {
  // Retained for backwards compatibility.
  const u16 SKIN = 1;
  const u16 LOCALE = 2;
  const u16 CONTENT = 4;
  const u16 PACKAGE = 7;

  // Tests if installation is enabled.
  pub fn enabled() -> bool;

  // Tests if installation is enabled.
  //
  // @deprecated Use "enabled" in the future.
  #[alias = "updateEnabled"]
  pub fn update_enabled() -> bool;

  // Starts a new installation of a set of add-ons.
  //
  // @param aArgs
  //         The add-ons to install. This should be a JS object, each property
  //         is the name of an add-on to be installed. The value of the
  //         property should either be a string URL, or an object with the
  //         following properties:
  //          * URL for the add-on's URL
  //          * IconURL for an icon for the add-on
  //          * Hash for a hash of the add-on
  // @param aCallback
  //         A callback to call as each installation succeeds or fails
  // @return true if the installations were successfully started
  bool install(record<DOMString, (DOMString or InstallTriggerData)> installs,
  optional callback: InstallTriggerCallback);

  // Starts installing a new add-on.
  //
  // @deprecated use "install" in the future.
  //
  // @param aType
  //         Unused, retained for backwards compatibility
  // @param aUrl
  //         The URL of the add-on
  // @param aSkin
  //         Unused, retained for backwards compatibility
  // @return true if the installation was successfully started
  #[alias = "installChrome"]
  pub fn install_chrome(type: u16, type: u16, type: u16) -> bool;

  // Starts installing a new add-on.
  //
  // @deprecated use "install" in the future.
  //
  // @param aUrl
  //         The URL of the add-on
  // @param aFlags
  //         Unused, retained for backwards compatibility
  // @return true if the installation was successfully started
  #[alias = "startSoftwareUpdate"]
  pub fn start_software_update(url: DOMString, optional flags: u16) -> bool;
}
