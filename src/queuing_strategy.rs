pub struct QueuingStrategy {
  unrestricted f64 highWaterMark;
  pub size: QueuingStrategySize,
}

callback QueuingStrategySize = unrestricted f64 (optional chunk: any);

pub struct QueuingStrategyInit {
  required unrestricted f64 highWaterMark;
}

#[exposed = (Window,Worker, Worklet)]
pub trait CountQueuingStrategy {
  #[alias = "constructor"]
  pub fn new(init: QueuingStrategyInit) -> Self;

  readonly attribute unrestricted f64 highWaterMark;

  #[throws]
  size: function,
}

#[exposed = (Window,Worker, Worklet)]
pub trait ByteLengthQueuingStrategy {
  #[alias = "constructor"]
  pub fn new(init: QueuingStrategyInit) -> Self;

  readonly attribute unrestricted f64 highWaterMark;

  #[throws]
  size: function,
}
