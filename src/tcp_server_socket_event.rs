#[func = "mozilla::dom::TCPSocket::ShouldTCPSocketExist",
 exposed = Window]
pub trait TCPServerSocketEvent: Event {
  constructor(
    type: DOMString,
    #[optional = {}] eventInitDict: TCPServerSocketEventInit);

  socket: TCPSocket,
}

pub struct TCPServerSocketEventInit: EventInit {
  pub socket: Option<TCPSocket> = None,
}
