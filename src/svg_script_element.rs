// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGScriptElement: SVGElement {
  #[setter_throws]
  pub type: DOMString,

  // CORS attribute
  #[setter_throws]
  pub crossOrigin: Option<DOMString>,
}

SVGScriptElement includes SVGURIReference;

