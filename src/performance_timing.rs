// The origin of this IDL file is
// https://w3c.github.io/navigation-timing/#the-performancetiming-trait

#[exposed = Window]
pub trait PerformanceTiming {
  #[alias = "navigationStart"]
  navigation_start: u64,
  #[alias = "unloadEventStart"]
  unload_event_start: u64,
  #[alias = "unloadEventEnd"]
  unload_event_end: u64,
  #[alias = "redirectStart"]
  redirect_start: u64,
  #[alias = "redirectEnd"]
  redirect_end: u64,
  #[alias = "fetchStart"]
  fetch_start: u64,
  #[alias = "domainLookupStart"]
  domain_lookup_start: u64,
  #[alias = "domainLookupEnd"]
  domain_lookup_end: u64,
  #[alias = "connectStart"]
  connect_start: u64,
  #[alias = "connectEnd"]
  connect_end: u64,
  #[alias = "secureConnectionStart"]
  secure_connection_start: u64,
  #[alias = "requestStart"]
  request_start: u64,
  #[alias = "responseStart"]
  response_start: u64,
  #[alias = "responseEnd"]
  response_end: u64,
  #[alias = "domLoading"]
  dom_loading: u64,
  #[alias = "domInteractive"]
  dom_interactive: u64,
  #[alias = "domContentLoadedEventStart"]
  dom_content_loadedEventStart: u64,
  #[alias = "domContentLoadedEventEnd"]
  dom_content_loadedEventEnd: u64,
  #[alias = "domComplete"]
  dom_complete: u64,
  #[alias = "loadEventStart"]
  load_event_start: u64,
  #[alias = "loadEventEnd"]
  load_event_end: u64,

  // This is a Chrome proprietary extension and not part of the
  // performance/navigation timing specification.
  // Returns 0 if a non-blank paint has not happened.
  #[pref = "dom.performance.time_to_non_blank_paint.enabled"]
  #[alias = "timeToNonBlankPaint"]
  time_to_nonBlankPaint: u64,

  // Returns 0 if a contentful paint has not happened.
  #[pref = "dom.performance.time_to_contentful_paint.enabled"]
  #[alias = "timeToContentfulPaint"]
  time_to_contentfulPaint: u64,

  // This is a Mozilla proprietary extension and not part of the
  // performance/navigation timing specification. It marks the
  // completion of the first presentation flush after DOMContentLoaded.
  #[pref = "dom.performance.time_to_dom_content_flushed.enabled"]
  #[alias = "timeToDOMContentFlushed"]
  time_toDOMContentFlushed: u64,

  // This is a Chrome proprietary extension and not part of the
  // performance/navigation timing specification.
  // Returns 0 if a time-to-interactive measurement has not happened.
  #[pref = "dom.performance.time_to_first_interactive.enabled"]
  #[alias = "timeToFirstInteractive"]
  time_to_firstInteractive: u64,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
