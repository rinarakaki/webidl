// This is a non-standard trait for @-moz-document rules
#[exposed = Window]
pub trait CSSMozDocumentRule: CSSConditionRule {
  // XXX Add access to the URL list.
}
