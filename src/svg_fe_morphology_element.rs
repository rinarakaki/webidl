// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEMorphologyElement: SVGElement {

  // Morphology Operators
  const u16 SVG_MORPHOLOGY_OPERATOR_UNKNOWN = 0;
  const u16 SVG_MORPHOLOGY_OPERATOR_ERODE = 1;
  const u16 SVG_MORPHOLOGY_OPERATOR_DILATE = 2;

  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  operator: SVGAnimatedEnumeration,
  #[constant]
  radiusX: SVGAnimatedNumber,
  #[constant]
  radiusY: SVGAnimatedNumber,
}

SVGFEMorphologyElement includes SVGFilterPrimitiveStandardAttributes;
