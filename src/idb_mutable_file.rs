#[exposed = Window]
pub trait IDBMutableFile: EventTarget {
  name: DOMString,
  type: DOMString,

  database: IDBDatabase,

  #[throws, deprecated = "IDBMutableFileOpen"]
  pub fn open(#[optional = "readonly"] mode: FileMode) -> IDBFileHandle;

  #[alias = "onabort"]
  pub on_abort: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
}
