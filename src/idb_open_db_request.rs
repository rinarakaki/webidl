// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBOpenDBRequest

#[exposed = (Window, Worker)]
pub trait IDBOpenDBRequest: IDBRequest {
  #[alias = "onblocked"]
  pub on_blocked: EventHandler,

  #[alias = "onupgradeneeded"]
  pub on_upgrade_needed: EventHandler,
}
