// The origin of this IDL file is
// https://www.khronos.org/registry/webgl/specs/latest/1.0/#fire-a-webgl-context-event

#[exposed = (Window, Worker),
 func = "mozilla::dom::OffscreenCanvas::PrefEnabledOnWorkerThread"]
pub trait WebGLContextEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInit: WebGLContextEventInit) -> Self;

  #[alias = "statusMessage"]
  status_message: DOMString,
}

// EventInit is defined in the DOM4 specification.
pub struct WebGLContextEventInit: EventInit {
  #[alias = "statusMessage"]
  pub status_message: DOMString = "",
}
