// https://drafts.csswg.org/cssom-view/#mediaquerylistevent

#[exposed = Window]
pub trait MediaQueryListEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: MediaQueryListEventInit);

  media: UTF8String,
  matches: bool,
}

pub struct MediaQueryListEventInit: EventInit {
  pub media: UTF8String = "",
  pub matches: bool = false,
}
