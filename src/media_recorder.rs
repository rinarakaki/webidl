// The origin of this IDL file is
// https://dvcs.w3.org/hg/dap/raw-file/default/media-stream-capture/MediaRecorder.html

pub enum RecordingState { "inactive", "recording", "paused" };

#[exposed = Window]
pub trait MediaRecorder: EventTarget {
  #[alias = "constructor"]
  pub fn new(stream: MediaStream, #[optional = {}] options: MediaRecorderOptions) -> Result<Self, Error>;
  #[throws]
  constructor(node: AudioNode, #[optional = 0] output: u32,
  #[optional = {}] options: MediaRecorderOptions);
  stream: MediaStream,
  #[alias = "mimeType"]
  mime_type: DOMString,
  state: RecordingState,
  #[alias = "onstart"]
  pub onstart: EventHandler,
  #[alias = "onstop"]
  pub onstop: EventHandler,
  #[alias = "ondataavailable"]
  pub ondataavailable: EventHandler,
  #[alias = "onpause"]
  pub onpause: EventHandler,
  #[alias = "onresume"]
  pub onresume: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  #[alias = "videoBitsPerSecond"]
  video_bits_perSecond: u32,
  #[alias = "audioBitsPerSecond"]
  audio_bits_perSecond: u32,

  #[throws]
  pub fn start(optional timeslice: u32);
  #[throws]
  pub fn stop();
  #[throws]
  pub fn pause();
  #[throws]
  pub fn resume();
  #[throws]
  #[alias = "requestData"]
  pub fn request_data();

  static bool isTypeSupported(type: DOMString);
}

pub struct MediaRecorderOptions {
  #[alias = "mimeType"]
  pub mime_type: DOMString = "",
  #[alias = "audioBitsPerSecond"]
  pub audio_bits_perSecond: u32,
  #[alias = "videoBitsPerSecond"]
  pub video_bits_perSecond: u32,
  #[alias = "bitsPerSecond"]
  pub bits_per_second: u32,
}
