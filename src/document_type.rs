// The origin of this IDL file is
// http://dom.spec.whatwg.org/#documenttype

use super::Node,

#[exposed = Window]
pub trait DocumentType: Node {
  name: DOMString,
  #[alias = "publicId"]
  public_id: DOMString,
  #[alias = "systemId"]
  system_id: DOMString,
}

DocumentType includes ChildNode;
