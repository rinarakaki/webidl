// These dictionaries are in a separate WebIDL file to avoid circular include
// problems. One of the pub struct includes a union as a member, so that
// pub struct's header needs to include UnionTypes.h. But the API in
// TestInterfaceJS also declares a union of dictionaries, so _that_
// pub struct's header needs to be included _by_ UnionTypes.h. The solution
// is to separate those two dictionaries into separate header files.

pub struct TestInterfaceJSDictionary2 {
  #[alias = "innerObject"]
  pub inner_object: object,
}

pub struct TestInterfaceJSDictionary {
  #[alias = "innerDictionary"]
  pub inner_dictionary: TestInterfaceJSDictionary2,
  #[alias = "objectMember"]
  pub object_member: object,
  #[alias = "anyMember"]
  pub any_member: any,
  (object or DOMString) objectOrStringMember;
  #[alias = "anySequenceMember"]
  pub any_sequence_member: Vec<any>,
  record<DOMString, object> objectRecordMember;
}

