// The origin of this IDL file is
// http://w3c.github.io/webrtc-pc/#trait-definition

callback RTCSessionDescriptionCallback = void (description: RTCSessionDescriptionInit);
callback RTCPeerConnectionErrorCallback = void (error: DOMException);
callback RTCStatsCallback = void (report: RTCStatsReport);

pub enum RTCSignalingState {
  #[alias = "stable"]
  Stable,
  #[alias = "have-local-offer"]
  HaveLocalOffer,
  #[alias = "have-remote-offer"]
  HaveRemoteOffer,
  #[alias = "have-local-pranswer"]
  HaveLocalPrAnswer,
  #[alias = "have-remote-pranswer"]
  HaveRemotePrAnswer,
  #[alias = "closed"]
  Closed
}

#[alias = "RTCIceGatheringState"]
pub enum RTCICEGatheringState {
  #[alias = "new"]
  New,
  #[alias = "gathering"]
  Gathering,
  #[alias = "complete"]
  Complete
}

#[alias = "RTCIceConnectionState"]
pub enum RTCICEConnectionState {
  #[alias = "new"]
  New,
  #[alias = "checking"]
  Checking,
  #[alias = "connected"]
  Connected,
  #[alias = "completed"]
  Completed,
  #[alias = "failed"]
  Failed,
  #[alias = "disconnected"]
  Disconnected,
  #[alias = "closed"]
  Closed
}

pub enum mozPacketDumpType {
  "rtp",  // dump unencrypted rtp as the MediaPipeline sees it
  "srtp",  // dump encrypted rtp as the MediaPipeline sees it
  "rtcp",  // dump unencrypted rtcp as the MediaPipeline sees it
  "srtcp" // dump encrypted rtcp as the MediaPipeline sees it
}

callback mozPacketCallback = Fn(level: u32,
  #[alias = "mozPacketDumpType"]
  type: moz_packet_dump_type,
  sending: bool,
  packet: ArrayBuffer);

pub struct RTCDataChannelInit {
  pub ordered: bool = true,
  #[EnforceRange]
  #[alias = "maxPacketLifeTime"]
  pub max_packet_lifeTime: u16,
  #[EnforceRange]
  #[alias = "maxRetransmits"]
  pub max_retransmits: u16,
  pub protocol: DOMString = "",
  pub negotiated: bool = false,
  #[EnforceRange]
  pub id: u16,

  // These are deprecated due to renaming in the spec, but still supported for Fx53
  #[alias = "maxRetransmitTime"]
  pub max_retransmit_time: u16,
}

pub struct RTCOfferAnswerOptions {
//  bool voiceActivityDetection = true; // TODO: support this (1184712: Bug)
}

pub struct RTCAnswerOptions: RTCOfferAnswerOptions {
}

pub struct RTCOfferOptions: RTCOfferAnswerOptions {
  #[alias = "offerToReceiveVideo"]
  pub offer_to_receiveVideo: bool,
  #[alias = "offerToReceiveAudio"]
  pub offer_to_receiveAudio: bool,
  #[alias = "iceRestart"]
  pub ice_restart: bool = false,
}

#[pref = "media.peerconnection.enabled",
 JSImplementation = "@mozilla.org/dom/peerconnection;1",
 exposed = Window]
pub trait RTCPeerConnection: EventTarget {
  #[throws]
  constructor(#[optional = {}] configuration: RTCConfiguration,
  optional Option<object> constraints);

  #[throws, StaticClassOverride = "mozilla::dom::RTCCertificate"]
  static Promise<RTCCertificate> generateCertificate (keygenAlgorithm: AlgorithmIdentifier);

  #[pref = "media.peerconnection.identity.enabled"]
  void setIdentityProvider (provider: DOMString,
  #[optional = {}] options: RTCIdentityProviderOptions);
  #[pref = "media.peerconnection.identity.enabled"]
  #[alias = "getIdentityAssertion"]
  pub fn get_identity_assertion() -> Promise<DOMString>;
  Promise<RTCSessionDescriptionInit> createOffer (#[optional = {}] options: RTCOfferOptions);
  Promise<RTCSessionDescriptionInit> createAnswer (#[optional = {}] options: RTCAnswerOptions);
  Promise<void> setLocalDescription (#[optional = {}] description: RTCSessionDescriptionInit);
  Promise<void> setRemoteDescription (#[optional = {}] description: RTCSessionDescriptionInit);
  #[alias = "localDescription"]
  local_description: Option<RTCSessionDescription>,
  #[alias = "currentLocalDescription"]
  current_local_description: Option<RTCSessionDescription>,
  #[alias = "pendingLocalDescription"]
  pending_local_description: Option<RTCSessionDescription>,
  #[alias = "remoteDescription"]
  remote_description: Option<RTCSessionDescription>,
  #[alias = "currentRemoteDescription"]
  current_remote_description: Option<RTCSessionDescription>,
  #[alias = "pendingRemoteDescription"]
  pending_remote_description: Option<RTCSessionDescription>,
  #[alias = "signalingState"]
  signaling_state: RTCSignalingState,
  Promise<void> addIceCandidate (optional (RTCIceCandidateInit or RTCIceCandidate) candidate = {});
  #[alias = "canTrickleIceCandidates"]
  can_trickle_ice_candidates: Option<bool>,
  #[alias = "iceGatheringState"]
  ice_gathering_state: RTCIceGatheringState,
  #[alias = "iceConnectionState"]
  ice_connection_state: RTCIceConnectionState,
  void restartIce ();
  #[pref = "media.peerconnection.identity.enabled"]
  #[alias = "peerIdentity"]
  peer_identity: Promise<RTCIdentityAssertion>,
  #[pref = "media.peerconnection.identity.enabled"]
  #[alias = "idpLoginUrl"]
  idp_login_url: Option<DOMString>,

  #[chrome_only]
  pub id: DOMString,

  RTCConfiguration getConfiguration ();
  #[deprecated = "RTCPeerConnectionGetStreams"]
  Vec<MediaStream> getLocalStreams ();
  #[deprecated = "RTCPeerConnectionGetStreams"]
  Vec<MediaStream> getRemoteStreams ();
  void addStream (stream: MediaStream);

  // replaces addStream; fails if already added
  // because a track can be part of multiple streams, stream parameters
  // indicate which particular streams should be referenced in signaling

  RTCRtpSender addTrack(track: MediaStreamTrack,
  MediaStream... streams);
  #[alias = "removeTrack"]
  pub fn remove_track(sender: RTCRtpSender);

  RTCRtpTransceiver addTransceiver((MediaStreamTrack or DOMString) trackOrKind,
  #[optional = {}] init: RTCRtpTransceiverInit);

  #[alias = "getSenders"]
  pub fn get_senders() -> Vec<RTCRtpSender>;
  #[alias = "getReceivers"]
  pub fn get_receivers() -> Vec<RTCRtpReceiver>;
  #[alias = "getTransceivers"]
  pub fn get_transceivers() -> Vec<RTCRtpTransceiver>;

  #[chrome_only]
  #[alias = "mozSetPacketCallback"]
  pub fn moz_set_packet_callback(callback: mozPacketCallback);
  #[chrome_only]
  void mozEnablePacketDump(level: u32,
  #[alias = "mozPacketDumpType"]
  type: moz_packet_dump_type,
  sending: bool);
  #[chrome_only]
  void mozDisablePacketDump(level: u32,
  #[alias = "mozPacketDumpType"]
  type: moz_packet_dump_type,
  sending: bool);

  void close ();
  #[alias = "onnegotiationneeded"]
  pub onnegotiationneeded: EventHandler,
  #[alias = "onicecandidate"]
  pub onicecandidate: EventHandler,
  #[alias = "onsignalingstatechange"]
  pub onsignalingstatechange: EventHandler,
  #[alias = "onaddstream"]
  pub onaddstream: EventHandler, // obsolete
  #[alias = "onaddtrack"]
  pub onaddtrack: EventHandler, // obsolete
  #[alias = "ontrack"]
  pub ontrack: EventHandler, // replaces onaddtrack and onaddstream.
  #[alias = "oniceconnectionstatechange"]
  pub oniceconnectionstatechange: EventHandler,
  #[alias = "onicegatheringstatechange"]
  pub onicegatheringstatechange: EventHandler,

  Promise<RTCStatsReport> getStats (optional Option<MediaStreamTrack> selector = None);

  // Data channel.
  RTCDataChannel createDataChannel (label: DOMString,
  #[optional = {}] dataChannelDict: RTCDataChannelInit);
  #[alias = "ondatachannel"]
  pub ondatachannel: EventHandler,
}

// Legacy callback API

partial trait RTCPeerConnection {

  // Dummy Promise<void> return values avoid "WebIDL.WebIDLError: error:
  // We have overloads with both Promise and non-Promise return types"

  Promise<void> createOffer (successCallback: RTCSessionDescriptionCallback,
  RTCPeerConnectionErrorCallback failureCallback,
  #[optional = {}] options: RTCOfferOptions);
  Promise<void> createAnswer (successCallback: RTCSessionDescriptionCallback,
  RTCPeerConnectionErrorCallback failureCallback);
  Promise<void> setLocalDescription (description: RTCSessionDescriptionInit,
  Voidfunction successCallback,
  RTCPeerConnectionErrorCallback failureCallback);
  Promise<void> setRemoteDescription (description: RTCSessionDescriptionInit,
  Voidfunction successCallback,
  RTCPeerConnectionErrorCallback failureCallback);
  Promise<void> addIceCandidate (candidate: RTCIceCandidate,
  Voidfunction successCallback,
  RTCPeerConnectionErrorCallback failureCallback);
}
