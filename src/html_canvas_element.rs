// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-canvas-element

pub trait nsISupports;
pub trait Variant;

pub type CanvasSource = (HTMLCanvasElement or OffscreenCanvas);

#[exposed = Window]
pub trait HTMLCanvasElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, pure, setter_throws]
  pub width: u32,
  #[ce_reactions, pure, setter_throws]
  pub height: u32,

  #[throws]
  #[alias = "getContext"]
  pub fn get_context(contextId: DOMString, #[optional = None] contextOptions: any) -> Option<nsISupports>;

  #[throws, needs_subject_principal]
  DOMString toDataURL(#[optional = ""] type: DOMString,
  optional encoderOptions: any);
  #[throws, needs_subject_principal]
  void toBlob(callback: BlobCallback,
  #[optional = ""] type: DOMString,
  optional encoderOptions: any);
}

// Mozilla specific bits
partial trait HTMLCanvasElement {
  #[pure, setter_throws]
  #[alias = "mozOpaque"]
  pub moz_opaque: bool,
  #[throws, needs_subject_principal, pref = "canvas.mozgetasfile.enabled"]
  #[alias = "mozGetAsFile"]
  pub fn moz_get_as_file(name: DOMString, optional Option<DOMString> type = None) -> File;
  // A Mozilla-only extension to get a canvas context backed by f64-buffered
  // shared memory. Only privileged callers can call this.
  #[chrome_only, throws]
  pub fn MozGetIPCContext(contextId: DOMString) -> Option<nsISupports>;

  pub mozPrintCallback: Option<PrintCallback>,

  #[throws, pref = "canvas.capturestream.enabled", needs_subject_principal]
  #[alias = "captureStream"]
  pub fn capture_stream(optional frameRate: f64) -> CanvasCaptureMediaStream;
}

// For OffscreenCanvas
// Reference: https://wiki.whatwg.org/wiki/OffscreenCanvas
partial trait HTMLCanvasElement {
  #[func = "CanvasUtils::IsOffscreenCanvasEnabled", throws]
  #[alias = "transferControlToOffscreen"]
  pub fn transfer_control_to_offscreen() -> OffscreenCanvas;
}

#[chrome_only, exposed = Window]
pub trait MozCanvasPrintState
{
  // A canvas rendering context.
  context: nsISupports,

  // To be called when rendering to the context is done.
  pub fn done();
}

callback PrintCallback = void(ctx: MozCanvasPrintState);

callback BlobCallback = void(blob: Option<Blob>);
