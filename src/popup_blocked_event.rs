pub trait URI;

#[exposed = Window]
pub trait PopupBlockedEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: PopupBlockedEventInit);

  #[alias = "requestingWindow"]
  requesting_window: Option<Window>,
  #[alias = "popupWindowURI"]
  popup_window_uri: Option<URI>,
  #[alias = "popupWindowName"]
  popup_window_name: Option<DOMString>,
  #[alias = "popupWindowFeatures"]
  popup_window_features: Option<DOMString>,
}

pub struct PopupBlockedEventInit: EventInit {
  #[alias = "requestingWindow"]
  pub requesting_window: Option<Window> = None,
  #[alias = "popupWindowURI"]
  pub popup_window_uri: Option<URI> = None,
  #[alias = "popupWindowName"]
  pub popup_window_name: DOMString = "",
  #[alias = "popupWindowFeatures"]
  pub popup_window_features: DOMString = "",
}
