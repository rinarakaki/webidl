// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/semantics.html#htmlhyperlinkelementutils

pub trait mixin HTMLHyperlinkElementUtils {
  #[ce_reactions, setter_throws]
  stringifier attribute USVString href;

  origin: USVString,
  #[ce_reactions]
  pub protocol: USVString,
  #[ce_reactions]
  pub username: USVString,
  #[ce_reactions]
  pub password: USVString,
  #[ce_reactions]
  pub host: USVString,
  #[ce_reactions]
  pub hostname: USVString,
  #[ce_reactions]
  pub port: USVString,
  #[ce_reactions]
  pub pathname: USVString,
  #[ce_reactions]
  pub search: USVString,
  #[ce_reactions]
  pub hash: USVString,
}
