pub enum SpeechRecognitionErrorCode {
  #[alias = "no-speech"]
  NoSpeech,
  #[alias = "aborted"]
  Aborted,
  #[alias = "audio-capture"]
  AudioCapture,
  #[alias = "network"]
  Network,
  #[alias = "not-allowed"]
  NotAllowed,
  #[alias = "service-not-allowed"]
  ServiceNotAllowed,
  #[alias = "bad-grammar"]
  BadGrammar,
  #[alias = "language-not-supported"]
  LanguageNotSupported
}

#[pref = "media.webspeech.recognition.enable",
  func = "SpeechRecognition::IsAuthorized",
  exposed = Window]
pub trait SpeechRecognitionError: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] eventInitDict: SpeechRecognitionErrorInit) -> Self;

  error: SpeechRecognitionErrorCode,
  message: Option<DOMString>,
}

pub struct SpeechRecognitionErrorInit: EventInit {
  pub error: SpeechRecognitionErrorCode = "no-speech",
  pub message: DOMString = "",
}
