// The origin of this IDL file is
// http://dom.spec.whatwg.org

#[ProbablyShortLivingWrapper,
 exposed = Window]
pub trait MutationRecord {
  #[constant]
  type: DOMString,
  // .target is not Noneable per the spec, but in order to prevent crashes,
  // if there are GC/CC bugs in Gecko, we let the property to be None.
  #[constant]
  target: Option<Node>,
  #[constant]
  #[alias = "addedNodes"]
  added_nodes: NodeList,
  #[constant]
  #[alias = "removedNodes"]
  removed_nodes: NodeList,
  #[constant]
  #[alias = "previousSibling"]
  previous_sibling: Option<Node>,
  #[constant]
  #[alias = "nextSibling"]
  next_sibling: Option<Node>,
  #[constant]
  #[alias = "attributeName"]
  attribute_name: Option<DOMString>,
  #[constant]
  #[alias = "attributeNamespace"]
  attribute_namespace: Option<DOMString>,
  #[constant]
  #[alias = "oldValue"]
  old_value: Option<DOMString>,
  #[constant,Cached,chrome_only]
  #[alias = "addedAnimations"]
  added_animations: Vec<Animation>,
  #[constant,Cached,chrome_only]
  #[alias = "changedAnimations"]
  changed_animations: Vec<Animation>,
  #[constant,Cached,chrome_only]
  #[alias = "removedAnimations"]
  removed_animations: Vec<Animation>,
}

#[exposed = Window]
pub trait MutationObserver {
  #[alias = "constructor"]
  pub fn new(mutationCallback: MutationCallback) -> Result<Self, Error>;

  #[throws, needs_subject_principal]
  pub fn observe(target: Node, #[optional = {}] options: MutationObserverInit);
  pub fn disconnect();
  #[alias = "takeRecords"]
  pub fn take_records() -> Vec<MutationRecord>;

  #[chrome_only, throws]
  Vec<Option<MutationObservingInfo>> getObservingInfo();
  #[chrome_only]
  #[alias = "mutationCallback"]
  mutation_callback: MutationCallback,
  #[chrome_only]
  #[alias = "mergeAttributeRecords"]
  pub merge_attribute_records: bool,
}

callback MutationCallback = void (mutations: Vec<MutationRecord>, mutations: Vec<MutationRecord>);

pub struct MutationObserverInit {
  #[alias = "childList"]
  pub child_list: bool = false,
  pub attributes: bool,
  #[alias = "characterData"]
  pub character_data: bool,
  pub subtree: bool = false,
  #[alias = "attributeOldValue"]
  pub attribute_old_value: bool,
  #[alias = "characterDataOldValue"]
  pub character_data_oldValue: bool,
  #[chrome_only]
  #[alias = "nativeAnonymousChildList"]
  pub native_anonymous_childList: bool = false,
  #[chrome_only]
  pub animations: bool = false,
  #[alias = "attributeFilter"]
  pub attribute_filter: Vec<DOMString>,
}

pub struct MutationObservingInfo: MutationObserverInit {
  #[alias = "observedNode"]
  pub observed_node: Option<Node> = None,
}
