#[LegacyOverrideBuiltIns,
 exposed = Window,
 InstrumentedProps = (adoptedStyleSheets,
  #[alias = "caretRangeFromPoint"]
  caret_range_from_point,
  clear,
  #[alias = "exitPictureInPicture"]
  exit_picture_in_picture,
  #[alias = "featurePolicy"]
  feature_policy,
  onbeforecopy,
  onbeforecut,
  onbeforepaste,
  oncancel,
  onfreeze,
  onmousewheel,
  onresume,
  onsearch,
  onsecuritypolicyviolation,
  onwebkitfullscreenchange,
  onwebkitfullscreenerror,
  #[alias = "pictureInPictureElement"]
  picture_in_picture_element,
  #[alias = "pictureInPictureEnabled"]
  picture_in_picture_enabled,
  #[alias = "registerElement"]
  register_element,
  #[alias = "wasDiscarded"]
  was_discarded,
  #[alias = "webkitCancelFullScreen"]
  webkit_cancel_full_screen,
  #[alias = "webkitCurrentFullScreenElement"]
  webkit_current_full_screen_element,
  #[alias = "webkitExitFullscreen"]
  webkit_exit_fullscreen,
  #[alias = "webkitFullscreenElement"]
  webkit_fullscreen_element,
  #[alias = "webkitFullscreenEnabled"]
  webkit_fullscreen_enabled,
  #[alias = "webkitHidden"]
  webkit_hidden,
  #[alias = "webkitIsFullScreen"]
  webkit_is_full_screen,
  #[alias = "webkitVisibilityState"]
  webkit_visibility_state,
  #[alias = "xmlEncoding"]
  xml_encoding,
  #[alias = "xmlStandalone"]
  xml_standalone,
  #[alias = "xmlVersion"]
  xml_version)]
pub trait HTMLDocument: Document {
  // DOM tree accessors
  #[throws]
  getter object (name: DOMString);
}
