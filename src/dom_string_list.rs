// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

#[exposed = (Window, Worker)]
pub trait DOMStringList {
  length: u32,
  getter Option<DOMString> item(index: u32);
  pub fn contains(string: DOMString) -> bool;
}
