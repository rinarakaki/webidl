#[chrome_only, exposed = Window]
pub trait TreeColumn {
  element: Element,

  columns: Option<TreeColumns>,

  #[throws]
  x: i32,
  #[throws]
  width: i32,

  id: DOMString,
  index: i32,

  primary: bool,
  cycler: bool,
  editable: bool,

  const short TYPE_TEXT = 1;
  const short TYPE_CHECKBOX = 2;
  type: short,

  #[alias = "getNext"]
  pub fn get_next() -> Option<TreeColumn>;
  #[alias = "getPrevious"]
  pub fn get_previous() -> Option<TreeColumn>;

  // Returns the previous displayed column, any: if, accounting for
  // the ordinals set on the columns.
  #[alias = "previousColumn"]
  previous_column: Option<TreeColumn>,

  #[throws]
  pub fn invalidate();
}
