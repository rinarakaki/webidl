// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimatedLength {
  #[constant]
  #[alias = "baseVal"]
  base_val: SVGLength,
  #[constant]
  #[alias = "animVal"]
  anim_val: SVGLength,
}

