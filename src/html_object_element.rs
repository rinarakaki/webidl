// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-object-element
// http://www.whatwg.org/specs/web-apps/current-work/#HTMLObjectElement-partial

// http://www.whatwg.org/specs/web-apps/current-work/#the-object-element
#[NeedResolve,
 exposed = Window]
pub trait HTMLObjectElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, pure, setter_throws]
  pub data: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub type: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub name: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "useMap"]
  pub use_map: DOMString,
  #[pure]
  form: Option<HTMLFormElement>,
  #[ce_reactions, pure, setter_throws]
  pub width: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub height: DOMString,
  // Not pure: can trigger about:blank instantiation
  #[needs_subject_principal]
  #[alias = "contentDocument"]
  content_document: Option<Document>,
  // Not pure: can trigger about:blank instantiation
  #[needs_subject_principal]
  #[alias = "contentWindow"]
  content_window: Option<WindowProxy>,

  #[alias = "willValidate"]
  will_validate: bool,
  validity: ValidityState,
  #[throws]
  #[alias = "validationMessage"]
  validation_message: DOMString,
  #[alias = "checkValidity"]
  pub fn check_validity() -> bool;
  #[alias = "reportValidity"]
  pub fn report_validity() -> bool;
  #[alias = "setCustomValidity"]
  pub fn set_custom_validity(error: DOMString);
}

// http://www.whatwg.org/specs/web-apps/current-work/#HTMLObjectElement-partial
partial trait HTMLObjectElement {
  #[ce_reactions, pure, setter_throws]
  pub align: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub archive: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub code: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub declare: bool,
  #[ce_reactions, pure, setter_throws]
  pub hspace: u32,
  #[ce_reactions, pure, setter_throws]
  pub standby: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub vspace: u32,
  #[ce_reactions, pure, setter_throws]
  #[alias = "codeBase"]
  pub code_base: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "codeType"]
  pub code_type: DOMString,

  #[ce_reactions, pure, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString border;
}

partial trait HTMLObjectElement {
  // GetSVGDocument
  #[needs_subject_principal]
  #[alias = "getSVGDocument"]
  pub fn get_svg_document() -> Option<Document>;
}

pub trait mixin MozObjectLoadingContent {
  // Mirrored chrome-only scriptable nsIObjectLoadingContent methods. Please
  // make sure to update this list if nsIObjectLoadingContent changes. Also,
  // make sure everything on here is #[chrome_only].
  #[chrome_only]
  const u32 TYPE_LOADING = 0;
  #[chrome_only]
  const u32 TYPE_IMAGE = 1;
  #[chrome_only]
  const u32 TYPE_FALLBACK = 2;
  #[chrome_only]
  const u32 TYPE_FAKE_PLUGIN = 3;
  #[chrome_only]
  const u32 TYPE_DOCUMENT = 4;
  #[chrome_only]
  const u32 TYPE_NULL = 5;

  // The actual mime type (the one we got back from the network
  // request) for the element.
  #[chrome_only]
  #[alias = "actualType"]
  actual_type: DOMString,

  // Gets the type of the content that's currently loaded. See
  // the constants above for the list of possible values.
  #[chrome_only]
  #[alias = "displayedType"]
  displayed_type: u32,

  // Gets the content type that corresponds to the give MIME type. See the
  // constants above for the list of possible values. If nothing else fits,
  // TYPE_NULL will be returned.
  #[chrome_only]
  u32 getContentTypeForMIMEType(aMimeType: DOMString);

  #[chrome_only]
  #[alias = "getPluginAttributes"]
  pub fn get_plugin_attributes() -> Vec<MozPluginParameter>;

  #[chrome_only]
  #[alias = "getPluginParameters"]
  pub fn get_plugin_parameters() -> Vec<MozPluginParameter>;

  // Forces a re-evaluation and reload of the tag, optionally invalidating its
  // click-to-play state. This can be used when the MIME type that provides a
  // type has changed, instance: for, to force the tag to re-evalulate the
  // handler to use.
  #[chrome_only, throws]
  pub fn reload(aClearActivation: bool);

  // The URL of the data/src loaded in the object. This may be None (i.e.
  // an <embed> with no src).
  #[chrome_only]
  #[alias = "srcURI"]
  src_uri: Option<URI>,

  // Disable the use of fake plugins and reload the tag if necessary
  #[chrome_only, throws]
  #[alias = "skipFakePlugins"]
  pub fn skip_fake_plugins();

  #[chrome_only, throws, needs_caller_type]
  runID: u32,
}

// Name:Value pair type used for passing parameters to NPAPI or javascript
// plugins.
pub struct MozPluginParameter {
  pub name: DOMString = "",
  pub value: DOMString = "",
}

HTMLObjectElement includes MozImageLoadingContent;
HTMLObjectElement includes MozFrameLoaderOwner;
HTMLObjectElement includes MozObjectLoadingContent;
