#[exposed = Window]
pub trait KeyboardEvent: UIEvent {
  #[binary_name = "constructorJS"]
  constructor(
    typeArg: DOMString,
    optional keyboardEventInitDict: KeyboardEventInit= {});

  #[needs_caller_type]
  #[alias = "charCode"]
  char_code: u32,
  #[needs_caller_type]
  #[alias = "keyCode"]
  key_code: u32,

  #[needs_caller_type]
  #[alias = "altKey"]
  alt_key: bool,
  #[needs_caller_type]
  #[alias = "ctrlKey"]
  ctrl_key: bool,
  #[needs_caller_type]
  #[alias = "shiftKey"]
  shift_key: bool,
  #[alias = "metaKey"]
  meta_key: bool,

  #[needs_caller_type]
  #[alias = "getModifierState"]
  pub fn get_modifier_state(key: DOMString) -> bool;

  const u32 DOM_KEY_LOCATION_STANDARD = 0x00;
  const u32 DOM_KEY_LOCATION_LEFT = 0x01;
  const u32 DOM_KEY_LOCATION_RIGHT = 0x02;
  const u32 DOM_KEY_LOCATION_NUMPAD = 0x03;

  location: u32,
  repeat: bool,
  #[alias = "isComposing"]
  is_composing: bool,

  key: DOMString,
  #[needs_caller_type]
  code: DOMString,

  #[binary_name = "initKeyboardEventJS"]
  void initKeyboardEvent(typeArg: DOMString,
  #[optional = false] bubblesArg: bool,
  #[optional = false] cancelableArg: bool,
  optional Option<Window> viewArg = None,
  #[optional = ""] keyArg: DOMString,
  #[optional = 0] locationArg: u32,
  #[optional = false] ctrlKey: bool,
  #[optional = false] altKey: bool,
  #[optional = false] shiftKey: bool,
  #[optional = false] metaKey: bool);

  // This returns the initialized pub struct for generating a
  // same-type keyboard event
  #[Cached, chrome_only, constant]
  #[alias = "initDict"]
  init_dict: KeyboardEventInit,
}

pub struct KeyboardEventInit: EventModifierInit {
  pub key: DOMString = "",
  pub code: DOMString = "",
  pub location: u32 = 0,
  pub repeat: bool = false,
  #[alias = "isComposing"]
  pub is_composing: bool = false,

  // legacy attributes
  #[alias = "charCode"]
  pub char_code: u32 = 0,
  #[alias = "keyCode"]
  pub key_code: u32 = 0,
  pub which: u32 = 0,
}

// Mozilla extensions
KeyboardEvent includes KeyEventMixin;
