pub super::*;

pub trait MozObserver;

pub struct AppNotificationServiceOptions {
  #[alias = "textClickable"]
  pub text_clickable: bool = false,
  #[alias = "manifestURL"]
  pub manifest_url: DOMString = "",
  pub id: DOMString = "",
  #[alias = "dbId"]
  pub db_id: DOMString = "",
  pub dir: DOMString = "",
  pub lang: DOMString = "",
  pub tag: DOMString = "",
  pub data: DOMString = "",
  pub mozbehavior: NotificationBehavior = {},
}
