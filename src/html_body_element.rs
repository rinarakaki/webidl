// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLBodyElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

}

partial trait HTMLBodyElement {
  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString text;
  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString link;
  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString vLink;
  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString aLink;
  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString bgColor;
  #[ce_reactions, setter_throws]
  pub background: DOMString,
}

HTMLBodyElement includes WindowEventHandlers;
