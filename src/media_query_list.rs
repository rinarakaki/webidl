// The origin of this IDL file is
// https://drafts.csswg.org/cssom-view/#mediaquerylist

#[ProbablyShortLivingWrapper,
 exposed = Window]
pub trait MediaQueryList: EventTarget {
  media: UTF8String,
  matches: bool,

  #[throws]
  #[alias = "addListener"]
  pub fn add_listener(listener: Option<EventListener>);

  #[throws]
  #[alias = "removeListener"]
  pub fn remove_listener(listener: Option<EventListener>);

  #[alias = "onchange"]
  pub onchange: EventHandler,
}
