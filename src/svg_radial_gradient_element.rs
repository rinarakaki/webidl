// The origin of this IDL file is
// https://svgwg.org/svg2-draft/

#[exposed = Window]
pub trait SVGRadialGradientElement: SVGGradientElement {
  #[constant]
  cx: SVGAnimatedLength,
  #[constant]
  cy: SVGAnimatedLength,
  #[constant]
  r: SVGAnimatedLength,
  #[constant]
  fx: SVGAnimatedLength,
  #[constant]
  fy: SVGAnimatedLength,
  // XXX: Bug 1242048
  // #[same_object]
  fr: SVGAnimatedLength,
}
