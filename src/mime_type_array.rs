#[LegacyUnenumerableNamedProperties,
 exposed = Window]
pub trait MimeTypeArray {
  length: u32,

  getter Option<MimeType> item(index: u32);
  getter Option<MimeType> namedItem(name: DOMString);
}
