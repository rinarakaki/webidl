// The origin of this IDL file is
// https://dvcs.w3.org/hg/FXTF/raw-file/tip/filters/index.html

#[exposed = Window]
pub trait SVGComponentTransferfunctionElement: SVGElement {
  // Component Transfer Types
  const u16 SVG_FECOMPONENTTRANSFER_TYPE_UNKNOWN = 0;
  const u16 SVG_FECOMPONENTTRANSFER_TYPE_IDENTITY = 1;
  const u16 SVG_FECOMPONENTTRANSFER_TYPE_TABLE = 2;
  const u16 SVG_FECOMPONENTTRANSFER_TYPE_DISCRETE = 3;
  const u16 SVG_FECOMPONENTTRANSFER_TYPE_LINEAR = 4;
  const u16 SVG_FECOMPONENTTRANSFER_TYPE_GAMMA = 5;

  #[constant]
  type: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "tableValues"]
  table_values: SVGAnimatedNumberList,
  #[constant]
  slope: SVGAnimatedNumber,
  #[constant]
  intercept: SVGAnimatedNumber,
  #[constant]
  amplitude: SVGAnimatedNumber,
  #[constant]
  exponent: SVGAnimatedNumber,
  #[constant]
  offset: SVGAnimatedNumber,
}
