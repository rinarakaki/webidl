// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/ and
// http://dev.w3.org/csswg/cssom-view/

#[exposed = Window]
pub trait HTMLElement: Element {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  // metadata attributes
  #[ce_reactions]
  pub title: DOMString,
  #[ce_reactions]
  pub lang: DOMString,
  //         attribute bool translate;
  #[ce_reactions, setter_throws, pure]
  pub dir: DOMString,

  #[ce_reactions, getter_throws, pure]
  attribute #[LegacyNullToEmptyString] DOMString innerText;
  #[ce_reactions, getter_throws, setter_throws, pure]
  attribute #[LegacyNullToEmptyString] DOMString outerText;

  // user interaction
  #[ce_reactions, setter_throws, pure]
  pub hidden: bool,
  #[ce_reactions, setter_throws, pure, pref = "html5.inert.enabled"]
  pub inert: bool,
  #[needs_caller_type]
  pub fn click();
  #[ce_reactions, setter_throws, pure]
  #[alias = "accessKey"]
  pub access_key: DOMString,
  #[pure]
  #[alias = "accessKeyLabel"]
  access_key_label: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub draggable: bool,
  //#[put_forwards = value] readonly attribute DOMTokenList dropzone;
  #[ce_reactions, setter_throws, pure]
  #[alias = "contentEditable"]
  pub content_editable: DOMString,
  #[pure]
  #[alias = "isContentEditable"]
  is_content_editable: bool,
  #[pure, pref = "dom.menuitem.enabled"]
  #[alias = "contextMenu"]
  context_menu: Option<HTMLMenuElement>,
  #[ce_reactions, setter_throws, pure]
  pub spellcheck: bool,
  #[ce_reactions, pure, setter_throws, pref = "dom.forms.inputmode"]
  #[alias = "inputMode"]
  pub input_mode: DOMString,
  #[ce_reactions, pure, setter_throws, pref = "dom.forms.enterkeyhint"]
  #[alias = "enterKeyHint"]
  pub enter_key_hint: DOMString,
  #[ce_reactions, pure, setter_throws, pref = "dom.forms.autocapitalize"]
  pub autocapitalize: DOMString,

  pub nonce: DOMString,

  // command API
  //readonly attribute Option<DOMString> commandType;
  //readonly attribute Option<DOMString> commandLabel;
  //readonly attribute Option<DOMString> commandIcon;
  //readonly attribute Option<bool> commandHidden;
  //readonly attribute Option<bool> commandDisabled;
  //readonly attribute Option<bool> commandChecked;

  // https://html.spec.whatwg.org/multipage/custom-elements.html#dom-attachinternals
  #[pref = "dom.webcomponents.elementInternals.enabled", throws]
  #[alias = "attachInternals"]
  pub fn attach_internals() -> ElementInternals;
}

// http://dev.w3.org/csswg/cssom-view/#extensions-to-the-htmlelement-trait
partial trait HTMLElement {
  // CSSOM things are not #[pure] because they can flush
  #[alias = "offsetParent"]
  offset_parent: Option<Element>,
  #[alias = "offsetTop"]
  offset_top: i32,
  #[alias = "offsetLeft"]
  offset_left: i32,
  #[alias = "offsetWidth"]
  offset_width: i32,
  #[alias = "offsetHeight"]
  offset_height: i32,
}

partial trait HTMLElement {
  #[chrome_only]
  internals: Option<ElementInternals>,

  #[chrome_only]
  #[alias = "isFormAssociatedCustomElements"]
  is_form_associated_custom_elements: bool,
}

pub trait mixin TouchEventHandlers {
  #[func = "nsGenericHTMLElement::LegacyTouchAPIEnabled"]
  #[alias = "ontouchstart"]
  pub ontouchstart: EventHandler,
  #[func = "nsGenericHTMLElement::LegacyTouchAPIEnabled"]
  #[alias = "ontouchend"]
  pub ontouchend: EventHandler,
  #[func = "nsGenericHTMLElement::LegacyTouchAPIEnabled"]
  #[alias = "ontouchmove"]
  pub ontouchmove: EventHandler,
  #[func = "nsGenericHTMLElement::LegacyTouchAPIEnabled"]
  #[alias = "ontouchcancel"]
  pub ontouchcancel: EventHandler,
}

HTMLElement includes GlobalEventHandlers;
HTMLElement includes HTMLOrForeignElement;
HTMLElement includes DocumentAndElementEventHandlers;
HTMLElement includes ElementCSSInlineStyle;
HTMLElement includes TouchEventHandlers;
HTMLElement includes OnErrorEventHandlerForNodes;

#[exposed = Window]
pub trait HTMLUnknownElement: HTMLElement {};
