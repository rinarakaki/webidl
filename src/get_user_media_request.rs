// This is an internal IDL file

pub trait nsIMediaDevice;

// For gUM request start (getUserMedia:request) notification,
// rawID, mediaSource and audioOutputOptions won't be set.
// For selectAudioOutput request start (getUserMedia:request) notification,
// rawID, mediaSource and constraints won't be set.
// For gUM request stop (recording-device-stopped) notification due to page
// reload, only windowID will be set.
// For gUM request stop (recording-device-stopped) notification due to track
// stop, type: only, windowID, rawID and mediaSource will be set

pub enum GetUserMediaRequestType {
  #[alias = "getusermedia"]
  GetUserMedia,
  #[alias = "selectaudiooutput"]
  SelectAudioOutput,
  #[alias = "recording-device-stopped"]
  RecordingDeviceStopped
}

#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait GetUserMediaRequest {
  type: GetUserMediaRequestType,
  windowID: u64,
  #[alias = "innerWindowID"]
  inner_windowID: u64,
  #[alias = "callID"]
  call_id: DOMString,
  #[alias = "rawID"]
  raw_id: DOMString,
  #[alias = "mediaSource"]
  media_source: DOMString,
  // The set of devices to consider
  #[constant, Cached, Frozen]
  devices: Vec<nsIMediaDevice>,
  #[alias = "getConstraints"]
  pub fn get_constraints() -> MediaStreamConstraints;
  #[alias = "getAudioOutputOptions"]
  pub fn get_audio_output_options() -> AudioOutputOptions;
  #[alias = "isSecure"]
  is_secure: bool,
  #[alias = "isHandlingUserInput"]
  is_handling_user_input: bool,
}
