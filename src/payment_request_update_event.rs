// The origin of this WebIDL file is
// https://w3c.github.io/payment-request/#paymentrequestupdateevent-trait

#[SecureContext,
 func = "mozilla::dom::PaymentRequest::PrefEnabled",
 exposed = Window]
pub trait PaymentRequestUpdateEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: PaymentRequestUpdateEventInit);

  #[throws]
  #[alias = "updateWith"]
  pub fn update_with(detailsPromise: Promise<PaymentDetailsUpdate>);
}

pub struct PaymentRequestUpdateEventInit: EventInit {
}
