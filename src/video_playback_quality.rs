// The origin of this IDL file is
// http://dvcs.w3.org/hg/html-media/raw-file/default/media-source/media-source.html

#[pref = "media.mediasource.enabled",
 exposed = Window]
pub trait VideoPlaybackQuality {
  #[alias = "creationTime"]
  creation_time: DOMHighResTimeStamp,
  #[alias = "totalVideoFrames"]
  total_video_frames: u32,
  #[alias = "droppedVideoFrames"]
  dropped_video_frames: u32,
// At Risk: readonly attribute f64 totalFrameDelay;
}

