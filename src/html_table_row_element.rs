// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLTableRowElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[alias = "rowIndex"]
  row_index: i32,
  #[alias = "sectionRowIndex"]
  section_row_index: i32,
  cells: HTMLCollection,
  #[throws]
  #[alias = "insertCell"]
  pub fn insert_cell(#[optional = -1] index: i32) -> HTMLElement;
  #[ce_reactions, throws]
  #[alias = "deleteCell"]
  pub fn delete_cell(index: i32);
}

partial trait HTMLTableRowElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
  #[ce_reactions, setter_throws]
  pub ch: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "chOff"]
  pub ch_off: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "vAlign"]
  pub v_align: DOMString,

  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString bgColor;
}
