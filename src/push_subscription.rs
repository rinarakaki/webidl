// The origin of this IDL file is
// https://w3c.github.io/push-api/

pub trait Principal;

pub enum PushEncryptionKeyName {
  #[alias = "p256dh"]
  P256dh,  // TODO
  #[alias = "auth"]
  Auth
}

pub struct PushSubscriptionKeys {
  pub p256dh: ByteString,
  pub auth: ByteString,
}

pub struct PushSubscriptionJSON {
  pub endpoint: USVString,
  // FIXME: bug 1493860: should this "= {}" be Option<here> For that matter, this
  // PushSubscriptionKeys thing is not even in the spec; "keys" is a record
  // there.
  pub keys: PushSubscriptionKeys = {},
  #[alias = "expirationTime"]
  pub expiration_time: Option<EpochTimeStamp>,
}

pub struct PushSubscriptionInit {
  required USVString endpoint;
  required USVString scope;
  pub p256dhKey: Option<ArrayBuffer>,
  #[alias = "authSecret"]
  pub auth_secret: Option<ArrayBuffer>,
  #[alias = "appServerKey"]
  pub app_server_key: Option<BufferSource>,
  #[alias = "expirationTime"]
  pub expiration_time: Option<EpochTimeStamp> = None,
}

#[exposed = (Window, Worker), pref = "dom.push.enabled"]
pub trait PushSubscription {
  #[chrome_only]
  #[alias = "constructor"]
  pub fn new(initDict: PushSubscriptionInit) -> Result<Self>;

  endpoint: USVString,
  options: PushSubscriptionOptions,
  #[alias = "expirationTime"]
  expiration_time: Option<EpochTimeStamp>,

  #[alias = "getKey"]
  pub fn get_key(name: PushEncryptionKeyName) -> Result<Option<ArrayBuffer>>;

  #[throws, use_counter]
  pub fn unsubscribe() -> Promise<bool>;

  // Implements the custom serializer specified in Push API, section 9.
  #[alias = "toJSON"]
  pub fn to_json() -> Result<PushSubscriptionJSON>;
}
