// The origin of this IDL file is:
// https://w3c.github.io/mediacapture-record/#blobevent-section

#[exposed = Window]
pub trait BlobEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, init: BlobEventInit) -> Self;
  pub data: Blob,
}

pub struct BlobEventInit: EventInit {
  required data: Blob
}
