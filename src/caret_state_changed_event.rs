use super::*;

pub enum CaretChangedReason {
  #[alias = "visibilitychange"]
  VisibilityChange,
  #[alias = "updateposition"]
  UpdatePosition,
  #[alias = "longpressonemptycontent"]
  LongPressOnEmptyContent,
  #[alias = "taponcaret"]
  TapOnCaret,
  #[alias = "presscaret"]
  PressCaret,
  #[alias = "releasecaret"]
  ReleaseCaret,
  #[alias = "scroll"]
  Scroll
}

pub struct CaretStateChangedEventInit: EventInit {
  pub #[optional = true] collapsed: bool
  #[alias = "boundingClientRect"]
  pub #[optional = None] bounding_client_rect: Option<DOMRectReadOnly>
  pub #[optional = VisibilityChange] reason: CaretChangedReason
  #[alias = "caretVisible"]
  pub #[optional = false] caret_visible: bool
  #[alias = "caretVisuallyVisible"]
  pub #[optional = false] caret_visually_visible: bool
  #[alias = "selectionVisible"]
  pub #[optional = false] selection_visible: bool
  #[alias = "selectionEditable"]
  pub #[optional = false] selection_editable: bool
  #[alias = "selectedTextContent"]
  pub #[optional = ""] selected_text_content: DOMString
}

#[chrome_only, exposed = Window]
pub trait CaretStateChangedEvent: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] init: CaretStateChangedEventInit) -> Self;

  pub collapsed: bool,
  // The bounding client rect is relative to the visual viewport. readonly attribute Option<DOMRectReadOnly> boundingClientRect;
  pub reason: CaretChangedReason,
  #[alias = "caretVisible"]
  pub caret_visible: bool,
  #[alias = "caretVisuallyVisible"]
  pub caret_visually_visible: bool,
  #[alias = "selectionVisible"]
  pub selection_visible: bool,
  #[alias = "selectionEditable"]
  pub selection_editable: bool,
  #[alias = "selectedTextContent"]
  pub selected_text_content: DOMString,
}
