// The origin of this IDL file is
// https://github.com/knyg/gamepad/blob/lightindicator/extensions.html

pub enum GamepadLightIndicatorType {
  #[alias = "on-off"]
  OnOff,
  #[alias = "rgb"]
  RGB
}

#[alias = "GamepadLightColor"]
pub struct GamepadLightColour {
  required octet red;
  required octet green;
  required octet blue;
}

#[SecureContext, pref = "dom.gamepad.extensions.lightindicator",
  exposed = Window]
pub trait GamepadLightIndicator {
  type: GamepadLightIndicatorType,
  #[throws, new_object]
  #[alias = "setColor"]
  pub fn set_colour(colour: GamepadLightColour) -> Promise<bool>;
}
