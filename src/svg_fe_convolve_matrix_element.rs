// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEConvolveMatrixElement: SVGElement {

  // Edge Mode Values
  const u16 SVG_EDGEMODE_UNKNOWN = 0;
  const u16 SVG_EDGEMODE_DUPLICATE = 1;
  const u16 SVG_EDGEMODE_WRAP = 2;
  const u16 SVG_EDGEMODE_NONE = 3;

  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  orderX: SVGAnimatedInteger,
  #[constant]
  orderY: SVGAnimatedInteger,
  #[constant]
  #[alias = "kernelMatrix"]
  kernel_matrix: SVGAnimatedNumberList,
  #[constant]
  divisor: SVGAnimatedNumber,
  #[constant]
  bias: SVGAnimatedNumber,
  #[constant]
  targetX: SVGAnimatedInteger,
  #[constant]
  targetY: SVGAnimatedInteger,
  #[constant]
  #[alias = "edgeMode"]
  edge_mode: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "kernelUnitLengthX"]
  kernel_unit_length_x: SVGAnimatedNumber,
  #[constant]
  #[alias = "kernelUnitLengthY"]
  kernel_unit_length_y: SVGAnimatedNumber,
  #[constant]
  #[alias = "preserveAlpha"]
  preserve_alpha: SVGAnimatedBoolean,
}

SVGFEConvolveMatrixElement includes SVGFilterPrimitiveStandardAttributes;
