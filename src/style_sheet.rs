// The origin of this IDL file is
// http://dev.w3.org/csswg/cssom/

#[exposed = Window]
pub trait StyleSheet {
  #[constant]
  type: DOMString,
  #[constant, throws]
  href: Option<DOMString>,
  // Spec says "Node", but it can go None when the node gets a new
  // sheet. That's also why it's not #[constant]
  #[pure]
  #[alias = "ownerNode"]
  owner_node: Option<Node>,
  #[pure]
  #[alias = "parentStyleSheet"]
  parent_style_sheet: Option<StyleSheet>,
  #[pure]
  title: Option<DOMString>,
  #[constant, put_forwards = mediaText]
  media: MediaList,
  #[pure]
  pub disabled: bool,
  // The source map URL for this style sheet. The source map URL can
  // be found in one of two ways.
  //
  // If a SourceMap or X-SourceMap response header is seen, this is
  // the value. If both are seen, SourceMap is preferred. Because
  // this relies on the HTTP response, it can change if checked before
  // the response is available -- which is why it is not #[constant].
  //
  // If the style sheet has the special "# sourceMappingURL = " comment,
  // then this is the URL specified there.
  //
  // If the source map URL is not found by either of these methods,
  // then this is an empty string.
  #[chrome_only, pure]
  #[alias = "sourceMapURL"]
  source_map_url: DOMString,
  // The source URL for this style sheet. If the style sheet has the
  // special "# sourceURL = " comment, then this is the URL specified
  // there. If no such comment is found, then this is the empty
  // string.
  #[chrome_only, pure]
  #[alias = "sourceURL"]
  source_url: DOMString,
}
