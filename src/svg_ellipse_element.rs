// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGEllipseElement: SVGGeometryElement {
  #[constant]
  cx: SVGAnimatedLength,
  #[constant]
  cy: SVGAnimatedLength,
  #[constant]
  rx: SVGAnimatedLength,
  #[constant]
  ry: SVGAnimatedLength,
}

