// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-p-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-p-element
#[exposed = Window]
pub trait HTMLParagraphElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLParagraphElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
}
