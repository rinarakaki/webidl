// The origin of this IDL file is
// https://drafts.csswg.org/cssom/#the-cssrule-trait
// https://drafts.csswg.org/css-animations/#trait-cssrule
// https://drafts.csswg.org/css-counter-styles-3/#extentions-to-cssrule-trait
// https://drafts.csswg.org/css-conditional-3/#extentions-to-cssrule-trait
// https://drafts.csswg.org/css-fonts-3/#om-fontfeaturevalues

// https://drafts.csswg.org/cssom/#the-cssrule-trait
#[exposed = Window]
pub trait CSSRule {
  const u16 STYLE_RULE = 1;
  const u16 CHARSET_RULE = 2; // historical
  const u16 IMPORT_RULE = 3;
  const u16 MEDIA_RULE = 4;
  const u16 FONT_FACE_RULE = 5;
  const u16 PAGE_RULE = 6;
  // FIXME: We don't support MARGIN_RULE yet.
  // XXXbz Should we expose the constant Option<anyway>
  // const u16 MARGIN_RULE = 9;
  const u16 NAMESPACE_RULE = 10;
  #[binary_name = "typeForBindings"]
  type: u16,
  #[alias = "cssText"]
  pub css_text: UTF8String,
  #[alias = "parentRule"]
  parent_rule: Option<CSSRule>,
  #[alias = "parentStyleSheet"]
  parent_style_sheet: Option<CSSStyleSheet>,
}

// https://drafts.csswg.org/css-animations/#trait-cssrule
partial trait CSSRule {
  const u16 KEYFRAMES_RULE = 7;
  const u16 KEYFRAME_RULE = 8;
}

// https://drafts.csswg.org/css-counter-styles-3/#extentions-to-cssrule-trait
partial trait CSSRule {
  const u16 COUNTER_STYLE_RULE = 11;
}

// https://drafts.csswg.org/css-conditional-3/#extentions-to-cssrule-trait
partial trait CSSRule {
  const u16 SUPPORTS_RULE = 12;
}

// Non-standard extension for @-moz-document rules.
partial trait CSSRule {
  #[chrome_only]
  const u16 DOCUMENT_RULE = 13;
}

// https://drafts.csswg.org/css-fonts-3/#om-fontfeaturevalues
partial trait CSSRule {
  const u16 FONT_FEATURE_VALUES_RULE = 14;
}
