// This pub struct holds the parameters sent to the network worker.
pub struct NetworkCommandOptions
{
  pub id: i32 = 0, // opaque id.
  pub cmd: DOMString = "", // the command name.
  pub ifname: DOMString, // for "removeNetworkRoute", "setDNS",
  //     "setDefaultRouteAndDNS", "removeDefaultRoute"
  //     "addHostRoute", "removeHostRoute"
  //     "removeHostRoutes".
  pub ip: DOMString, // for "removeNetworkRoute", "setWifiTethering".
  #[alias = "prefixLength"]
  pub prefix_length: u32, // for "removeNetworkRoute".
  pub domain: DOMString, // for "setDNS"
  pub dnses: Vec<DOMString>, // for "setDNS", "setDefaultRouteAndDNS".
  pub gateway: DOMString, // for "addSecondaryRoute", "removeSecondaryRoute".
  pub gateways: Vec<DOMString>, // for "setDefaultRouteAndDNS", "removeDefaultRoute".
  pub mode: DOMString, // for "setWifiOperationMode".
  pub report: bool, // for "setWifiOperationMode".
  pub enabled: bool, // for "setDhcpServer".
  pub wifictrlinterfacename: DOMString, // for "setWifiTethering".
  #[alias = "internalIfname"]
  pub internal_ifname: DOMString, // for "setWifiTethering".
  #[alias = "externalIfname"]
  pub external_ifname: DOMString, // for "setWifiTethering".
  pub enable: bool, // for "setWifiTethering".
  pub ssid: DOMString, // for "setWifiTethering".
  pub security: DOMString, // for "setWifiTethering".
  pub key: DOMString, // for "setWifiTethering".
  pub prefix: DOMString, // for "setWifiTethering", "setDhcpServer".
  pub link: DOMString, // for "setWifiTethering", "setDhcpServer".
  #[alias = "interfaceList"]
  pub interface_list: Vec<DOMString>, // for "setWifiTethering".
  #[alias = "wifiStartIp"]
  pub wifi_start_ip: DOMString, // for "setWifiTethering".
  #[alias = "wifiEndIp"]
  pub wifi_end_ip: DOMString, // for "setWifiTethering".
  #[alias = "usbStartIp"]
  pub usb_start_ip: DOMString, // for "setWifiTethering".
  #[alias = "usbEndIp"]
  pub usb_end_ip: DOMString, // for "setWifiTethering".
  pub dns1: DOMString, // for "setWifiTethering".
  pub dns2: DOMString, // for "setWifiTethering".
  pub threshold: i64, // for "setNetworkInterfaceAlarm",
  //     "enableNetworkInterfaceAlarm".
  pub startIp: DOMString, // for "setDhcpServer".
  pub endIp: DOMString, // for "setDhcpServer".
  pub serverIp: DOMString, // for "setDhcpServer".
  #[alias = "maskLength"]
  pub mask_length: DOMString, // for "setDhcpServer".
  #[alias = "preInternalIfname"]
  pub pre_internal_ifname: DOMString, // for "updateUpStream".
  #[alias = "preExternalIfname"]
  pub pre_external_ifname: DOMString, // for "updateUpStream".
  #[alias = "curInternalIfname"]
  pub cur_internal_ifname: DOMString, // for "updateUpStream".
  #[alias = "curExternalIfname"]
  pub cur_external_ifname: DOMString, // for "updateUpStream".

  pub ipaddr: i32, // for "ifc_configure".
  pub mask: i32, // for "ifc_configure".
  pub gateway_long: i32, // for "ifc_configure".
  pub dns1_long: i32, // for "ifc_configure".
  pub dns2_long: i32, // for "ifc_configure".

  pub mtu: i32, // for "setMtu".
}

* This pub struct holds the parameters sent back to NetworkService.js.
pub struct NetworkResultOptions
{
  pub id: i32 = 0, // opaque id.
  pub ret: bool = false, // for sync command.
  pub broadcast: bool = false, // for netd broadcast message.
  pub topic: DOMString = "", // for netd broadcast message.
  pub reason: DOMString = "", // for netd broadcast message.

  #[alias = "resultCode"]
  pub result_code: i32 = 0, // for all commands.
  #[alias = "resultReason"]
  pub result_reason: DOMString = "", // for all commands.
  pub error: bool = false, // for all commands.

  pub enable: bool = false, // for "setWifiTethering", "setUSBTethering"
  //     "enableUsbRndis".
  pub result: bool = false, // for "enableUsbRndis".
  pub success: bool = false, // for "setDhcpServer".
  #[alias = "curExternalIfname"]
  pub cur_external_ifname: DOMString = "", // for "updateUpStream".
  #[alias = "curInternalIfname"]
  pub cur_internal_ifname: DOMString = "", // for "updateUpStream".

  pub reply: DOMString = "", // for "command".
  pub route: DOMString = "", // for "ifc_get_default_route".
  pub ipaddr_str: DOMString = "", // The following are for the result of
  // dhcp_do_request.
  pub gateway_str: DOMString = "",
  pub dns1_str: DOMString = "",
  pub dns2_str: DOMString = "",
  pub mask_str: DOMString = "",
  pub server_str: DOMString = "",
  pub vendor_str: DOMString = "",
  pub lease: i32 = 0,
  #[alias = "prefixLength"]
  pub prefix_length: i32 = 0,
  pub mask: i32 = 0,
  pub ipaddr: i32 = 0,
  pub gateway: i32 = 0,
  pub dns1: i32 = 0,
  pub dns2: i32 = 0,
  pub server: i32 = 0,

  pub netId: DOMString = "", // for "getNetId".

  #[alias = "interfaceList"]
  pub interface_list: Vec<DOMString>, // for "getInterfaceList".

  pub flag: DOMString = "down", // for "getInterfaceConfig".
  #[alias = "macAddr"]
  pub mac_addr: DOMString = "", // for "getInterfaceConfig".
  #[alias = "ipAddr"]
  pub ip_addr: DOMString = "", // for "getInterfaceConfig".
}
