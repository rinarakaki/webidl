// This is in a separate file so it can be shared with unittests.

pub enum PCObserverStateType {
  "None",
  "IceConnectionState",
  "IceGatheringState",
  "SignalingState"
}

pub enum PCError {
  "UnknownError",
  "InvalidAccessError",
  "InvalidStateError",
  "InvalidModificationError",
  "OperationError",
  "NotSupportedError",
  "SyntaxError",
  "NotReadableError",
  "TypeError",
  "RangeError",
  "InvalidCharacterError"
}
