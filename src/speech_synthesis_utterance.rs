// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

#[pref = "media.webspeech.synth.enabled",
 exposed = Window]
pub trait SpeechSynthesisUtterance: EventTarget {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;
  #[alias = "constructor"]
  pub fn new(text: DOMString) -> Self;

  pub text: DOMString,
  pub lang: DOMString,
  pub voice: Option<SpeechSynthesisVoice>,
  pub volume: f32,
  pub rate: f32,
  pub pitch: f32,

  #[alias = "onstart"]
  pub onstart: EventHandler,
  #[alias = "onend"]
  pub onend: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  #[alias = "onpause"]
  pub onpause: EventHandler,
  #[alias = "onresume"]
  pub onresume: EventHandler,
  #[alias = "onmark"]
  pub onmark: EventHandler,
  #[alias = "onboundary"]
  pub onboundary: EventHandler,

  #[chrome_only]
  #[alias = "chosenVoiceURI"]
  chosen_voice_uri: DOMString,
}
