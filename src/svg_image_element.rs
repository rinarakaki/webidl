// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGImageElement: SVGGraphicsElement {
  #[constant]
  x: SVGAnimatedLength,
  #[constant]
  y: SVGAnimatedLength,
  #[constant]
  width: SVGAnimatedLength,
  #[constant]
  height: SVGAnimatedLength,
  #[constant]
  #[alias = "preserveAspectRatio"]
  preserve_aspect_ratio: SVGAnimatedPreserveAspectRatio,
  #[ce_reactions, setter_throws]
  pub decoding: DOMString,
  #[new_object]
  pub fn decode() -> Promise<void>;
}

SVGImageElement includes MozImageLoadingContent;
SVGImageElement includes SVGURIReference;

