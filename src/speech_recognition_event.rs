pub trait nsISupports;

#[pref = "media.webspeech.recognition.enable",
 func = "SpeechRecognition::IsAuthorized",
 exposed = Window]
pub trait SpeechRecognitionEvent: Event
{
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: SpeechRecognitionEventInit);

  #[alias = "resultIndex"]
  result_index: u32,
  results: Option<SpeechRecognitionResultList>,
  interpretation: any,
  emma: Option<Document>,
}

pub struct SpeechRecognitionEventInit: EventInit
{
  #[alias = "resultIndex"]
  pub result_index: u32 = 0,
  pub results: Option<SpeechRecognitionResultList> = None,
  pub interpretation: any = None,
  pub emma: Option<Document> = None,
}
