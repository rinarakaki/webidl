// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

#[pref = "media.webspeech.recognition.enable",
 LegacyFactoryfunction = webkitSpeechRecognition,
 func = "SpeechRecognition::IsAuthorized",
 exposed = Window]
pub trait SpeechRecognition: EventTarget {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  // recognition parameters
  pub grammars: SpeechGrammarList,
  pub lang: DOMString,
  #[throws]
  pub continuous: bool,
  #[alias = "interimResults"]
  pub interim_results: bool,
  #[alias = "maxAlternatives"]
  pub max_alternatives: u32,
  #[throws]
  #[alias = "serviceURI"]
  pub service_uri: DOMString,

  // methods to drive the speech interaction
  #[throws, needs_caller_type]
  pub fn start(optional stream: MediaStream);
  pub fn stop();
  pub fn abort();

  // event methods
  #[alias = "onaudiostart"]
  pub onaudiostart: EventHandler,
  #[alias = "onsoundstart"]
  pub onsoundstart: EventHandler,
  #[alias = "onspeechstart"]
  pub onspeechstart: EventHandler,
  #[alias = "onspeechend"]
  pub onspeechend: EventHandler,
  #[alias = "onsoundend"]
  pub onsoundend: EventHandler,
  #[alias = "onaudioend"]
  pub onaudioend: EventHandler,
  #[alias = "onresult"]
  pub onresult: EventHandler,
  #[alias = "onnomatch"]
  pub onnomatch: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  #[alias = "onstart"]
  pub onstart: EventHandler,
  #[alias = "onend"]
  pub onend: EventHandler,
}
