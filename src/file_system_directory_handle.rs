pub struct FileSystemGetFileOptions {
  pub create: bool = false,
}

pub struct FileSystemGetDirectoryOptions {
  pub create: bool = false,
}

pub struct FileSystemRemoveOptions {
  pub recursive: bool = false,
}

// TODO: Add Serializzable
#[exposed = (Window, Worker), SecureContext, pref = "dom.fs.enabled"]
pub trait FileSystemDirectoryHandle: FileSystemHandle {
  // This trait defines an async iterable, however that isn't supported yet
  // by the bindings. So for now just explicitly define what an async iterable
  // definition implies.
  //async iterable<USVString, FileSystemHandle>;
  pub fn entries() -> FileSystemDirectoryIterator;
  pub fn keys() -> FileSystemDirectoryIterator;
  pub fn values() -> FileSystemDirectoryIterator;

  #[alias = "getFileHandle"]
  pub fn get_file_handle(name: USVString, #[optional = {}] options: FileSystemGetFileOptions) -> Promise<FileSystemFileHandle>;
  #[alias = "getDirectoryHandle"]
  pub fn get_directory_handle(name: USVString, #[optional = {}] options: FileSystemGetDirectoryOptions) -> Promise<FileSystemDirectoryHandle>;

  #[alias = "removeEntry"]
  pub fn remove_entry(name: USVString, #[optional = {}] options: FileSystemRemoveOptions) -> Promise<void>;

  Promise<Vec<USVString>?> resolve(possibleDescendant: FileSystemHandle);
}
