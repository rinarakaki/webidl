// These objects support visualization of a css-grid by the dev tools.
// Explicit and implicit types apply to tracks, lines, and areas.
// https://drafts.csswg.org/css-grid/#explicit-grids
// https://drafts.csswg.org/css-grid/#implicit-grids
pub enum GridDeclaration {
  #[alias = "explicit"]
  Explicit,
  #[alias = "implicit"]
  Implicit
}

// Tracks expanded from auto-fill are repeat , auto-fits with elements are
// also repeat, auto-fits with no elements are removed, other tracks are static.
pub enum GridTrackState {
  #[alias = "static"]
  Static,
  #[alias = "repeat"]
  Repeat,
  #[alias = "removed"]
  Removed
}

#[chrome_only, exposed = Window]
pub trait Grid {
  rows: GridDimension,
  cols: GridDimension,
  #[Cached, constant]
  areas: Vec<GridArea>,
}

#[chrome_only, exposed = Window]
pub trait GridDimension {
  lines: GridLines,
  tracks: GridTracks,
}

#[chrome_only, exposed = Window]
pub trait GridLines {
  length: u32,

  // This accessor method allows array-like access to lines.
  // @param index A 0-indexed value.
  getter Option<GridLine> item(index: u32);
}

#[chrome_only, exposed = Window]
pub trait GridLine {
  // Names include both explicit names and implicit names, which will be
  // assigned if the line contributes to a named area.
  // https://drafts.csswg.org/css-grid/#implicit-named-lines
  #[Cached, constant]
  names: Vec<DOMString>,

  start: f64,

  // Breadth is the gap between the start of this line and the start of the
  // next track in flow direction. It primarily is set by use of the -gap
  // properties.
  // https://drafts.csswg.org/css-grid/#gutters
  breadth: f64,

  type: GridDeclaration,

  // Number is the 1-indexed index of the line in flow order. The
  // first explicit line has number 1, and numbers increment by 1 for
  // each line after that. Lines before the first explicit line
  // have number 0, which is not a valid addressable line number, and
  // should be filtered out by callers.
  number: u32,

  // NegativeNumber is the 1-indexed index of the line in reverse
  // flow order. The last explicit line has negativeNumber -1, and
  // negativeNumbers decrement by 1 for each line before that.
  // Lines after the last explicit line have negativeNumber 0, which
  // is not a valid addressable line number, and should be filtered
  // out by callers.
  #[alias = "negativeNumber"]
  negative_number: i32,
}

#[chrome_only, exposed = Window]
pub trait GridTracks {
  length: u32,

  // This accessor method allows array-like access to tracks.
  // @param index A 0-indexed value.
  getter Option<GridTrack> item(index: u32);
}

#[chrome_only, exposed = Window]
pub trait GridTrack {
  start: f64,
  breadth: f64,
  type: GridDeclaration,
  state: GridTrackState,
}

#[chrome_only, exposed = Window]
pub trait GridArea {
  name: DOMString,
  type: GridDeclaration,

  // These values are 1-indexed line numbers bounding the area.
  #[alias = "rowStart"]
  row_start: u32,
  #[alias = "rowEnd"]
  row_end: u32,
  #[alias = "columnStart"]
  column_start: u32,
  #[alias = "columnEnd"]
  column_end: u32,
}
