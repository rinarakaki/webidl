// The origin of this IDL file is
// https://w3c.github.io/paint-timing/#sec-PerformancePaintTiming

#[exposed = (Window)]
pub trait PerformancePaintTiming: PerformanceEntry {}
