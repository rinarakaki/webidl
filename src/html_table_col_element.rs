// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLTableColElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub span: u32,
}

partial trait HTMLTableColElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
  #[ce_reactions, setter_throws]
  pub ch: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "chOff"]
  pub ch_off: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "vAlign"]
  pub v_align: DOMString,
  #[ce_reactions, setter_throws]
  pub width: DOMString,
}
