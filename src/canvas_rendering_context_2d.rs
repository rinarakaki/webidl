// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

use super::*;

pub enum CanvasWindingRule {
  #[alias = NonZero]
  NonZero,
  #[alias = "evenodd"]
  EvenOdd
}

#[GenerateInit]
pub struct ContextAttributes2D {
  // Whether or not we're planning to do a lot of readback operations
  #[alias = "willReadFrequently"]
  pub #[optional = false] will_read_frequently: bool,
  // Signal if the canvas contains an alpha channel
  pub #[optional = true] alpha: bool,
}

pub struct HitRegionOptions {
  pub #[optional = None] path: Option<Path2D>,
  pub #[optional = ""] id: DOMString,
  pub #[optional = None] control: Option<Element>,
}

pub type HTMLOrSVGImageElement = (HTMLImageElement or SVGImageElement);

pub type CanvasImageSource = (
  HTMLOrSVGImageElement or
  HTMLCanvasElement or
  HTMLVideoElement or
  OffscreenCanvas or
  ImageBitmap);

#[exposed = Window]
pub trait CanvasRenderingContext2D {
  // Back-reference to the canvas. Might be None if we're not
  // associated with a canvas.
  pub canvas: Option<HTMLCanvasElement>,

  // Mozilla-specific stuff
  // FIXME Bug 768048 mozCurrentTransform/mozCurrentTransformInverse should return a WebIDL array.
  #[throws]
  #[alias = "mozCurrentTransform"]
  pub mut moz_current_transform: object, // #[ m11, m12, m21, m22, dx, dy ], i.e. row major
  #[throws]
  #[alias = "mozCurrentTransformInverse"]
  pub mut moz_current_transform_inverse: object,

  #[setter_throws]
  #[alias = "mozTextStyle"]
  pub mut moz_text_style: UTF8String,

  // Image smoothing mode -- if disabled, images won't be smoothed
  // if scaled.
  #[deprecated = "PrefixedImageSmoothingEnabled"]
  #[alias = "mozImageSmoothingEnabled"]
  pub moz_image_smoothing_enabled: bool,

  // Show the caret if appropriate when drawing
  #[func = "CanvasUtils::HasDrawWindowPrivilege"]
  const DRAWWINDOW_DRAW_CARET: u32 = 0x01;
  // Don't flush pending layout notifications that could otherwise
  // be batched up
  #[func = "CanvasUtils::HasDrawWindowPrivilege"]
  const DRAWWINDOW_DO_NOT_FLUSH: u32 = 0x02;
  // Draw scrollbars and scroll the viewport if they are present
  #[func = "CanvasUtils::HasDrawWindowPrivilege"]
  const DRAWWINDOW_DRAW_VIEW: u32 = 0x04;
  // Use the widget layer manager if available. This means hardware
  // acceleration may be used, but it might actually be slower or
  // lower quality than normal. It will however more accurately reflect
  // the pixels rendered to the screen.
  #[func = "CanvasUtils::HasDrawWindowPrivilege"]
  const DRAWWINDOW_USE_WIDGET_LAYERS: u32 = 0x08;
  // Don't synchronously decode images - draw what we have
  #[func = "CanvasUtils::HasDrawWindowPrivilege"]
  const DRAWWINDOW_ASYNC_DECODE_IMAGES: u32 = 0x10;

  // Renders a region of a window into the canvas. The contents of
  // the window's viewport are rendered, ignoring viewport clipping
  // and scrolling.
  //
  // @param x
  // @param y
  // @param w
  // @param h specify the area of the window to render, in CSS
  // pixels.
  //
  // @param backgroundColor the canvas is filled with this color
  // before we render the window into it. This color may be
  // transparent/translucent. It is given as a CSS color string
  // (e.g., rgb() or rgba()).
  //
  // @param flags Used to better control the drawWindow call.
  // Flags can be ORed together.
  //
  // Of course, the rendering obeys the current scale, transform and
  // globalAlpha values.
  //
  // Hints:
  // -- If 'rgba(0,0,0, 0)' is used for the background color, the
  // drawing will be transparent wherever the window is transparent.
  // -- Top-level browsed documents are usually not transparent
  // because the user's background-color preference is applied,
  // but IFRAMEs are transparent if the page doesn't set a background.
  // -- If an opaque color is used for the background color, rendering
  // will be faster because we won't have to compute the window's
  // transparency.
  //
  // This API cannot currently be used by Web content. It is chrome
  // and Web Extensions (with a permission) only.
  #[needs_subject_principal,
    func = "CanvasUtils::HasDrawWindowPrivilege"]
  #[alias = "drawWindow"]
  pub fn draw_window(
    &self,
    window: Window, x: f64, y: f64, z: f64, h: f64,
    bg_colour: UTF8String,
    #[optional = 0] flags: u32) -> Result<()>;

  // This causes a context that is currently using a hardware-accelerated
  // backend to fallback to a software one. All state should be preserved.
  #[chrome_only]
  pub fn demote(&self);
}

CanvasRenderingContext2D includes CanvasState;
CanvasRenderingContext2D includes CanvasTransform;
CanvasRenderingContext2D includes CanvasCompositing;
CanvasRenderingContext2D includes CanvasImageSmoothing;
CanvasRenderingContext2D includes CanvasFillStrokeStyles;
CanvasRenderingContext2D includes CanvasShadowStyles;
CanvasRenderingContext2D includes CanvasFilters;
CanvasRenderingContext2D includes CanvasRect;
CanvasRenderingContext2D includes CanvasDrawPath;
CanvasRenderingContext2D includes CanvasUserInterface;
CanvasRenderingContext2D includes CanvasText;
CanvasRenderingContext2D includes CanvasDrawImage;
CanvasRenderingContext2D includes CanvasImageData;
CanvasRenderingContext2D includes CanvasPathDrawingStyles;
CanvasRenderingContext2D includes CanvasTextDrawingStyles;
CanvasRenderingContext2D includes CanvasPathMethods;
CanvasRenderingContext2D includes CanvasHitRegions;

pub trait mixin CanvasState {
  // State
  pub fn save(&self);  // Push state on state stack
  pub fn restore(&self);  // Pop state stack and restore state
}

pub trait mixin CanvasTransform {
  // Transformations (default transform is the identity matrix)
  #[LenientFloat]
  pub fn scale(&self, x: f64, y: f64) -> Result<()>;
  #[LenientFloat]
  pub fn rotate(&self, angle: f64) -> Result<()>;
  #[LenientFloat]
  pub fn translate(&self, x: f64, 7: f64) -> Result<()>;
  #[LenientFloat]
  pub fn transform(&self, a: f64, b: f64, c: f64, d: f64, e: f64, f: f64)
    -> Result<()>;

  #[new_object]
  #[alias = "getTransform"]
  pub fn get_transform(&self) -> Result<DOMMatrix>;

  #[LenientFloat]
  #[alias = "setTransform"]
  pub fn set_transform(&self, a: f64, b: f64, c: f64, d: f64, e: f64, f: f64)
    -> Result<()>;

  #[alias = "setTransform"]
  pub fn set_transform(&self, #[optional = {}] transform: DOMMatrix2DInit)
    -> Result<()>;

  #[alias = "resetTransform"]
  pub fn reset_transform(&self) -> Result<()>;
}

pub trait mixin CanvasCompositing {
  #[alias = "globalAlpha"]
  pub mut global_alpha: unrestricted f64;  // (default 1.0)
  #[throws]
  #[alias = "globalCompositeOperation"]
  pub mut global_composite_operation: DOMString,  // (default source-over)
}

pub trait mixin CanvasImageSmoothing {
  // Drawing images
  #[alias = "imageSmoothingEnabled"]
  pub mut image_smoothing_enabled: bool,
}

pub trait mixin CanvasFillStrokeStyles {
  // Colours and styles (see also the CanvasPathDrawingStyles trait)
  #[alias = "strokeStyle"]
  pub mut stroke_style: (UTF8String or CanvasGradient or CanvasPattern);  // (black: default)
  #[alias = "fillStyle"]
  pub mut fill_style: (UTF8String or CanvasGradient or CanvasPattern);  // (black: default)

  #[new_object]
  #[alias = "createLinearGradient"]
  pub fn create_linear_gradient(&self, x0: f64, y0: f64, x1: f64, y1: f64)
    -> CanvasGradient;

  #[new_object]
  #[alias = "createRadialGradient"]
  pub fn create_radial_gradient(
    &self,
    x0: f64, y0: f64, r0: f64, x1: f64, y1: f64, r1: f64)
    -> Result<CanvasGradient>;

  #[pref = "canvas.createConicGradient.enabled", new_object]
  #[alias = "createConicGradient"]
  pub fn create_conic_gradient(&self, angle: f64, cx: f64, cy: f64)
    -> CanvasGradient;

  #[new_object]
  #[alias = "createPattern"]
  pub fn create_pattern(
    &self,
    image: CanvasImageSource,
    repetition: #[LegacyNullToEmptyString] DOMString)
    -> Result<Option<CanvasPattern>>;
}

pub trait mixin CanvasShadowStyles {
  #[LenientFloat]
  #[alias = "shadowOffsetX"]
  pub mut shadow_offset_x: f64,  // (default 0)
  #[LenientFloat]
  #[alias = "shadowOffsetY"]
  pub mut shadow_offset_y: f64,  // (default 0)
  #[LenientFloat]
  #[alias = "shadowBlur"]
  pub mut shadow_blur: f64,  // (default 0)
  #[alias = "shadowColor"]
  pub mut shadow_colour: UTF8String, // (default transparent black)
}

pub trait mixin CanvasFilters {
  #[pref = "canvas.filters.enabled", setter_throws]
  pub mut filter: UTF8String,  // (default empty string = no filter)
}

pub trait mixin CanvasRect {
  #[LenientFloat]
  #[alias = "clearRect"]
  pub fn clear_rect(&self, x: f64, y: f64, w: f64, h: f64);
  #[LenientFloat]
  #[alias = "fillRect"]
  pub fn fill_rect(&self, x: f64, y: f64, w: f64, h: f64);
  #[LenientFloat]
  #[alias = "strokeRect"]
  pub fn stroke_rect(&self, x: f64, y: f64, w: f64, h: f64);
}

pub trait mixin CanvasDrawPath {
  // Path API (see also CanvasPathMethods)
  #[alias = "beginPath"]
  pub fn begin_path(&self);
  pub fn fill(&self, #[optional = NonZero] winding: CanvasWindingRule);
  pub fn fill(
    &self, path: Path2D, #[optional = NonZero] winding: CanvasWindingRule);
  pub fn stroke(&self);
  pub fn stroke(&self, path: Path2D);
  pub fn clip(&self, #[optional = NonZero] winding: CanvasWindingRule);
  pub fn clip(
    &self, path: Path2D, #[optional = NonZero] winding: CanvasWindingRule);
  // NOT IMPLEMENTED fn reset_clip(&self);
  #[needs_subject_principal]
  #[alias = "isPointInPath"]
  pub fn is_point_in_path(
    &self, x: unrestricted f64, y: unrestricted f64,
    #[optional = NonZero] winding: CanvasWindingRule) -> bool;
  #[needs_subject_principal]
  // Only required because overloads can't have different extended attributes.
  #[alias = "isPointInPath"]
  pub fn is_point_in_path(
    &self, path: Path2D, x: unrestricted f64, y: unrestricted f64,
    #[optional = NonZero] winding: CanvasWindingRule) -> bool;
  #[needs_subject_principal]
  #[alias = "isPointInStroke"]
  pub fn is_point_in_stroke(&self, x: f64, y: f64) -> bool;
  #[needs_subject_principal]
  // Only required because overloads can't have different extended attributes.
  #[alias = "isPointInStroke"]
  pub fn is_point_in_stroke(
    &self, path: Path2D, x: unrestricted f64, y: unrestricted f64)
    -> bool;
}

pub trait mixin CanvasUserInterface {
  #[pref = "canvas.focusring.enabled"]
  #[alias = "drawFocusIfNeeded"]
  pub fn draw_focus_if_needed(&self, element: Element) -> Result<()>;
  // NOT IMPLEMENTED pub fn scrollPathIntoView();
  // NOT IMPLEMENTED pub fn scrollPathIntoView(path: Path);
}

pub trait mixin CanvasText {
  // Text (see also the CanvasPathDrawingStyles trait)
  #[LenientFloat]
  #[alias = "fillText"]
  pub fn fill_text(
    &self,
    text: DOMString, x: f64, y: f64,
    #[optional] max_width: f64) -> Result<()>;

  #[LenientFloat]
  #[alias = "strokeText"]
  pub fn stroke_text(
    &self,
    text: DOMString, x: f64, y: f64,
    #[optional] max_width: f64) -> Result<()>;

  #[new_object]
  #[alias = "measureText"]
  pub fn measure_text(&self, text: DOMString) -> Result<TextMetrics>;
}

pub trait mixin CanvasDrawImage {
  #[LenientFloat]
  #[alias = "drawImage"]
  pub fn draw_image(&self, image: CanvasImageSource, dx: f64, dy: f64)
    -> Result<()>;

  #[LenientFloat]
  #[alias = "drawImage"]
  pub fn draw_image(
    &self, image: CanvasImageSource, dx: f64, dy: f64, dw: f64, dh: f64)
    -> Result<()>;

  #[LenientFloat]
  #[alias = "drawImage"]
  pub fn draw_image(
    &self,
    image: CanvasImageSource,
    sx: f64, sy: f64, sw: f64, sh: f64,
    dx: f64, dy: f64, dw: f64, dh: f64) -> Result<()>;
}

// See https://github.com/whatwg/html/issues/6262 for #[EnforceRange] usage.
pub trait mixin CanvasImageData {
  // Pixel manipulation
  #[new_object]
  #[alias = "createImageData"]
  pub fn create_image_data(
    &self, sw: #[EnforceRange] i32, sh: #[EnforceRange] i32)
    -> Result<ImageData>;

  #[new_object]
  #[alias = "createImageData"]
  pub fn create_image_data(&self, imagedata: ImageData) -> Result<ImageData>;

  #[new_object, needs_subject_principal]
  #[alias = "getImageData"]
  pub fn get_image_data(
    &self,
    sx: #[EnforceRange] i32, sy: #[EnforceRange] i32, sw: #[EnforceRange] i32,
    sh: #[EnforceRange] i32) -> Result<ImageData>;

  #[alias = "putImageData"]
  pub fn put_image_data(
    &self,
    imagedata: ImageData, dx: #[EnforceRange] i32, dy: #[EnforceRange] i32)
    -> Result<()>;

  #[alias = "putImageData"]
  pub fn put_image_data(
    &self,
    imagedata: ImageData, dx: #[EnforceRange] i32, dy: #[EnforceRange] i32,
    dirty_x: #[EnforceRange] i32, dirty_y: #[EnforceRange] i32,
    dirty_width: #[EnforceRange] i32, dirty_height: #[EnforceRange] i32)
    -> Result<()>;
}

pub trait mixin CanvasPathDrawingStyles {
  // Line caps/joins
  #[LenientFloat]
  #[alias = "lineWidth"]
  pub mut line_width: f64,  // (default 1)
  #[alias = "lineCap"]
  pub mut line_cap: DOMString,  // "butt", "round", "square" (default "butt")
  #[getter_throws]
  #[alias = "lineJoin"]
  pub mut line_join: DOMString,  // "round", "bevel", "mitre" (default "mitre")
  #[LenientFloat]
  #[alias = "miterLimit"]
  pub mut mitre_limit: f64,  // (default 10)

  // Dashed lines
  #[LenientFloat]
  #[alias = "setLineDash"]
  pub fn set_line_dash(&self, segments: Vec<f64>) -> Result<()>;  // Default empty

  #[alias = "getLineDash"]
  pub fn get_line_dash(&self) -> Vec<f64>;

  #[LenientFloat]
  #[alias = "lineDashOffset"]
  pub mut line_dash_offset: f64,
}

pub trait mixin CanvasTextDrawingStyles {
  // Text
  #[setter_throws]
  pub mut font: UTF8String, // (default 10px sans-serif)
  #[alias = "textAlign"]
  pub mut text_align: DOMString, // "start", "end", "left", "right", "centre" (default: "start")
  #[alias = "textBaseline"]
  pub mut text_baseline: DOMString, // "top", "hanging", "middle", "alphabetic", "ideographic", "bottom" (default: "alphabetic")
}

pub trait mixin CanvasPathMethods {
  // shared path API methods
  #[alias = "closePath"]
  pub fn close_path(&self);
  #[LenientFloat]
  #[alias = "moveTo"]
  pub fn move_to(&self, x: f64, y: f64);
  #[LenientFloat]
  #[alias = "lineTo"]
  pub fn line_to(&self, x: f64, y: f64);
  #[LenientFloat]
  #[alias = "quadraticCurveTo"]
  pub fn quadratic_curve_to(&self, cpx: f64, cpy: f64, x: f64, y: f64);

  #[LenientFloat]
  #[alias = "bezierCurveTo"]
  pub fn bezier_curve_to(
    &self, cp1x: f64, cp1y: f64, cp2x: f64, cp2y: f64, x: f64, y: f64);

  #[LenientFloat]
  #[alias = "arcTo"]
  pub fn arc_to(&self, x1: f64, y1: f64, x2: f64, y2: f64, radius: f64)
    -> Result<()>;

  // NOT IMPLEMENTED
  // #[LenientFloat]
  // #[alias = "arcTo"]
  // pub fn arc_to(x1: f64, y1: f64, x2: f64, y2: f64, radius_x: f64, xradius_y1: f64, x1: rotation);

  #[LenientFloat]
  pub fn rect(&self, x: f64, y: f64, w: f64, h: f64);

  #[LenientFloat]
  pub fn arc(
    &self,
    x: f64, y: f64, radius: f64, start_angle: f64, end_angle: f64, 
    #[optional = false] anticlockwise: bool) -> Result<()>;

  #[LenientFloat]
  pub fn ellipse(
    &self,
    x: f64, y: f64, radius_x: f64, radius_y: f64, rotation: f64,
    start_angle: f64, end_angle: f64,
    #[optional = false] anticlockwise: bool) -> Result<()>;
}

pub trait mixin CanvasHitRegions {
  // Hit regions
  #[pref = "canvas.hitregions.enabled"]
  #[alias = "addHitRegion"]
  pub fn add_hit_region(&self, #[optional = {}] options: HitRegionOptions)
    -> Result<()>;
  #[pref = "canvas.hitregions.enabled"]
  #[alias = "removeHitRegion"]
  pub fn remove_hit_region(&self, id: DOMString);
  #[pref = "canvas.hitregions.enabled"]
  #[alias = "clearHitRegions"]
  pub fn clear_hit_regions(&self);
}

#[exposed = Window]
pub trait CanvasGradient {
  // Opaque object
  // add_colour_stop should take a f64
  #[alias = "addColorStop"]
  pub fn add_colour_stop(&self, offset: f32, colour: UTF8String) -> Result<()>;
}

#[exposed = (Window, Worker),
  Func="mozilla::dom::OffscreenCanvas::PrefEnabledOnWorkerThread"]
pub trait CanvasPattern {
  // Opaque object
  // #[LenientFloat] - could not do this overload because of bug 1020975
  // #[alias = "Worker"]
  // fn set_transform(&self, a: f64, b: f64, c: f64, d: f64, e: f64, f: f64)
  //   -> Result<()>;

  #[alias = "setTransform"]
  pub fn set_transform(&self, #[optional = {}] matrix: DOMMatrix2DInit)
    -> Result<()>;
}

#[exposed = Window]
pub trait TextMetrics {
  // x-direction
  pub width: f64, // advance width

  // #[experimental] actual_bounding_box* attributes
  #[pref = "dom.textMetrics.actualBoundingBox.enabled"]
  #[alias = "actualBoundingBoxLeft"]
  pub actual_bounding_box_left: f64,
  #[pref = "dom.textMetrics.actualBoundingBox.enabled"]
  #[alias = "actualBoundingBoxRight"]
  pub actual_bounding_box_right: f64,

  // y-direction
  // #[experimental] font_bounding_box* attributes
  #[pref = "dom.textMetrics.fontBoundingBox.enabled"]
  #[alias = "fontBoundingBoxAscent"]
  pub font_bounding_box_ascent: f64,
  #[pref = "dom.textMetrics.fontBoundingBox.enabled"]
  #[alias = "fontBoundingBoxDescent"]
  pub font_bounding_box_descent: f64,

  // #[experimental] actual_bounding_box* attributes
  #[pref = "dom.textMetrics.actualBoundingBox.enabled"]
  #[alias = "actualBoundingBoxAscent"]
  pub actual_bounding_box_ascent: f64,
  #[pref = "dom.textMetrics.actualBoundingBox.enabled"]
  #[alias = "actualBoundingBoxDescent"]
  pub actual_bounding_box_descent: f64,

  // #[experimental] em_height* attributes
  #[pref = "dom.textMetrics.emHeight.enabled"]
  #[alias = "emHeightAscent"]
  pub em_height_ascent: f64,
  #[pref = "dom.textMetrics.emHeight.enabled"]
  #[alias = "emHeightDescent"]
  pub em_height_descent: f64,

  // #[experimental] *Baseline attributes
  #[pref = "dom.textMetrics.baselines.enabled"]
  #[alias = "hangingBaseline"]
  pub hanging_baseline: f64,
  #[pref = "dom.textMetrics.baselines.enabled"]
  #[alias = "alphabeticBaseline"]
  pub alphabetic_baseline: f64,
  #[pref = "dom.textMetrics.baselines.enabled"]
  #[alias = "ideographicBaseline"]
  pub ideographic_baseline: f64,
}

#[pref = "canvas.path.enabled",
  func = "mozilla::dom::OffscreenCanvas::PrefEnabledOnWorkerThread",
  exposed = Window]
pub trait Path2D {
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[alias = "constructor"]
  pub fn new(other: Path2D) -> Self;

  #[alias = "constructor"]
  pub fn new(path_string: DOMString) -> Self;

  #[alias = "addPath"]
  pub fn add_path(
    &self, path: Path2D, #[optional = {}] transform: DOMMatrix2DInit)
    -> Result<()>;
}

Path2D includes CanvasPathMethods;
