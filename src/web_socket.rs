// The origin of this IDL file is
// http://www.whatwg.org/html/#network

pub enum BinaryType {
  #[alias = "blob"]
  Blob,
  #[alias = "arraybuffer"]
  ArrayBuffer
}

#[exposed = (Window, Worker)]
pub trait WebSocket: EventTarget {
  #[alias = "constructor"]
  pub fn new(url: DOMString,
    optional (DOMString or Vec<DOMString>) protocols = [])
    -> Result<Self>;

  url: DOMString,

  // ready state
  const u16 CONNECTING = 0;
  const u16 OPEN = 1;
  const u16 CLOSING = 2;
  const u16 CLOSED = 3;

  #[alias = "readyState"]
  ready_state: u16,

  #[alias = "bufferedAmount"]
  buffered_amount: u64,

  // networking

  #[alias = "onopen"]
  pub on_open: EventHandler,

  #[alias = "onerror"]
  pub on_error: EventHandler,

  #[alias = "onclose"]
  pub on_close: EventHandler,

  extensions: DOMString,

  protocol: DOMString,

  #[throws]
  pub fn close(optional #[Clamp] u16 code, optional reason: DOMString);

  // messaging

  #[alias = "onmessage"]
  pub on_message: EventHandler,

  #[alias = "binaryType"]
  pub binary_type: BinaryType,

  #[throws]
  pub fn send(data: DOMString);

  #[throws]
  pub fn send(data: Blob);

  #[throws]
  pub fn send(data: ArrayBuffer);

  #[throws]
  pub fn send(data: ArrayBufferView);
}

// Support for creating server-side chrome-only WebSocket. Used in
// devtools remote debugging server.
pub trait nsITransportProvider;

partial trait WebSocket {
  #[chrome_only, new_object, throws]
  static WebSocket createServerWebSocket(
    url: DOMString,
    protocols: Vec<DOMString>,
    #[alias = "nsITransportProvider"]
    ns_i_transport_provider transportProvider,
    DOMString negotiatedExtensions);
}
