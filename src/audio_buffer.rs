// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

pub struct AudioBufferOptions {
  #[alias = "numberOfChannels"]
  pub number_of_channels: u32 = 1,
  required length: u32,
  #[alias = "sampleRate"]
  required sample_rate: f32
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait AudioBuffer {
  #[alias = "constructor"]
  pub fn new(options: AudioBufferOptions) -> Result<Self>;

  #[alias = "sampleRate"]
  pub sample_rate: f32,
  pub length: u32,
  pub duration: f64,  // In seconds
  #[alias = "numberOfChannels"]
  pub number_of_channels: u32,

  #[alias = "getChannelData"]
  pub fn get_channel_data(&self, channel: u32) -> Result<F32Array>;

  #[alias = "copyFromChannel"]
  pub fn copy_from_channel(
    &self,
    destination: F32Array,
    channel_number: u32,
    #[optional = 0] start_in_channel: u32) -> Result<()>;

  #[alias = "copyToChannel"]
  pub fn copy_to_channel(
    &self,
    source: F32Array,
    channel_number: u32,
    #[optional = 0] start_in_channel: u32) -> Result<()>;
}
