#[exposed = Window]
pub trait PopStateEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: PopStateEventInit) -> Self;

  state: any,
}

pub struct PopStateEventInit: EventInit {
  pub state: any = None,
}
