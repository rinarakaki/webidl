pub enum ScrollState {
  #[alias = "started"]
  Started,
  #[alias = "stopped"]
  Stopped
}

pub struct ScrollViewChangeEventInit: EventInit {
  pub state: ScrollState = "started",
}

#[chrome_only, exposed = Window]
pub trait ScrollViewChangeEvent: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] eventInit: ScrollViewChangeEventInit) -> Self;

  state: ScrollState,
}
