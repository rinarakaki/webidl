// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/grouping-content.html#the-menu-element
// https://html.spec.whatwg.org/multipage/obsolete.html#HTMLMenuElement-partial

pub trait MenuBuilder;

// https://html.spec.whatwg.org/multipage/grouping-content.html#the-menu-element
#[exposed = Window]
pub trait HTMLMenuElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pref = "dom.menuitem.enabled"]
  pub type: DOMString,
  #[ce_reactions, setter_throws, pref = "dom.menuitem.enabled"]
  pub label: DOMString,
}

// https://html.spec.whatwg.org/multipage/obsolete.html#HTMLMenuElement-partial
partial trait HTMLMenuElement {
  #[ce_reactions, setter_throws]
  pub compact: bool,
}

// Mozilla specific stuff
partial trait HTMLMenuElement {
  // Creates and dispatches a trusted event named "show".
  // The event is not cancelable and does not bubble.
  // See http://www.whatwg.org/specs/web-apps/current-work/multipage/interactive-elements.html#context-menus
  #[chrome_only]
  #[alias = "sendShowEvent"]
  pub fn send_show_event();

  // Creates a native menu builder. The builder type is dependent on menu type.
  // Currently, it returns the @mozilla.org/content/html-menu-builder;1
  // component. Toolbar menus are not yet supported (the method returns None).
  #[chrome_only]
  #[alias = "createBuilder"]
  pub fn create_builder() -> Option<MenuBuilder>;

  //
  // Builds a menu by iterating over menu children.
  // See http://www.whatwg.org/specs/web-apps/current-work/multipage/interactive-elements.html#building-menus-and-toolbars
  // The caller can use a native builder by calling createBuilder() or provide
  // a custom builder that implements the nsIMenuBuilder trait.
  // A custom builder can be used for example to build native context menus
  // that are not defined using <menupopup>.
  #[chrome_only]
  pub fn build(aBuilder: MenuBuilder);
}
