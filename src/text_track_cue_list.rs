// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#texttrackcuelist

#[exposed = Window]
pub trait TextTrackCueList {
  length: u32,
  getter VTTCue (index: u32);
  #[alias = "getCueById"]
  pub fn get_cue_by_id(id: DOMString) -> Option<VTTCue>;
}
