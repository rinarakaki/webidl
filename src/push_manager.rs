// The origin of this IDL file is
// https://w3c.github.io/push-api/

pub struct PushSubscriptionOptionsInit {
  // bool userVisibleOnly = false;
  (BufferSource or DOMString)? applicationServerKey = None;
}

// The main thread JS implementation. Please see comments in
// dom/push/PushManager.h for the split between PushManagerImpl and PushManager.
#[JSImplementation = "@mozilla.org/push/PushManager;1",
 chrome_only,
 exposed = Window]
pub trait PushManagerImpl {
  #[alias = "constructor"]
  pub fn new(scope: DOMString) -> Result<Self, Error>;

  pub fn subscribe(#[optional = {}] options: PushSubscriptionOptionsInit) -> Promise<PushSubscription>;
  Promise<Option<PushSubscription>> getSubscription();
  #[alias = "permissionState"]
  pub fn permission_state(#[optional = {}] options: PushSubscriptionOptionsInit) -> Promise<PermissionState>;
}

#[exposed = (Window, Worker), pref = "dom.push.enabled"]
pub trait PushManager {
  #[throws, chrome_only]
  #[alias = "constructor"]
  pub fn new(scope: DOMString) -> Self;

  #[throws, use_counter]
  pub fn subscribe(#[optional = {}] options: PushSubscriptionOptionsInit) -> Promise<PushSubscription>;
  #[throws]
  Promise<Option<PushSubscription>> getSubscription();
  #[throws]
  #[alias = "permissionState"]
  pub fn permission_state(#[optional = {}] options: PushSubscriptionOptionsInit) -> Promise<PermissionState>;
}
