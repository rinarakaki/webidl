// You are granted a license to use, reproduce and create derivative works of
// this document.

// WebIDL definition for the "mockExtensionAPI" WebExtensions API,
// only available in tests and locked behind an about:config preference
// ("extensions.webidl-api.expose_mock_interface").
#[exposed = (ServiceWorker), LegacyNoInterfaceObject]
pub trait ExtensionMockAPI {
  // Test API methods scenarios.

  #[throws, WebExtensionStub]
  #[alias = "methodSyncWithReturn"]
  pub fn method_sync_with_return(any... args) -> any;

  #[throws, WebExtensionStub = "NoReturn"]
  #[alias = "methodNoReturn"]
  pub fn method_no_return(any... args);

  #[throws, WebExtensionStub = "Async"]
  #[alias = "methodAsync"]
  pub fn method_async(arg0: any, optional cb: function) -> any;

  #[throws, WebExtensionStub = "AsyncAmbiguous"]
  #[alias = "methodAmbiguousArgsAsync"]
  pub fn method_ambiguous_args_async(any... args) -> any;

  #[throws, WebExtensionStub = "ReturnsPort"]
  #[alias = "methodReturnsPort"]
  pub fn method_returns_port(testName: DOMString) -> ExtensionPort;

  // Test API properties.

  #[replaceable]
  #[alias = "propertyAsErrorObject"]
  property_as_error_object: any,

  #[replaceable]
  #[alias = "propertyAsString"]
  property_as_string: DOMString,

  // Test API events.

  #[replaceable, same_object]
  #[alias = "onTestEvent"]
  on_test_event: ExtensionEventManager,
}
