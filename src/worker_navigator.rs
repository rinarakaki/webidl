#[exposed = Worker]
pub trait WorkerNavigator {}

WorkerNavigator includes NavigatorID;
WorkerNavigator includes NavigatorLanguage;
WorkerNavigator includes NavigatorOnLine;
WorkerNavigator includes NavigatorConcurrentHardware;
WorkerNavigator includes NavigatorStorage;

// http://wicg.github.io/netinfo/#extensions-to-the-navigator-trait
#[exposed = Worker]
partial trait WorkerNavigator {
  #[pref = "dom.netinfo.enabled", throws]
  connection: NetworkInformation,
}

// https://wicg.github.io/media-capabilities/#idl-index
#[exposed = Worker]
partial trait WorkerNavigator {
  #[same_object, func = "mozilla::dom::MediaCapabilities::Enabled"]
  #[alias = "mediaCapabilities"]
  media_capabilities: MediaCapabilities,
}

// https://wicg.github.io/web-locks/#navigator-mixins
WorkerNavigator includes NavigatorLocks;
