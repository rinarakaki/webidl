// The origin of this IDL file is
// https://drafts.csswg.org/cssom/#the-cssstylerule-trait

use super::{CSSRule, CSSStyleDeclaration};

#[exposed = Window]
pub trait CSSStyleRule: CSSRule {
  #[alias = "selectorText"]
  pub selector_text: UTF8String,
  #[same_object, put_forwards = cssText]
  style: CSSStyleDeclaration,
}
