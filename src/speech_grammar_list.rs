// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

#[pref = "media.webspeech.recognition.enable",
 LegacyFactoryfunction = webkitSpeechGrammarList,
 func = "SpeechRecognition::IsAuthorized",
 exposed = Window]
pub trait SpeechGrammarList {
  #[alias = "constructor"]
  pub fn new() -> Self;

  length: u32,
  #[throws]
  getter SpeechGrammar item(index: u32);
  #[throws]
  #[alias = "addFromURI"]
  pub fn add_from_uri(src: DOMString, optional weight: f32);
  #[throws]
  #[alias = "addFromString"]
  pub fn add_from_string(string: DOMString, optional weight: f32);
}
