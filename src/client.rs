// The origin of this IDL file is
// https://w3c.github.io/ServiceWorker/#client-trait

use super::*;

#[exposed = ServiceWorker]
pub trait Client {
  pub url: USVString,

  // Remove frameType in bug 1290936
  #[alias = "frameType"]
  pub frame_type: FrameType,

  pub type: ClientType,
  pub id: DOMString,

  // Implement reserved in bug 1264177
  // pub reserved: bool,

  #[alias = "postMessage"]
  pub fn post_message(&self, message: any, transfer: Vec<object>) -> Result<()>;

  #[alias = "postMessage"]
  pub fn post_message(
    &self, 
    message: any,
    #[optional = {}] a_options: StructuredSerialiseOptions) -> Result<()>;
}

#[exposed = ServiceWorker]
pub trait WindowClient: Client {
  #[alias = "visibilityState"]
  pub visibility_state: VisibilityState,
  pub focused: bool,

  // Implement ancestorOrigins in bug 1264180
  // #[same_object]
  // #[alias = ancestorOrigins]
  // pub ancestor_origins: FrozenArray<USVString>,

  #[new_object, needs_caller_type]
  pub fn focus(&self) -> Result<Promise<WindowClient>>;

  #[new_object]
  pub fn navigate(&self, url: USVString) -> Result<Promise<WindowClient>>;
}

// Remove FrameType in bug 1290936
pub enum FrameType {
  #[alias = "auxiliary"]
  Auxiliary,
  #[alias = "top-level"]
  TopLevel,
  #[alias = "nested"]
  Nested,
  #[alias = "none"]
  None
}
