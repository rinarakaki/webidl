// The origin of this IDL file is
// http://www.w3.org/TR/SVG11/

#[exposed = Window,
 pref = "dom.svg.pathSeg.enabled"]
pub trait SVGPathSegList {
  #[alias = "numberOfItems"]
  number_of_items: u32,
  #[throws]
  getter SVGPathSeg getItem(index: u32);

  // Mozilla-specific stuff
  length: u32, // synonym for numberOfItems
}
