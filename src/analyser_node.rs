// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub super::*;

pub struct AnalyserOptions: AudioNodeOptions {
  #[alias = "fftSize"]
  pub fft_size: u32 = 2048,
  #[alias = "maxDecibels"]
  pub max_decibels: f64 = -30,
  #[alias = "minDecibels"]
  pub min_decibels: f64 = -100,
  #[alias = "smoothingTimeConstant"]
  pub smoothing_time_constant: f64 = 0.8,
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait AnalyserNode: AudioNode {
  #[throws]
  constructor(
    context: BaseAudioContext,
    #[optional = {}] options: AnalyserOptions);

  // Real-time frequency-domain data
  #[alias = "getFloatFrequencyData"]
  pub fn get_float_frequency_data(array: F32Array);
  #[alias = "getByteFrequencyData"]
  pub fn get_byte_frequency_data(array: Uint8Array);

  // Real-time waveform data
  #[alias = "getFloatTimeDomainData"]
  pub fn get_float_time_domain_data(array: F32Array);
  #[alias = "getByteTimeDomainData"]
  pub fn get_byte_time_domain_data(array: Uint8Array);

  #[setter_throws, pure]
  #[alias = "fftSize"]
  pub fft_size: u32,
  #[pure]
  #[alias = "frequencyBinCount"]
  frequency_bin_count: u32,

  #[setter_throws, pure]
  #[alias = "minDecibels"]
  pub min_decibels: f64,
  #[setter_throws, pure]
  #[alias = "maxDecibels"]
  pub max_decibels: f64,

  #[setter_throws, pure]
  #[alias = "smoothingTimeConstant"]
  pub smoothing_time_constant: f64,

}

// Mozilla extension
AnalyserNode includes AudioNodePassThrough;
