pub trait ContentSecurityPolicy;
pub trait Principal;
pub trait URI;
pub trait InputStream;
pub trait ReferrerInfo;

// This pub struct holds load arguments for docshell loads.
#[GenerateInit]
pub struct LoadURIOptions {
  // The principal that initiated the load.
  #[alias = "triggeringPrincipal"]
  pub triggering_principal: Option<Principal> = None,

  // The CSP to be used for the load. That is *not* the CSP that will
  // be applied to subresource loads within that document but the CSP
  // for the document load itself. E.g. if that CSP includes
  // upgrade-insecure-requests, then the new top-level load will
  // be upgraded to HTTPS.
  pub csp: Option<ContentSecurityPolicy> = None,

  // Flags modifying load behaviour. This parameter is a bitwise
  // combination of the load flags defined in nsIWebNavigation.idl.
  #[alias = "loadFlags"]
  pub load_flags: i32 = 0,

  // The referring info of the load. If this argument is None, then the
  // referrer URI and referrer policy will be inferred internally.
  #[alias = "referrerInfo"]
  pub referrer_info: Option<ReferrerInfo> = None,

  // If the URI to be loaded corresponds to a HTTP request, then this stream is
  // appended directly to the HTTP request headers. It may be prefixed
  // with additional HTTP headers. This stream must contain a "\r\n"
  // sequence separating any HTTP headers from the HTTP request body.
  #[alias = "postData"]
  pub post_data: Option<InputStream> = None,

  // If the URI corresponds to a HTTP request, then any HTTP headers
  // contained in this stream are set on the HTTP request. The HTTP
  // header stream is formatted as:
  //     ( HEADER "\r\n" )*
  pub headers: Option<InputStream> = None,

  // Set to indicate a base URI to be associated with the load. Note
  // that at present this argument is only used with view-source aURIs
  // and cannot be used to resolve aURI.
  #[alias = "baseURI"]
  pub base_uri: Option<URI> = None,

  // Set to indicate that the URI to be loaded was triggered by a user
  // action. (Mostly used in the context of Sec-Fetch-User).
  #[alias = "hasValidUserGestureActivation"]
  pub has_valid_user_gesture_activation: bool = false,

 // The SandboxFlags of the entity thats
 // responsible for causing the load.
  #[alias = "triggeringSandboxFlags"]
  pub triggering_sandbox_flags: u32 = 0,
  // If non-0, a value to pass to nsIDocShell::setCancelContentJSEpoch
  // when initiating the load.
  #[alias = "cancelContentJSEpoch"]
  pub cancel_content_js_epoch: i32 = 0,

  // If this is passed, it will control which remote type is used to finish this
  // load. Ignored for non-`about:` loads.
  //
  // NOTE: This is _NOT_ defaulted to `None`, as `None` is the value for
  // `NOT_REMOTE_TYPE`, and we need to determine the difference between no
  // `remoteTypeOverride` and a `remoteTypeOverride` of `NOT_REMOTE_TYPE`.
  #[alias = "remoteTypeOverride"]
  pub remote_type_override: Option<UTF8String>,
}
