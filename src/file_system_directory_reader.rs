// https://wicg.github.io/entries-api/#idl-index

callback FileSystemEntriesCallback = void (entries: Vec<FileSystemEntry>);

#[exposed = Window]
pub trait FileSystemDirectoryReader {

  // readEntries can be called just once. The second time it returns no data.

  #[throws]
  void readEntries(successCallback: FileSystemEntriesCallback,
  optional errorCallback: ErrorCallback);
}
