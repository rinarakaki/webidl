// The origin of this IDL file is
// https://w3c.github.io/FileAPI/#APIASynch

use super::{EventHandler, EventTarget};

#[exposed = (Window, Worker)]
pub trait FileReader: EventTarget {
  #[alias = "constructor"]
  pub fn new() -> Self;

  // Async read methods
  #[alias = "readAsArrayBuffer"]
  pub fn read_as_array_buffer(&self, blob: Blob) -> Result<()>;
  #[alias = "readAsBinaryString"]
  pub fn read_as_binary_string(&self, file_data: Blob) -> Result<()>;
  #[alias = "readAsText"]
  pub fn read_as_text(&self, blob: Blob, optional label: DOMString) -> Result<()>;
  #[alias = "readAsDataURL"]
  pub fn read_as_data_url(&self, blob: Blob) -> Result<()>;

  pub fn abort(&self);

  // States
  const u16 EMPTY = 0;
  const u16 LOADING = 1;
  const u16 DONE = 2;

  #[alias = "readyState"]
  ready_state: u16,

  readonly result: (DOMString or ArrayBuffer)?;

  error: Option<DOMException>,

  // event handler attributes
  #[alias = "onloadstart"]
  pub on_load_start: EventHandler,
  #[alias = "onprogress"]
  pub on_progress: EventHandler,
  #[alias = "onload"]
  pub on_load: EventHandler,
  #[alias = "onabort"]
  pub on_abort: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  #[alias = "onloadend"]
  pub on_loadend: EventHandler,
}
