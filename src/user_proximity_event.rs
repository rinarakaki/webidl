#[pref = "device.sensors.proximity.enabled", func = "nsGlobalWindowInner::DeviceSensorsEnabled",
 exposed = Window]
pub trait UserProximityEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: UserProximityEventInit);

  near: bool,
}

pub struct UserProximityEventInit: EventInit {
  pub near: bool = false,
}
