// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

#[exposed = Window]
pub trait NodeIterator {
  #[constant]
  root: Node,
  #[pure]
  #[alias = "referenceNode"]
  reference_node: Option<Node>,
  #[pure]
  #[alias = "pointerBeforeReferenceNode"]
  pointer_before_reference_node: bool,
  #[constant]
  #[alias = "whatToShow"]
  what_to_show: u32,
  #[constant]
  filter: Option<NodeFilter>,

  #[throws]
  #[alias = "nextNode"]
  pub fn next_node() -> Option<Node>;
  #[throws]
  #[alias = "previousNode"]
  pub fn previous_node() -> Option<Node>;

  pub fn detach();
}
