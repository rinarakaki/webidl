// This file declares data structures used to communicate data logged by
// various components for the purpose of APZ testing (see bug 961289 and 
// gfx/layers/apz/test/APZTestData.h) to JS test code.

pub super::*;

// A single key-value pair in the data.
pub struct ScrollFrameDataEntry {
  pub key: DOMString,
  pub value: DOMString,
}

// All the key-value pairs associated with a given scrollable frame.
// The scrollable frame is identified by a scroll id.
pub struct ScrollFrameData {
  #[alias = "scrollId"]
  pub scroll_id: u64,
  pub entries: Vec<ScrollFrameDataEntry>,
}

// All the scrollable frames associated with a given paint or repaint request.
// The paint or repaint request is identified by a sequence number.
pub struct APZBucket {
  #[alias = "sequenceNumber"]
  pub sequence_number: u32,
  #[alias = "scrollFrames"]
  pub scroll_frames: Vec<ScrollFrameData>,
}

#[pref = "apz.test.logging_enabled",
  exposed = Window]
namespace APZHitResultFlags {
  // These constants should be kept in sync with mozilla::gfx::CompositorHitTestInfo
  const INVISIBLE: u16 = 0;
  const VISIBLE: u16 = 0x0001;
  const IRREGULAR_AREA: u16 = 0x0002;
  const APZ_AWARE_LISTENERS: u16 = 0x0004;
  const INACTIVE_SCROLLFRAME: u16 = 0x0008;
  const PAN_X_DISABLED: u16 = 0x0010;
  const PAN_Y_DISABLED: u16 = 0x0020;
  const PINCH_ZOOM_DISABLED: u16 = 0x0040;
  const DOUBLE_TAP_ZOOM_DISABLED: u16 = 0x0080;
  const SCROLLBAR: u16 = 0x0100;
  const SCROLLBAR_THUMB: u16 = 0x0200;
  const SCROLLBAR_VERTICAL: u16 = 0x0400;
  const REQUIRES_TARGET_CONFIRMATION: u16 = 0x0800;
}

pub struct APZHitResult {
  #[alias = "screenX"]
  pub screen_x: f32,
  #[alias = "screenY"]
  pub screen_y: f32,
  #[alias = "hitResult"]
  pub hit_result: u16,  // Combination of the APZHitResultFlags.* flags
  #[alias = "layersId"]
  pub layers_id: u64,
  #[alias = "scrollId"]
  pub scroll_id: u64,
}

pub struct APZSampledResult {
  #[alias = "scrollOffsetX"]
  pub scroll_offset_x: f32,
  #[alias = "scrollOffsetY"]
  pub scroll_offset_y: f32,
  #[alias = "sampledTimeStamp"]
  pub sampled_time_stamp: DOMHighResTimeStamp,
  #[alias = "layersId"]
  pub layers_id: u64,
  #[alias = "scrollId"]
  pub scroll_id: u64,
}

pub struct AdditionalDataEntry {
  pub key: DOMString,
  pub value: DOMString,
}

// All the paints and repaint requests. This is the top-level data structure.
#[GenerateConversionToJS]
pub struct APZTestData {
  pub paints: Vec<APZBucket>,
  #[alias = "repaintRequests"]
  pub repaint_requests: Vec<APZBucket>,
  #[alias = "hitResults"]
  pub hit_results: Vec<APZHitResult>,
  #[alias = "sampledResults"]
  pub sampled_results: Vec<APZSampledResult>,
  #[alias = "additionalData"]
  pub additional_data: Vec<AdditionalDataEntry>,
}

// A frame uniformity measurement for every scrollable layer
pub struct FrameUniformity {
  #[alias = "layerAddress"]
  pub layer_address: u32,
  #[alias = "frameUniformity"]
  pub frame_uniformity: f32,
}

#[GenerateConversionToJS]
pub struct FrameUniformityResults {
  #[alias = "layerUniformities"]
  pub layer_uniformities: Vec<FrameUniformity>,
}
