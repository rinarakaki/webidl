// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLTableElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub caption: Option<HTMLTableCaptionElement>,
  #[alias = "createCaption"]
  pub fn create_caption() -> HTMLElement;
  #[ce_reactions]
  #[alias = "deleteCaption"]
  pub fn delete_caption();
  #[ce_reactions, setter_throws]
  pub tHead: Option<HTMLTableSectionElement>,
  #[alias = "createTHead"]
  pub fn create_t_head() -> HTMLElement;
  #[ce_reactions]
  #[alias = "deleteTHead"]
  pub fn delete_t_head();
  #[ce_reactions, setter_throws]
  pub tFoot: Option<HTMLTableSectionElement>,
  #[alias = "createTFoot"]
  pub fn create_t_foot() -> HTMLElement;
  #[ce_reactions]
  #[alias = "deleteTFoot"]
  pub fn delete_t_foot();
  #[alias = "tBodies"]
  t_bodies: HTMLCollection,
  #[alias = "createTBody"]
  pub fn create_t_body() -> HTMLElement;
  rows: HTMLCollection,
  #[throws]
  #[alias = "insertRow"]
  pub fn insert_row(#[optional = -1] index: i32) -> HTMLElement;
  #[ce_reactions, throws]
  #[alias = "deleteRow"]
  pub fn delete_row(index: i32);
  //         attribute bool sortable;
  //void stopSorting();
}

partial trait HTMLTableElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
  #[ce_reactions, setter_throws]
  pub border: DOMString,
  #[ce_reactions, setter_throws]
  pub frame: DOMString,
  #[ce_reactions, setter_throws]
  pub rules: DOMString,
  #[ce_reactions, setter_throws]
  pub summary: DOMString,
  #[ce_reactions, setter_throws]
  pub width: DOMString,

  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString bgColor;
  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString cellPadding;
  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString cellSpacing;
}
