// The origin of this IDL file is
// https://w3c.github.io/permissions/#status-of-a-permission

use super::EventTarget;

pub enum PermissionState {
  #[alias = "granted"]
  Granted,
  #[alias = "denied"]
  Denied,
  #[alias = "prompt"]
  Prompt
}

#[exposed = Window]
pub trait PermissionStatus: EventTarget {
  name: PermissionName,
  state: PermissionState,
  #[alias = "onchange"]
  pub on_change: EventHandler,
}
