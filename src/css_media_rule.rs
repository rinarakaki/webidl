// The origin of this IDL file is
// https://drafts.csswg.org/cssom/#the-cssmediarule-trait
// https://drafts.csswg.org/css-conditional/#the-cssmediarule-trait

use super::CSSConditionRule;

// https://drafts.csswg.org/cssom/#the-cssmediarule-trait and
// https://drafts.csswg.org/css-conditional/#the-cssmediarule-trait
// except they disagree with each other. We're taking the inheritance from
// css-conditional and the put_forwards behavior from cssom.
#[exposed = Window]
pub trait CSSMediaRule: CSSConditionRule {
  #[same_object, put_forwards = mediaText]
  media: MediaList,
}
