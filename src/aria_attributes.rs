// The origin of this IDL file is
// https://rawgit.com/w3c/aria/master/#AriaAttributes

pub super::*;

pub trait mixin AriaAttributes {
  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaAtomic"]
  pub mut aria_atomic: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaAutoComplete"]
  pub mut aria_auto_complete: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaBusy"]
  pub mut aria_busy: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaChecked"]
  pub mut aria_checked: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaColCount"]
  pub mut aria_col_count: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaColIndex"]
  pub mut aria_col_index: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaColIndexText"]
  pub mut aria_col_index_text: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaColSpan"]
  pub mut aria_col_span: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaCurrent"]
  pub mut aria_current: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaDescription"]
  pub mut aria_description: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaDisabled"]
  pub mut aria_disabled: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaExpanded"]
  pub mut aria_expanded: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaHasPopup"]
  pub mut aria_has_popup: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaHidden"]
  pub mut aria_hidden: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaInvalid"]
  pub mut aria_invalid: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaKeyShortcuts"]
  pub mut aria_key_shortcuts: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaLabel"]
  pub mut aria_label: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaLevel"]
  pub mut aria_level: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaLive"]
  pub mut aria_live: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaModal"]
  pub mut aria_modal: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaMultiLine"]
  pub mut aria_multi_line: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaMultiSelectable"]
  pub mut aria_multi_selectable: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaOrientation"]
  pub mut aria_orientation: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaPlaceholder"]
  pub mut aria_placeholder: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaPosInSet"]
  pub mut aria_pos_in_set: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaPressed"]
  pub mut aria_pressed: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaReadOnly"]
  pub mut aria_read_only: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaRelevant"]
  pub mut aria_relevant: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaRequired"]
  pub mut aria_required: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaRoleDescription"]
  pub mut aria_role_description: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaRowCount"]
  pub mut aria_row_count: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaRowIndex"]
  pub mut aria_row_index: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaRowIndexText"]
  pub mut aria_row_index_text: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaRowSpan"]
  pub mut aria_row_span: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaSelected"]
  pub mut aria_selected: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaSetSize"]
  pub mut aria_set_size: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaSort"]
  pub mut aria_sort: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaValueMax"]
  pub mut aria_value_max: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaValueMin"]
  pub mut aria_value_min: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaValueNow"]
  pub mut aria_value_now: DOMString,

  #[pref = "accessibility.ARIAReflection.enabled", ce_reactions, setter_throws]
  #[alias = "ariaValueText"]
  pub mut aria_value_text: DOMString,
}
