use super::*;

#[chrome_only, exposed = Window]
pub trait CommandEvent: Event {
  pub command: Option<DOMString>,
}
