// The origin of this IDL file is
// https://w3c.github.io/input-events/#trait-InputEvent

#[exposed = Window]
pub trait InputEvent: UIEvent {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: InputEventInit) -> Self;

  #[alias = "isComposing"]
  is_composing: bool,

  #[alias = "inputType"]
  input_type: DOMString,

  #[needs_caller_type]
  data: Option<DOMString>,
}

pub struct InputEventInit: UIEventInit {
  #[alias = "isComposing"]
  pub is_composing: bool = false,
  #[alias = "inputType"]
  pub input_type: DOMString = "",
  // NOTE: Currently, default value of `data` attribute is declared as empty
  //        string by UI Events. However, both Chrome and Safari uses `None`,
  //        and there is a spec issue about this:
  //        https://github.com/w3c/uievents/issues/139
  //        So, we take `None` for compatibility with them.
  pub data: Option<DOMString> = None,
}

// https://w3c.github.io/input-events/#trait-InputEvent
// https://rawgit.com/w3c/input-events/v1/index.html#trait-InputEvent
partial trait InputEvent {
  #[needs_caller_type]
  #[alias = "dataTransfer"]
  data_transfer: Option<DataTransfer>,
  // Enable `getTargetRanges()` only when `beforeinput` event is enabled
  // because this may be used for feature detection of `beforeinput` event
  // support (due to Chrome not supporting `onbeforeinput` attribute).
  #[pref = "dom.input_events.beforeinput.enabled"]
  #[alias = "getTargetRanges"]
  pub fn get_target_ranges() -> Vec<StaticRange>;
}

partial pub struct InputEventInit {
  #[alias = "dataTransfer"]
  pub data_transfer: Option<DataTransfer> = None,
  #[pref = "dom.input_events.beforeinput.enabled"]
  #[alias = "targetRanges"]
  pub target_ranges: Vec<StaticRange> = [],
}
