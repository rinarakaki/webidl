// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimatedAngle {
  #[constant]
  #[alias = "baseVal"]
  base_val: SVGAngle,
  #[constant]
  #[alias = "animVal"]
  anim_val: SVGAngle,
}

