// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/webrtc.html#idl-def-RTCPeerConnectionIceEvent

pub struct RTCPeerConnectionIceEventInit: EventInit {
  pub candidate: Option<RTCIceCandidate> = None,
}

#[pref = "media.peerconnection.enabled",
 exposed = Window]
pub trait RTCPeerConnectionIceEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: RTCPeerConnectionIceEventInit);

  candidate: Option<RTCIceCandidate>,
}
