// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/struct.html#InterfaceSVGSymbolElement

#[exposed = Window]
pub trait SVGSymbolElement: SVGElement {}

SVGSymbolElement includes SVGFitToViewBox;
SVGSymbolElement includes SVGTests;
