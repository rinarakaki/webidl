 TCPSocketEvent is the event dispatched for all of the events described by TCPSocket,
// except the "error" event. It contains the socket that was associated with the event,
// the type of event, and the data associated with the event if the event is a "data" event.

#[func = "mozilla::dom::TCPSocket::ShouldTCPSocketExist",
 exposed = Window]
pub trait TCPSocketEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: TCPSocketEventInit) -> Self;

  // If the event is a "data" event, data will be the bytes read from the network;
  // if the binaryType of the socket was "arraybuffer", this value will be of type
  // ArrayBuffer, otherwise, it will be a ByteString.
  //
  // For other events, data will be an empty string.
  //TODO: make this (ArrayBuffer or ByteString) after sorting out the rooting required. (1121634: bug)
  data: any,
}

pub struct TCPSocketEventInit: EventInit {
  //TODO: make this (ArrayBuffer or ByteString) after sorting out the rooting required. (1121634: bug)
  pub data: any = None,
}
