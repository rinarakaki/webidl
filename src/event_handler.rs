// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#eventhandler

#[LegacyTreatNonObjectAsNull]
callback EventHandlerNonNull = any (event: Event);
pub type EventHandler = Option<EventHandlerNonNull>;

#[LegacyTreatNonObjectAsNull]
callback OnBeforeUnloadEventHandlerNonNull = Option<DOMString> (event: Event);
pub type OnBeforeUnloadEventHandler = Option<OnBeforeUnloadEventHandlerNonNull>;

#[LegacyTreatNonObjectAsNull]
callback OnErrorEventHandlerNonNull = any ((Event or DOMString) event, optional source: DOMString, optional lineno: u32, optional column: u32, optional error: any);
pub type OnErrorEventHandler = Option<OnErrorEventHandlerNonNull>;

pub trait mixin GlobalEventHandlers {
  #[alias = "onabort"]
  pub on_abort: EventHandler,
  #[alias = "onblur"]
  pub onblur: EventHandler,
// We think the spec is wrong here. See OnErrorEventHandlerForNodes/Window
// below.
//         attribute OnErrorEventHandler onerror;
  #[alias = "onfocus"]
  pub onfocus: EventHandler,
  //(implemented: Not)attribute EventHandler oncancel;
  #[alias = "onauxclick"]
  pub onauxclick: EventHandler,
  #[pref = "dom.input_events.beforeinput.enabled"]
  #[alias = "onbeforeinput"]
  pub onbeforeinput: EventHandler,
  #[alias = "oncanplay"]
  pub oncanplay: EventHandler,
  #[alias = "oncanplaythrough"]
  pub oncanplaythrough: EventHandler,
  #[alias = "onchange"]
  pub onchange: EventHandler,
  #[alias = "onclick"]
  pub onclick: EventHandler,
  #[alias = "onclose"]
  pub onclose: EventHandler,
  #[alias = "oncontextmenu"]
  pub oncontextmenu: EventHandler,
  #[alias = "oncuechange"]
  pub oncuechange: EventHandler,
  #[alias = "ondblclick"]
  pub ondblclick: EventHandler,
  #[alias = "ondrag"]
  pub ondrag: EventHandler,
  #[alias = "ondragend"]
  pub ondragend: EventHandler,
  #[alias = "ondragenter"]
  pub ondragenter: EventHandler,
  #[func = "Event::IsDragExitEnabled"]
  #[alias = "ondragexit"]
  pub ondragexit: EventHandler,
  #[alias = "ondragleave"]
  pub ondragleave: EventHandler,
  #[alias = "ondragover"]
  pub ondragover: EventHandler,
  #[alias = "ondragstart"]
  pub ondragstart: EventHandler,
  #[alias = "ondrop"]
  pub ondrop: EventHandler,
  #[alias = "ondurationchange"]
  pub ondurationchange: EventHandler,
  #[alias = "onemptied"]
  pub onemptied: EventHandler,
  #[alias = "onended"]
  pub onended: EventHandler,
  #[alias = "onformdata"]
  pub onformdata: EventHandler,
  #[alias = "oninput"]
  pub oninput: EventHandler,
  #[alias = "oninvalid"]
  pub oninvalid: EventHandler,
  #[alias = "onkeydown"]
  pub onkeydown: EventHandler,
  #[alias = "onkeypress"]
  pub onkeypress: EventHandler,
  #[alias = "onkeyup"]
  pub onkeyup: EventHandler,
  #[alias = "onload"]
  pub on_load: EventHandler,
  #[alias = "onloadeddata"]
  pub on_loadeddata: EventHandler,
  #[alias = "onloadedmetadata"]
  pub on_loadedmetadata: EventHandler,
  #[alias = "onloadend"]
  pub on_loadend: EventHandler,
  #[alias = "onloadstart"]
  pub on_load_start: EventHandler,
  #[alias = "onmousedown"]
  pub onmousedown: EventHandler,
  #[LegacyLenientThis]
  #[alias = "onmouseenter"]
  pub onmouseenter: EventHandler,
  #[LegacyLenientThis]
  #[alias = "onmouseleave"]
  pub onmouseleave: EventHandler,
  #[alias = "onmousemove"]
  pub onmousemove: EventHandler,
  #[alias = "onmouseout"]
  pub onmouseout: EventHandler,
  #[alias = "onmouseover"]
  pub onmouseover: EventHandler,
  #[alias = "onmouseup"]
  pub onmouseup: EventHandler,
  #[alias = "onwheel"]
  pub onwheel: EventHandler,
  #[alias = "onpause"]
  pub onpause: EventHandler,
  #[alias = "onplay"]
  pub onplay: EventHandler,
  #[alias = "onplaying"]
  pub onplaying: EventHandler,
  #[alias = "onprogress"]
  pub on_progress: EventHandler,
  #[alias = "onratechange"]
  pub onratechange: EventHandler,
  #[alias = "onreset"]
  pub onreset: EventHandler,
  #[alias = "onresize"]
  pub onresize: EventHandler,
  #[alias = "onscroll"]
  pub onscroll: EventHandler,
  #[alias = "onsecuritypolicyviolation"]
  pub onsecuritypolicyviolation: EventHandler,
  #[alias = "onseeked"]
  pub onseeked: EventHandler,
  #[alias = "onseeking"]
  pub onseeking: EventHandler,
  #[alias = "onselect"]
  pub onselect: EventHandler,
  #[pref = "dom.menuitem.enabled"]
  #[alias = "onshow"]
  pub onshow: EventHandler,
  #[alias = "onslotchange"]
  pub onslotchange: EventHandler,
  //(implemented: Not)attribute EventHandler onsort;
  #[alias = "onstalled"]
  pub onstalled: EventHandler,
  #[alias = "onsubmit"]
  pub onsubmit: EventHandler,
  #[alias = "onsuspend"]
  pub onsuspend: EventHandler,
  #[alias = "ontimeupdate"]
  pub ontimeupdate: EventHandler,
  #[alias = "onvolumechange"]
  pub onvolumechange: EventHandler,
  #[alias = "onwaiting"]
  pub onwaiting: EventHandler,

  #[alias = "onselectstart"]
  pub onselectstart: EventHandler,
  #[alias = "onselectionchange"]
  pub onselectionchange: EventHandler,

  #[alias = "ontoggle"]
  pub ontoggle: EventHandler,

  // Pointer events handlers
  #[alias = "onpointercancel"]
  pub onpointercancel: EventHandler,
  #[alias = "onpointerdown"]
  pub onpointerdown: EventHandler,
  #[alias = "onpointerup"]
  pub onpointerup: EventHandler,
  #[alias = "onpointermove"]
  pub onpointermove: EventHandler,
  #[alias = "onpointerout"]
  pub onpointerout: EventHandler,
  #[alias = "onpointerover"]
  pub onpointerover: EventHandler,
  #[alias = "onpointerenter"]
  pub onpointerenter: EventHandler,
  #[alias = "onpointerleave"]
  pub onpointerleave: EventHandler,
  #[alias = "ongotpointercapture"]
  pub ongotpointercapture: EventHandler,
  #[alias = "onlostpointercapture"]
  pub onlostpointercapture: EventHandler,

  // Mozilla-specific handlers. Unprefixed handlers live in
  // Document rather than here.
  #[deprecated = "MozfullscreenchangeDeprecatedPrefix"]
  #[alias = "onmozfullscreenchange"]
  pub onmozfullscreenchange: EventHandler,
  #[deprecated = "MozfullscreenerrorDeprecatedPrefix"]
  #[alias = "onmozfullscreenerror"]
  pub onmozfullscreenerror: EventHandler,

  // CSS-Animation and CSS-Transition handlers.
  #[alias = "onanimationcancel"]
  pub onanimationcancel: EventHandler,
  #[alias = "onanimationend"]
  pub onanimationend: EventHandler,
  #[alias = "onanimationiteration"]
  pub onanimationiteration: EventHandler,
  #[alias = "onanimationstart"]
  pub onanimationstart: EventHandler,
  #[alias = "ontransitioncancel"]
  pub ontransitioncancel: EventHandler,
  #[alias = "ontransitionend"]
  pub ontransitionend: EventHandler,
  #[alias = "ontransitionrun"]
  pub ontransitionrun: EventHandler,
  #[alias = "ontransitionstart"]
  pub ontransitionstart: EventHandler,

  // CSS-Animation and CSS-Transition legacy handlers.
  // This handler isn't standard.
  #[binary_name = "onwebkitAnimationEnd"]
  #[alias = "onwebkitanimationend"]
  pub onwebkitanimationend: EventHandler,
  #[binary_name = "onwebkitAnimationIteration"]
  #[alias = "onwebkitanimationiteration"]
  pub onwebkitanimationiteration: EventHandler,
  #[binary_name = "onwebkitAnimationStart"]
  #[alias = "onwebkitanimationstart"]
  pub onwebkitanimationstart: EventHandler,
  #[binary_name = "onwebkitTransitionEnd"]
  #[alias = "onwebkittransitionend"]
  pub onwebkittransitionend: EventHandler,
}

pub trait mixin WindowEventHandlers {
  #[alias = "onafterprint"]
  pub on_after_print: EventHandler,
  #[alias = "onbeforeprint"]
  pub onbeforeprint: EventHandler,
  #[alias = "onbeforeunload"]
  pub onbeforeunload: OnBeforeUnloadEventHandler,
  #[alias = "onhashchange"]
  pub onhashchange: EventHandler,
  #[alias = "onlanguagechange"]
  pub on_language_change: EventHandler,
  #[alias = "onmessage"]
  pub on_message: EventHandler,
  #[alias = "onmessageerror"]
  pub on_messageerror: EventHandler,
  #[alias = "onoffline"]
  pub onoffline: EventHandler,
  #[alias = "ononline"]
  pub ononline: EventHandler,
  #[alias = "onpagehide"]
  pub onpagehide: EventHandler,
  #[alias = "onpageshow"]
  pub onpageshow: EventHandler,
  #[alias = "onpopstate"]
  pub onpopstate: EventHandler,
  #[alias = "onrejectionhandled"]
  pub onrejectionhandled: EventHandler,
  #[alias = "onstorage"]
  pub onstorage: EventHandler,
  #[alias = "onunhandledrejection"]
  pub onunhandledrejection: EventHandler,
  #[alias = "onunload"]
  pub on_unload: EventHandler,
}

// https://w3c.github.io/gamepad/#extensions-to-the-windoweventhandlers-trait-mixin
partial trait mixin WindowEventHandlers {
  #[alias = "ongamepadconnected"]
  pub ongamepadconnected: EventHandler,
  #[alias = "ongamepaddisconnected"]
  pub ongamepaddisconnected: EventHandler,
}

pub trait mixin DocumentAndElementEventHandlers {
  #[alias = "oncopy"]
  pub oncopy: EventHandler,
  #[alias = "oncut"]
  pub oncut: EventHandler,
  #[alias = "onpaste"]
  pub onpaste: EventHandler,
}

// The spec has |attribute OnErrorEventHandler onerror;| on
// GlobalEventHandlers, and calls the handler differently depending on
// whether an ErrorEvent was fired. We don't do that, and until we do we'll
// need to distinguish between onerror on Window or on nodes.

pub trait mixin OnErrorEventHandlerForNodes {
  #[alias = "onerror"]
  pub on_error: EventHandler,
}

pub trait mixin OnErrorEventHandlerForWindow {
  #[alias = "onerror"]
  pub on_error: OnErrorEventHandler,
}
