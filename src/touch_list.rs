// The origin of this IDL file is
// https://dvcs.w3.org/hg/webevents/raw-file/v1/touchevents.html

#[func = "mozilla::dom::TouchList::PrefEnabled",
 exposed = Window]
pub trait TouchList {
  #[pure]
  length: u32,
  getter Option<Touch> item(index: u32);
}
