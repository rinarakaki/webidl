#[chrome_only, exposed = Window]
pub trait MozApplicationEvent: Event
{
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: MozApplicationEventInit);

  application: Option<DOMApplication>,
}

pub struct MozApplicationEventInit: EventInit
{
  pub application: Option<DOMApplication> = None,
}
