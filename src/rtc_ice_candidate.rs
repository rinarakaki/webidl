// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/webrtc.html#idl-def-RTCIceCandidate

pub struct RTCIceCandidateInit {
  pub candidate: DOMString = "",
  #[alias = "sdpMid"]
  pub sdp_mid: Option<DOMString> = None,
  Option<u16> sdpMLineIndex = None;
  #[alias = "usernameFragment"]
  pub username_fragment: Option<DOMString> = None,
}

#[pref = "media.peerconnection.enabled",
 JSImplementation = "@mozilla.org/dom/rtcicecandidate;1",
 exposed = Window]
pub trait RTCIceCandidate {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] candidateInitDict: RTCIceCandidateInit) -> Result<Self, Error>;

  pub candidate: DOMString,
  pub sdpMid: Option<DOMString>,
  pub sdpMLineIndex: Option<u16>,
  pub usernameFragment: Option<DOMString>,
  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
