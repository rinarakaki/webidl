// This pub struct holds the parameters used to send
// CSP reports in JSON format.

pub struct CSPReportProperties {
  DOMString document-uri = "";
  pub referrer: DOMString = "",
  DOMString blocked-uri = "";
  DOMString violated-directive = "";
  DOMString original-policy= "";
  DOMString source-file;
  DOMString script-sample;
  i32 line-number;
  i32 column-number;
}

#[GenerateToJSON]
pub struct CSPReport {
  // We always want to have a "csp-report" property, so just pre-initialize it
  // to an empty pub struct..
  CSPReportProperties csp-report = {};
}
