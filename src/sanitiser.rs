// The origin of this IDL file is
// https://wicg.github.io/purification/

pub type SanitizerInput = (DocumentFragment or Document);
pub type AttributeMatchList = record<DOMString, Vec<DOMString>>;

#[exposed = Window, SecureContext, pref = "dom.security.sanitizer.enabled"]
#[alias = "Sanitizer"]
pub trait Sanitiser {
  #[throws, use_counter]
  #[alias = "constructor"]
  pub fn new(#[optional = {}] sanitiserConfig: SanitiserConfig) -> Self;

  #[use_counter, throws]
  #[alias = "sanitize"]
  pub fn sanitise(input: SanitizerInput) -> Result<DocumentFragment, Error>;

  #[use_counter, throws]
  #[alias = "sanitizeFor"]
  pub fn sanitise_for(element: DOMString, element: DOMString) -> Option<Element>;
}

#[alias = "SanitizerConfig"]
pub struct SanitiserConfig {
  #[alias = "allowElements"]
  pub allow_elements: Vec<DOMString>,
  #[alias = "blockElements"]
  pub block_elements: Vec<DOMString>,
  #[alias = "dropElements"]
  pub drop_elements: Vec<DOMString>,
  #[alias = "allowAttributes"]
  pub allow_attributes: AttributeMatchList,
  #[alias = "dropAttributes"]
  pub drop_attributes: AttributeMatchList,
  #[alias = "allowCustomElements"]
  pub allow_custom_elements: bool,
  #[alias = "allowComments"]
  pub allow_comments: bool,
}
