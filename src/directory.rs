// All functions on Directory that accept DOMString arguments for file or
// directory names only allow relative path to current directory itself. The
// path should be a descendent path like "path/to/file.txt" and not contain a
// segment of ".." or ".". So the paths aren't allowed to walk up the directory
// tree. For example, paths like "../foo", "..", "/foo/bar" or "foo/../bar" are
// not allowed.
//
// http://w3c.github.io/filesystem-api/#idl-def-Directory
// https://microsoftedge.github.io/directory-upload/proposal.html#directory-trait

#[exposed = (Window, Worker)]
pub trait Directory {
  // This chrome_only constructor is used by the MockFilePicker for testing only.
  #[chrome_only]
  #[alias = "constructor"]
  pub fn new(path: DOMString) -> Result<Self>;

  //
  // The leaf name of the directory.
  #[throws]
  name: DOMString,
}

#[exposed = (Window, Worker)]
partial trait Directory {
  // Already defined in the main trait declaration:
  //readonly attribute DOMString name;

  // The path of the Directory (includes both its basename and leafname).
  // The path begins with the name of the ancestor Directory that was
  // originally exposed to content (say via a directory picker) and traversed
  // to obtain this Directory. Full filesystem paths are not exposed to
  // unprivilaged content.
  #[throws]
  path: DOMString,

  // Getter for the immediate children of this directory.
  #[throws]
  pub fn getFilesAndDirectories() -> Promise<Vec<(File or Directory)>>;

  #[throws]
  pub fn getFiles(#[optional = false] recursiveFlag: bool) -> Promise<Vec<File>>;
}
