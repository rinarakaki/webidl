use super::{Blob, WritableStream};

pub enum WriteCommandType {
  #[alias = "write"]
  Write,
  #[alias = "seek"]
  Seek,
  #[alias = "truncate"]
  Truncate,
}

pub struct WriteParams {
  required WriteCommandType type;
  u32 Option<i32> size;  // TODO
  u32 Option<i32> position;
  (BufferSource or Blob or USVString)? data;
}

pub type FileSystemWriteChunkType = (BufferSource or Blob or USVString or WriteParams);

#[exposed = (Window, Worker), SecureContext, pref = "dom.fs.enabled"]
pub trait FileSystemWritableFileStream: WritableStream {
  pub fn write(data: FileSystemWriteChunkType) -> Promise<void>;
  pub fn seek(position: u64) -> Promise<void>;
  pub fn truncate(size: u64) -> Promise<void>;
}
