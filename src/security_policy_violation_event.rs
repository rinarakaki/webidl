pub enum SecurityPolicyViolationEventDisposition {
  "enforce", "report"
}

#[exposed = Window]
pub trait SecurityPolicyViolationEvent: Event {
  constructor(type: DOMString,
		#[optional = {}] eventInitDict: SecurityPolicyViolationEventInit);

  #[alias = "documentURI"]
  document_uri: DOMString,
  referrer: DOMString,
  #[alias = "blockedURI"]
  blocked_uri: DOMString,
  #[alias = "violatedDirective"]
  violated_directive: DOMString,
  #[alias = "effectiveDirective"]
  effective_directive: DOMString,
  #[alias = "originalPolicy"]
  original_policy: DOMString,
  #[alias = "sourceFile"]
  source_file: DOMString,
  sample: DOMString,
  disposition: SecurityPolicyViolationEventDisposition,
  #[alias = "statusCode"]
  status_code: u16,
  #[alias = "lineNumber"]
  line_number: i32,
  #[alias = "columnNumber"]
  column_number: i32,
}

#[GenerateInitFromJSON, GenerateToJSON]
pub struct SecurityPolicyViolationEventInit: EventInit {
  pub documentURI: DOMString = "",
  pub referrer: DOMString = "",
  pub blockedURI: DOMString = "",
  #[alias = "violatedDirective"]
  pub violated_directive: DOMString = "",
  #[alias = "effectiveDirective"]
  pub effective_directive: DOMString = "",
  #[alias = "originalPolicy"]
  pub original_policy: DOMString = "",
  #[alias = "sourceFile"]
  pub source_file: DOMString = "",
  pub sample: DOMString = "",
  pub disposition: SecurityPolicyViolationEventDisposition = "report",
  #[alias = "statusCode"]
  pub status_code: u16 = 0,
  #[alias = "lineNumber"]
  pub line_number: i32 = 0,
  #[alias = "columnNumber"]
  pub column_number: i32 = 0,
}
