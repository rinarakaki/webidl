// The origin of this IDL file is
// https://w3c.github.io/media-capabilities/

pub struct MediaConfiguration {
  pub video: VideoConfiguration,
  pub audio: AudioConfiguration,
}

pub struct MediaDecodingConfiguration: MediaConfiguration {
  required MediaDecodingType type;
}

pub struct MediaEncodingConfiguration: MediaConfiguration {
  required MediaEncodingType type;
}

pub enum MediaDecodingType {
  #[alias = "file"]
  File,
  #[alias = "media-source"]
  MediaSource,
}

pub enum MediaEncodingType {
  #[alias = "record"]
  Record,
  #[alias = "transmission"]
  Transmission
}

pub struct VideoConfiguration {
  required DOMString contentType;
  required u32 width;
  required u32 height;
  required u64 bitrate;
  required DOMString framerate;
  #[alias = "hasAlphaChannel"]
  pub has_alpha_channel: bool,
  #[alias = "hdrMetadataType"]
  pub hdr_metadata_type: HdrMetadataType,
  #[alias = "colorGamut"]
  pub colour_gamut: ColorGamut,
  #[alias = "transferfunction"]
  pub transfer_function: Transferfunction,
  #[alias = "scalabilityMode"]
  pub scalability_mode: DOMString,
}

#[alias = "HdrMetadataType"]
pub enum HDRMetadataType {
  #[alias = "smpteSt2086"]
  SMPTESt2086,
  "smpteSt2094-10",
  "smpteSt2094-40"
}

#[alias = "ColorGamut"]
pub enum ColourGamut {
  #[alias = "srgb"]
  SRGB,
  #[alias = "p3"]
  P3,
  #[alias = "rec2020"]
  Rec2020
}

pub enum Transferfunction {
  #[alias = "srgb"]
  SRGB,
  #[alias = "pq"]
  PQ,
  #[alias = "hlg"]
  HLG
}

pub struct AudioConfiguration {
  #[alias = "contentType"]
  required DOMString content_type;
  pub channels: DOMString,
  pub bitrate: u64,
  #[alias = "samplerate"]
  pub sample_rate: u32,
}

#[exposed = (Window, Worker),
  func = "mozilla::dom::MediaCapabilities::Enabled",
  HeaderFile = "mozilla/dom/MediaCapabilities.h"]
pub trait MediaCapabilitiesInfo {
  supported: bool,
  smooth: bool,
  #[alias = "powerEfficient"]
  power_efficient: bool,
}

#[exposed = (Window, Worker),
  func = "mozilla::dom::MediaCapabilities::Enabled"]
pub trait MediaCapabilities {
  #[new_object]
  #[alias = "decodingInfo"]
  pub fn decoding_info(configuration: MediaDecodingConfiguration)
    -> Promise<MediaCapabilitiesInfo>;

  #[new_object]
  #[alias = "encodingInfo"]
  pub fn encoding_info(configuration: MediaEncodingConfiguration)
    -> Promise<MediaCapabilitiesInfo>;
}
