// The origin of this IDL file is
// https://drafts.fxtf.org/geometry/

#[exposed = (Window, Worker), Serializable]
pub trait DOMPointReadOnly {
  constructor(
    #[optional = 0] f64: unrestricted x,
    #[optional = 0] f64: unrestricted y,
    #[optional = 0] f64: unrestricted z,
    #[optional = 1] f64: unrestricted w);

  #[new_object]
  static DOMPointReadOnly fromPoint(#[optional = {}] other: DOMPointInit);

  readonly attribute unrestricted f64 x;
  readonly attribute unrestricted f64 y;
  readonly attribute unrestricted f64 z;
  readonly attribute unrestricted f64 w;

  #[new_object, throws]
  #[alias = "matrixTransform"]
  pub fn matrix_transform(#[optional = {}] matrix: DOMMatrixInit) -> DOMPoint;

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}

#[exposed = (Window, Worker), Serializable]
pub trait DOMPoint: DOMPointReadOnly {
  constructor(
    #[optional = 0] f64: unrestricted x,
    #[optional = 0] f64: unrestricted y,
    #[optional = 0] f64: unrestricted z,
    #[optional = 1] f64: unrestricted w);

  #[new_object]
  static DOMPoint fromPoint(#[optional = {}] other: DOMPointInit);

  inherit attribute unrestricted f64 x;
  inherit attribute unrestricted f64 y;
  inherit attribute unrestricted f64 z;
  inherit attribute unrestricted f64 w;
}

pub struct DOMPointInit {
  unrestricted f64 x = 0;
  unrestricted f64 y = 0;
  unrestricted f64 z = 0;
  unrestricted f64 w = 1;
}
