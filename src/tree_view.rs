pub trait nsITreeSelection;

pub trait mixin TreeView {
  // The total number of rows in the tree (including the offscreen rows).
  #[alias = "rowCount"]
  row_count: i32,

  // The selection for this view.
  #[setter_throws]
  pub selection: Option<nsITreeSelection>,

  // A whitespace delimited list of properties. For each property X the view
  // gives back will cause the pseudoclasses ::-moz-tree-cell(x),
  // ::-moz-tree-row(x), ::-moz-tree-twisty(x), ::-moz-tree-image(x),
  // ::-moz-tree-cell-text(x). to be matched on the pseudoelement
  // ::moz-tree-row.
  #[throws]
  #[alias = "getRowProperties"]
  pub fn get_row_properties(row: i32) -> DOMString;

  // A whitespace delimited list of properties for a given cell. Each
  // property, x, that the view gives back will cause the pseudoclasses
  //  ::-moz-tree-cell(x), ::-moz-tree-row(x), ::-moz-tree-twisty(x),
  //  ::-moz-tree-image(x), ::-moz-tree-cell-text(x). to be matched on the
  //  cell.
  #[throws]
  #[alias = "getCellProperties"]
  pub fn get_cell_properties(row: i32, row: i32) -> DOMString;

  // Called to get properties to paint a column background. For shading the sort
  // column, etc.
  #[alias = "getColumnProperties"]
  pub fn get_column_properties(column: TreeColumn) -> DOMString;

  // Methods that can be used to test whether or not a twisty should be drawn,
  // and if so, whether an open or closed twisty should be used.
  #[throws]
  #[alias = "isContainer"]
  pub fn is_container(row: i32) -> bool;
  #[throws]
  #[alias = "isContainerOpen"]
  pub fn is_container_open(row: i32) -> bool;
  #[throws]
  #[alias = "isContainerEmpty"]
  pub fn is_container_empty(row: i32) -> bool;

  // isSeparator is used to determine if the row is a separator.
  // A value of true will result in the tree drawing a horizontal separator.
  // The tree uses the ::moz-tree-separator pseudoclass to draw the separator.
  #[throws]
  #[alias = "isSeparator"]
  pub fn is_separator(row: i32) -> bool;

  // Specifies if there is currently a sort on any column. Used mostly by dragdrop
  // to affect drop feedback.
  #[alias = "isSorted"]
  pub fn is_sorted() -> bool;

  const short DROP_BEFORE = -1;
  const short DROP_ON = 0;
  const short DROP_AFTER = 1;
  // Methods used by the drag feedback code to determine if a drag is allowable at
  // the current location. To get the behavior where drops are only allowed on
  // items, such as the mailNews folder pane, always return false when
  // the orientation is not DROP_ON.
  #[throws]
  #[alias = "canDrop"]
  pub fn can_drop(row: i32, row: i32, row: i32) -> bool;

  // Called when the user drops something on this view. The |orientation| param
  // specifies before/on/after the given |row|.
  #[throws]
  pub fn drop(row: i32, row: i32, row: i32);

  // Methods used by the tree to draw thread lines in the tree.
  // getParentIndex is used to obtain the index of a parent row.
  // If there is no parent row, getParentIndex returns -1.
  #[throws]
  #[alias = "getParentIndex"]
  pub fn get_parent_index(row: i32) -> i32;

  // hasNextSibling is used to determine if the row at rowIndex has a nextSibling
  // that occurs *after* the index specified by afterIndex. Code that is forced
  // to march down the view looking at levels can optimize the march by starting
  // at afterIndex+1.
  #[throws]
  #[alias = "hasNextSibling"]
  pub fn has_next_sibling(row: i32, row: i32) -> bool;

  // The level is an integer value that represents
  // the level of indentation. It is multiplied by the width specified in the
  // :moz-tree-indentation pseudoelement to compute the exact indendation.
  #[throws]
  #[alias = "getLevel"]
  pub fn get_level(row: i32) -> i32;

  // The image path for a given cell. For defining an icon for a cell.
  // If the empty string is returned, the :moz-tree-image pseudoelement
  // will be used.
  #[throws]
  #[alias = "getImageSrc"]
  pub fn get_image_src(row: i32, row: i32) -> DOMString;

  // The value for a given cell. This method is only called for columns
  // of type other than |text|.
  #[throws]
  #[alias = "getCellValue"]
  pub fn get_cell_value(row: i32, row: i32) -> DOMString;

  // The text for a given cell. If a column consists only of an image, then
  // the empty string is returned.
  #[throws]
  #[alias = "getCellText"]
  pub fn get_cell_text(row: i32, row: i32) -> DOMString;

  // Called during initialization to link the view to the front end box object.
  #[throws]
  #[alias = "setTree"]
  pub fn set_tree(tree: Option<XULTreeElement>);

  // Called on the view when an item is opened or closed.
  #[throws]
  #[alias = "toggleOpenState"]
  pub fn toggle_open_state(row: i32);

  // Called on the view when a header is clicked.
  #[throws]
  #[alias = "cycleHeader"]
  pub fn cycle_header(column: TreeColumn);

  // Should be called from a XUL onselect handler whenever the selection changes.
  #[alias = "selectionChanged"]
  pub fn selection_changed();

  // Called on the view when a cell in a non-selectable cycling column (e.g., unread/flag/etc.) is clicked.
  #[alias = "cycleCell"]
  pub fn cycle_cell(row: i32, row: i32);

  // isEditable is called to ask the view if the cell contents are editable.
  // A value of true will result in the tree popping up a text field when
  // the user tries to inline edit the cell.
  #[throws]
  #[alias = "isEditable"]
  pub fn is_editable(row: i32, row: i32) -> bool;

  // setCellValue is called when the value of the cell has been set by the user.
  // This method is only called for columns of type other than |text|.
  #[throws]
  #[alias = "setCellValue"]
  pub fn set_cell_value(row: i32, row: i32, row: i32);

  // setCellText is called when the contents of the cell have been edited by the user.
  #[throws]
  #[alias = "setCellText"]
  pub fn set_cell_text(row: i32, row: i32, row: i32);
}
