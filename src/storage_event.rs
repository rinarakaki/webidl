// Interface for a client side storage. See
// http://dev.w3.org/html5/webstorage/#the-storage-event
// for more information.
//
// Event sent to a window when a storage area changes.

#[exposed = Window]
pub trait StorageEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: StorageEventInit) -> Self;

  key: Option<DOMString>,
  #[alias = "oldValue"]
  old_value: Option<DOMString>,
  #[alias = "newValue"]
  new_value: Option<DOMString>,
  url: Option<DOMString>,
  #[alias = "storageArea"]
  storage_area: Option<Storage>,

  // Bug 1016053 - This is not spec compliant.
  void initStorageEvent(type: DOMString,
  #[optional = false] canBubble: bool,
  #[optional = false] cancelable: bool,
  optional Option<DOMString> key = None,
  optional Option<DOMString> oldValue = None,
  optional Option<DOMString> newValue = None,
  optional Option<DOMString> url = None,
  optional Option<Storage> storageArea = None);
}

pub struct StorageEventInit: EventInit {
  pub key: Option<DOMString> = None,
  #[alias = "oldValue"]
  pub old_value: Option<DOMString> = None,
  #[alias = "newValue"]
  pub new_value: Option<DOMString> = None,
  pub url: DOMString = "",
  #[alias = "storageArea"]
  pub storage_area: Option<Storage> = None,
}
