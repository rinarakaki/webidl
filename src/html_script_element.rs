// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-script-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

#[exposed = Window]
pub trait HTMLScriptElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws]
  pub src: DOMString,
  #[ce_reactions, setter_throws]
  pub type: DOMString,
  #[ce_reactions, setter_throws, pref = "dom.moduleScripts.enabled"]
  #[alias = "noModule"]
  pub no_module: bool,
  #[ce_reactions, setter_throws]
  pub charset: DOMString,
  #[ce_reactions, setter_throws]
  pub async: bool,
  #[ce_reactions, setter_throws]
  pub defer: bool,
  #[ce_reactions, setter_throws]
  pub crossOrigin: Option<DOMString>,
  #[ce_reactions, setter_throws]
  #[alias = "referrerPolicy"]
  pub referrer_policy: DOMString,
  #[ce_reactions, throws]
  pub text: DOMString,

  static bool supports(type: DOMString);
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLScriptElement {
  #[ce_reactions, setter_throws]
  pub event: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "htmlFor"]
  pub html_for: DOMString,
}

// https://w3c.github.io/webappsec/specs/subresourceintegrity/#htmlscriptelement-1
partial trait HTMLScriptElement {
  #[ce_reactions, setter_throws]
  pub integrity: DOMString,
}
