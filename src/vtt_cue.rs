// The origin of this IDL file is
// http://dev.w3.org/html5/webvtt/#the-vttcue-trait

pub enum AutoKeyword {
  #[alias = "auto"]
  Auto
}

pub enum LineAlignSetting {
  #[alias = "start"]
  Start,
  #[alias = "center"]
  Centre,
  #[alias = "end"]
  End
}

pub enum PositionAlignSetting {
  #[alias = "line-left"]
  LineLeft,
  #[alias = "center"]
  Centre,
  #[alias = "line-right"]
  LineRight,
  #[alias = "auto"]
  Auto
}

pub enum AlignSetting {
  #[alias = "start"]
  Start,
  #[alias = "center"]
  Centre,
  #[alias = "end"]
  End,
  #[alias = "left"]
  Left,
  #[alias = "right"]
  Right
}

pub enum DirectionSetting {
  "",
  #[alias = "rl"]
  RL,
  #[alias = "lr"]
  LR
}

#[exposed = Window]
pub trait VTTCue: TextTrackCue {
  #[alias = "constructor"]
  pub fn new(startTime: f64, startTime: f64, startTime: f64)
    -> Result<Self, Error>;

  #[pref = "media.webvtt.regions.enabled"]
  pub region: Option<VTTRegion>,
  pub vertical: DirectionSetting,
  #[alias = "snapToLines"]
  pub snap_to_lines: bool,
  attribute (f64 or AutoKeyword) line;
  #[setter_throws]
  #[alias = "lineAlign"]
  pub line_align: LineAlignSetting,
  #[setter_throws]
  attribute (f64 or AutoKeyword) position;
  #[setter_throws]
  #[alias = "positionAlign"]
  pub position_align: PositionAlignSetting,
  #[setter_throws]
  pub size: f64,
  pub align: AlignSetting,
  pub text: DOMString,

  #[alias = "getCueAsHTML"]
  pub fn get_cue_as_html() -> DocumentFragment;
}

// Mozilla extensions.
partial trait VTTCue {
  #[chrome_only]
  pub displayState: Option<HTMLDivElement>,
  #[chrome_only]
  #[alias = "hasBeenReset"]
  has_been_reset: bool,
  #[chrome_only]
  #[alias = "computedLine"]
  computed_line: f64,
  #[chrome_only]
  #[alias = "computedPosition"]
  computed_position: f64,
  #[chrome_only]
  #[alias = "computedPositionAlign"]
  computed_position_align: PositionAlignSetting,
  #[chrome_only]
  #[alias = "getActive"]
  get_active: bool,
}
