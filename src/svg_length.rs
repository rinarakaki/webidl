// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGLength {
  // Length Unit Types
  const u16 SVG_LENGTHTYPE_UNKNOWN = 0;
  const u16 SVG_LENGTHTYPE_NUMBER = 1;
  const u16 SVG_LENGTHTYPE_PERCENTAGE = 2;
  const u16 SVG_LENGTHTYPE_EMS = 3;
  const u16 SVG_LENGTHTYPE_EXS = 4;
  const u16 SVG_LENGTHTYPE_PX = 5;
  const u16 SVG_LENGTHTYPE_CM = 6;
  const u16 SVG_LENGTHTYPE_MM = 7;
  const u16 SVG_LENGTHTYPE_IN = 8;
  const u16 SVG_LENGTHTYPE_PT = 9;
  const u16 SVG_LENGTHTYPE_PC = 10;

  #[alias = "unitType"]
  unit_type: u16,
  #[throws]
  pub value: f32,
  #[setter_throws]
  #[alias = "valueInSpecifiedUnits"]
  pub value_in_specified_units: f32,
  #[setter_throws]
  #[alias = "valueAsString"]
  pub value_as_string: DOMString,

  #[throws]
  #[alias = "newValueSpecifiedUnits"]
  pub fn new_value_specified_units(unitType: u16, unitType: u16);
  #[throws]
  #[alias = "convertToSpecifiedUnits"]
  pub fn convert_to_specified_units(unitType: u16);
}
