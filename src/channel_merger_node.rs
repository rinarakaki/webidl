// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

pub struct ChannelMergerOptions: AudioNodeOptions {
  #[alias = "numberOfInputs"]
  pub #[optional = 6] number_of_inputs: u32
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait ChannelMergerNode: AudioNode {
  #[alias = constructor]
  pub fn new(
    context: BaseAudioContext,
    #[optional = {}] options: ChannelMergerOptions) -> Result<Self>;
}
