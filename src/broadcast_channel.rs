// For more information on this trait, please see
// https://html.spec.whatwg.org/#broadcastchannel

use super::*;

#[exposed = (Window, Worker)]
pub trait BroadcastChannel: EventTarget {
  #[alias = "constructor"]
  pub fn new(channel: DOMString) -> Result<Self>;

  pub name: DOMString,

  #[alias = "postMessage"]
  pub fn post_message(&self, message: any) -> Result<()>;

  pub fn close(&self);

  #[alias = "onmessage"]
  pub mut on_message: EventHandler,
  #[alias = "onmessageerror"]
  pub mut on_message_error: EventHandler,
}
