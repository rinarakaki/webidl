// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub enum OscillatorType {
  #[alias = "sine"]
  Sine,
  #[alias = "square"]
  Square,
  #[alias = "sawtooth"]
  Sawtooth,
  #[alias = "triangle"]
  Triangle,
  #[alias = "custom"]
  Custom
}

pub struct OscillatorOptions: AudioNodeOptions {
  pub type: OscillatorType = "sine",
  pub frequency: f32 = 440,
  pub detune: f32 = 0,
  #[alias = "periodicWave"]
  pub periodic_wave: PeriodicWave,
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait OscillatorNode: AudioScheduledSourceNode {
  #[throws]
  constructor(context: BaseAudioContext,
  #[optional = {}] options: OscillatorOptions);

  #[setter_throws]
  pub type: OscillatorType,

  frequency: AudioParam, // in Hertz
  detune: AudioParam, // in Cents

  #[alias = "setPeriodicWave"]
  pub fn set_periodic_wave(periodicWave: PeriodicWave);
}

// Mozilla extensions
OscillatorNode includes AudioNodePassThrough;
