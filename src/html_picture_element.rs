#[exposed = Window]
pub trait HTMLPictureElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

}
