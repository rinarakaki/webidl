// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLTableSectionElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  rows: HTMLCollection,
  #[throws]
  #[alias = "insertRow"]
  pub fn insert_row(#[optional = -1] index: i32) -> HTMLElement;
  #[ce_reactions, throws]
  #[alias = "deleteRow"]
  pub fn delete_row(index: i32);
}

partial trait HTMLTableSectionElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
  #[ce_reactions, setter_throws]
  pub ch: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "chOff"]
  pub ch_off: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "vAlign"]
  pub v_align: DOMString,
}
