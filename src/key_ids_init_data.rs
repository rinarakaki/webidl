// "KeyIds" EME init data format definition/parser, as defined by
// https://w3c.github.io/encrypted-media/format-registry/initdata/keyids.html
#[GenerateInitFromJSON]
pub struct KeyIdsInitData {
  required Vec<DOMString> kids;
}
