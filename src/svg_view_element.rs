// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGViewElement: SVGElement {}

SVGViewElement includes SVGFitToViewBox;
SVGViewElement includes SVGZoomAndPan;

