// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBFactory

pub trait Principal;

pub struct IDBOpenDBOptions
{
  #[EnforceRange]
  pub version: u64,
  pub storage: StorageType,
}

// Interface that defines the indexedDB property on a window. See
// http://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBFactory
// for more information.
#[exposed = (Window, Worker)]
pub trait IDBFactory {
  #[throws, needs_caller_type]
  IDBOpenDBRequest
  open(name: DOMString,
  #[EnforceRange]
  version: u64);

  #[throws, needs_caller_type]
  IDBOpenDBRequest
  open(name: DOMString,
  #[optional = {}] options: IDBOpenDBOptions);

  #[throws, needs_caller_type]
  IDBOpenDBRequest
  #[alias = "deleteDatabase"]
  delete_database(name: DOMString,
  #[optional = {}] options: IDBOpenDBOptions);

  #[throws]
  short
  cmp(first: any,
  second: any);

  #[throws, chrome_only, needs_caller_type]
  IDBOpenDBRequest
  #[alias = "openForPrincipal"]
  open_for_principal(principal: Principal,
  name: DOMString,
  #[EnforceRange]
  version: u64);

  #[throws, chrome_only, needs_caller_type]
  IDBOpenDBRequest
  #[alias = "openForPrincipal"]
  open_for_principal(principal: Principal,
  name: DOMString,
  #[optional = {}] options: IDBOpenDBOptions);

  #[throws, chrome_only, needs_caller_type]
  IDBOpenDBRequest
  #[alias = "deleteForPrincipal"]
  delete_for_principal(principal: Principal,
  name: DOMString,
  #[optional = {}] options: IDBOpenDBOptions);
}
