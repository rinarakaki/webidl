// This is a Mozilla-specific WebExtension API, which is not available to web
// content. It allows monitoring and filtering of HTTP response stream data.
//
// This API should currently be considered experimental, and is not defined by
// any standard.

pub enum StreamFilterStatus {
  // The StreamFilter is not fully initialized. No methods may be called until
  // a "start" event has been received.
  #[alias = "uninitialized"]
  Uninitialised,
  // The underlying channel is currently transferring data, which will be
  // dispatched via "data" events.
  #[alias = "transferringdata"]
  TransferringData,
  // The underlying channel has finished transferring data. Data may still be
  // written via write() calls at this point.
  #[alias = "finishedtransferringdata"]
  FinishedTransferringData,
  // Data transfer is currently suspended. It may be resumed by a call to
  // resume(). Data may still be written via write() calls in this state.
  #[alias = "suspended"]
  Suspended,
  // The channel has been closed by a call to close(). No further data wlil be
  // delivered via "data" events, and no further data may be written via
  // write() calls.
  #[alias = "closed"]
  Closed,
  // The channel has been disconnected by a call to disconnect(). All further
  // data will be delivered directly, without passing through the filter. No
  // further events will be dispatched, and no further data may be written by
  // write() calls.
  #[alias = "disconnected"]
  Disconnected,
  // An error has occurred and the channel is disconnected. The `error`
  // property contains the details of the error.
  #[alias = "failed"]
  Failed,
}

// An trait which allows an extension to intercept, and optionally modify,
// response data from an HTTP request.
#[exposed = Window,
 func = "mozilla::extensions::StreamFilter::IsAllowedInContext"]
pub trait StreamFilter: EventTarget {
  // Creates a stream filter for the given add-on and the given extension ID.
  #[chrome_only]
  static StreamFilter create(requestId: u64, requestId: u64);

  // Suspends processing of the request. After this is called, no further data
  // will be delivered until the request is resumed.
  #[throws]
  pub fn suspend();

  // Resumes delivery of data for a suspended request.
  #[throws]
  pub fn resume();

  // Closes the request. After this is called, no more data may be written to
  // the stream, and no further data will be delivered.
  //
  // This *must* be called after the consumer is finished writing data, unless
  // disconnect() has already been called.
  #[throws]
  pub fn close();

  // Disconnects the stream filter from the request. After this is called, no
  // further data will be delivered to the filter, and any unprocessed data
  // will be written directly to the output stream.
  #[throws]
  pub fn disconnect();

  // Writes a chunk of data to the output stream. This may not be called
  // before the "start" event has been received.
  #[throws]
  pub fn write((ArrayBuffer or Uint8Array) data);

  // Returns the current status of the stream.
  #[pure]
  status: StreamFilterStatus,

  // After an "error" event has been dispatched, this contains a message
  // describing the error.
  #[pure]
  error: DOMString,

  // Dispatched with a StreamFilterDataEvent whenever incoming data is
  // available on the stream. This data will not be delivered to the output
  // stream unless it is explicitly written via a write() call.
  #[alias = "ondata"]
  pub ondata: EventHandler,

  // Dispatched when the stream is opened, and is about to begin delivering
  // data.
  #[alias = "onstart"]
  pub onstart: EventHandler,

  // Dispatched when the stream has closed, and has no more data to deliver.
  // The output stream remains open and writable until close() is called.
  #[alias = "onstop"]
  pub onstop: EventHandler,

  // Dispatched when an error has occurred. No further data may be read or
  // written after this point.
  #[alias = "onerror"]
  pub on_error: EventHandler,
}
