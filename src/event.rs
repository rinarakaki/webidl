// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

use super::window::Window;
use super::worker::Worker;

#[exposed = (Window, Worker), ProbablyShortLivingWrapper]
pub trait Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: EventInit) -> Self;

  #[pure]
  type: DOMString,

  #[pure, BindingAlias = "srcElement"]
  target: Option<EventTarget>,

  #[pure]
  #[alias = "currentTarget"]
  current_target: Option<EventTarget>,

  #[alias = "composedPath"]
  pub fn composed_path() -> Vec<EventTarget>;

  const NONE: u16 = 0;
  const CAPTURING_PHASE: u16 = 1;
  const AT_TARGET: u16 = 2;
  const BUBBLING_PHASE: u16 = 3;

  #[pure]
  #[alias = "eventPhase"]
  event_phase: u16,

  #[alias = "stopPropagation"]
  pub fn stop_propagation();
  #[alias = "stopImmediatePropagation"]
  pub fn stop_immediate_propagation();

  #[pure]
  bubbles: bool,
  #[pure]
  cancelable: bool,
  #[needs_caller_type]
  #[alias = "returnValue"]
  pub return_value: bool,
  #[needs_caller_type]
  #[alias = "preventDefault"]
  pub fn prevent_default();
  #[pure, needs_caller_type]
  #[alias = "defaultPrevented"]
  default_prevented: bool,
  #[chrome_only, pure]
  #[alias = "defaultPreventedByChrome"]
  default_prevented_by_chrome: bool,
  #[chrome_only, pure]
  #[alias = "defaultPreventedByContent"]
  default_prevented_by_content: bool,
  #[pure]
  composed: bool,

  #[LegacyUnforgeable, pure]
  #[alias = "isTrusted"]
  is_trusted: bool,
  #[pure]
  #[alias = "timeStamp"]
  time_stamp: DOMHighResTimeStamp,

  void initEvent(type: DOMString,
  #[optional = false] bubbles: bool,
  #[optional = false] cancelable: bool);
  #[alias = "cancelBubble"]
  pub cancel_bubble: bool,
}

// Mozilla specific legacy stuff.
partial trait Event {
  const i32 ALT_MASK = 0x00000001;
  const i32 CONTROL_MASK = 0x00000002;
  const i32 SHIFT_MASK = 0x00000004;
  const i32 META_MASK = 0x00000008;

  //* The original target of the event, before any retargetings. readonly attribute Option<EventTarget> originalTarget;
  // The explicit original target of the event. If the event was retargeted
  // for some reason other than an anonymous boundary crossing, this will be set
  // to the target before the retargeting occurs. For example, mouse events
  // are retargeted to their parent node when they happen over text nodes (bug
  // 185889), and in that case .target will show the parent and
  // .explicitOriginalTarget will show the text node.
  // .explicitOriginalTarget differs from .originalTarget in that it will never
  // contain anonymous content.
  #[alias = "explicitOriginalTarget"]
  explicit_original_target: Option<EventTarget>,
  #[chrome_only]
  #[alias = "composedTarget"]
  composed_target: Option<EventTarget>,
  #[chrome_only]
  #[alias = "preventMultipleActions"]
  pub fn prevent_multiple_actions();
  #[chrome_only]
  #[alias = "multipleActionsPrevented"]
  multiple_actions_prevented: bool,
  #[chrome_only]
  #[alias = "isSynthesized"]
  is_synthesized: bool,
  // When the event target is a remote browser, calling this will fire an
  // reply event in the chrome process.
  #[chrome_only]
  #[alias = "requestReplyFromRemoteContent"]
  pub fn request_reply_from_remote_content();
  // Returns true when the event shouldn't be handled by chrome.
  #[chrome_only]
  #[alias = "isWaitingReplyFromRemoteContent"]
  is_waiting_reply_from_remote_content: bool,
  // Returns true when the event is a reply event from a remote process.
  #[chrome_only]
  #[alias = "isReplyEventFromRemoteContent"]
  is_reply_event_from_remote_content: bool,
}

pub struct EventInit {
  pub bubbles: bool = false,
  pub cancelable: bool = false,
  pub composed: bool = false,
}
