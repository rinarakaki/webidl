// The origin of this IDL file is
// https://drafts.csswg.org/web-animations/#animation

pub super::*;

pub enum AnimationPlayState {
  #[alias = "idle"]
  Idle,
  #[alias = "running"]
  Running,
  #[alias = "paused"]
  Paused,
  #[alias = "finished"]
  Finished
}

pub enum AnimationReplaceState {
  #[alias = "active"]
  Active,
  #[alias = "removed"]
  Removed,
  #[alias = "persisted"]
  Persisted
}

#[exposed = Window]
pub trait Animation: EventTarget {
  #[alias = constructor]
  pub fn new(
    #[optional = None] effect: Option<AnimationEffect>,
    #[optional] timeline: Option<AnimationTimeline>)
    -> Result<Self>;

  pub mut id: DOMString,
  #[func = "Document::IsWebAnimationsEnabled", pure]
  pub mut effect: Option<AnimationEffect>,
  #[func = "Document::AreWebAnimationsTimelinesEnabled"]
  // Animation.timeline setter is supported only on Nightly.
  #[cfg(NIGHTLY_BUILD)]
  pub mut timeline: Option<AnimationTimeline>,
  #[cfg(not(NIGHTLY_BUILD))]
  pub timeline: Option<AnimationTimeline>,

  #[alias = "startTime"]
  pub mut start_time: Option<f64>,
  #[setter_throws, alias = "currentTime"]
  pub mut current_time: Option<f64>,

  #[alias = "playbackRate"]
  pub mut playback_rate: f64,
  #[alias = "playState"]
  pub play_state: AnimationPlayState,
  #[binary_name = "pendingFromJS"]
  pub pending: bool,
  #[pref = "dom.animations-api.autoremove.enabled"]
  #[alias = "replaceState"]
  pub replace_state: AnimationReplaceState,
  #[func = "Document::IsWebAnimationsEnabled", throws]
  pub ready: Promise<Animation>,
  #[func = "Document::IsWebAnimationsEnabled", throws]
  pub finished: Promise<Animation>,
  #[alias = "onfinish"]
  pub mut on_finish: EventHandler,
  #[alias = "oncancel"]
  pub mut on_cancel: EventHandler,
  #[pref = "dom.animations-api.autoremove.enabled"]
  #[alias = "onremove"]
  pub mut on_remove: EventHandler,

  pub fn cancel(&self);
  pub fn finish(&self) -> Result<()>;
  pub fn play(&self) -> Result<()>;
  pub fn pause(&self) -> Result<()>;
  #[alias = "updatePlaybackRate"]
  pub fn update_playback_rate(&self, playback_rate: f64) -> Result<()>;
  pub fn reverse(&self) -> Result<()>;
  #[pref = "dom.animations-api.autoremove.enabled"]
  pub fn persist(&self);
  #[pref = "dom.animations-api.autoremove.enabled"]
  #[alias = "commitStyles"]
  pub fn commit_styles(&self) -> Result<()>;
}

// Non-standard extensions
partial trait Animation {
  #[chrome_only]
  #[alias = "isRunningOnCompositor"]
  pub is_running_on_compositor: bool,
}
