// The origin of this IDL file is
// https://dvcs.w3.org/hg/dap/raw-file/default/media-stream-capture/ImageCapture.html

#[pref = "dom.imagecapture.enabled",
 exposed = Window]
pub trait ImageCapture: EventTarget {
  #[alias = "constructor"]
  pub fn new(track: MediaStreamTrack) -> Result<Self, Error>;

  // readonly attribute PhotoSettingsOptions photoSettingsOptions;
  #[binary_name = "GetVideoStreamTrack"]
  #[alias = "videoStreamTrack"]
  video_stream_track: MediaStreamTrack,
  #[alias = "onphoto"]
  pub onphoto: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  // attribute EventHandler onphotosettingschange;
  // attribute EventHandler onframegrab;

  // #[throws]
  // void setOptions (photoSettings: Option<PhotoSettings>);
  #[throws]
  #[alias = "takePhoto"]
  pub fn take_photo();
  // #[throws]
  // void getFrame();
}
