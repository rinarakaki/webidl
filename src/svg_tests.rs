// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

pub trait mixin SVGTests {
  #[alias = "requiredExtensions"]
  required_extensions: SVGStringList,
  #[alias = "systemLanguage"]
  system_language: SVGStringList,
}

