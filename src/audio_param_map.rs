// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/#audioparammap

use super::*;

#[SecureContext, pref = "dom.audioworklet.enabled", exposed = Window]
pub trait AudioParamMap {
  pub maplike<DOMString, AudioParam>;
}
