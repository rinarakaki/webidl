// The origin of this IDL file is
// http://dev.w3.org/csswg/cssom/#the-linkstyle-trait

pub trait mixin LinkStyle {
  #[binary_name = "sheetForBindings"]
  sheet: Option<StyleSheet>,
}

