// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFEComponentTransferElement: SVGElement {
  #[constant]
  in1: SVGAnimatedString,
}

SVGFEComponentTransferElement includes SVGFilterPrimitiveStandardAttributes;
