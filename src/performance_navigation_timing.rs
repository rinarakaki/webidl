// The origin of this IDL file is
// https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming

pub enum NavigationType {
  #[alias = "navigate"]
  Navigate,
  #[alias = "reload"]
  Reload,
  #[alias = "back_forward"]
  BackForward,
  #[alias = "prerender"]
  Prerender
}

#[exposed = Window,
  func = "mozilla::dom::PerformanceNavigationTiming::Enabled"]
pub trait PerformanceNavigationTiming: PerformanceResourceTiming {
  #[alias = "unloadEventStart"]
  unload_event_start: DOMHighResTimeStamp,
  #[alias = "unloadEventEnd"]
  unload_event_end: DOMHighResTimeStamp,
  #[alias = "domInteractive"]
  dom_interactive: DOMHighResTimeStamp,
  #[alias = "domContentLoadedEventStart"]
  dom_content_loaded_event_start: DOMHighResTimeStamp,
  #[alias = "domContentLoadedEventEnd"]
  dom_content_loaded_event_end: DOMHighResTimeStamp,
  #[alias = "domComplete"]
  dom_complete: DOMHighResTimeStamp,
  #[alias = "loadEventStart"]
  load_event_start: DOMHighResTimeStamp,
  #[alias = "loadEventEnd"]
  load_event_end: DOMHighResTimeStamp,
  type: NavigationType,
  #[alias = "redirectCount"]
  redirect_count: u16,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
