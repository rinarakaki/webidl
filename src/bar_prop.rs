use super::*;

#[exposed = Window]
pub trait BarProp {
  #[throws, needs_caller_type]
  pub mut visible: bool,
}
