// This is an internal IDL file

use super::*;

#[chrome_only,
  JSImplementation = "@mozilla.org/dom/createofferrequest;1",
  exposed = Window]
pub trait CreateOfferRequest {
  #[alias = "windowID"]
  pub window_id: u64,
  #[alias = "innerWindowID"]
  pub inner_window_id: u64,
  #[alias = "callID"]
  pub call_id: DOMString,
  #[alias = "isSecure"]
  pub is_secure: bool,
}
