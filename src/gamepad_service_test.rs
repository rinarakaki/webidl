#[pref = "dom.gamepad.test.enabled",
 exposed = Window]
pub trait GamepadServiceTest {
  #[alias = "noMapping"]
  no_mapping: GamepadMappingType,
  #[alias = "standardMapping"]
  standard_mapping: GamepadMappingType,
  #[alias = "noHand"]
  no_hand: GamepadHand,
  #[alias = "leftHand"]
  left_hand: GamepadHand,
  #[alias = "rightHand"]
  right_hand: GamepadHand,

  #[throws]
  Promise<u32> addGamepad(id: DOMString,
  mapping: GamepadMappingType,
  hand: GamepadHand,
  u32 numButtons,
  u32 numAxes,
  u32 numHaptics,
  u32 numLightIndicator,
  u32 numTouchEvents);

  #[throws]
  Promise<u32> removeGamepad(index: u32);

  #[throws]
  Promise<u32> newButtonEvent(index: u32,
  button: u32,
  pressed: bool,
  touched: bool);

  #[throws]
  Promise<u32> newButtonValueEvent(index: u32,
  button: u32,
  pressed: bool,
  touched: bool,
  value: f64);

  #[throws]
  Promise<u32> newAxisMoveEvent(index: u32,
  axis: u32,
  value: f64);
  #[throws]
  Promise<u32> newPoseMove(index: u32,
  orient: Option<F32Array>,
  pos: Option<F32Array>,
  Option<F32Array> angVelocity,
  Option<F32Array> angAcceleration,
  Option<F32Array> linVelocity,
  Option<F32Array> linAcceleration);

  #[throws]
  Promise<u32> newTouch(index: u32, index: u32,
  u32 touchId, surfaceId: octet,
  position: F32Array, surfaceDimension: Option<F32Array>);
}
