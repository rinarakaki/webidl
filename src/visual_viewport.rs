// The origin of this IDL file is:
// https://wicg.github.io/visual-viewport/#the-visualviewport-trait

#[pref = "dom.visualviewport.enabled",
 exposed = Window]
pub trait VisualViewport: EventTarget {
  #[alias = "offsetLeft"]
  offset_left: f64,
  #[alias = "offsetTop"]
  offset_top: f64,

  #[alias = "pageLeft"]
  page_left: f64,
  #[alias = "pageTop"]
  page_top: f64,

  width: f64,
  height: f64,

  scale: f64,

  #[alias = "onresize"]
  pub onresize: EventHandler,
  #[alias = "onscroll"]
  pub onscroll: EventHandler,
}
