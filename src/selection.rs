// The origin of this IDL file is
// https://w3c.github.io/selection-api/#selection-trait

#[exposed = Window]
pub trait Selection {
  #[needs_caller_type]
  #[alias = "anchorNode"]
  anchor_node: Option<Node>,
  #[needs_caller_type]
  #[alias = "anchorOffset"]
  anchor_offset: u32,
  #[needs_caller_type]
  #[alias = "focusNode"]
  focus_node: Option<Node>,
  #[needs_caller_type]
  #[alias = "focusOffset"]
  focus_offset: u32,
  #[alias = "isCollapsed"]
  is_collapsed: bool,
  // Returns the number of ranges in the selection.
  #[alias = "rangeCount"]
  range_count: u32,
  type: DOMString,
  // Returns the range at the specified index. throws if the index is
  // out of range.
  #[throws]
  #[alias = "getRangeAt"]
  pub fn get_range_at(index: u32) -> Range;
  // Adds a range to the current selection.
  #[throws, binary_name = "addRangeJS"]
  #[alias = "addRange"]
  pub fn add_range(range: Range);
  // Removes a range from the current selection.
  #[throws, binary_name = "removeRangeAndUnselectFramesAndNotifyListeners"]
  #[alias = "removeRange"]
  pub fn remove_range(range: Range);
  // Removes all ranges from the current selection.
  #[throws]
  #[alias = "removeAllRanges"]
  pub fn remove_all_ranges();
  #[throws, binary_name = "RemoveAllRanges"]
  pub fn empty();
  #[throws, binary_name = "collapseJS"]
  pub fn collapse(node: Option<Node>, #[optional = 0] offset: u32);
  #[throws, binary_name = "collapseJS"]
  #[alias = "setPosition"]
  pub fn set_position(node: Option<Node>, #[optional = 0] offset: u32);
  #[throws, binary_name = "collapseToStartJS"]
  #[alias = "collapseToStart"]
  pub fn collapse_to_start();
  #[throws, binary_name = "collapseToEndJS"]
  #[alias = "collapseToEnd"]
  pub fn collapse_to_end();
  #[throws, binary_name = "extendJS"]
  pub fn extend(node: Node, #[optional = 0] offset: u32);
  #[throws, binary_name = "setBaseAndExtentJS"]
  void setBaseAndExtent(anchorNode: Node,
  u32 anchorOffset,
  Node focusNode,
  u32 focusOffset);
  #[throws, binary_name = "selectAllChildrenJS"]
  #[alias = "selectAllChildren"]
  pub fn select_all_children(node: Node);
  #[ce_reactions, throws]
  #[alias = "deleteFromDocument"]
  pub fn delete_from_document();
  #[throws]
  bool containsNode(node: Node,
  #[optional = false] allowPartialContainment: bool);
  stringifier DOMString ();
}

// Additional methods not currently in the spec
partial trait Selection {
  #[throws]
  void modify(alter: DOMString, alter: DOMString,
  granularity: DOMString);
}

// Additional chrome-only methods.
pub trait nsISelectionListener;
partial trait Selection {
  // A true value means "selection after newline"; false means "selection before
  // newline" when a selection is positioned "between lines".
  #[chrome_only,throws]
  #[alias = "interlinePosition"]
  pub interline_position: bool,

  #[throws]
  pub caretBidiLevel: Option<short>,

  #[chrome_only,throws]
  #[alias = "toStringWithFormat"]
  pub fn to_string_with_format(formatType: DOMString, formatType: DOMString, formatType: DOMString) -> DOMString;
  #[chrome_only]
  #[alias = "addSelectionListener"]
  pub fn add_selection_listener(newListener: nsISelectionListener);
  #[chrome_only]
  #[alias = "removeSelectionListener"]
  pub fn remove_selection_listener(listenerToRemove: nsISelectionListener);

  #[chrome_only,binary_name = "rawType"]
  #[alias = "selectionType"]
  selection_type: short,

  // Return array of ranges intersecting with the given DOM interval.
  #[chrome_only,throws,pref = "dom.testing.selection.GetRangesForInterval"]
  Vec<Range> GetRangesForInterval(beginNode: Node, beginNode: Node, beginNode: Node, beginNode: Node,
  bool allowAdjacent);

  // Scrolls a region of the selection, so that it is visible in
  // the scrolled view.
  //
  // @param aRegion the region inside the selection to scroll into view
  //                (see selection region constants defined in
  //                nsISelectionController).
  // @param aIsSynchronous when true, scrolls the selection into view
  //                       before returning. If false, posts a request which
  //                       is processed at some point after the method returns.
  // @param aVPercent how to align the frame vertically.
  // @param aHPercent how to align the frame horizontally.
  #[chrome_only,throws]
  #[alias = "scrollIntoView"]
  pub fn scroll_into_view(aRegion: short, aRegion: short, aRegion: short, aRegion: short);

  // setColors() sets custom colors for the selection.
  // Currently, this is supported only when the selection type is SELECTION_FIND.
  // Otherwise, throws an exception.
  //
  // @param aForegroundColor The foreground color of the selection.
  //                             If this is "currentColor", foreground color
  //                             isn't changed by this selection.
  // @param aBackgroundColor The background color of the selection.
  //                             If this is "transparent", background color is
  //                             never painted.
  // @param aAltForegroundColor The alternative foreground color of the
  //                             selection.
  //                             If aBackgroundColor doesn't have sufficient
  //                             contrast with its around or foreground color
  //                             if "currentColor" is specified, alternative
  //                             colors are used if it have higher contrast.
  // @param aAltBackgroundColor The alternative background color of the
  //                             selection.
  #[chrome_only,throws]
  void setColors(aForegroundColor: DOMString, aForegroundColor: DOMString,
  DOMString aAltForegroundColor, aAltBackgroundColor: DOMString);

  // resetColors() forget the customized colors which were set by setColors().
  #[chrome_only,throws]
  #[alias = "resetColors"]
  pub fn reset_colors();
}
