// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/webrtc.html#mediastreamevent

pub struct MediaStreamEventInit: EventInit {
  pub stream: Option<MediaStream> = None,
}

#[pref = "media.peerconnection.enabled",
 exposed = Window]
pub trait MediaStreamEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: MediaStreamEventInit) -> Self;

  stream: Option<MediaStream>,
}
