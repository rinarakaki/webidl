// The origin of this IDL file is
// https://drafts.csswg.org/css-conditional/#the-csssupportsrule-trait

// https://drafts.csswg.org/css-conditional/#the-csssupportsrule-trait
#[exposed = Window]
pub trait CSSSupportsRule: CSSConditionRule {
}
