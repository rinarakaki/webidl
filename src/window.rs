// The origin of this IDL file is:
// http://www.whatwg.org/specs/web-apps/current-work/
// https://dvcs.w3.org/hg/editing/raw-file/tip/editing.html
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html
// http://dev.w3.org/csswg/cssom/
// http://dev.w3.org/csswg/cssom-view/
// https://dvcs.w3.org/hg/webperf/raw-file/tip/specs/RequestAnimationFrame/Overview.html
// https://dvcs.w3.org/hg/webperf/raw-file/tip/specs/NavigationTiming/Overview.html
// https://dvcs.w3.org/hg/webcrypto-api/raw-file/tip/spec/Overview.html
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html
// https://w3c.github.io/webappsec-secure-contexts/#monkey-patching-global-object
// https://w3c.github.io/requestidlecallback/
// https://drafts.css-houdini.org/css-paint-api-1/#dom-window-paintworklet
// https://wicg.github.io/visual-viewport/#the-visualviewport-trait

pub trait Principal;
pub trait nsIBrowserDOMWindow;
pub trait XULControllers;
pub trait nsIDOMWindowUtils;
pub trait nsIPrintSettings;

pub type ApplicationCache = OfflineResourceList;

// http://www.whatwg.org/specs/web-apps/current-work/
#[Global, LegacyUnenumerableNamedProperties, NeedResolve,
 exposed = Window,
 InstrumentedProps = (AbsoluteOrientationSensor,
  Accelerometer,
  ApplicationCache,
  ApplicationCacheErrorEvent,
  Atomics,
  AudioParamMap,
  AudioWorklet,
  AudioWorkletNode,
  BackgroundFetchManager,
  BackgroundFetchRecord,
  BackgroundFetchRegistration,
  BeforeInstallPromptEvent,
  Bluetooth,
  BluetoothCharacteristicProperties,
  BluetoothDevice,
  BluetoothRemoteGATTCharacteristic,
  BluetoothRemoteGATTDescriptor,
  BluetoothRemoteGATTServer,
  BluetoothRemoteGATTService,
  BluetoothUUID,
  CanvasCaptureMediaStreamTrack,
  chrome,
  #[alias = "clientInformation"]
  client_information,
  ClipboardItem,
  CSSImageValue,
  CSSKeywordValue,
  CSSMathInvert,
  CSSMathMax,
  CSSMathMin,
  CSSMathNegate,
  CSSMathProduct,
  CSSMathSum,
  CSSMathValue,
  CSSMatrixComponent,
  CSSNumericArray,
  CSSNumericValue,
  CSSPerspective,
  CSSPositionValue,
  CSSRotate,
  CSSScale,
  CSSSkew,
  CSSSkewX,
  CSSSkewY,
  CSSStyleValue,
  CSSTransformComponent,
  CSSTransformValue,
  CSSTranslate,
  CSSUnitValue,
  CSSUnparsedValue,
  CSSVariableReferenceValue,
  #[alias = "defaultStatus"]
  default_status,
  // Unfortunately, our telemetry histogram name generator
  // (the one that generates TelemetryHistogramEnums.h) can't
  // handle two DOM methods with names that only differ in
  // case, because it forces everything to uppercase.
  //defaultstatus,
  DeviceMotionEventAcceleration,
  DeviceMotionEventRotationRate,
  DOMError,
  EnterPictureInPictureEvent,
  External,
  FederatedCredential,
  Gyroscope,
  HTMLContentElement,
  HTMLDialogElement,
  HTMLShadowElement,
  ImageCapture,
  InputDeviceCapabilities,
  InputDeviceInfo,
  Keyboard,
  KeyboardLayoutMap,
  LinearAccelerationSensor,
  Lock,
  LockManager,
  MediaMetadata,
  MediaSession,
  MediaSettingsRange,
  MIDIAccess,
  MIDIConnectionEvent,
  MIDIInput,
  MIDIInputMap,
  MIDIMessageEvent,
  MIDIOutput,
  MIDIOutputMap,
  MIDIPort,
  NavigationPreloadManager,
  NetworkInformation,
  #[alias = "offscreenBuffering"]
  offscreen_buffering,
  OffscreenCanvas,
  OffscreenCanvasRenderingContext2D,
  onbeforeinstallprompt,
  oncancel,
  ondeviceorientationabsolute,
  onmousewheel,
  onsearch,
  onselectionchange,
  #[alias = "openDatabase"]
  open_database,
  OrientationSensor,
  OverconstrainedError,
  PasswordCredential,
  PaymentAddress,
  PaymentInstruments,
  PaymentManager,
  PaymentMethodChangeEvent,
  PaymentRequest,
  PaymentRequestUpdateEvent,
  PaymentResponse,
  PerformanceEventTiming,
  PerformanceLongTaskTiming,
  PerformancePaintTiming,
  PhotoCapabilities,
  PictureInPictureWindow,
  Presentation,
  PresentationAvailability,
  PresentationConnection,
  PresentationConnectionAvailableEvent,
  PresentationConnectionCloseEvent,
  PresentationConnectionList,
  PresentationReceiver,
  PresentationRequest,
  RelativeOrientationSensor,
  RemotePlayback,
  ReportingObserver,
  RTCDtlsTransport,
  RTCError,
  RTCErrorEvent,
  RTCIceTransport,
  RTCSctpTransport,
  Sensor,
  SensorErrorEvent,
  SharedArrayBuffer,
  #[alias = "styleMedia"]
  style_media,
  StylePropertyMap,
  StylePropertyMapReadOnly,
  SVGDiscardElement,
  SyncManager,
  TaskAttributionTiming,
  TextDecoderStream,
  TextEncoderStream,
  TextEvent,
  Touch,
  TouchEvent,
  TouchList,
  TransformStream,
  USB,
  USBAlternateInterface,
  USBConfiguration,
  USBConnectionEvent,
  USBDevice,
  USBEndpoint,
  USBInterface,
  USBInTransferResult,
  USBIsochronousInTransferPacket,
  USBIsochronousInTransferResult,
  USBIsochronousOutTransferPacket,
  USBIsochronousOutTransferResult,
  USBOutTransferResult,
  UserActivation,
  #[alias = "visualViewport"]
  visual_viewport,
  #[alias = "webkitCancelAnimationFrame"]
  webkit_cancel_animation_frame,
  #[alias = "webkitMediaStream"]
  webkit_media_stream,
  WebKitMutationObserver,
  #[alias = "webkitRequestAnimationFrame"]
  webkit_request_animation_frame,
  #[alias = "webkitRequestFileSystem"]
  webkit_request_file_system,
  #[alias = "webkitResolveLocalFileSystemURL"]
  webkit_resolve_local_file_system_url,
  #[alias = "webkitRTCPeerConnection"]
  webkit_rtc_peer_connection,
  #[alias = "webkitSpeechGrammar"]
  webkit_speech_grammar,
  #[alias = "webkitSpeechGrammarList"]
  webkit_speech_grammar_list,
  #[alias = "webkitSpeechRecognition"]
  webkit_speech_recognition,
  #[alias = "webkitSpeechRecognitionError"]
  webkit_speech_recognition_error,
  #[alias = "webkitSpeechRecognitionEvent"]
  webkit_speech_recognition_event,
  #[alias = "webkitStorageInfo"]
  webkit_storage_info,
  Worklet,
  WritableStream)]
//sealed*/ trait Window: EventTarget {
  // the current browsing context
  [LegacyUnforgeable, constant, StoreInSlot, CrossOriginReadable]
  readonly attribute WindowProxy window;
  #[replaceable, constant, StoreInSlot, CrossOriginReadable]
  readonly attribute WindowProxy self;
  #[LegacyUnforgeable, StoreInSlot, pure]
  document: Option<Document>,
  #[throws]
  pub name: DOMString,
  #[put_forwards = href, LegacyUnforgeable, CrossOriginReadable,
  CrossOriginWritable]
  readonly attribute Location location;
  #[throws]
  history: History,
  #[alias = "customElements"]
  custom_elements: CustomElementRegistry,
  #[replaceable, throws]
  locationbar: BarProp,
  #[replaceable, throws]
  menubar: BarProp,
  #[replaceable, throws]
  personalbar: BarProp,
  #[replaceable, throws]
  scrollbars: BarProp,
  #[replaceable, throws]
  statusbar: BarProp,
  #[replaceable, throws]
  toolbar: BarProp,
  #[throws]
  pub status: DOMString,
  #[throws, CrossOriginCallable, needs_caller_type]
  pub fn close(&self);
  #[throws, CrossOriginReadable]
  closed: bool,
  #[throws]
  pub fn stop(&self);
  #[throws, CrossOriginCallable, needs_caller_type]
  pub fn focus(&self);
  #[throws, CrossOriginCallable, needs_caller_type]
  pub fn blur(&self);
  #[replaceable, pref = "dom.window.event.enabled"]
  event: any,

  // other browsing contexts
  #[replaceable, throws, CrossOriginReadable]
  frames: WindowProxy,
  #[replaceable, CrossOriginReadable]
  length: u32,
  //#[Unforgeable, throws, CrossOriginReadable] readonly attribute WindowProxy top;
  #[LegacyUnforgeable, throws, CrossOriginReadable]
  top: Option<WindowProxy>,
  #[throws, CrossOriginReadable]
  pub opener: any,
  //#[throws] readonly attribute WindowProxy parent;
  #[replaceable, throws, CrossOriginReadable]
  parent: Option<WindowProxy>,
  #[throws, needs_subject_principal]
  #[alias = "frameElement"]
  frame_element: Option<Element>,
  //#[throws] Option<WindowProxy> open(#[optional = "about:blank"] url: USVString, #[optional = "_blank"] target: DOMString, [TreatNullAs = EmptyString] #[optional = ""] features: DOMString);
  #[throws]
  pub features: DOMString = ""),
  getter object (name: DOMString);

  // the user agent
  navigator: Navigator,
  #[pref = "dom.window.clientinformation.enabled", binary_name = "Navigator"]
  #[alias = "clientInformation"]
  client_information: Navigator,

  #[replaceable, throws]
  external: External,
  #[throws, SecureContext, pref = "browser.cache.offline.enable"]
  #[alias = "applicationCache"]
  application_cache: ApplicationCache,

  // user prompts
  #[throws, needs_subject_principal]
  pub fn alert(&self);
  #[throws, needs_subject_principal]
  pub fn alert(&self, message: DOMString);
  #[throws, needs_subject_principal]
  pub fn confirm(&self, #[optional = ""] message: DOMString) -> bool;
  #[throws, needs_subject_principal]
  pub fn prompt(&self, #[optional = ""] message: DOMString, #[optional = ""] default: DOMString) -> Option<DOMString>;
  #[throws, pref = "dom.enable_window_print"]
  pub fn print(&self);

  // Returns a window that you can use for a print preview.
  //
  // This may reuse an existing window if this window is already a print
  // preview document, or if you pass a docshell explicitly.
  #[throws, func = "nsContentUtils::IsCallerChromeOrFuzzingEnabled"]
  Option<WindowProxy> printPreview(optional Option<nsIPrintSettings> settings = None,
  optional Option<nsIWebProgressListener> listener = None,
  optional Option<nsIDocShell> docShellToPreviewInto = None);

  #[throws, CrossOriginCallable, needs_subject_principal,
  binary_name = "postMessageMoz"]
  #[alias = "postMessage"]
  pub fn post_message(&self, message: any, message: any, optional Vec<object> transfer = []);

  #[throws, CrossOriginCallable, needs_subject_principal,
  binary_name = "postMessageMoz"]
  #[alias = "postMessage"]
  pub fn post_message(&self, message: any, #[optional = {}] options: WindowPostMessageOptions);

  // also has obsolete members
}

Window includes GlobalEventHandlers;
Window includes WindowEventHandlers;

// http://www.whatwg.org/specs/web-apps/current-work/
pub trait mixin WindowSessionStorage {
  //#[throws] readonly attribute Storage sessionStorage;
  #[throws]
  #[alias = "sessionStorage"]
  session_storage: Option<Storage>,
}

Window includes WindowSessionStorage;

// http://www.whatwg.org/specs/web-apps/current-work/
pub trait mixin WindowLocalStorage {
  #[throws]
  #[alias = "localStorage"]
  local_storage: Option<Storage>,
}

Window includes WindowLocalStorage;

// http://www.whatwg.org/specs/web-apps/current-work/
partial trait Window {
  #[alias = "captureEvents"]
  pub fn capture_events(&self);
  #[alias = "releaseEvents"]
  pub fn release_events(&self);
}

// https://dvcs.w3.org/hg/editing/raw-file/tip/editing.html
partial trait Window {
  #[alias = "getSelection"]
  pub fn get_selection(&self) -> Result<Option<Selection>>;
}

// https://drafts.csswg.org/cssom/#extensions-to-the-window-trait
partial trait Window {
  // #[new_object, throws] CSSStyleDeclaration getComputedStyle(elt: Element, optional Option<DOMString> pseudoElt = "");
  #[new_object]
  #[alias = "getComputedStyle"]
  pub fn get_computed_style(&self, 
    elt: Element, optional Option<DOMString> pseudoElt = "")
    -> Result<Option<CSSStyleDeclaration>>;
}

// http://dev.w3.org/csswg/cssom-view/
#[alias = "ScrollBehavior"]
pub enum ScrollBehaviour {
  #[alias = "auto"]
  Auto,
  #[alias = "instant"]
  Instant,
  #[alias = "smooth"]
  Smooth
}

pub struct ScrollOptions {
  #[alias = "behavior"]
  pub behaviour: ScrollBehaviour = "auto",
}

pub struct ScrollToOptions: ScrollOptions {
  unrestricted f64 left;
  unrestricted f64 top;
}

partial trait Window {
  // #[throws, new_object, needs_caller_type] MediaQueryList matchMedia(query: DOMString);
  #[new_object, needs_caller_type]
  #[alias = "matchMedia"]
  pub fn match_media(&self, query: UTF8String) -> Result<Option<MediaQueryList>>;
  // Per spec, screen is same_object, but we don't actually guarantee that given
  // nsGlobalWindow::Cleanup. :(
  //#[same_object, replaceable, throws] readonly attribute Screen screen;
  #[replaceable, throws]
  screen: Screen,

  // Browsing context
  //#[throws] void moveTo(x: f64, x: f64);
  //#[throws] void moveBy(x: f64, x: f64);
  //#[throws] void resizeTo(x: f64, x: f64);
  //#[throws] void resizeBy(x: f64, x: f64);
  #[needs_caller_type]
  #[alias = "moveTo"]
  pub fn move_to(&self, x: i32, x: i32) -> Result<()>;
  #[needs_caller_type]
  #[alias = "moveBy"]
  pub fn move_by(&self, x: i32, x: i32) -> Result<()>;
  #[needs_caller_type]
  #[alias = "resizeTo"]
  pub fn resize_to(&self, x: i32, x: i32) -> Result<()>;
  #[needs_caller_type]
  #[alias = "resizeBy"]
  pub fn resize_by(&self, x: i32, x: i32) -> Result<()>;

  // viewport
  // These are writable because we allow chrome to write them. And they need
  // to use 'any' as the type, because non-chrome writing them needs to act
  // like a #[replaceable] attribute would, which needs the original JS value.
  //#[replaceable, throws] readonly attribute f64 innerWidth;
  //#[replaceable, throws] readonly attribute f64 innerHeight;
  #[throws, needs_caller_type]
  #[alias = "innerWidth"]
  pub inner_width: any,
  #[throws, needs_caller_type]
  #[alias = "innerHeight"]
  pub inner_height: any,

  // viewport scrolling
  pub fn scroll(&self, unrestricted f64 x, unrestricted f64 y);
  pub fn scroll(&self, #[optional = {}] options: ScrollToOptions);
  #[alias = "scrollTo"]
  pub fn scroll_to(&self, unrestricted f64 x, unrestricted f64 y);
  #[alias = "scrollTo"]
  pub fn scroll_to(&self, #[optional = {}] options: ScrollToOptions);
  #[alias = "scrollBy"]
  pub fn scroll_by(&self, unrestricted f64 x, unrestricted f64 y);
  #[alias = "scrollBy"]
  pub fn scroll_by(&self, #[optional = {}] options: ScrollToOptions);
  // mozScrollSnap is used by chrome to perform scroll snapping after the
  // user performs actions that may affect scroll position
  // mozScrollSnap is deprecated, to be replaced by a web accessible API, such
  // as an extension to the ScrollOptions pub struct. See bug 1137937.
  #[chrome_only]
  #[alias = "mozScrollSnap"]
  pub fn moz_scroll_snap(&self);
  // The four properties below are f64 per spec at the moment, but whether
  // that will continue is unclear.
  #[replaceable, throws]
  scrollX: f64,
  #[replaceable, throws]
  #[alias = "pageXOffset"]
  page_x_offset: f64,
  #[replaceable, throws]
  scrollY: f64,
  #[replaceable, throws]
  #[alias = "pageYOffset"]
  page_y_offset: f64,

  // Aliases for screenX / screenY.
  #[replaceable, throws, needs_caller_type]
  #[alias = "screenLeft"]
  screen_left: f64,
  #[replaceable, throws, needs_caller_type]
  #[alias = "screenTop"]
  screen_top: f64,

  // client
  // These are writable because we allow chrome to write them. And they need
  // to use 'any' as the type, because non-chrome writing them needs to act
  // like a #[replaceable] attribute would, which needs the original JS value.
  // #[replaceable, throws] readonly attribute f64 screenX;
  // #[replaceable, throws] readonly attribute f64 screenY;
  // #[replaceable, throws] readonly attribute f64 outerWidth;
  // #[replaceable, throws] readonly attribute f64 outerHeight;
  #[throws, needs_caller_type]
  pub screenX: any,
  #[throws, needs_caller_type]
  pub screenY: any,
  #[throws, needs_caller_type]
  #[alias = "outerWidth"]
  pub outer_width: any,
  #[throws, needs_caller_type]
  #[alias = "outerHeight"]
  pub outer_height: any,
}

// https://html.spec.whatwg.org/multipage/imagebitmap-and-animations.html#animation-frames
Window includes AnimationFrameProvider;

// https://dvcs.w3.org/hg/webperf/raw-file/tip/specs/NavigationTiming/Overview.html
partial trait Window {
  #[replaceable, pure, StoreInSlot]
  performance: Option<Performance>,
}

// https://dvcs.w3.org/hg/webcrypto-api/raw-file/tip/spec/Overview.html
Window includes GlobalCrypto;

// https://fidoalliance.org/specifications/download/
Window includes GlobalU2F;

#ifdef MOZ_WEBSPEECH
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html
pub trait mixin SpeechSynthesisGetter {
  #[throws, pref = "media.webspeech.synth.enabled"]
  #[alias = "speechSynthesis"]
  speech_synthesis: SpeechSynthesis,
}

Window includes SpeechSynthesisGetter;
#endif

// Mozilla-specific stuff
partial trait Window {
  //#[new_object, throws] CSSStyleDeclaration getDefaultComputedStyle(elt: Element, #[optional = ""] pseudoElt: DOMString);
  #[new_object, throws]
  #[alias = "getDefaultComputedStyle"]
  pub fn get_default_computed_style(&self, elt: Element, #[optional = ""] pseudoElt: DOMString) -> Option<CSSStyleDeclaration>;

  // Mozilla extensions
  // Method for scrolling this window by a number of lines.
  #[alias = "scrollByLines"]
  pub fn scroll_by_lines(&self, numLines: i32, #[optional = {}] options: ScrollOptions);

  // Method for scrolling this window by a number of pages.
  #[alias = "scrollByPages"]
  pub fn scroll_by_pages(&self, numPages: i32, #[optional = {}] options: ScrollOptions);

  // Method for sizing this window to the content in the window.
  #[throws, needs_caller_type]
  #[alias = "sizeToContent"]
  pub fn size_to_content(&self);

  // XXX Shouldn't this be in Option<nsIDOMChromeWindow>
  #[chrome_only, replaceable, throws]
  controllers: XULControllers,

  #[chrome_only, throws]
  #[alias = "realFrameElement"]
  real_frame_element: Option<Element>,

  #[chrome_only]
  #[alias = "docShell"]
  doc_shell: Option<nsIDocShell>,

  #[chrome_only, constant, CrossOriginReadable, binary_name = "getBrowsingContext"]
  #[alias = "browsingContext"]
  browsing_context: BrowsingContext,

  #[throws, needs_caller_type]
  #[alias = "mozInnerScreenX"]
  moz_inner_screen_x: f32,
  #[throws, needs_caller_type]
  #[alias = "mozInnerScreenY"]
  moz_inner_screen_y: f32,
  #[replaceable, throws, needs_caller_type]
  #[alias = "devicePixelRatio"]
  device_pixel_ratio: f64,

  // The maximum offset that the window can be scrolled to
  (i.e., the document width/height minus the scrollport width/height) #[chrome_only, throws]
  #[alias = "scrollMinX"]
  scroll_min_x: i32,
  #[chrome_only, throws]
  #[alias = "scrollMinY"]
  scroll_min_y: i32,
  #[replaceable, throws]
  #[alias = "scrollMaxX"]
  scroll_max_x: i32,
  #[replaceable, throws]
  #[alias = "scrollMaxY"]
  scroll_max_y: i32,

  #[throws]
  #[alias = "fullScreen"]
  pub full_screen: bool,

  // XXX Should this be in Option<nsIDOMChromeWindow>
  void updateCommands(action: DOMString,
  optional Option<Selection> sel = None,
  #[optional = 0] reason: short);

  // Find in page.
  // @param str: the search pattern
  // @param caseSensitive: is the search caseSensitive
  // @param backwards: should we search backwards
  // @param wrapAround: should we wrap the search
  // @param wholeWord: should we search only for whole words
  // @param searchInFrames: should we search through all frames
  // @param showDialog: should we show the Find dialog
  #[throws]
  bool find(#[optional = ""] str: DOMString,
  #[optional = false] caseSensitive: bool,
  #[optional = false] backwards: bool,
  #[optional = false] wrapAround: bool,
  #[optional = false] wholeWord: bool,
  #[optional = false] searchInFrames: bool,
  #[optional = false] showDialog: bool);

  // Returns the number of times this document for this window has
  // been painted to the screen.
  //
  // If you need this for tests use nsIDOMWindowUtils.paintCount instead.
  #[throws, pref = "dom.mozPaintCount.enabled"]
  #[alias = "mozPaintCount"]
  moz_paint_count: u64,

  #[alias = "ondevicemotion"]
  pub ondevicemotion: EventHandler,
  #[alias = "ondeviceorientation"]
  pub ondeviceorientation: EventHandler,
  #[alias = "onabsolutedeviceorientation"]
  pub onabsolutedeviceorientation: EventHandler,
  #[pref = "device.sensors.proximity.enabled"]
  #[alias = "onuserproximity"]
  pub onuserproximity: EventHandler,
  #[pref = "device.sensors.ambientLight.enabled"]
  #[alias = "ondevicelight"]
  pub ondevicelight: EventHandler,

  pub fn dump(&self, str: DOMString);

  // This method is here for backwards compatibility with 4.x only,
  // its implementation is a no-op
  #[alias = "setResizable"]
  pub fn set_resizable(&self, resizable: bool);

  // This is the scriptable version of
  // nsPIDOMWindow::OpenDialog() that takes 3 optional
  // arguments, plus any additional arguments are passed on as
  // arguments on the dialog's window object (window.arguments).
  #[throws, chrome_only]
  Option<WindowProxy> openDialog(#[optional = ""] url: DOMString,
  #[optional = ""] name: DOMString,
  #[optional = ""] options: DOMString,
  any... extraArguments);

  #[func = "nsGlobalWindowInner::ContentPropertyEnabled",
  NonEnumerable, replaceable, throws, needs_caller_type]
  content: Option<object>,

  #[throws, chrome_only]
  #[alias = "getInterface"]
  pub fn get_interface(&self, iid: any) -> any;

  // Same as nsIDOMWindow.windowRoot, useful for event listener targeting.
  #[chrome_only, throws]
  #[alias = "windowRoot"]
  window_root: Option<WindowRoot>,

  // chrome_only method to determine if a particular window should see console
  // reports from service workers of the given scope.
  #[chrome_only]
  #[alias = "shouldReportForServiceWorkerScope"]
  pub fn should_report_for_service_worker_scope(&self, aScope: USVString) -> bool;

  // InstallTrigger is used for extension installs. Ideally it would
  // be something like a WebIDL namespace, but we don't support
  // JS-implemented static things yet. See bug 863952.
  #[replaceable]
  InstallTrigger: Option<InstallTriggerImpl>,

  // Get the nsIDOMWindowUtils for this window.
  #[constant, throws, chrome_only]
  #[alias = "windowUtils"]
  window_utils: nsIDOMWindowUtils,

  #[pure, chrome_only]
  #[alias = "windowGlobalChild"]
  window_global_child: Option<WindowGlobalChild>,

  // The principal of the client source of the window. This is supposed to be
  // used for the service worker.
  //
  // This is used for APIs like https://w3c.github.io/push-api/ that extend
  // ServiceWorkerRegistration and therefore need to operate consistently with
  // ServiceWorkers and its Clients API. The client principal is the appropriate
  // principal to pass to all nsIServiceWorkerManager APIs.
  //
  // Note that the client principal will be different from the node principal of
  // the window's document if the window is in a third-party context when dFPI
  // is enabled. In this case, the client principal will be the partitioned
  // principal to support the service worker partitioning.
  #[chrome_only]
  #[alias = "clientPrincipal"]
  client_principal: Option<Principal>,
}

Window includes TouchEventHandlers;

Window includes OnErrorEventHandlerForWindow;

#if defined(MOZ_WIDGET_ANDROID)
// https://compat.spec.whatwg.org/#windoworientation-trait
partial trait Window {
  #[needs_caller_type]
  orientation: short,
  #[alias = "onorientationchange"]
  pub on_orientation_change: EventHandler,
}
#endif

// Mozilla extension
// Sidebar is deprecated and it will be removed in the next cycles. See bug 1640138.
partial trait Window {
  #[replaceable, throws, use_counter, pref = "dom.window.sidebar.enabled"]
  readonly attribute (External or WindowProxy) sidebar;
}

#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
callback PromiseDocumentFlushedCallback = any ();

// Mozilla extensions for Chrome windows.
partial trait Window {
  // The STATE_* constants need to match the corresponding enum in nsGlobalWindow.cpp.
  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  const u16 STATE_MAXIMIZED = 1;
  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  const u16 STATE_MINIMIZED = 2;
  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  const u16 STATE_NORMAL = 3;
  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  const u16 STATE_FULLSCREEN = 4;

  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "windowState"]
  window_state: u16,

  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "isFullyOccluded"]
  is_fully_occluded: bool,

  // browserDOMWindow provides access to yet another layer of
  // utility functions implemented by chrome script. It will be None
  // for DOMWindows not corresponding to browsers.
  #[throws, func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  pub browserDOMWindow: Option<nsIBrowserDOMWindow>,

  #[throws, func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "getAttention"]
  pub fn get_attention(&self);

  #[throws, func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "getAttentionWithCycleCount"]
  pub fn get_attention_with_cycle_count(&self, aCycleCount: i32);

  #[throws, func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "setCursor"]
  pub fn set_cursor(&self, cursor: UTF8String);

  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  pub fn maximize(&self);
  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  pub fn minimize(&self);
  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  pub fn restore(&self);
  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "getWorkspaceID"]
  pub fn get_workspace_id(&self) -> DOMString;
  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "moveToWorkspace"]
  pub fn move_to_workspace(&self, workspaceID: DOMString);

  // Notify a default button is loaded on a dialog or a wizard.
  // defaultButton is the default button.
  #[throws, func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "notifyDefaultButtonLoaded"]
  pub fn notify_default_button_loaded(&self, defaultButton: Element);

  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "messageManager"]
  message_manager: ChromeMessageBroadcaster,

  // Returns the message manager identified by the given group name that
  // manages all frame loaders belonging to that group.
  #[func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "getGroupMessageManager"]
  pub fn get_group_message_manager(&self, aGroup: DOMString) -> ChromeMessageBroadcaster;

  // Calls the given function as soon as a style or layout flush for the
  // top-level document is not necessary, and returns a Promise which
  // resolves to the callback's return value after it executes.
  //
  // In the event that the window goes away before a flush can occur, the
  // callback will still be called and the Promise resolved as the window
  // tears itself down.
  //
  // The callback _must not modify the DOM for any window in any way_. If it
  // does, after finishing executing, the Promise returned by
  // promiseDocumentFlushed will reject with
  // NS_ERROR_DOM_NO_MODIFICATION_ALLOWED_ERR.
  //
  // Note that the callback can be called either synchronously or asynchronously
  // depending on whether or not flushes are pending:
  //
  //   The callback will be called synchronously when calling
  //   promiseDocumentFlushed when NO flushes are already pending. This is
  //   to ensure that no script has a chance to dirty the DOM before the callback
  //   is called.
  //
  //   The callback will be called asynchronously if a flush is pending.
  //
  // The expected execution order is that all pending callbacks will
  // be fired first (and in the order that they were queued) and then the
  // Promise resolution handlers will all be invoked later on during the
  // next microtask checkpoint.
  //
  // Using window.top.promiseDocumentFlushed in combination with a callback
  // that is querying items in a window that might be swapped out via
  // nsFrameLoader::SwapWithOtherLoader is highly discouraged. For example:
  //
  //   let result = await window.top.promiseDocumentFlushed(() => {
  //     return window.document.body.getBoundingClientRect();
  //   });
  //
  //   If "window" might get swapped out via nsFrameLoader::SwapWithOtherLoader
  //   at any time, then the callback might get called when the new host window
  //   will still incur layout flushes, since it's only the original host window
  //   that's being monitored via window.top.promiseDocumentFlushed.
  //
  //   See bug 1519407 for further details.
  //
  // promiseDocumentFlushed does not support re-entrancy - so calling it from
  // within a promiseDocumentFlushed callback will result in the inner call
  // throwing an NS_ERROR_FAILURE exception, and the outer Promise rejecting
  // with that exception.
  //
  // The callback function *must not make any changes which would require
  // a style or layout flush*.
  //
  // Also throws NS_ERROR_FAILURE if the window is not in a state where flushes
  // can be waited for (example: for, the PresShell has not yet been created).
  //
  // @param {function} callback
  // @returns {Promise}
  #[throws, func = "nsGlobalWindowInner::IsPrivilegedChromeWindow"]
  #[alias = "promiseDocumentFlushed"]
  pub fn promise_document_flushed(&self, callback: PromiseDocumentFlushedCallback) -> Promise<any>;

  #[chrome_only]
  #[alias = "isChromeWindow"]
  is_chrome_window: bool,

  #[chrome_only]
  Glean: GleanImpl,
  #[chrome_only]
  GleanPings: GleanPingsImpl,
}

partial trait Window {
  #[pref = "dom.vr.enabled"]
  #[alias = "onvrdisplayconnect"]
  pub on_vrdisplay_connect: EventHandler,
  #[pref = "dom.vr.enabled"]
  #[alias = "onvrdisplaydisconnect"]
  pub on_vrdisplay_disconnect: EventHandler,
  #[pref = "dom.vr.enabled"]
  #[alias = "onvrdisplayactivate"]
  pub on_vrdisplay_activate: EventHandler,
  #[pref = "dom.vr.enabled"]
  #[alias = "onvrdisplaydeactivate"]
  pub on_vrdisplay_deactivate: EventHandler,
  #[pref = "dom.vr.enabled"]
  #[alias = "onvrdisplaypresentchange"]
  pub on_vrdisplay_presentchange: EventHandler,
}

#ifndef RELEASE_OR_BETA
// https://drafts.css-houdini.org/css-paint-api-1/#dom-window-paintworklet
partial trait Window {
  #[pref = "dom.paintWorklet.enabled", throws]
  #[alias = "paintWorklet"]
  paint_worklet: Worklet,
}
#endif

Window includes WindowOrWorkerGlobalScope;

partial trait Window {
  #[throws, func = "nsGlobalWindowInner::IsRequestIdleCallbackEnabled"]
  u32 requestIdleCallback(callback: IdleRequestCallback,
  #[optional = {}] options: IdleRequestOptions);
  #[func = "nsGlobalWindowInner::IsRequestIdleCallbackEnabled"]
  #[alias = "cancelIdleCallback"]
  pub fn cancel_idle_callback(&self, handle: u32);
}

pub struct IdleRequestOptions {
  pub timeout: u32,
}

callback IdleRequestCallback = void (deadline: IdleDeadline);

partial trait Window {
  // Returns a list of locales that the internationalization components
  // should be localized to.
  //
  // The function name refers to Regional Preferences which can be either
  // fetched from the internal internationalization database (CLDR), or
  // from the host environment.
  //
  // The result is a sorted list of valid locale IDs and it should be
  // used for all APIs that accept list of locales, like ECMA402 and L10n APIs.
  //
  // This API always returns at least one locale.
  //
  // Example: #["en-US", "de", "pl", "sr-Cyrl", "zh-Hans-HK"]
  #[func = "IsChromeOrUAWidget"]
  #[alias = "getRegionalPrefsLocales"]
  pub fn get_regional_prefs_locales(&self) -> Vec<DOMString>;

  // Returns a list of locales that the web content would know from the user.
  //
  // One of the fingerprinting technique is to recognize users from their locales
  // exposed to web content. For those components that would be fingerprintable
  // from the locale should call this API instead of |getRegionalPrefsLocales()|.
  //
  // If the pref is set to spoof locale setting, this function will return the
  // spoofed locale, otherwise it returns what |getRegionalPrefsLocales()| returns.
  //
  // This API always returns at least one locale.
  //
  // Example: #["en-US"]
  #[func = "IsChromeOrUAWidget"]
  #[alias = "getWebExposedLocales"]
  pub fn get_web_exposed_locales(&self) -> Vec<DOMString>;

  // Getter funcion for IntlUtils, which provides helper functions for
  // localization.
  #[throws, func = "IsChromeOrUAWidget"]
  #[alias = "intlUtils"]
  intl_utils: IntlUtils,
}

partial trait Window {
  #[same_object, pref = "dom.visualviewport.enabled", replaceable]
  #[alias = "visualViewport"]
  visual_viewport: VisualViewport,
}

// Used to assign marks to appear on the scrollbar when
// finding on a page.
partial trait Window {
  // The marks are values between 0 and scrollMaxY.
  #[chrome_only]
  #[alias = "setScrollMarks"]
  pub fn set_scroll_marks(&self, marks: Vec<u32>);
}

pub struct WindowPostMessageOptions: StructuredSerializeOptions {
  #[alias = "targetOrigin"]
  pub target_origin: USVString = "/",
}
