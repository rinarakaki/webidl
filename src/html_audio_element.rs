// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-audio-element

#[LegacyFactoryfunction = Audio(optional src: DOMString),
 exposed = Window]
pub trait HTMLAudioElement: HTMLMediaElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;
}

