// PeerConnection.js' trait to the C++ PeerConnectionImpl.
//
// Do not confuse with RTCPeerConnection. This trait is purely for
// communication between the PeerConnection JS DOM binding and the C++
// implementation.
//
// See media/webrtc/signaling/include/PeerConnectionImpl.h
//

pub trait nsISupports;

// Must be created first. Observer events will be dispatched on the thread provided#[chrome_only, exposed = Window]
pub trait PeerConnectionImpl {
  #[alias = "constructor"]
  pub fn new() -> Self;

  // Must be called first. Observer events dispatched on the thread provided #[throws]
  void initialize(observer: PeerConnectionObserver, observer: PeerConnectionObserver,
  RTCConfiguration iceServers,
  #[alias = "nsISupports"]
  thread: ns_i_supports);

  // JSEP calls #[throws]
  #[alias = "createOffer"]
  pub fn create_offer(#[optional = {}] options: RTCOfferOptions);
  #[throws]
  #[alias = "createAnswer"]
  pub fn create_answer();
  #[throws]
  #[alias = "setLocalDescription"]
  pub fn set_local_description(action: i32, action: i32);
  #[throws]
  #[alias = "setRemoteDescription"]
  pub fn set_remote_description(action: i32, action: i32);

  #[alias = "getStats"]
  pub fn get_stats(selector: Option<MediaStreamTrack>) -> Promise<RTCStatsReport>;

  #[alias = "getRemoteStreams"]
  pub fn get_remote_streams() -> Vec<MediaStream>;

  // Adds the tracks created by GetUserMedia #[throws]
  TransceiverImpl createTransceiverImpl(kind: DOMString,
  track: Option<MediaStreamTrack>);
  #[throws]
  #[alias = "checkNegotiationNeeded"]
  pub fn check_negotiation_needed() -> bool;

  #[throws]
  void replaceTrackNoRenegotiation(transceiverImpl: TransceiverImpl,
  Option<MediaStreamTrack> withTrack);
  #[throws]
  #[alias = "closeStreams"]
  pub fn close_streams();

  #[throws]
  void enablePacketDump(level: u32,
  #[alias = "mozPacketDumpType"]
  type: moz_packet_dump_type,
  sending: bool);

  #[throws]
  void disablePacketDump(level: u32,
  #[alias = "mozPacketDumpType"]
  type: moz_packet_dump_type,
  sending: bool);

  // As the ICE candidates roll in this one should be called each time
  // in order to keep the candidate list up-to-date for the next SDP-related
  // call PeerConnectionImpl does not parse ICE candidates, just sticks them
  // into the SDP.
  #[throws]
  void addIceCandidate(candidate: DOMString,
  mid: DOMString,
  ufrag: DOMString,
  Option<u16> level);

  // Shuts down threads, deletes state #[throws]
  pub fn close();

  // Notify DOM window if this plugin crash is ours. bool pluginCrash(pluginId: u64, pluginId: u64);

  // Attributes // This provides the implementation with the certificate it uses to
  // authenticate itself. The JS side must set this before calling
  // createOffer/createAnswer or retrieving the value of fingerprint. This has
  // to be delayed because generating the certificate takes some time. attribute RTCCertificate certificate;
  #[constant]
  fingerprint: DOMString,
  #[alias = "currentLocalDescription"]
  current_local_description: DOMString,
  #[alias = "pendingLocalDescription"]
  pending_local_description: DOMString,
  #[alias = "currentRemoteDescription"]
  current_remote_description: DOMString,
  #[alias = "pendingRemoteDescription"]
  pending_remote_description: DOMString,
  #[alias = "currentOfferer"]
  current_offerer: Option<bool>,
  #[alias = "pendingOfferer"]
  pending_offerer: Option<bool>,

  #[alias = "iceConnectionState"]
  ice_connection_state: RTCIceConnectionState,
  #[alias = "iceGatheringState"]
  ice_gathering_state: RTCIceGatheringState,
  #[alias = "signalingState"]
  signaling_state: RTCSignalingState,
  pub id: DOMString,

  #[setter_throws]
  #[alias = "peerIdentity"]
  pub peer_identity: DOMString,
  #[alias = "privacyRequested"]
  privacy_requested: bool,

  // Data channels #[throws]
  RTCDataChannel createDataChannel(label: DOMString, label: DOMString,
  type: u16, ordered: bool,
  u16 maxTime, maxNum: u16,
  bool externalNegotiated, stream: u16);
}
