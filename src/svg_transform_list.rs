// The origin of this IDL file is
// http://www.w3.org/TR/SVG11/

#[exposed = Window]
pub trait SVGTransformList {
  #[alias = "numberOfItems"]
  number_of_items: u32,
  #[throws]
  pub fn clear();
  #[throws]
  pub fn initialize(newItem: SVGTransform) -> SVGTransform;
  #[throws]
  getter SVGTransform getItem(index: u32);
  #[throws]
  #[alias = "insertItemBefore"]
  pub fn insert_item_before(newItem: SVGTransform, newItem: SVGTransform) -> SVGTransform;
  #[throws]
  #[alias = "replaceItem"]
  pub fn replace_item(newItem: SVGTransform, newItem: SVGTransform) -> SVGTransform;
  #[throws]
  #[alias = "removeItem"]
  pub fn remove_item(index: u32) -> SVGTransform;
  #[throws]
  #[alias = "appendItem"]
  pub fn append_item(newItem: SVGTransform) -> SVGTransform;
  #[throws]
  #[alias = "createSVGTransformFromMatrix"]
  pub fn create_svg_transform_from_matrix(#[optional = {}] matrix: DOMMatrix2DInit) -> SVGTransform;
  #[throws]
  pub fn consolidate() -> Option<SVGTransform>;

  // Mozilla-specific stuff
  length: u32, // synonym for numberOfItems
}
