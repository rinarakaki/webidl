// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGCircleElement: SVGGeometryElement {
  #[constant]
  cx: SVGAnimatedLength,
  #[constant]
  cy: SVGAnimatedLength,
  #[constant]
  r: SVGAnimatedLength,
}

