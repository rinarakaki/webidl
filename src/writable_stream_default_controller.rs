// The origin of this IDL file is
// https://streams.spec.whatwg.org/#ws-default-controller-class-definition

#[exposed = (Window, Worker, Worklet),
  pref = "dom.streams.writable_streams.enabled"]
pub trait WritableStreamDefaultController {
  #[throws]
  pub fn error(optional e: any);
}

// TODO: AbortSignal is not exposed on Worklet
#[exposed = (Window, Worker)]
partial trait WritableStreamDefaultController {
  signal: AbortSignal,
}

