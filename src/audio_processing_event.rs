// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait AudioProcessingEvent: Event {
  #[alias = "playbackTime"]
  pub playback_time: f64,

  #[throws]
  #[alias = "inputBuffer"]
  pub input_buffer: AudioBuffer,
  #[throws]
  #[alias = "outputBuffer"]
  pub output_buffer: AudioBuffer,
}

