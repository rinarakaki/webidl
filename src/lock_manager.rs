#[SecureContext, exposed = (Window, Worker), pref = "dom.weblocks.enabled"]
pub trait LockManager {
  #[throws]
  Promise<any> request(name: DOMString,
  callback: LockGrantedCallback);
  #[throws]
  Promise<any> request(name: DOMString,
  options: LockOptions,
  callback: LockGrantedCallback);

  #[throws]
  pub fn query() -> Promise<LockManagerSnapshot>;
}

callback LockGrantedCallback = Promise<any> (lock: Option<Lock>);

pub enum LockMode { "shared", "exclusive" };

pub struct LockOptions {
  pub mode: LockMode = "exclusive",
  #[alias = "ifAvailable"]
  pub if_available: bool = false,
  pub steal: bool = false,
  pub signal: AbortSignal,
}

pub struct LockManagerSnapshot {
  pub held: Vec<LockInfo>,
  pub pending: Vec<LockInfo>,
}

pub struct LockInfo {
  pub name: DOMString,
  pub mode: LockMode,
  pub clientId: DOMString,
}
