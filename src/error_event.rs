// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/#the-errorevent-trait

#[exposed = (Window, Worker)]
pub trait ErrorEvent: Event
{
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: ErrorEventInit) -> Self;

  message: DOMString,
  filename: DOMString,
  lineno: u32,
  colno: u32,
  error: any,
}

pub struct ErrorEventInit: EventInit
{
  pub message: DOMString = "",
  pub filename: DOMString = "",
  pub lineno: u32 = 0,
  pub colno: u32 = 0,
  pub error: any = None,
}
