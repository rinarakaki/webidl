// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-legend-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-legend-element
#[exposed = Window]
pub trait HTMLLegendElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  form: Option<HTMLFormElement>,
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLLegendElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
}
