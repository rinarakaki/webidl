// Dispatched as part of the "error" event in the following situations:
// - if there's an error detected when the TCPSocket closes
// - if there's an internal error while sending data
// - if there's an error connecting to the host

#[func = "mozilla::dom::TCPSocket::ShouldTCPSocketExist",
 exposed = Window]
pub trait TCPSocketErrorEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: TCPSocketErrorEventInit);

  name: DOMString,
  message: DOMString,
  #[alias = "errorCode"]
  error_code: u32, // The internal nsresult error code.
}

pub struct TCPSocketErrorEventInit: EventInit {
  pub name: DOMString = "",
  pub message: DOMString = "",
  #[alias = "errorCode"]
  pub error_code: u32 = 0,
}
