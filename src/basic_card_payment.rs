
// The origin of this WebIDL file is
// https://www.w3.org/TR/payment-method-basic-card/

use super::*;

#[GenerateInit]
pub struct BasicCardRequest {
  #[alias = "supportedNetworks"]
  pub supported_networks: Vec<DOMString> = [],
  #[alias = "requestSecurityCode"]
  pub request_security_code: bool = true,
}

#[GenerateConversionToJS]
pub struct BasicCardResponse {
  #[alias = "cardholderName"]
  pub cardholder_name: DOMString = "",
  required card_number: DOMString,
  #[alias = "expiryMonth"]
  pub expiry_month: DOMString = "",
  #[alias = "expiryYear"]
  pub expiry_year: DOMString = "",
  #[alias = "cardSecurityCode"]
  pub card_security_code: DOMString = "",
  #[alias = "billingAddress"]
  pub billing_address: Option<PaymentAddress> = None,
}

#[GenerateConversionToJS]
pub struct BasicCardChangeDetails {
  #[alias = "billingAddress"]
  pub billing_address: Option<PaymentAddress> = None,
}

#[GenerateInit]
pub struct BasicCardErrors {
  #[alias = "cardNumber"]
  pub card_number: DOMString,
  #[alias = "cardholderName"]
  pub cardholder_name: DOMString,
  #[alias = "cardSecurityCode"]
  pub card_security_code: DOMString,
  #[alias = "expiryMonth"]
  pub expiry_month: DOMString,
  #[alias = "expiryYear"]
  pub expiry_year: DOMString,
  #[alias = "billingAddress"]
  pub billing_address: AddressErrors,
}
