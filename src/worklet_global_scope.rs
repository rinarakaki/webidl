// The origin of this IDL file is
// https://drafts.css-houdini.org/worklets/#idl-index

#[exposed = Worklet]
pub trait WorkletGlobalScope {}

// Mozilla extensions
partial trait WorkletGlobalScope {
  pub fn dump(&self, optional str: DOMString);
}
