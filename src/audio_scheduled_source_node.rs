// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/#idl-def-AudioScheduledSourceNode

use super::*;

#[exposed = Window]
pub trait AudioScheduledSourceNode: AudioNode {
  #[alias = "onended"]
  pub mut on_ended: EventHandler,

  pub fn start(&self, #[optional = 0] when: f64) -> Result<()>;

  pub fn stop(&self, #[optional = 0] when: f64) -> Result<()>;
}
