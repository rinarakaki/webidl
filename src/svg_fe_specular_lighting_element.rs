// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFESpecularLightingElement: SVGElement {
  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  #[alias = "surfaceScale"]
  surface_scale: SVGAnimatedNumber,
  #[constant]
  #[alias = "specularConstant"]
  specular_constant: SVGAnimatedNumber,
  #[constant]
  #[alias = "specularExponent"]
  specular_exponent: SVGAnimatedNumber,
  #[constant]
  #[alias = "kernelUnitLengthX"]
  kernel_unit_length_x: SVGAnimatedNumber,
  #[constant]
  #[alias = "kernelUnitLengthY"]
  kernel_unit_length_y: SVGAnimatedNumber,
}

SVGFESpecularLightingElement includes SVGFilterPrimitiveStandardAttributes;
