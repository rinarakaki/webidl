// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGMatrix {

  #[setter_throws]
  pub a: f32,
  #[setter_throws]
  pub b: f32,
  #[setter_throws]
  pub c: f32,
  #[setter_throws]
  pub d: f32,
  #[setter_throws]
  pub e: f32,
  #[setter_throws]
  pub f: f32,

  #[new_object]
  pub fn multiply(secondMatrix: SVGMatrix) -> SVGMatrix;
  #[new_object, throws]
  pub fn inverse() -> SVGMatrix;
  #[new_object]
  pub fn translate(x: f32, x: f32) -> SVGMatrix;
  #[new_object]
  pub fn scale(scaleFactor: f32) -> SVGMatrix;
  #[new_object]
  #[alias = "scaleNonUniform"]
  pub fn scale_non_uniform(scaleFactorX: f32, scaleFactorX: f32) -> SVGMatrix;
  #[new_object]
  pub fn rotate(angle: f32) -> SVGMatrix;
  #[new_object, throws]
  #[alias = "rotateFromVector"]
  pub fn rotate_from_vector(x: f32, x: f32) -> SVGMatrix;
  #[new_object]
  pub fn flip_x() -> SVGMatrix;
  #[new_object]
  pub fn flip_y() -> SVGMatrix;
  #[new_object, throws]
  pub fn skew_x(angle: f32) -> SVGMatrix;
  #[new_object, throws]
  pub fn skew_y(angle: f32) -> SVGMatrix;
}

