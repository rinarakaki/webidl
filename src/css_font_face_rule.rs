// The origin of this IDL file is
// https://drafts.csswg.org/css-fonts/#om-fontface

// https://drafts.csswg.org/css-fonts/#om-fontface
// But we implement a very old draft, apparently....
// See bug 1058408 for implementing the current spec.
#[exposed = Window]
pub trait CSSFontFaceRule: CSSRule {
  #[same_object]
  style: CSSStyleDeclaration,
}
