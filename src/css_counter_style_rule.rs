// The origin of this IDL file is
// https://drafts.csswg.org/css-counter-styles-3/#the-csscounterstylerule-trait

use super::CSSRule;

#[exposed = Window]
pub trait CSSCounterStyleRule: CSSRule {
  pub name: DOMString,
  pub system: UTF8String,
  pub symbols: UTF8String,
  #[alias = "additiveSymbols"]
  pub additive_symbols: UTF8String,
  pub negative: UTF8String,
  pub prefix: UTF8String,
  pub suffix: UTF8String,
  pub range: UTF8String,
  pub pad: UTF8String,
  #[alias = "speakAs"]
  pub speak_as: UTF8String,
  pub fallback: UTF8String,
}
