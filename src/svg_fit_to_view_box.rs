// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

pub trait mixin SVGFitToViewBox {
  #[constant]
  #[alias = "viewBox"]
  view_box: SVGAnimatedRect,
  #[constant]
  #[alias = "preserveAspectRatio"]
  preserve_aspect_ratio: SVGAnimatedPreserveAspectRatio,
}

