// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#audiotracklist

use super::*;

#[pref = "media.track.enabled", exposed = Window]
pub trait AudioTrackList: EventTarget {
  pub length: u32,
  getter AudioTrack(index: u32);  // TODO ops::Index?
  #[alias = "getTrackById"]
  pub fn get_track_by_id(&self, id: DOMString) -> Option<AudioTrack>;

  #[alias = "onchange"]
  pub mut on_change: EventHandler,
  #[alias = "onaddtrack"]
  pub mut on_add_track: EventHandler,
  #[alias = "onremovetrack"]
  pub mut on_remove_track: EventHandler,
}
