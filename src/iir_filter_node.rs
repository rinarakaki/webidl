// The origin of this IDL file is https://www.w3.org/TR/webaudio

pub struct IIRFilterOptions: AudioNodeOptions {
  required Vec<f64> feedforward;
  required Vec<f64> feedback;
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait IIRFilterNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(context: BaseAudioContext, context: BaseAudioContext) -> Result<Self, Error>;

  #[alias = "getFrequencyResponse"]
  pub fn get_frequency_response(frequencyHz: F32Array, frequencyHz: F32Array, frequencyHz: F32Array);
}

// Mozilla extension
IIRFilterNode includes AudioNodePassThrough;
