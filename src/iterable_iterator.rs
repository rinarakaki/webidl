#[GenerateConversionToJS]
pub struct IterableKeyOrValueResult {
  pub value: any,
  pub done: bool = false,
}

#[GenerateConversionToJS]
pub struct IterableKeyAndValueResult {
  pub value: Vec<any> = [],
  pub done: bool = false,
}
