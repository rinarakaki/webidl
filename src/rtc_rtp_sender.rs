// The origin of this IDL file is
// http://w3c.github.io/webrtc-pc/#rtcrtpsender-trait

pub enum RTCPriorityType {
  #[alias = "very-low"]
  VeryLow,
  #[alias = "low"]
  Low,
  #[alias = "medium"]
  Medium,
  #[alias = "high"]
  High
}

pub enum RTCDegradationPreference {
  #[alias = "maintain-framerate"]
  MaintainFramerate,
  #[alias = "maintain-resolution"]
  MaintainResolution,
  #[alias = "balanced"]
  Balanced
}

pub struct RTCRtxParameters {
  pub ssrc: u32,
}

pub struct RTCFecParameters {
  pub ssrc: u32,
}

pub struct RTCRtpEncodingParameters {
  pub ssrc: u32,
  pub rtx: RTCRtxParameters,
  pub fec: RTCFecParameters,
  pub active: bool,
  pub priority: RTCPriorityType,
  #[alias = "maxBitrate"]
  pub max_bitrate: u32,
  #[alias = "degradationPreference"]
  pub degradation_preference: RTCDegradationPreference = "balanced",
  pub rid: DOMString,
  #[alias = "scaleResolutionDownBy"]
  pub scale_resolution_downBy: f32 = 1.0,
}

pub struct RTCRtpHeaderExtensionParameters {
  pub uri: DOMString,
  pub id: u16,
  pub encrypted: bool,
}

pub struct RTCRtcpParameters {
  pub cname: DOMString,
  #[alias = "reducedSize"]
  pub reduced_size: bool,
}

pub struct RTCRtpCodecParameters {
  #[alias = "payloadType"]
  pub payload_type: u16,
  #[alias = "mimeType"]
  pub mime_type: DOMString,
  #[alias = "clockRate"]
  pub clock_rate: u32,
  pub channels: u16 = 1,
  #[alias = "sdpFmtpLine"]
  pub sdp_fmtp_line: DOMString,
}

pub struct RTCRtpParameters {
  pub encodings: Vec<RTCRtpEncodingParameters>,
  #[alias = "headerExtensions"]
  pub header_extensions: Vec<RTCRtpHeaderExtensionParameters>,
  pub rtcp: RTCRtcpParameters,
  pub codecs: Vec<RTCRtpCodecParameters>,
}

#[pref = "media.peerconnection.enabled",
 JSImplementation = "@mozilla.org/dom/rtpsender;1",
 exposed = Window]
pub trait RTCRtpSender {
  track: Option<MediaStreamTrack>,
  transport: Option<RTCDtlsTransport>,
  Promise<void> setParameters (#[optional = {}] parameters: RTCRtpParameters);
  #[alias = "getParameters"]
  pub fn get_parameters() -> RTCRtpParameters;
  #[alias = "replaceTrack"]
  pub fn replace_track(withTrack: Option<MediaStreamTrack>) -> Promise<void>;
  #[alias = "getStats"]
  pub fn get_stats() -> Promise<RTCStatsReport>;
  #[pref = "media.peerconnection.dtmf.enabled"]
  dtmf: Option<RTCDTMFSender>,
  // Ugh, can't use a chrome_only attibute Vec<MediaStream>...
  #[chrome_only]
  #[alias = "getStreams"]
  pub fn get_streams() -> Vec<MediaStream>;
  #[chrome_only]
  #[alias = "setStreams"]
  pub fn set_streams(streams: Vec<MediaStream>);
  #[chrome_only]
  #[alias = "setTrack"]
  pub fn set_track(track: Option<MediaStreamTrack>);
  #[chrome_only]
  #[alias = "checkWasCreatedByPc"]
  pub fn check_was_created_by_pc(pc: RTCPeerConnection);
}
