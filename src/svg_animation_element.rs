// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimationElement: SVGElement {

  #[alias = "targetElement"]
  target_element: Option<SVGElement>,

  #[alias = "onbegin"]
  pub onbegin: EventHandler,
  #[alias = "onend"]
  pub onend: EventHandler,
  #[alias = "onrepeat"]
  pub onrepeat: EventHandler,

  #[throws]
  #[alias = "getStartTime"]
  pub fn get_start_time() -> f32;
  #[binary_name = "getCurrentTimeAsFloat"]
  #[alias = "getCurrentTime"]
  pub fn get_current_time() -> f32;
  #[throws]
  #[alias = "getSimpleDuration"]
  pub fn get_simple_duration() -> f32;

  #[throws]
  #[alias = "beginElement"]
  pub fn begin_element();
  #[throws]
  #[alias = "beginElementAt"]
  pub fn begin_element_at(offset: f32);
  #[throws]
  #[alias = "endElement"]
  pub fn end_element();
  #[throws]
  #[alias = "endElementAt"]
  pub fn end_element_at(offset: f32);
}

SVGAnimationElement includes SVGTests;

