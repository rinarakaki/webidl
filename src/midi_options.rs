// The origin of this IDL file is
// https://webaudio.github.io/web-midi-api/

pub struct MIDIOptions {
  pub sysex: bool = false,
  pub software: bool = false,
}
