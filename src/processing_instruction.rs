// The origin of this IDL file is
// https://dom.spec.whatwg.org/#trait-processinginstruction
// https://drafts.csswg.org/cssom/#requirements-on-user-agents-implementing-the-xml-stylesheet-processing-instruction

// https://dom.spec.whatwg.org/#trait-processinginstruction
#[exposed = Window]
pub trait ProcessingInstruction: CharacterData {
  target: DOMString,
}

// https://drafts.csswg.org/cssom/#requirements-on-user-agents-implementing-the-xml-stylesheet-processing-instruction
ProcessingInstruction includes LinkStyle;
