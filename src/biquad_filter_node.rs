// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

pub enum BiquadFilterType {
  #[alias = "lowpass"]
  Lowpass,
  #[alias = "highpass"]
  Highpass,
  #[alias = "bandpass"]
  Bandpass,
  #[alias = "lowshelf"]
  Lowshelf,
  #[alias = "highshelf"]
  Highshelf,
  #[alias = "peaking"]
  Peaking,
  #[alias = "notch"]
  Notch,
  #[alias = "allpass"]
  Allpass
}

pub struct BiquadFilterOptions: AudioNodeOptions {
  pub type: BiquadFilterType = "lowpass",
  pub Q: f32 = 1,
  pub detune: f32 = 0,
  pub frequency: f32 = 350,
  pub gain: f32 = 0,
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait BiquadFilterNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(
    context: BaseAudioContext,
    #[optional = {}] options: BiquadFilterOptions) -> Result<Self>;

  pub mut type: BiquadFilterType,
  pub frequency: AudioParam,  // in Hertz
  pub detune: AudioParam,  // in Cents
  pub Q: AudioParam,  // Quality factor
  pub gain: AudioParam,  // in Decibels

  #[alias = "getFrequencyResponse"]
  pub fn get_frequency_response(
    &self,
    frequency_hz: F32Array,
    mag_response: F32Array,
    phase_response: F32Array) -> Result<()>;

}

// Mozilla extension
BiquadFilterNode includes AudioNodePassThrough;

