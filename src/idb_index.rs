// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBIndexParameters

pub struct IDBIndexParameters {
  pub unique: bool = false,
  #[alias = "multiEntry"]
  pub multi_entry: bool = false,
  // <None>: Not locale-aware, uses normal JS sorting.
  // <string>: Always sorted based on the rules of the specified
  //           locale (e.g. "en-US", etc.).
  // "auto": Sorted by the platform default, may change based on
  //           user agent options.
  pub locale: Option<DOMString> = None,
}

#[exposed = (Window, Worker)]
pub trait IDBIndex {
  #[setter_throws]
  pub name: DOMString,

  #[alias = "objectStore"]
  object_store: IDBObjectStore,

  #[throws]
  #[alias = "keyPath"]
  key_path: any,

  #[alias = "multiEntry"]
  multi_entry: bool,
  unique: bool,

  // <None>: Not locale-aware, uses normal JS sorting.
  // <string>: Sorted based on the rules of the specified locale.
  //           Note: never returns "auto", only the current locale.
  #[func = "mozilla::dom::IndexedDatabaseManager::ExperimentalFeaturesEnabled"]
  locale: Option<DOMString>,

  #[func = "mozilla::dom::IndexedDatabaseManager::ExperimentalFeaturesEnabled"]
  #[alias = "isAutoLocale"]
  is_auto_locale: bool,

  #[throws]
  IDBRequest openCursor (#[optional = "next"] range: any, optional direction: IDBCursorDirection);

  #[throws]
  IDBRequest openKeyCursor (#[optional = "next"] range: any, optional direction: IDBCursorDirection);

  #[throws]
  IDBRequest get (key: any);

  #[throws]
  IDBRequest getKey (key: any);

  #[throws]
  IDBRequest count (optional key: any);
}

partial trait IDBIndex {
  // If we decide to add use counters for the mozGetAll/mozGetAllKeys
  // functions, we'll need to pull them out into sepatate operations
  // with a binary_name mapping to the same underlying implementation.
  #[throws, Alias = "mozGetAll"]
  IDBRequest getAll (optional key: any, optional #[EnforceRange] u32 limit);

  #[throws, Alias = "mozGetAllKeys"]
  IDBRequest getAllKeys (optional key: any, optional #[EnforceRange] u32 limit);
}
