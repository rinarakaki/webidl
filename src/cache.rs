// The origin of this IDL file is
// http://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html

use super::*;

// https://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html#cache
#[exposed = (Window, Worker), pref = "dom.caches.enabled"]
pub trait Cache {
  #[new_object]
  pub fn match(
    &self,
    request: RequestInfo,
    #[optional = {}] options: CacheQueryOptions) -> Promise<Response>;

  #[new_object]
  #[alias = "matchAll"]
  pub fn match_all(
    &self,
    #[optional] request: RequestInfo,
    #[optional = {}] options: CacheQueryOptions)
    -> Promise<Vec<Response>>;
    
  #[new_object, needs_caller_type]
  pub fn add(&self, request: RequestInfo) -> Promise<void>;

  #[new_object, needs_caller_type]
  #[alias = "addAll"]
  pub fn add_all(&self, requests: Vec<RequestInfo>) -> Promise<void>;

  #[new_object]
  pub fn put(&self, request: RequestInfo, response: Response)
    -> Promise<void>;

  #[new_object]
  pub fn delete(
    &self,
    request: RequestInfo,
    #[optional = {}] options: CacheQueryOptions)
    -> Promise<bool>;

  #[new_object]
  pub fn keys(
    &self,
    #[optional] request: RequestInfo,
    #[optional = {}] options: CacheQueryOptions) -> Promise<Vec<Request>>;
}

pub struct CacheQueryOptions {
  #[alias = "ignoreSearch"]
  pub #[optional = false] ignore_search: bool,
  #[alias = "ignoreMethod"]
  pub #[optional = false] ignore_method: bool,
  #[alias = "ignoreVary"]
  pub #[optional = false] ignore_vary: bool,
}

pub struct CacheBatchOperation {
  pub #[optional] type: DOMString,
  pub #[optional] request: Request,
  pub #[optional] response: Response,
  pub #[optional] options: CacheQueryOptions,
}
