use super::Event;

#[pref = "device.sensors.orientation.enabled",
  func = "nsGlobalWindowInner::DeviceSensorsEnabled", LegacyEventInit,
  exposed = Window]
pub trait DeviceOrientationEvent: Event {
  constructor(
    type: DOMString,
    #[optional = {}] eventInitDict: DeviceOrientationEventInit);

  alpha: Option<f64>,
  beta: Option<f64>,
  gamma: Option<f64>,
  absolute: bool,

  // initDeviceOrientationEvent is a Gecko specific deprecated method.
  void initDeviceOrientationEvent(
    type: DOMString,
    #[optional = false] canBubble: bool,
    #[optional = false] cancelable: bool,
    optional Option<f64> alpha = None,
    optional Option<f64> beta = None,
    optional Option<f64> gamma = None,
    #[optional = false] absolute: bool);
}

pub struct DeviceOrientationEventInit: EventInit {
  pub alpha: Option<f64> = None,
  pub beta: Option<f64> = None,
  pub gamma: Option<f64> = None,
  pub absolute: bool = false,
}
