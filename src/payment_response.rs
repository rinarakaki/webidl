// The origin of this WebIDL file is
//   https:/w3c.github.io/payment-request/#paymentresponse-trait

pub enum PaymentComplete {
  #[alias = "success"]
  Success,
  #[alias = "fail"]
  Fail,
  #[alias = "unknown"]
  Unknown
}

#[SecureContext,
 func = "mozilla::dom::PaymentRequest::PrefEnabled",
 exposed = Window]
pub trait PaymentResponse: EventTarget {
  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;

  #[alias = "requestId"]
  request_id: DOMString,
  #[alias = "methodName"]
  method_name: DOMString,
  details: object,
  #[alias = "shippingAddress"]
  shipping_address: Option<PaymentAddress>,
  #[alias = "shippingOption"]
  shipping_option: Option<DOMString>,
  #[alias = "payerName"]
  payer_name: Option<DOMString>,
  #[alias = "payerEmail"]
  payer_email: Option<DOMString>,
  #[alias = "payerPhone"]
  payer_phone: Option<DOMString>,

  #[new_object]
  pub fn complete(#[optional = "unknown"] result: PaymentComplete) -> Promise<void>;

  // If the pub struct argument has no required members, it must be optional.
  #[new_object]
  pub fn retry(#[optional = {}] errorFields: PaymentValidationErrors) -> Promise<void>;

  #[alias = "onpayerdetailchange"]
  pub onpayerdetailchange: EventHandler,
}
