// For more information on this trait, please see
// https://html.spec.whatwg.org/#message-ports

#[exposed = (Window,Worker, AudioWorklet)]
pub trait MessagePort: EventTarget {
  #[throws]
  #[alias = "postMessage"]
  pub fn post_message(message: any, message: any);
  #[throws]
  #[alias = "postMessage"]
  pub fn post_message(message: any, #[optional = {}] options: StructuredSerializeOptions);

  pub fn start();
  pub fn close();

  // event handlers
  #[alias = "onmessage"]
  pub on_message: EventHandler,
  #[alias = "onmessageerror"]
  pub on_messageerror: EventHandler,
}

pub struct StructuredSerializeOptions {
  pub transfer: Vec<object> = [],
}
