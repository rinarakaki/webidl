pub super::*;

// This dictionnary holds the parameters supporting the app:// protocol.
pub struct AppInfo {
  pub path: DOMString = "",
  #[alias = "isCoreApp"]
  pub is_core_app: bool = false,
}
