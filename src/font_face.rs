// The origin of this IDL file is
// http://dev.w3.org/csswg/css-font-loading/#fontface-trait

pub type BinaryData = (ArrayBuffer or ArrayBufferView);

pub struct FontFaceDescriptors {
  pub style: UTF8String = "normal",
  pub weight: UTF8String = "normal",
  pub stretch: UTF8String = "normal",
  #[alias = "unicodeRange"]
  pub unicode_range: UTF8String = "U+0-10FFFF",
  pub variant: UTF8String = "normal",
  #[alias = "featureSettings"]
  pub feature_settings: UTF8String = "normal",
  #[pref = "layout.css.font-variations.enabled"]
  #[alias = "variationSettings"]
  pub variation_settings: UTF8String = "normal",
  #[pref = "layout.css.font-display.enabled"]
  pub display: UTF8String = "auto",
  #[pref = "layout.css.font-metrics-overrides.enabled"]
  #[alias = "ascentOverride"]
  pub ascent_override: UTF8String = "normal",
  #[pref = "layout.css.font-metrics-overrides.enabled"]
  #[alias = "descentOverride"]
  pub descent_override: UTF8String = "normal",
  #[pref = "layout.css.font-metrics-overrides.enabled"]
  #[alias = "lineGapOverride"]
  pub line_gap_override: UTF8String = "normal",
  #[pref = "layout.css.size-adjust.enabled"]
  #[alias = "sizeAdjust"]
  pub size_adjust: UTF8String = "100%",
}

pub enum FontFaceLoadStatus {
  #[alias = "unloaded"]
  Unloaded,
  #[alias = "loading"]
  Loading,
  #[alias = "loaded"]
  Loaded,
  #[alias = "error"]
  Error
}

// Bug 1072107 is for exposing this in workers.
// #[exposed = (Window, Worker)]
#[pref = "layout.css.font-loading-api.enabled",
 exposed = Window]
pub trait FontFace {
  #[alias = "constructor"]
  pub fn new(
    family: UTF8String,
    source: (UTF8String or BinaryData),
    #[optional = {}] descriptors: FontFaceDescriptors) -> Result<Self>;

  #[setter_throws]
  pub family: UTF8String,
  #[setter_throws]
  pub style: UTF8String,
  #[setter_throws]
  pub weight: UTF8String,
  #[setter_throws]
  pub stretch: UTF8String,
  #[setter_throws]
  #[alias = "unicodeRange"]
  pub unicode_range: UTF8String,
  #[setter_throws]
  pub variant: UTF8String,
  #[setter_throws]
  #[alias = "featureSettings"]
  pub feature_settings: UTF8String,
  #[setter_throws, pref = "layout.css.font-variations.enabled"]
  #[alias = "variationSettings"]
  pub variation_settings: UTF8String,
  #[setter_throws, pref = "layout.css.font-display.enabled"]
  pub display: UTF8String,
  #[setter_throws, pref = "layout.css.font-metrics-overrides.enabled"]
  #[alias = "ascentOverride"]
  pub ascent_override: UTF8String,
  #[setter_throws, pref = "layout.css.font-metrics-overrides.enabled"]
  #[alias = "descentOverride"]
  pub descent_override: UTF8String,
  #[setter_throws, pref = "layout.css.font-metrics-overrides.enabled"]
  #[alias = "lineGapOverride"]
  pub line_gap_override: UTF8String,
  #[setter_throws, pref = "layout.css.size-adjust.enabled"]
  #[alias = "sizeAdjust"]
  pub size_adjust: UTF8String,

  status: FontFaceLoadStatus,

  #[throws]
  pub fn load() -> Promise<FontFace>;

  #[throws]
  loaded: Promise<FontFace>,
}
