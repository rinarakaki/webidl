// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait MediaStreamAudioDestinationNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(context: AudioContext, #[optional = {}] options: AudioNodeOptions) -> Result<Self, Error>;

  #[binary_name = "DOMStream"]
  stream: MediaStream,
}
