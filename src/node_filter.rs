// The origin of this IDL file is
// http://dom.spec.whatwg.org/#trait-nodefilter

#[exposed = Window]
callback trait NodeFilter {
  // Constants for acceptNode()
  const u16 FILTER_ACCEPT = 1;
  const u16 FILTER_REJECT = 2;
  const u16 FILTER_SKIP = 3;

  // Constants for whatToShow
  const u32 SHOW_ALL = 0xFFFFFFFF;
  const u32 SHOW_ELEMENT = 0x1;
  const u32 SHOW_ATTRIBUTE = 0x2; // historical
  const u32 SHOW_TEXT = 0x4;
  const u32 SHOW_CDATA_SECTION = 0x8; // historical
  const u32 SHOW_ENTITY_REFERENCE = 0x10; // historical
  const u32 SHOW_ENTITY = 0x20; // historical
  const u32 SHOW_PROCESSING_INSTRUCTION = 0x40;
  const u32 SHOW_COMMENT = 0x80;
  const u32 SHOW_DOCUMENT = 0x100;
  const u32 SHOW_DOCUMENT_TYPE = 0x200;
  const u32 SHOW_DOCUMENT_FRAGMENT = 0x400;
  const u32 SHOW_NOTATION = 0x800; // historical

  u16 acceptNode(node: Node);
}
