// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-embed-element
// http://www.whatwg.org/specs/web-apps/current-work/#HTMLEmbedElement-partial

// http://www.whatwg.org/specs/web-apps/current-work/#the-embed-element
#[NeedResolve,
 exposed = Window]
pub trait HTMLEmbedElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, pure, setter_throws]
  pub src: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub type: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub width: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub height: DOMString,
}

// http://www.whatwg.org/specs/web-apps/current-work/#HTMLEmbedElement-partial
partial trait HTMLEmbedElement {
  #[ce_reactions, pure, setter_throws]
  pub align: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub name: DOMString,
}

partial trait HTMLEmbedElement {
  // GetSVGDocument
  #[needs_subject_principal]
  #[alias = "getSVGDocument"]
  pub fn get_svg_document() -> Option<Document>;
}

HTMLEmbedElement includes MozImageLoadingContent;
HTMLEmbedElement includes MozFrameLoaderOwner;
HTMLEmbedElement includes MozObjectLoadingContent;
