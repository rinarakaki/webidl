// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/webrtc.html#mediastreamevent

pub struct MediaStreamTrackEventInit: EventInit {
  required MediaStreamTrack track;
}

#[exposed = Window]
pub trait MediaStreamTrackEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;

  #[same_object]
  track: MediaStreamTrack,
}
