// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct MediaStreamAudioSourceOptions {
  required MediaStream mediaStream;
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait MediaStreamAudioSourceNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(context: AudioContext, context: AudioContext) -> Result<Self, Error>;

  #[binary_name = "GetMediaStream"]
  #[alias = "mediaStream"]
  media_stream: MediaStream,
}

// Mozilla extensions
MediaStreamAudioSourceNode includes AudioNodePassThrough;

