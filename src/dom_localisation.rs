// DOMLocalisation is an extension of the Fluent Localisation API.
//
// DOMLocalisation adds a state for storing `roots` - DOM elements
// which translation status is controlled by the DOMLocalisation
// instance and monitored for mutations.
// DOMLocalisation also adds methods dedicated to DOM manipulation.
//
// Methods:
//    - connectRoot - add a root
//    - disconnectRoot - remove a root
//    - pauseObserving - pause observing of roots
//    - resumeObserving - resume observing of roots
//    - setAttributes - set l10n attributes of an element
//    - getAttributes - retrieve l10n attributes of an element
//    - translateFragment - translate a DOM fragment
//    - translateElements - translate a list of DOM elements
//    - translateRoots - translate all attached roots

use super::Localisation;

#[func = "IsChromeOrUAWidget", exposed = Window]
#[alias = "DOMLocalization"]
pub trait DOMLocalisation: Localisation {
  // Constructor arguments:
  //    - aResourceids - a list of localization resource URIs
  //                           which will provide messages for this
  //                           Localisation instance.
  //    - aSync - Specifies if the initial state of the DOMLocalisation
  //                           and the underlying Localisation API is synchronous.
  //                           This enables a number of synchronous methods on the
  //                           Localisation API and uses it for `TranslateElements`
  //                           making the method return a synchronusly resolved promise.
  //    - aRegistry - optional L10nRegistry: custom to be used by this Localisation instance.
  //    - aLocales - custom set of locales to be used for this Localisation.
  #[alias = "constructor"]
  pub fn new(
    aResourceIds: Vec<L10nResourceId>,
    #[optional = false] aSync: bool,
    optional aRegistry: L10nRegistry,
    optional Vec<UTF8String> aLocales) -> Result<Self>;

  // Adds a node to nodes observed for localization
  // related changes.
  #[alias = "connectRoot"]
  pub fn connect_root(aElement: Node) -> Result<()>;

  // Removes a node from nodes observed for localization
  // related changes.
  #[alias = "disconnectRoot"]
  pub fn disconnect_root(aElement: Node) -> Result<()>;

  // Pauses the MutationObserver set to observe
  // localization related DOM mutations.
  #[alias = "pauseObserving"]
  pub fn pause_observing() -> Result<()>;

  // Resumes the MutationObserver set to observe
  // localization related DOM mutations.
  #[alias = "resumeObserving"]
  pub fn resume_observing() -> Result<()>;

  // A helper function which allows the user to set localization-specific attributes
  // on an element.
  // This method lives on `document.l10n` for compatibility reasons with the
  // JS FluentDOM implementation. We may consider moving it onto Element.
  //
  // Example:
  //    document.l10n.setAttributes(h1, "key1", { emailCount: 5 });
  //
  //    <h1 data-l10n-id = "key1" data-l10n-args = "{\"emailCount\": 5}"/>
  #[alias = "setAttributes"]
  pub fn set_attributes(
    aElement: Element, aElement: Element,
    optional Option<object> aArgs) -> Result<()>;

  // A helper function which allows the user to retrieve a set of localization-specific
  // attributes from an element.
  // This method lives on `document.l10n` for compatibility reasons with the
  // JS FluentDOM implementation. We may consider moving it onto Element.
  //
  // Example:
  //    let l10nAttrs = document.l10n.getAttributes(h1);
  //    assert.deepEqual(l10nAttrs, {id: "key1", args: { emailCount: 5});
  #[alias = "getAttributes"]
  pub fn get_attributes(aElement: Element) -> Result<L10nIdArgs>;

  // Triggers translation of a subtree rooted at aNode.
  //
  // The method finds all translatable descendants of the argument and
  // localizes them.
  //
  // This method is mainly useful to trigger translation not covered by the
  // DOMLocalisation's MutationObserver - for example before it gets attached
  // to the tree.
  // In such cases, when the already-translated fragment gets
  // injected into the observed root, one should `pauseObserving`,
  // then append the fragment, and finally `resumeObserving`.
  //
  // Example:
  //    await document.l10n.translatFragment(frag);
  //    root.pauseObserving();
  //    parent.appendChild(frag);
  //    root.resumeObserving();
  #[new_object]
  #[alias = "translateFragment"]
  pub fn translate_fragment(aNode: Node) -> Promise<any>;

  // Triggers translation of a list of Elements using the localization context.
  //
  // The method only translates the elements directly passed to the method,
  // not any descendant nodes.
  //
  // This method is mainly useful to trigger translation not covered by the
  // DOMLocalisation's MutationObserver - for example before it gets attached
  // to the tree.
  // In such cases, when the already-translated fragment gets
  // injected into the observed root, one should `pauseObserving`,
  // then append the fragment, and finally `resumeObserving`.
  //
  // Example:
  //    await document.l10n.translateElements(#[elem1, elem2]);
  //    root.pauseObserving();
  //    parent.appendChild(elem1);
  //    root.resumeObserving();
  //    alert(elem2.textContent);
  #[new_object]
  #[alias = "translateElements"]
  pub fn translate_elements(aElements: Vec<Element>) -> Promise<void>;

  // Triggers translation of all attached roots and sets their
  // locale info and directionality attributes.
  //
  // Example:
  //    await document.l10n.translateRoots();
  #[new_object]
  #[alias = "translateRoots"]
  pub fn translate_roots() -> Promise<void>;
}
