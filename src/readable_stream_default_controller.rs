// The origin of this IDL file is
// https://streams.spec.whatwg.org/#rs-default-controller-class-definition

#[exposed = (Window,Worker, Worklet), pref = "dom.streams.expose.ReadableStreamDefaultController"]
pub trait ReadableStreamDefaultController {
  readonly attribute unrestricted Option<f64> desiredSize;

  #[throws]
  pub fn close();

  #[throws]
  pub fn enqueue(optional chunk: any);

  #[throws]
  pub fn error(optional e: any);
}
