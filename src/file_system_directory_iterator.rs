// To implement FileSystemDirectoryHandle's async iteration until we can use
// a natively supported `async iterable`.
#[exposed = (Window, Worker), SecureContext, LegacyNoInterfaceObject]
pub trait FileSystemDirectoryIterator {
  pub fn next() -> Promise<any>;
}
