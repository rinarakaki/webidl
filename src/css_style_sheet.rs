// The origin of this IDL file is
// http://dev.w3.org/csswg/cssom/
// https://wicg.github.io/construct-stylesheets/

pub enum CSSStyleSheetParsingMode {
  #[alias = "author"]
  Author,
  #[alias = "user"]
  User,
  #[alias = "agent"]
  Agent
}

pub struct CSSStyleSheetInit {
  pub media : (MediaList or UTF8String) = "";
  pub disabled: bool = false,
  pub baseURL: UTF8String,
}

#[exposed = Window]
pub trait CSSStyleSheet: StyleSheet {
  #[pref = "layout.css.constructable-stylesheets.enabled"]
  #[alias = "constructor"]
  pub fn new(#[optional = {}] options: CSSStyleSheetInit) -> Result<Self>;

  #[pure, binary_name = "DOMOwnerRule"]
  #[alias = "ownerRule"]
  owner_rule: Option<CSSRule>,
  #[throws, needs_subject_principal]
  #[alias = "cssRules"]
  css_rules: CSSRuleList,
  #[chrome_only, binary_name = "parsingModeDOM"]
  #[alias = "parsingMode"]
  parsing_mode: CSSStyleSheetParsingMode,

  #[needs_subject_principal]
  #[alias = "insertRule"]
  pub fn insert_rule(&self, rule: UTF8String, #[optional = 0] index: u32) -> Result<u32>;

  #[needs_subject_principal]
  #[alias = "deleteRule"]
  pub fn delete_rule(&self, index: u32) -> Result<()>;

  #[pref = "layout.css.constructable-stylesheets.enabled"]
  pub fn replace(&self, text: UTF8String) -> Promise<CSSStyleSheet>;

  #[pref = "layout.css.constructable-stylesheets.enabled"]
  #[alias = "replaceSync"]
  pub fn replace_sync(&self, text: UTF8String) -> Result<()>;

  // Non-standard WebKit things.
  #[throws, needs_subject_principal, binary_name = "cssRules"]
  rules: CSSRuleList,

  #[needs_subject_principal, binary_name = "deleteRule"]
  #[alias = "removeRule"]
  pub fn remove_rule(&self, #[optional = 0] index: u32) -> Result<()>;

  #[needs_subject_principal]
  #[alias = "addRule"]
  pub fn add_rule(
    &self, 
    #[optional = "undefined"] selector: UTF8String,
    #[optional = "undefined"] style: UTF8String,
    optional index: u32)
    -> Result<i32>;
}
