// PeerConnection.js' trait to the C++ TransceiverImpl.
//
// Do not confuse with RTCRtpTransceiver. This trait is purely for
// communication between the PeerConnection JS DOM binding and the C++
// implementation.
//
// See media/webrtc/peerconnection/TransceiverImpl.h
//

// Constructed by PeerConnectionImpl::CreateTransceiverImpl.
#[chrome_only, exposed = Window]
pub trait TransceiverImpl {
  #[throws]
  #[alias = "syncWithJS"]
  pub fn sync_with_js(transceiver: RTCRtpTransceiver);
  receiver: RTCRtpReceiver,
  // TODO(1616937: bug): We won't need this once we implement RTCRtpSender in c++
  #[chrome_only]
  dtmf: Option<RTCDTMFSender>,
  #[chrome_only]
  #[alias = "dtlsTransport"]
  dtls_transport: Option<RTCDtlsTransport>,
}

