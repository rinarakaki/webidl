// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

#[exposed = Window, SecureContext, pref = "dom.audioworklet.enabled"]
pub trait AudioWorklet: Worklet {}
