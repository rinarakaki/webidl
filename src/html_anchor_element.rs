// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-a-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-a-element
#[exposed = Window]
pub trait HTMLAnchorElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub target: DOMString,
  #[ce_reactions, setter_throws]
  pub download: DOMString,
  #[ce_reactions, setter_throws]
  pub ping: DOMString,
  #[ce_reactions, setter_throws]
  pub rel: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "referrerPolicy"]
  pub referrer_policy: DOMString,
  #[put_forwards = value]
  #[alias = "relList"]
  rel_list: DOMTokenList,
  #[ce_reactions, setter_throws]
  pub hreflang: DOMString,
  #[ce_reactions, setter_throws]
  pub type: DOMString,

  #[ce_reactions, throws]
  pub text: DOMString,
}

HTMLAnchorElement includes HTMLHyperlinkElementUtils;

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLAnchorElement {
  #[ce_reactions, setter_throws]
  pub coords: DOMString,
  #[ce_reactions, setter_throws]
  pub charset: DOMString,
  #[ce_reactions, setter_throws]
  pub name: DOMString,
  #[ce_reactions, setter_throws]
  pub rev: DOMString,
  #[ce_reactions, setter_throws]
  pub shape: DOMString,
}
