pub enum FileSystemHandleKind {
  #[alias = "file"]
  File,
  #[alias = "directory"]
  Directory,
}

// TODO: Add Serializable
#[exposed = (Window, Worker), SecureContext, pref = "dom.fs.enabled"]
pub trait FileSystemHandle {
  kind: FileSystemHandleKind,
  name: USVString,

  #[alias = "isSameEntry"]
  pub fn is_same_entry(other: FileSystemHandle) -> Promise<bool>;
}
