// Emulates undefined through Codegen.py.
#[LegacyUnenumerableNamedProperties,
 exposed = Window]
pub trait HTMLAllCollection {
  length: u32,
  getter Element (index: u32);
  getter (HTMLCollection or Element)? namedItem(name: DOMString);
  (HTMLCollection or Element)? item(optional nameOrIndex: DOMString);
  legacycaller (HTMLCollection or Element)? (optional nameOrIndex: DOMString);
}
