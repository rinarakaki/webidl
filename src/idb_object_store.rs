// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBObjectStore

pub struct IDBObjectStoreParameters {
  (DOMString or Vec<DOMString>)? keyPath = None;
  #[alias = "autoIncrement"]
  pub auto_increment: bool = false,
}

#[exposed = (Window, Worker)]
pub trait IDBObjectStore {
  #[setter_throws]
  pub name: DOMString,

  #[throws]
  #[alias = "keyPath"]
  key_path: any,

  #[alias = "indexNames"]
  index_names: DOMStringList,
  transaction: IDBTransaction,
  #[alias = "autoIncrement"]
  auto_increment: bool,

  #[throws]
  IDBRequest put (value: any, optional key: any);

  #[throws]
  IDBRequest add (value: any, optional key: any);

  #[throws]
  IDBRequest delete (key: any);

  #[throws]
  IDBRequest get (key: any);

  #[throws]
  IDBRequest getKey (key: any);

  #[throws]
  IDBRequest clear ();

  #[throws]
  IDBRequest openCursor (#[optional = "next"] range: any, optional direction: IDBCursorDirection);

  #[throws]
  IDBIndex createIndex (name: DOMString, (DOMString or Vec<DOMString>) keyPath, #[optional = {}] optionalParameters: IDBIndexParameters);

  #[throws]
  IDBIndex index (name: DOMString);

  #[throws]
  void deleteIndex (indexName: DOMString);

  #[throws]
  IDBRequest count (optional key: any);
}

partial trait IDBObjectStore {
  // Success fires IDBTransactionEvent, result == array of values for given keys
  // If we decide to add use a counter for the mozGetAll function, we'll need
  // to pull it out into a sepatate operation with a binary_name mapping to the
  // same underlying implementation.
  #[throws, Alias = "mozGetAll"]
  IDBRequest getAll (optional key: any, optional #[EnforceRange] u32 limit);

  #[throws]
  IDBRequest getAllKeys (optional key: any, optional #[EnforceRange] u32 limit);

  #[throws]
  IDBRequest openKeyCursor (#[optional = "next"] range: any, optional direction: IDBCursorDirection);
}
