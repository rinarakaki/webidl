// The origin of this IDL file is
// https://w3c.github.io/push-api/

#[pref = "dom.push.enabled",
 exposed = ServiceWorker]
pub trait PushEvent: ExtendableEvent {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: PushEventInit)
    -> Result<Self, Error>;

  data: Option<PushMessageData>,
}

pub type PushMessageDataInit = (BufferSource or USVString);

pub struct PushEventInit: ExtendableEventInit {
  pub data: PushMessageDataInit,
}
