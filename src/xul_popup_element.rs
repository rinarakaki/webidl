pub struct OpenPopupOptions {
  // manner in which to anchor the popup to node
  pub position: DOMString = "",
  // horizontal offset
  pub x: i32 = 0,
  // vertical offset
  pub y: i32 = 0,
  // isContextMenu true for context menus, false for other popups
  #[alias = "isContextMenu"]
  pub is_context_menu: bool = false,
  // true if popup node attributes override position
  #[alias = "attributesOverride"]
  pub attributes_override: bool = false,
  // triggerEvent the event that triggered this popup (mouse click for example)
  #[alias = "triggerEvent"]
  pub trigger_event: Option<Event> = None,
}

pub struct ActivateMenuItemOptions {
  #[alias = "altKey"]
  pub alt_key: bool = false,
  #[alias = "metaKey"]
  pub meta_key: bool = false,
  #[alias = "ctrlKey"]
  pub ctrl_key: bool = false,
  #[alias = "shiftKey"]
  pub shift_key: bool = false,
  pub button: short = 0,
}

pub type StringOrOpenPopupOptions = (DOMString or OpenPopupOptions);

#[chrome_only, exposed = Window]
pub trait XULPopupElement: XULElement
{
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  // Allow the popup to automatically position itself.
  #[alias = "autoPosition"]
  pub auto_position: bool,

  // Open the popup relative to a specified node at a specific location.
  //
  // The popup may be either anchored to another node or opened freely.
  // To anchor a popup to a node, supply an anchor node and set the position
  // to a string indicating the manner in which the popup should be anchored.
  // Possible values for position are:
  //    before_start, before_end, after_start, after_end,
  //    start_before, start_after, end_before, end_after,
  //    overlap, after_pointer
  //
  // The anchor node does not need to be in the same document as the popup.
  //
  // If the attributesOverride argument is true, popupanchor: the, popupalign
  // and position attributes on the popup node override the position value
  // argument. If attributesOverride is false, the attributes are only used
  // if position is empty.
  //
  // For an anchored popup, the x and y arguments may be used to offset the
  // popup from its anchored position by some distance, measured in CSS pixels.
  // x increases to the right and y increases down. Negative values may also
  // be used to move to the left and upwards respectively.
  //
  // Unanchored popups may be created by supplying None as the anchor node.
  // An unanchored popup appears at the position specified by x and y,
  // relative to the viewport of the document containing the popup node. In
  // this case, position and attributesOverride are ignored.
  //
  // @param anchorElement the node to anchor the popup to, may be None
  // @param options either options to use, or a string position
  // @param x horizontal offset
  // @param y vertical offset
  // @param isContextMenu true for context menus, false for other popups
  // @param attributesOverride true if popup node attributes override position
  // @param triggerEvent the event that triggered this popup (mouse click for example)
  void openPopup(optional Option<Element> anchorElement = None,
  #[optional = {}] options: StringOrOpenPopupOptions,
  #[optional = 0] x: i32,
  #[optional = 0] y: i32,
  #[optional = false] isContextMenu: bool,
  #[optional = false] attributesOverride: bool,
  optional Option<Event> triggerEvent = None);

  // Open the popup at a specific screen position specified by x and y. This
  // position may be adjusted if it would cause the popup to be off of the
  // screen. The x and y coordinates are measured in CSS pixels, and like all
  // screen coordinates, are given relative to the top left of the primary
  // screen.
  //
  // @param isContextMenu true for context menus, false for other popups
  // @param x horizontal screen position
  // @param y vertical screen position
  // @param triggerEvent the event that triggered this popup (mouse click for example)
  void openPopupAtScreen(#[optional = 0] x: i32, #[optional = 0] y: i32,
  #[optional = false] isContextMenu: bool,
  optional Option<Event> triggerEvent = None);

  // Open the popup anchored at a specific screen rectangle. This function is
  // similar to openPopup except that that rectangle of the anchor is supplied
  // rather than an element. The anchor rectangle arguments are screen
  // coordinates.
  void openPopupAtScreenRect(#[optional = ""] position: DOMString,
  #[optional = 0] x: i32,
  #[optional = 0] y: i32,
  #[optional = 0] width: i32,
  #[optional = 0] height: i32,
  #[optional = false] isContextMenu: bool,
  #[optional = false] attributesOverride: bool,
  optional Option<Event> triggerEvent = None);

  //  Hide the popup if it is open. The cancel argument is used as a hint that
  //  the popup is being closed because it has been cancelled, rather than
  //  something being selected within the panel.
  //
  // @param cancel if true, then the popup is being cancelled.
  #[alias = "hidePopup"]
  pub fn hide_popup(#[optional = false] cancel: bool);

  // Activate the item itemElement. This is the recommended way to "click" a
  // menuitem in automated tests that involve menus.
  // Fires the command event for the item and then closes the menu.
  //
  // throws an InvalidStateError if the menu is not currently open, or if the
  // menuitem is not inside this menu, or if the menuitem is hidden. The menuitem
  // may be an item in a submenu, but that submenu must be open.
  //
  // @param itemElement The menuitem to activate.
  // @param options Which modifier keys and button should be set on the command
  //                event.
  #[throws]
  void activateItem(itemElement: Element,
  #[optional = {}] options: ActivateMenuItemOptions);

  // Attribute getter and setter for label.
  #[setter_throws]
  pub label: DOMString,

  // Attribute getter and setter for position.
  #[setter_throws]
  pub position: DOMString,

  // Returns the state of the popup:
  //   closed - the popup is closed
  //   open - the popup is open
  //   showing - the popup is in the process of being shown
  //   hiding - the popup is in the process of being hidden
  state: DOMString,

  // The node that triggered the popup. If the popup is not open, will return
  // None.
  #[alias = "triggerNode"]
  trigger_node: Option<Node>,

  // True if the popup is anchored to a point or rectangle. False if it
  // appears at a fixed screen coordinate.
  #[alias = "isAnchored"]
  is_anchored: bool,

  // Retrieve the anchor that was specified to openPopup or for menupopups in a
  // menu, the parent menu.
  #[alias = "anchorNode"]
  anchor_node: Option<Element>,

  // Retrieve the screen rectangle of the popup, including the area occupied by
  // any titlebar or borders present.
  #[alias = "getOuterScreenRect"]
  pub fn get_outer_screen_rect() -> DOMRect;

  // Move the popup to a point on screen in CSS pixels.
  #[alias = "moveTo"]
  pub fn move_to(left: i32, left: i32);

  // Move an open popup to the given anchor position. The arguments have the same
  // meaning as the corresponding argument to openPopup. This method has no effect
  // on popups that are not open.
  void moveToAnchor(optional Option<Element> anchorElement = None,
  #[optional = ""] position: DOMString,
  #[optional = 0] x: i32, #[optional = 0] y: i32,
  #[optional = false] attributesOverride: bool);

  // Size the popup to the given dimensions
  #[alias = "sizeTo"]
  pub fn size_to(width: i32, width: i32);

  #[alias = "setConstraintRect"]
  pub fn set_constraint_rect(rect: DOMRectReadOnly);
}
