// The origin of this IDL file is
// https://drafts.css-houdini.org/css-paint-api-1/#paintworkletglobalscope

#[Global = (Worklet, PaintWorklet),exposed = PaintWorklet]
pub trait PaintWorkletGlobalScope: WorkletGlobalScope {
  #[alias = "registerPaint"]
  pub fn register_paint(name: DOMString, name: DOMString);
}
