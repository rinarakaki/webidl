// This IDL file contains a callback used to integrate JS FinalizationRegistry
// objects with the browser.

callback FinalizationRegistryCleanupCallback = void();
