// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/#audioworkletprocessor

use super::*;

#[exposed = AudioWorklet]
pub trait AudioWorkletProcessor {
  #[alias = "constructor"]
  pub fn new() -> Result<Self>;

  pub port: MessagePort,
}
