#[exposed = (Window,Worker, Worklet), pref = "dom.streams.expose.ReadableStreamBYOBReader"]
pub trait ReadableStreamBYOBReader {
  #[alias = "constructor"]
  pub fn new(stream: ReadableStream) -> Result<Self, Error>;

  #[throws]
  pub fn read(view: ArrayBufferView) -> Promise<ReadableStreamBYOBReadResult>;

  #[throws]
  #[alias = "releaseLock"]
  pub fn release_lock();
}
ReadableStreamBYOBReader includes ReadableStreamGenericReader;

pub struct ReadableStreamBYOBReadResult {
 ArrayBufferView value;
 bool done;
}
