// The origin of this IDL file is
// https://drafts.csswg.org/web-animations/#documenttimeline

use super::{AnimationTimeline, DOMHighResTimeStamp};

pub struct DocumentTimelineOptions {
  #[alias = "originTime"]
  pub origin_time: DOMHighResTimeStamp = 0,
}

#[func = "Document::AreWebAnimationsTimelinesEnabled",
  exposed = Window]
pub trait DocumentTimeline: AnimationTimeline {
  #[alias = "constructor"]
  pub fn new(#[optional = {}] options: DocumentTimelineOptions)
    -> Result<Self>;
}
