use super::EventTarget;

#[chrome_only, exposed = Window]
pub trait WindowRoot: EventTarget {}
