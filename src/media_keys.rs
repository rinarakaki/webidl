// The origin of this IDL file is
// https://dvcs.w3.org/hg/html-media/raw-file/default/encrypted-media/encrypted-media.html

// Note: "persistent-usage-record" session type is unsupported yet, as
// it's marked as "at risk" in the spec, and Chrome doesn't support it.
pub enum MediaKeySessionType {
  #[alias = "temporary"]
  Temporary,
  #[alias = "persistent-license"]
  PersistentLicense,
  // persistent-usage-record,
}

// https://github.com/WICG/media-capabilities/blob/master/eme-extension-policy-check.md
pub struct MediaKeysPolicy {
  #[alias = "minHdcpVersion"]
  pub min_hdcp_version: DOMString = "",
}

#[exposed = Window]
pub trait MediaKeys {
  #[alias = "keySystem"]
  key_system: DOMString,

  #[new_object, throws]
  #[alias = "createSession"]
  pub fn create_session(#[optional = "temporary"] sessionType: MediaKeySessionType) -> MediaKeySession;

  #[new_object]
  #[alias = "setServerCertificate"]
  pub fn set_server_certificate(serverCertificate: BufferSource) -> Promise<void>;

  #[pref = "media.eme.hdcp-policy-check.enabled", new_object]
  #[alias = "getStatusForPolicy"]
  pub fn get_status_for_policy(#[optional = {}] policy: MediaKeysPolicy) -> Promise<MediaKeyStatus>;
}
