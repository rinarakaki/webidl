// This is a Mozilla-specific WebExtension API, which is not available to web
// content. It allows monitoring and filtering of HTTP response stream data.
//
// This API should currently be considered experimental, and is not defined by
// any standard.

#[func = "mozilla::extensions::StreamFilter::IsAllowedInContext",
 exposed = Window]
pub trait StreamFilterDataEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: StreamFilterDataEventInit);

  // Contains a chunk of data read from the input stream.
  #[pure]
  data: ArrayBuffer,
}

pub struct StreamFilterDataEventInit: EventInit {
  required ArrayBuffer data;
}

