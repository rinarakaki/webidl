// Corresponds to http://www.w3.org/TR/2002/WD-DOM-Level-3-XPath-20020208

#[exposed = Window]
pub trait XPathResult {
  // XPathResultType
  const u16 ANY_TYPE = 0;
  const u16 NUMBER_TYPE = 1;
  const u16 STRING_TYPE = 2;
  const u16 BOOLEAN_TYPE = 3;
  const u16 UNORDERED_NODE_ITERATOR_TYPE = 4;
  const u16 ORDERED_NODE_ITERATOR_TYPE = 5;
  const u16 UNORDERED_NODE_SNAPSHOT_TYPE = 6;
  const u16 ORDERED_NODE_SNAPSHOT_TYPE = 7;
  const u16 ANY_UNORDERED_NODE_TYPE = 8;
  const u16 FIRST_ORDERED_NODE_TYPE = 9;

  #[alias = "resultType"]
  result_type: u16,
  #[throws]
  #[alias = "numberValue"]
  number_value: f64,
  #[throws]
  #[alias = "stringValue"]
  string_value: DOMString,
  #[throws]
  #[alias = "booleanValue"]
  boolean_value: bool,
  #[throws]
  #[alias = "singleNodeValue"]
  single_node_value: Option<Node>,
  #[alias = "invalidIteratorState"]
  invalid_iterator_state: bool,
  #[throws]
  #[alias = "snapshotLength"]
  snapshot_length: u32,
  #[throws]
  #[alias = "iterateNext"]
  pub fn iterate_next() -> Option<Node>;
  #[throws]
  #[alias = "snapshotItem"]
  pub fn snapshot_item(index: u32) -> Option<Node>;
}
