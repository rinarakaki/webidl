use super::*;

#[chrome_only, exposed = Window]
pub trait ChromeNodeList: NodeList {
  #[alias = "constructor"]
  pub fn new() -> Self;

  pub fn append(&self, a_node: Node) -> Result<()>;
  pub fn remove(&self, a_node: Node) -> Result<()>;
}
