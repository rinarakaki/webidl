// https://w3c.github.io/deviceorientation/

#[LegacyNoInterfaceObject, exposed = Window]
pub trait DeviceAcceleration {
  x: Option<f64>,
  y: Option<f64>,
  z: Option<f64>,
}

#[LegacyNoInterfaceObject, exposed = Window]
pub trait DeviceRotationRate {
  alpha: Option<f64>,
  beta: Option<f64>,
  gamma: Option<f64>,
}

#[pref = "device.sensors.motion.enabled",
  func = "nsGlobalWindowInner::DeviceSensorsEnabled",
  exposed = Window]
pub trait DeviceMotionEvent: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] eventInitDict: DeviceMotionEventInit);

  acceleration: Option<DeviceAcceleration>,
  #[alias = "accelerationIncludingGravity"]
  acceleration_including_gravity: Option<DeviceAcceleration>,
  #[alias = "rotationRate"]
  rotation_rate: Option<DeviceRotationRate>,
  interval: Option<f64>,
}

pub struct DeviceAccelerationInit {
  pub x: Option<f64> = None,
  pub y: Option<f64> = None,
  pub z: Option<f64> = None,
}

pub struct DeviceRotationRateInit {
  pub alpha: Option<f64> = None,
  pub beta: Option<f64> = None,
  pub gamma: Option<f64> = None,
}

pub struct DeviceMotionEventInit: EventInit {
  // FIXME: bug 1493860: should this "= {}" be Option<here>
  pub acceleration: DeviceAccelerationInit = {},
  // FIXME: bug 1493860: should this "= {}" be Option<here>
  #[alias = "accelerationIncludingGravity"]
  pub acceleration_including_gravity: DeviceAccelerationInit = {},
  // FIXME: bug 1493860: should this "= {}" be Option<here>
  #[alias = "rotationRate"]
  pub rotation_rate: DeviceRotationRateInit = {},
  pub interval: Option<f64> = None,
}

// Mozilla extensions.
partial trait DeviceMotionEvent {
  void initDeviceMotionEvent(type: DOMString,
  #[optional = false] canBubble: bool,
  #[optional = false] cancelable: bool,
  #[optional = {}] acceleration: DeviceAccelerationInit,
  #[optional = {}] accelerationIncludingGravity: DeviceAccelerationInit,
  #[optional = {}] rotationRate: DeviceRotationRateInit,
  optional Option<f64> interval = None);
}
