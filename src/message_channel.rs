// For more information on this trait, please see
// http://www.whatwg.org/specs/web-apps/current-work/#channel-messaging

#[exposed = (Window, Worker)]
pub trait MessageChannel {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  port1: MessagePort,
  port2: MessagePort,
}
