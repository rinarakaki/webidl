// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLDivElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;
}

partial trait HTMLDivElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
}
