// The origin of this IDL file is
// http://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html
// http://w3c.github.io/push-api/
// https://notifications.spec.whatwg.org/

#[Global = (Worker, ServiceWorker),
 exposed = ServiceWorker]
pub trait ServiceWorkerGlobalScope: WorkerGlobalScope {
  #[same_object, binary_name = "GetClients"]
  clients: Clients,
  #[same_object]
  registration: ServiceWorkerRegistration,

  #[throws, new_object]
  #[alias = "skipWaiting"]
  pub fn skip_waiting() -> Promise<void>;

  #[alias = "oninstall"]
  pub oninstall: EventHandler,
  #[alias = "onactivate"]
  pub onactivate: EventHandler,

  #[alias = "onfetch"]
  pub onfetch: EventHandler,

  // The event.source of these MessageEvents are instances of Client
  #[alias = "onmessage"]
  pub on_message: EventHandler,
  #[alias = "onmessageerror"]
  pub on_messageerror: EventHandler,
}

// These are from w3c.github.io/push-api/
partial trait ServiceWorkerGlobalScope {
  #[alias = "onpush"]
  pub onpush: EventHandler,
  #[alias = "onpushsubscriptionchange"]
  pub onpushsubscriptionchange: EventHandler,
}

// https://notifications.spec.whatwg.org/
partial trait ServiceWorkerGlobalScope {
  #[alias = "onnotificationclick"]
  pub onnotificationclick: EventHandler,
  #[alias = "onnotificationclose"]
  pub onnotificationclose: EventHandler,
}

// Mixin the WebExtensions API globals (the actual properties are only available to
// extension service workers, locked behind a func = "extensions::ExtensionAPIAllowed" annotation).
ServiceWorkerGlobalScope includes ExtensionGlobalsMixin;
