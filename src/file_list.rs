// The origin of this IDL file is
// http://dev.w3.org/2006/webapi/FileAPI/

#[exposed = (Window, Worker)]
pub trait FileList {
  getter Option<File> item(index: u32);
  length: u32,
}
