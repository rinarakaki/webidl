// The origin of this IDL file is
// https://svgwg.org/svg2-draft/types.html#InterfaceSVGAnimatedEnumeration

#[exposed = Window]
pub trait SVGAnimatedEnumeration {
  #[setter_throws]
  #[alias = "baseVal"]
  pub base_val: u16,
  #[alias = "animVal"]
  anim_val: u16,
}
