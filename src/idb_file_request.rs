#[exposed = Window]
pub trait IDBFileRequest: DOMRequest {
  #[alias = "fileHandle"]
  file_handle: Option<IDBFileHandle>,
  // this is deprecated due to renaming in the spec
  #[alias = "lockedFile"]
  locked_file: Option<IDBFileHandle>, // now fileHandle

  #[alias = "onprogress"]
  pub on_progress: EventHandler,
}
