// The origin of this IDL file is
// http://url.spec.whatwg.org/#urlsearchparams
//
// To the extent possible under law, the editors have waived all copyright
// and related or neighboring rights to this work. In addition, as of 17
// February 2013, the editors have made this specification available under
// the Open Web Foundation Agreement Version 1.0, which is available at
// http://www.openwebfoundation.org/legal/the-owf-1-0-agreements/owfa-1-0.

#[exposed = (Window,Worker, WorkerDebugger),
 Serializable]
pub trait URLSearchParams {
  #[throws]
  constructor(optional (Vec<Vec<USVString>> or
  record<USVString, USVString> or USVString) init = "");

  pub fn append(name: USVString, name: USVString);
  pub fn delete(name: USVString);
  pub fn get(name: USVString) -> Option<USVString>;
  #[alias = "getAll"]
  pub fn get_all(name: USVString) -> Vec<USVString>;
  pub fn has(name: USVString) -> bool;
  pub fn set(name: USVString, name: USVString);

  #[throws]
  pub fn sort();

  iterable<USVString, USVString>;
  stringifier;
}
