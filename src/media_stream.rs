// The origins of this IDL file are
// http://dev.w3.org/2011/webrtc/editor/getusermedia.html

// These dictionaries need to be in a separate file from their
// MediaTrackConstraints* counterparts due to a webidl compiler limitation.

pub struct MediaStreamConstraints {
  (bool or MediaTrackConstraints) audio = false;
  (bool or MediaTrackConstraints) video = false;
  pub picture: bool = false, // Mozilla legacy
  pub fake: bool, // For testing purpose. Generates frames of solid
  // colors if video is enabled, and sound of 1Khz sine
  // wave if audio is enabled.
  #[alias = "peerIdentity"]
  pub peer_identity: Option<DOMString> = None,
}

pub struct DisplayMediaStreamConstraints {
  (bool or MediaTrackConstraints) video = true;
  (bool or MediaTrackConstraints) audio = false;
}

#[exposed = Window]
pub trait MediaStream: EventTarget {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;
  #[throws]
  #[alias = "constructor"]
  pub fn new(tracks: Vec<MediaStreamTrack>) -> Self;

  id: DOMString,
  Vec<MediaStreamTrack> getAudioTracks ();
  Vec<MediaStreamTrack> getVideoTracks ();
  Vec<MediaStreamTrack> getTracks ();
  Option<MediaStreamTrack> getTrackById (trackId: DOMString);
  void addTrack (track: MediaStreamTrack);
  void removeTrack (track: MediaStreamTrack);
  MediaStream clone ();
  active: bool,
  #[alias = "onaddtrack"]
  pub onaddtrack: EventHandler,
  #[alias = "onremovetrack"]
  pub on_removetrack: EventHandler,

  #[chrome_only, throws]
  static Promise<i32> countUnderlyingStreams();

  // Webrtc allows the remote side to name a stream whatever it wants, and we
  // need to surface this to content.
  #[chrome_only]
  #[alias = "assignId"]
  pub fn assign_id(id: DOMString);
}
