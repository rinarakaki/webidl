// The origin of this IDL file is
// https://streams.spec.whatwg.org/#rbs-controller-class-definition

#[exposed = (Window,Worker, Worklet), pref = "dom.streams.expose.ReadableByteStreamController"]
pub trait ReadableByteStreamController {
  #[throws]
  // throws on OOM
  #[alias = "byobRequest"]
  byob_request: Option<ReadableStreamBYOBRequest>,

  readonly attribute unrestricted Option<f64> desiredSize;

  #[throws]
  pub fn close();

  #[throws]
  pub fn enqueue(chunk: ArrayBufferView);

  #[throws]
  pub fn error(optional e: any);
}
