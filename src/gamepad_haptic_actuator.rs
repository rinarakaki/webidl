// The origin of this IDL file is
// https://w3c.github.io/gamepad/extensions.html#gamepadhapticactuator-trait

pub enum GamepadHapticActuatorType {
  #[alias = "vibration"]
  Vibration
}

#[pref = "dom.gamepad.extensions.enabled",
 HeaderFile = "mozilla/dom/GamepadHapticActuator.h",
 exposed = Window,
 SecureContext]
pub trait GamepadHapticActuator {
  type: GamepadHapticActuatorType,
  #[throws, new_object]
  pub fn pulse(value: f64, value: f64) -> Promise<bool>;
}
