// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

pub trait mixin SVGAnimatedPoints {
  #[constant]
  points: SVGPointList,
  #[constant]
  #[alias = "animatedPoints"]
  animated_points: SVGPointList,
}

