// The origin of this IDL file is
// http://www.w3.org/TR/WebCryptoAPI/

pub type KeyType = DOMString;
pub type KeyUsage = DOMString;
pub type NamedCurve = DOMString;
pub type BigInteger = Uint8Array;

//**** Algorithm dictionaries ****
pub struct Algorithm {
  required DOMString name;
}

#[GenerateInit]
pub struct AesCbcParams: Algorithm {
  required BufferSource iv;
}

#[GenerateInit]
pub struct AesCtrParams: Algorithm {
  required BufferSource counter;
  required #[EnforceRange] octet length;
}

#[GenerateInit]
pub struct AesGcmParams: Algorithm {
  required BufferSource iv;
  #[alias = "additionalData"]
  pub additional_data: BufferSource,
  #[EnforceRange]
  #[alias = "tagLength"]
  pub tag_length: octet,
}

pub struct HmacImportParams: Algorithm {
  required AlgorithmIdentifier hash;
}

#[GenerateInit]
pub struct Pbkdf2Params: Algorithm {
  required BufferSource salt;
  required #[EnforceRange] u32 iterations;
  required AlgorithmIdentifier hash;
}

#[GenerateInit]
pub struct RsaHashedImportParams {
  required AlgorithmIdentifier hash;
}

pub struct AesKeyGenParams: Algorithm {
  required #[EnforceRange] u16 length;
}

#[GenerateInit]
pub struct HmacKeyGenParams: Algorithm {
  required AlgorithmIdentifier hash;
  #[EnforceRange]
  pub length: u32,
}

#[GenerateInit]
pub struct RsaHashedKeyGenParams: Algorithm {
  required #[EnforceRange] u32 modulusLength;
  required BigInteger publicExponent;
  required AlgorithmIdentifier hash;
}

#[GenerateInit]
pub struct RsaOaepParams: Algorithm {
  pub label: BufferSource,
}

#[GenerateInit]
pub struct RsaPssParams: Algorithm {
  required #[EnforceRange] u32 saltLength;
}

#[GenerateInit]
pub struct EcKeyGenParams: Algorithm {
  required NamedCurve namedCurve;
}

#[GenerateInit]
pub struct AesDerivedKeyParams: Algorithm {
  required #[EnforceRange] u32 length;
}

#[GenerateInit]
pub struct HmacDerivedKeyParams: HmacImportParams {
  #[EnforceRange]
  pub length: u32,
}

#[GenerateInit]
pub struct EcdhKeyDeriveParams: Algorithm {
  required CryptoKey public;
}

#[GenerateInit]
pub struct DhImportKeyParams: Algorithm {
  required BigInteger prime;
  required BigInteger generator;
}

#[GenerateInit]
pub struct EcdsaParams: Algorithm {
  required AlgorithmIdentifier hash;
}

#[GenerateInit]
pub struct EcKeyImportParams: Algorithm {
  #[alias = "namedCurve"]
  pub named_curve: NamedCurve,
}

#[GenerateInit]
pub struct HkdfParams: Algorithm {
  required AlgorithmIdentifier hash;
  required BufferSource salt;
  required BufferSource info;
}

//**** JWK ****
pub struct RsaOtherPrimesInfo {
  // The following fields are defined in Section 6.3.2.7 of JSON Web Algorithms
  required DOMString r;
  required DOMString d;
  required DOMString t;
}

#[GenerateInitFromJSON, GenerateToJSON]
pub struct JsonWebKey {
  // The following fields are defined in Section 3.1 of JSON Web Key
  required DOMString kty;
  pub use: DOMString,
  pub key_ops: Vec<DOMString>,
  pub alg: DOMString,

  // The following fields are defined in JSON Web Key Parameters Registration
  pub ext: bool,

  // The following fields are defined in Section 6 of JSON Web Algorithms
  pub crv: DOMString,
  pub x: DOMString,
  pub y: DOMString,
  pub d: DOMString,
  pub n: DOMString,
  pub e: DOMString,
  pub p: DOMString,
  pub q: DOMString,
  pub dp: DOMString,
  pub dq: DOMString,
  pub qi: DOMString,
  pub oth: Vec<RsaOtherPrimesInfo>,
  pub k: DOMString,
}

//**** The Main API ****
#[Serializable,
 SecureContext,
 exposed = Window]
pub trait CryptoKey {
  type: KeyType,
  extractable: bool,
  #[Cached, constant, throws]
  algorithm: object,
  #[Cached, constant, Frozen]
  usages: Vec<KeyUsage>,
}

#[GenerateConversionToJS]
pub struct CryptoKeyPair {
  required CryptoKey publicKey;
  required CryptoKey privateKey;
}

pub type KeyFormat = DOMString;
pub type AlgorithmIdentifier = (object or DOMString);

#[exposed = (Window, Worker),
 SecureContext]
pub trait SubtleCrypto {
  #[throws]
  Promise<any> encrypt(algorithm: AlgorithmIdentifier,
  key: CryptoKey,
  data: BufferSource);
  #[throws]
  Promise<any> decrypt(algorithm: AlgorithmIdentifier,
  key: CryptoKey,
  data: BufferSource);
  #[throws]
  Promise<any> sign(algorithm: AlgorithmIdentifier,
  key: CryptoKey,
  data: BufferSource);
  #[throws]
  Promise<any> verify(algorithm: AlgorithmIdentifier,
  key: CryptoKey,
  signature: BufferSource,
  data: BufferSource);
  #[throws]
  Promise<any> digest(algorithm: AlgorithmIdentifier,
  data: BufferSource);

  #[throws]
  Promise<any> generateKey(algorithm: AlgorithmIdentifier,
  extractable: bool,
  Vec<KeyUsage> keyUsages );
  #[throws]
  Promise<any> deriveKey(algorithm: AlgorithmIdentifier,
  CryptoKey baseKey,
  AlgorithmIdentifier derivedKeyType,
  extractable: bool,
  Vec<KeyUsage> keyUsages );
  #[throws]
  Promise<any> deriveBits(algorithm: AlgorithmIdentifier,
  CryptoKey baseKey,
  length: u32);

  #[throws]
  Promise<any> importKey(format: KeyFormat,
  object keyData,
  algorithm: AlgorithmIdentifier,
  extractable: bool,
  Vec<KeyUsage> keyUsages );
  #[throws]
  #[alias = "exportKey"]
  pub fn export_key(format: KeyFormat, format: KeyFormat) -> Promise<any>;

  #[throws]
  Promise<any> wrapKey(format: KeyFormat,
  key: CryptoKey,
  CryptoKey wrappingKey,
  AlgorithmIdentifier wrapAlgorithm);

  #[throws]
  Promise<any> unwrapKey(format: KeyFormat,
  BufferSource wrappedKey,
  CryptoKey unwrappingKey,
  AlgorithmIdentifier unwrapAlgorithm,
  AlgorithmIdentifier unwrappedKeyAlgorithm,
  extractable: bool,
  Vec<KeyUsage> keyUsages );
}

