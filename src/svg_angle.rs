// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAngle {

  // Angle Unit Types
  const u16 SVG_ANGLETYPE_UNKNOWN = 0;
  const u16 SVG_ANGLETYPE_UNSPECIFIED = 1;
  const u16 SVG_ANGLETYPE_DEG = 2;
  const u16 SVG_ANGLETYPE_RAD = 3;
  const u16 SVG_ANGLETYPE_GRAD = 4;

  #[alias = "unitType"]
  unit_type: u16,
  #[setter_throws]
  pub value: f32,
  #[setter_throws]
  #[alias = "valueInSpecifiedUnits"]
  pub value_in_specified_units: f32,
  #[setter_throws]
  #[alias = "valueAsString"]
  pub value_as_string: DOMString,

  #[throws]
  #[alias = "newValueSpecifiedUnits"]
  pub fn new_value_specified_units(unitType: u16, unitType: u16);
  #[throws]
  #[alias = "convertToSpecifiedUnits"]
  pub fn convert_to_specified_units(unitType: u16);
}

