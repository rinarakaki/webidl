// The origin of this IDL file is
// http://dom.spec.whatwg.org/#element and
// http://domparsing.spec.whatwg.org/ and
// http://dev.w3.org/csswg/cssom-view/ and
// http://www.w3.org/TR/selectors-api/

#[exposed = Window]
pub trait Element: Node {
  #[constant]
  #[alias = "namespaceURI"]
  namespace_uri: Option<DOMString>,

  #[constant]
  prefix: Option<DOMString>,

  #[constant]
  #[alias = "localName"]
  local_name: DOMString,

  // Not #[constant] because it depends on which document we're in
  #[pure]
  #[alias = "tagName"]
  tag_name: DOMString,

  #[ce_reactions, pure]
  pub id: DOMString,

  #[ce_reactions, pure]
  #[alias = "className"]
  pub class_name: DOMString,

  #[constant, put_forwards = value]
  #[alias = "classList"]
  class_list: DOMTokenList,

  // https://drafts.csswg.org/css-shadow-parts/#idl
  #[same_object, put_forwards = value]
  part: DOMTokenList,

  #[same_object]
  attributes: NamedNodeMap,

  #[pure]
  #[alias = "getAttributeNames"]
  pub fn get_attribute_names() -> Vec<DOMString>;

  #[pure]
  #[alias = "getAttribute"]
  pub fn get_attribute(name: DOMString) -> Option<DOMString>;

  #[pure]
  #[alias = "getAttributeNS"]
  pub fn get_attribute_ns(namespace: Option<DOMString>, namespace: Option<DOMString>) -> Option<DOMString>;

  #[ce_reactions, needs_subject_principal = NonSystem, throws]
  #[alias = "toggleAttribute"]
  pub fn toggle_attribute(name: DOMString, optional force: bool) -> bool;

  #[ce_reactions, needs_subject_principal = NonSystem, throws]
  #[alias = "setAttribute"]
  pub fn set_attribute(name: DOMString, name: DOMString);

  #[ce_reactions, needs_subject_principal = NonSystem, throws]
  #[alias = "setAttributeNS"]
  pub fn set_attribute_ns(namespace: Option<DOMString>, namespace: Option<DOMString>, namespace: Option<DOMString>);

  #[ce_reactions, throws]
  #[alias = "removeAttribute"]
  pub fn remove_attribute(name: DOMString);

  #[ce_reactions, throws]
  #[alias = "removeAttributeNS"]
  pub fn remove_attribute_ns(namespace: Option<DOMString>, namespace: Option<DOMString>);

  #[pure]
  #[alias = "hasAttribute"]
  pub fn has_attribute(name: DOMString) -> bool;

  #[pure]
  #[alias = "hasAttributeNS"]
  pub fn has_attribute_ns(namespace: Option<DOMString>, namespace: Option<DOMString>) -> bool;

  #[pure]
  #[alias = "hasAttributes"]
  pub fn has_attributes() -> bool;

  #[throws, pure]
  pub fn closest(selector: UTF8String) -> Option<Element>;

  #[throws, pure]
  pub fn matches(selector: UTF8String) -> bool;

  #[throws, pure, binary_name = "matches"]
  #[alias = "webkitMatchesSelector"]
  pub fn webkit_matches_selector(selector: UTF8String) -> bool;

  #[pure]
  #[alias = "getElementsByTagName"]
  pub fn get_elements_by_tag_name(localName: DOMString) -> HTMLCollection;

  #[throws, pure]
  #[alias = "getElementsByTagNameNS"]
  pub fn get_elements_by_tag_name_ns(namespace: Option<DOMString>, namespace: Option<DOMString>) -> HTMLCollection;

  #[pure]
  #[alias = "getElementsByClassName"]
  pub fn get_elements_by_class_name(classNames: DOMString) -> HTMLCollection;

  // historical
  #[ce_reactions, throws]
  #[alias = "insertAdjacentElement"]
  pub fn insert_adjacent_element(where: DOMString, where: DOMString) -> Option<Element>;

  // historical
  #[alias = "insertAdjacentText"]
  pub fn insert_adjacent_text(where: DOMString, where: DOMString) -> Result<(), Error>;

  // The ratio of font-size-inflated text font size to computed font
  // size for this element. This will query the element for its primary frame,
  // and then use this to get font size inflation information about the frame.
  // This will be 1.0 if font size inflation is not enabled, and -1.0 if an
  // error occurred during the retrieval of the font size inflation.
  //
  // @note The font size inflation ratio that is returned is actually the
  //       font size inflation data for the element's _primary frame_, not the
  //       element itself, but for most purposes, this should be sufficient.
  #[chrome_only]
  #[alias = "fontSizeInflation"]
  font_size_inflation: f32,

  // Returns the pseudo-element string if this element represents a
  // pseudo-element, or None otherwise.
  #[chrome_only]
  #[alias = "implementedPseudoElement"]
  implemented_pseudo_element: Option<DOMString>,

  // Selectors API
  // Returns whether this element would be selected by the given selector
  // string.
  //
  // See <http://dev.w3.org/2006/webapi/selectors-api2/#matchesselector>
  #[throws, pure, binary_name = "matches"]
  #[alias = "mozMatchesSelector"]
  pub fn moz_matches_selector(selector: UTF8String) -> bool;

  // Pointer events methods.
  #[use_counter]
  #[alias = "setPointerCapture"]
  pub fn set_pointer_capture(pointerId: i32) -> Result<(), Error>;

  #[use_counter]
  #[alias = "releasePointerCapture"]
  pub fn release_pointer_capture(pointerId: i32) -> Result<(), Error>;

  #[alias = "hasPointerCapture"]
  pub fn has_pointer_capture(pointerId: i32) -> bool;

  // Proprietary extensions
  // Set this during a mousedown event to grab and retarget all mouse events
  // to this element until the mouse button is released or releaseCapture is
  // called. If retargetToElement is true, then all events are targetted at
  // this element. If false, events can also fire at descendants of this
  // element.
  //
  #[deprecated = ElementSetCapture, pref = "dom.mouse_capture.enabled"]
  #[alias = "setCapture"]
  pub fn set_capture(#[optional = false] retargetToElement: bool);

  // If this element has captured the mouse, release the capture. If another
  // element has captured the mouse, this method has no effect.
  #[deprecated = ElementReleaseCapture, pref = "dom.mouse_capture.enabled"]
  #[alias = "releaseCapture"]
  pub fn release_capture();

  //
  // Chrome-only version of setCapture that works outside of a mousedown event.
  #[chrome_only]
  #[alias = "setCaptureAlways"]
  pub fn set_capture_always(#[optional = false] retargetToElement: bool);

  // Mozilla extensions

  // Obsolete methods.
  #[alias = "getAttributeNode"]
  pub fn get_attribute_node(name: DOMString) -> Option<Attr>;

  #[ce_reactions, throws]
  #[alias = "setAttributeNode"]
  pub fn set_attribute_node(newAttr: Attr) -> Option<Attr>;

  #[ce_reactions, throws]
  #[alias = "removeAttributeNode"]
  pub fn remove_attribute_node(oldAttr: Attr) -> Option<Attr>;

  #[alias = "getAttributeNodeNS"]
  pub fn get_attribute_node_ns(namespaceURI: Option<DOMString>, namespaceURI: Option<DOMString>) -> Option<Attr>;

  #[ce_reactions, throws]
  #[alias = "setAttributeNodeNS"]
  pub fn set_attribute_node_ns(newAttr: Attr) -> Option<Attr>;

  #[func = "nsContentUtils::IsCallerChromeOrElementTransformGettersEnabled"]
  #[alias = "getTransformToAncestor"]
  pub fn get_transform_to_ancestor(ancestor: Element) -> DOMMatrixReadOnly;

  #[func = "nsContentUtils::IsCallerChromeOrElementTransformGettersEnabled"]
  #[alias = "getTransformToParent"]
  pub fn get_transform_to_parent() -> DOMMatrixReadOnly;

  #[func = "nsContentUtils::IsCallerChromeOrElementTransformGettersEnabled"]
  #[alias = "getTransformToViewport"]
  pub fn get_transform_to_viewport() -> DOMMatrixReadOnly;
}

// https://html.spec.whatwg.org/#focus-management-apis
pub struct FocusOptions {
  #[alias = "preventScroll"]
  pub prevent_scroll: bool = false,

  // Prevents the focus ring if this is not a text control / editable element.
  #[func = "nsContentUtils::IsCallerChromeOrErrorPage"]
  #[alias = "preventFocusRing"]
  pub prevent_focus_ring: bool = false,
}

pub trait mixin HTMLOrForeignElement {
  #[same_object]
  dataset: DOMStringMap,
  // See bug 1389421
  // attribute DOMString nonce; // intentionally no #[ce_reactions]

  // See bug 1575154
  // #[ce_reactions] attribute bool autofocus;
  #[ce_reactions, setter_throws, pure]
  #[alias = "tabIndex"]
  pub tab_index: i32,

  #[needs_caller_type]
  pub fn focus(#[optional = {}] options: FocusOptions) -> Result<(), Error>;

  pub fn blur() -> Result<(), Error>;
}

// https://drafts.csswg.org/cssom/#the-elementcssinlinestyle-mixin
pub trait mixin ElementCSSInlineStyle {
  #[same_object, put_forwards = cssText]
  style: CSSStyleDeclaration,
}

// http://dev.w3.org/csswg/cssom-view/
pub enum ScrollLogicalPosition { "start", "center", "end", "nearest" };

pub struct ScrollIntoViewOptions: ScrollOptions {
  pub block: ScrollLogicalPosition = "start",
  pub inline: ScrollLogicalPosition = "nearest",
}

// http://dev.w3.org/csswg/cssom-view/#extensions-to-the-element-trait
partial trait Element {
  #[alias = "getClientRects"]
  pub fn get_client_rects() -> DOMRectList;

  #[alias = "getBoundingClientRect"]
  pub fn get_bounding_client_rect() -> DOMRect;

  // scrolling
  #[alias = "scrollIntoView"]
  pub fn scroll_into_view(optional (bool or ScrollIntoViewOptions) arg = {});

  // None of the CSSOM attributes are #[pure], because they flush
  #[alias = "scrollTop"]
  pub scroll_top: i32, // scroll on setting

  #[alias = "scrollLeft"]
  pub scroll_left: i32, // scroll on setting

  #[alias = "scrollWidth"]
  scroll_width: i32,

  #[alias = "scrollHeight"]
  scroll_height: i32,

  pub fn scroll(unrestricted f64 x, unrestricted f64 y);
  pub fn scroll(#[optional = {}] options: ScrollToOptions);

  #[alias = "scrollTo"]
  pub fn scroll_to(unrestricted f64 x, unrestricted f64 y);

  #[alias = "scrollTo"]
  pub fn scroll_to(#[optional = {}] options: ScrollToOptions);

  #[alias = "scrollBy"]
  pub fn scroll_by(unrestricted f64 x, unrestricted f64 y);

  #[alias = "scrollBy"]
  pub fn scroll_by(#[optional = {}] options: ScrollToOptions);

  // mozScrollSnap is used by chrome to perform scroll snapping after the
  // user performs actions that may affect scroll position
  // mozScrollSnap is deprecated, to be replaced by a web accessible API, such
  // as an extension to the ScrollOptions pub struct. See bug 1137937.
  #[chrome_only]
  #[alias = "mozScrollSnap"]
  pub fn moz_scroll_snap();

  #[alias = "clientTop"]
  client_top: i32,
  #[alias = "clientLeft"]
  client_left: i32,
  #[alias = "clientWidth"]
  client_width: i32,
  #[alias = "clientHeight"]
  client_height: i32,

  // Mozilla specific stuff
  // The minimum/maximum offset that the element can be scrolled to
  (i.e., the value that scrollLeft/scrollTop would be clamped to if they were
  set to arbitrarily large values. #[chrome_only]
  #[alias = "scrollTopMin"]
  scroll_top_min: i32,
  #[alias = "scrollTopMax"]
  scroll_top_max: i32,
  #[chrome_only]
  #[alias = "scrollLeftMin"]
  scroll_left_min: i32,
  #[alias = "scrollLeftMax"]
  scroll_left_max: i32,
}

// http://domparsing.spec.whatwg.org/#extensions-to-the-element-trait
partial trait Element {
  #[ce_reactions, Setterneeds_subject_principal = NonSystem, pure, setter_throws, GetterCanOOM]
  attribute #[LegacyNullToEmptyString] DOMString innerHTML;

  #[ce_reactions, pure, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString outerHTML;
  
  #[ce_reactions]
  #[alias = "insertAdjacentHTML"]
  pub fn insert_adjacent_html(position: DOMString, position: DOMString) -> Result<(), Error>;
}

// http://www.w3.org/TR/selectors-api/#trait-definitions
partial trait Element {
  #[throws, pure]
  #[alias = "querySelector"]
  pub fn query_selector(selectors: UTF8String) -> Option<Element>;

  #[throws, pure]
  #[alias = "querySelectorAll"]
  pub fn query_selector_all(selectors: UTF8String) -> NodeList;
}

// https://dom.spec.whatwg.org/#dictdef-shadowrootinit
pub struct ShadowRootInit {
  required ShadowRootMode mode;

  #[pref = "dom.shadowdom.delegatesFocus.enabled"]
  #[alias = "delegatesFocus"]
  pub delegates_focus: bool = false,

  #[pref = "dom.shadowdom.slot.assign.enabled"]
  #[alias = "slotAssignment"]
  pub slot_assignment: SlotAssignmentMode = "named",
}

// https://dom.spec.whatwg.org/#element
partial trait Element {
  // Shadow DOM v1
  #[use_counter]
  #[alias = "attachShadow"]
  pub fn attach_shadow(shadow_root_init_dict: ShadowRootInit) -> Result<ShadowRoot, Error>;
  
  #[binary_name = "shadowRootByMode"]
  #[alias = "shadowRoot"]
  shadow_root: Option<ShadowRoot>,

  #[func = "Document::IsCallerChromeOrAddon", binary_name = "shadowRoot"]
  #[alias = "openOrClosedShadowRoot"]
  open_or_closed_shadow_root: Option<ShadowRoot>,

  #[binary_name = "assignedSlotByMode"]
  #[alias = "assignedSlot"]
  assigned_slot: Option<HTMLSlotElement>,

  #[chrome_only, binary_name = "assignedSlot"]
  #[alias = "openOrClosedAssignedSlot"]
  open_or_closed_assigned_slot: Option<HTMLSlotElement>,

  #[ce_reactions, Unscopable, setter_throws]
  pub slot: DOMString,
}

Element includes ChildNode;
Element includes NonDocumentTypeChildNode;
Element includes ParentNode;
Element includes Animatable;
Element includes GeometryUtils;
Element includes AccessibilityRole;
Element includes AriaAttributes;

// https://fullscreen.spec.whatwg.org/#api
partial trait Element {
  #[throws, needs_caller_type]
  #[alias = "requestFullscreen"]
  pub fn request_fullscreen() -> Promise<void>;

  #[throws, binary_name = "requestFullscreen", needs_caller_type, deprecated = "MozRequestFullScreenDeprecatedPrefix"]
  #[alias = "mozRequestFullScreen"]
  pub fn moz_request_full_screen() -> Promise<void>;

  // Events handlers
  #[alias = "onfullscreenchange"]
  pub on_fullscreen_change: EventHandler,

  #[alias = "onfullscreenerror"]
  pub on_fullscreen_error: EventHandler,
}

// https://w3c.github.io/pointerlock/#extensions-to-the-element-trait
partial trait Element {
  #[needs_caller_type, pref = "dom.pointer-lock.enabled"]
  #[alias = "requestPointerLock"]
  pub fn request_pointer_lock();
}

// Mozilla-specific additions to support devtools
partial trait Element {
  // Support reporting of Flexbox properties
  // If this element has a display:flex or display:inline-flex style,
  // this property returns an object with computed values for flex
  // properties, as well as a property that exposes the flex lines
  // in this container.
  #[chrome_only, pure]
  #[alias = "getAsFlexContainer"]
  pub fn get_as_flex_container() -> Option<Flex>;

  // Support reporting of Grid properties
  // If this element has a display:grid or display:inline-grid style,
  // this property returns an object with computed values for grid
  // tracks and lines.
  #[chrome_only, pure]
  #[alias = "getGridFragments"]
  pub fn get_grid_fragments() -> Vec<Grid>;

  // Returns whether there are any grid fragments on this element.
  #[chrome_only, pure]
  #[alias = "hasGridFragments"]
  pub fn has_grid_fragments() -> bool;

  // Returns a sequence of all the descendent elements of this element
  // that have display:grid or display:inline-grid style and generate
  // a frame.
  #[chrome_only, pure]
  #[alias = "getElementsWithGrid"]
  pub fn get_elements_with_grid() -> Vec<Element>;

  // Set attribute on the Element with a customized Content-Security-Policy
  // appropriate to devtools, which includes:
  // style-src 'unsafe-inline'
  #[chrome_only, ce_reactions]
  #[alias = "setAttributeDevtools"]
  pub fn set_attribute_devtools(name: DOMString, name: DOMString) -> Result<(), Error>;

  #[chrome_only, ce_reactions]
  #[alias = "setAttributeDevtoolsNS"]
  pub fn set_attribute_devtools_ns(namespace: Option<DOMString>, namespace: Option<DOMString>, namespace: Option<DOMString>) -> Result<(), Error>;

  // Provide a direct way to determine if this Element has visible
  // scrollbars. Flushes layout.
  #[chrome_only]
  #[alias = "hasVisibleScrollbars"]
  has_visible_scrollbars: bool,
}

// These variables are used in vtt.js, they are used for positioning vtt cues.
partial trait Element {
  // These two attributes are a f64 version of the clientHeight and the
  // clientWidth.
  #[chrome_only]
  #[alias = "clientHeightDouble"]
  client_height_double: f64,

  #[chrome_only]
  #[alias = "clientWidthDouble"]
  client_width_double: f64,

  // This attribute returns the block size of the first line box under the different
  // writing directions. If the direction is horizontal, it represents box's
  // height. If the direction is vertical, it represents box's width.
  #[chrome_only]
  #[alias = "firstLineBoxBSize"]
  first_line_box_b_size: f64,
}

// Sanitizer API, https://wicg.github.io/sanitizer-api/
pub struct SetHTMLOptions {
  pub sanitizer: Sanitizer,
}

partial trait Element {
  #[use_counter, pref = "dom.security.sanitizer.enabled"]
  #[alias = "setHTML"]
  pub fn set_html(aInnerHTML: DOMString, #[optional = {}] options: SetHTMLOptions) -> Result<(), Error>;
}
