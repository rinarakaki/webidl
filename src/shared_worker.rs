#[exposed = Window]
pub trait SharedWorker: EventTarget {
  #[throws]
  constructor(
    scriptURL: USVString,
    optional (DOMString or WorkerOptions) options = {});

  port: MessagePort,
}

SharedWorker includes AbstractWorker;
