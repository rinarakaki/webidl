// The origin of this IDL file is
// https://drafts.csswg.org/web-animations/#the-compositeoperation-enumeration
// https://drafts.csswg.org/web-animations/#dictdef-basepropertyindexedkeyframe
// https://drafts.csswg.org/web-animations/#dictdef-basekeyframe
// https://drafts.csswg.org/web-animations/#dictdef-basecomputedkeyframe

use super::*;

pub enum CompositeOperation {
  #[alias = "replace"]
  Replace,
  #[alias = "add"]
  Add,
  #[alias = "accumulate"]
  Accumulate
}

// NOTE: The order of the values in this enum are important.
//
// We assume that CompositeOperation is a subset of CompositeOperationOrAuto so
// that we can cast between the two types (provided the value is not "auto").
//
// If that assumption ceases to hold we will need to update the conversion
// in KeyframeUtils::GetAnimationPropertiesFromKeyframes.
pub enum CompositeOperationOrAuto {
  #[alias = "replace"]
  Replace,
  #[alias = "add"]
  Add,
  #[alias = "accumulate"]
  Accumulate,
  #[alias = "auto"]
  Auto
}

// The following pub struct types are not referred to by other .webidl files,
// but we use it for manual JS->IDL and IDL->JS conversions in KeyframeEffect's
// implementation.

#[GenerateInit]
pub struct BasePropertyIndexedKeyframe {
  pub offset: (Option<f64> or Vec<Option<f64>>) = [],
  pub easing: (UTF8String or Vec<UTF8String>) = [],
  pub composite: (CompositeOperationOrAuto or Vec<CompositeOperationOrAuto>) = []
}

#[GenerateInit]
pub struct BaseKeyframe {
  pub offset: Option<f64> = None,
  pub easing: UTF8String = "linear",
  #[pref = "dom.animations-api.compositing.enabled"]
  pub composite: CompositeOperationOrAuto = "auto",

  // Non-standard extensions

  // Member to allow testing when StyleAnimationValue::ComputeValues fails.
  //
  // Note that we currently only apply this to shorthand properties since
  // it's easier to annotate shorthand property values and because we have
  // only ever observed ComputeValues failing on shorthand values.
  //
  // Bug 1216844 should remove this member since after that bug is fixed we will
  // have a well-defined behavior to use when animation endpoints are not
  // available.
  #[chrome_only]
  #[alias = "simulateComputeValuesFailure"]
  pub simulate_compute_values_failure: bool = false,
}

#[GenerateConversionToJS]
pub struct BaseComputedKeyframe: BaseKeyframe {
  #[alias = "computedOffset"]
  pub computed_offset: f64,
}
