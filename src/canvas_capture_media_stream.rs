// The origin of this IDL file is
// https://w3c.github.io/mediacapture-fromelement/index.html

use super::*;

#[pref = "canvas.capturestream.enabled", exposed = Window]
pub trait CanvasCaptureMediaStream: MediaStream {
  pub canvas: HTMLCanvasElement,
  #[alias = "requestFrame"]
  pub fn request_frame(&self);
}
