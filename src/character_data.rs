// The origin of this IDL file is
// http://dom.spec.whatwg.org/#characterdata

use super::*;

#[exposed = Window]
pub trait CharacterData: Node {
  #[pure, setter_throws]
  pub mut data: #[LegacyNullToEmptyString] DOMString,
  #[pure]
  pub length: u32,

  #[alias = "substringData"]
  pub fn substring_data(&self, offset: u32, count: u32) -> Result<DOMString>;

  #[alias = "appendData"]
  pub fn append_data(&self, data: DOMString) -> Result<()>;

  #[alias = "insertData"]
  pub fn insert_data(&self, offset: u32, data: DOMString) -> Result<()>;

  #[alias = "deleteData"]
  pub fn delete_data(&self, offset: u32, count: u32) -> Result<()>;

  #[alias = "replaceData"]
  pub fn replace_data(&self, offset: u32, count: u32, data: DOMString)
    -> Result<()>;
}

CharacterData includes ChildNode;
CharacterData includes NonDocumentTypeChildNode;
