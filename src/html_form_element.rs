// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#htmlformelement

#[LegacyOverrideBuiltIns, LegacyUnenumerableNamedProperties,
 exposed = Window]
pub trait HTMLFormElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, pure, setter_throws]
  #[alias = "acceptCharset"]
  pub accept_charset: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub action: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub autocomplete: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub enctype: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub encoding: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub method: DOMString,
  #[ce_reactions, pure, setter_throws]
  pub name: DOMString,
  #[ce_reactions, pure, setter_throws]
  #[alias = "noValidate"]
  pub no_validate: bool,
  #[ce_reactions, pure, setter_throws]
  pub target: DOMString,

  #[constant]
  elements: HTMLCollection,
  #[pure]
  length: i32,

  getter Element (index: u32);
  // TODO this should be: getter (RadioNodeList or HTMLInputElement or HTMLImageElement) (name: DOMString);
  getter nsISupports (name: DOMString);

  #[throws]
  pub fn submit();
  #[throws]
  #[alias = "requestSubmit"]
  pub fn request_submit(optional Option<HTMLElement> submitter = None);
  #[ce_reactions]
  pub fn reset();
  #[alias = "checkValidity"]
  pub fn check_validity() -> bool;
  #[alias = "reportValidity"]
  pub fn report_validity() -> bool;
}
