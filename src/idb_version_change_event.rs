// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBVersionChangeEvent

pub struct IDBVersionChangeEventInit: EventInit {
  #[alias = "oldVersion"]
  pub old_version: u64 = 0,
  u32 Option<i32> newVersion = None;
}

#[exposed = (Window, Worker)]
pub trait IDBVersionChangeEvent: Event {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: IDBVersionChangeEventInit);

  #[alias = "oldVersion"]
  old_version: u64,
  readonly attribute u32 Option<i32> newVersion;
}

