// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-output-element

// http://www.whatwg.org/specs/web-apps/current-work/#the-output-element
#[exposed = Window]
pub trait HTMLOutputElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[put_forwards = value, constant]
  #[alias = "htmlFor"]
  html_for: DOMTokenList,
  form: Option<HTMLFormElement>,
  #[ce_reactions, setter_throws, pure]
  pub name: DOMString,

  #[constant]
  type: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "defaultValue"]
  pub default_value: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub value: DOMString,

  #[alias = "willValidate"]
  will_validate: bool,
  validity: ValidityState,
  #[throws]
  #[alias = "validationMessage"]
  validation_message: DOMString,
  #[alias = "checkValidity"]
  pub fn check_validity() -> bool;
  #[alias = "reportValidity"]
  pub fn report_validity() -> bool;
  #[alias = "setCustomValidity"]
  pub fn set_custom_validity(error: DOMString);

  labels: NodeList,
}
