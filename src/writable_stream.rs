// The origin of this IDL file is
// https://streams.spec.whatwg.org/#ws-class-definition

use super::WritableStreamDefaultWriter;

#[exposed = (Window, Worker, Worklet),
//Transferable See Bug 1734240
pref = "dom.streams.writable_streams.enabled"
]
pub trait WritableStream {
  #[alias = "constructor"]
  pub fn new(
    optional underlyingSink: object,
    #[optional = {}] strategy: QueuingStrategy) -> Result<Self, Error>;

  locked: bool,

  #[throws]
  pub fn abort(optional reason: any) -> Promise<void>;

  #[throws]
  pub fn close() -> Promise<void>;

  #[throws]
  #[alias = "getWriter"]
  pub fn get_writer() -> WritableStreamDefaultWriter;
}
