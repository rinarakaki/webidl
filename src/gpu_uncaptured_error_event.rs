// The origin of this IDL file is
// https://gpuweb.github.io/gpuweb/

pub struct GPUUncapturedErrorEventInit: EventInit {
  required GPUError error;
}

#[pref = "dom.webgpu.enabled",
 exposed = Window]
pub trait GPUUncapturedErrorEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;
  error: GPUError,
}
