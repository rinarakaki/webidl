// The origins of this IDL file are
// http://url.spec.whatwg.org/#api
// http://dev.w3.org/2006/webapi/FileAPI/#creating-revoking

#[exposed = (Window,Worker, WorkerDebugger),
 LegacyWindowAlias = webkitURL]
pub trait URL {
  #[alias = "constructor"]
  pub fn new(url: USVString, optional base: USVString) -> Result<Self, Error>;

  #[setter_throws]
  stringifier attribute USVString href;
  #[getter_throws]
  origin: USVString,
  #[setter_throws]
  pub protocol: USVString,
  pub username: USVString,
  pub password: USVString,
  pub host: USVString,
  pub hostname: USVString,
  pub port: USVString,
  pub pathname: USVString,
  pub search: USVString,
  #[same_object]
  #[alias = "searchParams"]
  search_params: URLSearchParams,
  pub hash: USVString,

  #[alias = "toJSON"]
  pub fn to_json() -> USVString;
}

#[exposed = (Window,DedicatedWorker, SharedWorker)]
partial trait URL {
  #[throws]
  static DOMString createObjectURL(blob: Blob);
  #[throws]
  static void revokeObjectURL(url: DOMString);
  #[chrome_only, throws]
  static bool isValidURL(url: DOMString);

  // https://dvcs.w3.org/hg/html-media/raw-file/default/media-source/media-source.html
  #[throws]
  static DOMString createObjectURL(source: MediaSource);
}
