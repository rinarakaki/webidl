// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGStyleElement: SVGElement {
  #[setter_throws]
  pub type: DOMString,
  #[setter_throws]
  pub media: DOMString,
  #[setter_throws]
  pub title: DOMString,
}

SVGStyleElement includes LinkStyle;

