pub super::*;

#[exposed = (Window, Worker)]
pub trait mixin AbstractWorker {
    #[alias = "onerror"]
    pub mut on_error: EventHandler,
}
