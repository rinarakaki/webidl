// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/multipage/common-dom-interfaces.html#htmlformcontrolscollection-0

#[exposed = Window]
pub trait RadioNodeList: NodeList {
  #[needs_caller_type]
  pub value: DOMString,
}
