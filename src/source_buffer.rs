// The origin of this IDL file is
// http://dvcs.w3.org/hg/html-media/raw-file/default/media-source/media-source.html

pub enum SourceBufferAppendMode {
  #[alias = "segments"]
  Segments,
  #[alias = "sequence"]
  Sequence
}

#[pref = "media.mediasource.enabled",
  exposed = Window]
pub trait SourceBuffer: EventTarget {
  #[setter_throws]
  pub mode: SourceBufferAppendMode,
  updating: bool,
  #[throws]
  buffered: TimeRanges,
  #[setter_throws]
  #[alias = "timestampOffset"]
  pub timestamp_offset: f64,
  //readonly attribute AudioTrackList audioTracks;
  //readonly attribute VideoTrackList videoTracks;
  //readonly attribute TextTrackList textTracks;
  #[setter_throws]
  #[alias = "appendWindowStart"]
  pub append_window_start: f64,
  #[setter_throws]
  attribute unrestricted f64 appendWindowEnd;
  #[alias = "onupdatestart"]
  pub onupdatestart: EventHandler,
  #[alias = "onupdate"]
  pub onupdate: EventHandler,
  #[alias = "onupdateend"]
  pub onupdateend: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  #[alias = "onabort"]
  pub on_abort: EventHandler,
  #[throws]
  #[alias = "appendBuffer"]
  pub fn append_buffer(data: ArrayBuffer);
  #[throws]
  #[alias = "appendBuffer"]
  pub fn append_buffer(data: ArrayBufferView);
  //#[throws]
  //void appendStream(stream: Stream, #[EnforceRange] optional maxSize: u64);
  #[throws]
  pub fn abort();
  #[throws]
  pub fn remove(start: f64, unrestricted f64 end);
}

// Mozilla extensions for experimental features
partial trait SourceBuffer {
  // Experimental function as proposed in:
  // https://github.com/w3c/media-source/issues/100 for promise proposal.
  #[throws, pref = "media.mediasource.experimental.enabled"]
  #[alias = "appendBufferAsync"]
  pub fn append_buffer_async(data: ArrayBuffer) -> Promise<void>;
  #[throws, pref = "media.mediasource.experimental.enabled"]
  #[alias = "appendBufferAsync"]
  pub fn append_buffer_async(data: ArrayBufferView) -> Promise<void>;
  #[throws, pref = "media.mediasource.experimental.enabled"]
  #[alias = "removeAsync"]
  pub fn remove_async(start: f64, unrestricted f64 end) -> Promise<void>;

  // Experimental function as proposed in:
  // https://github.com/w3c/media-source/issues/155
  #[throws]
  #[alias = "changeType"]
  pub fn change_type(type: DOMString);
}
