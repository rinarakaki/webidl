// The origin of this IDL file is
// http://dev.w3.org/csswg/css-font-loading/#FontFaceSet-trait

// To implement FontFaceSet's iterator until we can use setlike.
pub struct FontFaceSetIteratorResult {
  required any value;
  required bool done;
}

// To implement FontFaceSet's iterator until we can use setlike.
#[LegacyNoInterfaceObject,
 exposed = Window]
pub trait FontFaceSetIterator {
  #[throws]
  pub fn next() -> FontFaceSetIteratorResult;
}

callback FontFaceSetForEachCallback = void (value: FontFace, value: FontFace, value: FontFace);

pub enum FontFaceSetLoadStatus { "loading", "loaded" };

#[pref = "layout.css.font-loading-api.enabled",
 exposed = Window]
pub trait FontFaceSet: EventTarget {
  // Bug 1072762 is for the FontFaceSet constructor.
  // constructor(initialFaces: Vec<FontFace>);

  // Emulate setlike behavior until we can use that directly.
  size: u32,
  #[throws]
  pub fn add(font: FontFace);
  pub fn has(font: FontFace) -> bool;
  pub fn delete(font: FontFace) -> bool;
  pub fn clear();
  #[new_object]
  pub fn entries() -> FontFaceSetIterator;
  // Iterator keys();
  #[new_object, Alias = keys, Alias = "@@iterator"]
  pub fn values() -> FontFaceSetIterator;
  #[throws]
  #[alias = "forEach"]
  pub fn for_each(cb: FontFaceSetForEachCallback, optional thisArg: any);

  // -- events for when loading state changes
  #[alias = "onloading"]
  pub on_loading: EventHandler,
  #[alias = "onloadingdone"]
  pub on_loadingdone: EventHandler,
  #[alias = "onloadingerror"]
  pub on_loadingerror: EventHandler,

  // check and start loads if appropriate
  // and fulfill promise when all loads complete
  #[new_object]
  Promise<Vec<FontFace>> load(font: UTF8String, #[optional = " "] text: DOMString);

  // return whether all fonts in the fontlist are loaded
  // (does not initiate load if not available)
  #[throws]
  pub fn check(font: UTF8String, #[optional = " "] text: DOMString) -> bool;

  // async notification that font loading and layout operations are done
  #[throws]
  ready: Promise<void>,

  // loading state, "loading" while one or more fonts loading, "loaded" otherwise
  status: FontFaceSetLoadStatus,
}
