// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-fieldset-element

#[exposed = Window]
pub trait HTMLFieldSetElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub disabled: bool,
  form: Option<HTMLFormElement>,
  #[ce_reactions, setter_throws]
  pub name: DOMString,

  type: DOMString,

  elements: HTMLCollection,

  #[alias = "willValidate"]
  will_validate: bool,
  validity: ValidityState,
  #[throws]
  #[alias = "validationMessage"]
  validation_message: DOMString,

  #[alias = "checkValidity"]
  pub fn check_validity() -> bool;
  #[alias = "reportValidity"]
  pub fn report_validity() -> bool;

  #[alias = "setCustomValidity"]
  pub fn set_custom_validity(error: DOMString);
}
