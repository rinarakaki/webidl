// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGTextContentElement: SVGGraphicsElement {
  // lengthAdjust Types
  const u16 LENGTHADJUST_UNKNOWN = 0;
  const u16 LENGTHADJUST_SPACING = 1;
  const u16 LENGTHADJUST_SPACINGANDGLYPHS = 2;

  #[constant]
  #[alias = "textLength"]
  text_length: SVGAnimatedLength,
  #[constant]
  #[alias = "lengthAdjust"]
  length_adjust: SVGAnimatedEnumeration,

  #[alias = "getNumberOfChars"]
  pub fn get_number_of_chars() -> i32;
  #[alias = "getComputedTextLength"]
  pub fn get_computed_text_length() -> f32;
  #[throws]
  #[alias = "getSubStringLength"]
  pub fn get_sub_string_length(charnum: u32, charnum: u32) -> f32;
  #[throws]
  #[alias = "getStartPositionOfChar"]
  pub fn get_start_position_of_char(charnum: u32) -> SVGPoint;
  #[throws]
  #[alias = "getEndPositionOfChar"]
  pub fn get_end_position_of_char(charnum: u32) -> SVGPoint;
  #[new_object, throws]
  #[alias = "getExtentOfChar"]
  pub fn get_extent_of_char(charnum: u32) -> SVGRect;
  #[throws]
  #[alias = "getRotationOfChar"]
  pub fn get_rotation_of_char(charnum: u32) -> f32;
  #[alias = "getCharNumAtPosition"]
  pub fn get_char_num_at_position(#[optional = {}] point: DOMPointInit) -> i32;
  #[throws]
  #[alias = "selectSubString"]
  pub fn select_sub_string(charnum: u32, charnum: u32);
}

