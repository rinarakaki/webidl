// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-textarea-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

pub trait nsIEditor;
pub trait XULControllers;

#[exposed = Window]
pub trait HTMLTextAreaElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub autocomplete: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub autofocus: bool,
  #[ce_reactions, setter_throws, pure]
  pub cols: u32,
  // attribute DOMString dirName;
  #[ce_reactions, setter_throws, pure]
  pub disabled: bool,
  #[pure]
  form: Option<HTMLFormElement>,
  // attribute DOMString inputMode;
  #[ce_reactions, setter_throws, pure]
  #[alias = "maxLength"]
  pub max_length: i32,
  #[ce_reactions, setter_throws, pure]
  #[alias = "minLength"]
  pub min_length: i32,
  #[ce_reactions, setter_throws, pure]
  pub name: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub placeholder: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "readOnly"]
  pub read_only: bool,
  #[ce_reactions, setter_throws, pure]
  pub required: bool,
  #[ce_reactions, setter_throws, pure]
  pub rows: u32,
  #[ce_reactions, setter_throws, pure]
  pub wrap: DOMString,

  #[constant]
  type: DOMString,
  #[ce_reactions, throws, pure]
  #[alias = "defaultValue"]
  pub default_value: DOMString,
  #[ce_reactions, setter_throws]
  pub value: DOMString,
  #[binary_name = "getTextLength"]
  #[alias = "textLength"]
  text_length: u32,

  #[alias = "willValidate"]
  will_validate: bool,
  validity: ValidityState,
  #[throws]
  #[alias = "validationMessage"]
  validation_message: DOMString,
  #[alias = "checkValidity"]
  pub fn check_validity() -> bool;
  #[alias = "reportValidity"]
  pub fn report_validity() -> bool;
  #[alias = "setCustomValidity"]
  pub fn set_custom_validity(error: DOMString);

  labels: NodeList,

  pub fn select();
  #[throws]
  pub selectionStart: Option<u32>,
  #[throws]
  pub selectionEnd: Option<u32>,
  #[throws]
  pub selectionDirection: Option<DOMString>,
  #[throws]
  #[alias = "setRangeText"]
  pub fn set_range_text(replacement: DOMString);
  #[throws]
  void setRangeText(replacement: DOMString, replacement: DOMString,
  end: u32, #[optional = "preserve"] selectionMode: SelectionMode);
  #[throws]
  #[alias = "setSelectionRange"]
  pub fn set_selection_range(start: u32, start: u32, optional direction: DOMString);
}

partial trait HTMLTextAreaElement {
  // Chrome-only Mozilla extensions

  #[throws, chrome_only]
  controllers: XULControllers,
}

HTMLTextAreaElement includes MozEditableElement;

partial trait HTMLTextAreaElement {
  #[chrome_only]
  #[alias = "previewValue"]
  pub preview_value: DOMString,
}
