// The origin of this IDL file is
// http://www.w3.org/TR/geolocation-API

#[exposed = Window]
pub trait GeolocationPositionError {
  const u16 PERMISSION_DENIED = 1;
  const u16 POSITION_UNAVAILABLE = 2;
  const u16 TIMEOUT = 3;
  code: u16,
  message: DOMString,
}
