#[LegacyUnenumerableNamedProperties,
 exposed = Window]
pub trait NamedNodeMap {
  getter Option<Attr> getNamedItem(name: DOMString);
  #[ce_reactions, throws, binary_name = "setNamedItemNS"]
  #[alias = "setNamedItem"]
  pub fn set_named_item(arg: Attr) -> Option<Attr>;
  #[ce_reactions, throws]
  #[alias = "removeNamedItem"]
  pub fn remove_named_item(name: DOMString) -> Attr;

  getter Option<Attr> item(index: u32);
  length: u32,

  #[alias = "getNamedItemNS"]
  pub fn get_named_item_ns(namespaceURI: Option<DOMString>, namespaceURI: Option<DOMString>) -> Option<Attr>;
  #[ce_reactions, throws]
  #[alias = "setNamedItemNS"]
  pub fn set_named_item_ns(arg: Attr) -> Option<Attr>;
  #[ce_reactions, throws]
  #[alias = "removeNamedItemNS"]
  pub fn remove_named_item_ns(namespaceURI: Option<DOMString>, namespaceURI: Option<DOMString>) -> Attr;
}
