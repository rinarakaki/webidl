// The origin of this IDL file is
// https://drafts.csswg.org/web-animations/#the-keyframeeffect-interfaces

pub enum IterationCompositeOperation {
  #[alias = "replace"]
  Replace,
  #[alias = "accumulate"]
  Accumulate
}

pub struct KeyframeEffectOptions: EffectTiming {
  #[pref = "dom.animations-api.compositing.enabled"]
  #[alias = "iterationComposite"]
  pub iteration_composite: IterationCompositeOperation = "replace",
  #[pref = "dom.animations-api.compositing.enabled"]
  pub composite: CompositeOperation = "replace",
  #[alias = "pseudoElement"]
  pub pseudo_element: Option<DOMString> = None,
}

// KeyframeEffect should run in the caller's compartment to do custom
// processing on the `keyframes` object.
#[func = "Document::IsWebAnimationsEnabled",
 RunConstructorInCallerCompartment,
 exposed = Window]
pub trait KeyframeEffect: AnimationEffect {
  #[alias = "constructor"]
  pub fn new(
    target: Option<Element>,
    keyframes: Option<object>,
    optional (unrestricted f64 or KeyframeEffectOptions) options = {})
    -> Result<Self>;

  #[alias = "constructor"]
  pub fn new(source: KeyframeEffect) -> Result<Self>;

  pub target: Option<Element>;
  #[setter_throws]
  #[alias = "pseudoElement"]
  pub pseudo_element: Option<DOMString>;
  #[pref = "dom.animations-api.compositing.enabled"]
  #[alias = "iterationComposite"]
  pub iteration_composite: IterationCompositeOperation,
  #[pref = "dom.animations-api.compositing.enabled"]
  pub composite: CompositeOperation,

  #[alias = "getKeyframes"]
  pub fn get_keyframes() -> Result<Vec<object>>;

  #[alias = "setKeyframes"]
  pub fn set_keyframes(keyframes: Option<object>) -> Result<()>;
}

// Non-standard extensions
pub struct AnimationPropertyValueDetails {
  required f64 offset;
  pub value: UTF8String,
  pub easing: UTF8String,
  required CompositeOperation composite;
}

pub struct AnimationPropertyDetails {
  required DOMString property;
  required bool runningOnCompositor;
  pub warning: DOMString,
  required Vec<AnimationPropertyValueDetails> values;
}

partial trait KeyframeEffect {
  #[chrome_only]
  #[alias = "getProperties"]
  pub fn get_properties() -> Result<Vec<AnimationPropertyDetails>>;
}
