// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#htmlframeelement

// http://www.whatwg.org/specs/web-apps/current-work/#htmlframeelement
#[exposed = Window]
pub trait HTMLFrameElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub name: DOMString,
  #[ce_reactions, setter_throws]
  pub scrolling: DOMString,
  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws]
  pub src: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "frameBorder"]
  pub frame_border: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "longDesc"]
  pub long_desc: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "noResize"]
  pub no_resize: bool,
  #[needs_subject_principal]
  #[alias = "contentDocument"]
  content_document: Option<Document>,
  #[alias = "contentWindow"]
  content_window: Option<WindowProxy>,

  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString marginHeight;
  #[ce_reactions, setter_throws]
  attribute #[LegacyNullToEmptyString] DOMString marginWidth;
}

HTMLFrameElement includes MozFrameLoaderOwner;
