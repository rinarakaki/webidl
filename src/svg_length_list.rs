// The origin of this IDL file is
// http://www.w3.org/TR/SVG11/
// https://svgwg.org/svg2-draft/types.html#InterfaceSVGLengthList

#[exposed = Window]
pub trait SVGLengthList {
  #[alias = "numberOfItems"]
  number_of_items: u32,
  #[throws]
  pub fn clear();
  #[throws]
  pub fn initialize(newItem: SVGLength) -> SVGLength;
  #[throws]
  getter SVGLength getItem(index: u32);
  #[throws]
  #[alias = "insertItemBefore"]
  pub fn insert_item_before(newItem: SVGLength, newItem: SVGLength) -> SVGLength;
  #[throws]
  #[alias = "replaceItem"]
  pub fn replace_item(newItem: SVGLength, newItem: SVGLength) -> SVGLength;
  #[throws]
  #[alias = "removeItem"]
  pub fn remove_item(index: u32) -> SVGLength;
  #[throws]
  #[alias = "appendItem"]
  pub fn append_item(newItem: SVGLength) -> SVGLength;
  #[throws]
  setter void (index: u32, index: u32);

  // Mozilla-specific stuff
  length: u32, // synonym for numberOfItems
}
