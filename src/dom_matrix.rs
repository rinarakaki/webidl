// The origin of this IDL file is
// https://drafts.fxtf.org/geometry/

#[exposed = (Window, Worker), Serializable]
pub trait DOMMatrixReadOnly {
  #[alias = "constructor"]
  pub fn new(o
    ptional (UTF8String or Vec<unrestricted f64> or DOMMatrixReadOnly) init)
    -> Result<Self, Error>;

  #[new_object]
  #[alias = "fromMatrix"]
  static fn from_matrix(#[optional = {}] other: DOMMatrixInit)
    -> Result<Self, Error>;

  #[new_object]
  #[alias = "fromF32Array"]
  static fn from_f32_array(array32: F32Array)
    -> Result<Self, Error>;

  #[new_object]
  #[alias = "fromFloat64Array"]
  static fn from_f64_array(array64: Float64Array)
    -> Result<Self, Error>;

  // These attributes are simple aliases for certain elements of the 4x4 matrix
  readonly attribute unrestricted f64 a;
  readonly attribute unrestricted f64 b;
  readonly attribute unrestricted f64 c;
  readonly attribute unrestricted f64 d;
  readonly attribute unrestricted f64 e;
  readonly attribute unrestricted f64 f;

  readonly attribute unrestricted f64 m11;
  readonly attribute unrestricted f64 m12;
  readonly attribute unrestricted f64 m13;
  readonly attribute unrestricted f64 m14;
  readonly attribute unrestricted f64 m21;
  readonly attribute unrestricted f64 m22;
  readonly attribute unrestricted f64 m23;
  readonly attribute unrestricted f64 m24;
  readonly attribute unrestricted f64 m31;
  readonly attribute unrestricted f64 m32;
  readonly attribute unrestricted f64 m33;
  readonly attribute unrestricted f64 m34;
  readonly attribute unrestricted f64 m41;
  readonly attribute unrestricted f64 m42;
  readonly attribute unrestricted f64 m43;
  readonly attribute unrestricted f64 m44;

  // Immutable transform methods
  DOMMatrix translate(
      #[optional = 0] f64: unrestricted tx,
    #[optional = 0] f64: unrestricted ty,
    #[optional = 0] f64: unrestricted tz);

  #[new_object]
  DOMMatrix scale(
    #[optional = 1] f64: unrestricted scaleX,
    optional f64: unrestricted scaleY,
    #[optional = 1] f64: unrestricted scaleZ,
    #[optional = 0] f64: unrestricted originX,
    #[optional = 0] f64: unrestricted originY,
    #[optional = 0] f64: unrestricted originZ);

  #[new_object]
  DOMMatrix scaleNonUniform(
    #[optional = 1] f64: unrestricted scaleX,
    #[optional = 1] f64: unrestricted scaleY);

  DOMMatrix scale3d(
    #[optional = 1] f64: unrestricted scale,
    #[optional = 0] f64: unrestricted originX,
    #[optional = 0] f64: unrestricted originY,
    #[optional = 0] f64: unrestricted originZ);

  #[new_object]
  DOMMatrix rotate(
    #[optional = 0] f64: unrestricted rotX,
    optional f64: unrestricted rotY,
    optional f64: unrestricted rotZ);

  #[new_object]
  DOMMatrix rotateFromVector(
    #[optional = 0] f64: unrestricted x,
    #[optional = 0] f64: unrestricted y);

  #[new_object]
  DOMMatrix rotateAxisAngle(
    #[optional = 0] f64: unrestricted x,
    #[optional = 0] f64: unrestricted y,
    #[optional = 0] f64: unrestricted z,
    #[optional = 0] f64: unrestricted angle);

  pub fn skew_x(#[optional = 0] f64: unrestricted sx) -> DOMMatrix;
  pub fn skew_y(#[optional = 0] f64: unrestricted sy) -> DOMMatrix;

  #[new_object, throws]
  pub fn multiply(#[optional = {}] other: DOMMatrixInit) -> DOMMatrix;

  pub fn flip_x() -> DOMMatrix;
  pub fn flip_y() -> DOMMatrix;
  pub fn inverse() -> DOMMatrix;

  // Helper methods
  is2D: bool,
  #[alias = "isIdentity"]
  is_identity: bool,
  #[alias = "transformPoint"]
  pub fn transform_point(#[optional = {}] point: DOMPointInit) -> DOMPoint;

  #[throws]
  #[alias = "toF32Array"]
  pub fn to_float32Array() -> F32Array;

  #[throws]
  #[alias = "toFloat64Array"]
  pub fn to_float64Array() -> Float64Array;

  #[exposed = Window, throws]
  stringifier;

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}

#[exposed = (Window, Worker),
 Serializable,
 LegacyWindowAlias = WebKitCSSMatrix]
pub trait DOMMatrix: DOMMatrixReadOnly {
  #[alias = "constructor"]
  pub fn new(
    optional (UTF8String or Vec<unrestricted f64> or DOMMatrixReadOnly) init)
    -> Result<Self>;

  #[new_object]
  #[alias = "fromMatrix"]
  static pub fn from_matrix(#[optional = {}] other: DOMMatrixInit)
    -> Result<Self>;

  #[new_object]
  #[alias = "fromFloat32Array"]
  static pub fn from_f32_array(array32: F32Array) -> Result<Self>;

  #[new_object]
  #[alias = "fromFloat64Array"]
  static pub fn from_f64_array(array64: Float64Array) -> Result<Self>;

  // These attributes are simple aliases for certain elements of the 4x4 matrix
  inherit attribute unrestricted f64 a;
  inherit attribute unrestricted f64 b;
  inherit attribute unrestricted f64 c;
  inherit attribute unrestricted f64 d;
  inherit attribute unrestricted f64 e;
  inherit attribute unrestricted f64 f;

  inherit attribute unrestricted f64 m11;
  inherit attribute unrestricted f64 m12;
  inherit attribute unrestricted f64 m13;
  inherit attribute unrestricted f64 m14;
  inherit attribute unrestricted f64 m21;
  inherit attribute unrestricted f64 m22;
  inherit attribute unrestricted f64 m23;
  inherit attribute unrestricted f64 m24;
  inherit attribute unrestricted f64 m31;
  inherit attribute unrestricted f64 m32;
  inherit attribute unrestricted f64 m33;
  inherit attribute unrestricted f64 m34;
  inherit attribute unrestricted f64 m41;
  inherit attribute unrestricted f64 m42;
  inherit attribute unrestricted f64 m43;
  inherit attribute unrestricted f64 m44;

  // Mutable transform methods
  #[alias = "multiplySelf"]
  pub fn multiply_self(#[optional = {}] other: DOMMatrixInit) -> Result<DOMMatrix>;

  #[alias = "preMultiplySelf"]
  pub fn pre_multiply_self(#[optional = {}] other: DOMMatrixInit) -> Result<DOMMatrix>;

  DOMMatrix translateSelf(
    #[optional = 0] f64: unrestricted tx,
    #[optional = 0] f64: unrestricted ty,
    #[optional = 0] f64: unrestricted tz);

  DOMMatrix scaleSelf(
    #[optional = 1] f64: unrestricted scaleX,
    optional f64: unrestricted scaleY,
    #[optional = 1] f64: unrestricted scaleZ,
    #[optional = 0] f64: unrestricted originX,
    #[optional = 0] f64: unrestricted originY,
    #[optional = 0] f64: unrestricted originZ);

  DOMMatrix scale3dSelf(
    #[optional = 1] f64: unrestricted scale,
    #[optional = 0] f64: unrestricted originX,
    #[optional = 0] f64: unrestricted originY,
    #[optional = 0] f64: unrestricted originZ);

  DOMMatrix rotateSelf(
    #[optional = 0] f64: unrestricted rotX,
    optional f64: unrestricted rotY,
    optional f64: unrestricted rotZ);

  DOMMatrix rotateFromVectorSelf(
    #[optional = 0] f64: unrestricted x,
    #[optional = 0] f64: unrestricted y);

  DOMMatrix rotateAxisAngleSelf(
    #[optional = 0] f64: unrestricted x,
    #[optional = 0] f64: unrestricted y,
    #[optional = 0] f64: unrestricted z,
    #[optional = 0] f64: unrestricted angle);

  #[alias = "skewXSelf"]
  pub fn skew_x_self(#[optional = 0] f64: unrestricted sx) -> DOMMatrix;

  #[alias = "skewYSelf"]
  pub fn skew_y_self(#[optional = 0] f64: unrestricted sy) -> DOMMatrix;

  #[alias = "invertSelf"]
  pub fn invert_self() -> DOMMatrix;

  #[exposed = Window]
  #[alias = "setMatrixValue"]
  pub fn set_matrix_value(transformList: UTF8String) -> Result<DOMMatrix>;
}

pub struct DOMMatrix2DInit {
  unrestricted f64 a;
  unrestricted f64 b;
  unrestricted f64 c;
  unrestricted f64 d;
  unrestricted f64 e;
  unrestricted f64 f;
  unrestricted f64 m11;
  unrestricted f64 m12;
  unrestricted f64 m21;
  unrestricted f64 m22;
  unrestricted f64 m41;
  unrestricted f64 m42;
}

pub struct DOMMatrixInit: DOMMatrix2DInit {
  unrestricted f64 m13 = 0;
  unrestricted f64 m14 = 0;
  unrestricted f64 m23 = 0;
  unrestricted f64 m24 = 0;
  unrestricted f64 m31 = 0;
  unrestricted f64 m32 = 0;
  unrestricted f64 m33 = 1;
  unrestricted f64 m34 = 0;
  unrestricted f64 m43 = 0;
  unrestricted f64 m44 = 1;
  pub is2D: bool,
}
