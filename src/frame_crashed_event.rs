use super::Event;

#[chrome_only, exposed = Window]
pub trait FrameCrashedEvent: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] eventInitDict: FrameCrashedEventInit) -> Self;

  // The browsingContextId of the frame that crashed.
  #[alias = "browsingContextId"]
  browsing_context_id: u64,

  // True if the top-most frame crashed.
  #[alias = "isTopFrame"]
  is_top_frame: bool,

  // Internal process identifier of the frame that crashed. This will be
  // 0 if this identifier is not known, for example a process that failed
  // to start.
  #[alias = "childID"]
  child_id: u64,
}

pub struct FrameCrashedEventInit: EventInit {
  #[alias = "browsingContextId"]
  pub browsing_context_id: u64 = 0,
  #[alias = "isTopFrame"]
  pub is_top_frame: bool = true,
  #[alias = "childID"]
  pub child_id: u64 = 0,
}
