// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-ol-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-ol-element
#[exposed = Window]
pub trait HTMLOListElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub reversed: bool,
  #[ce_reactions, setter_throws]
  pub start: i32,
  #[ce_reactions, setter_throws]
  pub type: DOMString,
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLOListElement {
  #[ce_reactions, setter_throws]
  pub compact: bool,
}
