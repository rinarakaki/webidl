// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-html-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-html-element
#[exposed = Window]
pub trait HTMLHtmlElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLHtmlElement {
  #[ce_reactions, setter_throws, pure]
  pub version: DOMString,
}
