// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-track-element

#[exposed = Window]
pub trait HTMLTrackElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub kind: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub src: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub srclang: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub label: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub default: bool,

  const u16 NONE = 0;
  const u16 LOADING = 1;
  const u16 LOADED = 2;
  const u16 ERROR = 3;

  #[binary_name = "readyStateForBindings"]
  #[alias = "readyState"]
  ready_state: u16,

  track: Option<TextTrack>,
}
