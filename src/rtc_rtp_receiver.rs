// The origin of this IDL file is
// http://lists.w3.org/Archives/Public/public-webrtc/2014May/0067.html

#[pref = "media.peerconnection.enabled",
 exposed = Window]
pub trait RTCRtpReceiver {
  track: MediaStreamTrack,
  transport: Option<RTCDtlsTransport>,
  #[alias = "getStats"]
  pub fn get_stats() -> Promise<RTCStatsReport>;
  #[pref = "media.peerconnection.rtpsourcesapi.enabled"]
  #[alias = "getContributingSources"]
  pub fn get_contributing_sources() -> Vec<RTCRtpContributingSource>;
  #[pref = "media.peerconnection.rtpsourcesapi.enabled"]
  #[alias = "getSynchronizationSources"]
  pub fn get_synchronization_sources() -> Vec<RTCRtpSynchronizationSource>;

  // test-only: for testing getContributingSources
  #[chrome_only]
  void mozInsertAudioLevelForContributingSource(source: u32,
  timestamp: DOMHighResTimeStamp,
  u32 rtpTimestamp,
  bool hasLevel,
  level: byte);
}
