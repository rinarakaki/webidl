#[SecureContext, pref = "browser.cache.offline.enable",
exposed = Window]
pub trait OfflineResourceList: EventTarget {
  // State of the application cache this object is associated with.

  // This object is not associated with an application cache. const u16 UNCACHED = 0;

  // The application cache is not being updated. const u16 IDLE = 1;

  // The manifest is being fetched and checked for updates const u16 CHECKING = 2;

  // Resources are being downloaded to be added to the cache const u16 DOWNLOADING = 3;

  // There is a new version of the application cache available const u16 UPDATEREADY = 4;

  // The application cache group is now obsolete. const u16 OBSOLETE = 5;

  #[throws, use_counter]
  status: u16,

  // Begin the application update process on the associated application cache.
  #[throws, use_counter]
  pub fn update();

  // Swap in the newest version of the application cache, or disassociate
  // from the cache if the cache group is obsolete.
  #[throws, use_counter]
  #[alias = "swapCache"]
  pub fn swap_cache();

  // Events #[use_counter]
  #[alias = "onchecking"]
  pub onchecking: EventHandler,
  #[use_counter]
  #[alias = "onerror"]
  pub on_error: EventHandler,
  #[use_counter]
  #[alias = "onnoupdate"]
  pub onnoupdate: EventHandler,
  #[use_counter]
  #[alias = "ondownloading"]
  pub ondownloading: EventHandler,
  #[use_counter]
  #[alias = "onprogress"]
  pub on_progress: EventHandler,
  #[use_counter]
  #[alias = "onupdateready"]
  pub onupdateready: EventHandler,
  #[use_counter]
  #[alias = "oncached"]
  pub oncached: EventHandler,
  #[use_counter]
  #[alias = "onobsolete"]
  pub onobsolete: EventHandler,
}

// Mozilla extensions.
partial trait OfflineResourceList {
  // Get the list of dynamically-managed entries.
  #[throws]
  #[alias = "mozItems"]
  moz_items: DOMStringList,

  // Check that an entry exists in the list of dynamically-managed entries.
  //
  // @param uri
  //        The resource to check.
  #[throws]
  #[alias = "mozHasItem"]
  pub fn moz_has_item(uri: DOMString) -> bool;

  // Get the number of dynamically-managed entries.
  // @status DEPRECATED
  //         Clients should use the "items" attribute.
  #[throws]
  #[alias = "mozLength"]
  moz_length: u32,

  // Get the URI of a dynamically-managed entry.
  // @status DEPRECATED
  //         Clients should use the "items" attribute.
  #[throws]
  getter DOMString mozItem(index: u32);

  // We need a "length" to actually be valid Web IDL, given that we have an
  // indexed getter.
  length: u32,

  // Add an item to the list of dynamically-managed entries. The resource
  // will be fetched into the application cache.
  //
  // @param uri
  //        The resource to add.
  #[throws]
  #[alias = "mozAdd"]
  pub fn moz_add(uri: DOMString);

  // Remove an item from the list of dynamically-managed entries. If this
  // was the last reference to a URI in the application cache, the cache
  // entry will be removed.
  //
  // @param uri
  //        The resource to remove.
  #[throws]
  #[alias = "mozRemove"]
  pub fn moz_remove(uri: DOMString);
}
