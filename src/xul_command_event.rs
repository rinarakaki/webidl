// This trait is supported by command events, which are dispatched to
// XUL elements as a result of mouse or keyboard activation.
#[chrome_only, exposed = Window]
pub trait XULCommandEvent: UIEvent {
  // Command events support the same set of modifier keys as mouse and key
  // events.
  #[alias = "ctrlKey"]
  ctrl_key: bool,
  #[alias = "shiftKey"]
  shift_key: bool,
  #[alias = "altKey"]
  alt_key: bool,
  #[alias = "metaKey"]
  meta_key: bool,

  // Command events use the same button values as mouse events.
  // The button will be 0 if the command was not caused by a mouse event.
  button: short,

  // The input source, if this event was triggered by a mouse event.
  #[alias = "inputSource"]
  input_source: u16,

  // If the command event was redispatched because of a command= attribute
  // on the original target, sourceEvent will be set to the original DOM Event.
  // Otherwise, sourceEvent is None.
  #[alias = "sourceEvent"]
  source_event: Option<Event>,

  // Creates a new command event with the given attributes.
  #[throws]
  void initCommandEvent(type: DOMString,
  #[optional = false] canBubble: bool,
  #[optional = false] cancelable: bool,
  optional Option<Window> view = None,
  #[optional = 0] detail: i32,
  #[optional = false] ctrlKey: bool,
  #[optional = false] altKey: bool,
  #[optional = false] shiftKey: bool,
  #[optional = false] metaKey: bool,
  #[optional = 0] buttonArg: short,
  optional Option<Event> sourceEvent = None,
  #[optional = 0] inputSource: u16);
}
