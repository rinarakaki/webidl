// The origin of this IDL file is
// http://dev.w3.org/fxtf/web-animations/#the-animatable-interface

pub super::*;

pub struct KeyframeAnimationOptions: KeyframeEffectOptions {
  pub mut id: DOMString = "",
}

pub struct GetAnimationsOptions {
  pub subtree: bool = false,
}

pub trait mixin Animatable {
  pub fn animate(
    &self,
    keyframes: Option<object>,
    #[optional] options: UnrestrictedDoubleOrKeyframeAnimationOptions = {})
    -> Result<Animation>;

  #[func = "Document::IsWebAnimationsGetAnimationsEnabled"]
  #[alias = "getAnimations"]
  pub fn get_animations(&self, #[optional] options: GetAnimationsOptions = {})
    -> Vec<Animation>;
}
