// The origin of this IDL file is
// https://webaudio.github.io/web-midi-api/

#[SecureContext, pref = "dom.webmidi.enabled",
 exposed = Window]
pub trait MIDIAccess: EventTarget {
  inputs: MIDIInputMap,
  outputs: MIDIOutputMap,
  #[alias = "onstatechange"]
  pub onstatechange: EventHandler,
  #[alias = "sysexEnabled"]
  sysex_enabled: bool,
}
