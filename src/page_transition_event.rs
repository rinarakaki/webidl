// The PageTransitionEvent trait is used for the pageshow and
// pagehide events, which are generic events that apply to both page
// load/unload and saving/restoring a document from session history.

#[exposed = Window]
pub trait PageTransitionEvent: Event
{
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: PageTransitionEventInit);

  // Set to true if the document has been or will be persisted across
  // firing of the event. For example, if a document is being cached in
  // session history, |persisted| is true for the PageHide event.
  persisted: bool,

  // Whether the document is in the middle of a frame swap.
  #[chrome_only]
  #[alias = "inFrameSwap"]
  in_frame_swap: bool,
}

pub struct PageTransitionEventInit: EventInit
{
  pub persisted: bool = false,
  #[alias = "inFrameSwap"]
  pub in_frame_swap: bool = false,
}
