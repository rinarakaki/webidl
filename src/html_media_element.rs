// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#media-elements

#[exposed = Window]
pub trait HTMLMediaElement: HTMLElement {

  // error state
  error: Option<MediaError>,

  // network state
  #[ce_reactions, Setterneeds_subject_principal = NonSystem, setter_throws]
  pub src: DOMString,
  #[alias = "currentSrc"]
  current_src: DOMString,

  #[ce_reactions, setter_throws]
  pub crossOrigin: Option<DOMString>,
  const u16 NETWORK_EMPTY = 0;
  const u16 NETWORK_IDLE = 1;
  const u16 NETWORK_LOADING = 2;
  const u16 NETWORK_NO_SOURCE = 3;
  #[alias = "networkState"]
  network_state: u16,
  #[ce_reactions, setter_throws]
  pub preload: DOMString,
  #[new_object]
  buffered: TimeRanges,
  pub fn load();
  #[alias = "canPlayType"]
  pub fn can_play_type(type: DOMString) -> DOMString;

  // ready state
  const u16 HAVE_NOTHING = 0;
  const u16 HAVE_METADATA = 1;
  const u16 HAVE_CURRENT_DATA = 2;
  const u16 HAVE_FUTURE_DATA = 3;
  const u16 HAVE_ENOUGH_DATA = 4;
  #[alias = "readyState"]
  ready_state: u16,
  seeking: bool,

  // playback state
  #[setter_throws]
  #[alias = "currentTime"]
  pub current_time: f64,
  #[throws]
  #[alias = "fastSeek"]
  pub fn fast_seek(time: f64);
  readonly attribute unrestricted f64 duration;
  #[chrome_only]
  #[alias = "isEncrypted"]
  is_encrypted: bool,
  // TODO: Bug 847376 - readonly attribute any startDate;
  paused: bool,
  #[setter_throws]
  #[alias = "defaultPlaybackRate"]
  pub default_playback_rate: f64,
  #[setter_throws]
  #[alias = "playbackRate"]
  pub playback_rate: f64,
  #[new_object]
  played: TimeRanges,
  #[new_object]
  seekable: TimeRanges,
  ended: bool,
  #[ce_reactions, setter_throws]
  pub autoplay: bool,
  #[ce_reactions, setter_throws]
  pub loop: bool,
  #[throws]
  pub fn play() -> Promise<void>;
  #[throws]
  pub fn pause();

  // TODO: Bug 847377 - mediaGroup and MediaController
  // media controller
  //         attribute DOMString mediaGroup;
  //         pub controller: Option<MediaController>,

  // controls
  #[ce_reactions, setter_throws]
  pub controls: bool,
  #[setter_throws]
  pub volume: f64,
  pub muted: bool,
  #[ce_reactions, setter_throws]
  #[alias = "defaultMuted"]
  pub default_muted: bool,

  // TODO: Bug 847379
  // tracks
  #[pref = "media.track.enabled"]
  #[alias = "audioTracks"]
  audio_tracks: AudioTrackList,
  #[pref = "media.track.enabled"]
  #[alias = "videoTracks"]
  video_tracks: VideoTrackList,
  #[alias = "textTracks"]
  text_tracks: Option<TextTrackList>,
  TextTrack addTextTrack(kind: TextTrackKind,
  #[optional = ""] label: DOMString,
  #[optional = ""] language: DOMString);
}

// Mozilla extensions:
partial trait HTMLMediaElement {
  #[func = "HasDebuggerOrTabsPrivilege"]
  #[alias = "mozMediaSourceObject"]
  moz_media_source_object: Option<MediaSource>,

  #[func = "HasDebuggerOrTabsPrivilege", new_object]
  #[alias = "mozRequestDebugInfo"]
  pub fn moz_request_debug_info() -> Promise<HTMLMediaElementDebugInfo>;

  #[func = "HasDebuggerOrTabsPrivilege", new_object]
  static void mozEnableDebugLog();
  #[func = "HasDebuggerOrTabsPrivilege", new_object]
  #[alias = "mozRequestDebugLog"]
  pub fn moz_request_debug_log() -> Promise<DOMString>;

  pub srcObject: Option<MediaStream>,
  #[alias = "mozPreservesPitch"]
  pub moz_preserves_pitch: bool,

  // NB: for internal use with the video controls:
  #[func = "IsChromeOrUAWidget"]
  #[alias = "mozAllowCasting"]
  pub moz_allow_casting: bool,
  #[func = "IsChromeOrUAWidget"]
  #[alias = "mozIsCasting"]
  pub moz_is_casting: bool,

  // Mozilla extension: stream capture
  #[throws]
  #[alias = "mozCaptureStream"]
  pub fn moz_capture_stream() -> MediaStream;
  #[throws]
  #[alias = "mozCaptureStreamUntilEnded"]
  pub fn moz_capture_stream_until_ended() -> MediaStream;
  #[alias = "mozAudioCaptured"]
  moz_audio_captured: bool,

  // Mozilla extension: return embedded metadata from the stream as a
  // JSObject with key:value pairs for each tag. This can be used by
  // player interfaces to display the song title, artist, etc.
  #[throws]
  #[alias = "mozGetMetadata"]
  pub fn moz_get_metadata() -> Option<object>;

  // Mozilla extension: provides access to the fragment end time if
  // the media element has a fragment URI for the currentSrc, otherwise
  // it is equal to the media duration.
  #[alias = "mozFragmentEnd"]
  moz_fragment_end: f64,
}

// Encrypted Media Extensions
partial trait HTMLMediaElement {
  #[alias = "mediaKeys"]
  media_keys: Option<MediaKeys>,

  // void, not any: https://www.w3.org/Bugs/Public/show_bug.Option<cgi>id = 26457
  #[new_object]
  #[alias = "setMediaKeys"]
  pub fn set_media_keys(mediaKeys: Option<MediaKeys>) -> Promise<void>;

  #[alias = "onencrypted"]
  pub onencrypted: EventHandler,

  #[alias = "onwaitingforkey"]
  pub onwaitingforkey: EventHandler,
}

// These attributes are general testing attributes and they should only be used
// in tests.
partial trait HTMLMediaElement {
  #[pref = "media.useAudioChannelService.testing"]
  #[alias = "computedVolume"]
  computed_volume: f64,

  #[pref = "media.useAudioChannelService.testing"]
  #[alias = "computedMuted"]
  computed_muted: bool,

  // Return true if the media is suspended because its document is inactive or
  // the docshell is inactive and explicitly being set to prohibit all media
  // from playing.
  #[chrome_only]
  #[alias = "isSuspendedByInactiveDocOrDocShell"]
  is_suspended_by_inactive_doc_or_doc_shell: bool,
}

//
// HTMLMediaElement::seekToNextFrame() is a Mozilla experimental feature.
//
// The SeekToNextFrame() method provides a way to access a video element's video
// frames one by one without going through the realtime playback. So, it lets
// authors use "frame" as unit to access the video element's underlying data,
// instead of "time".
//
// The SeekToNextFrame() is a kind of seek operation, normally: so, once it is
// invoked, a "seeking" event is dispatched. However, if the media source has no
// video data or is not seekable, the operation is ignored without filing the
// "seeking" event.
//
// Once the SeekToNextFrame() is done, a "seeked" event should always be filed
// and a "ended" event might also be filed depends on where the media element's
// position before seeking was. There are two cases:
// Assume the media source has n+1 video frames where n is a non-negative
// integers and the frame sequence is indexed from zero.
// (1) If the currentTime is at anywhere smaller than the n-th frame's beginning
//     time, say the currentTime is now pointing to a position which is smaller
//     than the x-th frame's beginning time and larger or equal to the (x-1)-th
//     frame's beginning time, where x belongs to #[1, n], then the
//     SeekToNextFrame() operation seeks the media to the x-th frame, sets the
//     media's currentTime to the x-th frame's beginning time and dispatches a
//     "seeked" event.
// (2) Otherwise, if the currentTime is larger or equal to the n-th frame's
//     beginning time, then the SeekToNextFrame() operation sets the media's
//     currentTime to the duration of the media source and dispatches a "seeked"
//     event and an "ended" event.
partial trait HTMLMediaElement {
  #[throws, pref = "media.seekToNextFrame.enabled"]
  #[alias = "seekToNextFrame"]
  pub fn seek_to_next_frame() -> Promise<void>;
}

// Internal testing only APIpartial trait HTMLMediaElement {
  // These APIs are used to simulate visibility changes to help debug and write
  // tests about suspend-video-decoding.
  // - SetVisible() is for simulating visibility changes.
  // - HasSuspendTaint() is for querying that the element's decoder cannot suspend
  //   video decoding because it has been tainted by an operation, such as
  //   drawImage().
  // - isInViewPort is a bool value which indicate whether media element is
  //   in view port.
  // - isVideoDecodingSuspended() is used to know whether video decoding has
  //   suspended.
  #[pref = "media.test.video-suspend"]
  #[alias = "setVisible"]
  pub fn set_visible(aVisible: bool);

  #[pref = "media.test.video-suspend"]
  #[alias = "hasSuspendTaint"]
  pub fn has_suspend_taint() -> bool;

  #[chrome_only]
  #[alias = "isInViewPort"]
  is_in_view_port: bool,

  #[chrome_only]
  #[alias = "isVideoDecodingSuspended"]
  is_video_decoding_suspended: bool,

  #[chrome_only]
  #[alias = "totalVideoPlayTime"]
  total_video_play_time: f64,

  #[chrome_only]
  #[alias = "visiblePlayTime"]
  visible_play_time: f64,

  #[chrome_only]
  #[alias = "invisiblePlayTime"]
  invisible_play_time: f64,

  #[chrome_only]
  #[alias = "videoDecodeSuspendedTime"]
  video_decode_suspended_time: f64,

  #[chrome_only]
  #[alias = "totalAudioPlayTime"]
  total_audio_play_time: f64,

  #[chrome_only]
  #[alias = "audiblePlayTime"]
  audible_play_time: f64,

  #[chrome_only]
  #[alias = "inaudiblePlayTime"]
  inaudible_play_time: f64,

  #[chrome_only]
  #[alias = "mutedPlayTime"]
  muted_play_time: f64,

  // These APIs are used for decoder doctor tests.
  #[chrome_only]
  #[alias = "setFormatDiagnosticsReportForMimeType"]
  pub fn set_format_diagnostics_report_for_mime_type(mimeType: DOMString, mimeType: DOMString);

  #[throws, chrome_only]
  #[alias = "setDecodeError"]
  pub fn set_decode_error(error: DOMString);

  #[chrome_only]
  #[alias = "setAudioSinkFailedStartup"]
  pub fn set_audio_sink_failed_startup();
}

// Audio Output Devices API
// https://w3c.github.io/mediacapture-output/
partial trait HTMLMediaElement {
  #[SecureContext, pref = "media.setsinkid.enabled"]
  #[alias = "sinkId"]
  sink_id: DOMString,
  #[throws, SecureContext, pref = "media.setsinkid.enabled"]
  #[alias = "setSinkId"]
  pub fn set_sink_id(sinkId: DOMString) -> Promise<void>;
}

//
// API that exposes whether a call to HTMLMediaElement.play() would be
// blocked by autoplay policies; whether the promise returned by play()
// would be rejected with NotAllowedError.
partial trait HTMLMediaElement {
  #[pref = "media.allowed-to-play.enabled"]
  #[alias = "allowedToPlay"]
  allowed_to_play: bool,
}
