// The nsIDOMCloseEvent trait is the trait to the event
// close on a WebSocket object.
//
// For more information on this trait, please see
// http://www.whatwg.org/specs/web-apps/current-work/multipage/network.html#closeevent

use super::*;

#[LegacyEventInit, exposed = (Window, Worker)]
pub trait CloseEvent: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] init: CloseEventInit)
    -> Self;

  #[alias = "wasClean"]
  pub was_clean: bool,
  pub code: u16,
  pub reason: DOMString,
}

pub struct CloseEventInit: EventInit {
  #[alias = "wasClean"]
  pub #[optional = false] was_clean: bool
  pub #[optional = 0] code: u16
  pub #[optional = ""] reason: DOMString
}
