// The origin of this IDL file is
// https://streams.spec.whatwg.org/#rs-byob-request-class-definition

#[exposed = (Window,Worker, Worklet), pref = "dom.streams.expose.ReadableStreamBYOBRequest"]
pub trait ReadableStreamBYOBRequest {
  view: Option<ArrayBufferView>,

  #[throws]
  pub fn respond(#[EnforceRange] u64 bytesWritten);

  #[throws]
  #[alias = "respondWithNewView"]
  pub fn respond_with_new_view(view: ArrayBufferView);
}
