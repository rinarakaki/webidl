// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGTextPathElement: SVGTextContentElement {
  // textPath Method Types
  const u16 TEXTPATH_METHODTYPE_UNKNOWN = 0;
  const u16 TEXTPATH_METHODTYPE_ALIGN = 1;
  const u16 TEXTPATH_METHODTYPE_STRETCH = 2;

  // textPath Spacing Types
  const u16 TEXTPATH_SPACINGTYPE_UNKNOWN = 0;
  const u16 TEXTPATH_SPACINGTYPE_AUTO = 1;
  const u16 TEXTPATH_SPACINGTYPE_EXACT = 2;

  #[constant]
  #[alias = "startOffset"]
  start_offset: SVGAnimatedLength,
  #[constant]
  method: SVGAnimatedEnumeration,
  #[constant]
  spacing: SVGAnimatedEnumeration,
}

SVGTextPathElement includes SVGURIReference;

