// The origin of this IDL file is
// https://drafts.csswg.org/web-animations/#animationeffectreadonly

pub super::*;

pub enum FillMode {
  #[alias = "none"]
  None,
  #[alias = "forwards"]
  Forwards,
  #[alias = "backwards"]
  Backwards,
  #[alias = "both"]
  Both,
  #[alias = "auto"]
  Auto
}

pub enum PlaybackDirection {
  #[alias = "normal"]
  Normal,
  #[alias = "reverse"]
  Reverse,
  #[alias = "alternate"]
  Alternate,
  #[alias = "alternate-reverse"]
  AlternateReverse
}

pub struct EffectTiming {
  pub delay: f64 = 0.0,
  #[alias = "endDelay"]
  pub end_delay: f64 = 0.0,
  pub fill: FillMode = "auto",
  #[alias = "iterationStart"]
  pub iteration_start: f64 = 0.0,
  pub iterations: unrestricted f64 = 1.0,
  pub duration: (unrestricted f64 or DOMString) = "auto",
  pub direction: PlaybackDirection = "normal",
  pub easing: UTF8String = "linear",
}

pub struct OptionalEffectTiming {
  pub delay: f64,
  #[alias = "endDelay"]
  pub end_delay: f64,
  pub fill: FillMode,
  #[alias = "iterationStart"]
  pub iteration_start: f64,
  pub iterations: unrestricted f64,
  pub duration: (unrestricted f64 or DOMString),
  pub direction: PlaybackDirection,
  pub easing: UTF8String,
}

pub struct ComputedEffectTiming: EffectTiming {
  #[alias = "endTime"]
  pub end_time: unrestricted f64 = 0.0,
  #[alias = "activeDuration"]
  pub active_duration: unrestricted f64 = 0.0,
  #[alias = "localTime"]
  pub local_time: Option<f64> = None,
  pub progress: Option<f64> = None,
  #[alias = "currentIteration"]
  pub current_iteration: Option<unrestricted f64> = None,
}

#[func = "Document::IsWebAnimationsEnabled",
 exposed = Window]
pub trait AnimationEffect {
  #[alias = "getTiming"]
  pub fn get_timing(&self) -> EffectTiming;
  #[alias = "getComputedTiming"]
  pub fn get_computed_timing(&self) -> ComputedEffectTiming;
  #[alias = "updateTiming"]
  pub fn update_timing(&self,
    #[optional] timing: OptionalEffectTiming = {}) -> Result<()>;
}
