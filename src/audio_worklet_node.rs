// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

#[GenerateConversionToJS]
pub struct AudioWorkletNodeOptions: AudioNodeOptions {
  #[alias = "numberOfInputs"]
  pub number_of_inputs: u32 = 1,
  #[alias = "numberOfOutputs"]
  pub number_of_outputs: u32 = 1,
  #[alias = "outputChannelCount"]
  pub output_channel_count: Vec<u32>,
  #[alias = "parameterData"]
  pub parameter_data: record<DOMString, f64>,
  #[alias = "processorOptions"]
  pub processor_options: object,
}

#[SecureContext, pref = "dom.audioworklet.enabled",
 exposed = Window]
pub trait AudioWorkletNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(
    context: BaseAudioContext,
    name: DOMString,
    #[optional = {}] options: AudioWorkletNodeOptions) -> Result<Self>;

  #[throws]
  pub parameters: AudioParamMap,
  pub port: MessagePort,
  #[alias = "onprocessorerror"]
  pub mut on_processor_error: EventHandler,
}
