// A fake plugin is fundamentally identified by its handlerURI.
//
// In addition to that, a fake plugin registration needs to provide at least one
// FakePluginMimeEntry so we'll know what types(s) the plugin is registered for.
// Other information is optional, though having usable niceName is highly
// recommended.
#[GenerateInit]
pub struct FakePluginTagInit {
  required DOMString handlerURI;
  required Vec<FakePluginMimeEntry> mimeEntries;

  // The niceName should really be provided, and be unique, if possible; it can
  // be used as a key to persist state for this plug-in.
  #[alias = "niceName"]
  pub nice_name: DOMString = "",

  // Other things can be provided but don't really matter that much.
  #[alias = "fullPath"]
  pub full_path: DOMString = "",
  pub name: DOMString = "",
  pub description: DOMString = "",
  #[alias = "fileName"]
  pub file_name: DOMString = "",
  pub version: DOMString = "",

  // Optional script to run in a sandbox when instantiating a plugin. The script
  // runs in a sandbox with system principal in the process that contains the
  // element that instantiates the plugin (ie the EMBED or OBJECT element). The
  // sandbox global has a 'pluginElement' property that the script can use to
  // access the element that instantiates the plugin.
  #[alias = "sandboxScript"]
  pub sandbox_script: DOMString = "",
}

// A single MIME entry for the fake plugin.
pub struct FakePluginMimeEntry {
  required DOMString type;
  pub description: DOMString = "",
  pub extension: DOMString = "",
}

