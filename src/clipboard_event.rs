// For more information on this trait please see
// http://dev.w3.org/2006/webapi/clipops/#x5-clipboard-event-interfaces

use super::*;

#[exposed = Window]
pub trait ClipboardEvent: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] init: ClipboardEventInit)
    -> Result<Self>;

  #[alias = "clipboardData"]
  pub clipboard_data: Option<DataTransfer>,
}

pub struct ClipboardEventInit: EventInit {
  pub #[optional = ""] data: DOMString,
  #[alias = "dataType"]
  pub #[optional = ""] data_type: DOMString,
}
