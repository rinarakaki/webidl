// The origin of this IDL file is
// https://w3c.github.io/push-api/

#[pref = "dom.push.enabled",
 exposed = ServiceWorker]
pub trait PushMessageData {
  #[throws]
  #[alias = "arrayBuffer"]
  pub fn array_buffer() -> ArrayBuffer;
  #[throws]
  pub fn blob() -> Blob;
  #[throws]
  pub fn json() -> any;
  pub fn text() -> USVString;
}
