// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/multipage/text-level-semantics.html#the-time-element

#[exposed = Window]
pub trait HTMLTimeElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  #[alias = "dateTime"]
  pub date_time: DOMString,
}
