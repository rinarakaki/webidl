#[exposed = Window]
pub trait MouseScrollEvent: MouseEvent {
  const i32 HORIZONTAL_AXIS = 1;
  const i32 VERTICAL_AXIS = 2;

  axis: i32,

  void initMouseScrollEvent(type: DOMString,
  #[optional = false] canBubble: bool,
  #[optional = false] cancelable: bool,
  optional Option<Window> view = None,
  #[optional = 0] detail: i32,
  #[optional = 0] screenX: i32,
  #[optional = 0] screenY: i32,
  #[optional = 0] clientX: i32,
  #[optional = 0] clientY: i32,
  #[optional = false] ctrlKey: bool,
  #[optional = false] altKey: bool,
  #[optional = false] shiftKey: bool,
  #[optional = false] metaKey: bool,
  #[optional = 0] button: short,
  optional Option<EventTarget> relatedTarget = None,
  #[optional = 0] axis: i32);
}
