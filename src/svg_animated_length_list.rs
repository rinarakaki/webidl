// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimatedLengthList {
  #[constant]
  #[alias = "baseVal"]
  base_val: SVGLengthList,
  #[constant]
  #[alias = "animVal"]
  anim_val: SVGLengthList,
}

