// The origin of this IDL file is
// https://streams.spec.whatwg.org/#underlying-source-api

#[GenerateInit]
pub struct UnderlyingSource {
  pub start: UnderlyingSourceStartCallback,
  pub pull: UnderlyingSourcePullCallback,
  pub cancel: UnderlyingSourceCancelCallback,
  pub type: ReadableStreamType,
  #[EnforceRange]
  #[alias = "autoAllocateChunkSize"]
  pub auto_allocate_chunkSize: u64,
}

pub type ReadableStreamController = (ReadableStreamDefaultController or ReadableByteStreamController);

callback UnderlyingSourceStartCallback = any (controller: ReadableStreamController);
callback UnderlyingSourcePullCallback = Promise<void> (controller: ReadableStreamController);
callback UnderlyingSourceCancelCallback = Promise<void> (optional reason: any);
