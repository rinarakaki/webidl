// The origin of this IDL file is
// http://w3c.github.io/webrtc-pc/#idl-def-RTCIdentityAssertion

pub struct RTCIdentityAssertion {
  pub idp: DOMString,
  pub name: DOMString,
}
