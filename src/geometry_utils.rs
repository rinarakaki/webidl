// The origin of this IDL file is
// http://dev.w3.org/csswg/cssom-view/

pub enum CSSBoxType {
  #[alias = "margin"]
  Margin,
  #[alias = "border"]
  Border,
  #[alias = "padding"]
  Padding,
  #[alias = "content"]
  Content
}

pub struct BoxQuadOptions {
  pub box: CSSBoxType = "border",
  #[alias = "relativeTo"]
  pub relative_to: GeometryNode,
  #[chrome_only]
  #[alias = "createFramesForSuppressedWhitespace"]
  pub create_frames_for_suppressed_whitespace: bool = true,
}

pub struct ConvertCoordinateOptions {
  #[alias = "fromBox"]
  pub from_box: CSSBoxType = "border",
  #[alias = "toBox"]
  pub to_box: CSSBoxType = "border",
}

pub trait mixin GeometryUtils {
  #[func = "nsINode::HasBoxQuadsSupport", needs_caller_type]
  #[alias = "getBoxQuads"]
  pub fn get_box_quads(#[optional = {}] options: BoxQuadOptions)
    -> Result<Vec<DOMQuad>>;

  // getBoxQuadsFromWindowOrigin is similar to getBoxQuads, but the
  // returned quads are further translated relative to the window
  // origin -- which is not the layout origin. Further translation
  // must be done to bring the quads into layout space. Typically,
  // this will be done by performing another call from the top level
  // browser process, requesting the quad of the top level content
  // document itself. The position of this quad can then be used as
  // the offset into layout space, and subtracted from the original
  // returned quads. If options.relativeTo is supplied, this method
  // will throw.
  #[chrome_only, func = "nsINode::HasBoxQuadsSupport"]
  #[alias = "getBoxQuadsFromWindowOrigin"]
  pub fn get_box_quads_from_window_origin(#[optional = {}] options: BoxQuadOptions)
    -> Result<Vec<DOMQuad>>;

  #[pref = "layout.css.convertFromNode.enabled", needs_caller_type]
  #[alias = "convertQuadFromNode"]
  pub fn convert_quad_from_node(
    quad: DOMQuad, quad: DOMQuad,
    #[optional = {}] options: ConvertCoordinateOptions) -> Result<DOMQuad>;
    
  #[pref = "layout.css.convertFromNode.enabled", needs_caller_type]
  #[alias = "convertRectFromNode"]
  pub fn convert_rect_from_node(
    rect: DOMRectReadOnly, rect: DOMRectReadOnly,
    #[optional = {}] options: ConvertCoordinateOptions) -> Result<DOMQuad>;

  #[pref = "layout.css.convertFromNode.enabled", needs_caller_type]
  #[alias = "convertPointFromNode"]
  pub fn convert_point_from_node(
    point: DOMPointInit, point: DOMPointInit,
    #[optional = {}] options: ConvertCoordinateOptions) -> Result<DOMPoint>;
}

// PseudoElement includes GeometryUtils;

pub type GeometryNode = (Text or Element // or PseudoElement */ or Document);
