// The origin of this IDL file is
// https://w3c.github.io/gamepad/#gamepadevent-trait

#[pref = "dom.gamepad.enabled",
 exposed = Window,
 SecureContext]
pub trait GamepadEvent: Event
{
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: GamepadEventInit) -> Self;

  gamepad: Option<Gamepad>,
}

pub struct GamepadEventInit: EventInit
{
  pub gamepad: Option<Gamepad> = None,
}
