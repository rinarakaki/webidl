// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/getusermedia.html

#[func = "Navigator::HasUserMediaSupport",
 exposed = Window]
pub trait MediaDevices: EventTarget {
  #[pref = "media.ondevicechange.enabled"]
  #[alias = "ondevicechange"]
  pub ondevicechange: EventHandler,
  #[alias = "getSupportedConstraints"]
  pub fn get_supported_constraints() -> MediaTrackSupportedConstraints;

  #[throws, use_counter]
  Promise<Vec<MediaDeviceInfo>> enumerateDevices();

  #[throws, needs_caller_type, use_counter]
  #[alias = "getUserMedia"]
  pub fn get_user_media(#[optional = {}] constraints: MediaStreamConstraints) -> Promise<MediaStream>;

  // We need #[SecureContext] in case media.devices.insecure.enabled = true
  // because we don't want that legacy pref to expose this newer method.
  #[SecureContext, pref = "media.getdisplaymedia.enabled", throws, needs_caller_type, use_counter]
  #[alias = "getDisplayMedia"]
  pub fn get_display_media(#[optional = {}] constraints: DisplayMediaStreamConstraints) -> Promise<MediaStream>;
}

// https://w3c.github.io/mediacapture-output/#audiooutputoptions-pub struct
pub struct AudioOutputOptions {
  pub deviceId: DOMString = "",
}
// https://w3c.github.io/mediacapture-output/#mediadevices-extensions
partial trait MediaDevices {
  #[SecureContext, pref = "media.setsinkid.enabled", throws, needs_caller_type]
  #[alias = "selectAudioOutput"]
  pub fn select_audio_output(#[optional = {}] options: AudioOutputOptions) -> Promise<MediaDeviceInfo>;
}
