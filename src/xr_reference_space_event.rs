// The origin of this IDL file is
// https://immersive-web.github.io/webxr/#xrreferencespaceevent-trait

use super::{Event, XRReferenceSpace, XRRigidTransform};

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRReferenceSpaceEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;
  #[same_object]
  #[alias = "referenceSpace"]
  reference_space: XRReferenceSpace,
  #[same_object]
  transform: Option<XRRigidTransform>,
}

pub struct XRReferenceSpaceEventInit: EventInit {
  required XRReferenceSpace referenceSpace;
  //
  // Changed from "XRRigidTransform transform;" in order to work with the
  // event code generation.
  pub transform: Option<XRRigidTransform> = None,
}
