// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGAnimatedBoolean {
  #[alias = "baseVal"]
  pub base_val: bool,
  #[alias = "animVal"]
  anim_val: bool,
}

