// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

pub enum ChannelCountMode {
  #[alias = "max"]
  Max,
  #[alias = "clamped-max"]
  ClampedMax,
  #[alias = "explicit"]
  Explicit
}

pub enum ChannelInterpretation {
  #[alias = "speakers"]
  Speakers,
  #[alias = "discrete"]
  Discrete
}

pub struct AudioNodeOptions {
  #[alias = "channelCount"]
  pub channel_count: u32,
  #[alias = "channelCountMode"]
  pub channel_count_mode: ChannelCountMode,
  #[alias = "channelInterpretation"]
  pub channel_interpretation: ChannelInterpretation,
}

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait AudioNode: EventTarget {
  pub fn connect(
    &self,
    destination: AudioNode,
    #[optional = 0] output: u32,
    #[optional = 0] input: u32) -> Result<AudioNode>;
    
  pub fn connect(&self, destination: AudioParam, #[optional = 0] output: u32)
    -> Result<()>;

  pub fn disconnect(&self) -> Result<()>;
  pub fn disconnect(&self, output: u32) -> Result<()>;
  pub fn disconnect(&self, destination: AudioNode) -> Result<()>;
  pub fn disconnect(&self, destination: AudioNode, output: u32)
    -> Result<()>;
  pub fn disconnect(&self, destination: AudioNode, output: u32, input: u32)
    -> Result<()>;
  pub fn disconnect(&self, destination: AudioParam) -> Result<()>;
  pub fn disconnect(&self, destination: AudioParam, output: u32) -> Result<()>;

  pub context: BaseAudioContext,
  #[alias = "numberOfInputs"]
  pub number_of_inputs: u32,
  #[alias = "numberOfOutputs"]
  pub number_of_outputs: u32,

  // Channel up-mixing and down-mixing rules for all inputs.
  #[setter_throws]
  #[alias = "channelCount"]
  pub mut channel_count: u32,
  #[setter_throws]
  #[alias = "channelCountMode"]
  pub mut channel_count_mode: ChannelCountMode,
  #[setter_throws]
  #[alias = "channelInterpretation"]
  pub mut channel_interpretation: ChannelInterpretation,
}

// Mozilla extension
partial trait AudioNode {
  #[chrome_only]
  pub id: u32,
}
pub trait mixin AudioNodePassThrough {
  #[chrome_only]
  #[alias = "passThrough"]
  pub mut pass_through: bool,
}

