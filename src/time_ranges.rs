// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#timeranges

#[exposed = Window]
pub trait TimeRanges {
  length: u32,

  #[throws]
  pub fn start(index: u32) -> f64;

  #[throws]
  pub fn end(index: u32) -> f64;
}
