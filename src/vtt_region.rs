// The origin of this IDL file is
// https://w3c.github.io/webvtt/#the-vttregion-trait

pub enum ScrollSetting {
  "",
  #[alias = "up"]
  Up
}

#[pref = "media.webvtt.regions.enabled",
  exposed = Window]
pub trait VTTRegion {
  #[alias = "constructor"]
  pub fn new() -> Result<Self>;

  pub id: DOMString,
  #[setter_throws]
  pub width: f64,
  #[setter_throws]
  pub lines: i32,
  #[setter_throws]
  #[alias = "regionAnchorX"]
  pub region_anchor_x: f64,
  #[setter_throws]
  #[alias = "regionAnchorY"]
  pub region_anchor_y: f64,
  #[setter_throws]
  #[alias = "viewportAnchorX"]
  pub viewport_anchor_x: f64,
  #[setter_throws]
  #[alias = "viewportAnchorY"]
  pub viewport_anchor_y: f64,

  pub scroll: ScrollSetting,
}
