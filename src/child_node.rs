// The origin of this IDL file is
// http://dom.spec.whatwg.org/#trait-childnode

use super::*;

pub trait mixin ChildNode {
  #[ce_reactions, Unscopable]
  pub fn before(&self, (Node or DOMString)... nodes) -> Result<()>;

  #[ce_reactions, Unscopable]
  pub fn after(&self, (Node or DOMString)... nodes) -> Result<()>;

  #[ce_reactions, Unscopable]
  #[alias = "replaceWith"]
  pub fn replace_with(&self, (Node or DOMString)... nodes) -> Result<()>;

  #[ce_reactions, Unscopable]
  pub fn remove(&self);
}

pub trait mixin NonDocumentTypeChildNode {
  #[pure]
  #[alias = "previousElementSibling"]
  pub previous_element_sibling: Option<Element>,
  #[pure]
  #[alias = "nextElementSibling"]
  pub next_element_sibling: Option<Element>,
}
