// The origin of this IDL file is
// https://streams.spec.whatwg.org/#default-writer-class-definition

#[exposed = (Window, Worker, Worklet),
  pref = "dom.streams.writable_streams.enabled"]
pub trait WritableStreamDefaultWriter {
  #[alias = "constructor"]
  pub fn new(stream: WritableStream) -> Result<Self, Error>;

  closed: Promise<void>,
  #[throws]
  readonly attribute unrestricted Option<f64> desiredSize;
  ready: Promise<void>,

  #[throws]
  pub fn abort(optional reason: any) -> Promise<void>;

  #[throws]
  pub fn close() -> Promise<void>;

  #[throws]
  #[alias = "releaseLock"]
  pub fn release_lock();

  #[throws]
  pub fn write(optional chunk: any) -> Promise<void>;
}
