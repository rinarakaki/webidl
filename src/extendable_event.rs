// For more information on this trait, please see
// http://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html

#[exposed = ServiceWorker]
pub trait ExtendableEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: ExtendableEventInit) -> Self;
  
  // https://github.com/slightlyoff/ServiceWorker/issues/261
  #[throws]
  #[alias = "waitUntil"]
  pub fn wait_until(p: Promise<any>);
}

pub struct ExtendableEventInit: EventInit {
  // Defined for the forward compatibility across the derived events
}
