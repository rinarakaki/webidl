// https://dom.spec.whatwg.org/#trait-document
// https://html.spec.whatwg.org/multipage/dom.html#the-document-object
// https://html.spec.whatwg.org/multipage/obsolete.html#other-elements%2C-attributes-and-apis
// https://fullscreen.spec.whatwg.org/#api
// https://w3c.github.io/pointerlock/#extensions-to-the-document-trait
// https://w3c.github.io/pointerlock/#extensions-to-the-documentorshadowroot-mixin
// https://w3c.github.io/page-visibility/#extensions-to-the-document-trait
// https://drafts.csswg.org/cssom/#extensions-to-the-document-trait
// https://drafts.csswg.org/cssom-view/#extensions-to-the-document-trait
// https://wicg.github.io/feature-policy/#policy

pub trait ContentSecurityPolicy;
pub trait Principal;
pub trait WindowProxy;
pub trait nsISupports;
pub trait URI;
pub trait nsIDocShell;
pub trait nsILoadGroup;
pub trait nsIReferrerInfo;
pub trait nsICookieJarSettings;
pub trait nsIPermissionDelegateHandler;
pub trait XULCommandDispatcher;

pub enum VisibilityState {
  #[alias = "hidden"]
  Hidden,
  #[alias = "visible"]
  Visible
}

// https://dom.spec.whatwg.org/#dictdef-elementcreationoptions
pub struct ElementCreationOptions {
  pub is: DOMString,

  #[chrome_only]
  pub pseudo: DOMString,
}

// https://dom.spec.whatwg.org/#trait-document#[exposed = Window]
pub trait Document: Node {
  #[alias = "constructor"]
  pub fn new() -> Result<Self>;

  #[throws]
  implementation: DOMImplementation,

  #[pure, throws, binary_name = "documentURIFromJS", needs_caller_type]
  URL: DOMString,

  #[pure, throws, binary_name = "documentURIFromJS", needs_caller_type]
  #[alias = "documentURI"]
  document_uri: DOMString,

  #[pure]
  #[alias = "compatMode"]
  compat_mode: DOMString,

  #[pure]
  #[alias = "characterSet"]
  character_set: DOMString,

  #[pure, binary_name = "characterSet"]
  charset: DOMString, // legacy alias of .characterSet

  #[pure, binary_name = "characterSet"]
  #[alias = "inputEncoding"]
  input_encoding: DOMString, // legacy alias of .characterSet

  #[pure]
  #[alias = "contentType"]
  content_type: DOMString,

  #[pure]
  doctype: Option<DocumentType>,

  #[pure]
  #[alias = "documentElement"]
  document_element: Option<Element>,

  #[pure]
  #[alias = "getElementsByTagName"]
  pub fn get_elements_by_tag_name(localName: DOMString) -> HTMLCollection;

  #[pure]
  #[alias = "getElementsByTagNameNS"]
  pub fn get_elements_by_tag_name_ns(
    namespace: Option<DOMString>, namespace: Option<DOMString>)
    -> Result<HTMLCollection>;

  #[pure]
  #[alias = "getElementsByClassName"]
  pub fn get_elements_by_class_name(classNames: DOMString) -> HTMLCollection;

  #[pure]
  #[alias = "getElementById"]
  pub fn get_element_by_id(elementId: DOMString) -> Option<Element>;

  // These DOM methods cannot be accessed by UA Widget scripts
  // because the DOM element reflectors will be in the content scope,
  // instead of the desired UA Widget scope.
  #[ce_reactions, new_object, func = "IsNotUAWidget"]
  #[alias = "createElement"]
  pub fn create_element(
    localName: DOMString,
    optional (ElementCreationOptions or DOMString) options = {})
    -> Result<Element>;

  #[ce_reactions, new_object, func = "IsNotUAWidget"]
  #[alias = "createElementNS"]
  pub fn create_element_ns(
    namespace: Option<DOMString>,
    namespace: Option<DOMString>,
    optional (ElementCreationOptions or DOMString) options = {})
    -> Result<Element>;

  #[new_object]
  #[alias = "createDocumentFragment"]
  pub fn create_document_fragment() -> DocumentFragment;

  #[new_object, func = "IsNotUAWidget"]
  #[alias = "createTextNode"]
  pub fn create_text_node(data: DOMString) -> Text;

  #[new_object, func = "IsNotUAWidget"]
  #[alias = "createComment"]
  pub fn create_comment(data: DOMString) -> Comment;

  #[new_object]
  #[alias = "createProcessingInstruction"]
  pub fn create_processing_instruction(target: DOMString, target: DOMString)
    -> Result<ProcessingInstruction>;

  #[ce_reactions, func = "IsNotUAWidget"]
  #[alias = "importNode"]
  pub fn import_node(node: Node, #[optional = false] deep: bool) -> Result<Node>;

  #[ce_reactions, func = "IsNotUAWidget"]
  #[alias = "adoptNode"]
  pub fn adopt_node(node: Node) -> Result<Node>;

  #[new_object, needs_caller_type]
  #[alias = "createEvent"]
  pub fn create_event(trait: DOMString) -> Result<Event>;

  #[new_object]
  #[alias = "createRange"]
  pub fn create_range() -> Result<Range>;

  // NodeFilter.SHOW_ALL = 0xFFFFFFFF
  #[new_object]
  #[alias = "createNodeIterator"]
  pub fn create_node_iterator(
    root: Node,
    #[optional = 0xFFFFFFFF] whatToShow: u32,
    optional Option<NodeFilter> filter = None)
    -> Result<NodeIterator>;

  #[new_object]
  #[alias = "createTreeWalker"]
  pub fn create_tree_walker(
    root: Node,
    #[optional = 0xFFFFFFFF] whatToShow: u32,
    optional Option<NodeFilter> filter = None)
    -> Result<TreeWalker>;

  // NEW
  // No support for prepend/append yet
  // void prepend((Node or DOMString)... nodes);
  // void append((Node or DOMString)... nodes);

  // These are not in the spec, but leave them for now for backwards compat.
  // So sort of like Gecko extensions
  #[new_object]
  #[alias = "createCDATASection"]
  pub fn create_cdata_section(data: DOMString) -> Result<CDATASection>;

  #[new_object]
  #[alias = "createAttribute"]
  pub fn create_attribute(name: DOMString) -> Result<Attr>;

  #[new_object]
  #[alias = "createAttributeNS"]
  pub fn create_attribute_ns(
    namespace: Option<DOMString>, namespace: Option<DOMString>)
    -> Result<Attr>;
}

// https://html.spec.whatwg.org/multipage/dom.html#the-document-object
partial trait Document {
  #[put_forwards = href, LegacyUnforgeable]
  location: Option<Location>,

  #[setter_throws]
  pub domain: DOMString,

  referrer: DOMString,

  #[throws]
  pub cookie: DOMString,

  #[alias = "lastModified"]
  last_modified: DOMString,

  #[alias = "readyState"]
  ready_state: DOMString,

  // DOM tree accessors
  //(Not proxy yet)getter object (name: DOMString);
  #[ce_reactions, setter_throws, pure]
  pub title: DOMString,

  #[ce_reactions, pure]
  pub dir: DOMString,

  #[ce_reactions, pure, setter_throws]
  pub body: Option<HTMLElement>,

  #[pure]
  head: Option<HTMLHeadElement>,

  #[same_object]
  images: HTMLCollection,

  #[same_object]
  embeds: HTMLCollection,

  #[same_object]
  plugins: HTMLCollection,

  #[same_object]
  links: HTMLCollection,

  #[same_object]
  forms: HTMLCollection,

  #[same_object]
  scripts: HTMLCollection,

  #[pure]
  #[alias = "getElementsByName"]
  pub fn get_elements_by_name(elementName: DOMString) -> NodeList;
  //(implemented: Not)readonly attribute DOMElementMap cssElementMap;

  // dynamic markup insertion
  #[ce_reactions]
  pub fn open(optional unused1: DOMString, optional unused2: DOMString)
    -> Result<Document>;
 // both arguments are ignored

  #[ce_reactions]
  pub fn open(url: USVString, url: USVString, url: USVString) -> Result<Option<WindowProxy>>;

  #[ce_reactions]
  pub fn close() -> Result<()>;

  #[ce_reactions]
  pub fn write(DOMString... text) -> Result<()>;

  #[ce_reactions]
  pub fn writeln(DOMString... text) -> Result<()>;

  // user interaction
  #[pure]
  #[alias = "defaultView"]
  default_view: Option<WindowProxy>,

  #[alias = "hasFocus"]
  pub fn has_focus() -> Result<bool>;

  #[ce_reactions, setter_throws, Setterneeds_subject_principal]
  #[alias = "designMode"]
  pub design_mode: DOMString,

  #[ce_reactions, needs_subject_principal]
  pub fn execCommand(commandId: DOMString, #[optional = false] showUI: bool,
  #[optional = ""] value: DOMString) -> Result<bool>;

  #[needs_subject_principal]
  #[alias = "queryCommandEnabled"]
  pub fn query_command_enabled(commandId: DOMString) -> Result<bool>;

  #[alias = "queryCommandIndeterm"]
  pub fn query_command_indeterm(commandId: DOMString) -> Result<bool>;

  #[alias = "queryCommandState"]
  pub fn query_command_state(commandId: DOMString) -> Result<bool>;

  #[needs_caller_type]
  #[alias = "queryCommandSupported"]
  pub fn query_command_supported(commandId: DOMString) -> Result<bool>;

  #[alias = "queryCommandValue"]
  pub fn query_command_value(commandId: DOMString) -> Result<DOMString>;

  //(implemented: Not)readonly attribute HTMLCollection commands;

  // special event handler IDL attributes that only apply to Document objects
  #[LegacyLenientThis]
  #[alias = "onreadystatechange"]
  pub onreadystatechange: EventHandler,

  // Gecko Option<extensions>
  #[alias = "onbeforescriptexecute"]
  pub onbeforescriptexecute: EventHandler,

  #[alias = "onafterscriptexecute"]
  pub onafterscriptexecute: EventHandler,

  // True if this document is synthetic : stand alone image, video, file: audio,
  // etc.
  #[func = "IsChromeOrUAWidget"]
  #[alias = "mozSyntheticDocument"]
  moz_synthetic_document: bool,

  // Returns the script element whose script is currently being processed.
  //
  // @see <https://developer.mozilla.org/en/DOM/document.currentScript>
  #[pure]
  #[alias = "currentScript"]
  current_script: Option<Element>,

  // Release the current mouse capture if it is on an element within this
  // document.
  //
  // @see <https://developer.mozilla.org/en/DOM/document.releaseCapture>
  #[deprecated = DocumentReleaseCapture, pref = "dom.mouse_capture.enabled"]
  #[alias = "releaseCapture"]
  pub fn release_capture();
  // Use the given DOM element as the source image of target |-moz-element()|.
  //
  // This function introduces a new special ID (called "image element ID"),
  // which is only used by |-moz-element()|, and associates it with the given
  // DOM element. Image elements ID's have the higher precedence than general
  // HTML id's, so if |document.mozSetImageElement(<id>, <element>)| is called,
  // |-moz-element(#<id>)| uses |<element>| as the source image even if there
  // is another element with id attribute = |<id>|. To unregister an image
  // element ID |<id>|, call |document.mozSetImageElement(<id>, None)|.
  //
  // Example:
  // <script>
  //   canvas = document.createElement("canvas");
  //   canvas.setAttribute("width", 100);
  //   canvas.setAttribute("height", 100);
  //   // draw to canvas
  //   document.mozSetImageElement("canvasbg", canvas);
  // </script>
  // <div style = "background-image: -moz-element(#canvasbg);"></div>
  //
  // @param aImageElementId an image element ID to associate with
  // |aImageElement|
  // @param aImageElement a DOM element to be used as the source image of
  // |-moz-element(#aImageElementId)|. If this is None, the function will
  // unregister the image element ID |aImageElementId|.
  //
  // @see <https://developer.mozilla.org/en/DOM/document.mozSetImageElement>
  #[use_counter]
  void mozSetImageElement(aImageElementId: DOMString,
  Option<Element> aImageElement);

  #[chrome_only]
  #[alias = "documentURIObject"]
  document_uri_object: Option<URI>,

  // Current referrer policy - one of the referrer policy value from
  // ReferrerPolicy.webidl.
  #[chrome_only]
  #[alias = "referrerPolicy"]
  referrer_policy: ReferrerPolicy,

  // Current referrer info, which holds all referrer related information
  // including referrer policy and raw referrer of document.
  #[chrome_only]
  #[alias = "referrerInfo"]
  referrer_info: nsIReferrerInfo,

}

// https://html.spec.whatwg.org/multipage/obsolete.html#other-elements%2C-attributes-and-apis
partial trait Document {
  #[ce_reactions]
  #[alias = "fgColor"]
  pub fg_color: DOMString,

  #[ce_reactions]
  #[alias = "linkColor"]
  pub link_color: DOMString,

  #[ce_reactions]
  #[alias = "vlinkColor"]
  pub vlink_color: DOMString,

  #[ce_reactions]
  #[alias = "alinkColor"]
  pub alink_color: DOMString,

  #[ce_reactions]
  #[alias = "bgColor"]
  pub bg_color: DOMString,

  #[same_object]
  anchors: HTMLCollection,

  #[same_object]
  applets: HTMLCollection,

  pub fn clear();

  // @deprecated These are old Netscape 4 methods. Do not use,
  //             the implementation is no-op.
  // XXXbz do we actually need these Option<anymore>
  #[alias = "captureEvents"]
  #[deprecated]
  pub fn capture_events();

  #[alias = "releaseEvents"]
  pub fn release_events();

  #[same_object]
  all: HTMLAllCollection,
}

// https://fullscreen.spec.whatwg.org/#api
partial trait Document {
  // Note: Per spec the 'S' in these two is lowercase, but the "Moz"
  // versions have it uppercase.
  #[LegacyLenientSetter, Unscopable]
  fullscreen: bool,

  #[binary_name = "fullscreen"]
  #[alias = "mozFullScreen"]
  moz_full_screen: bool,

  #[LegacyLenientSetter, needs_caller_type]
  #[alias = "fullscreenEnabled"]
  fullscreen_enabled: bool,

  #[binary_name = "fullscreenEnabled", needs_caller_type]
  #[alias = "mozFullScreenEnabled"]
  moz_full_screen_enabled: bool,

  #[throws]
  #[alias = "exitFullscreen"]
  pub fn exit_fullscreen() -> Promise<void>;

  #[throws, binary_name = "exitFullscreen"]
  #[alias = "mozCancelFullScreen"]
  pub fn moz_cancel_full_screen() -> Promise<void>;

  // Events handlers
  #[alias = "onfullscreenchange"]
  pub onfullscreenchange: EventHandler,

  #[alias = "onfullscreenerror"]
  pub onfullscreenerror: EventHandler,
}

// https://w3c.github.io/pointerlock/#extensions-to-the-document-trait
// https://w3c.github.io/pointerlock/#extensions-to-the-documentorshadowroot-mixin
partial trait Document {
  #[alias = "exitPointerLock"]
  pub fn exit_pointer_lock();

  // Event handlers
  #[alias = "onpointerlockchange"]
  pub onpointerlockchange: EventHandler,

  #[alias = "onpointerlockerror"]
  pub onpointerlockerror: EventHandler,
}

// Mozilla-internal document extensions specific to error pages.
partial trait Document {
  #[func = "Document::CallerIsTrustedAboutCertError"]
  #[alias = "addCertException"]
  pub fn add_cert_exception(isTemporary: bool) -> Promise<any>;

  #[func = "Document::CallerIsTrustedAboutHttpsOnlyError"]
  #[alias = "reloadWithHttpsOnlyException"]
  pub fn reload_with_https_only_exception();

  #[func = "Document::CallerIsTrustedAboutCertError"]
  #[alias = "getFailedCertSecurityInfo"]
  pub fn get_failed_cert_security_info() -> Result<FailedCertSecurityInfo>;

  #[func = "Document::CallerIsTrustedAboutNetError"]
  #[alias = "getNetErrorInfo"]
  pub fn get_net_error_info() -> Result<NetErrorInfo>;

  #[func = "Document::CallerIsTrustedAboutNetError"]
  #[alias = "allowDeprecatedTls"]
  pub allow_deprecated_tls: bool,
}

// https://w3c.github.io/page-visibility/#extensions-to-the-document-trait
partial trait Document {
  hidden: bool,

  #[alias = "visibilityState"]
  visibility_state: VisibilityState,

  #[alias = "onvisibilitychange"]
  pub onvisibilitychange: EventHandler,
}

// https://drafts.csswg.org/cssom/#extensions-to-the-document-trait
partial trait Document {
  pub selectedStyleSheetSet: Option<DOMString>,

  #[alias = "lastStyleSheetSet"]
  last_style_sheet_set: Option<DOMString>,

  #[alias = "preferredStyleSheetSet"]
  preferred_style_sheet_set: Option<DOMString>,

  #[constant]
  #[alias = "styleSheetSets"]
  style_sheet_sets: DOMStringList,

  void enableStyleSheetsForSet (name: Option<DOMString>);
}

// https://drafts.csswg.org/cssom-view/#extensions-to-the-document-trait
partial trait Document {
  Option<CaretPosition> caretPositionFromPoint (x: f32, x: f32);

  #[alias = "scrollingElement"]
  scrolling_element: Option<Element>,
}

// http://dev.w3.org/2006/webapi/selectors-api2/#trait-definitions
partial trait Document {
  #[pure]
  #[alias = "querySelector"]
  pub fn query_selector(selectors: UTF8String) -> Result<Option<Element>>;

  #[pure]
  #[alias = "querySelectorAll"]
  pub fn query_selector_all(selectors: UTF8String) -> Result<NodeList>;

  //(implemented: Not)Option<Element> find(implemented: Not, optional (Element or Vec<Node>)? refNodes);

  //(implemented: Not)NodeList findAll(implemented: Not, optional (Element or Vec<Node>)? refNodes);
}

// https://drafts.csswg.org/web-animations/#extensions-to-the-document-trait
partial trait Document {
  #[func = "Document::AreWebAnimationsTimelinesEnabled"]
  timeline: DocumentTimeline,
}

// https://svgwg.org/svg2-draft/struct.html#InterfaceDocumentExtensions
partial trait Document {
  #[binary_name = "SVGRootElement"]
  #[alias = "rootElement"]
  root_element: Option<SVGSVGElement>,
}

//  Mozilla extensions of various sorts
partial trait Document {
  // Creates a new XUL element regardless of the document's default type.
  #[chrome_only, ce_reactions, new_object]
  #[alias = "createXULElement"]
  pub fn create_xul_element(localName: DOMString, optional (ElementCreationOptions or DOMString) options = {}) -> Result<Element>;

  // Wether the document was loaded using a nsXULPrototypeDocument.
  #[chrome_only]
  #[alias = "loadedFromPrototype"]
  loaded_from_prototype: bool,

  // The principal to use for the storage area of this document
  #[chrome_only]
  #[alias = "effectiveStoragePrincipal"]
  effective_storage_principal: Principal,

  // You should probably not be using this principal getter since it performs
  // no checks to ensure that the partitioned principal should really be used
  // here. It is only designed to be used in very specific circumstances, such
  // as when inheriting the document/storage principal.
  #[chrome_only]
  #[alias = "partitionedPrincipal"]
  partitioned_principal: Principal,

  // The cookieJarSettings of this document
  #[chrome_only]
  #[alias = "cookieJarSettings"]
  cookie_jar_settings: nsICookieJarSettings,

  // Touch bits
  // XXXbz I can't find the sane spec for this stuff, so just cribbing
  // from our xpidl for now.
  #[new_object, func = "nsGenericHTMLElement::LegacyTouchAPIEnabled"]
  Touch createTouch(optional Option<Window> view = None,
  optional Option<EventTarget> target = None,
  #[optional = 0] identifier: i32,
  #[optional = 0] pageX: i32,
  #[optional = 0] pageY: i32,
  #[optional = 0] screenX: i32,
  #[optional = 0] screenY: i32,
  #[optional = 0] clientX: i32,
  #[optional = 0] clientY: i32,
  #[optional = 0] radiusX: i32,
  #[optional = 0] radiusY: i32,
  #[optional = 0] rotationAngle: f32,
  #[optional = 0] force: f32);

  // XXXbz a hack to get around the fact that we don't support variadics as
  // distinguishing arguments yet. Once this hack is removed. we can also
  // remove the corresponding overload on Document, since Touch... and
  // Vec<Touch> look the same in the C++.
  #[new_object, func = "nsGenericHTMLElement::LegacyTouchAPIEnabled"]
  #[alias = "createTouchList"]
  pub fn create_touch_list(touch: Touch, Touch... touches) -> TouchList;

  // XXXbz and another hack for the fact that we can't usefully have optional
  // distinguishing arguments but need a working zero-arg form of
  // createTouchList().
  #[new_object, func = "nsGenericHTMLElement::LegacyTouchAPIEnabled"]
  #[alias = "createTouchList"]
  pub fn create_touch_list() -> TouchList;

  #[new_object, func = "nsGenericHTMLElement::LegacyTouchAPIEnabled"]
  #[alias = "createTouchList"]
  pub fn create_touch_list(touches: Vec<Touch>) -> TouchList;

  #[chrome_only]
  #[alias = "styleSheetChangeEventsEnabled"]
  pub style_sheet_change_events_enabled: bool,

  #[chrome_only]
  #[alias = "shadowRootAttachedEventEnabled"]
  pub shadow_root_attached_event_enabled: bool,

  #[chrome_only]
  #[alias = "contentLanguage"]
  content_language: DOMString,

  #[chrome_only]
  #[alias = "documentLoadGroup"]
  document_load_group: Option<nsILoadGroup>,

  // Blocks the initial document parser until the given promise is settled.
  #[chrome_only, throws]
  Promise<any> blockParsing(promise: Promise<any>,
  #[optional = {}] options: BlockParsingOptions);

  #[func = "nsContentUtils::IsPDFJS", binary_name = "blockUnblockOnloadForPDFJS"]
  #[alias = "blockUnblockOnload"]
  pub fn block_unblock_onload(block: bool);

  // like documentURI, except that for error pages, it returns the URI we were
  // trying to load when we hit an error, rather than the error page's own URI.
  #[chrome_only]
  #[alias = "mozDocumentURIIfNotForErrorPages"]
  moz_document_uri_if_not_for_error_pages: Option<URI>,

  // A promise that is resolved when we have both fired DOMContentLoaded and
  // are ready to start layout.
  // This is used for the "document_idle" webextension script injection point.
  #[chrome_only, throws]
  #[alias = "documentReadyForIdle"]
  document_ready_for_idle: Promise<void>,

  // Lazily created command dispatcher, returns None if the document is not
  // chrome privileged.
  #[chrome_only]
  #[alias = "commandDispatcher"]
  command_dispatcher: Option<XULCommandDispatcher>,

  #[chrome_only]
  #[alias = "devToolsWatchingDOMMutations"]
  pub dev_tools_watching_dom_mutations: bool,

  // Returns all the shadow roots connected to the document, in no particular
  // order, and without regard to open/closed-ness. Also returns UA widgets
  // (like <video> controls), which can be checked using
  // ShadowRoot.isUAWidget().
  #[chrome_only]
  #[alias = "getConnectedShadowRoots"]
  pub fn get_connected_shadow_roots() -> Vec<ShadowRoot>;
}

pub struct BlockParsingOptions {
  // If true, blocks script-created parsers (created via document.open()) in
  // addition to network-created parsers.
  #[alias = "blockScriptCreated"]
  pub block_script_created: bool = true,
}

// Extension to give chrome JS the ability to determine when a document was
// created to satisfy an iframe with srcdoc attribute.
partial trait Document {
  #[chrome_only]
  #[alias = "isSrcdocDocument"]
  is_srcdoc_document: bool,
}

// Extension to give chrome JS the ability to get the underlying
// sandbox flag attribute
partial trait Document {
  #[chrome_only]
  #[alias = "sandboxFlagsAsString"]
  sandbox_flags_as_string: Option<DOMString>,
}

// Chrome document anonymous content management.
// This is a Chrome-only API that allows inserting fixed positioned anonymous
// content on top of the current page displayed in the document.
// The supplied content is cloned and inserted into the document's CanvasFrame.
// Note that this only works for HTML documents.
partial trait Document {
  // Deep-clones the provided element and inserts it into the CanvasFrame.
  // Returns an AnonymousContent instance that can be used to manipulate the
  // inserted element.
  //
  // If aForce is true, tries to update layout to be able to insert the element
  // synchronously.
  #[chrome_only, new_object]
  #[alias = "insertAnonymousContent"]
  pub fn insert_anonymous_content(aElement: Element, #[optional = false] aForce: bool)
    -> Result<AnonymousContent>;

  // Removes the element inserted into the CanvasFrame given an AnonymousContent
  // instance.
  #[chrome_only]
  #[alias = "removeAnonymousContent"]
  pub fn remove_anonymous_content(aContent: AnonymousContent) -> Result<()>;
}

// http://w3c.github.io/selection-api/#extensions-to-document-trait
partial trait Document {
  #[alias = "getSelection"]
  pub fn get_selection() -> Result<Option<Selection>>;
}

// https://github.com/whatwg/html/issues/3338
partial trait Document {
  #[pref = "dom.storage_access.enabled", throws]
  #[alias = "hasStorageAccess"]
  pub fn has_storage_access() -> Promise<bool>;

  #[pref = "dom.storage_access.enabled", throws]
  #[alias = "requestStorageAccess"]
  pub fn request_storage_access() -> Promise<void>;
}

// A privileged API to give chrome privileged code and the content script of the
// webcompat extension the ability to request the storage access for a given
// third party.
partial trait Document {
  #[func = "Document::CallerCanAccessPrivilegeSSA", throws]
  #[alias = "requestStorageAccessForOrigin"]
  pub fn request_storage_access_for_origin(
    thirdPartyOrigin: DOMString,
    #[optional = true] requireUserInteraction: bool) -> Promise<void>;
}

pub enum DocumentAutoplayPolicy {
  "allowed",  // autoplay is currently allowed
  "allowed-muted",  // muted video autoplay is currently allowed
  "disallowed"  // autoplay is not current allowed
}

// https://github.com/WICG/autoplay/issues/1
partial trait Document {
  #[pref = "dom.media.autoplay.autoplay-policy-api"]
  #[alias = "autoplayPolicy"]
  autoplay_policy: DocumentAutoplayPolicy,
}

// Extension to give chrome JS the ability to determine whether
// the user has interacted with the document or not.
partial trait Document {
  #[chrome_only]
  #[alias = "userHasInteracted"]
  user_has_interacted: bool,
}

// Extension to give chrome JS the ability to simulate activate the document
// by user gesture.
partial trait Document {
  #[chrome_only]
  #[alias = "notifyUserGestureActivation"]
  pub fn notify_user_gesture_activation();

  // For testing only.
  #[chrome_only]
  #[alias = "clearUserGestureActivation"]
  pub fn clear_user_gesture_activation();

  #[chrome_only]
  #[alias = "hasBeenUserGestureActivated"]
  has_been_user_gesture_activated: bool,

  #[chrome_only]
  #[alias = "hasValidTransientUserGestureActivation"]
  has_valid_transient_user_gesture_activation: bool,

  #[chrome_only]
  #[alias = "consumeTransientUserGestureActivation"]
  pub fn consume_transient_user_gesture_activation() -> bool;
}

// Extension to give chrome JS the ability to set an event handler which is
// called with certain events that happened while events were suppressed in the
// document or one of its subdocuments.
partial trait Document {
  #[chrome_only]
  #[alias = "setSuppressedEventListener"]
  pub fn set_suppressed_event_listener(aListener: Option<EventListener>);
}

// Allows frontend code to query a CSP which needs to be passed for a
// new load into docshell. Further, allows to query the CSP in JSON
// format for testing purposes.
partial trait Document {
  #[chrome_only]
  csp: Option<ContentSecurityPolicy>,

  #[chrome_only]
  #[alias = "cspJSON"]
  csp_json: DOMString,
}

// For more information on Flash classification, see
// toolkit/components/url-classifier/flash-block-lists.rst
pub enum FlashClassification {
  "unknown", // Site is not on the whitelist or blacklist
  "allowed", // Site is on the Flash whitelist
  "denied" // Site is on the Flash blacklist
}

partial trait Document {
  #[chrome_only]
  #[alias = "documentFlashClassification"]
  document_flash_classification: FlashClassification,
}

partial trait Document {
  #[func = "Document::DocumentSupportsL10n"]
  l10n: Option<DocumentL10n>,
}

Document includes XPathEvaluatorMixin;
Document includes GlobalEventHandlers;
Document includes DocumentAndElementEventHandlers;
Document includes TouchEventHandlers;
Document includes ParentNode;
Document includes OnErrorEventHandlerForNodes;
Document includes GeometryUtils;
Document includes FontFaceSource;
Document includes DocumentOrShadowRoot;

// https://w3c.github.io/webappsec-feature-policy/#idl-index
partial trait Document {
  #[same_object, pref = "dom.security.featurePolicy.webidl.enabled"]
  #[alias = "featurePolicy"]
  feature_policy: FeaturePolicy,
}

// Extension to give chrome JS the ability to specify a non-default keypress
// event model.
partial trait Document {
  // setKeyPressEventModel() is called when we need to check whether the web
  // app requires specific keypress event model or not.
  //
  // @param aKeyPressEventModel Proper keypress event model for the web app.
  //   KEYPRESS_EVENT_MODEL_DEFAULT:
  //     Use default keypress event model. I.e., depending on
  //     "dom.keyboardevent.keypress.set_keycode_and_charcode_to_same_value"
  //     pref.
  //   KEYPRESS_EVENT_MODEL_SPLIT:
  //     Use split model. I.e, if keypress event inputs a character,
  //     keyCode should be 0. Otherwise, charCode should be 0.
  //   KEYPRESS_EVENT_MODEL_CONFLATED:
  //     Use conflated model. I.e., keyCode and charCode values of each
  //     keypress event should be set to same value.
  #[chrome_only]
  const u16 KEYPRESS_EVENT_MODEL_DEFAULT = 0;
  #[chrome_only]
  const u16 KEYPRESS_EVENT_MODEL_SPLIT = 1;
  #[chrome_only]
  const u16 KEYPRESS_EVENT_MODEL_CONFLATED = 2;

  #[chrome_only]
  #[alias = "setKeyPressEventModel"]
  pub fn set_key_press_event_model(aKeyPressEventModel: u16);
}

// Extensions to return information about about the nodes blocked by the
// Safebrowsing API inside a document.
partial trait Document {
  //
  // Number of nodes that have been blocked by the Safebrowsing API to prevent
  // tracking, cryptomining and so on. This method is for testing only.
  #[chrome_only, pure]
  #[alias = "blockedNodeByClassifierCount"]
  blocked_node_by_classifier_count: i32,

  //
  // List of nodes that have been blocked by the Safebrowsing API to prevent
  // tracking, fingerprinting, cryptomining and so on. This method is for
  // testing only.
  #[chrome_only, pure]
  #[alias = "blockedNodesByClassifier"]
  blocked_nodes_by_classifier: NodeList,
}

// Extension to programmatically simulate a user interaction on a document,
// used for testing.
partial trait Document {
  #[chrome_only, binary_name = "setUserHasInteracted"]
  #[alias = "userInteractionForTesting"]
  pub fn user_interaction_for_testing();
}

// Extension for permission delegation.
partial trait Document {
  #[chrome_only, pure]
  #[alias = "permDelegateHandler"]
  perm_delegate_handler: nsIPermissionDelegateHandler,
}

// Extension used by the password manager to infer form submissions.
partial trait Document {
  //
  // Set whether the document notifies an event when a fetch or
  // XHR completes successfully.
  #[chrome_only]
  #[alias = "setNotifyFetchSuccess"]
  pub fn set_notify_fetch_success(aShouldNotify: bool);

  //
  // Set whether a form and a password field notify an event when it is
  // removed from the DOM tree.
  #[chrome_only]
  #[alias = "setNotifyFormOrPasswordRemoved"]
  pub fn set_notify_form_or_password_removed(aShouldNotify: bool);
}

// Extension to allow chrome code to detect initial about:blank documents.
partial trait Document {
  #[chrome_only]
  #[alias = "isInitialDocument"]
  is_initial_document: bool,
}

// Extension to allow chrome code to get some wireframe-like structure.
pub enum WireframeRectType {
  #[alias = "image"]
  Image,
  #[alias = "background"]
  Background,
  #[alias = "text"]
  Text,
  #[alias = "unknown"]
  Unknown,
}

pub struct WireframeTaggedRect {
  unrestricted f64 x = 0;
  unrestricted f64 y = 0;
  unrestricted f64 width = 0;
  unrestricted f64 height = 0;
  #[alias = "color"]
  pub colour: Option<DOMString>, // Only relevant for "background" rects WireframeRectType type;
  pub node: Option<Node>,
}

pub struct Wireframe {
  #[alias = "canvasBackground"]
  pub canvas_background: DOMString,
  pub rects: Vec<WireframeTaggedRect>,
}

partial trait Document {
  #[chrome_only]
  #[alias = "getWireframe"]
  pub fn get_wireframe(#[optional = false] aIncludeNodes: bool) -> Option<Wireframe>;
}
