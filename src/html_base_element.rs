// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-base-element

// http://www.whatwg.org/specs/web-apps/current-work/#the-base-element
#[exposed = Window]
pub trait HTMLBaseElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub href: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub target: DOMString,
}

