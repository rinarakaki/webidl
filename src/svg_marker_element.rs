// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGMarkerElement: SVGElement {

  // Marker Unit Types
  const u16 SVG_MARKERUNITS_UNKNOWN = 0;
  const u16 SVG_MARKERUNITS_USERSPACEONUSE = 1;
  const u16 SVG_MARKERUNITS_STROKEWIDTH = 2;

  // Marker Orientation Types
  const u16 SVG_MARKER_ORIENT_UNKNOWN = 0;
  const u16 SVG_MARKER_ORIENT_AUTO = 1;
  const u16 SVG_MARKER_ORIENT_ANGLE = 2;
  const u16 SVG_MARKER_ORIENT_AUTO_START_REVERSE = 3;

  #[constant]
  refX: SVGAnimatedLength,
  #[constant]
  refY: SVGAnimatedLength,
  #[constant]
  #[alias = "markerUnits"]
  marker_units: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "markerWidth"]
  marker_width: SVGAnimatedLength,
  #[constant]
  #[alias = "markerHeight"]
  marker_height: SVGAnimatedLength,
  #[constant]
  #[alias = "orientType"]
  orient_type: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "orientAngle"]
  orient_angle: SVGAnimatedAngle,

  #[alias = "setOrientToAuto"]
  pub fn set_orient_to_auto();
  #[throws]
  #[alias = "setOrientToAngle"]
  pub fn set_orient_to_angle(angle: SVGAngle);
}

SVGMarkerElement includes SVGFitToViewBox;

