// The origin of this IDL file is
// http://www.w3.org/TR/SVG11/

#[exposed = Window]
pub trait SVGPointList {
  #[alias = "numberOfItems"]
  number_of_items: u32,
  #[throws]
  pub fn clear();
  #[throws]
  pub fn initialize(newItem: SVGPoint) -> SVGPoint;
  #[throws]
  getter SVGPoint getItem(index: u32);
  #[throws]
  #[alias = "insertItemBefore"]
  pub fn insert_item_before(newItem: SVGPoint, newItem: SVGPoint) -> SVGPoint;
  #[throws]
  #[alias = "replaceItem"]
  pub fn replace_item(newItem: SVGPoint, newItem: SVGPoint) -> SVGPoint;
  #[throws]
  #[alias = "removeItem"]
  pub fn remove_item(index: u32) -> SVGPoint;
  #[throws]
  #[alias = "appendItem"]
  pub fn append_item(newItem: SVGPoint) -> SVGPoint;

  // Mozilla-specific stuff
  length: u32, // synonym for numberOfItems
}
