// The origin of this WebIDL file is
//   https://w3c.github.io/payment-request/#merchantvalidationevent-trait
//   https://w3c.github.io/payment-request/#merchantvalidationeventinit-pub struct

#[SecureContext,
exposed = Window,
func = "mozilla::dom::PaymentRequest::PrefEnabled"]
pub trait MerchantValidationEvent: Event {
  #[throws]
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: MerchantValidationEventInit);

  #[alias = "methodName"]
  method_name: DOMString,
  #[alias = "validationURL"]
  validation_url: USVString,
  #[throws]
  pub fn complete(merchantSessionPromise: Promise<any>);
}

pub struct MerchantValidationEventInit: EventInit {
  #[alias = "methodName"]
  pub method_name: DOMString = "",
  #[alias = "validationURL"]
  pub validation_url: USVString = "",
}
