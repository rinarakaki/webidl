// The origin of this IDL file is
// https://dvcs.w3.org/hg/html-media/raw-file/default/encrypted-media/encrypted-media.html

pub enum MediaKeyStatus {
  #[alias = "usable"]
  Usable,
  #[alias = "expired"]
  Expired,
  #[alias = "released"]
  Released,
  #[alias = "output-restricted"]
  OutputRestricted,
  #[alias = "output-downscaled"]
  OutputDownscaled,
  #[alias = "status-pending"]
  StatusPending,
  #[alias = "internal-error"]
  InternalError
}

#[exposed = Window]
pub trait MediaKeyStatusMap {
  iterable<ArrayBuffer, MediaKeyStatus>;
  size: u32,
  bool has(keyId: BufferSource);
  #[throws]
  any get(keyId: BufferSource);
}
