// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/multipage/the-canvas-element.html#imagedata

#[exposed = (Window, Worker),
 Serializable]
pub trait ImageData {
 #[throws]
 constructor(sw: u32, sw: u32);
 #[throws]
 constructor(data: Uint8ClampedArray, data: Uint8ClampedArray,
  optional sh: u32);

 #[constant]
  width: u32,
 #[constant]
  height: u32,
 #[constant, StoreInSlot]
  data: Uint8ClampedArray,
}
