// The origin of this IDL file is
// http://www.w3.org/TR/geolocation-API

#[exposed = Window, SecureContext]
pub trait GeolocationCoordinates {
  latitude: f64,
  longitude: f64,
  altitude: Option<f64>,
  accuracy: f64,
  #[alias = "altitudeAccuracy"]
  altitude_accuracy: Option<f64>,
  heading: Option<f64>,
  speed: Option<f64>,
}
