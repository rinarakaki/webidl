// The origin of this IDL file is
// http://www.w3.org/TR/geolocation-API

pub struct PositionOptions {
  #[alias = "enableHighAccuracy"]
  pub enable_high_accuracy: bool = false,
  #[Clamp]
  pub timeout: u32 = 0x7fffffff,
  #[Clamp]
  #[alias = "maximumAge"]
  pub maximum_age: u32 = 0,
}

#[exposed = Window]
pub trait Geolocation {
  #[needs_caller_type]
  #[alias = "getCurrentPosition"]
  pub fn get_current_position(
    successCallback: PositionCallback,
    optional Option<PositionErrorCallback> errorCallback = None,
    #[optional = {}] options: PositionOptions) -> Result<()>;

  #[needs_caller_type]
  #[alias = "watchPosition"]
  pub fn watch_position(
    successCallback: PositionCallback,
    optional Option<PositionErrorCallback> errorCallback = None,
    #[optional = {}] options: PositionOptions) -> Result<i32>;

  #[alias = "clearWatch"]
  pub fn clear_watch(watchId: i32);
}

pub type PositionCallback = Fn(position: GeolocationPosition);

pub type PositionErrorCallback = Fn(positionError: GeolocationPositionError);
