pub struct TestInterfaceJSUnionableDictionary {
  #[alias = "objectMember"]
  pub object_member: object,
  #[alias = "anyMember"]
  pub any_member: any,
}

#[JSImplementation = "@mozilla.org/dom/test-trait-js;1",
 pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait TestInterfaceJS: EventTarget {
  #[throws]
  constructor(optional anyArg: any, optional objectArg: object,
  #[optional = {}] pub: TestInterfaceJSDictionary structArg);

  #[alias = "anyArg"]
  any_arg: any,
  #[alias = "objectArg"]
  object_arg: object,
  #[alias = "getDictionaryArg"]
  pub fn get_pub struct_arg() -> TestInterfaceJSDictionary;
  #[alias = "anyAttr"]
  pub any_attr: any,
  #[alias = "objectAttr"]
  pub object_attr: object,
  #[alias = "getDictionaryAttr"]
  pub fn get_pub struct_attr() -> TestInterfaceJSDictionary;
  #[alias = "setDictionaryAttr"]
  pub fn set_pub struct_attr(#[optional = {}] dict: TestInterfaceJSDictionary);
  #[alias = "pingPongAny"]
  pub fn ping_pong_any(arg: any) -> any;
  #[alias = "pingPongObject"]
  pub fn ping_pong_object(obj: object) -> object;
  #[alias = "pingPongObjectOrString"]
  pub fn ping_pong_object_or_string((object or DOMString) objOrString) -> any;
  #[alias = "pingPongDictionary"]
  pub fn ping_pong_pub struct(#[optional = {}] dict: TestInterfaceJSDictionary) -> TestInterfaceJSDictionary;
  #[alias = "pingPongDictionaryOrLong"]
  pub fn ping_pong_pub struct_or_long(optional (TestInterfaceJSUnionableDictionary or i32) dictOrLong = {}) -> i32;
  #[alias = "pingPongRecord"]
  pub fn ping_pong_record(record<DOMString, any> rec) -> DOMString;
  #[alias = "objectSequenceLength"]
  pub fn object_sequence_length(seq: Vec<object>) -> i32;
  #[alias = "anySequenceLength"]
  pub fn any_sequence_length(seq: Vec<any>) -> i32;

  // For testing bug 968335.
  #[alias = "getCallerPrincipal"]
  pub fn get_caller_principal() -> DOMString;

  #[alias = "convertSVS"]
  pub fn convert_svs(svs: USVString) -> DOMString;

  (TestInterfaceJS or i32) pingPongUnion((TestInterfaceJS or i32) something);
  (DOMString or Option<TestInterfaceJS>) pingPongUnionContainingNull((Option<TestInterfaceJS> or DOMString) something);
  (TestInterfaceJS or i32)? pingPongNullableUnion((TestInterfaceJS or i32)? something);
  (Location or TestInterfaceJS) returnBadUnion();

  // Test for sequence overloading and union behavior
  #[alias = "testSequenceOverload"]
  pub fn test_sequence_overload(arg: Vec<DOMString>);
  #[alias = "testSequenceOverload"]
  pub fn test_sequence_overload(arg: DOMString);

  #[alias = "testSequenceUnion"]
  pub fn test_sequence_union((Vec<DOMString> or DOMString) arg);

  // Tests for exception-throwing behavior
  #[throws]
  #[alias = "testThrowError"]
  pub fn test_throw_error();

  #[throws]
  #[alias = "testThrowDOMException"]
  pub fn test_throw_dom_exception();

  #[throws]
  #[alias = "testThrowTypeError"]
  pub fn test_throw_type_error();

  #[throws]
  #[alias = "testThrowCallbackError"]
  pub fn test_throw_callback_error(callback: function);

  #[throws]
  #[alias = "testThrowXraySelfHosted"]
  pub fn test_throw_xray_self_hosted();

  #[throws]
  #[alias = "testThrowSelfHosted"]
  pub fn test_throw_self_hosted();

  // Tests for promise-rejection behavior
  #[alias = "testPromiseWithThrowingChromePromiseInit"]
  pub fn test_promise_with_throwing_chrome_promise_init() -> Promise<void>;
  #[alias = "testPromiseWithThrowingContentPromiseInit"]
  pub fn test_promise_with_throwing_content_promise_init(func: function) -> Promise<void>;
  #[alias = "testPromiseWithDOMExceptionThrowingPromiseInit"]
  pub fn test_promise_with_dom_exception_throwing_promise_init() -> Promise<void>;
  #[alias = "testPromiseWithThrowingChromeThenfunction"]
  pub fn test_promise_with_throwing_chrome_then_function() -> Promise<void>;
  #[alias = "testPromiseWithThrowingContentThenfunction"]
  pub fn test_promise_with_throwing_content_then_function(func: AnyCallback) -> Promise<void>;
  #[alias = "testPromiseWithDOMExceptionThrowingThenfunction"]
  pub fn test_promise_with_dom_exception_throwing_then_function() -> Promise<void>;
  #[alias = "testPromiseWithThrowingChromeThenable"]
  pub fn test_promise_with_throwing_chrome_thenable() -> Promise<void>;
  #[alias = "testPromiseWithThrowingContentThenable"]
  pub fn test_promise_with_throwing_content_thenable(thenable: object) -> Promise<void>;
  #[alias = "testPromiseWithDOMExceptionThrowingThenable"]
  pub fn test_promise_with_dom_exception_throwing_thenable() -> Promise<void>;

  // Event handler tests
  #[alias = "onsomething"]
  pub onsomething: EventHandler,
}
