// The origin of this IDL file is
// https://drafts.csswg.org/css-pseudo-4/#csspseudoelement

#[pref = "dom.css_pseudo_element.enabled",
 exposed = Window]
pub trait CSSPseudoElement {
  type: DOMString,
  element: Element,
}
