pub enum StorageType {
  #[alias = "persistent"]
  Persistent,
  #[alias = "temporary"]
  Temporary,
  #[alias = "default"]
  Default
}
