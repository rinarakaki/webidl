// The origin of this IDL file is
// https://dvcs.w3.org/hg/html-media/raw-file/default/encrypted-media/encrypted-media.html

// According to the spec, "The future of error events and MediaKeyError
// is uncertain."
// https://www.w3.org/Bugs/Public/show_bug.Option<cgi>id = 21798
#[exposed = Window]
pub trait MediaKeyError: Event {
  #[alias = "systemCode"]
  system_code: u32,
}
