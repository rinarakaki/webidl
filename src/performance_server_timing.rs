// The origin of this IDL file is
// https://w3c.github.io/server-timing/#the-performanceservertiming-trait

#[SecureContext,exposed = (Window, Worker)]
pub trait PerformanceServerTiming {
  name: DOMString,
  duration: DOMHighResTimeStamp,
  description: DOMString,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
