// The origin of this IDL file is
// https://webaudio.github.io/web-midi-api/

pub enum MIDIPortType {
  #[alias = "input"]
  Input,
  #[alias = "output"]
  Output
}

pub enum MIDIPortDeviceState {
  #[alias = "disconnected"]
  Disconnected,
  #[alias = "connected"]
  Connected
}

pub enum MIDIPortConnectionState {
  #[alias = "open"]
  Open,
  #[alias = "closed"]
  Closed,
  #[alias = "pending"]
  Pending
}

#[SecureContext, pref = "dom.webmidi.enabled",
  exposed = Window]
pub trait MIDIPort: EventTarget {
  id: DOMString,
  manufacturer: Option<DOMString>,
  name: Option<DOMString>,
  version: Option<DOMString>,
  type: MIDIPortType,
  state: MIDIPortDeviceState,
  connection: MIDIPortConnectionState,
  #[alias = "onstatechange"]
  pub on_state_change: EventHandler,
  pub fn open() -> Promise<MIDIPort>;
  pub fn close() -> Promise<MIDIPort>;
}

