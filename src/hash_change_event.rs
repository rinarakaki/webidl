// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/#the-hashchangeevent-trait

#[LegacyEventInit,
 exposed = Window]
pub trait HashChangeEvent: Event
{
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: HashChangeEventInit) -> Self;

  #[alias = "oldURL"]
  old_url: DOMString,
  #[alias = "newURL"]
  new_url: DOMString,

  void initHashChangeEvent(typeArg: DOMString,
  #[optional = false] canBubbleArg: bool,
  #[optional = false] cancelableArg: bool,
  #[optional = ""] oldURLArg: DOMString,
  #[optional = ""] newURLArg: DOMString);
}

pub struct HashChangeEventInit: EventInit
{
  pub oldURL: DOMString = "",
  pub newURL: DOMString = "",
}
