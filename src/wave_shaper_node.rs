// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub enum OverSampleType {
  #[alias = "none"]
  None,
  "2x",  // TODO
  "4x"
}

pub struct WaveShaperOptions: AudioNodeOptions {
  pub curve: Vec<f32>,
  pub oversample: OverSampleType = "none",
}

#[pref = "dom.webaudio.enabled",
  exposed = Window]
pub trait WaveShaperNode: AudioNode {
  #[throws]
  constructor(
    context: BaseAudioContext,
    #[optional = {}] options: WaveShaperOptions);

  #[Cached, pure, setter_throws]
  pub curve: Option<F32Array>;
  pub oversample: OverSampleType,

}

// Mozilla extension
WaveShaperNode includes AudioNodePassThrough;

