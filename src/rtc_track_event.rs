// The origin of this IDL file is
// http://w3c.github.io/webrtc-pc/#idl-def-RTCTrackEvent

pub struct RTCTrackEventInit: EventInit {
  required RTCRtpReceiver receiver;
  required MediaStreamTrack track;
  pub streams: Vec<MediaStream> = [],
  required RTCRtpTransceiver transceiver;
}

#[pref = "media.peerconnection.enabled",
 exposed = Window]
pub trait RTCTrackEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;
  
  receiver: RTCRtpReceiver,
  track: MediaStreamTrack,

// TODO: Use FrozenArray once available. (1236777: Bug)
//  readonly attribute FrozenArray<MediaStream> streams;

  #[Frozen, Cached, pure]
  streams: Vec<MediaStream>, // workaround
  transceiver: RTCRtpTransceiver,
}
