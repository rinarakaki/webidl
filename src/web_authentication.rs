// The origin of this IDL file is
// https://w3c.github.io/webauthn/

//**** Interfaces to Data ****
#[SecureContext, pref = "security.webauth.webauthn",
 exposed = Window]
pub trait PublicKeyCredential: Credential {
  #[same_object]
  #[alias = "rawId"]
  raw_id: ArrayBuffer,
  #[same_object]
  response: AuthenticatorResponse,
  #[alias = "getClientExtensionResults"]
  pub fn get_client_extension_results() -> AuthenticationExtensionsClientOutputs;
}

#[SecureContext]
partial trait PublicKeyCredential {
  static Promise<bool> isUserVerifyingPlatformAuthenticatorAvailable();
  // isExternalCTAP2SecurityKeySupported is non-standard; see Bug 1526023
  static Promise<bool> isExternalCTAP2SecurityKeySupported();
}

#[SecureContext, pref = "security.webauth.webauthn",
 exposed = Window]
pub trait AuthenticatorResponse {
  #[same_object]
  #[alias = "clientDataJSON"]
  client_data_json: ArrayBuffer,
}

#[SecureContext, pref = "security.webauth.webauthn",
 exposed = Window]
pub trait AuthenticatorAttestationResponse: AuthenticatorResponse {
  #[same_object]
  #[alias = "attestationObject"]
  attestation_object: ArrayBuffer,
}

#[SecureContext, pref = "security.webauth.webauthn",
 exposed = Window]
pub trait AuthenticatorAssertionResponse: AuthenticatorResponse {
  #[same_object]
  #[alias = "authenticatorData"]
  authenticator_data: ArrayBuffer,
  #[same_object]
  signature: ArrayBuffer,
  #[same_object]
  #[alias = "userHandle"]
  user_handle: Option<ArrayBuffer>,
}

pub struct PublicKeyCredentialParameters {
  required PublicKeyCredentialType type;
  required COSEAlgorithmIdentifier alg;
}

pub struct PublicKeyCredentialCreationOptions {
  required PublicKeyCredentialRpEntity rp;
  required PublicKeyCredentialUserEntity user;

  required BufferSource challenge;
  required Vec<PublicKeyCredentialParameters> pubKeyCredParams;

  pub timeout: u32,
  #[alias = "excludeCredentials"]
  pub exclude_credentials: Vec<PublicKeyCredentialDescriptor> = [],
  // FIXME: bug 1493860: should this "= {}" be Option<here>
  #[alias = "authenticatorSelection"]
  pub authenticator_selection: AuthenticatorSelectionCriteria = {},
  pub attestation: AttestationConveyancePreference = "none",
  // FIXME: bug 1493860: should this "= {}" be Option<here>
  pub extensions: AuthenticationExtensionsClientInputs = {},
}

pub struct PublicKeyCredentialEntity {
  required DOMString name;
  pub icon: USVString,
}

pub struct PublicKeyCredentialRpEntity: PublicKeyCredentialEntity {
  pub id: DOMString,
}

pub struct PublicKeyCredentialUserEntity: PublicKeyCredentialEntity {
  required BufferSource id;
  required DOMString displayName;
}

pub struct AuthenticatorSelectionCriteria {
  #[alias = "authenticatorAttachment"]
  pub authenticator_attachment: AuthenticatorAttachment,
  #[alias = "requireResidentKey"]
  pub require_resident_key: bool = false,
  #[alias = "userVerification"]
  pub user_verification: UserVerificationRequirement = "preferred",
}

pub enum AuthenticatorAttachment {
  "platform", // Platform attachment
  "cross-platform" // Cross-platform attachment
}

pub enum AttestationConveyancePreference {
  #[alias = "none"]
  None,
  #[alias = "indirect"]
  Indirect,
  #[alias = "direct"]
  Direct
}

pub enum UserVerificationRequirement {
  #[alias = "required"]
  Required,
  #[alias = "preferred"]
  Preferred,
  #[alias = "discouraged"]
  Discouraged
}

pub struct PublicKeyCredentialRequestOptions {
  required BufferSource challenge;
  pub timeout: u32,
  pub rpId: USVString,
  #[alias = "allowCredentials"]
  pub allow_credentials: Vec<PublicKeyCredentialDescriptor> = [],
  #[alias = "userVerification"]
  pub user_verification: UserVerificationRequirement = "preferred",
  // FIXME: bug 1493860: should this "= {}" be Option<here>
  pub extensions: AuthenticationExtensionsClientInputs = {},
}

// TODO - Use partial dictionaries when bug 1436329 is fixed.
pub struct AuthenticationExtensionsClientInputs {
  // FIDO AppID Extension (appid)
  // <https://w3c.github.io/webauthn/#sctn-appid-extension>
  pub appid: USVString,

  // hmac-secret
  // <https://fidoalliance.org/specs/fido-v2.0-ps-20190130/fido-client-to-authenticator-protocol-v2.0-ps-20190130.html#sctn-hmac-secret-extension>
  #[alias = "hmacCreateSecret"]
  pub hmac_create_secret: bool,
}

// TODO - Use partial dictionaries when bug 1436329 is fixed.
pub struct AuthenticationExtensionsClientOutputs {
  // FIDO AppID Extension (appid)
  // <https://w3c.github.io/webauthn/#sctn-appid-extension>
  pub appid: bool,

  // <https://fidoalliance.org/specs/fido-v2.0-ps-20190130/fido-client-to-authenticator-protocol-v2.0-ps-20190130.html#sctn-hmac-secret-extension>
  #[alias = "hmacCreateSecret"]
  pub hmac_create_secret: bool,
}

pub type AuthenticationExtensionsAuthenticatorInputs = record<DOMString, DOMString>;

#[GenerateToJSON]
pub struct CollectedClientData {
  required DOMString type;
  required DOMString challenge;
  required DOMString origin;
  required DOMString hashAlgorithm;
  #[alias = "tokenBindingId"]
  pub token_binding_id: DOMString,
  // FIXME: bug 1493860: should this "= {}" be Option<here>
  #[alias = "clientExtensions"]
  pub client_extensions: AuthenticationExtensionsClientInputs = {},
  #[alias = "authenticatorExtensions"]
  pub authenticator_extensions: AuthenticationExtensionsAuthenticatorInputs,
}

pub enum PublicKeyCredentialType {
  #[alias = "public-key"]
  PublicKey
}

pub struct PublicKeyCredentialDescriptor {
  required PublicKeyCredentialType type;
  required BufferSource id;
  // Transports is a string that is matched against the AuthenticatorTransport
  // enumeration so that we have forward-compatibility for new transports.
  pub transports: Vec<DOMString>,
}

pub enum AuthenticatorTransport {
  #[alias = "usb"]
  Usb,  // TODO
  #[alias = "nfc"]
  Nfc,  // TODO
  #[alias = "ble"]
  Ble,  // TODO
  #[alias = "internal"]
  Internal
}

pub type COSEAlgorithmIdentifier = i32;

pub type AuthenticatorSelectionList = Vec<AAGUID>;

pub type AAGUID = BufferSource;

// FIDO AppID Extension (appid)
// <https://w3c.github.io/webauthn/#sctn-appid-extension>
partial pub struct AuthenticationExtensionsClientInputs {
  pub appid: USVString,
}

// FIDO AppID Extension (appid)
// <https://w3c.github.io/webauthn/#sctn-appid-extension>
partial pub struct AuthenticationExtensionsClientOutputs {
  pub appid: bool,
}
