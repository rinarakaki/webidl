#[pref = "device.sensors.ambientLight.enabled",
  func = "nsGlobalWindowInner::DeviceSensorsEnabled",
  exposed = Window]
#[alias = "DeviceLightEvent"]
pub trait Event::DeviceLight {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: DeviceLightEventInit)
    -> Self;

  readonly attribute unrestricted f64 value;
}

pub struct DeviceLightEventInit: EventInit {
  unrestricted f64 value = Infinity;
}
