// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-optgroup-element

#[exposed = Window]
pub trait HTMLOptGroupElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub disabled: bool,
  #[ce_reactions, setter_throws]
  pub label: DOMString,
}
