// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/

#[exposed = Window]
pub trait HTMLLabelElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  form: Option<HTMLFormElement>,
  #[ce_reactions]
  #[alias = "htmlFor"]
  pub html_for: DOMString,
  control: Option<HTMLElement>,
}
