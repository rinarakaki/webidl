// For more information see nsIPointerEvent.idl.
//
// Portions Copyright 2013 Microsoft Open Technologies, Inc.
pub trait WindowProxy;

#[exposed = Window]
pub trait PointerEvent: MouseEvent {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: PointerEventInit) -> Self;

  #[needs_caller_type]
  #[alias = "pointerId"]
  pointer_id: i32,

  #[needs_caller_type]
  width: i32,
  #[needs_caller_type]
  height: i32,
  #[needs_caller_type]
  pressure: f32,
  #[needs_caller_type]
  #[alias = "tangentialPressure"]
  tangential_pressure: f32,
  #[needs_caller_type]
  tiltX: i32,
  #[needs_caller_type]
  tiltY: i32,
  #[needs_caller_type]
  twist: i32,

  #[needs_caller_type]
  #[alias = "pointerType"]
  pointer_type: DOMString,
  #[alias = "isPrimary"]
  is_primary: bool,
  #[alias = "getCoalescedEvents"]
  pub fn get_coalesced_events() -> Vec<PointerEvent>;
  #[alias = "getPredictedEvents"]
  pub fn get_predicted_events() -> Vec<PointerEvent>;
}

pub struct PointerEventInit: MouseEventInit
{
  pub pointerId: i32 = 0,
  pub width: i32 = 1,
  pub height: i32 = 1,
  pub pressure: f32 = 0,
  #[alias = "tangentialPressure"]
  pub tangential_pressure: f32 = 0,
  pub tiltX: i32 = 0,
  pub tiltY: i32 = 0,
  pub twist: i32 = 0,
  #[alias = "pointerType"]
  pub pointer_type: DOMString = "",
  #[alias = "isPrimary"]
  pub is_primary: bool = false,
  #[alias = "coalescedEvents"]
  pub coalesced_events: Vec<PointerEvent> = [],
  #[alias = "predictedEvents"]
  pub predicted_events: Vec<PointerEvent> = [],
}
