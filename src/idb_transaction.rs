// The origin of this IDL file is
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBTransaction
// https://dvcs.w3.org/hg/IndexedDB/raw-file/tip/Overview.html#idl-def-IDBTransactionMode

use super::EventTarget;

pub enum IDBTransactionMode {
  #[alias = "readonly"]
  ReadOnly,
  #[alias = "readwrite"]
  ReadWrite,
  // The "readwriteflush" mode is only available when the
  // |IndexedDatabaseManager::ExperimentalFeaturesEnabled()| function returns
  // true. This mode is not yet part of the standard.
  #[alias = "readwriteflush"]
  ReadWriteFlush,
  #[alias = "cleanup"]
  Cleanup,
  #[alias = "versionchange"]
  VersionChange
}

#[exposed = (Window, Worker)]
pub trait IDBTransaction: EventTarget {
  #[throws]
  mode: IDBTransactionMode,
  db: IDBDatabase,

  error: Option<DOMException>,

  pub fn objectStore(name: DOMString) -> Result<IDBObjectStore>;

  pub fn commit() -> Result<()>;

  pub fn abort() -> Result<()>;

  #[alias = "onabort"]
  pub on_abort: EventHandler,
  #[alias = "oncomplete"]
  pub on_complete: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
}

// This seems to be custom
partial trait IDBTransaction {
  #[alias = "objectStoreNames"]
  object_store_names: DOMStringList,
}
