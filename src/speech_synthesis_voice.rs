// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

#[pref = "media.webspeech.synth.enabled",
 exposed = Window]
pub trait SpeechSynthesisVoice {
  #[alias = "voiceURI"]
  voice_uri: DOMString,
  name: DOMString,
  lang: DOMString,
  #[alias = "localService"]
  local_service: bool,
  default: bool,
}
