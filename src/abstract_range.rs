// The origin of this IDL file is
// https://dom.spec.whatwg.org/#abstractrange

use super::*;

#[exposed = Window]
pub trait AbstractRange {
    #[alias = "startContainer"]
    pub start_container: Node,
    #[alias = "startOffset"]
    pub start_offset: u32,
    #[alias = "endContainer"]
    pub end_container: Node,
    #[alias = "endOffset"]
    pub end_offset: u32,
    pub collapsed: bool,
}
