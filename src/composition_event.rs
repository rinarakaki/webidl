// https://w3c.github.io/uievents/#trait-compositionevent

use super::*;

#[exposed = Window]
pub trait CompositionEvent: UIEvent {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] init: CompositionEventInit)
    -> Self;

  pub data: Option<DOMString>,
  // locale is currently non-standard
  pub locale: DOMString,

 // ranges is trying to expose TextRangeArray in Gecko so a
 // js-plugin couble be able to know the clauses information
  #[chrome_only, Cached, pure]
  pub ranges: Vec<TextClause>,
}

pub struct CompositionEventInit: UIEventInit {
  pub #[optional = ""] data: DOMString
}

partial trait CompositionEvent {
  #[alias = initCompositionEvent]
  pub fn init_composition_event(
    typeArg: DOMString,
    #[optional = false] canBubbleArg: bool,
    #[optional = false] cancellableArg: bool,
    #[optional = None] viewArg: Option<Window>,
    #[optional = None] dataArg: Option<DOMString>,
    #[optional = ""] localeArg: DOMString);
}
