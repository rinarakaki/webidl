// The origin of this IDL file is
// https://storage.spec.whatwg.org/#storagemanager

#[SecureContext,
 exposed = (Window, Worker),
 pref = "dom.storageManager.enabled"]
pub trait StorageManager {
  #[throws]
  pub fn persisted() -> Promise<bool>;

  #[exposed = Window, throws]
  pub fn persist() -> Promise<bool>;

  #[throws]
  pub fn estimate() -> Promise<StorageEstimate>;
}

pub struct StorageEstimate {
  pub usage: u64,
  pub quota: u64,
}

#[SecureContext]
partial trait StorageManager {
  #[pref = "dom.fs.enabled"]
  #[alias = "getDirectory"]
  pub fn get_directory() -> Promise<FileSystemDirectoryHandle>;
}
