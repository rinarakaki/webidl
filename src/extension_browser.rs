// This IDL file is related to trait mixin for the additional globals that should be
// available in windows and service workers allowed to access the WebExtensions API and
// the WebExtensions browser API namespace.
//
// You are granted a license to use, reproduce and create derivative works of
// this document.

// WebExtensions API trait mixin (used to include ExtensionBrowser trait
// in the ServiceWorkerGlobalScope and Window).
#[exposed = (ServiceWorker)]
pub trait mixin ExtensionGlobalsMixin {
  #[replaceable, same_object, binary_name = "AcquireExtensionBrowser",
  BindingAlias = "chrome", func = "extensions::ExtensionAPIAllowed"]
  browser: ExtensionBrowser,
}

#[exposed = (ServiceWorker), LegacyNoInterfaceObject]
pub trait ExtensionBrowser {
  // A mock API only exposed in tests to unit test the internals
  // meant to be reused by the real WebExtensions API bindings
  // in xpcshell tests.
  #[replaceable, same_object, binary_name = "GetExtensionMockAPI",
  func = "mozilla::extensions::ExtensionMockAPI::IsAllowed",
  pref = "extensions.webidl-api.expose_mock_interface"]
  #[alias = "mockExtensionAPI"]
  mock_extension_api: ExtensionMockAPI,

  // `browser.alarms` API namespace
  #[replaceable, same_object, binary_name = "GetExtensionAlarms",
  func = "mozilla::extensions::ExtensionAlarms::IsAllowed"]
  alarms: ExtensionAlarms,

  // `browser.runtime` API namespace
  #[replaceable, same_object, binary_name = "GetExtensionRuntime",
  func = "mozilla::extensions::ExtensionRuntime::IsAllowed"]
  runtime: ExtensionRuntime,

  // `browser.test` API namespace, available in tests.
  #[replaceable, same_object, binary_name = "GetExtensionTest",
  func = "mozilla::extensions::ExtensionTest::IsAllowed"]
  test: ExtensionTest,
}
