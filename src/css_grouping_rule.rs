// The origin of this IDL file is
// https://drafts.csswg.org/cssom/#cssgroupingrule

use super::{CSSRule, CSSRuleList};

#[exposed = Window]
pub trait CSSGroupingRule: CSSRule {
  #[same_object]
  #[alias = "cssRules"]
  css_rules: Option<CSSRuleList>,

  #[alias = "insertRule"]
  pub fn insert_rule(&self, rule: UTF8String, #[optional = 0] index: u32)
    -> Result<u32>;

  #[alias = "deleteRule"]
  pub fn delete_rule(&self, index: u32) -> Result<()>;
}
