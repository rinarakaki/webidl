// The origin of this IDL file is
// https://w3c.github.io/resource-timing/#sec-performanceresourcetiming

#[exposed = (Window, Worker)]
pub trait PerformanceResourceTiming: PerformanceEntry {
  #[alias = "initiatorType"]
  initiator_type: DOMString,
  #[alias = "nextHopProtocol"]
  next_hop_protocol: DOMString,

  #[alias = "workerStart"]
  worker_start: DOMHighResTimeStamp,

  #[needs_subject_principal]
  #[alias = "redirectStart"]
  redirect_start: DOMHighResTimeStamp,
  #[needs_subject_principal]
  #[alias = "redirectEnd"]
  redirect_end: DOMHighResTimeStamp,

  #[alias = "fetchStart"]
  fetch_start: DOMHighResTimeStamp,

  #[needs_subject_principal]
  #[alias = "domainLookupStart"]
  domain_lookup_start: DOMHighResTimeStamp,
  #[needs_subject_principal]
  #[alias = "domainLookupEnd"]
  domain_lookup_end: DOMHighResTimeStamp,
  #[needs_subject_principal]
  #[alias = "connectStart"]
  connect_start: DOMHighResTimeStamp,
  #[needs_subject_principal]
  #[alias = "connectEnd"]
  connect_end: DOMHighResTimeStamp,
  #[needs_subject_principal]
  #[alias = "secureConnectionStart"]
  secure_connection_start: DOMHighResTimeStamp,
  #[needs_subject_principal]
  #[alias = "requestStart"]
  request_start: DOMHighResTimeStamp,
  #[needs_subject_principal]
  #[alias = "responseStart"]
  response_start: DOMHighResTimeStamp,

  #[alias = "responseEnd"]
  response_end: DOMHighResTimeStamp,

  #[needs_subject_principal]
  #[alias = "transferSize"]
  transfer_size: u64,
  #[needs_subject_principal]
  #[alias = "encodedBodySize"]
  encoded_body_size: u64,
  #[needs_subject_principal]
  #[alias = "decodedBodySize"]
  decoded_body_size: u64,

  // TODO: Use FrozenArray once available. (1236777: Bug)
  // readonly attribute FrozenArray<PerformanceServerTiming> serverTiming;
  #[SecureContext, Frozen, Cached, pure, needs_subject_principal]
  #[alias = "serverTiming"]
  server_timing: Vec<PerformanceServerTiming>,

  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;
}
