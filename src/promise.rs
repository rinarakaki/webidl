// This IDL file contains utilities to help connect JS promises to our
// Web IDL infrastructure.

pub type PromiseJobCallback = Fn();

#[TreatNonCallableAsNull]
pub type AnyCallback = Fn(value: any) -> any;

// Hack to allow us to have JS owning and properly tracing/CCing/etc a
// PromiseNativeHandler.
#[LegacyNoInterfaceObject, exposed = (Window, Worker)]
pub trait PromiseNativeHandler {}
