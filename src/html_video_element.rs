// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-video-element

use super::HTMLMediaElement;

#[exposed = Window]
pub trait HTMLVideoElement: HTMLMediaElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub width: u32,
  #[ce_reactions, setter_throws]
  pub height: u32,
  #[alias = "videoWidth"]
  video_width: u32,
  #[alias = "videoHeight"]
  video_height: u32,
  #[ce_reactions, setter_throws]
  pub poster: DOMString,
}

partial trait HTMLVideoElement {
  // A count of the number of video frames that have demuxed from the media
  // resource. If we were playing perfectly, we'd be able to paint this many
  // frames.
  #[alias = "mozParsedFrames"]
  moz_parsed_frames: u32,

  // A count of the number of frames that have been decoded. We may drop
  // frames if the decode is taking too much time.
  #[alias = "mozDecodedFrames"]
  moz_decoded_frames: u32,

  // A count of the number of frames that have been presented to the rendering
  // pipeline. We may drop frames if they arrive late at the renderer.
  #[alias = "mozPresentedFrames"]
  moz_presented_frames: u32,

  // Number of presented frames which were painted on screen.
  #[alias = "mozPaintedFrames"]
  moz_painted_frames: u32,

  // Time which the last painted video frame was late by, in seconds.
  #[alias = "mozFrameDelay"]
  moz_frame_delay: f64,

  // True if the video has an audio track available.
  #[alias = "mozHasAudio"]
  moz_has_audio: bool,

  // Attributes for builtin video controls to lock screen orientation.
  // True if video controls should lock orientation when fullscreen.
  #[pref = "media.videocontrols.lock-video-orientation",
    func = "IsChromeOrUAWidget"]
  #[alias = "mozOrientationLockEnabled"]
  moz_orientation_lock_enabled: bool,
  // True if screen orientation is locked by video controls.
  #[pref = "media.videocontrols.lock-video-orientation",
    func = "IsChromeOrUAWidget"]
  #[alias = "mozIsOrientationLocked"]
  pub moz_is_orientation_locked: bool,

  // Clones the frames playing in this <video> to the target. Cloning ends
  // when either node is removed from their DOM trees. throws if one or
  // both <video> elements are not attached to a DOM tree.
  // Returns a promise that resolves when the target's ImageContainer has been
  // installed in this <video>'s MediaDecoder, or selected video
  // MediaStreamTrack, whichever is available first. Note that it might never
  // resolve.
  #[throws, func = "IsChromeOrUAWidget"]
  #[alias = "cloneElementVisually"]
  pub fn clone_element_visually(target: HTMLVideoElement) -> Promise<void>;

  // Stops a <video> from cloning visually. Does nothing if the <video>
  // wasn't cloning in the first place.
  #[func = "IsChromeOrUAWidget"]
  #[alias = "stopCloningElementVisually"]
  pub fn stop_cloning_element_visually();

  // Returns true if the <video> is being cloned visually to another
  // <video> element (cloneElementVisually: see).
  #[func = "IsChromeOrUAWidget"]
  #[alias = "isCloningElementVisually"]
  is_cloning_element_visually: bool,
}

// https://dvcs.w3.org/hg/html-media/raw-file/default/media-source/media-source.html#idl-def-HTMLVideoElement
partial trait HTMLVideoElement {
  #[pref = "media.mediasource.enabled", new_object]
  #[alias = "getVideoPlaybackQuality"]
  pub fn get_video_playback_quality() -> VideoPlaybackQuality;
}
