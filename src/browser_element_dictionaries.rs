use super::*;

#[GenerateConversionToJS]
pub struct OpenWindowEventDetail {
  pub #[optional = ""] url: DOMString,
  pub #[optional = ""] name: DOMString,
  pub #[optional = ""] features: DOMString,
  #[alias = "frameElement"]
  pub #[optional = None] frame_element: Option<Node>,
  #[alias = "forceNoReferrer"]
  pub #[optional = false] force_no_referrer: bool,
}

#[GenerateConversionToJS]
pub struct DOMWindowResizeEventDetail {
  pub #[optional = 0] width: i32,
  pub #[optional = 0] height: i32,
}
