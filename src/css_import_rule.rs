// The origin of this IDL file is
// https://drafts.csswg.org/cssom/#cssimportrule

// https://drafts.csswg.org/cssom/#cssimportrule
#[exposed = Window]
pub trait CSSImportRule: CSSRule {
  href: DOMString,
  // Per spec, the .media is never None, but in our implementation it can
  // be since stylesheet can be None, and in Stylo, media is derived from
  // the stylesheet. See <https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1326509>.
  #[same_object, put_forwards = mediaText]
  media: Option<MediaList>,
  // Per spec, the .styleSheet is never None, but in our implementation it can
  // be. See <https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1326509>.
  #[same_object, binary_name = "styleSheetForBindings"]
  #[alias = "styleSheet"]
  style_sheet: Option<CSSStyleSheet>,
}

// https://drafts.csswg.org/css-cascade-5/#extensions-to-cssimportrule-trait
partial trait CSSImportRule {
  #[pref = "layout.css.cascade-layers.enabled"]
  #[alias = "layerName"]
  layer_name: Option<UTF8String>,
}
