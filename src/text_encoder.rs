// The origin of this IDL file is
// http://encoding.spec.whatwg.org/#trait-textencoder

pub struct TextEncoderEncodeIntoResult {
  pub read: u64,
  pub written: u64,
}

#[exposed = (Window, Worker)]
pub trait TextEncoder {
  #[alias = "constructor"]
  pub fn new() -> Self;

  //
  // This is DOMString in the spec, but the value is always ASCII
  // and short. By declaring this as ByteString, we get the same
  // end result (storage as inline Latin1 string in SpiderMonkey)
  // with fewer conversions.
  #[constant]
  encoding: ByteString,

  //
  // This is spec-wise USVString but marking it as UTF8String as an
  // optimization. (The SpiderMonkey-provided conversion to UTF-8 takes care of
  // replacing lone surrogates with the REPLACEMENT CHARACTER, so the
  // observable behavior of USVString is matched.)
  #[new_object]
  pub fn encode(#[optional = ""] input: UTF8String) -> Uint8Array;

  //
  // The same comment about UTF8String as above applies here with JSString.
  //
  // We use JSString because we don't want to encode the full string, just as
  // much as the capacity of the Uint8Array.
  #[CanOOM]
  #[alias = "encodeInto"]
  pub fn encode_into(source: JSString, source: JSString) -> TextEncoderEncodeIntoResult;
}
