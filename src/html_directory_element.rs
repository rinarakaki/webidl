// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
#[exposed = Window]
pub trait HTMLDirectoryElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub compact: bool,
}

