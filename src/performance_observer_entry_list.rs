// The origin of this IDL file is
// https://w3c.github.io/performance-timeline/#the-performanceobserverentrylist-trait

// XXX should be moved into Performance.webidl.
pub struct PerformanceEntryFilterOptions {
  pub name: DOMString,
  #[alias = "entryType"]
  pub entry_type: DOMString,
  #[alias = "initiatorType"]
  pub initiator_type: DOMString,
}

#[pref = "dom.enable_performance_observer",
 exposed = (Window, Worker)]
pub trait PerformanceObserverEntryList {
  #[alias = "getEntries"]
  pub fn get_entries(#[optional = {}] filter: PerformanceEntryFilterOptions) -> PerformanceEntryList;
  #[alias = "getEntriesByType"]
  pub fn get_entries_by_type(entryType: DOMString) -> PerformanceEntryList;
  PerformanceEntryList getEntriesByName(name: DOMString,
  optional entryType: DOMString);
}

