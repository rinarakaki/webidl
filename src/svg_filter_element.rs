// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFilterElement: SVGElement {
  #[constant]
  #[alias = "filterUnits"]
  filter_units: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "primitiveUnits"]
  primitive_units: SVGAnimatedEnumeration,
  #[constant]
  x: SVGAnimatedLength,
  #[constant]
  y: SVGAnimatedLength,
  #[constant]
  width: SVGAnimatedLength,
  #[constant]
  height: SVGAnimatedLength,

  // ImageData apply(source: ImageData);
}

SVGFilterElement includes SVGURIReference;

