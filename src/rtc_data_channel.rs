pub enum RTCDataChannelState {
  #[alias = "connecting"]
  Connecting,
  #[alias = "open"]
  Open,
  #[alias = "closing"]
  Closing,
  #[alias = "closed"]
  Closed
}

pub enum RTCDataChannelType {
  #[alias = "arraybuffer"]
  ArrayBuffer,
  #[alias = "blob"]
  Blob
}

#[exposed = Window]
pub trait RTCDataChannel: EventTarget {
  label: DOMString,
  negotiated: bool,
  ordered: bool,
  reliable: bool,
  readonly maxPacketLifeTime: Option<u16>;
  readonly maxRetransmits: Option<u16>;
  protocol: USVString,
  readonly id: Option<u16>;
  #[alias = "readyState"]
  ready_state: RTCDataChannelState,
  #[alias = "bufferedAmount"]
  buffered_amount: u32,
  #[alias = "bufferedAmountLowThreshold"]
  pub buffered_amount_low_threshold: u32,
  #[alias = "onopen"]
  pub on_open: EventHandler,
  #[alias = "onerror"]
  pub on_error: EventHandler,
  #[alias = "onclose"]
  pub on_close: EventHandler,
  pub fn close();
  #[alias = "onmessage"]
  pub on_message: EventHandler,
  #[alias = "onbufferedamountlow"]
  pub on_buffered_amount_low: EventHandler,
  #[alias = "binaryType"]
  pub binary_type: RTCDataChannelType,

  pub fn send(data: DOMString) -> Result<()>;
  pub fn send(data: Blob) -> Result<()>;
  pub fn send(data: ArrayBuffer) -> Result<()>;
  pub fn send(data: ArrayBufferView) -> Result<()>;
}
