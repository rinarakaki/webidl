// The origin of this IDL file is
// https://testutils.spec.whatwg.org/#the-testutils-namespace

#[exposed = (Window, Worker),
 pref = "dom.testing.testutils.enabled"]
namespace TestUtils {
  #[new_object, throws]
  pub fn gc() -> Promise<void>;
}
