// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/multipage/common-dom-interfaces.html#domstringmap-0

#[LegacyOverrideBuiltIns, exposed = Window]
pub trait DOMStringMap {
  getter DOMString (name: DOMString);
  #[ce_reactions, throws]
  setter void (name: DOMString, name: DOMString);
  #[ce_reactions]
  deleter void (name: DOMString);
}
