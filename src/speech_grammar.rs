// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

#[pref = "media.webspeech.recognition.enable",
 LegacyFactoryfunction = webkitSpeechGrammar,
 func = "SpeechRecognition::IsAuthorized",
 exposed = Window]
pub trait SpeechGrammar {
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[throws]
  pub src: DOMString,
  #[throws]
  pub weight: f32,
}

