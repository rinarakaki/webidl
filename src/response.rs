// The origin of this IDL file is
// https://fetch.spec.whatwg.org/#response-class

#[exposed = (Window, Worker)]
pub trait Response {
  // This should be constructor(optional BodyInit... but BodyInit doesn't
  // include ReadableStream yet because we don't want to expose Streams API to
  // Request.
  #[throws]
  constructor(optional (Blob or BufferSource or FormData or URLSearchParams or ReadableStream or USVString)? body = None,
  #[optional = {}] init: ResponseInit);

  #[new_object]
  static Response error();
  #[throws,
  new_object] static Response redirect(url: USVString, #[optional = 302] status: u16);

  type: ResponseType,

  url: USVString,
  redirected: bool,
  status: u16,
  ok: bool,
  #[alias = "statusText"]
  status_text: ByteString,
  #[same_object, binary_name = "headers_"]
  headers: Headers,

  #[throws,
  new_object] Response clone();

  #[chrome_only, new_object, throws]
  #[alias = "cloneUnfiltered"]
  pub fn clone_unfiltered() -> Response;

  // For testing only.
  #[chrome_only]
  #[alias = "hasCacheInfoChannel"]
  has_cache_info_channel: bool,
}
Response includes Body;

// This should be part of Body but we don't want to expose body to request yet.
// See bug 1387483.
partial trait Response {
  #[getter_throws, pref = "javascript.options.streams"]
  body: Option<ReadableStream>,
}

pub struct ResponseInit {
  pub status: u16 = 200,
  #[alias = "statusText"]
  pub status_text: ByteString = "",
  pub headers: HeadersInit,
}

pub enum ResponseType { "basic", "cors", "default", "error", "opaque", "opaqueredirect" };
