// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct StereoPannerOptions: AudioNodeOptions {
  pub pan: f32 = 0,
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait StereoPannerNode: AudioNode {
  #[throws]
  constructor(context: BaseAudioContext,
  #[optional = {}] options: StereoPannerOptions);

  pan: AudioParam,
}

// Mozilla extension
StereoPannerNode includes AudioNodePassThrough;

