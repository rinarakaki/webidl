// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-pre-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-pre-element
#[exposed = Window]
pub trait HTMLPreElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLPreElement {
  #[ce_reactions, setter_throws]
  pub width: i32,
}
