// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

#[pref = "media.webspeech.recognition.enable",
 func = "SpeechRecognition::IsAuthorized",
 exposed = Window]
pub trait SpeechRecognitionResult {
  length: u32,
  getter SpeechRecognitionAlternative item(index: u32);
  #[alias = "isFinal"]
  is_final: bool,
}
