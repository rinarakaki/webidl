// The origin of this IDL file is
// https://w3c.github.io/ServiceWorker/#navigation-preload-manager

#[pref = "dom.serviceWorkers.navigationPreload.enabled", SecureContext,
 exposed = (Window, Worker)]
pub trait NavigationPreloadManager {
  pub fn enable() -> Promise<void>;
  pub fn disable() -> Promise<void>;
  #[alias = "setHeaderValue"]
  pub fn set_header_value(value: ByteString) -> Promise<void>;
  #[alias = "getState"]
  pub fn get_state() -> Promise<NavigationPreloadState>;
}

pub struct NavigationPreloadState {
  pub enabled: bool = false,
  #[alias = "headerValue"]
  pub header_value: ByteString,
}
