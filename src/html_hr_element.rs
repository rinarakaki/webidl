// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-hr-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-hr-element
#[exposed = Window]
pub trait HTMLHRElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLHRElement {
  #[ce_reactions, setter_throws]
  pub align: DOMString,
  #[ce_reactions, setter_throws]
  pub color: DOMString,
  #[ce_reactions, setter_throws]
  #[alias = "noShade"]
  pub no_shade: bool,
  #[ce_reactions, setter_throws]
  pub size: DOMString,
  #[ce_reactions, setter_throws]
  pub width: DOMString,
}
