// The origin of this IDL file is
// http://fetch.spec.whatwg.org/#headers-class

pub type HeadersInit = (Vec<Vec<ByteString>> or record<ByteString, ByteString>);

pub enum HeadersGuardEnum {
  #[alias = "none"]
  None,
  #[alias = "request"]
  Request,
  #[alias = "request-no-cors"]
  RequestNoCORS,
  #[alias = "response"]
  Response,
  #[alias = "immutable"]
  Immutable
}

#[exposed = (Window, Worker)]
pub trait Headers {
  #[alias = "constructor"]
  pub fn new(optional init: HeadersInit) -> Result<Self>;

  pub fn append(name: ByteString, name: ByteString) -> Result<()>;

  pub fn delete(name: ByteString) -> Result<()>;

  pub fn get(name: ByteString) -> Result<Option<ByteString>>;

  pub fn has(name: ByteString) -> Result<bool>;
  
  pub fn set(name: ByteString, name: ByteString) -> Result<()>;
  iterable<ByteString, ByteString>;  // TODO

  // Used to test different guard states from mochitest.
  // Note: Must be set prior to populating headers or will throw.
  #[chrome_only, setter_throws]
  pub guard: HeadersGuardEnum,
}
