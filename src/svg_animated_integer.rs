// The origin of this IDL file is
// https://svgwg.org/svg2-draft/types.html#InterfaceSVGAnimatedInteger

#[exposed = Window]
pub trait SVGAnimatedInteger {
  #[alias = "baseVal"]
  pub base_val: i32,
  #[alias = "animVal"]
  anim_val: i32,
}
