use super::*;

// https://html.spec.whatwg.org/#dom-window-customelements
#[exposed = Window]
pub trait CustomElementRegistry {
  #[ce_reactions, use_counter]
  pub fn define(
    &self,
    name: DOMString, constructor: CustomElementConstructor,
    #[optional = {}] options: ElementDefinitionOptions) -> Result<()>;

  #[chrome_only]
  #[alias = "setElementCreationCallback"]
  pub fn set_element_creation_callback(
    &self, name: DOMString, callback: CustomElementCreationCallback)
    -> Result<()>;

  pub fn get(&self, name: DOMString) -> any;
  
  #[alias = "whenDefined"]
  pub fn when_defined(&self, name: DOMString)
    -> Result<Promise<CustomElementConstructor>>;

  #[ce_reactions]
  pub fn upgrade(&self, root: Node);
}

pub struct ElementDefinitionOptions {
  pub #[optional] extends: DOMString,
}

callback constructor CustomElementConstructor = any ();

#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
type CustomElementCreationCallback = Fn(name: DOMString);

#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
type LifecycleConnectedCallback = Fn();
#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
type LifecycleDisconnectedCallback = Fn();
#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
type LifecycleAdoptedCallback = Fn(oldDocument: Option<Document>,
  Option<Document> newDocment);
#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
type LifecycleAttributeChangedCallback = Fn(attrName: DOMString,
  Option<DOMString> oldValue,
  Option<DOMString> newValue,
  Option<DOMString> namespaceURI);
#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
type LifecycleFormAssociatedCallback = Fn(form: Option<HTMLFormElement>);
#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
type LifecycleFormResetCallback = Fn();
#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
type LifecycleFormDisabledCallback = Fn(disabled: bool);
#[MOZ_CAN_RUN_SCRIPT_BOUNDARY]
type LifecycleGetCustomInterfaceCallback = Option<object>(iid: any);

#[GenerateInit]
pub struct LifecycleCallbacks {
  #[alias = "connectedCallback"]
  pub #[optional] connected_callback: LifecycleConnectedCallback,
  #[alias = "disconnectedCallback"]
  pub #[optional] disconnected_callback: LifecycleDisconnectedCallback,
  #[alias = "adoptedCallback"]
  pub #[optional] adopted_callback: LifecycleAdoptedCallback,
  #[alias = "attributeChangedCallback"]
  pub #[optional] attribute_changed_callback: LifecycleAttributeChangedCallback,
  #[alias = "formAssociatedCallback"]
  pub #[optional] form_associated_callback: LifecycleFormAssociatedCallback,
  #[alias = "formResetCallback"]
  pub #[optional] form_reset_callback: LifecycleFormResetCallback,
  #[alias = "formDisabledCallback"]
  pub #[optional] form_disabled_callback: LifecycleFormDisabledCallback,
  #[chrome_only]
  #[alias = "getCustomInterfaceCallback"]
  pub #[optional] get_custom_interface_callback:
    LifecycleGetCustomInterfaceCallback,
}
