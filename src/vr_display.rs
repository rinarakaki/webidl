// The origin of this IDL file is
// https://immersive-web.github.io/webvr/spec/1.1/

pub enum VREye {
  #[alias = "left"]
  Left,
  #[alias = "right"]
  Right
}

#[pref = "dom.vr.enabled",
 HeaderFile = "mozilla/dom/VRDisplay.h",
 SecureContext,
 exposed = Window]
pub trait VRFieldOfView {
  #[alias = "upDegrees"]
  up_degrees: f64,
  #[alias = "rightDegrees"]
  right_degrees: f64,
  #[alias = "downDegrees"]
  down_degrees: f64,
  #[alias = "leftDegrees"]
  left_degrees: f64,
}

pub type VRSource = (HTMLCanvasElement or OffscreenCanvas);

pub struct VRLayer {
  // XXX - When WebVR in WebWorkers is implemented, HTMLCanvasElement below
  //       should be replaced with VRSource.
  pub source: Option<HTMLCanvasElement> = None,

  // The left and right viewports contain 4 values defining the viewport
  // rectangles within the canvas to present to the eye in UV space.
  // #[0] left offset of the viewport (0.0 - 1.0)
  // #[1] top offset of the viewport (0.0 - 1.0)
  // #[2] width of the viewport (0.0 - 1.0)
  // #[3] height of the viewport (0.0 - 1.0)
  //
  // When no values are passed, they will be processed as though the left
  // and right sides of the viewport were passed:
  //
  // leftBounds: #[0.0, 0.0, 0.5, 1.0]
  // rightBounds: #[0.5, 0.0, 0.5, 1.0]
  #[alias = "leftBounds"]
  pub left_bounds: Vec<f32> = [],
  #[alias = "rightBounds"]
  pub right_bounds: Vec<f32> = [],
}

// Values describing the capabilities of a VRDisplay.
// These are expected to be static per-device/per-user.
#[pref = "dom.vr.enabled",
 HeaderFile = "mozilla/dom/VRDisplay.h",
 SecureContext,
 exposed = Window]
pub trait VRDisplayCapabilities {
  // hasPosition is true if the VRDisplay is capable of tracking its position.
  #[alias = "hasPosition"]
  has_position: bool,

  // hasOrientation is true if the VRDisplay is capable of tracking its orientation.
  #[alias = "hasOrientation"]
  has_orientation: bool,

  // Whether the VRDisplay is separate from the device’s
  // primary display. If presenting VR content will obscure
  // other content on the device, this should be false. When
  // false, the application should not attempt to mirror VR content
  // or update non-VR UI because that content will not be visible.
  #[alias = "hasExternalDisplay"]
  has_external_display: bool,

  // Whether the VRDisplay is capable of presenting content to an HMD or similar device.
  // Can be used to indicate “magic window” devices that are capable of 6DoF tracking but for
  // which requestPresent is not meaningful. If false then calls to requestPresent should
  // always fail, and getEyeParameters should return None.
  #[alias = "canPresent"]
  can_present: bool,

  // Indicates the maximum length of the array that requestPresent() will accept. MUST be 1 if
  #[alias = "canPresent"]
  can_present is true, 0 otherwise.
  #[alias = "maxLayers"]
  max_layers: u32,
}

// Values describing the the stage / play area for devices
// that support room-scale experiences.
#[pref = "dom.vr.enabled",
 HeaderFile = "mozilla/dom/VRDisplay.h",
 SecureContext,
 exposed = Window]
pub trait VRStageParameters {
  // A 16-element array containing the components of a column-major 4x4
  // affine transform matrix. This matrix transforms the sitting-space position
  // returned by get{Immediate}Pose() to a standing-space position.
  #[throws]
  #[alias = "sittingToStandingTransform"]
  sitting_to_standing_transform: F32Array,

  // Dimensions of the play-area bounds. The bounds are defined
  // as an axis-aligned rectangle on the floor.
  // The center of the rectangle is at (0,0, 0) in standing-space
  // coordinates.
  // These bounds are defined for safety purposes.
  // Content should not require the user to move beyond these
  // bounds; however, it is possible for the user to ignore
  // the bounds resulting in position values outside of
  // this rectangle.
  sizeX: f32,
  sizeZ: f32,
}

#[pref = "dom.vr.enabled",
 HeaderFile = "mozilla/dom/VRDisplay.h",
 SecureContext,
 exposed = Window]
pub trait VRPose
{
  // position, linearVelocity, and linearAcceleration are 3-component vectors.
  // position is relative to a sitting space. Transforming this point with
  // VRStageParameters.sittingToStandingTransform converts this to standing space.
  #[constant, throws]
  position: Option<F32Array>,
  #[constant, throws]
  #[alias = "linearVelocity"]
  linear_velocity: Option<F32Array>,
  #[constant, throws]
  #[alias = "linearAcceleration"]
  linear_acceleration: Option<F32Array>,

  // orientation is a 4-entry array representing the components of a quaternion. #[constant, throws]
  orientation: Option<F32Array>,
  // angularVelocity and angularAcceleration are the components of 3-dimensional vectors. #[constant, throws]
  #[alias = "angularVelocity"]
  angular_velocity: Option<F32Array>,
  #[constant, throws]
  #[alias = "angularAcceleration"]
  angular_acceleration: Option<F32Array>,
}

#[pref = "dom.vr.enabled",
 HeaderFile = "mozilla/dom/VRDisplay.h",
 SecureContext,
 exposed = Window]
pub trait VRFrameData {
  #[alias = "constructor"]
  pub fn new() -> Self;

  timestamp: DOMHighResTimeStamp,

  #[throws, pure]
  #[alias = "leftProjectionMatrix"]
  left_projection_matrix: F32Array,
  #[throws, pure]
  #[alias = "leftViewMatrix"]
  left_view_matrix: F32Array,

  #[throws, pure]
  #[alias = "rightProjectionMatrix"]
  right_projection_matrix: F32Array,
  #[throws, pure]
  #[alias = "rightViewMatrix"]
  right_view_matrix: F32Array,

  #[pure]
  pose: VRPose,
}

#[pref = "dom.vr.enabled",
 HeaderFile = "mozilla/dom/VRDisplay.h",
 SecureContext,
 exposed = Window]
pub trait VREyeParameters {
  // offset is a 3-component vector representing an offset to
  // translate the eye. This value may vary from frame
  // to frame if the user adjusts their headset ipd.
  #[constant, throws]
  offset: F32Array,

  // These values may vary as the user adjusts their headset ipd. #[constant]
  #[alias = "fieldOfView"]
  field_of_view: VRFieldOfView,

  // renderWidth and renderHeight specify the recommended render target
  // size of each eye viewport, in pixels. If multiple eyes are rendered
  // in a single render target, then the render target should be made large
  // enough to fit both viewports.
  #[constant]
  #[alias = "renderWidth"]
  render_width: u32,
  #[constant]
  #[alias = "renderHeight"]
  render_height: u32,
}

#[pref = "dom.vr.enabled",
 HeaderFile = "mozilla/dom/VRDisplay.h",
 SecureContext,
 exposed = Window]
pub trait VRDisplay: EventTarget {
  // presentingGroups is a bitmask indicating which VR session groups
  // have an active VR presentation.
  #[chrome_only]
  #[alias = "presentingGroups"]
  presenting_groups: u32,
  // Setting groupMask causes submitted frames by VR sessions that
  // aren't included in the bitmasked groups to be ignored.
  // Non-chrome content is not aware of the value of groupMask.
  // VRDisplay.RequestAnimationFrame will still fire for VR sessions
  // that are hidden by groupMask, enabling their performance to be
  // measured by chrome UI that is presented in other groups.
  // This is expected to be used in cases where chrome UI is presenting
  // information during link traversal or presenting options when content
  // performance is too low for comfort.
  // The VR refresh / VSync cycle is driven by the visible content
  // and the non-visible content may have a throttled refresh rate.
  #[chrome_only]
  #[alias = "groupMask"]
  pub group_mask: u32,

  #[alias = "isConnected"]
  is_connected: bool,
  #[alias = "isPresenting"]
  is_presenting: bool,

  // Dictionary of capabilities describing the VRDisplay.
  #[constant]
  capabilities: VRDisplayCapabilities,

  // If this VRDisplay supports room-scale experiences, the optional
  // stage attribute contains details on the room-scale parameters.
  #[alias = "stageParameters"]
  stage_parameters: Option<VRStageParameters>,

  // Return the current VREyeParameters for the given eye. VREyeParameters getEyeParameters(whichEye: VREye);

  // An identifier for this distinct VRDisplay. Used as an
  // association point in the Gamepad API.
  #[constant]
  displayId: u32,

  // A display name, a user-readable name identifying it.
  #[constant]
  #[alias = "displayName"]
  display_name: DOMString,

  // Populates the passed VRFrameData with the information required to render
  // the current frame.
  #[alias = "getFrameData"]
  pub fn get_frame_data(frameData: VRFrameData) -> bool;

  // Return a VRPose containing the future predicted pose of the VRDisplay
  // when the current frame will be presented. Subsequent calls to getPose()
  // MUST return a VRPose with the same values until the next call to
  // submitFrame().
  //
  // The VRPose will contain the position, orientation, velocity,
  // and acceleration of each of these properties.
  #[new_object]
  #[alias = "getPose"]
  pub fn get_pose() -> VRPose;

  // Reset the pose for this display, treating its current position and
  // orientation as the "origin/zero" values. VRPose.position,
  // VRPose.orientation, and VRStageParameters.sittingToStandingTransform may be
  // updated when calling resetPose(). This should be called in only
  // sitting-space experiences.
  #[alias = "resetPose"]
  pub fn reset_pose();

  // z-depth defining the near plane of the eye view frustum
  // enables mapping of values in the render target depth
  // attachment to scene coordinates. Initially set to 0.01.
  #[alias = "depthNear"]
  pub depth_near: f64,

  // z-depth defining the far plane of the eye view frustum
  // enables mapping of values in the render target depth
  // attachment to scene coordinates. Initially set to 10000.0.
  #[alias = "depthFar"]
  pub depth_far: f64,

  // The callback passed to `requestAnimationFrame` will be called
  // any time a new frame should be rendered. When the VRDisplay is
  // presenting the callback will be called at the native refresh
  // rate of the HMD. When not presenting this function acts
  // identically to how window.requestAnimationFrame acts. Content should
  // make no assumptions of frame rate or vsync behavior as the HMD runs
  // asynchronously from other displays and at differing refresh rates.
  #[throws]
  #[alias = "requestAnimationFrame"]
  pub fn request_animation_frame(callback: FrameRequestCallback) -> i32;

  // Passing the value returned by `requestAnimationFrame` to
  // `cancelAnimationFrame` will unregister the callback.
  #[throws]
  #[alias = "cancelAnimationFrame"]
  pub fn cancel_animation_frame(handle: i32);

  // Begin presenting to the VRDisplay. Must be called in response to a user gesture.
  // Repeat calls while already presenting will update the VRLayers being displayed.
  #[throws, needs_caller_type]
  #[alias = "requestPresent"]
  pub fn request_present(layers: Vec<VRLayer>) -> Promise<void>;

  // Stops presenting to the VRDisplay.
  #[throws]
  #[alias = "exitPresent"]
  pub fn exit_present() -> Promise<void>;

  // Get the layers currently being presented.
  #[alias = "getLayers"]
  pub fn get_layers() -> Vec<VRLayer>;

  // The VRLayer provided to the VRDisplay will be captured and presented
  // in the HMD. Calling this function has the same effect on the source
  // canvas as any other operation that uses its source image, and canvases
  // created without preserveDrawingBuffer set to true will be cleared.
  #[alias = "submitFrame"]
  pub fn submit_frame();
}
