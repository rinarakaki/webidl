// The origin of this IDL file is
// https://webaudio.github.io/web-midi-api/

#[SecureContext, pref = "dom.webmidi.enabled",
 exposed = Window]
pub trait MIDIOutput: MIDIPort {
  #[throws]
  pub fn send(data: Vec<octet>, optional timestamp: DOMHighResTimeStamp);
  pub fn clear();
}
