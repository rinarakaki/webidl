BinaryData

DOMString
USVString
UTF8String

#[alias = "Int32Array"]
I32Array

#[alias = "Uint8Array"]
U8Array

#[alias = "Uint32Array"]
U32Array

#[alias = "Float32Array"]
F32Array

#[alias = "Float64Array"]
F64Array

ArrayBuffer

Promise
