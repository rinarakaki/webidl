// The origin of this IDL file is
// http://www.w3.org/TR/2012/WD-dom-20120105/

use super::*;

#[exposed = (Window, Worker)]
pub trait CustomEvent: Event {
  #[alias = "constructor"]
  pub fn new(
    type: DOMString,
    #[optional = {}] init: CustomEventInit)
    -> Self;

  pub detail: any,

  // initCustomEvent is a Gecko specific deprecated method.
  #[deprecated]
  pub fn initCustomEvent(
    type: DOMString,
    #[optional = false] canBubble: bool,
    #[optional = false] cancelable: bool,
    #[optional = None] detail: any);
}

pub struct CustomEventInit: EventInit {
  pub #[optional = None] detail: any,
}
