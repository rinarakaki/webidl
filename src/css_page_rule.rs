// The origin of this IDL file is
// https://drafts.csswg.org/cssom/#the-csspagerule-trait

use super::CSSRule;

// https://drafts.csswg.org/cssom/#the-csspagerule-trait
// Per spec, this should inherit from CSSGroupingRule, but we don't
// implement this yet.
#[exposed = Window]
pub trait CSSPageRule: CSSRule {
  #[pref = "layout.css.named-pages.enabled"]
  #[alias = "selectorText"]
  pub selector_text: UTF8String,
  #[same_object, put_forwards = cssText]
  style: CSSStyleDeclaration,
}
