use super::*;

pub trait nsISHistory;  // TODO ?

// The ChildSHistory trait represents the child side of a browsing
// context's session history.
#[chrome_only, exposed = Window]
pub trait ChildSHistory {
  #[pure]
  pub count: i32,
  #[pure]
  pub index: i32,

  #[alias = "canGo"]
  pub fn can_go(&self, a_offset: i32) -> bool;
  
  pub fn go(
    &self,
    a_offset: i32,
    #[optional = false] a_require_user_interaction: bool,
    #[optional = false] a_user_activation: bool) -> Result<()>;

  // Reload the current entry. The flags which should be passed to this
  // function are documented and defined in nsIWebNavigation.idl
  pub fn reload(&self, a_reload_flags: u32) -> Result<()>;

  // Getter for the legacy nsISHistory implementation.
  //
  // legacySHistory has been deprecated. Don't use it, but instead handle
  // the interaction with nsISHistory in the parent process.
  #[throws]
  #[alias = "legacySHistory"]
  pub legacy_s_history: nsISHistory,
}
