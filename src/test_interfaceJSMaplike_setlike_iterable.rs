#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait TestInterfaceMaplike {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  maplike<DOMString, i32>;
  #[alias = "setInternal"]
  pub fn set_internal(aKey: DOMString, aKey: DOMString);
  #[alias = "clearInternal"]
  pub fn clear_internal();
  #[alias = "deleteInternal"]
  pub fn delete_internal(aKey: DOMString) -> bool;
  #[alias = "hasInternal"]
  pub fn has_internal(aKey: DOMString) -> bool;
  #[throws]
  #[alias = "getInternal"]
  pub fn get_internal(aKey: DOMString) -> i32;
}

#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait TestInterfaceMaplikeObject {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  readonly maplike<DOMString, TestInterfaceMaplike>;
  #[alias = "setInternal"]
  pub fn set_internal(aKey: DOMString);
  #[alias = "clearInternal"]
  pub fn clear_internal();
  #[alias = "deleteInternal"]
  pub fn delete_internal(aKey: DOMString) -> bool;
  #[alias = "hasInternal"]
  pub fn has_internal(aKey: DOMString) -> bool;
  #[throws]
  #[alias = "getInternal"]
  pub fn get_internal(aKey: DOMString) -> Option<TestInterfaceMaplike>;
}

#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait TestInterfaceMaplikeJSObject {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  readonly maplike<DOMString, object>;
  #[alias = "setInternal"]
  pub fn set_internal(aKey: DOMString, aKey: DOMString);
  #[alias = "clearInternal"]
  pub fn clear_internal();
  #[alias = "deleteInternal"]
  pub fn delete_internal(aKey: DOMString) -> bool;
  #[alias = "hasInternal"]
  pub fn has_internal(aKey: DOMString) -> bool;
  #[throws]
  #[alias = "getInternal"]
  pub fn get_internal(aKey: DOMString) -> Option<object>;
}

#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait TestInterfaceSetlike {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  setlike<DOMString>;
}

#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait TestInterfaceSetlikeNode {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  setlike<Node>;
}

#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait TestInterfaceIterableSingle {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  iterable<i32>;
  pub fn long(index: u32) -> getter;
  length: u32,
}

#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait TestInterfaceIterableDouble {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  iterable<DOMString, DOMString>;
}

#[pref = "dom.expose_test_interfaces",
 exposed = Window]
pub trait TestInterfaceIterableDoubleUnion {
  #[alias = "constructor"]
  pub fn new() -> Result<Self, Error>;

  iterable<DOMString, (DOMString or long)>;
}

