// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-progress-element

#[exposed = Window]
pub trait HTMLProgressElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub value: f64,
  #[ce_reactions, setter_throws]
  pub max: f64,
  position: f64,
  labels: NodeList,
}
