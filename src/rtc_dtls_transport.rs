// The origin of this IDL file is
// https://w3c.github.io/webrtc-pc/#rtcdtlstransport-trait

pub enum RTCDtlsTransportState {
  #[alias = "new"]
  New,
  #[alias = "connecting"]
  Connecting,
  #[alias = "connected"]
  Connected,
  #[alias = "closed"]
  Closed,
  #[alias = "failed"]
  Failed
}

#[pref = "media.peerconnection.enabled",
  exposed = Window]
#[alias = "RTCDtlsTransport"]
pub trait RTCDTLSTransport: EventTarget {
  state: RTCDtlsTransportState,
  #[alias = "onstatechange"]
  pub on_state_change: EventHandler,
}
