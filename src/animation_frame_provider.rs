// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/imagebitmap-and-animations.html#animation-frames

pub super::*;

type FrameRequestCallback = Fn(time: DOMHighResTimeStamp);

pub trait mixin AnimationFrameProvider {
  #[alias = "requestAnimationFrame"]
  pub fn request_animation_frame(&self, callback: FrameRequestCallback)
    -> Result<i32>;
  #[alias = "cancelAnimationFrame"]
  pub fn cancel_animation_frame(&self, handle: i32) -> Result<()>;
}
