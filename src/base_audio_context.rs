// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/#idl-def-BaseAudioContext

use super::*;

pub type DecodeSuccessCallback = Fn(decoded_data: AudioBuffer);
pub type DecodeErrorCallback = Fn(error: DOMException);

pub enum AudioContextState {
  #[alias = "suspended"]
  Suspended,
  #[alias = "running"]
  Running,
  #[alias = "closed"]
  Closed
}

#[exposed = Window]
pub trait BaseAudioContext: EventTarget {
  pub destination: AudioDestinationNode,
  #[alias = "sampleRate"]
  pub sample_rate: f32,
  #[alias = "currentTime"]
  pub current_time: f64,
  pub listener: AudioListener,
  pub state: AudioContextState,
  #[throws, same_object, SecureContext, pref = "dom.audioworklet.enabled"]
  #[alias = "audioWorklet"]
  pub audio_worklet: AudioWorklet,

  pub fn resume(&self) -> Result<Promise<void>>;

  #[alias = "onstatechange"]
  pub mut on_state_change: EventHandler,

  #[new_object]
  #[alias = "createBuffer"]
  pub fn create_buffer(
    &self,
    number_of_channels: u32,
    length: u32,
    sample_rate: f32) -> Result<AudioBuffer>;

  #[alias = "decodeAudioData"]
  pub fn decode_audio_data(
    &self,
    audio_data: ArrayBuffer,
    #[optional] success_callback: DecodeSuccessCallback,
    #[optional] error_callback: DecodeErrorCallback)
    -> Result<Promise<AudioBuffer>>;

  // AudioNode creation
  #[new_object]
  #[alias = "createBufferSource"]
  pub fn create_buffer_source(&self) -> AudioBufferSourceNode;

  #[new_object]
  #[alias = "createConstantSource"]
  pub fn create_constant_source(&self) -> ConstantSourceNode;

  #[new_object]
  #[alias = "createScriptProcessor"]
  pub fn create_script_processor(
    &self,
    #[optional = 0] buffer_size: u32,
    #[optional = 2] number_of_input_channels: u32,
    #[optional = 2] number_of_output_channels: u32)
    -> Result<ScriptProcessorNode>;

  #[new_object]
  #[alias = "createAnalyser"]
  pub fn create_analyser(&self) -> Result<AnalyserNode>;

  #[new_object]
  #[alias = "createGain"]
  pub fn create_gain(&self) -> Result<GainNode>;

  #[new_object]
  #[alias = "createDelay"]
  pub fn create_delay(&self, #[optional = 1] max_delay_time: f64)
    -> Result<DelayNode>;  // TODO: no = 1

  #[new_object]
  #[alias = "createBiquadFilter"]
  pub fn create_biquad_filter(&self) -> Result<BiquadFilterNode>;

  #[new_object]
  #[alias = "createIIRFilter"]
  pub fn create_iir_filter(&self, feedforward: Vec<f64>, feedback: Vec<f64>)
    -> Result<IIRFilterNode>;

  #[new_object]
  #[alias = "createWaveShaper"]
  pub fn create_wave_shaper(&self) -> Result<WaveShaperNode>;

  #[new_object]
  #[alias = "createPanner"]
  pub fn create_panner(&self) -> Result<PannerNode>;

  #[new_object]
  #[alias = "createStereoPanner"]
  pub fn create_stereo_panner(&self) -> Result<StereoPannerNode>;

  #[new_object]
  #[alias = "createConvolver"]
  pub fn create_convolver(&self) -> Result<ConvolverNode>;

  #[new_object]
  #[alias = "createChannelSplitter"]
  pub fn create_channel_splitter(&self, #[optional = 6] number_of_outputs: u32)
    -> Result<ChannelSplitterNode>;

  #[new_object]
  #[alias = "createChannelMerger"]
  pub fn create_channel_merger(&self, #[optional = 6] number_of_inputs: u32)
    -> Result<ChannelMergerNode>;

  #[new_object]
  #[alias = "createDynamicsCompressor"]
  pub fn create_dynamics_compressor(&self) -> Result<DynamicsCompressorNode>;

  #[new_object]
  #[alias = "createOscillator"]
  pub fn create_oscillator(&self) -> Result<OscillatorNode>;

  #[new_object]
  #[alias = "createPeriodicWave"]
  pub fn create_periodic_wave(
    &self,
    real: Vec<f32>,
    imag: Vec<f32>,
    #[optional = {}] constraints: PeriodicWaveConstraints)
    -> Result<PeriodicWave>;
}
