// The origin of this IDL file is
// https://svgwg.org/svg2-draft/

#[exposed = Window]
pub trait SVGUnitTypes {
  // Unit Types
  const u16 SVG_UNIT_TYPE_UNKNOWN = 0;
  const u16 SVG_UNIT_TYPE_USERSPACEONUSE = 1;
  const u16 SVG_UNIT_TYPE_OBJECTBOUNDINGBOX = 2;
}

