// These objects are exposed by the MozDOMAfterPaint event. Each one represents
// a request to repaint a rectangle that was generated by the browser.
#[exposed = Window]
pub trait PaintRequest {
  // The client rect where invalidation was triggered.
  #[alias = "clientRect"]
  client_rect: DOMRect,

  // The reason for the request, as a string. If an empty string, then we don't know
  // the reason (this is common). Reasons include "scroll repaint", meaning that we
  // needed to repaint the rectangle due to scrolling, and "scroll copy", meaning
  // that we updated the rectangle due to scrolling but instead of painting
  // manually, we were able to do a copy from another area of the screen.
  reason: DOMString,
}
