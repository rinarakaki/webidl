// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

use super::*;

#[pref = "dom.webaudio.enabled", exposed = Window]
pub trait AudioListener {
  // Uses a 3D cartesian coordinate system
  #[alias = "setPosition"]
  pub fn set_position(&self, x: f64, y: f64, z: f64);
  #[alias = "setOrientation"]
  pub fn set_orientation(
    &self, x: f64, y: f64, z: f64, x_up: f64, y_up: f64, z_up: f64);
}

