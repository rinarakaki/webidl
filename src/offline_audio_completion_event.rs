// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct OfflineAudioCompletionEventInit: EventInit {
  required AudioBuffer renderedBuffer;
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait OfflineAudioCompletionEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;

  #[alias = "renderedBuffer"]
  rendered_buffer: AudioBuffer,
}
