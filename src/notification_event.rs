// The origin of this IDL file is
// http://notifications.spec.whatwg.org/

#[exposed = ServiceWorker,func = "mozilla::dom::Notification::PrefEnabled"]
pub trait NotificationEvent: ExtendableEvent {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;

  #[binary_name = "notification_"]
  notification: Notification,
}

pub struct NotificationEventInit: ExtendableEventInit {
  required Notification notification;
}
