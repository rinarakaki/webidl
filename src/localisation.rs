// L10nIdArgs is an object used to carry localization tuple for message
// translation.
//
// Fields:
//    id - identifier of a message.
//  args - an optional of: record arguments used to format the message.
//         The argument will be converted to/from JSON, and the API
//         will only handle strings and numbers.
pub struct L10nIdArgs {
  pub id: Option<UTF8String> = None,
  pub args: Option<L10nArgs> = None,
}

// When no arguments are required to format a message a simple string can be
// used instead.
pub type L10nKey = (UTF8String or L10nIdArgs);

// L10nMessage is a compound translation unit from Fluent which
// encodes the value and (optionally) a list of attributes used
// to translate a given widget.
//
// Most simple imperative translations will only use the `value`,
// but when building a Message for a UI widget, a combination
// of a value and attributes will be used.
pub struct AttributeNameValue {
  required UTF8String name;
  required UTF8String value;
}

pub struct L10nMessage {
  pub value: Option<UTF8String> = None,
  Vec<AttributeNameValue>? attributes = None;
}

// Localisation is an implementation of the Fluent Localisation API.
//
// An instance of a Localisation class stores a state of a mix
// of localization resources and provides the API to resolve
// translation value for localization identifiers from the
// resources.
//
// Methods:
//    - addResourceIds - add resources
//    - removeResourceIds - remove resources
//    - formatValue - format a single value
//    - formatValues - format multiple values
//    - formatMessages - format multiple compound messages
//
#[func = "IsChromeOrUAWidget", exposed = Window]
#[alias = "Localization"]
pub trait Localisation {
  // Constructor arguments:
  //    - aResourceids - a list of localization resource URIs
  //                             which will provide messages for this
  //                             Localisation instance.
  //    - aSync - Specifies if the initial state of the Localisation API is synchronous.
  //                             This enables a number of synchronous methods on the
  //                             Localisation API.
  //    - aRegistry - optional L10nRegistry: custom to be used by this Localisation instance.
  //    - aLocales - custom set of locales to be used for this Localisation.
  #[throws]
  constructor(aResourceIds: Vec<L10nResourceId>,
  #[optional = false] aSync: bool,
  optional aRegistry: L10nRegistry,
  optional Vec<UTF8String> aLocales);

  // A method for adding resources to the localization context.
  #[alias = "addResourceIds"]
  pub fn add_resource_ids(aResourceIds: Vec<L10nResourceId>);

  // A method for removing resources from the localization context.
  //
  // Returns a new count of resources used by the context.
  u32 removeResourceIds(aResourceIds: Vec<L10nResourceId>);

  // Formats a value of a localization message with a given id.
  // An optional struct: pub of arguments can be passed to inform
  // the message formatting logic.
  //
  // Example:
  //    let value = await document.l10n.formatValue("unread-emails", {count: 5});
  //    assert.equal(value, "You have 5 unread emails");
  #[new_object]
  Promise<Option<UTF8String>> formatValue(aId: UTF8String, optional aArgs: L10nArgs);

  // Formats values of a list of messages with given ids.
  //
  // Example:
  //    let values = await document.l10n.formatValues([
  //      {id: "hello-world"},
  //      {id: "unread-emails", args: {count: 5}
  //    ]);
  //    assert.deepEqual(values, [
  //      "Hello World",
  //      "You have 5 unread emails"
  //    ]);
  #[new_object]
  Promise<Vec<Option<UTF8String>>> formatValues(aKeys: Vec<L10nKey>);

  // Formats values and attributes of a list of messages with given ids.
  //
  // Example:
  //    let values = await document.l10n.formatMessages([
  //      {id: "hello-world"},
  //      {id: "unread-emails", args: {count: 5}
  //    ]);
  //    assert.deepEqual(values, [
  //      {
  //        value: "Hello World",
  //        attributes: None
  //      },
  //      {
  //        value: "You have 5 unread emails",
  //        attributes: {
  //          tooltip: "Click to select them all"
  //        }
  //      }
  //    ]);
  #[new_object]
  Promise<Vec<Option<L10nMessage>>> formatMessages(aKeys: Vec<L10nKey>);

  #[alias = "setAsync"]
  pub fn set_async();

  #[new_object, throws]
  #[alias = "formatValueSync"]
  pub fn format_value_sync(aId: UTF8String, optional aArgs: L10nArgs) -> Option<UTF8String>;
  #[new_object, throws]
  Vec<Option<UTF8String>> formatValuesSync(aKeys: Vec<L10nKey>);
  #[new_object, throws]
  Vec<Option<L10nMessage>> formatMessagesSync(aKeys: Vec<L10nKey>);
}

// A helper dict for converting between JS Value and L10nArgs.
#[GenerateInitFromJSON, GenerateConversionToJS]
pub struct L10nArgsHelperDict {
  required L10nArgs args;
}
