// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#audiotrack

use super::*;

#[pref = "media.track.enabled", exposed = Window]
pub trait AudioTrack {
  pub id: DOMString,
  pub kind: DOMString,
  pub label: DOMString,
  pub language: DOMString,
  pub mut enabled: bool,
}
