// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#submitevent

#[exposed = Window]
pub trait SubmitEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: SubmitEventInit) -> Self;

  submitter: Option<HTMLElement>,
}

pub struct SubmitEventInit: EventInit {
  pub submitter: Option<HTMLElement> = None,
}
