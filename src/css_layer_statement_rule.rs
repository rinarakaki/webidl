// The origin of this IDL file is
// https://drafts.csswg.org/css-cascade-5/#the-csslayerstatementrule-trait

use super::CSSRule;

#[exposed = Window, pref = "layout.css.cascade-layers.enabled"]
pub trait CSSLayerStatementRule: CSSRule {
  // readonly attribute FrozenArray<CSSOMString> nameList;
  #[Frozen, Cached, pure]
  #[alias = "nameList"]
  name_list: Vec<UTF8String>,
}
