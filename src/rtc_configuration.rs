// The origin of this IDL file is
// http://dev.w3.org/2011/webrtc/editor/webrtc.html#idl-def-RTCConfiguration

pub enum RTCIceCredentialType {
  #[alias = "password"]
  Password,
}

pub struct RTCIceServer {
  (DOMString or Vec<DOMString>) urls;
  pub url: DOMString, //deprecated
  pub username: DOMString,
  pub credential: DOMString,
  #[alias = "credentialType"]
  pub credential_type: RTCIceCredentialType = "password",
}

pub enum RTCIceTransportPolicy {
  #[alias = "relay"]
  Relay,
  #[alias = "all"]
  All
}

pub enum RTCBundlePolicy {
  #[alias = "balanced"]
  Balanced,
  #[alias = "max-compat"]
  MaxCompat,
  #[alias = "max-bundle"]
  MaxBundle
}

pub struct RTCConfiguration {
  #[alias = "iceServers"]
  pub ice_servers: Vec<RTCIceServer>,
  #[alias = "iceTransportPolicy"]
  pub ice_transport_policy: RTCIceTransportPolicy = "all",
  #[alias = "bundlePolicy"]
  pub bundle_policy: RTCBundlePolicy = "balanced",
  #[alias = "peerIdentity"]
  pub peer_identity: Option<DOMString> = None,
  pub certificates: Vec<RTCCertificate>,

  // Non-standard. Only here to be able to detect and warn in web console.
  // Uses DOMString over enum as a trade-off between type errors and safety.
  // TODO: Remove once sdpSemantics usage drops to zero (1632243: bug).
  #[alias = "sdpSemantics"]
  pub sdp_semantics: DOMString,
}
