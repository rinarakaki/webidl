pub struct TouchEventInit: EventModifierInit {
  pub touches: Vec<Touch> = [],
  #[alias = "targetTouches"]
  pub target_touches: Vec<Touch> = [],
  #[alias = "changedTouches"]
  pub changed_touches: Vec<Touch> = [],
}

#[func = "mozilla::dom::TouchEvent::PrefEnabled",
 exposed = Window]
pub trait TouchEvent: UIEvent {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: TouchEventInit) -> Self;

  touches: TouchList,
  #[alias = "targetTouches"]
  target_touches: TouchList,
  #[alias = "changedTouches"]
  changed_touches: TouchList,

  #[alias = "altKey"]
  alt_key: bool,
  #[alias = "metaKey"]
  meta_key: bool,
  #[alias = "ctrlKey"]
  ctrl_key: bool,
  #[alias = "shiftKey"]
  shift_key: bool,

  void initTouchEvent(type: DOMString,
  #[optional = false] canBubble: bool,
  #[optional = false] cancelable: bool,
  optional Option<Window> view = None,
  #[optional = 0] detail: i32,
  #[optional = false] ctrlKey: bool,
  #[optional = false] altKey: bool,
  #[optional = false] shiftKey: bool,
  #[optional = false] metaKey: bool,
  optional Option<TouchList> touches = None,
  optional Option<TouchList> targetTouches = None,
  optional Option<TouchList> changedTouches = None);
}
