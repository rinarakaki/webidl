// The origin of this IDL file is
// http://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html

use super::*;

#[exposed = ServiceWorker]
pub trait Clients {
  // The objects returned will be new instances every time
  #[new_object]
  pub fn get(&self, id: DOMString) -> Promise<any>;

  #[new_object]
  #[alias = "matchAll"]
  pub fn match_all(&self, #[optional = {}] options: ClientQueryOptions)
    -> Promise<Vec<Client>>;

  #[new_object]
  #[alias = "openWindow"]
  pub fn open_window(&self, url: USVString) -> Promise<Option<WindowClient>>;

  #[new_object]
  pub fn claim(&self) -> Promise<void>;
}

pub struct ClientQueryOptions {
  #[alias = "includeUncontrolled"]
  pub #[optional = false] include_uncontrolled: bool
  pub #[optional = Window] type: ClientType
}

pub enum ClientType {
  #[alias = "window"]
  Window,
  #[alias = "worker"]
  Worker,
  #[alias = "sharedworker"]
  SharedWorker,
  // https://github.com/w3c/ServiceWorker/issues/1036
  #[alias = "serviceworker"]
  ServiceWorker,
  #[alias = "all"]
  All
}

