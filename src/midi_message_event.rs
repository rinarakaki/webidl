// The origin of this IDL file is
// https://webaudio.github.io/web-midi-api/

#[SecureContext,
 pref = "dom.webmidi.enabled",
 exposed = Window]
pub trait MIDIMessageEvent: Event
{
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: MIDIMessageEventInit) -> Result<Self, Error>;
  
  #[throws]
  data: Uint8Array,
}

pub struct MIDIMessageEventInit: EventInit
{
  pub data: Uint8Array,
}
