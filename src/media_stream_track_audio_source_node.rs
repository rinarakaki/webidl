// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct MediaStreamTrackAudioSourceOptions {
  required MediaStreamTrack mediaStreamTrack;
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait MediaStreamTrackAudioSourceNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(context: AudioContext, context: AudioContext) -> Result<Self, Error>;
}

// Mozilla extensions
MediaStreamTrackAudioSourceNode includes AudioNodePassThrough;
