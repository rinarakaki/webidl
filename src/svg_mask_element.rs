// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGMaskElement: SVGElement {

  // Mask Types
  const u16 SVG_MASKTYPE_LUMINANCE = 0;
  const u16 SVG_MASKTYPE_ALPHA = 1;

  #[constant]
  #[alias = "maskUnits"]
  mask_units: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "maskContentUnits"]
  mask_content_units: SVGAnimatedEnumeration,
  #[constant]
  x: SVGAnimatedLength,
  #[constant]
  y: SVGAnimatedLength,
  #[constant]
  width: SVGAnimatedLength,
  #[constant]
  height: SVGAnimatedLength,
}

