// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGFECompositeElement: SVGElement {

  // Composite Operators
  const u16 SVG_FECOMPOSITE_OPERATOR_UNKNOWN = 0;
  const u16 SVG_FECOMPOSITE_OPERATOR_OVER = 1;
  const u16 SVG_FECOMPOSITE_OPERATOR_IN = 2;
  const u16 SVG_FECOMPOSITE_OPERATOR_OUT = 3;
  const u16 SVG_FECOMPOSITE_OPERATOR_ATOP = 4;
  const u16 SVG_FECOMPOSITE_OPERATOR_XOR = 5;
  const u16 SVG_FECOMPOSITE_OPERATOR_ARITHMETIC = 6;
  const u16 SVG_FECOMPOSITE_OPERATOR_LIGHTER = 7;

  #[constant]
  in1: SVGAnimatedString,
  #[constant]
  in2: SVGAnimatedString,
  #[constant]
  operator: SVGAnimatedEnumeration,
  #[constant]
  k1: SVGAnimatedNumber,
  #[constant]
  k2: SVGAnimatedNumber,
  #[constant]
  k3: SVGAnimatedNumber,
  #[constant]
  k4: SVGAnimatedNumber,
}

SVGFECompositeElement includes SVGFilterPrimitiveStandardAttributes;
