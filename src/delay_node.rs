// The origin of this IDL file is
// https://webaudio.github.io/web-audio-api/

pub struct DelayOptions: AudioNodeOptions {
  #[alias = "maxDelayTime"]
  pub max_delay_time: f64 = 1,
  #[alias = "delayTime"]
  pub delay_time: f64 = 0,
}

#[pref = "dom.webaudio.enabled",
 exposed = Window]
pub trait DelayNode: AudioNode {
  #[alias = "constructor"]
  pub fn new(context: BaseAudioContext, #[optional = {}] options: DelayOptions)
    -> Result<Self>;

  #[alias = "delayTime"]
  delay_time: AudioParam,
}

// Mozilla extension
DelayNode includes AudioNodePassThrough;

