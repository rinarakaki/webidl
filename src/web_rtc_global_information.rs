pub struct WebrtcGlobalStatisticsReport {
  pub reports: Vec<RTCStatsReportInternal> = [],
}

callback WebrtcGlobalStatisticsCallback = void (reports: WebrtcGlobalStatisticsReport);
callback WebrtcGlobalLoggingCallback = void (logMessages: Vec<DOMString>);

#[chrome_only, exposed = Window]
namespace WebrtcGlobalInformation {

  #[throws]
  void getAllStats(callback: WebrtcGlobalStatisticsCallback,
  optional pcIdFilter: DOMString);

  #[alias = "clearAllStats"]
  pub fn clear_all_stats();

  #[throws]
  #[alias = "getLogging"]
  pub fn get_logging(pattern: DOMString, pattern: DOMString);

  #[alias = "clearLogging"]
  pub fn clear_logging();

  // NSPR WebRTC Trace debug level (0 - 65535)
  //
  // Notes:
  // - Setting a non-zero debug level turns on gathering of log for file output.
  // - Subsequently setting a zero debug level writes that log to disk.

  #[alias = "debugLevel"]
  pub debug_level: i32,

  // WebRTC AEC debugging enable
  #[alias = "aecDebug"]
  pub aec_debug: bool,

  #[alias = "aecDebugLogDir"]
  aec_debug_log_dir: DOMString,
}
