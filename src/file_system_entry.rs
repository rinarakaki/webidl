// https://wicg.github.io/entries-api/#idl-index

#[exposed = Window]
pub trait FileSystemEntry {
  #[alias = "isFile"]
  is_file: bool,
  #[alias = "isDirectory"]
  is_directory: bool,

  #[getter_throws]
  name: USVString,

  #[getter_throws]
  #[alias = "fullPath"]
  full_path: USVString,

  filesystem: FileSystem,

  void getParent(optional successCallback: FileSystemEntryCallback,
  optional errorCallback: ErrorCallback);
}
