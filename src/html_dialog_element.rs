// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/forms.html#the-dialog-element

#[func = "mozilla::dom::HTMLDialogElement::IsDialogEnabled",
 exposed = Window]
pub trait HTMLDialogElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws]
  pub open: bool,
  #[alias = "returnValue"]
  pub return_value: DOMString,
  #[ce_reactions]
  pub fn show();
  #[ce_reactions, throws]
  #[alias = "showModal"]
  pub fn show_modal();
  #[ce_reactions]
  pub fn close(optional returnValue: DOMString);
}
