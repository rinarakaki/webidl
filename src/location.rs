// The origin of this IDL file is
// https://html.spec.whatwg.org/multipage/history.html#the-location-trait

#[LegacyUnforgeable,
 exposed = Window]
pub trait Location {
  #[throws, CrossOriginWritable, needs_subject_principal]
  stringifier attribute USVString href;
  #[throws, needs_subject_principal]
  origin: USVString,
  #[throws, needs_subject_principal]
  pub protocol: USVString,
  #[throws, needs_subject_principal]
  pub host: USVString,
  #[throws, needs_subject_principal]
  pub hostname: USVString,
  #[throws, needs_subject_principal]
  pub port: USVString,
  #[throws, needs_subject_principal]
  pub pathname: USVString,
  #[throws, needs_subject_principal]
  pub search: USVString,
  #[throws, needs_subject_principal]
  pub hash: USVString,

  #[throws, needs_subject_principal]
  pub fn assign(url: USVString);

  #[throws, CrossOriginCallable, needs_subject_principal]
  pub fn replace(url: USVString);

  // XXXbz there is no forceget argument in the spec! See bug 1037721.
  #[throws, needs_subject_principal]
  pub fn reload(#[optional = false] forceget: bool);

  // Bug 1085214 #[same_object] readonly attribute USVString[] ancestor_origins;
}
