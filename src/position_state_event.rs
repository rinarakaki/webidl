pub struct PositionStateEventInit: EventInit {
  required f64 duration;
  required f64 playbackRate;
  required f64 position;
}

#[exposed = Window, chrome_only]
pub trait PositionStateEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] eventInitDict: PositionStateEventInit) -> Self;
  duration: f64,
  #[alias = "playbackRate"]
  playback_rate: f64,
  position: f64,
}
