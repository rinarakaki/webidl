#[exposed = Window]
pub trait DOMRectList {
  length: u32,
  getter Option<DOMRect> item(index: u32);
}
