#[LegacyNoInterfaceObject, exposed = Window]
pub trait External {
  #[deprecated = "External_AddSearchProvider"]
  pub fn AddSearchProvider();
  pub fn IsSearchProviderInstalled();
}
