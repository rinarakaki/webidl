// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/multipage/workers.html#the-workerglobalscope-common-trait

#[Global = (Worker, SharedWorker),
 exposed = SharedWorker]
pub trait SharedWorkerGlobalScope: WorkerGlobalScope {
  #[replaceable]
  name: DOMString,

  pub fn close();

  #[alias = "onconnect"]
  pub onconnect: EventHandler,
}
