// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

pub trait mixin SVGZoomAndPan {
  // Zoom and Pan Types
  const u16 SVG_ZOOMANDPAN_UNKNOWN = 0;
  const u16 SVG_ZOOMANDPAN_DISABLE = 1;
  const u16 SVG_ZOOMANDPAN_MAGNIFY = 2;

  #[setter_throws]
  #[alias = "zoomAndPan"]
  pub zoom_and_pan: u16,
}
