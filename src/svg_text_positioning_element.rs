// The origin of this IDL file is
// http://www.w3.org/TR/SVG2/

#[exposed = Window]
pub trait SVGTextPositioningElement: SVGTextContentElement {
  #[constant]
  x: SVGAnimatedLengthList,
  #[constant]
  y: SVGAnimatedLengthList,
  #[constant]
  dx: SVGAnimatedLengthList,
  #[constant]
  dy: SVGAnimatedLengthList,
  #[constant]
  rotate: SVGAnimatedNumberList,
}

