// The origin of this WebIDL file is
// https://www.w3.org/TR/payment-request/#paymentaddress-trait

#[SecureContext,
 func = "mozilla::dom::PaymentRequest::PrefEnabled",
 exposed = Window]
pub trait PaymentAddress {
  #[Default]
  #[alias = "toJSON"]
  pub fn to_json() -> object;

  country: DOMString,
  // TODO: Use FrozenArray once available. (1236777: Bug)
  // readonly attribute FrozenArray<DOMString> addressLine;
  #[Frozen, Cached, pure]
  #[alias = "addressLine"]
  address_line: Vec<DOMString>,
  region: DOMString,
  #[alias = "regionCode"]
  region_code: DOMString,
  city: DOMString,
  #[alias = "dependentLocality"]
  dependent_locality: DOMString,
  #[alias = "postalCode"]
  postal_code: DOMString,
  #[alias = "sortingCode"]
  sorting_code: DOMString,
  organization: DOMString,
  recipient: DOMString,
  phone: DOMString,
}
