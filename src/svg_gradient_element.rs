// The origin of this IDL file is
// https://svgwg.org/svg2-draft/

#[exposed = Window]
pub trait SVGGradientElement: SVGElement {

  // Spread Method Types
  const u16 SVG_SPREADMETHOD_UNKNOWN = 0;
  const u16 SVG_SPREADMETHOD_PAD = 1;
  const u16 SVG_SPREADMETHOD_REFLECT = 2;
  const u16 SVG_SPREADMETHOD_REPEAT = 3;

  #[constant]
  #[alias = "gradientUnits"]
  gradient_units: SVGAnimatedEnumeration,
  #[constant]
  #[alias = "gradientTransform"]
  gradient_transform: SVGAnimatedTransformList,
  #[constant]
  #[alias = "spreadMethod"]
  spread_method: SVGAnimatedEnumeration,
}

SVGGradientElement includes SVGURIReference;
