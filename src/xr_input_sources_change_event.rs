// The origin of this IDL file is
// https://immersive-web.github.io/webxr/#xrinputsourceschangeevent-trait

use super::{Event, XRInputSource, XRSession};

#[pref = "dom.vr.webxr.enabled", SecureContext, exposed = Window]
pub trait XRInputSourcesChangeEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, type: DOMString) -> Self;
  #[same_object]
  session: XRSession,
  // TODO: Use FrozenArray once available. (1236777: Bug)
  #[constant, Cached, Frozen]
  added: Vec<XRInputSource>,
  // TODO: Use FrozenArray once available. (1236777: Bug)
  #[constant, Cached, Frozen]
  removed: Vec<XRInputSource>,
}

pub struct XRInputSourcesChangeEventInit: EventInit {
  required XRSession session;
  // TODO: Use FrozenArray once available. (1236777: Bug)
  required Vec<XRInputSource> added;
  // TODO: Use FrozenArray once available. (1236777: Bug)
  required Vec<XRInputSource> removed;
}
