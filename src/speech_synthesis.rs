// The origin of this IDL file is
// http://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html

#[pref = "media.webspeech.synth.enabled",
 exposed = Window]
pub trait SpeechSynthesis: EventTarget{
  pending: bool,
  speaking: bool,
  paused: bool,

  pub fn speak(utterance: SpeechSynthesisUtterance);
  pub fn cancel();
  pub fn pause();
  pub fn resume();
  #[alias = "getVoices"]
  pub fn get_voices() -> Vec<SpeechSynthesisVoice>;

  #[alias = "onvoiceschanged"]
  pub onvoiceschanged: EventHandler,

  #[chrome_only]
  // Force an utterance to end. Circumvents bad speech service implementations.
  #[alias = "forceEnd"]
  pub fn force_end();
}
