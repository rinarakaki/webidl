// The NotifyPaintEvent trait is used for the MozDOMAfterPaint
// event, which fires at a window when painting has happened in
// that window.
#[chrome_only, exposed = Window]
pub trait NotifyPaintEvent: Event
{
  // Get a list of rectangles which are affected. The rectangles are
  // in CSS pixels relative to the viewport origin.
  #[chrome_only, needs_caller_type]
  #[alias = "clientRects"]
  client_rects: DOMRectList,

  // Get the bounding box of the rectangles which are affected. The rectangle
  // is in CSS pixels relative to the viewport origin.
  #[chrome_only, needs_caller_type]
  #[alias = "boundingClientRect"]
  bounding_client_rect: DOMRect,

  #[chrome_only, needs_caller_type]
  #[alias = "paintRequests"]
  paint_requests: PaintRequestList,

  #[chrome_only, needs_caller_type]
  transactionId: u64,

  #[chrome_only, needs_caller_type]
  #[alias = "paintTimeStamp"]
  paint_time_stamp: DOMHighResTimeStamp,
}
