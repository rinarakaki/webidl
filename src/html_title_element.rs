// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-title-element

#[exposed = Window]
pub trait HTMLTitleElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, throws]
  pub text: DOMString,
}
