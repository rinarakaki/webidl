// The origin of this IDL file is
// http://w3c.github.io/mediacapture-main/getusermedia.html

pub struct MediaTrackSettings {
  pub width: i32,
  pub height: i32,
  #[alias = "frameRate"]
  pub frame_rate: f64,
  #[alias = "facingMode"]
  pub facing_mode: DOMString,
  #[alias = "echoCancellation"]
  pub echo_cancellation: bool,
  #[alias = "autoGainControl"]
  pub auto_gain_control: bool,
  #[alias = "noiseSuppression"]
  pub noise_suppression: bool,
  #[alias = "channelCount"]
  pub channel_count: i32,
  pub deviceId: DOMString,
  pub groupId: DOMString,

  // Mozilla-specific extensions:

  // http://fluffy.github.io/w3c-screen-share/#screen-based-video-constraints
  // OBE by http://w3c.github.io/mediacapture-screen-share

  #[alias = "mediaSource"]
  pub media_source: DOMString,

  // Experimental https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1131568#c3
  //              https://bugzilla.mozilla.org/show_bug.Option<cgi>id = 1193075

  #[alias = "browserWindow"]
  pub browser_window: i64,
  #[alias = "scrollWithPage"]
  pub scroll_with_page: bool,
  #[alias = "viewportOffsetX"]
  pub viewport_offsetX: i32,
  #[alias = "viewportOffsetY"]
  pub viewport_offsetY: i32,
  #[alias = "viewportWidth"]
  pub viewport_width: i32,
  #[alias = "viewportHeight"]
  pub viewport_height: i32,
}
