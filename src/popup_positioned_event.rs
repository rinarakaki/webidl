pub struct PopupPositionedEventInit: EventInit {
  // Returns the alignment position where the popup has appeared relative to its
  // anchor node or point, accounting for any flipping that occurred.
  #[alias = "alignmentPosition"]
  pub alignment_position: DOMString = "",
  #[alias = "alignmentOffset"]
  pub alignment_offset: i32 = 0,
}

#[chrome_only, exposed = Window]
pub trait PopupPositionedEvent: Event {
  #[alias = "constructor"]
  pub fn new(type: DOMString, #[optional = {}] init: PopupPositionedEventInit) -> Self;

  #[alias = "alignmentPosition"]
  alignment_position: DOMString,
  #[alias = "alignmentOffset"]
  alignment_offset: i32,
}
