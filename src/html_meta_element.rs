// The origin of this IDL file is
// http://www.whatwg.org/specs/web-apps/current-work/#the-meta-element
// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis

// http://www.whatwg.org/specs/web-apps/current-work/#the-meta-element
#[exposed = Window]
pub trait HTMLMetaElement: HTMLElement {
  #[HTMLConstructor]
  #[alias = "constructor"]
  pub fn new() -> Self;

  #[ce_reactions, setter_throws, pure]
  pub name: DOMString,
  #[ce_reactions, setter_throws, pure]
  #[alias = "httpEquiv"]
  pub http_equiv: DOMString,
  #[ce_reactions, setter_throws, pure]
  pub content: DOMString,
}

// http://www.whatwg.org/specs/web-apps/current-work/#other-elements,-attributes-and-apis
partial trait HTMLMetaElement {
  #[ce_reactions, setter_throws, pure]
  pub scheme: DOMString,
}
