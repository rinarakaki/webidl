pub super::*;

// We need a JSImplementation but cannot get one without a contract ID.
// Since Addon and AddonInstall are only ever created from JS they don't need
// real contract IDs.
#[chrome_only, JSImplementation = "dummy"]
#[exposed = Window]
pub trait Addon {
    // The add-on's ID.
    pub id: DOMString,
    // The add-on's version.
    pub version: DOMString,
    // The add-on's type (extension, theme, etc.).
    pub r#type: DOMString,
    // The add-on's name in the current locale.
    pub name: DOMString,
    // The add-on's description in the current locale.
    pub description: DOMString,
    // If the user has enabled this add-on, note that it still may not be running
    // depending on whether enabling requires a restart or if the add-on is
    // incompatible in some way.
    #[alias = "isEnabled"]
    pub is_enabled: bool,
    // If the add-on is currently active in the browser.
    #[alias = "isActive"]
    pub is_active: bool,
    // If the add-on may be uninstalled
    #[alias = "canUninstall"]
    pub can_uninstall: bool,

    pub fn uninstall(&self) -> Promise<bool>;
    #[alias = "setEnabled"]
    pub fn set_enabled(&self, value: bool) -> Promise<void>;
}

#[chrome_only, JSImplementation = "dummy",
exposed = Window]
pub trait AddonInstall: EventTarget {
    // One of the STATE_* symbols from AddonManager.jsm
    pub state: DOMString,
    // One of the ERROR_* symbols from AddonManager.jsm, or None
    pub error: Option<DOMString>,
    // How many bytes have been downloaded
    pub progress: i64,
    // How many total bytes will need to be downloaded or -1 if unknown
    #[alias = "maxProgress"]
    pub max_progress: i64,

    pub fn install(&self) -> Promise<void>;
    pub fn cancel(&self) -> Promise<void>;
}

pub struct addonInstallOptions {
    required DOMString url;
    // If a non-empty string is passed for "hash", it is used to verify the
    // checksum of the downloaded XPI before installing. If is omitted or if
    // it is None or empty string, no checksum verification is performed.
    pub hash: Option<DOMString> = None,
}

#[HeaderFile = "mozilla/AddonManagerWebAPI.h",
  func = "mozilla::AddonManagerWebAPI::IsAPIEnabled",
  JSImplementation = "@mozilla.org/addon-web-api/manager;1",
  WantsEventListenerHooks,
  exposed = Window]
pub trait AddonManager: EventTarget {
    // Gets information about an add-on
    //
    // @param id
    //         The ID of the add-on to test for.
    // @return A promise. It will resolve to an Addon if the add-on is installed.
    #[alias = "getAddonByID"]
    pub fn get_addon_by_id(&self, id: DOMString) -> Promise<Addon>;

    // Creates an AddonInstall object for a given URL.
    //
    // @param options
    //        Only one supported option: 'url', the URL of the addon to install.
    // @return A promise that resolves to an instance of AddonInstall.
    #[alias = "createInstall"]
    pub fn create_install(
      &self,
      #[optional] options: addonInstallOptions = {}) -> Promise<AddonInstall>;

    // Opens an Abuse Report dialog window for the addon with the given id.
    // The addon may be currently installed (in which case the report will
    // include the details available locally), or not (in which case the report
    // will include the details that can be retrieved from the AMO API endpoint).
    //
    // @param id
    //         The ID of the add-on to report.
    // @return A promise that resolves to a bool (true when the report
    //         has been submitted successfully, false if the user cancelled
    //         the report). The Promise is rejected is the report fails
    //         for a reason other than user cancellation.
    #[alias = "reportAbuse"]
    pub fn report_abuse(&self, id: DOMString) -> Promise<bool>;

    // Indicator to content whether handing off the reports to the integrated
    // abuse report panel is enabled.
    #[alias = "abuseReportPanelEnabled"]
    pub abuse_report_panel_enabled: bool,
}

#[chrome_only, exposed = Window, HeaderFile = "mozilla/AddonManagerWebAPI.h"]
namespace AddonManagerPermissions {
    #[alias = "isHostPermitted"]
    pub fn is_host_permitted(host: DOMString) -> bool;
}

