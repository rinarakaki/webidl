pub struct FileSystemReadWriteOptions {
  #[EnforceRange]
  pub at: u64,
}

#[exposed = (DedicatedWorker), SecureContext, pref = "dom.fs.enabled"]
pub trait FileSystemSyncAccessHandle {
  // TODO: Use `#[AllowShared] BufferSource data` once it works (1696216: bug)
  pub fn read(
    buffer: ([AllowShared] ArrayBufferView or [AllowShared] ArrayBuffer),
    #[optional = {}] options: FileSystemReadWriteOptions) -> u64;
  pub fn write(
    buffer: ([AllowShared] ArrayBufferView or [AllowShared] ArrayBuffer),
    #[optional = {}] options: FileSystemReadWriteOptions) -> u64;

  pub fn truncate(size: [EnforceRange] u64) -> Promise<void>;
  #[alias = "getSize"]
  pub fn get_size() -> Promise<u64>;
  pub fn flush() -> Promise<void>;
  pub fn close() -> Promise<void>;
}
