// The origin of this IDL file is
// https://drafts.csswg.org/css-fonts/#om-fontfeaturevalues

// but we don't implement anything remotely resembling the spec.
#[exposed = Window]
pub trait CSSFontFeatureValuesRule: CSSRule {
  #[setter_throws]
  #[alias = "fontFamily"]
  pub font_family: UTF8String,

  // Not yet implemented
  //  readonly attribute CSSFontFeatureValuesMap annotation;
  //  readonly attribute CSSFontFeatureValuesMap ornaments;
  //  readonly attribute CSSFontFeatureValuesMap stylistic;
  //  readonly attribute CSSFontFeatureValuesMap swash;
  //  readonly attribute CSSFontFeatureValuesMap characterVariant;
  //  readonly attribute CSSFontFeatureValuesMap styleset;
}

partial trait CSSFontFeatureValuesRule {
  // Gecko Option<addition>
  #[setter_throws]
  #[alias = "valueText"]
  pub value_text: UTF8String,
}
