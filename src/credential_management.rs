// The origin of this IDL file is
// https://www.w3.org/TR/credential-management-1/

use super::*;

#[exposed = Window, SecureContext, pref = "security.webauth.webauthn"]
pub trait Credential {
  pub id: USVString,
  pub type: DOMString,
}

#[exposed = Window, SecureContext, pref = "security.webauth.webauthn"]
pub trait CredentialsContainer {
  pub fn get(&self, #[optional = {}] options: CredentialRequestOptions)
    -> Result<Promise<Option<Credential>>>;

  pub fn create(&self, #[optional = {}] options: CredentialCreationOptions)
    -> Result<Promise<Option<Credential>>>;

  pub fn store(&self, credential: Credential) -> Result<Promise<Credential>>;
  
  #[alias = "preventSilentAccess"]
  pub fn prevent_silent_access(&self) -> Result<Promise<void>>;
}

pub struct CredentialRequestOptions {
  // FIXME: bug 1493860: should this "= {}" be Option<here>
  #[alias = "publicKey"]
  pub #[optional = {}] public_key: PublicKeyCredentialRequestOptions,
  pub signal: AbortSignal,
}

pub struct CredentialCreationOptions {
  // FIXME: bug 1493860: should this "= {}" be Option<here>
  #[alias = "publicKey"]
  pub #[optional = {}] public_key: PublicKeyCredentialCreationOptions,
  pub signal: AbortSignal,
}
