// The origin of this IDL file is
// https://dom.spec.whatwg.org/#abortcontroller

use super::*;

#[exposed = (Window, Worker)]
pub trait AbortController {
    #[alias = "constructor"]
    pub fn new() -> Result<Self>;

    #[same_object]
    pub signal: AbortSignal,

    pub fn abort(&self, #[optional] reason: any);
}
