#[pref = "dom.gamepad.non_standard_events.enabled",
 exposed = Window,
 SecureContext]
pub trait GamepadButtonEvent: GamepadEvent {
  constructor(type: DOMString,
  #[optional = {}] eventInitDict: GamepadButtonEventInit);

  button: u32,
}

pub struct GamepadButtonEventInit: GamepadEventInit {
  pub button: u32 = 0,
}
