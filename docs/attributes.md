# Contents

- [AllowShared](#AllowShared)
- [CEReactions](#CEReactions)
- [Clamp](#Clamp)
- [Default](#Default)
- [EnforceRange](#EnforceRange)
- [Exposed](#Exposed)
- [Global](#Global)
- [NewObject](#NewObject)
- [PutForwards](#PutForwards)
- [Replaceable](#Replaceable)
- [SameObject](#SameObject)
- [SecureContext](#SecureContext)
- [Unscopable](#Unscopable)
- [LegacyNoInterfaceObject](#LegacyNoInterfaceObject)
- [LegacyNullToEmptyString](#LegacyNullToEmptyString)

# AllowShared

https://webidl.spec.whatwg.org/#AllowShared

```
```

# CEReactions

https://html.spec.whatwg.org/multipage/custom-elements.html#concept-custom-element-reaction

```
CEReactions
```

# Clamp

https://webidl.spec.whatwg.org/#Clamp

```
Clamp
```

# Default

https://webidl.spec.whatwg.org/#Default

```
```

# EnforceRange

https://webidl.spec.whatwg.org/#EnforceRange

```
```

# Exposed

https://webidl.spec.whatwg.org/#Exposed

```
Exposed
```

# Global

https://webidl.spec.whatwg.org/#Global

```
```

# NewObject

https://webidl.spec.whatwg.org/#NewObject

```
```

# PutForwards

https://webidl.spec.whatwg.org/#PutForwards

```
```

# Replaceable

https://webidl.spec.whatwg.org/#Replaceable

```
```

# SameObject

https://webidl.spec.whatwg.org/#SameObject

```
SameObject
```

# SecureContext

https://webidl.spec.whatwg.org/#SecureContext

```
```

# Unscopable

https://webidl.spec.whatwg.org/#Unscopable

# LegacyNoInterfaceObject

https://webidl.spec.whatwg.org/#LegacyNoInterfaceObject

```
```

# LegacyNullToEmptyString

https://webidl.spec.whatwg.org/#LegacyNullToEmptyString

```
LegacyNullToEmptyString
```

# TODO

- ClassString
- GenerateInit
- GenerateToJSON
- GenerateConversionToJS
- ProtoObjectHack
- RunConstructorInCallerCompartment
- UseCounter
