# primitives

## Contents

* [short](primitives.md#short)
* [long](primitives.md#long)
* [long long](primitives.md#long-long)
* [unsigned short](primitives.md#unsigned-short)
* [unsigned long](primitives.md#unsigned-long)
* [unsigned long long](primitives.md#unsigned-long-long)
* [float](primitives.md#float)
* [double](primitives.md#doible)
* [unrestricted float](primitives.md#unrestricted-float)
* [unrestricted double](primitives.md#unrestricted-double)
* [boolean](primitives.md#boolean)
* [Int8Array](primitives.md#Int8Array)
* [Float32Array](primitives.md#Float32Array)
* [octet](primitives.md#octet)
* [DOMString](primitives.md#DOMString)
* [USVString](primitives.md#USVString)
* [ArrayBufferView](primitives.md#ArrayBufferView)
* [void](primitives.md#void)
* [object](primitives.md#object)
* [any](primitives.md#any)
* [sequence](primitives.md#sequence)
* [maplike](primitives.md#maplike)
* [setlike](primitives.md#setlike)
* [record](primitives.md#record)

## short

https://webidl.spec.whatwg.org/#idl-short

```
short
```

```
i16
```

## long

```
long
```

```
i32
```

## long long

```
long long
```

```
i64
```

## unsigned short

```
unsigned short

u16
```

## unsigned long

```
unsigned long

u32
```

## unsigned long long

```
unsigned long long

u64
```

## unrestricted double

```
unrestricted double

```

## boolean

```
boolean

bool
```

## Int8Array

```
Int8Array

$mut [i32]
```

## Float32Array

```
Float32Array

$mut [f32]
```

## octet

https://webidl.spec.whatwg.org/#idl-octet

```
```

## DOMString

https://webidl.spec.whatwg.org/#idl-DOMString

## USVString

https://webidl.spec.whatwg.org/#idl-USVString

## ArrayBufferView

https://webidl.spec.whatwg.org/#ArrayBufferView

## void

## object

https://webidl.spec.whatwg.org/#idl-object

## any

https://webidl.spec.whatwg.org/#idl-any

## sequence

https://webidl.spec.whatwg.org/#idl-sequence

## record

https://webidl.spec.whatwg.org/#idl-record

## maplike

https://webidl.spec.whatwg.org/#idl-maplike

## setlike

https://webidl.spec.whatwg.org/#idl-setlike
