# Contents

- [[]](#)
- [interface](#interface)
- [partial interface](#partial-interface)
- [mixin](#mixin)
- [includes](#includes)
- [readonly attribute](#readonly-attribute)
- [attribute](#attribute)
- [const](#const)
- [constructor](#constructor)
- [static operation](#static-operation)
- [operation](#operation)
- [getter](#getter)
- [optional](#optional)
- [enum](#enum)
- [dictionary](#dictionary)
- [required](#required)
- [typedef](#typedef)
- [callback](#callback)
- [callback constructor](#callback-constructor)
- [namespace](#namespace)
- [variadic (...)](#variadic-)

# []

```
(?<!#)\[(.+)\]

#[$1]
```

# interface

```
interface

trait
```

```
^  type name\(args\);

  pub fn name(args) -> type;
```

# partial interface

# readonly attribute

```
^ *attribute (\w+) (\w+);

  pub $2: $1,
```

```
^ *readonly attribute (\w+) (\w+);

  $2: $1,
```

# attribute

# constructor

```
^  #\[throws\]
^  constructor\((.*)\);

  #[alias = "constructor"]
  pub fn new($1) -> Result<Self, Error>;
```

```
^  constructor\((.*)\);

  #[alias = "constructor"]
  pub fn new($1) -> Self;
```

# static operation

```
static fn
```

# operation

```
(?<=fn \w+)\(\)

(&self)
```

```
(?<=fn \w+)\((?!&self)

(&self, 
```

# getter

```
getter
```

# optional

```
optional (\w+(<\w+>)?) (\w+)

optional $3: $1
```

```
(\w+)\?

Option<$1>
```

# typedef

```
typedef (.+) (\w+);

pub type $2 = $1;
```

# enum

```
^enum (\w+) \{ (("\W+")(, "\W+")?) \};

enum $1 {
  $2
} 
```

TODO Which one is better?

```
^  "(\w+?)"(,)?$

  #[alias = "$1"]
  $1$2
```

```
^  "([a-z])(\w+?)"(,)?$

  #[alias = "$1$2"]
  \U$1$2$3
```

```
^  "([a-z])(\w+?)-([a-z])(\w+)"(,)?$

  #[alias = "$1$2-$3$4"]
  \U$1$2\U$3$4$5
```

```
^  "([a-z])(\w+?)-([a-z])(\w+)-([a-z])(\w+)"(,)?$

  #[alias = "$1$2-$3$4-$5$6"]
  \U$1$2\U$3$4\U$5$6$7
```

# dictionary

```
dictionary

struct
```

```
^  (\w+?(<\w+?>)?) (\w+)( = .+)?;

  pub $3: $1$4,
```

# required

https://webidl.spec.whatwg.org/#required-dictionary-member

# const

```
^  const (\w+) (\w+) = (.+);

  const $2: $1 = $3;
```

# callback

# callback constructor

# namespace

https://webidl.spec.whatwg.org/#idl-namespaces

# variadic (...)

https://webidl.spec.whatwg.org/#dfn-variadic
